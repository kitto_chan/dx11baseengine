# 就活作品

## 私のポートフォリオ
詳しく説明もポートフォリオの中でご覧ください

https://sites.google.com/view/kittochan/tales-of-sword


## <font color="red">注意事項</font>
日本語を使用したフォルダ名・パスですと、各プロジェクトに用意した bat ファイルが正常に機能しません。お手数をおかけしますが、フォルダ名とそのパスには日本語を使用しないようお願いいたします。

## ビルド
`open.bat`を実行するとプロジェクトを生成します。自動的にVisual Studio 2019で開きます。（お勧め）

もしくは

`make.bat`を実行するとプロジェクトを生成します。

## 開発概要
2年生から(2021/05~)開発を始めました。  
このプロジェクトの一部ソースは先生から配布されたソースです。(大幅な改変を加えています)
主に使ってる機能：　
プロジェクトの設定
FBX モデルとアニメションの読み込む
Direct11のフレームワーク設定とヘルパクラス
詳しくは Gitで First Commit (タグ： TeacherSource)  この部分は先生から配布されたソースを流用しています。  
以降のコミットはすべて自作したものです。

Unityのようなオブジェクト指向、コンポーネント指向を目指して設計開発しました。

## Editor 画面
![アニメションエディタ](https://user-images.githubusercontent.com/46988847/144286606-5f5cdacb-a597-4e01-b143-10122dc222d9.png)

![シーンのデバッグ](https://user-images.githubusercontent.com/46988847/144286727-78a423e5-b745-410d-8a3f-0e3d2fe6bd26.png)

## 使ったライブラリー
 - DirectXTK       (いろんな便利の機能)
 - DirectXTex      (テクスチャー)
 - Bullet Physics3 (物理演算のため)
 - Imgui           (GUIのために)
 - Imguizmo        (GUIのために)
 - IconFonts       (Guiのために)   
 - OpenFBX         (Fbxファイルを読み込む)
 - Git             (ソース管理)
 - Premake5        (ソースコードから構成を自動的に構築するため)
 - hlslpp          (C++でシェーダと共通のシンタックス)
 - cereal          (シリアライザ)
 - assimp          (モデル読み込む)