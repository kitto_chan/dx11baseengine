﻿
require "setup"

--============================================================================
-- ソリューションファイル
--============================================================================
config_solution("D3D11BaseEngine")
    startproject "Game"		-- スタートアッププロジェクト名

	-- プリプロセッサ #define
   	defines {
		--"_ITERATOR_DEBUG_LEVEL=0",	-- イテレーターのデバッグを高速化(他のライブラリと干渉する場合は削除)
	}

	-- リンカー警告抑制
	linkoptions {
		"-IGNORE:4006",	-- warning LNK4006 : symbolはxxxx.libで定義されています。2 つ目以降の定義は無視されます。
	}
--============================================================================
-- 外部のプロジェクトファイル
--============================================================================
group "OpenSource"

-----------------------------------------------------------------
-- DirectXTex
-----------------------------------------------------------------
config_project("DirectXTex", "StaticLib")

	characterset      "UNICODE"		-- UNICODE文字セット

	local SOURCE_PATH = "../opensource/DirectXTex"

	-- 追加するソースコード
	-- *=フォルダ内 **=フォルダ内とその階層下サブフォルダ内
    	files {
		path.join(SOURCE_PATH, "DirectXTex/**.cpp"),
		path.join(SOURCE_PATH, "DirectXTex/**.h"),
		path.join(SOURCE_PATH, "DirectXTex/**.inl"),
	}
	
	-- "" インクルードパス
	includedirs {
		SOURCE_PATH,
	}

	-- プリプロセッサ #define
   	defines {
		"_WIN32_WINNT=0x0A00",						-- _WIN32_WINNT_WIN10 (0x0A00)
		"_CRT_STDIO_ARBITRARY_WIDE_SPECIFIERS",
	}

	-- フォルダ分け
	vpaths {
		["ヘッダー ファイル/*"] = {
			path.join(SOURCE_PATH, "**.h"),
			path.join(SOURCE_PATH, "**.hxx"),
			path.join(SOURCE_PATH, "**.hpp"),
			path.join(SOURCE_PATH, "**.inl")
		},
		["ソース ファイル/*"] = {
			path.join(SOURCE_PATH, "**.c"),
			path.join(SOURCE_PATH, "**.cxx"),
			path.join(SOURCE_PATH, "**.cpp")
		},
	}

-----------------------------------------------------------------
-- DirectXTK
-----------------------------------------------------------------
config_project("DirectXTK", "StaticLib")

	characterset      "UNICODE"		-- UNICODE文字セット

	local SOURCE_PATH = "../opensource/DirectXTK"

	-- 追加するソースコード
	-- *=フォルダ内 **=フォルダ内とその階層下サブフォルダ内
    	files {
		path.join(SOURCE_PATH, "Inc/**.cpp"),
		path.join(SOURCE_PATH, "Inc/**.h"),
		path.join(SOURCE_PATH, "Inc/**.inl"),
		path.join(SOURCE_PATH, "Src/**.cpp"),
		path.join(SOURCE_PATH, "Src/**.h"),
		path.join(SOURCE_PATH, "Src/**.inl"),
		path.join(SOURCE_PATH, "Audio/**.h"),
		path.join(SOURCE_PATH, "Audio/**.cpp"),
	}

	-- 除去するファイル
	removefiles {
		path.join(SOURCE_PATH, "Inc/XboxDDSTextureLoader.h"),
		path.join(SOURCE_PATH, "Src/XboxDDSTextureLoader.cpp"),
	}

	-- "" インクルードパス
	includedirs {
		path.join(SOURCE_PATH, "Inc"),
		path.join(SOURCE_PATH, "Src"),
	}

	-- プリプロセッサ #define
    defines {
		"_WIN32_WINNT=0x0A00",						-- _WIN32_WINNT_WIN10 (0x0A00)
		"_CRT_STDIO_ARBITRARY_WIDE_SPECIFIERS",
	}

	-- プリコンパイル済ヘッダー
	pchheader( "pch.h" )
	pchsource( path.join(SOURCE_PATH, "Src/pch.cpp") )
	forceincludes ( "pch.h" )

-----------------------------------------------------------------
-- nlohmann(json ライブラリー)
-----------------------------------------------------------------
config_project("nlohmann", "StaticLib")

	local SOURCE_PATH = "../opensource/nlohmann"

	files
	{
		path.join(SOURCE_PATH, "json.hpp")
	}

	includedirs {
		SOURCE_PATH,
	}
-----------------------------------------------------------------
-- nlohmann(json ライブラリー)
-----------------------------------------------------------------
config_project("magicEnum", "StaticLib")

	local SOURCE_PATH = "../opensource/magic_enum"

	files
	{
		path.join(SOURCE_PATH, "include/magic_enum.hpp")
	}

	includedirs {
		SOURCE_PATH,
	}
-----------------------------------------------------------------
-- cereal(シリアライザ)
-----------------------------------------------------------------
config_project("cereal", "StaticLib")

	local SOURCE_PATH = "../opensource/cereal"

	files
	{
		path.join(SOURCE_PATH, "cereal.hpp")
	}

	includedirs {
		SOURCE_PATH,
	}
-----------------------------------------------------------------
-- hlslpp
-----------------------------------------------------------------
config_project("hlsl++", "StaticLib")

	local SOURCE_PATH = "../opensource/hlslpp"

	files
	{
		path.join(SOURCE_PATH, "include/**.h"),
		path.join(SOURCE_PATH, "include/*.natvis"),
	}

-----------------------------------------------------------------
-- imgui
-----------------------------------------------------------------
config_project("imgui", "StaticLib")

	characterset      "UNICODE"		-- UNICODE文字セット
   	-- warnings "Default"
	warnings "Off"
	
	local SOURCE_PATH = "../opensource/imgui"

	-- 追加するソースコード
	-- *=フォルダ内 **=フォルダ内とその階層下サブフォルダ内
    	files {
		path.join(SOURCE_PATH, "*.cpp"),
		path.join(SOURCE_PATH, "*.h"),
		path.join(SOURCE_PATH, "*.inl"),

		path.join(SOURCE_PATH, "backends/imgui_impl_dx11.*"),
		path.join(SOURCE_PATH, "backends/imgui_impl_win32.*"),
		
		path.join(SOURCE_PATH, "misc/cpp/imgui_stdlib.*"),
	}
	
	-- "" インクルードパス
	includedirs {
		SOURCE_PATH,
	}

	-- 除去するファイル
	removefiles {
	}

	-- プリプロセッサ #define
   	defines {
		"_WIN32_WINNT=0x0A00",						-- _WIN32_WINNT_WIN10 (0x0A00)
	}

	-- フォルダ分け
	vpaths {
		["ヘッダー ファイル/*"] = {
			path.join(SOURCE_PATH, "**.h"),
			path.join(SOURCE_PATH, "**.hxx"),
			path.join(SOURCE_PATH, "**.hpp"),
			path.join(SOURCE_PATH, "**.inl")
		},
		["ソース ファイル/*"] = {
			path.join(SOURCE_PATH, "**.c"),
			path.join(SOURCE_PATH, "**.cxx"),
			path.join(SOURCE_PATH, "**.cpp")
		},
	}

-----------------------------------------------------------------
-- ImGuizmo
-----------------------------------------------------------------
config_project("ImGuizmo", "StaticLib")

	local SOURCE_PATH = "../opensource/ImGuizmo"

	warnings "Off"
	
	files
	{
		path.join(SOURCE_PATH, "ImGuizmo.h"),
		path.join(SOURCE_PATH, "ImGuizmo.cpp")
	}
-----------------------------------------------------------------
-- ImguiFileDialog
-- ref https://github.com/aiekick/ImGuiFileDialog
-----------------------------------------------------------------
config_project("ImguiFileDialog", "StaticLib")

	local SOURCE_PATH = "../opensource/ImguiFileDialog"

	warnings "Off"
	
	files
	{
		path.join(SOURCE_PATH, "*.cpp"),
		path.join(SOURCE_PATH, "*.h")
	}
	
	includedirs {
		"../opensource/imgui",
	}

-----------------------------------------------------------------
-- IconFont
-- ref https://github.com/juliettef/IconFontCppHeaders
-----------------------------------------------------------------
config_project("IconFont", "StaticLib")
local SOURCE_PATH = "../opensource/IconFont"

	warnings "Off"
	
	files
	{
		path.join(SOURCE_PATH, "IconsFontAwesome5.h"),
	}

-----------------------------------------------------------------
-- OpenFBX
-----------------------------------------------------------------
config_project("OpenFBX", "StaticLib")

	local SOURCE_PATH = "../opensource/OpenFBX/src"

   	warnings "Default"

	-- 追加するソースコード
	-- *=フォルダ内 **=フォルダ内とその階層下サブフォルダ内
    	files {
		path.join(SOURCE_PATH, "*.c"),
		path.join(SOURCE_PATH, "*.cpp"),
		path.join(SOURCE_PATH, "*.h"),
	}
	
	-- "" インクルードパス
	includedirs {
		SOURCE_PATH,
	}

	-- 除去するファイル
	removefiles {
	}

	-- プリプロセッサ #define
    	defines {
	}

	-- フォルダ分け
	vpaths {
		["ヘッダー ファイル/*"] = {
			path.join(SOURCE_PATH, "**.h"),
			path.join(SOURCE_PATH, "**.hxx"),
			path.join(SOURCE_PATH, "**.hpp"),
			path.join(SOURCE_PATH, "**.inl")
		},
		["ソース ファイル/*"] = {
			path.join(SOURCE_PATH, "**.c"),
			path.join(SOURCE_PATH, "**.cxx"),
			path.join(SOURCE_PATH, "**.cpp")
		},
	}


-----------------------------------------------------------------
-- bullet
-----------------------------------------------------------------
config_project("bullet", "StaticLib")

	local SOURCE_PATH = "../opensource/bullet3/src"

   	warnings "Off"

	-- 追加するソースコード
	-- *=フォルダ内 **=フォルダ内とその階層下サブフォルダ内
    	files {
		path.join(SOURCE_PATH, "**.c"),
		path.join(SOURCE_PATH, "**.cpp"),
		path.join(SOURCE_PATH, "**.h"),
	}
	
	-- "" インクルードパス
	includedirs {
		SOURCE_PATH,
	}

	-- 除去するファイル
	removefiles {
	}

	-- プリプロセッサ #define
	defines {
		"BT_USE_SSE_IN_API",	-- [bullet] SSE命令の使用
		"BT_THREADSAFE",		-- [bullet] マルチスレッド時スレッドセーフ
	}

	-- フォルダ分け
	vpaths {
		["ヘッダー ファイル/*"] = {
			path.join(SOURCE_PATH, "**.h"),
			path.join(SOURCE_PATH, "*.hxx"),
			path.join(SOURCE_PATH, "**.hpp"),
			path.join(SOURCE_PATH, "**.inl")
		},
		["ソース ファイル/*"] = {
			path.join(SOURCE_PATH, "**.c"),
			path.join(SOURCE_PATH, "**.cxx"),
			path.join(SOURCE_PATH, "**.cpp")
		},
	}

-----------------------------------------------------------------
-- meshoptimizer
-----------------------------------------------------------------
config_project("meshoptimizer", "StaticLib")

	local SOURCE_PATH = "../opensource/meshoptimizer/src"

   	warnings "Off"

	-- 追加するソースコード
	-- *=フォルダ内 **=フォルダ内とその階層下サブフォルダ内
    	files {
		path.join(SOURCE_PATH, "**.c"),
		path.join(SOURCE_PATH, "**.cpp"),
		path.join(SOURCE_PATH, "**.h"),
	}
	
	-- "" インクルードパス
	includedirs {
		SOURCE_PATH,
	}

	-- 除去するファイル
	removefiles {
	}

	-- プリプロセッサ #define
	defines {
	}

	-- フォルダ分け
	vpaths {
		["ヘッダー ファイル/*"] = {
			path.join(SOURCE_PATH, "**.h"),
			path.join(SOURCE_PATH, "*.hxx"),
			path.join(SOURCE_PATH, "**.hpp"),
			path.join(SOURCE_PATH, "**.inl")
		},
		["ソース ファイル/*"] = {
			path.join(SOURCE_PATH, "**.c"),
			path.join(SOURCE_PATH, "**.cxx"),
			path.join(SOURCE_PATH, "**.cpp")
		},
	}
-----------------------------------------------------------------
-- Effekseer
-----------------------------------------------------------------
config_project("Effekseer", "StaticLib")

	local SOURCE_PATH = "../opensource/Effekseer/"

   	warnings "Off"

	-- 追加するソースコード
	-- *=フォルダ内 **=フォルダ内とその階層下サブフォルダ内
    files {
		path.join(SOURCE_PATH, "main.cpp"),
		path.join(SOURCE_PATH, "include/Effekseer/**.*"),
		path.join(SOURCE_PATH, "include/EffekseerRendererDX11/**.*")
	}
	
	-- "" インクルードパス
	includedirs {
		SOURCE_PATH,
		path.join(SOURCE_PATH, "include/Effekseer"),
		path.join(SOURCE_PATH, "include/EffekseerRendererDX11")
	}
	
	libdirs {
		path.join(SOURCE_PATH, "lib")
	}
	
	-- 除去するファイル
	removefiles {
	}

	filter { "configurations:Debug" }
		links {
			"Effekseerd.lib",
			"EffekseerRendererDX11d.lib",
		}
	filter { "configurations:Release*" }
		links {
				"Effekseer.lib",
				"EffekseerRendererDX11.lib",
			}
	
	-- プリプロセッサ #define
   	defines {

	}
	-- フォルダ分け
	vpaths {
		["ヘッダー ファイル/*"] = {
			path.join(SOURCE_PATH, "**.h"),
			path.join(SOURCE_PATH, "*.hxx"),
			path.join(SOURCE_PATH, "**.hpp"),
			path.join(SOURCE_PATH, "**.inl")
		},
		["ソース ファイル/*"] = {
			path.join(SOURCE_PATH, "**.c"),
			path.join(SOURCE_PATH, "**.cxx"),
			path.join(SOURCE_PATH, "**.cpp")
		},
	}
	
	
--============================================================================
-- フレームワーク プロジェクトファイル
--============================================================================
group ""
config_project("_framework_", "StaticLib")

	local SOURCE_PATH = "../source/framework"

	-- 追加するソースコード
	-- *=フォルダ内 **=フォルダ内とその階層下サブフォルダ内
	files {
		path.join(SOURCE_PATH, "**.h"),
		path.join(SOURCE_PATH, "**.inl"),
		path.join(SOURCE_PATH, "**.cpp"),
		
		-- シェーダー
		path.join("../resource", "**.fx"),	
		path.join("../resource", "**.hlsli"),
		path.join("../resource", "**.hlsl")
	}
	
	-- "" インクルードパス
	includedirs {
		SOURCE_PATH,
		"../opensource",		-- オープンソース
		"../opensource/bullet3/src"	-- Bullet物理シミュレーション
	}

	-- 依存ライブラリ・プロジェクト
	links {
		"d3dcompiler",
		"d3d11",
		"dxgi",
	--	"dxguid",
	}

	-- プリプロセッサ #define
   	defines {
	--	"_CRT_SECURE_NO_WARNINGS",
		"BT_USE_SSE_IN_API",	-- [bullet] SSE命令の使用
		"BT_THREADSAFE",		-- [bullet] マルチスレッド時スレッドセーフ
	}

	-- プリコンパイル済ヘッダー
	pchheader "precompile.h"
	pchsource (path.join(SOURCE_PATH, "precompile.cpp"))
	forceincludes "precompile.h"

	-- フォルダ分け
	vpaths {
		["ヘッダー ファイル/*"] = {
			path.join(SOURCE_PATH, "**.h"),
			path.join(SOURCE_PATH, "**.hxx"),
			path.join(SOURCE_PATH, "**.hpp"),
			path.join(SOURCE_PATH, "**.inl")
		},
		["ソース ファイル/*"] = {
			path.join(SOURCE_PATH, "**.c"),
			path.join(SOURCE_PATH, "**.cxx"),
			path.join(SOURCE_PATH, "**.cpp")
		},
	}
	
		-- Shader設定
	shadermodel("5.0")
		shaderassembler("AssemblyCode")
		local shader_dir = "../resource"
		
		-- HLSL files that don't end with 'Extensions' will be ignored as they will be
		-- used as includes
		filter("files:**.hlsl")
			flags("ExcludeFromBuild")
			shaderobjectfileoutput(shader_dir.."%{file.basename}"..".cso")
			shaderassembleroutput(shader_dir.."%{file.basename}"..".asm")
		
		filter("files:**_PS.hlsl")
			removeflags("ExcludeFromBuild")
			shadertype("Pixel")
			shaderentry("PS")
		filter("files:**_VS.hlsl")
			removeflags("ExcludeFromBuild")
			shadertype("Vertex")
			shaderentry("VS")
	
	-- Warnings as errors
	shaderoptions({"/WX"})
--============================================================================
-- network プロジェクトファイル
--============================================================================
group ""
config_project("_network_", "StaticLib")

	local SOURCE_PATH = "../source/network"

	-- 追加するソースコード
	-- *=フォルダ内 **=フォルダ内とその階層下サブフォルダ内
	files {
		path.join(SOURCE_PATH, "**.h"),
		path.join(SOURCE_PATH, "**.cpp"),
	}
	
	-- "" インクルードパス
	includedirs {
		SOURCE_PATH,
		"../opensource",		-- オープンソース
	}

	-- 依存ライブラリ・プロジェクト
	links {

	}

	-- プリプロセッサ #define
   	defines {

	}

	-- プリコンパイル済ヘッダー
	pchheader "precompile.h"
	pchsource (path.join(SOURCE_PATH, "precompile.cpp"))
	forceincludes "precompile.h"

	-- フォルダ分け
	vpaths {
		["ヘッダー ファイル/*"] = {
			path.join(SOURCE_PATH, "**.h"),
			path.join(SOURCE_PATH, "**.hxx"),
			path.join(SOURCE_PATH, "**.hpp"),
			path.join(SOURCE_PATH, "**.inl")
		},
		["ソース ファイル/*"] = {
			path.join(SOURCE_PATH, "**.c"),
			path.join(SOURCE_PATH, "**.cxx"),
			path.join(SOURCE_PATH, "**.cpp")
		},
	}
	
--============================================================================
-- プロジェクトファイル
--============================================================================
group ""
config_project("Game", "WindowedApp")

	local SOURCE_PATH    = "../source/app"
	local NETWORK_PATH   = "../source/network"
	local FRAMEWORK_PATH = "../source/framework"

	debugdir  "../resource"			-- 実行開始時のカレントディレクトリ

	-- 追加するソースコード
	-- *=フォルダ内 **=フォルダ内とその階層下サブフォルダ内
    files {
		path.join(SOURCE_PATH, "**.h"),
		path.join(SOURCE_PATH, "**.inl"),
		path.join(SOURCE_PATH, "**.cpp"),
	}
	
	-- "" インクルードパス
	includedirs {
		SOURCE_PATH,
		FRAMEWORK_PATH,
		NETWORK_PATH,
		"../opensource",						-- オープンソース
		"../opensource/bullet3/src",				-- Bullet物理シミュレーション
	}

	-- ライブラリディレクトリ
	libdirs {
		
	}

	-- 依存ライブラリ・プロジェクト
	links {
	}

	-- プリプロセッサ #define
   	defines {
	--	"_CRT_SECURE_NO_WARNINGS",
		"BT_USE_SSE_IN_API",	-- [bullet] SSE命令の使用
		"BT_THREADSAFE",		-- [bullet] マルチスレッド時スレッドセーフ
	}

	-- プリコンパイル済ヘッダー
	pchheader "precompile.h"
	pchsource (path.join(SOURCE_PATH, "precompile.cpp"))
	forceincludes "precompile.h"

	-- フォルダ分け
	vpaths {
		["ヘッダー ファイル/*"] = {
			path.join(SOURCE_PATH, "**.h"),
			path.join(SOURCE_PATH, "**.hxx"),
			path.join(SOURCE_PATH, "**.hpp"),
			path.join(SOURCE_PATH, "**.inl")
		},
		["ソース ファイル/*"] = {
			path.join(SOURCE_PATH, "**.c"),
			path.join(SOURCE_PATH, "**.cxx"),
			path.join(SOURCE_PATH, "**.cpp")
		},
	}

	links {
		"DirectXTex",
		"DirectXTK",
		"imgui",
		"ImGuizmo",
		"OpenFBX",
		"bullet",
		"meshoptimizer",
		"ImguiFileDialog",
		"Effekseer",
		"_framework_",
		"_network_"
	}

	filter { 'configurations:Debug' }
		linkoptions {'/SUBSYSTEM:CONSOLE', 
					 "-IGNORE:4099" -- warning LNK 4099 pdb warning 
					}
