@echo off
@setlocal enabledelayedexpansion

pushd "%~dp0"
call cleanup.bat warm
popd

::---------------------------------------------------------------
:: .buildフォルダを作成
:: 環境変数BUILD_PATHを設定した場合は作成先を移動可能。
::---------------------------------------------------------------
pushd "%~dp0"

:: すでにフォルダが存在していたら作成をスキップ
if exist .build goto skip

:: カスタムビルドパスの環境変数が存在しなかった場合はスキップ
if not defined BUILD_PATH goto skip

echo 環境変数BUILD_PATHが設定されています。%BUILD_PATH%
echo BUILD_PATH=%BUILD_PATH%

:: シンボリックリンク生成には管理者権限が必要なため自動昇格
whoami /priv | find "SeDebugPrivilege" > nul
if %errorlevel% neq 0 (
	echo 管理者権限がありませんでした。管理者権限に昇格して実行します
	echo 待機中です...
	rem 新規に管理者権限のウィンドウを起動。実行終了まで待つ (-wait)
	powershell start-process "%~0" -verb runas -wait
	goto skip
)

echo 管理者権限に自動で昇格できました。

:: テンポラリフォルダ用にGUIDを取得
for /f %%a in ('powershell -command "$([guid]::NewGuid().ToString())"') do ( set GUID=%%a )

:: シンボリックリンク接続先のフォルダ生成
if not exist "%BUILD_PATH%\.build\%GUID%" (
	md "%BUILD_PATH%\.build\%GUID%"
)

:: シンボリックリンク作成
mklink /d .build "%BUILD_PATH%\.build\%GUID%"
::　終了
exit /b 0

:skip
::---------------------------------------------------------------
:: プロジェクトファイルを生成
::---------------------------------------------------------------

bin\premake5.exe vs2019 --file=bin/premake5.lua

if %ERRORLEVEL% neq 0 (
	echo.
	echo エラーが発生しました。Luaスクリプトを確認してください。
	pause
)

popd

:finish
exit /b 0
