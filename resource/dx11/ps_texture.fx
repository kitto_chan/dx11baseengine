//----------------------------------------------------------------------------
//!	@file	ps_texture.fx
//!	@brief	テクスチャありピクセルシェーダー
//----------------------------------------------------------------------------

Texture2D		Texture : register(t0);
SamplerState	Sampler : register(s0);

cbuffer ColorCB : register(b1)
{
	float4	meshColor_;	// メッシュの色
};


// 頂点シェーダー出力
struct VS_OUTPUT
{
	float4	position_ : SV_Position;
	float4	color_    : COLOR;
	float2	uv_		  : TEXCOORD0;
};

// ピクセルシェーダー出力
struct PS_OUTPUT
{
	float4	color0_ : SV_Target0;
};

//----------------------------------------------------------------------------
// ピクセルシェーダー
//----------------------------------------------------------------------------
PS_OUTPUT main(VS_OUTPUT input)
{
	PS_OUTPUT	output = (PS_OUTPUT)0;

    output.color0_ = Texture.Sample(Sampler, input.uv_) * input.color_ * meshColor_;

	return output;
}
