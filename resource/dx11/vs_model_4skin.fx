//----------------------------------------------------------------------------
//!	@file	vs_model_4skin.fx
//!	@brief	3Dモデル4weightスキニング
//----------------------------------------------------------------------------
#include "../gameSource/Camera_H.hlsli"

// 定数バッファ
cbuffer WorldCB : register(b0)
{
    matrix matWorld; // ワールド行列
};

cbuffer ModelCB : register(b2)
{
    matrix matJoints_[512]; // 関節行列
};

// 頂点シェーダー入力
struct VS_INPUT
{
    float4 position_ : POSITION;
    float4 color_ : COLOR;
    float2 uv_ : TEXCOORD;
    float3 normal_ : NORMAL;
    float3 tangent_ : TANGENT;
    int4   skinIndex_ : BLENDINDICES;
    float4 skinWeight_ : BLENDWEIGHT;
};

// 頂点シェーダー出力
struct VS_OUTPUT
{
    float4 position_ : SV_Position;
    float4 color_ : COLOR;
    float2 uv_ : TEXCOORD0;
    float3 normal_ : NORMAL;
    float3 tangent_ : TANGENT;
    float3 worldPosition_ : WORLD_POSITION;
};

//----------------------------------------------------------------------------
// 頂点シェーダー
//----------------------------------------------------------------------------
VS_OUTPUT main(VS_INPUT input)
{
    VS_OUTPUT output = (VS_OUTPUT) 0;

	//-----------------------------------------------------------
	// スキニング計算
	//-----------------------------------------------------------
    float4 position = 0;
    float3 normal = 0;
    float3 tangent = 0;
	{
        int4 indices = input.skinIndex_;
        float4 weights = input.skinWeight_;
        for (int i = 0; i < 4; ++i)
        {
            if (weights.x == 0.0)
                break;
            position += mul(matJoints_[indices.x], input.position_) * weights.x;
            normal += mul(matJoints_[indices.x], float4(input.normal_, 0)).xyz * weights.x;
            tangent += mul(matJoints_[indices.x], float4(input.tangent_, 0)).xyz * weights.x;
			
            indices = indices.yzwx;
            weights = weights.yzwx;
        }
        position.w = 1.0;
    }

	// 座標変換
    position = mul(matWorld, position);
    normal = mul(matWorld, float4(normal, 0.0)).xyz;
    tangent = mul(matWorld, float4(tangent, 0.0)).xyz;
    output.worldPosition_ = position.xyz; // ワールド座標

    position = mul(matView, position);
    output.position_ = mul(matProj, position);

    output.color_ = input.color_;
    output.uv_ = input.uv_;
    output.normal_ = normal;
    output.tangent_ = tangent;
	//-----------------------------------------------------------
	// [DEBUG] スキニングの関節番号を簡易色分け表示
	//-----------------------------------------------------------
    if (0)
    {
        output.color_ = 0.0;
        for (int i = 0; i < 4; ++i)
        {
            int n = input.skinIndex_.x;
            output.color_ += float4(float((n) & 1) * 0.5 + float((n >> 3) & 3) * 0.25,
				float((n >> 1) & 1) * 0.5 + float((n >> 3) & 3) * 0.25,
				float((n >> 2) & 1) * 0.5 + float((n >> 3) & 3) * 0.25,
				1.0) * input.skinWeight_.x;
            input.skinIndex_ = input.skinIndex_.yzwx;
            input.skinWeight_ = input.skinWeight_.yzwx;
        }
    }

	//-----------------------------------------------------------
	// [DEBUG] 法線を簡易色分け表示
	//-----------------------------------------------------------
    if (0)
    {
        output.color_.rgb = output.normal_;
    }

    return output;
}
