//----------------------------------------------------------------------------
//!	@file	ps_texture.fx
//!	@brief	テクスチャあり
//----------------------------------------------------------------------------
Texture2D		texture0 : register(t0);
SamplerState	sampler0 : register(s0);

// 頂点シェーダー出力
struct VS_OUTPUT
{
	float4	position_ : SV_Position;
	float2	uv_       : TEXCOORD0;
};

//----------------------------------------------------------------------------
// テクスチャあり
//----------------------------------------------------------------------------
float4 main(VS_OUTPUT input) : SV_Target
{
	return texture0.Sample(sampler0, input.uv_);
}
