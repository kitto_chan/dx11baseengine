//----------------------------------------------------------------------------
//!	@file	vs_quad2d.fx
//!	@brief	クアッド四角形描画専用頂点シェーダー
//----------------------------------------------------------------------------

// 頂点シェーダー出力
struct VS_OUTPUT
{
	float4	position_ : SV_Position;
	float2	uv_       : TEXCOORD0;
	//float4  _color    : COLOR;
};

//----------------------------------------------------------------------------
// 頂点シェーダー
//----------------------------------------------------------------------------
VS_OUTPUT main(uint vertexId : SV_VertexID)
{
	VS_OUTPUT output = (VS_OUTPUT)0;

	// 座標生成
	// Vertex Shader Tricks by Bill Bilodeau - AMD at GDC14 より
	// 頂点バッファ入力なしに頂点IDから頂点座標を生成
	output.position_.x = float(vertexId / 2) * 4.0 - 1.0;
	output.position_.y = float(vertexId % 2) * 4.0 - 1.0;
	output.position_.z = 0.0;
	output.position_.w = 1.0;

	// テクスチャ座標
	output.uv_.x = float(vertexId / 2) * 2.0;
	output.uv_.y = 1.0 - float(vertexId % 2) * 2.0;
	return output;
}
