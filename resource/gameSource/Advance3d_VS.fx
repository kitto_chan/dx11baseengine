//----------------------------------------------------------------------------
//!	@file	Advance3d_VS.fx
//!	@brief	3D頂点シェーダー
//----------------------------------------------------------------------------
#include "Camera_H.hlsli"
#include "ShaderHelper_H.hlsli"

// 定数バッファ
cbuffer WorldCB : register(b0)
{
    matrix matWorld; // ワールド行列
};

// 頂点シェーダー入力
struct VS_INPUT
{
    float3 _pos : POSITION;
    float3 _nor : NORMAL;
    float3 _tan : TANGENT;
    float2 _uv  : TEXCOORD0;
};
// 頂点シェーダー出力
struct VS_OUTPUT
{
    float4 _pos : SV_POSITION;
    float4 _col : COLOR;
    float2 _uv : TEXCOORD0;
    float3 _nor : NORMAL;
    float3 _tan : TANGENT;
    
    float3 _worldPos : WORLD_POSITION;
};

VS_OUTPUT VS(VS_INPUT input)
{
    VS_OUTPUT output = (VS_OUTPUT) 0;
    output._pos = mul(matWorld, float4(input._pos, 1.0));
    
    output._worldPos = output._pos.xyz;
        
    output._pos = mul(matView, output._pos);
    output._pos = mul(matProj, output._pos);
  
    output._nor = mul(matWorld, float4(input._nor, 0.0)).xyz;
    output._tan = mul(matWorld, float4(input._tan, 0.0)).xyz;
    
    output._uv = input._uv;
    output._col = float4(1.0f, 1.0f, 1.0f, 1.0f);
    return output;
};