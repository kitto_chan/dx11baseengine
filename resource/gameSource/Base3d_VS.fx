//----------------------------------------------------------------------------
//!	@file	Base3d_VS.fx
//!	@brief	3D頂点シェーダー
//----------------------------------------------------------------------------
#include "Camera_H.hlsli"
// 定数バッファ
cbuffer WorldCB : register(b0)
{
    matrix matWorld; // ワールド行列
};
cbuffer InvWorldCB : register(b2)
{
    matrix invMatWorld; // ワールド行列
};

// 頂点シェーダー入力
struct VS_INPUT
{
    float3 _pos : POSITION;
    float3 _nor : NORMAL;
    float2 _uv  : TEXCOORD0;
};
// 頂点シェーダー出力
struct VS_OUTPUT
{
    float4 _pos : SV_POSITION;
    float3 _nor : NORMAL;
    float2 _uv  : TEXCOORD0;
    
    float3 _viewDir : TEXCOORD1;
    float3 _viewNor : TEXCOORD2;
};

VS_OUTPUT VS(VS_INPUT input)
{
    VS_OUTPUT output = (VS_OUTPUT) 0;
    output._pos = mul(matWorld, float4(input._pos, 1.0f));
    output._pos = mul(matView, output._pos);
    output._pos = mul(matProj, output._pos);
    
    output._uv = input._uv;
    output._nor = input._nor;

    matrix matWorldView = mul(matWorld, matView);
    //output._viewNor = mul((float3x3) invMatWorld, input._nor);
    output._viewNor = mul((float3x3) matWorldView, input._nor);
    output._viewDir = mul(mul(matWorld, matView), float4(input._pos, 1.0f)).xyz;
    
    return output;
}
