//----------------------------------------------------------------------------
//!	@file	Dissolve_PS.fx
//!	@brief	テクスチャあり
//----------------------------------------------------------------------------
#include "ShaderHelper_H.hlsli"
Texture2D _Texture0 : register(t0);
Texture2D _Texture1 : register(t1);
Texture2D _NoiseTexture : register(t2);
Texture2D _TextureGradient : register(t3);

SamplerState _Sampler0 : register(s0);

cbuffer DissolveCB : register(b0)
{
    float4 _EdgeColor;
    float _EdgeWidth;
    float _EdgeSoftness;
    float _Dissolve;
};

// 頂点シェーダー出力
struct VS_OUTPUT
{
    float4 _position : SV_POSITION;
    float4 _color : COLOR;
    float2 _uv : TEXCOORD0;
};

//----------------------------------------------------------------------------
// テクスチャあり
//----------------------------------------------------------------------------
float4 PS(VS_OUTPUT input) : SV_Target
{
    float hardEdge = _EdgeWidth / 2; //! 中心エッジの長さ
    float softEdge = hardEdge + _EdgeSoftness + EPSILON; //! 中心エッジに沿って拡散するエッジの長さ
    float dissolve = lerp(-softEdge, 1 + softEdge, _Dissolve); //!< ディゾルブエの値
	
    float4 tex0 = _Texture0.Sample(_Sampler0, input._uv);
    float4 tex1 = _Texture1.Sample(_Sampler0, input._uv);
    float mask = 1 - _NoiseTexture.Sample(_Sampler0, input._uv).r;
	
    float4 o = lerp(tex0, tex1, step(mask, dissolve)); // どちを消すか

    if (_EdgeWidth || _EdgeSoftness) // 長さは0なら影響がないので計算しない
    {
        float e = abs(mask - dissolve); //!< -|- 左右も同じの値
        float w = smoothstep(hardEdge, softEdge, e); //! 滑らかな値を返します
        float2 fireUv = float2(1 - w, 1);
        float4 texCol = _TextureGradient.Sample(_Sampler0, fireUv);
        texCol *= 2;
        
        o = lerp(texCol, o, saturate(w));
    }
    
    return o;

}
