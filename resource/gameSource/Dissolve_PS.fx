//----------------------------------------------------------------------------
//!	@file	Dissolve_PS.fx
//!	@brief	テクスチャあり
//----------------------------------------------------------------------------
Texture2D		_Texture0 : register(t0);
Texture2D		_Texture1 : register(t1);
Texture2D		_NoiseTexture : register(t2);
SamplerState	_Sampler0 : register(s0);

cbuffer DissolveCB : register(b0)
{
    float4 _EdgeColor;
    float _EdgeWidth;
    float _EdgeSoftness;
    float _Dissolve;
};

// 頂点シェーダー出力
struct VS_OUTPUT
{
	float4 _position : SV_POSITION;
	float2 _uv       : TEXCOORD0;
};

//----------------------------------------------------------------------------
// テクスチャあり
//----------------------------------------------------------------------------
float4 PS(VS_OUTPUT input) : SV_Target
{
	const static float epsilon = 1E-4;

	float dissolve = _Dissolve;

	float4 tex0 = _Texture0.Sample(_Sampler0, input._uv);
	float4 tex1 = _Texture1.Sample(_Sampler0, input._uv);
	float mask = 1 - _NoiseTexture.Sample(_Sampler0, input._uv).r;

	float4 o = lerp(tex0, tex1, step(mask, dissolve));

	return o;

}
