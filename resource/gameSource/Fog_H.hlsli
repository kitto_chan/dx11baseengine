//----------------------------------------------------------------------------
//!	@file   Fog.hlsli
//----------------------------------------------------------------------------

// 霧
cbuffer FogCB : register(b12)
{
    int isUseFog;
    float3 fogColor;

    float2 fogTexel;
    float minHeight; //頂点の高さの最小値。頂点の高さがこの値より小さくなるとフォグカラーで塗りつぶされる。
    float maxHeight; //頂点の高さの最大値。頂点の高さがこの値より大きくなるとフォグがかからなくなる。
};

static const float TEX_SCALE = 0.03f;

float4 CalFogColor(float4 texColor, in float3 worldpos, in Texture2D noise, in SamplerState sam)
{
    float4 outColor = 0;
    if (isUseFog)
    {
	    // 適当にずらす
        //float2 texel1 = float2(fogTexel.x * 3.0, 0.0);
        //float2 texel2 = float2(fogTexel.x * 0.3, fogTexel.y * 2.0);

        //// ノイズ計算
        //float uv1 = noise.Sample(sam, worldpos.xz * TEX_SCALE + texel1).r;
        //float uv2 = noise.Sample(sam, worldpos.xz * TEX_SCALE + texel2).r;
        //float finalUv = (uv2 - uv1 * 0.1f)* 1.15f;

        float2 texel1 = float2(fogTexel.x, 0.0f);
        float2 texel2 = float2(0.3f, fogTexel.y);

        float uv1 = noise.Sample(sam, worldpos.xz * TEX_SCALE + texel1).r;
        float uv2 = noise.Sample(sam, worldpos.xz * TEX_SCALE + texel2).r;
        float finalUv = (uv2 + uv1) * 0.5f;
        
        // 霧の計算
        // minHeight から　maxHeight に霧色をつけます
        float alpha = clamp((worldpos.y - minHeight) / (maxHeight - minHeight), 0.0f, 1.0f);
        alpha = 1.0f - (1.0f - alpha) * finalUv;

        float4 fogColorF4 = float4(fogColor, 1.0);
        outColor = texColor * alpha + fogColorF4 * (1.0 - alpha);
        outColor.a = 1;
    }
    else
    {
        outColor = texColor;
    }
    return outColor;
}