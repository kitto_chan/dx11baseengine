//----------------------------------------------------------------------------
//!	@file   LightHelper.hlsli
//!	@brief  ライティング用ヘルパーシェーダヘーダ
//! ＠ref   ライトの種類説明: https://docs.unity3d.com/ja/2018.4/Manual/Lighting.html
//----------------------------------------------------------------------------

//============================================================================
//! 構造体
//! @note 拡散反射 (Diffuse reflection)
//! @note 鏡面反射 (Specular reflection)
//! @note 環境反射 (Ambient reflection)
//============================================================================

//! 平行光源（ディレクショナルライト）
struct PhongDirectionalLight
{
    float4 _ambient;
    float4 _diffuse;
    float4 _specular;
    float3 _direction;
    int _useless;
};

//！ 点光源（ポイントライト）
struct PhongPointLight
{
    float4 _ambient;
    float4 _diffuse;
    float4 _specular;

    float3 _position;
    float _range;

    float3 _attenuation;
    int useless;
};

//！ スポットライト
struct PhongSpotLight
{
    float4 _ambient;
    float4 _diffuse;
    float4 _specular;

    float3 _position;
    float _range;

    float3 _direction;
    float _spot;

    float3 _attenuation;
    int _useless;
};

//! 物体表面の材質
struct PhongMaterial
{
    float4 _ambient;
    float4 _diffuse;
    float3 _specular;
    float  _specularShininess;
};

//! 材質のテクスチャーをマップ
struct PhongMaterialTexture
{
    float4 _specularMap;
};
//----------------------------------------------------------------------------
//! 平行光源を計算
//----------------------------------------------------------------------------
void ComputePhongDirectionalLight(PhongMaterial mat,
                                  PhongDirectionalLight dirLight,
	                              float3 N, float3 V,
	                              out float4 outAmbient,
	                              out float4 outDiffuse,
	                              out float4 outSpecular)
{
	// 初期化
    outAmbient = float4(0.0f, 0.0f, 0.0f, 0.0f);
    outDiffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
    outSpecular = float4(0.0f, 0.0f, 0.0f, 128.0f);

	// 光のベクトルは照射方向と反対
    float3 L = -dirLight._direction;

	// 環境光（アンビエント）を追加する
    outAmbient = mat._ambient * dirLight._ambient;

	// 拡散光と鏡面反射光を追加します 
    float diffuseFactor = dot(L, N);

    if (diffuseFactor > 0.0f)
    {
        float3 R = reflect(-L, N);
        float specFactor = pow(max(dot(R, V), 0.0f), mat._specularShininess);

        outDiffuse = diffuseFactor * mat._diffuse * dirLight._diffuse;
        outSpecular = specFactor * float4(mat._specular, 1) * dirLight._specular;
    }
}
//----------------------------------------------------------------------------
//! 点光源を計算
//----------------------------------------------------------------------------
void ComputePhongPointLight(PhongMaterial mat,
                            PhongPointLight pointLight,
                            float3 worldPos,
                            float3 N,
                            float3 V,
	                        out float4 outAmbient,
                            out float4 outDiffuse,
                            out float4 outSpecular)
{
	// 初期化
    outAmbient = float4(0.0f, 0.0f, 0.0f, 0.0f);
    outDiffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
    outSpecular = float4(0.0f, 0.0f, 0.0f, 0.0f);

	// 面から光源のベクトル
    float3 L = pointLight._position - worldPos;

	// 面から光源の距離
    float dist = length(L);

	// 範囲を超えたらライティングの計算はやめる
    if (dist > pointLight._range)
        return;

	// ライトのベクトルをノーマル化
    L /= dist;

	// 環境光を計算
    outAmbient = mat._ambient * pointLight._ambient;

	// 拡散光と鏡面反射光を計算する
    float diffuseFactor = dot(L, N);

    //! diffuseの計算
    if (diffuseFactor > 0.0f)
    {
        float3 R = reflect(-L, N);
        float specFactor = pow(max(dot(R, V), 0.0f), mat._specularShininess);

        outDiffuse = diffuseFactor * mat._diffuse * pointLight._diffuse;
        outSpecular = specFactor * float4(mat._specular, 1) * pointLight._specular;
    }

	// 光の衰弱を計算（弱くなる）
    float att = 1.0f / dot(pointLight._attenuation, float3(1.0f, dist, dist * dist));

    outDiffuse *= att;
    outSpecular *= att;
}

//----------------------------------------------------------------------------
//! スポットライトを計算
//----------------------------------------------------------------------------
void ComputePhongSpotLight(PhongMaterial mat,
                           PhongSpotLight spotLight,
                           float3 worldPos,
                           float3 N,
                           float3 V,
	                       out float4 outAmbient,
                           out float4 outDiffuse,
                           out float4 outSpecular)
{
	// 初期化
    outAmbient = float4(0.0f, 0.0f, 0.0f, 0.0f);
    outDiffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
    outSpecular = float4(0.0f, 0.0f, 0.0f, 0.0f);

	// 物体表面からライトのベクトル
    float3 L = spotLight._position - worldPos;

    // 表面からライト距離
    float dist = length(L);

	// 範囲内かどうかをチェック
    if (dist > spotLight._range)
        return;

	// ノーマル
    L /= dist;

	// 環境光の計算
    outAmbient = mat._ambient * spotLight._ambient;

    // 拡散反射と鏡面反射の計算
    float diffuseFactor = dot(L, N);

    //! diffuseの計算
    if (diffuseFactor > 0.0f)
    {
        float3 R = reflect(-L, N);
        float specFactor = pow(max(dot(R, V), 0.0f), mat._specularShininess);

        outDiffuse = diffuseFactor * mat._diffuse * spotLight._diffuse;
        outSpecular = specFactor * float4(mat._specular, 1) * spotLight._specular;
    }

	// スポットライトの計算
    float spot = pow(max(dot(-L, spotLight._direction), 0.0f), spotLight._spot);
    // 衰弱計算
    float att = spot / dot(spotLight._attenuation, float3(1.0f, dist, dist * dist));

    outAmbient  *= spot;
    outDiffuse  *= att;
    outSpecular *= att;
}
