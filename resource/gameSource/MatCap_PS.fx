//----------------------------------------------------------------------------
//!	@file	Base3d_VS.fx
//!	@brief	MatCap頂点シェーダー
//----------------------------------------------------------------------------
// 頂点シェーダー出力
Texture2D _TEX_MATCAP : register(t0);
SamplerState _SAMPLER0 : register(s0);

struct VS_OUTPUT
{
    float4 _pos : SV_POSITION;
    float3 _nor : NORMAL;
    float2 _uv : TEXCOORD0;
    
    float3 _viewDir : TEXCOORD1;
    float3 _viewNor : TEXCOORD2;
};

float4 MatCap(float3 viewNor, float3 viewDir)
{
    float3 N = normalize(viewNor);
    float3 V = normalize(viewDir);
    
    N -= V * dot(V, N);
    float2 uv = N.xy * 0.5 * 0.99 + 0.5;
    uv.y *= -1;
    return _TEX_MATCAP.Sample(_SAMPLER0, uv);
}

float4 PS(VS_OUTPUT input) : SV_Target
{
    float4 color = 0;
    
    color += MatCap(input._viewNor, input._viewDir);
    
    return color;
}