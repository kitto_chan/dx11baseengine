//----------------------------------------------------------------------------
//!	@file	vs_model_4skin.fx
//!	@brief	3Dモデル4weightスキニング
//----------------------------------------------------------------------------
#include "../gameSource/Camera_H.hlsli"

// 定数バッファ
cbuffer WorldCB : register(b0)
{
    matrix matWorld; // ワールド行列
};

cbuffer ModelCB : register(b2)
{
    matrix matJoints_[512]; // 関節行列
};

cbuffer OutlineCB : register(b3) {
    float3 outlineColor;
    float outlineWidth;
}
// 頂点シェーダー入力
struct VS_INPUT
{
    float4 position_ : POSITION;
    float4 color_ : COLOR;
    float2 uv_ : TEXCOORD;
    float3 normal_ : NORMAL;
    float3 tangent_ : TANGENT;
    int4   skinIndex_ : BLENDINDICES;
    float4 skinWeight_ : BLENDWEIGHT;
};

// 頂点シェーダー出力
struct VS_OUTPUT
{
    float4 position_ : SV_Position;
    float4 color_ : COLOR;
    float2 uv_ : TEXCOORD0;
};

//----------------------------------------------------------------------------
// 頂点シェーダー
//----------------------------------------------------------------------------
VS_OUTPUT main(VS_INPUT input)
{
    VS_OUTPUT output = (VS_OUTPUT) 0;

	//-----------------------------------------------------------
	// スキニング計算
	//-----------------------------------------------------------
    float4 position = 0;
    float3 normal = 0;
    float3 tangent = 0;
	{
        int4 indices = input.skinIndex_;
        float4 weights = input.skinWeight_;
        for (int i = 0; i < 4; ++i)
        {
            if (weights.x == 0.0)
                break;
            position += mul(matJoints_[indices.x], input.position_) * weights.x;
            normal += mul(matJoints_[indices.x], float4(input.normal_, 0)).xyz * weights.x;
			
            indices = indices.yzwx;
            weights = weights.yzwx;
        }
        position.w = 1.0;
    }

    position.xyz += normal * outlineWidth;

	// 座標変換
    position = mul(matWorld, position);
    normal = mul(matWorld, float4(normal, 0.0)).xyz;

    position = mul(matView, position);
    output.position_ = mul(matProj, position);

    output.color_ = float4(outlineColor,1.0);
    output.uv_ = input.uv_;

    return output;
}
