//----------------------------------------------------------------------------
//!	@file	Dissolve_PS.fx
//!	@brief	テクスチャあり
//----------------------------------------------------------------------------
#include "Camera_H.hlsli"

Texture2D		_Texture0 : register(t2);
Texture2D		_NormalMap : register(t1);
SamplerState	_Sampler0 : register(s0);

// 頂点シェーダー出力
struct VS_OUTPUT
{
    float4 _pos : SV_POSITION;
    float4 _color : COLOR;
    float2 _uv  : TEXCOORD0;
    float3 _nor : NORMAL;
    float3 _tan : TANGENT;
    
    float3 _worldPos : POSITION;
};

//----------------------------------------------------------------------------
// テクスチャあり
//----------------------------------------------------------------------------
float4 PS(VS_OUTPUT input) : SV_Target
{
    float3 T = normalize(input._tan);
    float3 N = normalize(input._nor);
    float3x3 TBN = float3x3(T, cross(N, T), N);
    N = _NormalMap.Sample(_Sampler0, input._uv).xyz * 2 - 1;
    N = mul(N, TBN);
    float3 lightPos = { 0, 5, 0 };
    float3 L = normalize(lightPos - input._worldPos);
    float3 V = normalize(eyePos - input._worldPos);
    float3 R = reflect(L, N);
    
    float4 diffuseColor = float4(1, 1, 1, 1);
    float4 specularColor = float4(1, 1, 1, 1);
    float4 ambientColor = float4(0, 0, 0, 1);
    float4 ambient = ambientColor;
    float4 diffuse = diffuseColor * _Texture0.Sample(_Sampler0, input._uv) * max(0, dot(N, L));
    float specularAngle = max(0, dot(-R, V));
    float specularShininess = 128;
    float4 specular = specularAngle * pow(specularAngle, specularShininess);
    
    float4 color = 0;
    color += ambient;
    color += diffuse;
    color += specular;

    return color;
}
