//----------------------------------------------------------------------------
//!	@file	PBRModel_PS.fx
//!	@brief	モデル用ピクセルシェーダー
//----------------------------------------------------------------------------
#include "Camera_H.hlsli"
#include "ShaderHelper_H.hlsli"
#include "Shadow_H.hlsli"

#define MAX_LIGHT 2

struct IncidentLight
{
    float3 _color;
    float3 _direction;
    bool _visible;
};

struct PBRDirectionalLight
{
    float3 _color;
    int useless1;
    
    float3 _direction;
    int useless2;
};

struct PBRPointLight
{
    float3 _color;
    float _range;
    
    float3 _position;
    float _decay;
};

cbuffer PBRLightingCB : register(b7)
{
    PBRDirectionalLight directionalLights[MAX_LIGHT];
    PBRPointLight pointLights[MAX_LIGHT];
    
    int PBRDirectionalLightNum;
    int PBRPointLightNum;
}

Texture2D Texture : register(t0);

Texture2D Texture_AO : register(t20);
Texture2D Texture_N : register(t21);
Texture2D Texture_M : register(t22);
Texture2D Texture_R : register(t23);

SamplerState Sampler : register(s0);

TextureCube TextureIBLDiffuse : register(t16);
TextureCube TextureIBLSpecular : register(t17);

// 頂点シェーダー出力
struct VS_OUTPUT
{
    float4 _position : SV_Position;
    float4 _color : COLOR;
    float2 _uv : TEXCOORD0;
    float3 _normal : NORMAL;
    float3 _tangent : TANGENT;
    float3 _worldPosition : WORLD_POSITION;
};

// ピクセルシェーダー出力
struct PS_OUTPUT
{
    float4 _color0 : SV_Target0;
};

// GGX (Trowbridge-Reitz model)
float D_GGX(float roughness, float NdotH)
{
    float alpha = max(0.001, roughness * roughness);
    float alpha2 = alpha * alpha;
    float NdotH2 = NdotH * NdotH;

    float denominator = NdotH2 * (alpha2 - 1.0) + 1.0;
    denominator *= denominator * PI;

    return alpha2 / denominator;
}
//----------------------------------------------------------
// ジオメトリG項 Smith
//----------------------------------------------------------
float G_Smith(float roughness, float NdotV, float NdotL)
{
    float alpha = max(0.001, roughness * roughness);
    float k = alpha * 0.5f;
    float GV = NdotV / (NdotV * (1 - k) + k);
    float GL = NdotL / (NdotL * (1 - k) + k);

    return GV * GL;
}
//----------------------------------------------------------
// Schlickの近似式 
//----------------------------------------------------------
float3 F_Schlick(float3 specularColor, float NdotV)
{
	// R0 + (1 - R0)(1 - NdotV)^5
    return specularColor + (1 - specularColor) * pow(1 - NdotV, 5);
}
//----------------------------------------------------------
// [鏡面反射光] Cook-Torrance
//----------------------------------------------------------
float3 SpecularBRDF(float3 N, float3 L, float3 V, float3 specularColor, float roughnessFactor)
{
    float3 specularBRDF = (float3) 0;
	
    float3 H = normalize(V + L);

    float LdotH = dot(L, H);
    float NdotH = dot(N, H);
    float NdotV = saturate(dot(N, V));
    float NdotL = saturate(dot(N, L));
    //----------------------------------------------------------
	// [鏡面反射光] Cook-Torrance
	//----------------------------------------------------------
	// BRDF (Bidirectional Reflectance Distribution Function) 双方向反射率分布関数
	//
	//	光の量 = 光の色強さ * BRDF * NdotL;
	// 
	// Cook-Torranceモデル (BRDF)
	//     F  *  G  *  D
	// ----------------------
	//   4 * NdotL * NdotV 
	//
	//	F = Schlickの近似式
	//  G = Smith
	//  D = GGX (Trowbridge-Reitzモデル)
#if 1
    specularBRDF = F_Schlick(specularColor, NdotV) * G_Smith(roughnessFactor, NdotV, NdotL) * D_GGX(roughnessFactor, NdotH)
		/ //------------------------------------------------------------------------------------------		
		(4.0 * NdotL * NdotV);
#else

	// 最適化Ver.
	// [SIGGRAPH2015] Optimizing PBR
	float	roughness4 = roughness * roughness * roughness * roughness;
	float	NdotH2 = NdotH * NdotH;
	float	LdotH2 = LdotH * LdotH;

	float	denominator = NdotH2 * (roughness4 - 1.0) + 1.0;
	denominator *= 4.0 * PI * denominator * LdotH2 * (roughness + 0.5);

		specularBRDF = roughness4 * specularColor
		/ //------------------------------
		denominator;

#endif
    return specularBRDF;
}

float LightIntensityToIrradianceFactor(const in float lightDistance, const in float cutoffDistance, const in float decayExponent)
{
    if (1)
    {
        // 衰弱計算(適当に)
        // TODO better
        float att = 1.0f / dot(float3(decayExponent, decayExponent, decayExponent), float3(1.0f, lightDistance, lightDistance * lightDistance));
        return att;
    }
    else
    {
        // バッグある
        // TODO fix
        if (decayExponent > 0.0)
        {
            return pow(saturate(-lightDistance / cutoffDistance + 1.0), decayExponent);
        }
  
        return 1.0;
    }
}

void GetDirectionalDirectLightIrradiance(const in PBRDirectionalLight directionalLight,
                                         out IncidentLight outIncidentLight)
{
    outIncidentLight._color = directionalLight._color;
    outIncidentLight._direction = normalize(-directionalLight._direction);
    outIncidentLight._visible = true;
}

void GetPointDirectLightIrradiance(const in PBRPointLight pointLight, const in float3 worldPos,
                                   out IncidentLight outIncidentLight)
{
    float3 L = pointLight._position - worldPos;
    outIncidentLight._direction = normalize(L);
    
    float lightDistance = length(L);
    
    if (lightDistance < pointLight._range)
    {
        outIncidentLight._color = pointLight._color;
        outIncidentLight._color *= LightIntensityToIrradianceFactor(lightDistance, pointLight._range, pointLight._decay);
        outIncidentLight._visible = true;
    }
    else
    {
        outIncidentLight._color = float3(0.0f, 0.0f, 0.0f);
        outIncidentLight._visible = false;
    }
}

void ComputePBRLighting(const in IncidentLight incidentLight,
                        float3 V, float3 N,
                        const in float3 albedo,
                        const in float3 specularColor,
                        const in float roughness,
                        const in float metalness,
                        out float3 outColor)
{
    outColor = float3(0.0, 0.0, 0.0);
    float3 L = incidentLight._direction;
    
    float NdotL = saturate(dot(N, L));
	//----------------------------------------------------------
	// [拡散反射光] 正規化Lambert (normalized lambert)
	//----------------------------------------------------------
    static const float Kd = PI;
    float diffuse = saturate(dot(N, L)) * (1.0 / Kd);

    float3 specularBRDF = SpecularBRDF(N, L, V, specularColor, roughness);
        
	// 光量計算
    float3 lightColor = incidentLight._color * 5;

    float3 diffuseTerm = lightColor * diffuse * albedo;
    float3 specularTerm = lightColor * specularBRDF * NdotL;

    outColor = diffuseTerm * (1 - metalness) + specularTerm;
	//	float3 color = lerp(diffuseTerm, specularTerm, metalness);

	////-------------------------------------------------------------
	//// Image Based Lighting (IBL)
	////-------------------------------------------------------------
	//float3	diffuseIBL = TextureIBLDiffuse.SampleLevel(Sampler, N, 0).rgb;

	//color.rgb = diffuseIBL;
}

cbuffer WorldCB : register(b0)
{
    matrix matWorld_; // ワールド行列
};

//----------------------------------------------------------------------------
// ピクセルシェーダー
//----------------------------------------------------------------------------
PS_OUTPUT main(VS_OUTPUT input)
{
    PS_OUTPUT output = (PS_OUTPUT) 0;
    
    output._color0 = Texture.Sample(Sampler, input._uv);
	// 透明度85%以下はピクセル棄却(くりぬき)
    if (output._color0.a < 0.85)
        discard;

    // [DEBUG] 色表示
	//	output.color0_ *= input.color_;

	// [DEBUG] 法線表示
    //output.color0_ = float4(input.normal_, 1);
    //return output;
	
    float3 N = normalize(input._normal);
    float3 T = normalize(input._tangent);
    float3 V = normalize(eyePos - input._worldPosition);
    
    //float3 T = normalize(input.tangent_ - dot(input.tangent_, N) * N);

    float3 normalMapSample = Texture_N.Sample(Sampler, input._uv).xyz;
    N = NormalMapSampleToWorldSpace(normalMapSample, N, T);
	
    // [DEBUG] 法線 / 接線表示
    //output.color0_ = float4(T, 1);
    //return output;
    
    float3 albedo = output._color0.rgb; // アルベド(材質の反射能)
    //================================================================
	// ラフネス
	//===============================================================
    //float roughness = 0.5f; // ラフネス
    float roughness = /*1 -*/ Texture_R.Sample(Sampler, input._uv).r;
    
    //================================================================
	// 金属度(metallic)
	//================================================================
    //float metalness = 0.0; // 金属度(metallic)
    float metalness = Texture_M.Sample(Sampler, input._uv).r;

    //================================================================
	// SpecularColor
	//================================================================
    float3 specularColor = lerp(float3(0.04, 0.04, 0.04), albedo, metalness);
    
    //===============================================================
    // PBR Lightingの計算
    //===============================================================
    IncidentLight inDirectLight;
    float3 outColor = float3(0.0, 0.0, 0.0);
    float3 finalColor = float3(0.0, 0.0, 0.0);
    int i = 0;
    for (i = 0; i < PBRDirectionalLightNum; ++i)
    {
        GetDirectionalDirectLightIrradiance(directionalLights[i], inDirectLight);
        if (inDirectLight._visible)
        {
            ComputePBRLighting(inDirectLight, V, N, albedo, specularColor, roughness, metalness, outColor);
            finalColor += outColor;
        }
    }
    
    for (i = 0; i < PBRPointLightNum; ++i)
    {
        GetPointDirectLightIrradiance(pointLights[i], input._worldPosition, inDirectLight);
        if (inDirectLight._visible)
        {
            ComputePBRLighting(inDirectLight, V, N, albedo, specularColor, roughness, metalness, outColor);
            finalColor += outColor;
        }
    }
	//-------------------------------------------------------------
	// 出力
	//-------------------------------------------------------------
    // ToneMap
    {
        float A = 0.15;
        float B = 0.50;
        float C = 0.10;
        float D = 0.20;
        float E = 0.02;
        float F = 0.30;
        float W = 11.2;
        float exposure = 2.;
        finalColor *= exposure;
        finalColor = ((finalColor * (A * finalColor + C * B) + D * E) / (finalColor * (A * finalColor + B) + D * F)) - E / F;
        float white = ((W * (A * W + C * B) + D * E) / (W * (A * W + B) + D * F)) - E / F;
        finalColor /= white;
        finalColor = pow(abs(finalColor), (1 / 2.2));
        //finalColor = finalColor / (finalColor + 1);
    }
    output._color0 = float4(finalColor, 1);

    return output;
}
