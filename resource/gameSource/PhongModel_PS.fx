//----------------------------------------------------------------------------
//!	@file	ps_model.fx
//!	@brief	モデル用ピクセルシェーダー
//----------------------------------------------------------------------------
#include "Camera_H.hlsli"
#include "Lighting_H.hlsli"
#include "ShaderHelper_H.hlsli"
#include "Fog_H.hlsli"
#include "Shadow_H.hlsli"

#define MAX_LIGHT 2

Texture2D _Texture : register(t0);
Texture2D _TextureNormal : register(t1);
Texture2D _TextureSpec : register(t2);

Texture2D _TextureFog : register(t3);
SamplerState _Sampler : register(s0);

cbuffer TextureMappingFlagCB : register(b6)
{
	int hasNormalMap;
}

cbuffer PhongLightingCB : register(b7)
{
	PhongDirectionalLight dirLights[MAX_LIGHT];
	PhongPointLight pointLights[MAX_LIGHT];
	PhongSpotLight spotLights[MAX_LIGHT];

	int dirLightNum;
	int pointLightNum;
	int spotLightNum;
};

cbuffer PhongMaterialCB : register(b8)
{
	PhongMaterial phongMaterial;
}
// 頂点シェーダー出力
struct VS_OUTPUT
{
	float4 _position : SV_Position;
	float4 _color : COLOR;
	float2 _uv : TEXCOORD0;
	float3 _normal : NORMAL;
	float3 _tangent : TANGENT;
	float3 _worldPosition : WORLD_POSITION;
};

// ピクセルシェーダー出力
struct PS_OUTPUT
{
	float4 _color : SV_Target0;
};

cbuffer MeshColorCB : register(b0)
{
	float4 meshColor = float4(1, 1, 1, 1); // メッシュの色
};

//----------------------------------------------------------------------------
// ピクセルシェーダー
//----------------------------------------------------------------------------
PS_OUTPUT main(VS_OUTPUT input)
{
	PS_OUTPUT output = (PS_OUTPUT)0;

	output._color = _Texture.Sample(_Sampler, input._uv);

	// 透明度85%以下はピクセル棄却(くりぬき)
	if (output._color.a < 0.85)
		discard;

	output._color *= input._color * meshColor;

	float3 N = normalize(input._normal);
	float3 T = normalize(input._tangent);
	float3 V = normalize(eyePos - input._worldPosition);
	float V_dist = distance(eyePos, input._worldPosition);

	// 初期化
	float4 ambient = float4(0.0, 0.0, 0.0, 0.0);
	float4 diffuse = float4(0.0, 0.0, 0.0, 0.0);
	float4 spec = float4(0.0, 0.0, 0.0, 0.0);
	float4 A = float4(0.0, 0.0, 0.0, 0.0);
	float4 D = float4(0.0, 0.0, 0.0, 0.0);
	float4 S = float4(0.0, 0.0, 0.0, 0.0);
	int i;

	PhongMaterialTexture materialTexture = (PhongMaterialTexture)0;

	PhongMaterial pm = phongMaterial;
	pm._specular *= _TextureSpec.Sample(_Sampler, input._uv).r;
	//pm._diffuse *= output._color;
	if (hasNormalMap)
	{
		float3 normalMapSample = _TextureNormal.Sample(_Sampler, input._uv).xyz;
		N = NormalMapSampleToWorldSpace(normalMapSample, N, T);
	}

	for (i = 0; i < dirLightNum; ++i)
	{
		//-------------------------------------------
		// 影判定
		// 処理重くなるので、平行光だけやる
		//-------------------------------------------
		float shadow = 1.0f;
		{
			float4 shadowPosition = mul(matLightViewProj, float4(input._worldPosition, 1));
			// ＊平行投影の場合はwで悪必要がない
			// スクリーン座標系（ -1.0f ～ 1.0f ）をＵＶ座標（ 0.0f ～ 1.0f ）に収めたい
			// 後々関数にしてもよし
			float2 shadowUv = shadowPosition.xy * float2(0.5, -0.5) + float2(0.5, 0.5);
            float alpha = saturate(2 - length(float2(0.5, 0.5) - shadowUv) * 4.0f);
  
            for (int j = 0; j < sampleCount; ++j)
            {
                float2 offset = vogelDiskSampling(j, sampleCount);
				// サンプリング
                float2 uv = shadowUv + offset * 0.001;
                float sceneDepth = shadowPosition.z;
#if 0
			float shadowDepth = TextureShadow.Sample(SamplerShadow, uv).r;
			//static const float SHADOW_BIAS = 0.0002;
			shadow += (sceneDepth < shadowDepth) ? 1.0 : 0.0;
#else
                shadow += TextureShadow.SampleCmp(SamplerShadow, uv, sceneDepth).r;
#endif
            }

            shadow = lerp(1.0, shadow, alpha);
			// Lambertと合成
            float3 L = -dirLights[i]._direction;
			float NdotL = saturate(dot(N, L));
            shadow *= 1.0 / sampleCount;
			shadow = min(shadow, NdotL);
		}

		ComputePhongDirectionalLight(pm,
			dirLights[i],
			N, V,
			A, D, S);
		ambient += A ;
        diffuse += D * shadow;
		spec += S  ;
	}

	for (i = 0; i < pointLightNum; ++i)
	{
		ComputePhongPointLight(pm,
			pointLights[i],
			input._worldPosition,
			N, V,
			A, D, S);
		ambient += A;
		diffuse += D;
		spec += S;
	}

	for (i = 0; i < spotLightNum; ++i)
	{
		ComputePhongSpotLight(pm,
			spotLights[i],
			input._worldPosition,
			N, V,
			A, D, S);
		ambient += A;
		diffuse += D;
		spec += S;
	}

	// ライトのambient , diffuse , specularを計算する
	float4 lightColor = output._color * (ambient + diffuse) + spec;
	lightColor.a = 1;
	output._color = lightColor;

	// 霧計算
	float4 fogColor = CalFogColor(lightColor, input._worldPosition, _TextureFog, _Sampler);
	output._color = fogColor;

	return output;
}
