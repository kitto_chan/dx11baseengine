//----------------------------------------------------------------------------
//!	@file	Dissolve_PS.fx
//!	@brief	テクスチャあり
//----------------------------------------------------------------------------
#include "ShaderHelper_H.hlsli"
Texture2D _Texture0 : register(t0);
SamplerState _Sampler0 : register(s0);

cbuffer ProcessBarCB : register(b11)
{
    float FillAmount;
}

// 頂点シェーダー出力
struct VS_OUTPUT
{
    float4 _position : SV_POSITION;
    float4 _color : COLOR;
    float2 _uv : TEXCOORD0;
};

//----------------------------------------------------------------------------
// テクスチャあり
//----------------------------------------------------------------------------
float4 PS(VS_OUTPUT input) : SV_Target
{
    if (input._uv.x > FillAmount)
    {
        discard;
    }

    return _Texture0.Sample(_Sampler0, input._uv);
}
