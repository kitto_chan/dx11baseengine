//----------------------------------------------------------------------------
//!	@file   ShaderHelper_H.hlsli
//!	@brief  ヘルパークラス
//----------------------------------------------------------------------------
static const float PI = 3.14159265359; // 円周率 π
static const float EPSILON =  1E-4; // アンダーフロー

float3 NormalMapSampleToWorldSpace(float3 normalMapSample, float3 normalW, float3 tangentW)
{
    // 渡すの変数すべて正規化した後べきだ
    float3 N = normalW;
    float3 T = tangentW;
    
    // 正規化
    //float3 N = normalize(normalW);
    //float3 T = normalize(tangentW);

    float3x3 TBN = float3x3(T, cross(T, N), N);
    
    // 0 ~ 1 > -1 ~ 1
    N = normalMapSample * 2 - 1;
    //N.y = -N.y;
    
    // 接ベクトル空間からワールド座標
    N = mul(N, TBN);
    
    return N;
}
