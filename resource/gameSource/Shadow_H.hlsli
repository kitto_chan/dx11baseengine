//----------------------------------------------------------------------------
//!	@file	shadow.h
//!	@brief	影関係
//----------------------------------------------------------------------------

// 定数バッファ
cbuffer ShadowCB : register(b5)
{
	matrix	matLightView;		// シャドウ用ビュー行列
	matrix	matLightProj;		// シャドウ用投影行列
	matrix	matLightViewProj;	// シャドウ用ビュー×投影行列
    
    int sampleCount; // サンプリング回数
};

// シャドウ用デプスバッファ
// テクスチャは１２０個ぐらいまで
Texture2D		TextureShadow : register(t15);
// サンプラーステートは１６個まで
SamplerComparisonState SamplerShadow : register(s15);

// 均一な円形サンプリング
// @return 中心を(0,0) としたオフセット値
float2 vogelDiskSampling(int i, int sampleCount, float rotation = 0.0)
{
    static const float GOLDEN_ANGLE = 2.4;
    float theta = GOLDEN_ANGLE * float(i) + rotation;

    float radius = sqrt(float(i) + 0.5) / sqrt(float(sampleCount));
    return float2(cos(theta), sin(theta)) * radius;
};