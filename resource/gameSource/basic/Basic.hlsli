//----------------------------------------------------------------------------
//!	@file   Basic.hlsli
//!	@brief  基本的な汎用3Dシェーダヘーダ
//----------------------------------------------------------------------------
//#include "LightHelper.hlsli"
struct DirectionalLight
{
    float4 Ambient;
    float4 Diffuse;
    float4 Specular;
    float3 Direction;
    float Pad;
};

//！ 点光源（ポイントライト）
struct PointLight
{
    float4 Ambient;
    float4 Diffuse;
    float4 Specular;

    float3 Position;
    float Range;

    float3 Att;
    float Pad;
};

//！ スポットライト
struct SpotLight
{
    float4 Ambient;
    float4 Diffuse;
    float4 Specular;

    float3 Position;
    float Range;

    float3 Direction;
    float Spot;

    float3 Att;
    float Pad;
};

//! 物体表面材质
struct Material
{
    float4 Ambient;
    float4 Diffuse;
    float4 Specular; // w = SpecPower
    float4 Reflect;
};
//----------------------------------------------------------------------------
//! 平行光源を計算
//----------------------------------------------------------------------------
void ComputeDirectionalLight(Material mat,
                             DirectionalLight L,
	                         float3 normal, 
                             float3 toEye,
	                         out float4 ambient,
	                         out float4 diffuse,
	                         out float4 spec)
{
	// 初期化
    ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
    diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
    spec = float4(0.0f, 0.0f, 0.0f, 0.0f);

	// 光のベクトルは照射方向と反対
    float3 lightVec = -L.Direction;

	// 環境光（アンビエント）を追加する
    ambient = mat.Ambient * L.Ambient;

	// 拡散光と鏡面反射光を追加します 
    float diffuseFactor = dot(lightVec, normal);

	// 実際の分岐命令を回避するのに平坦化する必要があるということをコンパイラに示唆します。
	[flatten]
    if (diffuseFactor > 0.0f)
    {
        float3 v = reflect(-lightVec, normal);
        float specFactor = pow(max(dot(v, toEye), 0.0f), mat.Specular.w);

        diffuse = diffuseFactor * mat.Diffuse * L.Diffuse;
        spec = specFactor * mat.Specular * L.Specular;
    }
}
//----------------------------------------------------------------------------
//! 点光源を計算
//----------------------------------------------------------------------------
void ComputePointLight(Material mat, PointLight L, float3 pos, float3 normal, float3 toEye,
	out float4 ambient, out float4 diffuse, out float4 spec)
{
	// 初期化
    ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
    diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
    spec = float4(0.0f, 0.0f, 0.0f, 0.0f);

	// 面から光源のベクトル
    float3 lightVec = L.Position - pos;

	// 面から光源の距離
    float d = length(lightVec);

	// 範囲
    if (d > L.Range)
        return;

	// ライトのベクトルをノーマル化
    lightVec /= d;

	// 環境光を計算
    ambient = mat.Ambient * L.Ambient;

	// 拡散光と鏡面反射光を計算する
    float diffuseFactor = dot(lightVec, normal);

	// 実際の分岐命令を回避するのに平坦化する必要があるということをコンパイラに示唆します。
	[flatten]
    if (diffuseFactor > 0.0f)
    {
        float3 v = reflect(-lightVec, normal);
        float specFactor = pow(max(dot(v, toEye), 0.0f), mat.Specular.w);

        diffuse = diffuseFactor * mat.Diffuse * L.Diffuse;
        spec = specFactor * mat.Specular * L.Specular;
    }

	// 光は段々衰弱します。（弱くなる）
    float att = 1.0f / dot(L.Att, float3(1.0f, d, d * d));

    diffuse *= att;
    spec *= att;
}

//----------------------------------------------------------------------------
//! スポットライトを計算
//----------------------------------------------------------------------------
void ComputeSpotLight(Material mat, SpotLight L, float3 pos, float3 normal, float3 toEye,
	out float4 ambient, out float4 diffuse, out float4 spec)
{
	// 初期化
    ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
    diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
    spec = float4(0.0f, 0.0f, 0.0f, 0.0f);

	// 光のベクトル
    float3 lightVec = L.Position - pos;

    // 光のベクトルの距離（長さ）
    float d = length(lightVec);

	// 範囲をテスト
    if (d > L.Range)
        return;

	// ノーマル
    lightVec /= d;

	// アンビエントライティングを計算
    ambient = mat.Ambient * L.Ambient;


    // 拡散光と鏡面反射光を計算します
    float diffuseFactor = dot(lightVec, normal);

	// 実際の分岐命令を回避するのに平坦化する必要があるということをコンパイラに示唆します。
	[flatten]
    if (diffuseFactor > 0.0f)
    {
        float3 v = reflect(-lightVec, normal);
        float specFactor = pow(max(dot(v, toEye), 0.0f), mat.Specular.w);

        diffuse = diffuseFactor * mat.Diffuse * L.Diffuse;
        spec = specFactor * mat.Specular * L.Specular;
    }

	// 光は段々衰弱します。（弱くなる）
    float spot = pow(max(dot(-lightVec, L.Direction), 0.0f), L.Spot);
    float att = spot / dot(L.Att, float3(1.0f, d, d * d));

    ambient *= spot;
    diffuse *= att;
    spec *= att;
}

Texture2D g_tex : register(t0);
TextureCube g_texCube : register(t1);
SamplerState g_sam : register(s0);

// 定数バッファ

cbuffer FullWorldCB : register(b0)
{
    matrix matWorld; //! ワールド行列
    matrix matWorldInv; //! 逆 ワールド行列
};

cbuffer CameraCB : register(b1)
{
    matrix matView; // ビュー行列
    matrix matProj; // 投影行列
    float3 eyePos;
};

cbuffer DrawingMatCB : register(b2)
{
    Material g_material;
    float3 g_eyePosW;
}

cbuffer DrawingStatesCB : register(b3)
{
    bool g_isReflection;
    bool g_isShadow;
    bool g_isReflectionMat;
    bool g_isTextureUsed;
}

cbuffer ChangesRarelyCB : register(b4)
{
    matrix g_reflection;
    matrix g_shadow;
    matrix g_refShadow;
    DirectionalLight g_DirLight[5];
    PointLight g_PointLight[5];
    SpotLight g_SpotLight[5];
}

cbuffer FogCB : register(b5)
{
    float4 g_fogColor;
    
    bool g_fogEnabled;
    float g_fogStart;
    float g_fogRange;
    float g_pad;
}

struct VertexPosNormalTex
{
    float3 PosL : POSITION;
    float3 NormalL : NORMAL;
    float2 Tex : TEXCOORD;
};

struct VertexPosTex
{
    float3 PosL : POSITION;
    float2 Tex : TEXCOORD;
};

struct VertexPosHWNormalTex
{
    float4 PosH : SV_POSITION;
    float3 PosW : POSITION;
    float3 NormalW : NORMAL;
    float2 Tex : TEXCOORD;
};

struct VertexPosHTex
{
    float4 PosH : SV_POSITION;
    float2 Tex : TEXCOORD;
};