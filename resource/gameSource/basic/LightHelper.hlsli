//----------------------------------------------------------------------------
//!	@file   LightHelper.hlsli
//!	@brief  ライティング用ヘルパーシェーダヘーダ
//! ＠ref   ライトの種類説明: https://docs.unity3d.com/ja/2018.4/Manual/Lighting.html
//----------------------------------------------------------------------------

//============================================================================
//! 構造体
//! @note 拡散反射 (Diffuse reflection)
//! @note 鏡面反射 (Specular reflection)
//! @note 環境反射 (Ambient reflection)
//============================================================================

//! 平行光源（ディレクショナルライト）
struct DirectionalLight
{
    float4 Ambient;
    float4 Diffuse;
    float4 Specular;
    float3 Direction;
    float Pad;
};

//！ 点光源（ポイントライト）
struct PointLight
{
    float4 Ambient;
    float4 Diffuse;
    float4 Specular;

    float3 Position;
    float Range;

    float3 Att;
    float Pad;
};

//！ スポットライト
struct SpotLight
{
    float4 Ambient;
    float4 Diffuse;
    float4 Specular;

    float3 Position;
    float Range;

    float3 Direction;
    float Spot;

    float3 Att;
    float Pad;
};

//! 物体表面の材質
struct Material
{
    float4 Ambient;
    float4 Diffuse;
    float4 Specular; // w = SpecPower
    float4 Reflect;
};
//----------------------------------------------------------------------------
//! 平行光源を計算
//----------------------------------------------------------------------------
void ComputeDirectionalLight(Material mat,
                             DirectionalLight L,
	                         float3 normal, float3 toEye,
	                         out float4 ambient,
	                         out float4 diffuse,
	                         out float4 spec)
{
	// 初期化
    ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
    diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
    spec = float4(0.0f, 0.0f, 0.0f, 0.0f);

	// 光のベクトルは照射方向と反対
    float3 lightVec = -L.Direction;

	// 環境光（アンビエント）を追加する
    ambient = mat.Ambient * L.Ambient;

	// 拡散光と鏡面反射光を追加します 
    float diffuseFactor = dot(lightVec, normal);

	// 実際の分岐命令を回避するのに平坦化する必要があるということをコンパイラに示唆します。
	[flatten]
    if (diffuseFactor > 0.0f)
    {
        float3 v = reflect(-lightVec, normal);
        float specFactor = pow(max(dot(v, toEye), 0.0f), mat.Specular.w);

        diffuse = diffuseFactor * mat.Diffuse * L.Diffuse;
        spec = specFactor * mat.Specular * L.Specular;
    }
}
//----------------------------------------------------------------------------
//! 点光源を計算
//----------------------------------------------------------------------------
void ComputePointLight(Material mat,
                       PointLight L,
                       float3 pos,
                       float3 normal,
                       float3 toEye,
	                   out float4 ambient,
                       out float4 diffuse,
                       out float4 spec)
{
	// 初期化
    ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
    diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
    spec = float4(0.0f, 0.0f, 0.0f, 0.0f);

	// 面から光源のベクトル
    float3 lightVec = L.Position - pos;

	// 面から光源の距離
    float d = length(lightVec);

	// 範囲
    if (d > L.Range)
        return;

	// ライトのベクトルをノーマル化
    lightVec /= d;

	// 環境光を計算
    ambient = mat.Ambient * L.Ambient;

	// 拡散光と鏡面反射光を計算する
    float diffuseFactor = dot(lightVec, normal);

	// 実際の分岐命令を回避するのに平坦化する必要があるということをコンパイラに示唆します。
	[flatten]
    if (diffuseFactor > 0.0f)
    {
        float3 v = reflect(-lightVec, normal);
        float specFactor = pow(max(dot(v, toEye), 0.0f), mat.Specular.w);

        diffuse = diffuseFactor * mat.Diffuse * L.Diffuse;
        spec = specFactor * mat.Specular * L.Specular;
    }

	// 光は段々衰弱します。（弱くなる）
    float att = 1.0f / dot(L.Att, float3(1.0f, d, d * d));

    diffuse *= att;
    spec *= att;
}

//----------------------------------------------------------------------------
//! スポットライトを計算
//----------------------------------------------------------------------------
void ComputeSpotLight(Material mat,
                      SpotLight L,
                      float3 pos,
                      float3 normal,
                      float3 toEye,
	                  out float4 ambient,
                      out float4 diffuse,
                      out float4 spec)
{
	// 初期化
    ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
    diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
    spec = float4(0.0f, 0.0f, 0.0f, 0.0f);

	// 物体表面からライトのベクトル
    float3 lightVec = L.Position - pos;

    // 表面からライト距離
    float d = length(lightVec);

	// 範囲内のチェック
    if (d > L.Range)
        return;

	// ノーマル
    lightVec /= d;

	// 環境光の計算
    ambient = mat.Ambient * L.Ambient;


    // 拡散反射と鏡面反射の計算
    float diffuseFactor = dot(lightVec, normal);

	// 実際の分岐命令を回避するのに平坦化する必要があるということをコンパイラに示唆します。
	[flatten]
    if (diffuseFactor > 0.0f)
    {
        float3 v = reflect(-lightVec, normal);
        float specFactor = pow(max(dot(v, toEye), 0.0f), mat.Specular.w);

        diffuse = diffuseFactor * mat.Diffuse * L.Diffuse;
        spec = specFactor * mat.Specular * L.Specular;
    }

	// スポットライトの計算
    float spot = pow(max(dot(-lightVec, L.Direction), 0.0f), L.Spot);
    float att = spot / dot(L.Att, float3(1.0f, d, d * d));

    ambient *= spot;
    diffuse *= att;
    spec *= att;
}
