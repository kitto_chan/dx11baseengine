//----------------------------------------------------------------------------
//!	@file	Basic_2D_PS.hlsl
//!	@brief	基本的の汎用2Dピクセルシェーダ
//----------------------------------------------------------------------------
#include "../Basic.hlsli"

cbuffer ColorCB : register(b11)
{
	float4 meshColor_ = float4(1.0f, 1.0f, 1.0f, 1.0f); // メッシュの色
};

cbuffer Sprite2DCB : register(b12)
{
	float4 fillColor_;
	float4 fillColor2_;
	float percent_;
	float percent2_;
}

float4 PS(VertexPosHTex pIn) : SV_Target
{
	float4 texColor = meshColor_;

	if (pIn.Tex.x < percent2_)
	{
		texColor = fillColor2_;
	}
	
	if (pIn.Tex.x < percent_)
	{
		texColor = fillColor_;
	}

	return g_tex.Sample(g_sam, pIn.Tex) * texColor;
}
