//----------------------------------------------------------------------------
//!	@file	Basic_2D_PS.hlsli
//!	@brief	基本的の汎用2D頂点シェーダ
//----------------------------------------------------------------------------

#include "../Basic.hlsli"

VertexPosHTex VS(VertexPosTex vIn)
{
    VertexPosHTex vOut;
	vOut.PosH = mul(matWorld, float4(vIn.PosL, 1.0f));
	//vOut.PosH = mul(matView, vOut.PosH);
	//vOut.PosH = mul(matProj, vOut.PosH);
    //vOut.PosH = float4(vIn.PosL, 1.0f);
    vOut.Tex = vIn.Tex;
    return vOut;
}
