//----------------------------------------------------------------------------
//!	@file	Basic_3D_PS.hlsl
//!	@brief	基本的の汎用3Dピクセルシェーダ
//----------------------------------------------------------------------------
#include "../Basic.hlsli"

// ピクセルシェーダ(3D)
float4 PS(VertexPosHWNormalTex pIn) : SV_Target
{
	 // デフォルトカラー：　白い
	float4 texColor = float4(1.0f, 1.0f, 1.0f, 1.0f);
	//if (g_isTextureUsed)
	{
		texColor = g_tex.Sample(g_sam, pIn.Tex);
        // 先に透明な色がクリップして、透明なのであとの計算しなくてもいい
		clip(texColor.a - 0.1f);
	}
    //return texColor;
    
    // ノーマル
	pIn.NormalW = normalize(pIn.NormalW);

    // 目のベクトル
	float3 toEyeW = normalize(g_eyePosW - pIn.PosW);
	float distToEye = distance(g_eyePosW, pIn.PosW);

    // 初期化
	float4 ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
	float4 diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
	float4 spec = float4(0.0f, 0.0f, 0.0f, 0.0f);
	float4 A = float4(0.0f, 0.0f, 0.0f, 0.0f);
	float4 D = float4(0.0f, 0.0f, 0.0f, 0.0f);
	float4 S = float4(0.0f, 0.0f, 0.0f, 0.0f);
	int i;

	//もし反射物件を描画するとき、 ディレクトライトの計算は必要だ
    [unroll]

	for (i = 0; i < 5; ++i)
	{
		DirectionalLight dirLight = g_DirLight[i];
		
		ComputeDirectionalLight(g_material, g_DirLight[i], pIn.NormalW, toEyeW, A, D, S);
		
		ambient += A;
		diffuse += D;
		spec += S;
	}
        
    // もし反射物件を描画するとき、ポイントライトの計算は必要だ
	PointLight pointLight;
    [unroll]
	for (i = 0; i < 5; ++i)
	{
		pointLight = g_PointLight[i];
		
		ComputePointLight(g_material, pointLight, pIn.PosW, pIn.NormalW, toEyeW, A, D, S);
		
		ambient += A;
		diffuse += D;
		spec += S;
	}
        
    
	// もし反射物件を描画するとき、スポットライトの計算は必要だ
	SpotLight spotLight;
    [unroll]
	for (i = 0; i < 5; ++i)
	{
		spotLight = g_SpotLight[i];

		ComputeSpotLight(g_material, spotLight, pIn.PosW, pIn.NormalW, toEyeW, A, D, S);
		
		ambient += A;
		diffuse += D;
		spec += S;
	}
	float4 litColor = texColor * (ambient + diffuse) + spec;
        
	//float4 litColor = texColor;
    // 霧エフェクトの処理
    [flatten]
	if (g_fogEnabled)
	{
        // 0 ~ 1 の範囲
		float fogLerp = saturate((distToEye - g_fogStart) / g_fogRange);
        // 物件の色と光の補間
		litColor = lerp(litColor, g_fogColor, fogLerp);
	}
    
	litColor.a = texColor.a * g_material.Diffuse.a;
	return litColor;
}
