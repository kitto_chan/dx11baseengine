//----------------------------------------------------------------------------
//!	@file	Blur_PS.fx
//!	@brief	ブラーエフェクト
//----------------------------------------------------------------------------
Texture2D    _Texture0 : register(t0); //!< 本来のテクスチャー
SamplerState _Sampler0 : register(s0);

cbuffer BlurCB : register(b0)
{
    float power;
};
cbuffer ColorCB : register(b1)
{
    float4 meshColor_ = float4(1, 1, 1, 1); // メッシュの色
};

struct VS_OUTPUT
{
    float4 _position : SV_POSITION;
    float4 _color    : COLOR;
    float2 _uv       : TEXCOORD0;
};

float4 PS(VS_OUTPUT input) : SV_Target
{
    //  ぼかすための周りのＵＶ座標を取得
    float4 outColor = 0;
    float2 dir = power / float2(1440, 810);
    
    // ガウシアンぼかしに基づいてブラーします
    // ref https://dev.theomader.com/gaussian-kernel-calculator/

    float alpha = _Texture0.Sample(_Sampler0, input._uv).a;
    // 3x3 ブラー
    outColor += _Texture0.Sample(_Sampler0, input._uv + float2(-1, -1) * dir) * 0.077847; // 左上
    outColor += _Texture0.Sample(_Sampler0, input._uv + float2( 0, -1) * dir) * 0.123317; // 上
    outColor += _Texture0.Sample(_Sampler0, input._uv + float2( 1, -1) * dir) * 0.077847; // 右上
    
    outColor += _Texture0.Sample(_Sampler0, input._uv + float2(-1, 0) * dir) * 0.123317; // 左中
    outColor += _Texture0.Sample(_Sampler0, input._uv + float2( 0, 0) * dir) * 0.195346; // 中　
    outColor += _Texture0.Sample(_Sampler0, input._uv + float2( 1, 0) * dir) * 0.123317; // 右中
    
    outColor += _Texture0.Sample(_Sampler0, input._uv + float2(-1, 1) * dir) * 0.077847; // 左上
    outColor += _Texture0.Sample(_Sampler0, input._uv + float2( 0, 1) * dir) * 0.123317; // 下
    outColor += _Texture0.Sample(_Sampler0, input._uv + float2( 1, 1) * dir) * 0.077847; // 右下
    
    outColor.a = alpha;
    
    outColor *= meshColor_;
    
    return outColor;
}