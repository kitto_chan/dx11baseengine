//----------------------------------------------------------------------------
//!	@file	Mosaic_PS.fx
//!	@brief	モザイクエフェクト
//----------------------------------------------------------------------------
Texture2D _Texture0 : register(t0); //!< 本来のテクスチャー
SamplerState _Sampler0 : register(s0);

cbuffer BlurCB : register(b0)
{
    float power;
};

struct VS_OUTPUT
{
    float4 _position : SV_POSITION;
    //float4 _color : COLOR;
    float2 _uv : TEXCOORD0;
};

float4 PS(VS_OUTPUT input) : SV_Target
{
    float4 outColor;
    
    float u = round(input._uv.x / (power / 1440)) * (power / 1440);
    float v = round(input._uv.y / (power / 810)) * (power / 810);
    
    outColor = _Texture0.Sample(_Sampler0, float2(u, v));
    
    return outColor ;
}