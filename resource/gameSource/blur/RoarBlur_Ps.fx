//----------------------------------------------------------------------------
//!	@file	Blur_PS.fx
//!	@brief	ブラーエフェクト
//----------------------------------------------------------------------------
Texture2D    _Texture0 : register(t0); //!< 本来のテクスチャー
SamplerState _Sampler0 : register(s0);

cbuffer BlurCB : register(b0)
{
    float power;
};

struct VS_OUTPUT
{
    float4 _position : SV_POSITION;
    float2 _uv : TEXCOORD0;
};

float4 PS(VS_OUTPUT input) : SV_Target
{
    float4 Color[10];
   
   //ブラーの中心位置 ← 現在のテクセル位置
    float2 dir = float2(0.5,0.5) - input._uv;
   
   //距離を計算する
    float len = length(dir);

   // 方向ベクトルの正規化し、１テクセル分の長さとなる方向ベクトルを計算する
   // TODO: screen size Pass by cbuffer
   dir = normalize(dir) * float2(1.0 / 1440.0, 1.0 / 810.0);
   
   //m_BlurPower でボケ具合を調整する
   //距離を積算することにより、爆発の中心位置に近いほどブラーの影響が小さくなるようにする
   dir *= power * len;

   //合成する
    Color[0] = _Texture0.Sample(_Sampler0, input._uv) * 0.19f;
    Color[1] = _Texture0.Sample(_Sampler0, input._uv + dir) * 0.17f;
    Color[2] = _Texture0.Sample(_Sampler0, input._uv + dir * 2.0f) * 0.15f;
    Color[3] = _Texture0.Sample(_Sampler0, input._uv + dir * 3.0f) * 0.13f;
    Color[4] = _Texture0.Sample(_Sampler0, input._uv + dir * 4.0f) * 0.11f;
    Color[5] = _Texture0.Sample(_Sampler0, input._uv + dir * 5.0f) * 0.09f;
    Color[6] = _Texture0.Sample(_Sampler0, input._uv + dir * 6.0f) * 0.07f;
    Color[7] = _Texture0.Sample(_Sampler0, input._uv + dir * 7.0f) * 0.05f;
    Color[8] = _Texture0.Sample(_Sampler0, input._uv + dir * 8.0f) * 0.03f;
    Color[9] = _Texture0.Sample(_Sampler0, input._uv + dir * 9.0f) * 0.01f;
   
    return Color[0] + Color[1] + Color[2] + Color[3] + Color[4] + Color[5] + Color[6] + Color[7] + Color[8] + Color[9];
}