//----------------------------------------------------------------------------
//!	@file	ps_texture.fx
//!	@brief	テクスチャありピクセルシェーダー
//----------------------------------------------------------------------------

Texture2D Texture : register(t0);
Texture2D Texture_Noise : register(t1);
Texture2D Texture_Mask : register(t2);

SamplerState SampleType : register(s0);
SamplerState SampleType2 : register(s1);

cbuffer ColorCB : register(b1)
{
    float4 meshColor_ = float4(1, 0, 0, 1); // メッシュの色
};

cbuffer FireCB : register(b6)
{
    float time;
    float speed1;
    float speed2;
    float speed3;
};

// 頂点シェーダー出力
struct VS_OUTPUT
{
    float4 position_ : SV_Position;
    float4 color_ : COLOR;
    float2 uv_ : TEXCOORD0;
};

// ピクセルシェーダー出力
struct PS_OUTPUT
{
    float4 color0_ : SV_Target0;
};

//----------------------------------------------------------------------------
// ピクセルシェーダー
//----------------------------------------------------------------------------
float4 PS(VS_OUTPUT input) : SV_Target0
{
    float4 noise1;  //ノイズ1色
    float4 noise2; //ノイズ1色
    float4 noise3; //ノイズ1色
    float4 finalNoise; //最終色 (noise 1 + 2 + 3 )
    
    float perturb; // 火の摂動
    float2 noiseCoords; // noiseUv
    
    float4 fireColor;  // 火の色
    float4 alphaColor; // マスク
    
    // 一枚のノイズテクスチャーを使って、異なるUV座標をずらしてサンプルします。
    // バレないように３種類ノイズがテクスチャーを得ることができます
    float2 texCoords1 = input.uv_;
    texCoords1.y += time * 1.3f; // (TODO: cb speed1)
    float2 texCoords2 = input.uv_;
    texCoords2.y += time * 2.1f; // (TODO: cb speed2)
    float2 texCoords3 = input.uv_;
    texCoords3.y += time * 2.3f; // (TODO: cb speed3)
    noise1 = Texture_Noise.Sample(SampleType, texCoords1);
    noise2 = Texture_Noise.Sample(SampleType, texCoords2);
    noise3 = Texture_Noise.Sample(SampleType, texCoords3);
    
    //　画像座標が(0, 1) から (-1, +1)に変換
    noise1 = (noise1 - 0.5f) * 2.0f;
    noise2 = (noise2 - 0.5f) * 2.0f;
    noise3 = (noise3 - 0.5f) * 2.0f;
    
    // ３つのノイズ画像がx,y値でゆがませる
    float2 distortion1 = float2(0.1f, 0.2f); //TODO: Cbで制御可能
    float2 distortion2 = float2(0.1f, 0.3f); //TODO: Cbで制御可能
    float2 distortion3 = float2(0.1f, 0.1f); //TODO: Cbで制御可能
    noise1.xy = noise1.xy * distortion1.xy;
    noise2.xy = noise2.xy * distortion2.xy;
    noise3.xy = noise3.xy * distortion3.xy;
    
    // 火の動きのサークルがバレないのため
    // 3つの歪んだノイズの結果を一枚ノイズに合成する
    finalNoise = noise1 + noise2 + noise3;
    
    // 最終的なノイズの結果に摂動を与えて、全体的なノイズテクスチャに炎のような外観を作り出します
	// れにより、上部で炎の揺らぎが生じ、下方に向かうにつれて、より強固な炎のベースが形成されます。
    float distortionScale = 0.8f; //!< 歪みスケール //TODO: Cbで制御可能
    float distortionBias = 0.5f; //!< 歪みバイアス(摂動) //TODO: Cbで制御可能
    perturb = ((1.0f - input.uv_.y) * distortionScale) + distortionBias;
    
    // ここで、火の色のテクスチャをサンプリングするために使用される、摂動と歪んだテクスチャのサンプリング座標を作成します。
    noiseCoords.xy = (finalNoise.xy * perturb) + input.uv_.xy;
    
    // 乱れた歪んだテクスチャのサンプリング座標を使用して、火のテクスチャから色をサンプリングします。
	// 炎が回り込むのを防ぐために、wrap sampleの代わりに clamp を使用します。
    fireColor = Texture.Sample(SampleType2, noiseCoords.xy);
    alphaColor = Texture_Mask.Sample(SampleType2, noiseCoords.xy);
    fireColor.a = alphaColor.r;

    return fireColor;
}
