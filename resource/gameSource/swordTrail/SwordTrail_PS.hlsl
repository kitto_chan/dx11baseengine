//----------------------------------------------------------------------------
//!	@file	SwordTrail.hlsl
//!	@brief  剣の軌跡描画用ピクセルシェーダ
//----------------------------------------------------------------------------
Texture2D SwordTex : register(t0);
SamplerState SwordSampler : register(s0);

struct VS_OUTPUT
{
	float4 _posH : SV_POSITION;
	float2 _uv : TEXCOORD;
};

float4 PS(VS_OUTPUT input) : SV_Target
{
	float4 color = SwordTex.Sample(SwordSampler, input._uv);
	color.a = color.r;
	return color;
}
