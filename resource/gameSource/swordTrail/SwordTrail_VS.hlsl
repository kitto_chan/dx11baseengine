//----------------------------------------------------------------------------
//!	@file	SwordTrail.hlsli
//!	@brief	剣の軌跡用のシェーダヘッダー
//----------------------------------------------------------------------------
#include "../Camera_H.hlsli"

// 定数バッファ
cbuffer WorldCB : register(b0)
{
	matrix matWorld; // ワールド行列
};

struct VS_INPUT
{
	float3 _posL : POSITION;
	float2 _uv : TEXCOORD;
};

struct VS_OUTPUT
{
	float4 _posH : SV_POSITION;
	float2 _uv : TEXCOORD;
};

VS_OUTPUT VS(VS_INPUT input){
	VS_OUTPUT vOut = (VS_OUTPUT) 0;
	
	vOut._posH = float4(input._posL, 1);
	vOut._posH = mul(matWorld, vOut._posH);
	vOut._posH = mul(matView, vOut._posH);
	vOut._posH = mul(matProj, vOut._posH);
	
	vOut._uv = input._uv;
	return vOut;
}
