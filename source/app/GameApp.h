﻿//---------------------------------------------------------------------------
//!	@file	GameApp.h
//!	@brief	ゲームアプリケーションメイン
//---------------------------------------------------------------------------
#pragma once
#include "D3DApp.h"
class GameApp : public D3DApp
{
public:
    GameApp(HINSTANCE hInstance);   //!< コンストラクタ
    ~GameApp();                     //!< デストラクタ

    bool Init() override;       //!< 初期化
    void Update() override;     //!< 更新
    void Render() override;     //!< 描画
    void Finalize() override;   //!< 解放
};
