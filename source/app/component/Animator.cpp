﻿//---------------------------------------------------------------------------
//! @file	Animator.h
//!	@brief	アニメーターはモデルのアニメション処理コンポネント
//---------------------------------------------------------------------------
#include "Animator.h"

#include <filesystem>
#include <fstream>

#include "component/Transform.h"
#include "component/Animator.h"
#include "component/ModelRenderer.h"
#include "Entity/Entity.h"
namespace component {
namespace {
//!< TODO Assert Manager　現在playerしか使ってるので、先にこうします
//std::unordered_map<std::string, std::shared_ptr<Animation>> animationList;
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
Animator::Animator()
{
    _name = NAME;
}
Animator::Animator(std::string_view animPath)
{
    _name = NAME;
    ReadAnimation(animPath);
}
Animator::Animator(const Animation::Desc* desc, u64 count, s32 defaultId, s32 rootBoneId)
{
    _name = NAME;
    CreateAnimation(desc, count, defaultId, rootBoneId);
}
//---------------------------------------------------------------------------
//! デスストラクタ
//---------------------------------------------------------------------------
Animator::~Animator()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool Animator::OnInit()
{
    //==========================================================
    // 必要なコンポーネントを取得
    //==========================================================
    _pTransform = _pOwner->GetComponent<Transform>();
    ASSERT_MESSAGE(_pTransform, "AnimatorはTransformは必要です");

    _pModelRenderer = _pOwner->GetComponent<ModelRenderer>();
    ASSERT_MESSAGE(_pTransform, "AnimatorはModelRendererは必要です");
    //-------------------------------------------------------------
    // アニメションのチェック
    //-------------------------------------------------------------
    ASSERT_MESSAGE(_pAnimation, "先にアニメションを作ってください");

    _pOwner->GetComponent<ModelRenderer>()->SetAnimation();

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void Animator::OnUpdate()
{
    if(_isPlay) {
        _pAnimation->update(timer::DeltaTime() * _animationSpeed);
    }

    if(_animationSpeedReturnFrame > 0.0f) {
        _animationSpeedReturnFrame -= timer::DeltaTime();
        if(_animationSpeedReturnFrame <= 0.0f) {
            _animationSpeed = DEFAULT_ANIM_SPEED;
        }
    }
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void Animator::OnRender()
{
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void Animator::OnRenderImgui()
{
    imgui::ImguiDragFloat("Speed", _animationSpeed);

    imgui::ImguiColumn2FormatBegin("IsPlay");
    ImGui::Checkbox("##IsPlay", &_isPlay);
    imgui::ImguiColumn2FormatEnd();

    // 現在の再生時間を設定
    f32 length      = _pAnimation->GetCurrentLayerAnimationLength();
    f32 currentTime = _pAnimation->GetCurretLayerAnimationTime();
    imgui::ImguiSliderFloat("Animation", currentTime, 0.0f, length, "time = %.3f");
    _pAnimation->SetAnimationTime(currentTime);
    _pOwner->GetComponent<ModelRenderer>()->OnUpdate();
}
//---------------------------------------------------------------------------
//！ アニメションを生成
//---------------------------------------------------------------------------
void Animator::CreateAnimation(const Animation::Desc* desc, u64 count, s32 defaultId, s32 rootBoneId)
{
    ASSERT_MESSAGE(defaultId < count, "Default Id > Size");
    if(_pAnimation)
        return;

    _pAnimation = createAnimation(desc, count, rootBoneId);

    // デフォルト アニメーションを再生
    Play(desc[defaultId].name_, Animation::PlayType::Loop);
}
//---------------------------------------------------------------------------
//! アニメションを読み込む
//! @param path .animのファイルパス
//---------------------------------------------------------------------------
bool Animator::ReadAnimation(std::string_view path)
{
    _pAnimation = createAnimation(path);
    if(_pAnimation) {
        return true;
    }
    else {
        ASSERT_MESSAGE(false, "読み取りデータを失敗した");
        return false;
    }
}
//---------------------------------------------------------------------------
//！ アニメションを再生
//---------------------------------------------------------------------------
void Animator::Play(std::string_view name, Animation::PlayType playType, f32 speed)
{
    if(!std::strcmp(_currentAnimationName.c_str(), name.data())) return;

    _pAnimation->play(name.data(), playType, speed);
    _currentAnimationName = name;
    _isPlay               = true;
}
//---------------------------------------------------------------------------
//！ 現在のアニメションの名前を取得
//---------------------------------------------------------------------------
std::string_view Animator::GetAnimationName() const
{
    return _currentAnimationName;
}
//---------------------------------------------------------------------------
//！ 現在のアニメションが終了かどうかのチェック
//---------------------------------------------------------------------------
bool Animator::IsLastFrame(f32 blendTime) const
{
    return _pAnimation->IsLastFrame(blendTime);
}
//---------------------------------------------------------------------------
//！ 現在のアニメションが終了かどうかのチェック
//---------------------------------------------------------------------------
bool Animator::IsAnimationName(std::string_view name) const
{
    return _currentAnimationName.compare(name) == 0;
}

//---------------------------------------------------------------------------
//! 現在のアニメションは終了かどうか
//---------------------------------------------------------------------------
bool Animator::IsAnimationLastFrame(std::string_view name, f32 frame) const
{
    return IsLastFrame(frame) && IsAnimationName(name);
}
//---------------------------------------------------------------------------
//! アニメションを停止する(指定フレーム)
//! param	frame 指定のフレームで停止する
//---------------------------------------------------------------------------
void Animator::PauseAnimation(f32 frame)
{
    SetAnimationTime(frame);
    PauseAnimation();
}
//---------------------------------------------------------------------------
//! アニメションを停止する
//---------------------------------------------------------------------------
void Animator::PauseAnimation()
{
    _isPlay = false;
}

void Animator::SetPlayType(Animation::PlayType playType)
{
    _pAnimation->SetPlayType(playType);
}

void Animator::SetAnimationSpeed(f32 animationSpeed, f32 animationSpeedReturnFrame)
{
    _animationSpeed            = animationSpeed;
    _animationSpeedReturnFrame = animationSpeedReturnFrame;
}

}   // namespace component
