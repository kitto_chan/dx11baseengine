﻿//---------------------------------------------------------------------------
//! @file	Animator.h
//!	@brief	アニメーターはモデルのアニメション処理コンポネント
//---------------------------------------------------------------------------
#pragma once
#include "Component.h"
#include "renderer/animation.h"

namespace component {

class Transform;
class ModelRenderer;

class Animator : public Component
{
    static constexpr char* NAME               = "Animator";
    static constexpr f32   DEFAULT_ANIM_SPEED = 1.2f;

public:
    Animator();                                                                                  //!< コンストラクタ
    Animator(std::string_view animPath);                                                         //!< コンストラクタ
    Animator(const Animation::Desc* desc, u64 count, s32 defaultId = 0, s32 _rootBoneID = -1);   //!< コンストラクタ
    ~Animator();                                                                                 //!< デストラクタ

private:
    //---------------------------------------------------------------------------
    //! 継承関数
    //---------------------------------------------------------------------------
    bool OnInit() override;          //!< 初期化
    void OnUpdate() override;        //!< 更新
    void OnRender() override;        //!< 描画
    void OnRenderImgui() override;   //!< ImGui描画
public:
    //---------------------------------------------------------------------------
    //! public 関数
    //---------------------------------------------------------------------------

    //！ アニメションを生成
    void CreateAnimation(const Animation::Desc* desc, u64 count, s32 defaultId = 0, s32 _rootBoneID = -1);
    //! アニメションを読み込む
    //! @param path .animのファイルパス
    bool ReadAnimation(std::string_view path);

    //！ アニメションを再生
    void Play(std::string_view name, Animation::PlayType playType, f32 speed = 1.0f);

    ////! アニメションクラスを取得
    //shr_ptr<Animation> GetAnimation() const;

    //! 現在のアニメションの名前を取得
    std::string_view GetAnimationName() const;

    //! 現在のアニメションが終了かどうかのチェック
    bool IsLastFrame(f32 blendTime = 0.0f) const;

    //! 現在のアニメションは渡したの名前かどうか
    bool IsAnimationName(std::string_view name) const;

    //! 現在のアニメションは終了かどうか
    bool IsAnimationLastFrame(std::string_view name, f32 frame = 0.0f) const;

    //！ アニメションクラスを取得
    shr_ptr<Animation> GetAnimation() const { return _pAnimation; }
    //! 現在のアニメションのフレームを取得
    f32 GetCurrentAnimationTime() const { return _pAnimation->GetCurretLayerAnimationTime(); };
    //! 現在のアニメションのフレームの長さを取得
    f32 GetCurrentLayerAnimationLength() const { return _pAnimation->GetCurrentLayerAnimationLength(); }

    //! 現在のアニメションのフレームを設定
    void SetAnimationTime(f32 time) { _pAnimation->SetAnimationTime(time); };

    //!< アニメションの再生をつつく
    void ContinueAnimation() { _isPlay = true; }

    //!< アニメションを停止する(指定フレーム)
    void PauseAnimation(f32 frame);
    //!< アニメションを停止する
    void PauseAnimation();

    //!< アニメションの再生プレイタイプを変更
    void SetPlayType(Animation::PlayType playType);

    void SetAnimationSpeed(f32 animationSpeed, f32 animationSpeedReturnFrame);

private:
    //---------------------------------------------------------------------------
    //! private関数
    //---------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    //! private 変数
    //---------------------------------------------------------------------------
    raw_ptr<component::Transform>     _pTransform;       //!< Transform コンポーネント 参照
    raw_ptr<component::ModelRenderer> _pModelRenderer;   //!< ModelRendererコンポーネント 参照

    f32 _animationSpeed            = DEFAULT_ANIM_SPEED;   //!< アニメションの速さ
    f32 _animationSpeedReturnFrame = 0.0f;                    //!<  0: _animationSpeed を DEFAULT_ANIM_SPEEDに戻す

    std::string _currentAnimationName = "";   //!< 現在使ってるアニメション

    std::shared_ptr<Animation> _pAnimation;   //!< メインアニメーションのロジック

    bool _isPlay = true;   //!< アニメションを再生するか
};
}   // namespace component
