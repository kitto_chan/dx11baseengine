﻿//---------------------------------------------------------------------------
//!	@file	Conponent.h
//!	@brief	コンポーネント
//! @note	実行順番
//!			Init()		  -> OnInit()		 ->
//!			Update()	  -> OnUpdate()		 -> OnLateUpdate() ->
//!			Render()      -> OnRender()		 ->
//!			RenderImGui() -> OnRenderImGui() ->
//!         Finalize()    -> OnFinalize()
//!
//! @note	LateUpdate() は EntityのUpdate 関数が呼び出された後に実行されます。
//!			これはスクリプトの実行順を決める時に便利です。
//!			例えばカメラを追従するには Update で移動された結果を基に常に
//!			LateUpdate で位置を更新する必要があります。
//---------------------------------------------------------------------------
#pragma once
namespace entity {
class Entity;
}

namespace component {

class Component
{
    friend class entity::Entity;

public:
    Component();
    // コピー禁止
    //Component(const Component&) = delete;
    // 代入禁止
    Component& operator=(const Component&) = delete;
    virtual ~Component()                   = default;

private:
    //---------------------------------------------------------------------------
    //! メイン関数
    //---------------------------------------------------------------------------
    bool Init();          //!< 初期化
    void Update();        //!< 更新
    void LateUpdate();    //!< 後半の更新
    void Render();        //!< 描画
    void RenderImgui();   //!< ImGui描画
    void Finalize();      //!< 解放

public:
    std::string GetName() const;   //!< コンポネントの名前を取得

    //! ゲッター / セッター
    entity::Entity* GetOwner() const;                   //!< オーナーを取得
    void            SetOwner(entity::Entity* _owner);   //!< オーナーを設定

    //@{ enable

    //---------------------------------------------------------------------------
    //! Enableのsetter/getter
    //---------------------------------------------------------------------------
    bool IsEnable() const { return _isEnable; }          //!<　有効かどうか
    void SetEnable(bool value) { _isEnable = value; };   //!<　Enableフラッグ設定
    void Enable() { SetEnable(true); }                   //!<　有効する
    void Disable() { SetEnable(false); };                //!<　無効する

    //!< 用意できたか？　出来たら更新と描画実行する
    bool IsReady() { return _isEnable && _isInited; };
    //@}

protected:
    //---------------------------------------------------------------------------
    //! protected 変数
    //---------------------------------------------------------------------------
    // 継承用
    virtual bool OnInit() { return true; };   //!< 初期化
    virtual void OnUpdate(){};                //!< 更新
    virtual void OnLateUpdate() {}            //!< 後半の更新
    virtual void OnRender(){};                //!< 描画
    virtual void OnRenderImgui(){};           //!< ImGui描画
    virtual void OnFinalize(){};              //!< 解放

    bool _isEnable  = true;    //!< 有効したら(Update/Render)の関数を実行
    bool _isInited  = false;   //!< 初期化した
    bool _isRelease = false;   //!< エンティティマネジャーから外す
    bool _isRemoved = false;   //!< 完全に削除

    std::string _name;   //!< コンポネントの名前（表示用）

    entity::Entity* _pOwner = nullptr;   //!< オーナー
};

}   // namespace component
