﻿//---------------------------------------------------------------------------
//! @file	ModelRenderer.cpp
//!	@brief	モデルレンダー用のコンポネント
//---------------------------------------------------------------------------
#include "ModelRenderer.h"

#include <filesystem>
#include <fstream>

#include "component/Transform.h"
#include "component/Animator.h"
#include "Entity/Entity.h"
#include "effect/Effect.h"
namespace component {
namespace {
//! モデル管理(仮)
//! TODO:AssertManager Class
std::map<std::string, shr_ptr<Model>> ModelMapping;
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
ModelRenderer::ModelRenderer()
{
    _name = NAME;
}
//---------------------------------------------------------------------------
//! コンストラクタ
//! @params	[in]	modelPath モデルのパス
//! @params [in]	scale モデルのスゲール
//---------------------------------------------------------------------------
ModelRenderer::ModelRenderer(std::string_view modelPath, f32 scale)
{
    _modelPath  = modelPath;
    _modelScale = scale;
    _name       = NAME;

    //----------
    // もしパスは.animファイルなら
    if(string::IsPathExtension(modelPath, L".anim")) {
        ReadModel(_modelPath);
    };
}
//---------------------------------------------------------------------------
//! デスストラクタ
//---------------------------------------------------------------------------
ModelRenderer::~ModelRenderer()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool ModelRenderer::OnInit()
{
    // モデルを読み込む
    CreateModel(_modelPath, _modelScale);

    // 外部からのdiffuseテクスチャーを設定
    if(!_externalDiffuseTexture.empty())
        _model->loadExternalDiffuseTexture(_externalDiffuseTexture);

    // PBRテクスチャーを設定
    for(int i = 0; i < _pbrTexturePaths.size(); i++) {
        if(_pbrTexturePaths[i].empty()) continue;

        _model->SetPBRTexture(Model::PBRTextureType(i), _pbrTexturePaths[i]);
        _model->SetIsPBRModel(true);
    }

	_model->SetPhongMaterial(_phongMaterial);
    //==========================================================
    // 必要なコンポーネントを取得
    //==========================================================
    _pTransform = _pOwner->GetComponent<Transform>();
    ASSERT_MESSAGE(_pTransform, "Transformは必要です");

    //-------------------------------------------------------------
    // モデルのチェック
    //-------------------------------------------------------------
    ASSERT_MESSAGE(_model, "モデル読み込んでないです");

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void ModelRenderer::OnUpdate()
{
    _model->update();
}
//---------------------------------------------------------------------------
//!< 後半の更新
//---------------------------------------------------------------------------
void ModelRenderer::OnLateUpdate()
{
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void ModelRenderer::OnRender()
{
    _drawMatrix = _pTransform->GetLocalToWorldMatrix();
    // モデルの骨とバインドするの計算
    if(_isBindBone) {
        _drawMatrix = mul(_boneMatrix, _drawMatrix);
    }

    // モデルの行列を設定
    _model->setWorldMatrix(_drawMatrix);

    _model->render();

    if(_renderDebug) {
        _model->renderDebug();
    }
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void ModelRenderer::OnRenderImgui()
{
    imgui::ImguiCheckBox("RenderDebug", _renderDebug);

    ImGui::ColorEdit4("PhongAmbient", (float*)&_phongMaterial._ambient, ImGuiColorEditFlags_NoInputs);
    ImGui::ColorEdit4("PhongDiffuse", (float*)&_phongMaterial._diffuse, ImGuiColorEditFlags_NoInputs);
    ImGui::ColorEdit3("PhongSpecular", (float*)&_phongMaterial._specular, ImGuiColorEditFlags_NoInputs);
    imgui::ImguiDragFloat("SpecularShininess", _phongMaterial._specularShininess);
    SetPhongMaterial(_phongMaterial);
}
//---------------------------------------------------------------------------
//!< モデルを読み込む
//---------------------------------------------------------------------------
void ModelRenderer::CreateModel(std::string_view path, f32 scale)
{
    // バッグあり　同じモデル共用してるので、アニメションがお互いに影響された
    /*  auto itr = ModelMapping.find(path.data());
    if(itr != ModelMapping.end()) {
        _model = itr->second;
        return;
    }*/
    _modelPath = path;

    _model = createModel(_modelPath.c_str(), scale);
    ASSERT_MESSAGE(_model, "モデルを読み込めない");

    //ModelMapping.insert(std::pair(path, _model));
}
//---------------------------------------------------------------------------
//!< ボーンの行列をセット
//---------------------------------------------------------------------------
void ModelRenderer::SetBoneMatrix(const matrix& matrix)
{
    _isBindBone = true;
    _boneMatrix = matrix;
}
//---------------------------------------------------------------------------
//! 外部（別の）DiffuseTexture設定する、fbx Fileのテクスチャ載ってない(かも)
//---------------------------------------------------------------------------
void ModelRenderer::LoadExternalDiffuseTexture(std::string_view path)
{
    _externalDiffuseTexture = path;
    if(_model) {
        _model->loadExternalDiffuseTexture(path);
    }
}
//---------------------------------------------------------------------------
//! PBRテクスチャーを設定
//---------------------------------------------------------------------------
void ModelRenderer::SetPBRTexture(Model::PBRTextureType type, std::string_view path)
{
    _pbrTexturePaths[type] = path;
}
//---------------------------------------------------------------------------
//!< モデルを取得
//---------------------------------------------------------------------------
shr_ptr<Model> ModelRenderer::GetModel() const
{
    return _model;
}
//---------------------------------------------------------------------------
//! 特定のボーンの行列を取得
//---------------------------------------------------------------------------
matrix ModelRenderer::GetBoneMatrix(s32 index) const
{
    return _model->GetBoneMatrix(index);
}
//---------------------------------------------------------------------------
//!< アニメションを設定
//---------------------------------------------------------------------------
void ModelRenderer::SetAnimation()
{
    _pAnimator = _pOwner->GetComponent<Animator>();
    if(_pAnimator) {
        _isAnimated = true;
        _model->bindAnimation(_pAnimator->GetAnimation());
    }
}
void ModelRenderer::SetPhongMaterial(cb::PhongMaterial phongMaterial)
{
    _phongMaterial = phongMaterial;
    if(_model) {
        _model->SetPhongMaterial(phongMaterial);
    }
}
//---------------------------------------------------------------------------
//! モデルを読み込む
//! @param path .animのファイルパス
//---------------------------------------------------------------------------
bool ModelRenderer::ReadModel(std::string_view path)
{
    if(!string::IsPathExtension(path, L".anim")) {
        ASSERT_MESSAGE(false, ".animファイルではない、対応できない");
        return false;
    }

    nlohmann::json jModel;
    //---------
    // ファイルを読み込む
    std::ifstream ifs(path);
    ifs >> jModel;
    if(ifs.is_open())
        ifs.close();

    //---------
    // Jsonを読み込む
    return ReadModelJson(jModel["Model"]);
}
//---------------------------------------------------------------------------
//! モデルを読み込む
//! @param jAnim jsonのモデルデータ
//---------------------------------------------------------------------------
bool ModelRenderer::ReadModelJson(const nlohmann::json& jModel)
{
    //----------
    // モデル
    ASSERT_MESSAGE(jModel.contains("Path"), "パスがない");
    _modelPath = jModel["Path"];

    //----------
    // モデルテクスチャー
    std::string diffuseTexturePath = jModel["ExternalTexture"]["Diffuse"];
    if(!diffuseTexturePath.empty()) {
        LoadExternalDiffuseTexture(diffuseTexturePath);
    }

    return true;
}
}   // namespace component
