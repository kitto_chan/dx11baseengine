﻿//---------------------------------------------------------------------------
//! @file	ModelRenderer.h
//!	@brief	モデルレンダー用のコンポネント
//---------------------------------------------------------------------------
#pragma once
#include "Component.h"
#include "renderer/Model.h"
namespace component {

class Transform;
class Animator;

class ModelRenderer : public Component
{
    static constexpr char NAME[] = "ModelRenderer";
    friend class Animator;

public:
    ModelRenderer();                                               //!< コンストラクタ
    ModelRenderer(std::string_view modelPath, f32 scale = 1.0f);   //!< コンストラクタ
    ~ModelRenderer();                                              //!< デストラクタ

private:
    //---------------------------------------------------------------------------
    // 継承関数
    //---------------------------------------------------------------------------
    bool OnInit() override;          //!< 初期化
    void OnUpdate() override;        //!< 更新
    void OnLateUpdate() override;    //!< 後半の更新
    void OnRender() override;        //!< 描画
    void OnRenderImgui() override;   //!< ImGui描画
public:
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    void CreateModel(std::string_view path, f32 scale = 1.0f);   //!< モデルを作成

    //! モデルを読み込む
    //! @param path .animのファイルパス
    bool ReadModel(std::string_view path);
    //! モデルを読み込む
    //! @param jAnim jsonのモデルデータ
    bool ReadModelJson(const nlohmann::json& jAnim);

    void SetBoneMatrix(const matrix& matrix);   //!< ボーンの行列をセット

    //! 外部（別の）DiffuseTexture設定する、fbx Fileのテクスチャ載ってない(かも)
    void LoadExternalDiffuseTexture(std::string_view path);

    // PBRテクスチャーを設定
    void SetPBRTexture(Model::PBRTextureType type, std::string_view path);

    //---------
    // ゲッター
    shr_ptr<Model> GetModel() const;                                //!< Modelを取得
    matrix         GetBoneMatrix(s32 index) const;                  //!< 特定のボーンの行列を取得
    matrix         GetDrawMatrix() const { return _drawMatrix; };   //!< 描画行列を取得

    //!< モデルの修正を行う行列を設定
    void SetDefaultMatrix(const matrix& defaultmatrix) { _defaultMatrix = defaultmatrix; };

    void SetAnimation();   //!< アニメションを設定

    //! 特定の頂点インデックスを取得
    matrix GetVertexByIndex(s32 index) { return _model->GetGeometryVertexMatrix(index); };

    void SetPhongMaterial(cb::PhongMaterial phongMaterial);   //!< Phongモデルのマテリアル設定

    //! アウトラインを描画かどうか
    void SetIsRenderOutline(bool isRenderOutline) { _model->SetIsRenderOutline(isRenderOutline); };

private:
    //---------------------------------------------------------------------------
    // private関数
    //---------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    std::string _modelPath;                //!< モデルのパス
    std::string _externalDiffuseTexture;   //!< 外部から読み込むのテクスチャ
    f32         _modelScale = 1.0f;        //!<	モデルのスケール

    shr_ptr<Model> _model;   //!< モデル

    raw_ptr<component::Transform> _pTransform;   //!< Transform コンポーネント参照
    raw_ptr<component::Animator>  _pAnimator;    //!< Animator  コンポーネント参照

    bool _isAnimated = false;   //!< このモデルアニメションあるかどうか

    bool   _isBindBone = false;   //!< もしこのモデルがボーンにバインドしたら、その行列を計算する
    matrix _boneMatrix;           //!< 従ってるボーンの行列

    matrix _drawMatrix    = math::identity();   //!< 描画用の行列
    matrix _defaultMatrix = math::identity();   //!< モデルの修正を行う行列

    bool _renderDebug = false;   //!< モデルデバッグ描画するかどうか

    cb::PhongMaterial _phongMaterial;   //!< phongモデルのマテリアル

    //! PBRテクスチャーのパスを保存
    std::array<std::string, Model::PBRTextureType::COUNT> _pbrTexturePaths;
};
}   // namespace component
