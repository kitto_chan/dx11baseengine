﻿//---------------------------------------------------------------------------
//!	@file	Transform.h
//!	@brief	オブジェクトの位置、回転、スケールを扱うクラス
//---------------------------------------------------------------------------
#include "Transform.h"
namespace component {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
Transform::Transform(const float3& position, const float3& scale, const float3& rotation)
{
    _name = _NAME;

    _position = position;
    _scale    = scale;
    _rotation = rotation;
}
//---------------------------------------------------------------------------
//! コピーコンストラクタ
//---------------------------------------------------------------------------
Transform::Transform(const Transform& other)
{
    _position = other._position;
    _rotation = other._rotation;
    _scale    = other._scale;
}
//---------------------------------------------------------------------------
//! 代入
//---------------------------------------------------------------------------
Transform& Transform::operator=(const Transform& other)
{
    _position = other._position;
    _rotation = other._rotation;
    _scale    = other._scale;

    return *this;
}

//---------------------------------------------------------------------------
//! ImGui描画
//---------------------------------------------------------------------------
void Transform::OnRenderImgui()
{
    imgui::ImguiDragXYZ("Position", _position);
    ImGui::Separator();

    // プログラムはオイラー角を使ってるけど、
    // 見やすいために degree で表示
    float3 degreeRot = float3(math::R2D(_rotation.x), math::R2D(_rotation.y), math::R2D(_rotation.z));
    imgui::ImguiDragXYZ("Rotation", degreeRot);
    _rotation = { math::D2R(degreeRot.x), math::D2R(degreeRot.y), math::D2R(degreeRot.z) };
    ImGui::Separator();

    imgui::ImguiDragXYZ("Scale", _scale);
}
//---------------------------------------------------------------------------
//! スケールを取得
//---------------------------------------------------------------------------
float3 Transform::GetScale() const
{
    return _scale;
}
//---------------------------------------------------------------------------
//! 回転のオイラー角を取得
//! 対象はZ-X-Y軸の順番に回転します(DirectXMathと同じ順番する)
//---------------------------------------------------------------------------
float3 Transform::GetRotation() const
{
    return _rotation;
}
//---------------------------------------------------------------------------
//! 座標を取得
//---------------------------------------------------------------------------
float3 Transform::GetPosition() const
{
    return _position;
}
//---------------------------------------------------------------------------
//! 右軸（Right Axis） (X)
//---------------------------------------------------------------------------
float3 Transform::GetRightAxis() const
{
    matrix m = math::MatrixRotationRollPitchYaw(_rotation);
    return float3(m._11, m._12, m._13);
}
//---------------------------------------------------------------------------
//! アップ軸 (Up Axis) (Y)
//---------------------------------------------------------------------------
float3 Transform::GetUpAxis() const
{
    matrix m = math::MatrixRotationRollPitchYaw(_rotation);
    return float3(m._21, m._22, m._23);
}
//---------------------------------------------------------------------------
//! 前方軸   (Forward Axis) (+Z)
//! @note この前方軸は+Z
//---------------------------------------------------------------------------
float3 Transform::GetForwardAxis() const
{
    matrix m = math::MatrixRotationRollPitchYaw(_rotation);
    return float3(m._31, m._32, m._33) * -1.0f;
}
//---------------------------------------------------------------------------
//! 後方軸   (Bacjwad軸)     (-z)
//! @note この後方軸は-Z
//---------------------------------------------------------------------------
float3 Transform::GetBackwardAxis() const
{
    matrix m = math::MatrixRotationRollPitchYaw(_rotation);
    return float3(m._31, m._32, m._33);
}
//---------------------------------------------------------------------------
//!< ローカル表列
//---------------------------------------------------------------------------
matrix Transform::GetLocalMatrix() const
{
    matrix scaleMatrix       = math::scale(_scale);
    matrix rotationMatrix    = math::MatrixRotationRollPitchYaw(_rotation);
    matrix translationMatrix = math::translate(_position);
    matrix localMatrix       = mul(mul(scaleMatrix, rotationMatrix), translationMatrix);
    return localMatrix;
}
//---------------------------------------------------------------------------
//!　ワールド行列
//---------------------------------------------------------------------------
matrix Transform::GetLocalToWorldMatrix() const
{
    matrix localMatrix = GetLocalMatrix();

    if(_parent) {
        localMatrix = mul(localMatrix, _parent->GetLocalToWorldMatrix());
    }

    return localMatrix;
}
//---------------------------------------------------------------------------
//! 逆ワールド行列
//---------------------------------------------------------------------------
matrix Transform::GetWorldToLocalMatrix() const
{
    return hlslpp::inverse(GetLocalToWorldMatrix());
}
//---------------------------------------------------------------------------
//! 拡大縮小のセット
//---------------------------------------------------------------------------
void Transform::SetScale(const f32 x, const f32 y, const f32 z)
{
    _scale = float3(x, y, z);
}
//---------------------------------------------------------------------------
//! 拡大縮小のセット
//---------------------------------------------------------------------------
void Transform::SetScale(const float3& scale)
{
    _scale = scale;
}
//---------------------------------------------------------------------------
//! 回転のオイラー角のセット
//---------------------------------------------------------------------------
void Transform::SetRotation(const float3& eulerAnglesInRadian)
{
    _rotation = eulerAnglesInRadian;
}
//---------------------------------------------------------------------------
//! 回転のオイラー角のセット
//---------------------------------------------------------------------------
void Transform::SetRotation(const f32 x, const f32 y, const f32 z)
{
    _rotation = float3(x, y, z);
}
//---------------------------------------------------------------------------
//! 回転のオイラー角のセット(X)
//---------------------------------------------------------------------------
void Transform::SetRotationX(const f32 rad)
{
    _rotation.x = rad;
}
//---------------------------------------------------------------------------
//! 回転のオイラー角のセット(Y)
//---------------------------------------------------------------------------
void Transform::SetRotationY(const f32 rad)
{
    _rotation.y = rad;
}
//---------------------------------------------------------------------------
//! 回転のオイラー角のセット(Z)
//---------------------------------------------------------------------------
void Transform::SetRotationZ(const f32 rad)
{
    _rotation.z = rad;
}
//---------------------------------------------------------------------------
//!  座標のセット
//---------------------------------------------------------------------------
void Transform::SetPosition(const float3& position)
{
    _position = position;
}
//---------------------------------------------------------------------------
//!  座標のセット
//---------------------------------------------------------------------------
void Transform::SetPosition(const f32 x, const f32 y, const f32 z)
{
    _position = float3(x, y, z);
}
//---------------------------------------------------------------------------
//!  現在のX座標を渡した値を設定する
//---------------------------------------------------------------------------
void Transform::SetPositionX(const f32 x)
{
    _position.x = x;
}
//---------------------------------------------------------------------------
//!  現在のY座標を渡した値を設定する
//---------------------------------------------------------------------------
void Transform::SetPositionY(const f32 y)
{
    _position.y = y;
}
//---------------------------------------------------------------------------
//!  現在のZ座標を渡した値を設定する
//---------------------------------------------------------------------------
void Transform::SetPositionZ(const f32 z)
{
    _position.z = z;
}
//---------------------------------------------------------------------------
//!  現在のX座標を渡した値と加算する
//---------------------------------------------------------------------------
void Transform::AddPositionX(const f32 addX)
{
    _position.x += addX;
}
//---------------------------------------------------------------------------
//! 現在のY座標を渡した値と加算する
//---------------------------------------------------------------------------
void Transform::AddPositionY(const f32 addY)
{
    _position.y += addY;
}
//---------------------------------------------------------------------------
//!  現在のZ座標を渡した値と加算する
//---------------------------------------------------------------------------
void Transform::AddPositionZ(const f32 addZ)
{
    _position.z += addZ;
}
//---------------------------------------------------------------------------
//!  回転角度調整（オイラー角）
//---------------------------------------------------------------------------
void Transform::Rotate(const float3& eulerAnglesInRadian)
{
    _rotation += eulerAnglesInRadian;
}
//---------------------------------------------------------------------------
//! ピッチ (Pitch)
//---------------------------------------------------------------------------
void Transform::RotateYAxis(const f32 rad)
{
    float3 rotation = GetRotation();
    rotation.y      = DirectX::XMScalarModAngle(rotation.f32[1] + rad);
    SetRotation(rotation);
}
//---------------------------------------------------------------------------
//! ロール (Roll)
//---------------------------------------------------------------------------
void Transform::RotateXAxis(const f32 rad)
{
    float3 rotation = GetRotation();
    rotation.x      = DirectX::XMScalarModAngle(rotation.x + rad);
    SetRotation(rotation);
}
//---------------------------------------------------------------------------
//! ヨー (Yaw)
//---------------------------------------------------------------------------
void Transform::RotateZAxis(const f32 rad)
{
    float3 rotation = GetRotation();
    rotation.z      = DirectX::XMScalarModAngle(rotation.z + rad);
    SetRotation(rotation);
}
//---------------------------------------------------------------------------
//! 指定の軸を回転(オイラー角）
//---------------------------------------------------------------------------
void Transform::RotateAxis(const float3& axis, float radian)
{
    matrix rotation = math::rotateAxis(axis, radian);
    _rotation       = GetEulerAnglesFromRotationMatrix(rotation);
}
//---------------------------------------------------------------------------
//! 指定の座標から回転(オイラー角）
//---------------------------------------------------------------------------
void Transform::RotateAround(const float3& point, const float3& axis, const float radian)
{
    float3 rotation = GetRotation();
    float3 position = GetPosition();

    matrix rt = mul(math::MatrixRotationRollPitchYaw(rotation), math::translate(position - point));
    rt        = mul(rt, math::rotateAxis(axis, radian));
    rt        = mul(rt, math::translate(point));

    _rotation = GetEulerAnglesFromRotationMatrix(rt);
    _position = float3(rt._41, rt._42, rt._43);
}
//---------------------------------------------------------------------------
//! 変換行列
//---------------------------------------------------------------------------
void Transform::Translate(const float3& direction, float magnitude)
{
    float3 unitDir         = hlslpp::normalize(direction);
    float3 magnitudeVector = { magnitude, magnitude, magnitude };

    float3 move = magnitude * unitDir;
    _position += move;
}
//---------------------------------------------------------------------------
//! 対象の 座標 を設定し、その方向へと向かせます
//---------------------------------------------------------------------------
void Transform::LookAt(const float3& target, const float3& up)
{
    matrix view    = math::lookAtRH(_position, target, up);
    matrix invView = hlslpp::inverse(view);
    _rotation      = GetEulerAnglesFromRotationMatrix(invView);
}
//---------------------------------------------------------------------------
//! 対象の 方向 を設定し、その方向へと向かせます
//---------------------------------------------------------------------------
void Transform::LookTo(const float3& direction, const float3& up)
{
    // DirectX::XMMatrixLookToRH の関数使うため
    // TODO: 自分のLookToRH関数を作ります
    DirectX::XMVECTOR posVector = { _position.x, _position.y, _position.z };
    DirectX::XMVECTOR dirVector = { direction.x, direction.y, direction.z };
    DirectX::XMVECTOR upVector  = { up.x, up.y, up.z };
    DirectX::XMMATRIX xmView    = DirectX::XMMatrixLookToRH(posVector, dirVector, upVector);

    DirectX::XMFLOAT4X4 rotMatrix;
    DirectX::XMStoreFloat4x4(&rotMatrix, xmView);

    matrix view = {
        float4(rotMatrix._11, rotMatrix._12, rotMatrix._13, rotMatrix._14),
        float4(rotMatrix._21, rotMatrix._22, rotMatrix._23, rotMatrix._24),
        float4(rotMatrix._31, rotMatrix._32, rotMatrix._33, rotMatrix._34),
        float4(rotMatrix._41, rotMatrix._42, rotMatrix._43, rotMatrix._44)
    };
    matrix invView = hlslpp::inverse(view);
    _rotation      = GetEulerAnglesFromRotationMatrix(invView);
}
//---------------------------------------------------------------------------
//! 対象の 座標 を設定し、その方向へと正面に向かせます
//---------------------------------------------------------------------------
void Transform::FaceAt(const float3& target, const float3& up)
{
    float3 axisZ = normalize(target - _position);
    float3 axisX = normalize(cross(up, axisZ));
    float3 axisY = cross(axisZ, axisX);

    f32 x = -dot(axisX, _position);
    f32 y = -dot(axisY, _position);
    f32 z = -dot(axisZ, _position);

    matrix view = {
        float4{ axisX.x, axisY.x, axisZ.x, 0.0f },
        float4{ axisX.y, axisY.y, axisZ.y, 0.0f },
        float4{ axisX.z, axisY.z, axisZ.z, 0.0f },
        float4{ x, y, z, 1.0f },
    };

    matrix invView = hlslpp::inverse(view);
    _rotation      = GetEulerAnglesFromRotationMatrix(invView);
}
void Transform::FaceAtWithoutY(const float3& target, const float3& up)
{
    float3 targetPos = target;
    targetPos.f32[1] = _position.f32[1];

	FaceAt(targetPos, up);
}
//---------------------------------------------------------------------------
//! 行列を分解して、Position, Scale, Rotationに設定する
//---------------------------------------------------------------------------
void Transform::DecomposeMatrix(const matrix& mat)
{
    f32* worldF16 = math::CastTof32Matrix(mat);
    f32  position[3], rotation[3], scale[3];
    ImGuizmo::DecomposeMatrixToComponents(worldF16, position, rotation, scale);

    float3 rot;
    _rotation.x = math::D2R(rotation[0]);
    _rotation.y = math::D2R(rotation[1]);
    _rotation.z = math::D2R(rotation[2]);

    _position = float3(position[0], position[1], position[2]);
    _scale    = float3(scale[0], scale[1], scale[2]);
}
//---------------------------------------------------------------------------
//! 親物件を取得
//---------------------------------------------------------------------------
raw_ptr<Transform> Transform::GetParent() const
{
    return _parent;
}
//---------------------------------------------------------------------------
//! 親物件を設定
//---------------------------------------------------------------------------
void Transform::SetParent(const raw_ptr<Transform>& parent)
{
    _parent = parent;
    if(parent) {
        _parent->_childs.emplace_back(this);
    }
}
//---------------------------------------------------------------------------
//! 子物件を削除
//---------------------------------------------------------------------------
void Transform::RemoveChild(const raw_ptr<Transform>& child)
{
    //   for(auto itr = _childs.begin(); itr != _childs.end(); ++itr) {
    //       if((*itr) == child) {
    //           _childs.erase(itr);
    //           break;
    //	}
    //}
    _childs.erase(std::remove(_childs.begin(), _childs.end(), child),
                  _childs.end());
}
//---------------------------------------------------------------------------
//! 親物件があるかどうか
//---------------------------------------------------------------------------
bool Transform::HasParent() const
{
    return _parent.get() != nullptr;
}
//---------------------------------------------------------------------------
//! 子物件があるかどうか
//---------------------------------------------------------------------------
bool Transform::HasChild() const
{
    return _childs.empty();
}
//---------------------------------------------------------------------------
//! 全部子物件を取得（Transform）
//! @returns vector<raw_ptr<Transform>>
//---------------------------------------------------------------------------
std::vector<raw_ptr<Transform>> Transform::GetChildTransform() const
{
    return _childs;
}
//---------------------------------------------------------------------------
//! 全部子物件を取得（Entity）
//! @returns vector<raw_ptr<Entity>>
//---------------------------------------------------------------------------
std::vector<raw_ptr<entity::Entity>> Transform::GetChildsInEntity() const
{
    std::vector<raw_ptr<entity::Entity>> childs;
    for(auto child : _childs) {
        childs.push_back(child->GetOwner());
    }
    return childs;
}
//---------------------------------------------------------------------------
//!  回転行列からオイラー角を取得
//---------------------------------------------------------------------------
float3 Transform::GetEulerAnglesFromRotationMatrix(const matrix& rotationMatrix)
{
    float c = sqrtf(1.0f - rotationMatrix._32 * rotationMatrix._32);

    if(isnan(c))
        c = 0.0f;

    float3 rotation;
    rotation.x = atan2f(-rotationMatrix._32, c);
    rotation.y = atan2f(rotationMatrix._31, rotationMatrix._33);
    rotation.z = atan2f(rotationMatrix._12, rotationMatrix._22);

    //pitch yaw row x y z;
    /*  rotation.z = atan2(rotationMatrix._32, rotationMatrix._22);
    rotation.x = asin(rotationMatrix._31);
    rotation.y = -atan2(rotationMatrix._21, rotationMatrix._11);*/
    return rotation;
}
}   // namespace component
