﻿//---------------------------------------------------------------------------
//!	@file	Transform.h
//!	@brief	オブジェクトの位置、回転、スケールを扱うクラス
//---------------------------------------------------------------------------
#pragma once
#include "Component.h"

namespace component {
class Transform : public Component
{
    const std::string _NAME = "Transform";

public:
    //!< コンストラクタ
    Transform(const float3& position = float3(0.0f, 0.0f, 0.0f),
              const float3& scale    = float3(1.0f, 1.0f, 1.0f),
              const float3& rotation = float3(0.0f, 0.0f, 0.0f));
    Transform(const Transform& other);              //!< コピーコンストラクタ
    Transform& operator=(const Transform& other);   //!< 代入
    ~Transform()       = default;                   //!< デストラクタ
protected:
    //---------------------------------------------------------------------------
    //! 継承関数
    //---------------------------------------------------------------------------
    void OnRenderImgui() override;   //!< ImGui描画

public:
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------

    float3 GetScale() const;   //! スケールを取得

    //! 回転のオイラー角を取得
    //! 対象はZ-X-Y軸の順番に回転します(DirectXMathと同じ順番する)
    float3 GetRotation() const;

    float3 GetPosition() const;   //!< 座標を取得

    //====================
    // 軸
    //====================
    //! 右軸    （Right Axis）  (X)
    float3 GetRightAxis() const;
    //! アップ軸 (Up Axis)      (Y)
    float3 GetUpAxis() const;
    //! 前方軸   (Forward Axis) (+Z)
    //! @note この前方軸は+Z
    float3 GetForwardAxis() const;
    //! 後方軸   (Bacjwad軸)    (-z)
    //! @note この後方軸は-Z
    float3 GetBackwardAxis() const;

    matrix GetLocalMatrix() const;          //!< ローカル表列
    matrix GetLocalToWorldMatrix() const;   //!< ワールド行列
    matrix GetWorldToLocalMatrix() const;   //!< 逆ワールド行列

    //----------
    // 拡大縮小
    void SetScale(const f32 x, const f32 y, const f32 z);   //!< 拡大縮小のセット
    void SetScale(const float3& scale);                     //!< 拡大縮小のセット

    //----------
    // 回転
    void SetRotation(const float3& eulerAnglesInRadian);       //!< 回転のオイラー角のセット   (radian)
    void SetRotation(const f32 x, const f32 y, const f32 z);   //!< 回転のオイラー角のセット   (radian)
    void SetRotationX(const f32 rad);                          //!< 回転のオイラー角のセット(X)(radian)
    void SetRotationY(const f32 rad);                          //!< 回転のオイラー角のセット(Y)(radian)
    void SetRotationZ(const f32 rad);                          //!< 回転のオイラー角のセット(Z)(radian)

    //-----------
    // 回転角度
    void Rotate(const float3& eulerAnglesInRadian);      //!< 回転角度調整（オイラー角）
    void RotateYAxis(const f32 rad);                     //!< ピッチ (Pitch) (Y)
    void RotateXAxis(const f32 rad);                     //!< ロール (Roll)  (X)
    void RotateZAxis(const f32 rad);                     //!< ヨー   (Yaw)   (Z)
    void RotateAxis(const float3& axis, float radian);   //!< 指定の軸を回転(オイラー角）
    //! 指定の座標から回転(オイラー角）
    void RotateAround(const float3& point, const float3& axis, const float radian);

    //----------
    // 座標
    void SetPosition(const float3& position);                  //!< 座標のセット
    void SetPosition(const f32 x, const f32 y, const f32 z);   //!< 座標のセット

    void SetPositionX(const f32 x);   //!< 現在のX座標を渡した値を設定する
    void SetPositionY(const f32 y);   //!< 現在のY座標を渡した値を設定する
    void SetPositionZ(const f32 z);   //!< 現在のZ座標を渡した値を設定する

    void AddPositionX(const f32 addX);   //!< 現在のX座標を渡した値と加算する
    void AddPositionY(const f32 addY);   //!< 現在のY座標を渡した値と加算する
    void AddPositionZ(const f32 addZ);   //!< 現在のZ座標を渡した値と加算する

    //! 変換行列
    void Translate(const float3& direction, float magnitude);

    //! 対象の 座標 を設定し、その方向へと向かせます
    void LookAt(const float3& target, const float3& up = { 0.0f, 1.0f, 0.0f });
    //! 対象の 方向 を設定し、その方向へと向かせます
    void LookTo(const float3& direction, const float3& up = { 0.0f, 1.0f, 0.0f });
    //! 対象の 座標 を設定し、その方向へと正面に向かせます
    void FaceAt(const float3& target, const float3& up = { 0.0f, 1.0f, 0.0f });
    //! 対象の 座標 を設定し、その方向へと正面に向かせます、でもY軸は固定
    void FaceAtWithoutY(const float3& target, const float3& up = { 0.0f, 1.0f, 0.0f });

    //! 行列を分解して、Position, Scale, Rotationに設定する
    void DecomposeMatrix(const matrix& mat);

    raw_ptr<Transform>                   GetParent() const;                              //!< 親物件を取得
    void                                 SetParent(const raw_ptr<Transform>& parent);    //!< 親物件を設定
    void                                 RemoveChild(const raw_ptr<Transform>& child);   //!< 子物件を削除
    bool                                 HasParent() const;                              //!< 親物件があるかどうか
    bool                                 HasChild() const;                               //!< 子物件があるかどうか
    std::vector<raw_ptr<entity::Entity>> GetChildsInEntity() const;                      //!< 全部子物件を取得（Entity）
    std::vector<raw_ptr<Transform>>      GetChildTransform() const;                      //!< 全部子物件を取得（Transform）
private:
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------

    //! 回転行列からオイラー角を取得
    static float3 GetEulerAnglesFromRotationMatrix(const matrix& rotationMatrix);

private:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    float3 _position = { 0.0f, 0.0f, 0.0f };   //!< 座標
    float3 _rotation = { 0.0f, 0.0f, 0.0f };   //!< 回転角度(オイラー角)
    float3 _scale    = { 1.0f, 1.0f, 1.0f };   //!< 拡大縮小

    raw_ptr<Transform>              _parent = nullptr;   //!< 親Transform
    std::vector<raw_ptr<Transform>> _childs;             //!< 子Transformリスト
};
}   // namespace component
