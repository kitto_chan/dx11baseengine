﻿//---------------------------------------------------------------------------
//!	@file	MonsterBearAction.h
//!	@brief	キャラクターのアクション処理
//---------------------------------------------------------------------------
#include "MonsterBearAction.h"
#include "entity/Entity.h"
#include "component/Transform.h"
#include "component/Animator.h"
#include "component/info/MonsterBearInfo.h"
#include "component/physics/GhostBodyCharacter.h"
#include "component/action/PlayerAction.h"

#include "component/action/SandStormAction.h"
#include "component/physics/Collider.h"

#include "manager/CameraManager.h"
#include "manager/SceneManager.h"

#include <random>

namespace component {
namespace {
std::random_device rnd;
using ActionState = MonsterBearInfo::EMonsterBearState;
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
MonsterBearAction::MonsterBearAction()
{
    _name = _NAME;
}

MonsterBearAction::~MonsterBearAction()
{
    manager::EffekseerMgr()->StopEffect(_sandStormEffectHandle);
    manager::EffekseerMgr()->StopEffect(_shockWaveHandle);
    manager::EffekseerMgr()->StopEffect(_fireFlameHandle);
}

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool MonsterBearAction::OnInit()
{
    //waitCounter = 120;
    //=======================================================================
    // 必要なコンポネントを揃えます
    //=======================================================================
    _pTransform      = _pOwner->GetComponent<component::Transform>();
    _pAnimator       = _pOwner->GetComponent<component::Animator>();
    _pCharacterInfo  = _pOwner->GetComponent<component::MonsterBearInfo>();
    _pCharaGhostBody = _pOwner->GetComponent<component::GhostBodyCharacter>();

    if(!(_pTransform && _pAnimator && _pCharacterInfo)) {
        ASSERT_MESSAGE(_pTransform, "Transform Componentが見つけません");
        ASSERT_MESSAGE(_pAnimator, "RigidBody Componentが見つけません");
        ASSERT_MESSAGE(_pCharacterInfo, "CharacterInfo Componentが見つけません");
        return false;
    }

    //=======================================================================
    // リソースを読み込み
    //=======================================================================
    _sandStormEffect = manager::EffekseerMgr()->Create(u"effekseer/SandStorm/Sandstorm.efk");
    _shockWaveEffect = manager::EffekseerMgr()->Create(u"effekseer/Normal/shockwave.efk");
    _fireFlameEffect = manager::EffekseerMgr()->Create(u"effekseer/Flame/Flame.efk");
    return true;
}

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void MonsterBearAction::OnUpdate()
{
    //waitCounter--;
    //if(waitCounter > 0) return;
    //==========================================================
    // キャラの操作
    //==========================================================
    if(_firstAction) {
        ChangeToMoveState();
        _firstAction = false;
    }
    else {
        switch(_pCharacterInfo->GetCharaState()) {
            case ECharacterState::Idle: IdleState(); break;       // 待機
            case ECharacterState::Hit: HitState(); break;         // 攻撃
            case ECharacterState::Move: MoveState(); break;       // 追跡
            case ECharacterState::Attack: AttackState(); break;   // 攻撃
            case ECharacterState::Death: DeathState(); break;
        }
    }

    // debug用
    //std::vector<raw_ptr<Entity>> enttList = manager::FindEntitiesWithTag(Tag::Player);
    //for(auto& entt : enttList) {
    //    float3 targetPos = entt->GetComponent<Transform>()->GetPosition();
    //    float2 dir       = _pTransform->GetPosition().xz - targetPos.xz;
    //    f32    length    = hlslpp::length(dir);
    //    printf_s("%f\n", length);
    //}
}

//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void MonsterBearAction::OnRender()
{
    //debug::drawMatrix(_pTransform->GetLocalToWorldMatrix(), 2.0f);
}

//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void MonsterBearAction::OnRenderImgui()
{
}

//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void MonsterBearAction::OnFinalize()
{
}
//---------------------------------------------------------------------------
//! 待機ステート処理
//---------------------------------------------------------------------------
void MonsterBearAction::IdleState()
{
    if(_idleCount > 0) {
        _idleCount--;
        return;
    }

    s32 randNum = math::GetRandomI(100);
    if(randNum < 20) {   // 20%
        _idleCount = math::GetRandomI(30, 60);
    }
    else if(randNum < 50) {   // 30%
        ChangeToMoveState();
    }
    else if(randNum < 60) {   // 10%
        ChangeToFireFlameAttackState();
    }
    else if(randNum < 75) {   // 15%
        ChangeToSandStormAttackState();
    }
    else {   // 15%
        ChangeToJumpAttackState();
    }
}
//---------------------------------------------------------------------------
//! ヒットステート
//---------------------------------------------------------------------------
void MonsterBearAction::HitState()
{
    if(_pAnimator->IsLastFrame()) {
        ChangeToIdleState();
    }
}
//---------------------------------------------------------------------------
//! 移動ステート
//---------------------------------------------------------------------------
void MonsterBearAction::MoveState()
{
    std::vector<raw_ptr<Entity>> enttList = manager::FindEntitiesWithTag(Tag::Player);
    raw_ptr<Entity>              target   = enttList.at(rnd() % enttList.size());
    if(!target) return;

    float3 targetPos = target->GetComponent<Transform>()->GetPosition();

    _pTransform->FaceAt({ targetPos.x, _pTransform->GetPosition().y, targetPos.z });

    float2 myPos  = _pTransform->GetPosition().xz - targetPos.xz;
    f32    length = hlslpp::length(myPos);

    if(length > 2.f) {
        f32    magnitude = timer::DeltaTime() * 0.5f;                               // 移動距離
        float3 unitDir   = hlslpp::normalize(_pTransform->GetForwardAxis() * -1);   // ユニット
        float3 dir       = magnitude * unitDir;                                     // 移動距離

        _pCharaGhostBody->walk(dir);
    }
    else {
        s32 randNum = math::GetRandomI(5);
        if(randNum < 1) {
            ChangeToSandStormAttackState();
        }
        else if(randNum < 2) {
            ChangeToFireFlameAttackState();
        }
        else {
            ChangeToAttackState();
        }
    }
}
//---------------------------------------------------------------------------
//! 攻撃ステート
//---------------------------------------------------------------------------
void MonsterBearAction::AttackState()
{
    static bool slowed = false;
    switch(_pCharacterInfo->GetActionState()) {
        case MonsterBearInfo::EMonsterBearState::JumpAttack:
            if(!_jumpAttackInfo.attacked && _pAnimator->GetCurrentAnimationTime() >= _jumpAttackInfo.ATTACK_TIME) {
                std::vector<raw_ptr<Entity>> enttList = manager::FindEntitiesWithTag(Tag::Player);

                for(auto& entt : enttList) {
                    float3 targetPos = entt->GetComponent<Transform>()->GetPosition();
                    float2 dir       = _pTransform->GetPosition().xz - targetPos.xz;
                    f32    length    = hlslpp::length(dir);
                    if(length < 5.f) {
                        float3 impulseDir = hlslpp::normalize(targetPos - _pTransform->GetPosition());
                        entt->GetComponent<PlayerAction>()->ApplyBackwardImpulse(impulseDir, 5);
                        entt->GetComponent<PlayerAction>()->ApplyDamage(15);
                    }
                }

                // 衝撃波エフェクト
                float3 frontDir  = hlslpp::normalize(_pTransform->GetForwardAxis());
                _shockWaveHandle = manager::EffekseerMgr()->Play(_shockWaveEffect, _pTransform->GetPosition() - frontDir);
                manager::EffekseerMgr()->SetScale(_shockWaveHandle, float3(2.5f, 2.5f, 2.5f));
                manager::EffekseerMgr()->SetRotation(_shockWaveHandle, float3(math::D2R(90.0f), 0.0f, 0.0f));

                _pCharaGhostBody->stop();   //!< 移動停止
                input::GamePadMgr()->SetVibration(1.0f, 1.0f, 10);
                manager::GetCurrentCameraMgr()->GetCurrentCamera()->SetShakeTime(10);
                _jumpAttackInfo.attacked = true;
            }
        case MonsterBearInfo::EMonsterBearState::SandStormAttack:
            break;
        case MonsterBearInfo::EMonsterBearState::Punch:
            if(!slowed && _pAnimator->GetCurrentAnimationTime() >= 0.0f) {
                _pAnimator->SetAnimationSpeed(math::GetRandomF(0.3f, 1.0f), 0.25f);
                slowed = true;
            }
            break;
        case MonsterBearInfo::EMonsterBearState::Swiping:
            if(!slowed && _pAnimator->GetCurrentAnimationTime() > 1.f) {
                _pAnimator->SetAnimationSpeed(math::GetRandomF(0.1f, 1.5f), 0.25f);
                slowed = true;
            }
            break;
        case MonsterBearInfo::EMonsterBearState::FireFlameAttact:
            if(!_fireFrameAttackInfo.attacked && _pAnimator->GetCurrentAnimationTime() >= _fireFrameAttackInfo.ATTACK_TIME) {
                // プレイヤーを探す
                std::vector<raw_ptr<Entity>> enttList = manager::FindEntitiesWithTag(Tag::Player);
                for(auto& entt : enttList) {
                    float3 targetPos = entt->GetComponent<Transform>()->GetPosition();
                    float2 dir       = _pTransform->GetPosition().xz - targetPos.xz;
                    f32    length    = hlslpp::length(dir);
                    if(length < 7.5f) {
                        float3 impulseDir = hlslpp::normalize(targetPos - _pTransform->GetPosition());
                        entt->GetComponent<PlayerAction>()->ApplyBackwardImpulse(impulseDir, 10);
                        entt->GetComponent<PlayerAction>()->ApplyDamage(35);
                    }
                }
                _fireFrameAttackInfo.attacked = true;
            }
            break;
        default:
            break;
    }

    if(_pAnimator->IsLastFrame()) {
        _jumpAttackInfo.attacked      = false;
        _fireFrameAttackInfo.attacked = false;
        _pCharacterInfo->SetSuperArmor(false);
        slowed = false;
        ChangeToIdleState();
    }
}
void MonsterBearAction::SandStormAttackState()
{
}
//---------------------------------------------------------------------------
//! デスステート
//---------------------------------------------------------------------------
void MonsterBearAction::DeathState()
{
    if(_pAnimator->IsLastFrame()) {
        manager::SetNextSceneWithFade(scene::EScene::Clear);
    }
}
//---------------------------------------------------------------------------
//! デスステートに移行
//---------------------------------------------------------------------------
bool MonsterBearAction::ChangeToDeathState()
{
    _pCharaGhostBody->stop();
    _pAnimator->Play("Death", Animation::PlayType::Once);
    _pCharacterInfo->SetCharaState(ECharacterState::Death);

    return true;
}
//---------------------------------------------------------------------------
//!ジャンプアタックステートに移行
//---------------------------------------------------------------------------
bool MonsterBearAction::ChangeToJumpAttackState()
{
    std::vector<raw_ptr<Entity>> enttList = manager::FindEntitiesWithTag(Tag::Player);
    raw_ptr<Entity>              target   = enttList.at(rnd() % enttList.size());
    if(!target) return false;

    float3 targetPos = target->GetComponent<Transform>()->GetPosition();

    _pTransform->FaceAt({ targetPos.x, _pTransform->GetPosition().y, targetPos.z });

    f32    magnitude = timer::DeltaTime() * 1.0f;                               // 移動距離
    float3 unitDir   = hlslpp::normalize(_pTransform->GetForwardAxis() * -1);   // ユニット
    float3 dir       = magnitude * unitDir;                                     // 移動距離

    _pCharaGhostBody->walk(dir);

    _pAnimator->Play("JumpAttack", Animation::PlayType::Once);
    _pCharacterInfo->SetActionState(ActionState::JumpAttack);
    _pCharacterInfo->SetCharaState(ECharacterState::Attack);
    _pCharacterInfo->SetSuperArmor(true);

    return true;
}
//---------------------------------------------------------------------------
//! 砂嵐を生成
//---------------------------------------------------------------------------
void MonsterBearAction::InstantiateSandStorm()
{
    gameobject::GameObject* pMonsterSandStormArea = new gameobject::GameObject("SandStorm");
    pMonsterSandStormArea->SetLayer(Layer::EnemyWeapon);
    pMonsterSandStormArea->AddComponent<component::BoxCollider>(float3(3.0f, 3.0f, 7.0f), float3(0.0f, 0.0f, 7.0f));
    pMonsterSandStormArea->AddComponent<component::GhostBody>();
    pMonsterSandStormArea->AddComponent<component::SandStormAction>()->SetParent(_pOwner);
    pMonsterSandStormArea->GetComponent<component::Transform>()->SetPosition(_pTransform->GetPosition());
    pMonsterSandStormArea->GetComponent<component::Transform>()->SetRotation(_pTransform->GetRotation());
    manager::Regist(pMonsterSandStormArea);
}
//---------------------------------------------------------------------------
//! 攻撃を受けたら、ダメージを返す
//! @note -1 なら まだCD中
//---------------------------------------------------------------------------
s32 MonsterBearAction::GetAttackDamage() const
{
    MonsterBearInfo::AttackInfo atkInfo = _pCharacterInfo->GetAttackInfo(_pCharacterInfo->GetActionState());
    if(_pAnimator->GetCurrentAnimationTime() > atkInfo.inTime &&
       _pAnimator->GetCurrentAnimationTime() < atkInfo.outTime) {
        return atkInfo.damage;
    }
    return -1;
}
//---------------------------------------------------------------------------
//! 待機ステート設定
//---------------------------------------------------------------------------
bool MonsterBearAction::ChangeToIdleState()
{
    _pCharacterInfo->SetCharaState(ECharacterState::Idle);
    _pAnimator->Play("Idle", Animation::PlayType::Loop);
    return true;
}
//---------------------------------------------------------------------------
//! ヒットステート設定
//---------------------------------------------------------------------------
bool MonsterBearAction::ChangeToHitState(s32 damage)
{
    if(_pCharacterInfo->CanHit()) {
        _pCharacterInfo->SetHitCount(20);
        _pCharacterInfo->AddCurrentHP(-damage);

        if(_pCharacterInfo->GetCurrentHPRate() <= 0.0f) {
            ChangeToDeathState();
        }
        else {
            // スパアマ状態じゃないと、アニメションと反作用力を与えない
            if(!_pCharacterInfo->IsSuperArmor()) {
                _pCharaGhostBody->stop();
                _pCharacterInfo->SetCharaState(ECharacterState::Hit);
                _pAnimator->Play("Hit", Animation::PlayType::Once);

                // 反作用力
                float3 backworkDir = normalize(_pTransform->GetForwardAxis());
                _pCharaGhostBody->applyImpulse(backworkDir * 1.0f);
            }
        }
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
//! 移動ステート設定
//---------------------------------------------------------------------------
bool MonsterBearAction::ChangeToMoveState()
{
    _pCharacterInfo->SetCharaState(ECharacterState::Move);
    _pAnimator->Play("Walk", Animation::PlayType::Loop);
    return true;
}
//---------------------------------------------------------------------------
//! 攻撃ステート設定
//---------------------------------------------------------------------------
bool MonsterBearAction::ChangeToAttackState()
{
    _pCharaGhostBody->stop();
    _pCharacterInfo->SetCharaState(ECharacterState::Attack);

    if(math::GetRandomI(10) > 4) {
        _pCharacterInfo->SetActionState(MonsterBearInfo::EMonsterBearState::Punch);
        _pAnimator->Play("Punch", Animation::PlayType::Once);
    }
    else {
        _pCharacterInfo->SetActionState(MonsterBearInfo::EMonsterBearState::Swiping);
        _pAnimator->Play("Swiping", Animation::PlayType::Once);
    }
    return true;
}
//---------------------------------------------------------------------------
//! 砂の魔法攻撃設定
//---------------------------------------------------------------------------
bool MonsterBearAction::ChangeToSandStormAttackState()
{
    _pCharaGhostBody->stop();
    // ターゲットを探します
    std::vector<raw_ptr<Entity>> enttList = manager::FindEntitiesWithTag(Tag::Player);
    raw_ptr<Entity>              target   = enttList.at(rnd() % enttList.size());
    if(!target) return false;

    // ターゲットを向けます
    float3 faceAt = target->GetComponent<component::Transform>()->GetPosition();
    faceAt.f32[1] = _pTransform->GetPosition().f32[1];
    _pTransform->FaceAt(faceAt);

    // 砂嵐を生成
    InstantiateSandStorm();

    // 砂嵐を生成
    float3 monsterForwardAxis = _pTransform->GetBackwardAxis();   //!< モンスターに対してBackwardはForward
    monsterForwardAxis        = hlslpp::normalize(monsterForwardAxis) * 2;
    _sandStormEffectHandle    = manager::EffekseerMgr()->Play(_sandStormEffect, _pTransform->GetPosition() + monsterForwardAxis);
    manager::EffekseerMgr()->SetRotation(_sandStormEffectHandle, _pTransform->GetRotation());
    manager::EffekseerMgr()->SetScale(_sandStormEffectHandle, float3(0.3f, 0.3f, 0.3f));

    // 設定
    _pAnimator->Play("MagicAttack", Animation::PlayType::Once);
    _pCharacterInfo->SetActionState(ActionState::SandStormAttack);
    _pCharacterInfo->SetCharaState(ECharacterState::Attack);
    _pCharacterInfo->SetSuperArmor(true);

    return true;
}
//---------------------------------------------------------------------------
//! ファイアーフレームの魔法攻撃ステートに移行
//---------------------------------------------------------------------------
bool MonsterBearAction::ChangeToFireFlameAttackState()
{
    // 移動停止
    _pCharaGhostBody->stop();

    // ファイアーフレームの設定
    float3 frontDir  = hlslpp::normalize(_pTransform->GetForwardAxis());
    float3 effectPos = _pTransform->GetPosition() - frontDir;
    _pOwner->GetComponent<component::Animator>()->SetAnimationSpeed(0.78f, 1.7f);
    _fireFlameHandle = manager::EffekseerMgr()->Play(_fireFlameEffect, effectPos);
    //manager::EffekseerMgr()->SetScale(_fireFlameHandle, float3(1.0f, 1.0f, 1.0f));

    // 設定
    _pAnimator->Play("FireFlameAttack", Animation::PlayType::Once);
    _pCharacterInfo->SetActionState(ActionState::FireFlameAttact);
    _pCharacterInfo->SetCharaState(ECharacterState::Attack);
    _pCharacterInfo->SetSuperArmor(true);

    return true;
}
}   // namespace component
