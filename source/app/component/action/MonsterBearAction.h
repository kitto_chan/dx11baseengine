﻿//---------------------------------------------------------------------------
//!	@file	MonsterBearAction.h
//!	@brief	キャラクターのアクション処理
//---------------------------------------------------------------------------
#pragma once
#include "component/Component.h"
using namespace state;

namespace component {
class Transform;
class Animator;
class MonsterBearInfo;
class GhostBodyCharacter;

class MonsterBearAction : public Component
{
    const std::string _NAME = "MonsterBearAction";

public:
    MonsterBearAction();    //!< コンストラクタ
    ~MonsterBearAction();   //!< デストラクタ

    //　ジャップの攻撃データ
    struct JumpAttackInfo
    {
        bool                 attacked    = false;
        static constexpr f32 ATTACK_TIME = 1.65f;   //　攻撃時間
    };

    // ファイアーフレームの攻撃データ
	// TODO: clean code with JumpAttackInfo
    struct FireFrameAttackInfo
    {
        bool                 attacked    = false;
        static constexpr f32 ATTACK_TIME = 1.8f;   //　攻撃時間
    };

private:
    //---------------------------------------------------------------------------
    // private 継承関数
    //---------------------------------------------------------------------------
    virtual bool OnInit() override;          //!< 初期化
    virtual void OnUpdate() override;        //!< 更新
    virtual void OnRender() override;        //!< 描画
    virtual void OnRenderImgui() override;   //!< ImGui描画
    virtual void OnFinalize() override;      //!< 解放

public:
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    // public 変数
    //---------------------------------------------------------------------------
    s32 GetAttackDamage() const;   //!< ダメージを取得

    bool ChangeToHitState(s32 damage);   //!< ヒットステートに移行

private:
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------

    //==============================
    // 各ステートの処理
    //==============================
    void IdleState();              //!< アイドルステート
    void HitState();               //!< ヒットステート
    void MoveState();              //!< 移動ステート
    void AttackState();            //!< 攻撃ステート
    void SandStormAttackState();   //!< 砂の魔法攻撃ステート
    void DeathState();             //!< デスステート

    //==============================
    // ステート移行の処理
    //==============================
    bool ChangeToIdleState();              //!< アイドルステートに移行
    bool ChangeToMoveState();              //!< 移動ステートに移行
    bool ChangeToAttackState();            //!< 攻撃ステートに移行
    bool ChangeToDeathState();             //!< デスステートの移行
    bool ChangeToJumpAttackState();        //!< ジャンプアタックに移行
    bool ChangeToSandStormAttackState();   //!< 砂の魔法攻撃ステートに移行
    bool ChangeToFireFlameAttackState();   //!< ファイアーフレームの魔法攻撃ステートに移行

    void InstantiateSandStorm();   //!< 砂嵐を生成
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    raw_ptr<Transform>          _pTransform      = nullptr;   //!< Transformコンポーネントの参照
    raw_ptr<Animator>           _pAnimator       = nullptr;   //!< Animatorコンポーネントの参照
    raw_ptr<MonsterBearInfo>    _pCharacterInfo  = nullptr;   //!< 自身状態のデータコンポーネントの参照
    raw_ptr<GhostBodyCharacter> _pCharaGhostBody = nullptr;   //!< 物理キャラのコンポーネントの参照

    s32 waitCounter = 0;   //!< 次の行動までのカウント

    s32 _idleCount = 0;

    Effekseer::EffectRef _sandStormEffect;         //!< 砂嵐のエフェクト
    Effekseer::Handle    _sandStormEffectHandle;   //!< 砂嵐のハンドル

    Effekseer::EffectRef _shockWaveEffect;   //!< 衝撃波のエフェクト
    Effekseer::Handle    _shockWaveHandle;   //!< 衝撃波のハンドル

    Effekseer::EffectRef _fireFlameEffect;   //!< ファイアーフレームのエフェクト
    Effekseer::Handle    _fireFlameHandle;   //!< ファイアーフレームのハンドル

    bool _firstAction = true;

    JumpAttackInfo      _jumpAttackInfo;        //!< ジャンプアタック用データ
    FireFrameAttackInfo _fireFrameAttackInfo;   //!< ファイアーフレームアタック用データ
};
}   // namespace component
