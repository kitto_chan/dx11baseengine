﻿//---------------------------------------------------------------------------
//!	@file	MonsterBearWeaponAction.h
//!	@brief	MonsterBear武器の処理
//---------------------------------------------------------------------------
#pragma once
#include "component/Component.h"
using namespace state;

namespace component {
class MonsterBearAction;
class Animator;
class GhostBody;

class MonsterBearWeaponAction : public Component, btActionInterface
{
    const std::string _NAME = "MonsterBearWeaponAction";

public:
    MonsterBearWeaponAction();              //!< コンストラクタ
    ~MonsterBearWeaponAction() = default;   //!< デストラクタ

    enum class CollisionMode
    {
        None,
        RightHand,
        LeftHand
    };

    //---------------------------------------------------------------------------
    // public 継承関数
    //---------------------------------------------------------------------------

    // Bulletから呼び出される関数
    virtual void updateAction([[maybe_unused]] btCollisionWorld* collisionWorld, [[maybe_unused]] btScalar deltaTimeStep) override;
    virtual void debugDraw([[maybe_unused]] btIDebugDraw* debugDrawer) override{};

public:
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    // public 変数
    //---------------------------------------------------------------------------
    bool BeforeCheck();
    void SetCollisionMode(CollisionMode collisionMode) { _collisionMode = collisionMode; };

private:
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
    virtual bool OnInit() override;          //!< 初期化
    virtual void OnUpdate() override;        //!< 更新
    virtual void OnRender() override;        //!< 描画
    virtual void OnRenderImgui() override;   //!< ImGui描画
    virtual void OnFinalize() override;      //!< 解放

    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    raw_ptr<Transform> _pTransform = nullptr;
    raw_ptr<GhostBody> _pGhostBody = nullptr;

    CollisionMode _collisionMode = CollisionMode::None;   //!< 当たり判定モード
};
}   // namespace component
