﻿//---------------------------------------------------------------------------
//! @file	PlayerAction.h
//!	@brief	プレイヤーの入力処理のコンポーネント
//---------------------------------------------------------------------------
#include "PlayerAction.h"
#include "component/Transform.h"
#include "component/Animator.h"
#include "component/physics/Collider.h"
#include "component/info/PlayerInfo.h"
#include "component/physics/GhostBodyCharacter.h"

#include "entity/Entity.h"
#include "manager/CameraManager.h"
#include "manager/SceneManager.h"

namespace component {
namespace {
constexpr f32 ROLL_MP = 15.0f;   //!< 回避の消費MP
constexpr f32 RUN_MP  = 8.0f;    //!< 走るの消費MP

constexpr f32 RECOVER_MP = 20.0f;   //!< 回復
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
PlayerAction::PlayerAction()
{
    _name = _NAME;
}
//---------------------------------------------------------------------------
//! デスストラクタ
//---------------------------------------------------------------------------
PlayerAction::~PlayerAction()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool PlayerAction::OnInit()
{
    //==========================================================
    // 必要なコンポーネントを取得
    //==========================================================
    _pTransform        = _pOwner->GetComponent<Transform>();
    _pAnimator         = _pOwner->GetComponent<Animator>();
    _pCapsuleZCollider = _pOwner->GetComponent<CapsuleZCollider>();
    _pPlayerInfo       = _pOwner->GetComponent<PlayerInfo>();
    _pGhostBodyChara   = _pOwner->GetComponent<GhostBodyCharacter>();

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void PlayerAction::OnUpdate()
{
    //==========================================================
    // プレイヤー操作
    //==========================================================
    switch(_pPlayerInfo->GetCharaState()) {
        case ECharacterState::Idle: IdleState(); break;       // 待機
        case ECharacterState::Move: MoveState(); break;       // 移動
        case ECharacterState::Jump: JumpState(); break;       // ジャンプ
        case ECharacterState::Attack: AttackState(); break;   // 攻撃
        case ECharacterState::Roll: RollState(); break;       // ロール
        case ECharacterState::Hit: HitState(); break;         // ヒット
        case ECharacterState::Death: DeathState(); break;
    }
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void PlayerAction::OnRenderImgui()
{
    imgui::ImguiDragFloat("WalkSpeed", _walkSpeed);
    imgui::ImguiDragFloat("RunSpeed", _runSpeed);
    imgui::ImguiDragFloat("RotateSpeed", _rotSpeed);

    imgui::ImguiDragXYZ("Jump Force", _jumpForce);
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void PlayerAction::OnFinalize()
{
}

//---------------------------------------------------------------------------
//! 待機処理
//---------------------------------------------------------------------------
void PlayerAction::IdleState()
{
    _pAnimator->Play("Idle", Animation::PlayType::Loop);
    _pPlayerInfo->SetPlayerState(PlayerInfo::EPlayerState::Idle);
    _pPlayerInfo->SetCharaState(ECharacterState::Idle);
    _pPlayerInfo->AddCurrentMP(RECOVER_MP * timer::DeltaTime());
    // 移動
    if(IsPressingMovingKey()) {
        _pPlayerInfo->SetCharaState(ECharacterState::Move);
    }

    // ジャンプ
    HandleJumpKey();

    // 攻撃
    HandleAttackKey();

    // ロール
    HandleRollKey();
}
//---------------------------------------------------------------------------
//! 移動処理
//---------------------------------------------------------------------------
void PlayerAction::MoveState()
{
    if((input::IsKeyDown(DirectX::Keyboard::LeftShift) ||
        input::GamePadMgr()->IsRPressed())) {
        _currentSpeed = _runSpeed;
        _pAnimator->Play("Run", Animation::PlayType::Loop);
        _pPlayerInfo->SetPlayerState(PlayerInfo::EPlayerState::Run);
        _pPlayerInfo->AddCurrentMP(-RUN_MP * timer::DeltaTime());
    }
    else {
        _currentSpeed = _walkSpeed;
        _pAnimator->Play("Walk", Animation::PlayType::Loop);
        _pPlayerInfo->SetPlayerState(PlayerInfo::EPlayerState::Walk);
        _pPlayerInfo->AddCurrentMP(RECOVER_MP * timer::DeltaTime());
    }

    // 移動
    float3 moveDir = GetMoveDirection();   // 移動のベクトルを取得
    if(!HandleMoveKey(moveDir)) {
        ChangeToIdleState();
    }

    // ジャンプ
    HandleJumpKey();

    // 攻撃
    HandleAttackKey();

    // ロール（回避）
    HandleRollKey();
}
//---------------------------------------------------------------------------
//! ジャンプ処理
//---------------------------------------------------------------------------
void PlayerAction::JumpState()
{
    if(_pGhostBodyChara->canJump() || _pAnimator->IsAnimationLastFrame("Jump", 0.25f)) {
        if(IsPressingMovingKey()) {
            _pPlayerInfo->SetCharaState(ECharacterState::Move);
        }
        else {
            _pAnimator->Play("Idle", Animation::PlayType::Loop);
            ChangeToIdleState();
        }
    }
    // ジャンプ中の移動
    else {
        if(input::IsKeyDown(DirectX::Keyboard::LeftShift) || input::GamePadMgr()->IsXPressed()) {
            _currentSpeed = _runSpeed;
        }
        else {
            _currentSpeed = _walkSpeed;
        }

        float3 dir = GetMoveDirection();   // 移動のベクトルを取得
        HandleMoveKey(dir);
    }
}
//---------------------------------------------------------------------------
//! 攻撃処理
//---------------------------------------------------------------------------
void PlayerAction::AttackState()
{
    _pPlayerInfo->AddCurrentMP(RECOVER_MP * 0.5f * timer::DeltaTime());
    // スレッシュ（コンボ1）
    if(_pPlayerInfo->IsPlayerState(PlayerInfo::EPlayerState::Slash1)) {
        _pAnimator->Play("Slash", Animation::PlayType::Once, 1.3f);

        f32 comboTime   = _pAnimator->GetCurrentLayerAnimationLength() * 0.2f;
        f32 currentTime = _pAnimator->GetCurrentAnimationTime();
        if(_pAnimator->IsAnimationLastFrame("Slash", 0.45f)) {
            if(_isCombo) {
                _pAnimator->Play("Slash2", Animation::PlayType::Once, 1.3f);
                _pPlayerInfo->SetPlayerState(PlayerInfo::EPlayerState::Slash2);
                _isCombo = false;
            }
            else {
                if(IsPressingMovingKey()) {
                    _pPlayerInfo->SetCharaState(ECharacterState::Move);
                }
                else {
                    _pAnimator->Play("Idle", Animation::PlayType::Loop);
                    ChangeToIdleState();
                }
            }
        }
        else {
            if((input::IsPressLeftButton() || input::GamePadMgr()->IsXPressed()) &&
               currentTime > comboTime) {
                _isCombo = true;
            }
        }
    }
    // スレッシュ（コンボ2）
    else if(_pPlayerInfo->IsPlayerState(PlayerInfo::EPlayerState::Slash2)) {
        f32 comboTime   = _pAnimator->GetCurrentLayerAnimationLength() * 0.2f;
        f32 currentTime = _pAnimator->GetCurrentAnimationTime();
        if(_pAnimator->IsAnimationLastFrame("Slash2", 0.45f)) {
            if(_isCombo) {
                _pAnimator->Play("Slash3", Animation::PlayType::Once, 1.3f);
                _pPlayerInfo->SetPlayerState(PlayerInfo::EPlayerState::Slash3);
                _isCombo = false;
            }
            else {
                if(IsPressingMovingKey()) {
                    _pPlayerInfo->SetCharaState(ECharacterState::Move);
                }
                else {
                    _pAnimator->Play("Idle", Animation::PlayType::Loop);
                    ChangeToIdleState();
                }
            }
        }
        else {
            if((input::IsPressLeftButton() || input::GamePadMgr()->IsXPressed()) &&
               currentTime > comboTime) {
                _isCombo = true;
            }
        }
    }
    // スレッシュ（コンボ3）
    else if(_pPlayerInfo->IsPlayerState(PlayerInfo::EPlayerState::Slash3)) {
        if(_pAnimator->IsAnimationLastFrame("Slash3", 0.25f)) {
            if(IsPressingMovingKey()) {
                _pPlayerInfo->SetCharaState(ECharacterState::Move);
            }
            else {
                _pAnimator->Play("Idle", Animation::PlayType::Loop);
                ChangeToIdleState();
            }
        }
    }

    // ロール（回避）
    HandleRollKey();
}
//---------------------------------------------------------------------------
//! ロール処理
//---------------------------------------------------------------------------
void PlayerAction::RollState()
{
    if(_pAnimator->IsAnimationLastFrame("Roll", 0.35f)) {
        _pGhostBodyChara->walk(float3(0.f, 0.f, 0.f));
    }

    if(_pAnimator->IsAnimationLastFrame("Roll", 0.35f)) {
        if(IsPressingMovingKey()) {
            _pPlayerInfo->SetCharaState(ECharacterState::Move);
        }
        else {
            _pAnimator->Play("Idle", Animation::PlayType::Loop);
            ChangeToIdleState();
        }
    }
}
//---------------------------------------------------------------------------
//! ヒットステート処理
//---------------------------------------------------------------------------
void PlayerAction::HitState()
{
    if(!_pAnimator->IsLastFrame()) return;

    if(IsPressingMovingKey()) {
        ChangeToMoveState();
    }
    else {
        ChangeToIdleState();
    }
}
//---------------------------------------------------------------------------
//! デスステートの処理
//---------------------------------------------------------------------------
void PlayerAction::DeathState()
{
    if(_pAnimator->IsLastFrame()) {
        manager::SetNextSceneWithFade(scene::EScene::Over);
    }
}
//---------------------------------------------------------------------------
//! 移動のキー押してるかどうか
//---------------------------------------------------------------------------
bool PlayerAction::IsPressingMovingKey() const
{
    // キーボード
    bool isPressingKeyboard = input::IsKeyDown(DirectX::Keyboard::W) ||
                              input::IsKeyDown(DirectX::Keyboard::S) ||
                              input::IsKeyDown(DirectX::Keyboard::A) ||
                              input::IsKeyDown(DirectX::Keyboard::D);

    if(!input::IsGamePadConnected()) return isPressingKeyboard;

    bool isPressingGamePad = input::GamePadMgr()->GetLeftThumbSticksX() != 0.0f ||
                             input::GamePadMgr()->GetLeftThumbSticksY() != 0.0f;

    return isPressingGamePad || isPressingKeyboard;
}
//---------------------------------------------------------------------------
//! 移動キー処理
//---------------------------------------------------------------------------
bool PlayerAction::HandleMoveKey(float3 moveDir)
{
    const raw_ptr<BaseCamera> currentCameraRef = manager::GetCurrentCameraMgr()->GetCurrentCamera();

    // Y軸回転角度計算
    f32 x = 0.0f;
    f32 z = 0.0f;

    if(input::IsGamePadConnected()) {
        x = input::GamePadMgr()->GetLeftThumbSticksX();
        z = input::GamePadMgr()->GetLeftThumbSticksY() * -1.0f;
    }

    if(input::IsKeyDown(DirectX::Keyboard::W)) {
        z = -1.0f;
    }
    if(input::IsKeyDown(DirectX::Keyboard::S)) {
        z = 1.0f;
    }
    if(input::IsKeyDown(DirectX::Keyboard::A)) {
        x = -1.0f;
    }
    if(input::IsKeyDown(DirectX::Keyboard::D)) {
        x = 1.0f;
    }

    if(z != 0.0f || x != 0.0f) {
        //==========
        // 回転処理8方向
        //==========
        f32 angle = atan2f(x, z);
        angle += currentCameraRef->GetRotation().y;
        _pTransform->RotateAxis(float3(0.0f, 1.0f, 0.0f), angle);

        //==========
        // 移動
        //==========
        _pGhostBodyChara->walk(moveDir * -1);   //!< 前方は-1なので * -1
        return true;
    }
    // ストップ
    else {
        _pGhostBodyChara->walk(float3(0.f, 0.f, 0.f));
        return false;
    }
}
//---------------------------------------------------------------------------
//! ジャンプキー処理
//---------------------------------------------------------------------------
bool PlayerAction::HandleJumpKey()
{
    if(!input::IsKeyPress(DirectX::Keyboard::Space) &&
       !input::GamePadMgr()->IsAPressed())
        return false;

    if(!_pGhostBodyChara->canJump()) return false;

    // ジャンプ
    _pGhostBodyChara->jump(_jumpForce);
    _pAnimator->Play("Jump", Animation::PlayType::Once);
    _pPlayerInfo->SetCharaState(ECharacterState::Jump);
    _pPlayerInfo->SetPlayerState(PlayerInfo::EPlayerState::Jumping);

    return true;
}
//---------------------------------------------------------------------------
//! 攻撃キー処理
//---------------------------------------------------------------------------
bool PlayerAction::HandleAttackKey()
{
    if(!input::IsPressLeftButton() &&
       !input::GamePadMgr()->IsXPressed())
        return false;

    _pGhostBodyChara->walk(float3(0.f, 0.f, 0.f));

    _pPlayerInfo->SetCharaState(ECharacterState::Attack);
    _pPlayerInfo->SetPlayerState(PlayerInfo::EPlayerState::Slash1);

    // 一番近く敵に向きます
    {
        auto   enemys  = manager::FindEntitiesWithTag(entity::Tag::Enemey);
        float3 selfPos = _pTransform->GetPosition();
        float3 nearPos = selfPos + 4.0f;
        f32    minDist = 4.0f;
        for(auto enemy : enemys) {
            float3 enemyPos = enemy->GetComponent<Transform>()->GetPosition();
            enemyPos.f32[1] = selfPos.f32[1];
            f32 dist        = hlslpp::length(enemyPos - selfPos);
            if(dist < minDist) {
                minDist = dist;
                nearPos = enemyPos;
            }
        }
        if(minDist < 4) {
            _pTransform->FaceAt(nearPos);
        }
        if(IsPressingMovingKey())
            _pGhostBodyChara->applyImpulse(hlslpp::normalize(_pTransform->GetBackwardAxis()) * 6.0f);
    }

    return true;
}
//---------------------------------------------------------------------------
//! ロールキー処理
//---------------------------------------------------------------------------
bool PlayerAction::HandleRollKey()
{
    if(!input::IsKeyPress(DirectX::Keyboard::LeftControl) &&
       !input::GamePadMgr()->IsBPressed())
        return false;

    if(!_pPlayerInfo->IsEnoughMP(ROLL_MP)) {
        return false;
    }

    const raw_ptr<BaseCamera> currentCameraRef = manager::GetCurrentCameraMgr()->GetCurrentCamera();

    // Y軸回転角度計算
    f32 x = 0.0f;
    f32 z = 0.0f;

    if(input::IsGamePadConnected()) {
        x = input::GamePadMgr()->GetLeftThumbSticksX();
        z = input::GamePadMgr()->GetLeftThumbSticksY() * -1.0f;
    }

    if(input::IsKeyDown(DirectX::Keyboard::W)) {
        z = -1.0f;
    }
    if(input::IsKeyDown(DirectX::Keyboard::S)) {
        z = 1.0f;
    }
    if(input::IsKeyDown(DirectX::Keyboard::A)) {
        x = -1.0f;
    }
    if(input::IsKeyDown(DirectX::Keyboard::D)) {
        x = 1.0f;
    }

    if(z != 0.0f || x != 0.0f) {
        //==========
        // 回転処理8方向
        //==========
        f32 angle = atan2f(x, z);
        angle += currentCameraRef->GetRotation().y;
        _pTransform->RotateAxis(float3(0.0f, 1.0f, 0.0f), angle);
    }

    _currentSpeed = 1.0f;
    _pGhostBodyChara->walk(-GetMoveDirection());
    _pAnimator->Play("Roll", Animation::PlayType::Once);
    _pPlayerInfo->SetCharaState(ECharacterState::Roll);
    _pPlayerInfo->SetPlayerState(PlayerInfo::EPlayerState::Roll);
    _pPlayerInfo->AddCurrentMP(-ROLL_MP);
    return true;
}
//---------------------------------------------------------------------------
//! ヒットステートに変更
//---------------------------------------------------------------------------
void PlayerAction::ChangeToHitState()
{
    if(_pPlayerInfo->IsCharaState(ECharacterState::Hit)) return;

    _pGhostBodyChara->stop();
    _pAnimator->Play("Hit", Animation::PlayType::Once);
    _pPlayerInfo->SetCharaState(ECharacterState::Hit);
    _pPlayerInfo->SetPlayerState(PlayerInfo::EPlayerState::Hit);
    input::GamePadMgr()->SetVibration(0.5f, 0.5f, 7);
}
//---------------------------------------------------------------------------
//! デスステートに変換
//---------------------------------------------------------------------------
void PlayerAction::ChangeToDeathState()
{
    _pGhostBodyChara->stop();
    _pAnimator->Play("Death", Animation::PlayType::Once);
    _pPlayerInfo->SetCharaState(ECharacterState::Death);
}
//---------------------------------------------------------------------------
//! 反作用力を与える
//---------------------------------------------------------------------------
void PlayerAction::ApplyBackwardImpulse(const float3& dir, f32 power)
{
    _pGhostBodyChara->applyImpulse(dir * power);
}
//---------------------------------------------------------------------------
//! ダメージを与える
//---------------------------------------------------------------------------
void PlayerAction::ApplyDamage(s32 damage)
{
    if(_pPlayerInfo->CanHit() && !_pAnimator->IsAnimationName("Roll")) {
        _pPlayerInfo->SetHitCount(60);
        _pPlayerInfo->AddCurrentHP(-damage);

        if(_pPlayerInfo->GetCurrentHPRate() <= 0.0f) {
            ChangeToDeathState();
        }
        else {
            ChangeToHitState();
        }
    }
}
//---------------------------------------------------------------------------
//! 待機ステートに変換
//---------------------------------------------------------------------------
void PlayerAction::ChangeToIdleState()
{
    _pGhostBodyChara->walk(float3(0.f, 0.f, 0.f));

    _pPlayerInfo->SetCharaState(ECharacterState::Idle);
    _pPlayerInfo->SetPlayerState(PlayerInfo::EPlayerState::Idle);
}
//---------------------------------------------------------------------------
//! 移動ステートに変換
//---------------------------------------------------------------------------
void PlayerAction::ChangeToMoveState()
{
    _pPlayerInfo->SetCharaState(ECharacterState::Move);
}
//---------------------------------------------------------------------------
//! 移動キー処理
//---------------------------------------------------------------------------
float3 PlayerAction::GetMoveDirection() const
{
    f32    magnitude = timer::DeltaTime() * _currentSpeed;                 // 移動距離
    float3 unitDir   = hlslpp::normalize(_pTransform->GetForwardAxis());   // ユニット
    float3 dir       = magnitude * unitDir;                                // 移動距離
    return dir;
}
//---------------------------------------------------------------------------
//! 回転方向の角度(Y軸)
//---------------------------------------------------------------------------
f32 PlayerAction::GetRotateAngle() const
{
    const raw_ptr<BaseCamera> currentCameraRef = manager::GetCurrentCameraMgr()->GetCurrentCamera();

    // Y軸回転角度計算
    f32 x = 0.0f;
    f32 z = 0.0f;

    // ゲームパッド処理
    if(input::IsGamePadConnected()) {
        x = input::GamePadMgr()->GetLeftThumbSticksX();
        z = input::GamePadMgr()->GetLeftThumbSticksY() * -1.0f;
    }

    // キーボード処理
    if(input::IsKeyDown(DirectX::Keyboard::W)) {
        z = -1.0f;
    }
    if(input::IsKeyDown(DirectX::Keyboard::S)) {
        z = 1.0f;
    }
    if(input::IsKeyDown(DirectX::Keyboard::A)) {
        x = -1.0f;
    }
    if(input::IsKeyDown(DirectX::Keyboard::D)) {
        x = 1.0f;
    }

    f32 angle = atan2f(x, z);

    return angle;
}
//---------------------------------------------------------------------------
//! ダメージ取得
//---------------------------------------------------------------------------
s32 PlayerAction::GetAttackDamage()
{
    PlayerInfo::AttackInfo attackInfo = _pPlayerInfo->GetAttackInfo(_pPlayerInfo->GetPlayerState());
    if(_pAnimator->GetCurrentAnimationTime() > attackInfo.inTime &&
       _pAnimator->GetCurrentAnimationTime() < attackInfo.outTime) {
        return attackInfo.damage;
    }
    return -1;
}
}   // namespace component
