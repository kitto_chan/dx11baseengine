﻿//---------------------------------------------------------------------------
//! @file	PlayerConller.cpp
//!	@brief	プレイヤーの入力処理のコンポーネント
//---------------------------------------------------------------------------
#pragma once
#include "component/Component.h"

namespace component {

class Transform;
class Animator;
class CapsuleZCollider;
class PlayerInfo;
class GhostBodyCharacter;
using namespace state;

class PlayerAction : public Component
{
    const std::string _NAME = "PlayerAction";

public:
    PlayerAction();    //!< コンストラクタ
    ~PlayerAction();   //!< デストラクタ

    s32 GetAttackDamage();   //!< 攻撃力を取得

    //! 反作用力を与える
    void ApplyBackwardImpulse(const float3& dir, f32 power);

    //! ダメージを与える
    void ApplyDamage(s32 damage);

private:
    //===========================================================================
    //! 継承関数
    //===========================================================================
    bool OnInit() override;          //!< 初期化
    void OnUpdate() override;        //!< 更新
    void OnRenderImgui() override;   //!< ImGui描画
    void OnFinalize() override;      //!< 解放

private:
    //===========================================================================
    //! private関数
    //===========================================================================
    void IdleState();     //!< 待機ステートの処理
    void MoveState();     //!< 移動ステートの処理
    void JumpState();     //!< ジャンプステートの処理
    void AttackState();   //!< 攻撃ステートの処理
    void RollState();     //!< ロールステートの処理
    void HitState();      //!< ヒットステートの処理(攻撃を受けた時)
    void DeathState();    //!< デスステートの処理

    bool IsPressingMovingKey() const;   //!< 移動のキー押してるかどうか

    bool HandleMoveKey(float3 dir);   //!< 移動キー処理
    bool HandleJumpKey();             //!< ジャンプキー処理
    bool HandleAttackKey();           //!< 攻撃キー処理
    bool HandleRollKey();             //!< ロールキー処理

    void ChangeToIdleState();    //!< 待機ステートに変更
    void ChangeToMoveState();    //!< 移動ステートに変更
    void ChangeToHitState();     //!< ヒットステートに変更
    void ChangeToDeathState();   //!< デスステートに変更

    float3 GetMoveDirection() const;   //!< 移動ベクトルを取得
    f32    GetRotateAngle() const;     //!< 回転方向の角度(Y軸)
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    f32 _walkSpeed    = 0.8f;   //!< 歩く速度
    f32 _runSpeed     = 2.0f;   //!< 走る速度
    f32 _currentSpeed = 0.0f;   //!< 現在の速度

    f32    _rotSpeed  = 1.5f;                   //!< 回転の速度
    float3 _jumpForce = { 0.0f, 5.0f, 0.0f };   //!< ジャンプ

    bool _isCombo = false;   // コンボ判定

    raw_ptr<Animator>           _pAnimator         = nullptr;   //!< Anmatorのコンポネント参照
    raw_ptr<Transform>          _pTransform        = nullptr;   //!< Transformのコンポーネント参照
    raw_ptr<CapsuleZCollider>   _pCapsuleZCollider = nullptr;   //!< CapsuleZColliderのコンポーネント参照
    raw_ptr<PlayerInfo>         _pPlayerInfo       = nullptr;   //!< PlayerInfoのコンポーネント参照
    raw_ptr<GhostBodyCharacter> _pGhostBodyChara   = nullptr;   //!< GhostBodyCharacterのコンポーネント参照
};
}   // namespace component
