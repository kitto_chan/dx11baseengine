﻿//---------------------------------------------------------------------------
//!	@file	PlayerWeaponAction.h
//!	@brief	プレイヤー武器の処理
//---------------------------------------------------------------------------
#include "PlayerWeaponAction.h"
#include "entity/Entity.h"
#include "entity/gameObject/GameObject.h"
#include "component/Transform.h"
#include "component/Animator.h"
#include "component/action/MonsterBearAction.h"
#include "component/physics/GhostBody.h"
#include "component/info/PlayerInfo.h"
#include "component/action/PlayerAction.h"
#include "component/effect/SwordTrail.h"
namespace component {
namespace {
}
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
PlayerWeaponAction::PlayerWeaponAction()
{
    _name = _NAME;
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool PlayerWeaponAction::OnInit()
{
    _slashingEffect = manager::EffekseerMgr()->Create(u"effekseer/Sword/sword.efk");

    //=======================================================================
    // 必要なコンポネントを揃えます
    //=======================================================================
    _pTransform = _pOwner->GetComponent<component::Transform>();
    _pGhostBody = _pOwner->GetComponent<component::GhostBody>();
    if(!(_pTransform && _pGhostBody)) {
        ASSERT_MESSAGE(_pTransform, "Transform Componentが見つけません");
        ASSERT_MESSAGE(_pGhostBody, "GhostBody Componentが見つけません");
        return false;
    }

    // この武器の持ち主を取得
    _player = static_cast<go::GameObject*>(_pOwner)->GetParent().get();

    // 物理エンジンでアクションを追加
    physics::PhysicsEngine::instance()->dynamicsWorld()->addAction(this);

    return true;
}

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void PlayerWeaponAction::OnUpdate()
{
    if(_player->GetComponent<component::PlayerInfo>()->IsAttackState()) {
        // 有効の攻撃時間
        s32 damage = _player->GetComponent<PlayerAction>()->GetAttackDamage();
        if(damage >= 0)
            _pOwner->GetComponent<component::SwordTrail>()->SetSwordTrailIsActive(true);
    }
    else {
        _pOwner->GetComponent<component::SwordTrail>()->SetSwordTrailIsActive(false);
    }
}

//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void PlayerWeaponAction::OnRender()
{
}

//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void PlayerWeaponAction::OnRenderImgui()
{
}

//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void PlayerWeaponAction::OnFinalize()
{
    physics::PhysicsEngine::instance()->dynamicsWorld()->removeAction(this);
}
//---------------------------------------------------------------------------
//! 当たり判定と行為
//---------------------------------------------------------------------------
void PlayerWeaponAction::updateAction([[maybe_unused]] btCollisionWorld* collisionWorld, [[maybe_unused]] btScalar deltaTimeStep)
{
    // 攻撃ステートではないと判定しないように
    if(!_player->GetComponent<PlayerInfo>()->IsCharaState(ECharacterState::Attack))
        return;

    // ダメージは-1になったらこの攻撃は無効になるので、判定しないように
    s32 damage = _player->GetComponent<PlayerAction>()->GetAttackDamage();
    if(damage == -1)
        return;

    // 物理のGhostObjectを取得
    btPairCachingGhostObject* pBtGhostObj = _pGhostBody->GetBtGhostBody().get();

    // より精確の当たり判定を使うかどうか
    // 不精確: Overlapping = AABB Box 重なったら判定する
    // 精確:   ContactPoint = 衝突点によっての当たり判定
    constexpr bool USE_OVERLAPPING = FALSE;
#if USE_OVERLAPPING
    //  Overlappingによっての当たり判定(不精確)
    s32 num = pBtGhostObj->getNumOverlappingObjects();
    for(int i = 0; i < num; ++i) {
        // 重なっているコリジョンオブジェクト（剛体など）
        btCollisionObject* obj   = pBtGhostObj->getOverlappingObject(i);
        Entity*            other = static_cast<entity::Entity*>(obj->getUserPointer());

        if(other->GetTag() == Tag::Enemey) {
            other->GetComponent<MonsterBearAction>()->ChangeToHitState(damage);
        }
    }
#else
    // ContactPointによっての当たり判定(精確)

    // 重ね合わせた1つのペア(OverlappingPair)のすべてのcontact manifoldsを取得する準備
    btManifoldArray manifoldArray;

    //  重ね合わせたペア(OverlappingPair)をすべて取得
    btBroadphasePairArray& pairArray = pBtGhostObj->getOverlappingPairCache()->getOverlappingPairArray();

    // 各ペアを調べていく
    for(int i = 0; i < pairArray.size(); i++) {
        manifoldArray.clear();

        const btBroadphasePair& pair = pairArray[i];

        //unless we manually perform collision detection on this pair, the contacts are in the dynamics world paircache:
        //The next line fetches the collision information for this Pair
        btBroadphasePair* collisionPair = collisionWorld->getPairCache()->findPair(pair.m_pProxy0, pair.m_pProxy1);
        if(!collisionPair) continue;

        // Read out the all contact manifolds for this Overlapping Pair
        if(collisionPair->m_algorithm) collisionPair->m_algorithm->getAllContactManifolds(manifoldArray);

        for(int j = 0; j < manifoldArray.size(); j++) {
            btPersistentManifold* manifold = manifoldArray[j];

            // どっちかほかのゲームオブジェクトか
            Entity* other = nullptr;
            if(manifold->getBody0() == pBtGhostObj) {
                other = static_cast<entity::Entity*>(manifold->getBody1()->getUserPointer());
            }
            else {
                other = static_cast<entity::Entity*>(manifold->getBody0()->getUserPointer());
            }

            // オブジェクト間の衝突点数
            for(int p = 0; p < manifold->getNumContacts(); p++) {
                const btManifoldPoint& pt = manifold->getContactPoint(p);
                // 衝突点間の距離がゼロ以下なら実際に衝突している
                if(pt.getDistance() < 0.f) {
                    // もし敵と当たったらダメージを与える
                    if(other->GetTag() == Tag::Enemey) {
                        float3 attackPos = math::CastToFloat3(pt.getPositionWorldOnA());
                        if(!other->GetComponent<MonsterBearAction>()) return;

                        if(other->GetComponent<MonsterBearAction>()->ChangeToHitState(damage)) {
                            // 斬撃エフェクト再生
                            manager::EffekseerMgr()->Play(_slashingEffect, attackPos);

                            // 当たったら一瞬スローモーションする(打撃感アップ)
                            other->GetComponent<component::Animator>()->SetAnimationSpeed(0.5f, 0.15f);
                            _player->GetComponent<component::Animator>()->SetAnimationSpeed(0.5f, 0.15f);
                        };
                    }
                    break;
                }
            }
        }
    }
#endif
}
}   // namespace component
