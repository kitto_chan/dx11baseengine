﻿//---------------------------------------------------------------------------
//!	@file	PlayerWeaponAction.h
//!	@brief	プレイヤー武器の処理
//---------------------------------------------------------------------------
#pragma once
#include "component/Component.h"
using namespace state;
#include "manager/EffekseerManager.h"

namespace entity::go {
class GameObject;
}

namespace component {
class PlayerAction;
class Animator;
class GhostBody;

class PlayerWeaponAction : public Component, btActionInterface
{
    const std::string _NAME = "PlayerWeaponAction";

public:
    PlayerWeaponAction();              //!< コンストラクタ
    ~PlayerWeaponAction() = default;   //!< デストラクタ

    //---------------------------------------------------------------------------
    // public 継承関数
    //---------------------------------------------------------------------------

    // Bulletから呼び出される関数
    virtual void updateAction([[maybe_unused]] btCollisionWorld* collisionWorld, [[maybe_unused]] btScalar deltaTimeStep) override;
    virtual void debugDraw([[maybe_unused]] btIDebugDraw* debugDrawer) override{};

public:
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    // public 変数
    //---------------------------------------------------------------------------

private:
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
    virtual bool OnInit() override;          //!< 初期化
    virtual void OnUpdate() override;        //!< 更新
    virtual void OnRender() override;        //!< 描画
    virtual void OnRenderImgui() override;   //!< ImGui描画
    virtual void OnFinalize() override;      //!< 解放

    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    raw_ptr<Transform> _pTransform = nullptr;
    raw_ptr<GhostBody> _pGhostBody = nullptr;

    gameobject::GameObject* _player;           //!<プレイやー(武器の持ち主)
    Effekseer::EffectRef    _slashingEffect;   //!< 斬撃エフェクト
};
}   // namespace component
