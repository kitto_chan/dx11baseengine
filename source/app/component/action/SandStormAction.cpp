﻿//---------------------------------------------------------------------------
//!	@file	MonsterBearWeaponAction.h
//!	@brief	MonsterBear武器の処理
//---------------------------------------------------------------------------
#include "SandStormAction.h"

#include "entity/Entity.h"
#include "entity/gameObject/GameObject.h"

#include "component/Transform.h"
#include "component/Animator.h"
#include "component/action/MonsterBearAction.h"
#include "component/info/MonsterBearInfo.h"
#include "component/physics/GhostBody.h"
#include "component/info/CharacterInfo.h"
#include "component/action/PlayerAction.h"

namespace component {
namespace {
}
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
SandStormAction::SandStormAction()
{
    _name = _NAME;
}

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool SandStormAction::OnInit()
{
    //=======================================================================
    // 必要なコンポネントを揃えます
    //=======================================================================
    _pTransform = _pOwner->GetComponent<component::Transform>();
    _pGhostBody = _pOwner->GetComponent<component::GhostBody>();
    if(!(_pTransform && _pGhostBody)) {
        ASSERT_MESSAGE(_pTransform, "Transform Componentが見つけません");
        ASSERT_MESSAGE(_pGhostBody, "GhostBody Componentが見つけません");
        return false;
    }

    physics::PhysicsEngine::instance()->dynamicsWorld()->addAction(this);

    return true;
}

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void SandStormAction::OnUpdate()
{
    if(!_pParent || !_pParent->GetComponent<CharacterInfo>()->IsAttackState()) {
        _pOwner->Release();
    }
}

//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void SandStormAction::OnRender()
{
}

//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void SandStormAction::OnRenderImgui()
{
}

//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void SandStormAction::OnFinalize()
{
    physics::PhysicsEngine::instance()->dynamicsWorld()->removeAction(this);
}
bool SandStormAction::BeforeCheck()
{
    if(!_pParent) {
        return false;
    }
    if(!_pParent->GetComponent<CharacterInfo>()) {
        ASSERT_MESSAGE("false", "キャラインフォがない");
        return false;
    }

    // 攻撃ステートであるかどうか
    if(!_pParent->GetComponent<CharacterInfo>()->IsCharaState(ECharacterState::Attack))
        return false;

    auto animationTime = _pParent->GetComponent<Animator>()->GetCurrentAnimationTime();

    auto attackInfo = _pParent->GetComponent<MonsterBearInfo>()
                          ->GetAttackInfo(MonsterBearInfo::EMonsterBearState::SandStormAttack);

    if(animationTime < attackInfo.inTime) {
        return false;
    }
    return true;
}

void SandStormAction::updateAction([[maybe_unused]] btCollisionWorld* collisionWorld, [[maybe_unused]] btScalar deltaTimeStep)
{
    if(!BeforeCheck()) return;

    // 物理のGhostObjectを取得
    btPairCachingGhostObject* pBtGhostObj = _pGhostBody->GetBtGhostBody().get();

    // より精確の当たり判定を使うかどうか
    // 不精確: Overlapping = AABB Box 重なったら判定する
    // 精確:   ContactPoint = 衝突点によっての当たり判定
    constexpr bool USE_OVERLAPPING = FALSE;
#if USE_OVERLAPPING
    //  Overlappingによっての当たり判定(不精確)
    s32 num = pBtGhostObj->getNumOverlappingObjects();
    for(int i = 0; i < num; ++i) {
        // 重なっているコリジョンオブジェクト（剛体など）
        btCollisionObject* obj   = pBtGhostObj->getOverlappingObject(i);
        Entity*            other = static_cast<entity::Entity*>(obj->getUserPointer());

        if(other->GetTag() == Tag::Enemey) {
            other->GetComponent<MonsterBearAction>()->ChangeToHitState(damage);
        }
    }
#else
    // ContactPointによっての当たり判定(精確)

    // 重ね合わせた1つのペア(OverlappingPair)のすべてのcontact manifoldsを取得する準備
    btManifoldArray manifoldArray;

    //  重ね合わせたペア(OverlappingPair)をすべて取得
    btBroadphasePairArray& pairArray = pBtGhostObj->getOverlappingPairCache()->getOverlappingPairArray();

    // 各ペアを調べていく
    for(int i = 0; i < pairArray.size(); i++) {
        manifoldArray.clear();

        const btBroadphasePair& pair = pairArray[i];

        //unless we manually perform collision detection on this pair, the contacts are in the dynamics world paircache:
        //The next line fetches the collision information for this Pair
        btBroadphasePair* collisionPair = collisionWorld->getPairCache()->findPair(pair.m_pProxy0, pair.m_pProxy1);
        if(!collisionPair) continue;

        // Read out the all contact manifolds for this Overlapping Pair
        if(collisionPair->m_algorithm) collisionPair->m_algorithm->getAllContactManifolds(manifoldArray);

        for(int j = 0; j < manifoldArray.size(); j++) {
            btPersistentManifold* manifold = manifoldArray[j];

            // どっちかほかのゲームオブジェクトか
            Entity* other = nullptr;
            if(manifold->getBody0() == pBtGhostObj) {
                other = static_cast<entity::Entity*>(manifold->getBody1()->getUserPointer());
            }
            else {
                other = static_cast<entity::Entity*>(manifold->getBody0()->getUserPointer());
            }

            // オブジェクト間の衝突点の数
            for(int p = 0; p < manifold->getNumContacts(); p++) {
                const btManifoldPoint& pt = manifold->getContactPoint(p);
                // 衝突点間の距離がゼロ以下なら実際に衝突している
                if(pt.getDistance() < 0.f) {
                    // もし敵と当たったらダメージを与える
                    if(other->GetTag() == Tag::Player) {
                        // 撃退のベクトルを求める
                        float3 impulseDir = _pOwner->GetComponent<Transform>()->GetBackwardAxis();

                        // Y軸は移動しないように
                        impulseDir.f32[1] = 0;

                        constexpr s32 IMPULSE_POWER = 3;    //!< 撃退の力
                        constexpr s32 DAMAGE        = 15;   //!< 与えるダメージ

                        other->GetComponent<PlayerAction>()->ApplyBackwardImpulse(impulseDir, IMPULSE_POWER);
                        other->GetComponent<PlayerAction>()->ApplyDamage(DAMAGE);
                    }
                    break;
                }
            }
        }
    }
#endif
}
}   // namespace component
