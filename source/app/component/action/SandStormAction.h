﻿//---------------------------------------------------------------------------
//!	@file	SandStormAction.h
//!	@brief	砂嵐の技処理
//---------------------------------------------------------------------------
#pragma once
#include "component/Component.h"
using namespace state;
namespace entity::go {
class GameObject;
}

namespace component {
class MonsterBearAction;
class Animator;
class GhostBody;

class SandStormAction : public Component, btActionInterface
{
    const std::string _NAME = "SandStormAction";

public:
    SandStormAction();              //!< コンストラクタ
    ~SandStormAction() = default;   //!< デストラクタ

    //---------------------------------------------------------------------------
    // public 継承関数
    //---------------------------------------------------------------------------

    // Bulletから呼び出される関数
    virtual void updateAction([[maybe_unused]] btCollisionWorld* collisionWorld, [[maybe_unused]] btScalar deltaTimeStep) override;
    virtual void debugDraw([[maybe_unused]] btIDebugDraw* debugDrawer) override{};

    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    void SetParent(raw_ptr<entity::Entity> parent) { _pParent = parent; };

    //---------------------------------------------------------------------------
    // public 変数
    //---------------------------------------------------------------------------
    bool BeforeCheck();

private:
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
    virtual bool OnInit() override;          //!< 初期化
    virtual void OnUpdate() override;        //!< 更新
    virtual void OnRender() override;        //!< 描画
    virtual void OnRenderImgui() override;   //!< ImGui描画
    virtual void OnFinalize() override;      //!< 解放

    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    raw_ptr<Transform> _pTransform = nullptr;
    raw_ptr<GhostBody> _pGhostBody = nullptr;

    raw_ptr<entity::Entity> _pParent; //!< 親
};
}   // namespace component
