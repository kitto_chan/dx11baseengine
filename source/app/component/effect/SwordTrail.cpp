﻿//---------------------------------------------------------------------------
//! @file	SwordTrail.cpp
//!	@brief	剣の軌跡
//---------------------------------------------------------------------------
#include "SwordTrail.h"

#include "component/Transform.h"
#include "component/ModelRenderer.h"
#include "Entity/Entity.h"
#include "effect/SwordTrailEffect.h"

namespace component {
namespace {
//!< TODO Assert Manager　現在playerしか使ってるので、先にこうします
//std::unordered_map<std::string, std::shared_ptr<Animation>> animationList;
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
SwordTrail::SwordTrail()
{
    _name             = NAME;
    _swordTrailEffect = std::make_unique<effect::SwordTrailEffect>();
}
//---------------------------------------------------------------------------
/// @brief コンストラクタ
/// @param headIndex 剣の先のモデルイデックス
/// @param tailIndex 剣の端のモデルイデックス
//---------------------------------------------------------------------------
SwordTrail::SwordTrail(s32 headIndex, s32 tailIndex)
{
    _name = NAME;
    _swordTrailEffect = std::make_unique<effect::SwordTrailEffect>();
    SwordHeadTailModelIndex(headIndex, tailIndex);
}
//---------------------------------------------------------------------------
//! デスストラクタ
//---------------------------------------------------------------------------
SwordTrail::~SwordTrail()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool SwordTrail::OnInit()
{
    //==========================================================
    // 必要なコンポーネントを取得
    //==========================================================
    _pTransform = _pOwner->GetComponent<Transform>();
    ASSERT_MESSAGE(_pTransform, "AnimatorはTransformは必要です");

    _pModelRenderer = _pOwner->GetComponent<ModelRenderer>();
    ASSERT_MESSAGE(_pTransform, "AnimatorはModelRendererは必要です");

    if(_swordTailModelIndex <= -1 || _swordHeadModelIndex <= -1) {
        ASSERT_MESSAGE("false", "剣の軌跡描画のため、剣の先と剣の端設定してください");
        return false;
    }

    _swordTrailEffect->Init();

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void SwordTrail::OnUpdate()
{
    matrix matrixHead = _pModelRenderer->GetVertexByIndex(_swordHeadModelIndex);
    matrix matrixTail = _pModelRenderer->GetVertexByIndex(_swordTailModelIndex);

    float3 currentHeadPos = matrixHead._41_42_43;
    float3 currentTailPos = matrixTail._41_42_43;
    _swordTrailEffect->SetPos(currentHeadPos, currentTailPos);

    _swordTrailEffect->Update();
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void SwordTrail::OnRender()
{
    _swordTrailEffect->Render();
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void SwordTrail::OnRenderImgui()
{
}
//---------------------------------------------------------------------------
//! 剣の先と端の座標設定
//---------------------------------------------------------------------------
void SwordTrail::SetPos(const float3& head, const float3& tail)
{
    _swordTrailEffect->SetPos(head, tail);
}
//---------------------------------------------------------------------------
//! 剣の軌跡のテクスチャーを設定
//! @param trailTexture テクスチャー
//---------------------------------------------------------------------------
void SwordTrail::SetTrailTexture(shr_ptr<gpu::Texture> trailTexture)
{
    _swordTrailEffect->SetTrailTexture(trailTexture);
}
//---------------------------------------------------------------------------
//! 剣の軌跡のテクスチャーを設定
//! @param　パス 画像のパス
//---------------------------------------------------------------------------
void SwordTrail::SetTrailTexture(std::string_view path)
{
    _swordTrailEffect->SetTrailTexture(path);
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void SwordTrail::SetSwordTrailIsActive(bool isActive)
{
    _swordTrailEffect->SetSwordTrailIsActive(isActive);
}
}   // namespace component
