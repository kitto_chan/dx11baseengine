﻿//---------------------------------------------------------------------------
//! @file	SwordTrail.h
//!	@brief	剣の軌跡
//---------------------------------------------------------------------------
#pragma once
#include "component/Component.h"

namespace effect {
class SwordTrailEffect;
}
namespace component {

class Transform;
class ModelRenderer;

class SwordTrail : public Component
{
    static constexpr char* NAME = "SwordTrail";

public:
    SwordTrail();   //!< コンストラクタ

    /// @brief コンストラクタ
    /// @param headIndex 剣の先のモデルイデックス
    /// @param tailIndex 剣の端のモデルイデックス
    SwordTrail(s32 headIndex, s32 tailIndex);

    ~SwordTrail();   //!< デストラクタ

private:
    //---------------------------------------------------------------------------
    //! 継承関数
    //---------------------------------------------------------------------------
    bool OnInit() override;          //!< 初期化
    void OnUpdate() override;        //!< 更新
    void OnRender() override;        //!< 描画
    void OnRenderImgui() override;   //!< ImGui描画
public:
    //---------------------------------------------------------------------------
    //! public 関数
    //---------------------------------------------------------------------------

    //! 剣の先と端の座標設定
    void SetPos(const float3& head, const float3& tail);

    //! 剣の軌跡のテクスチャーを設定
    //! @param trailTexture テクスチャー
    void SetTrailTexture(shr_ptr<gpu::Texture> trailTexture);

    //! 剣の軌跡のテクスチャーを設定
    //! @param　パス 画像のパス
    void SetTrailTexture(std::string_view path);

    // 剣の軌跡を描画かどうか
    void SetSwordTrailIsActive(bool isActive);

    //! 剣の先と端を設定
    void SwordHeadTailModelIndex(s32 headIndex, s32 tailIndex)
    {
        _swordHeadModelIndex = headIndex;
        _swordTailModelIndex = tailIndex;
    };

private:
    //---------------------------------------------------------------------------
    //! private関数
    //---------------------------------------------------------------------------
    s32 _swordHeadModelIndex = -1;   //!< モデルの剣の先のインデックス
    s32 _swordTailModelIndex = -1;   //!< モデルの剣の端のインデックス

    uni_ptr<effect::SwordTrailEffect> _swordTrailEffect;   //!< 剣の軌跡の実装クラス

    //---------------------------------------------------------------------------
    //! private 変数
    //---------------------------------------------------------------------------
    raw_ptr<component::Transform>     _pTransform;       //!< Transform コンポーネント 参照
    raw_ptr<component::ModelRenderer> _pModelRenderer;   //!< ModelRendererコンポーネント 参照
};
}   // namespace component
