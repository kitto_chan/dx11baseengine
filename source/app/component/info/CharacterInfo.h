﻿//---------------------------------------------------------------------------
//!	@file	CharacterInfo.h
//!	@brief	キャラクターのインフォメーション
//! @note	状態、ステートとか
//---------------------------------------------------------------------------
#pragma once
#include "component/Component.h"
using namespace state;

namespace component {
class Transform;
class Animator;

class CharacterInfo : public Component
{
    const std::string _NAME = "CharacterInfo";

public:
    CharacterInfo();              //!< コンストラクタ
    ~CharacterInfo() = default;   //!< デストラクタ

private:
    //---------------------------------------------------------------------------
    // private 継承関数
    //---------------------------------------------------------------------------

public:
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    ECharacterState GetCharaState() const { return _currentCharaState; }
    void            SetCharaState(const ECharacterState charaState) { _currentCharaState = charaState; }
    bool            IsCharaState(ECharacterState charaState) const { return charaState == _currentCharaState; }
    bool            IsAttackState() const { return _currentCharaState == ECharacterState::Attack; }

    bool IsSuperArmor() const;       //!< スパアマ状態にしてるかどうか
    void SetSuperArmor(bool flag);   //!< スパアマ状態に設定(フラッグ)
    void SetSuperArmor(s32 time);    //!< スパアマ状態を設定（時間）

    void SetMaxHP(s32 maxHP, bool setToCurrent = true);   //!< 最大HPを設定

    //! 現在のHp Rateを取得(0.f - 1.f);
    f32 GetCurrentHPRate() { return static_cast<f32>(_currentHP) / _maxHP; };

    //! 現在HPを加算する
    void AddCurrentHP(s32 value);

	//!< ヒットカウントを設定
	void SetHitCount(s32 hitCount) { _hitCount = hitCount; }

	//!< 攻撃を受けるか
	bool CanHit() const; 
    //---------------------------------------------------------------------------
    // public 変数
    //---------------------------------------------------------------------------
protected:
    //---------------------------------------------------------------------------
    // protected 関数
    //---------------------------------------------------------------------------
    virtual bool OnInit() override;          //!< 初期化
    virtual void OnUpdate() override;        //!< 更新
    virtual void OnRender() override;        //!< 描画
    virtual void OnRenderImgui() override;   //!< ImGui描画
    virtual void OnFinalize() override;      //!< 解放

    //---------------------------------------------------------------------------
    // protected 変数
    //---------------------------------------------------------------------------
    ECharacterState _currentCharaState = ECharacterState::Idle;

    s32  _hitCount        = 0;       //!< ヒットカウンター
    s32  _superArmorCount = -1;      //!< スパアマのカウンター
    bool _isSuperArmor    = false;   //!< スパアマのフラッグ

    s32 _maxHP     = 0;   //!< キャラの最大HP
    s32 _currentHP = 0;   //!< キャラ現在のHP

private:
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
};
}   // namespace component
