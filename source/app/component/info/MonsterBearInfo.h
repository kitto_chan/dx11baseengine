﻿//---------------------------------------------------------------------------
//!	@file	PlayerInfo.h
//!	@brief	プレイヤーが所有するステータスのみ
//---------------------------------------------------------------------------
#pragma once
#include "component/info/CharacterInfo.h"
using namespace state;

namespace component {
class Transform;
class Animator;

class MonsterBearInfo : public CharacterInfo
{
    const std::string _NAME = "MonsterBearInfo";

public:
    MonsterBearInfo();              //!< コンストラクタ
    ~MonsterBearInfo() = default;   //!< デストラクタ

    //! プレイヤーのステート
    enum class EMonsterBearState
    {
        Idle,   //!< 待機
        // ECharacterState: Move
        Walk,   //!< 歩く
        Run,    //!< 走る
        // ECharacterState: Attack
        Punch,             //!< 普通攻撃
        Swiping,           //!< 重攻撃
        JumpAttack,        //!< ジャンプアタック
        SandStormAttack,   //!< 砂技
		FireFlameAttact,       //!< ファイアーフレーム技
        // ECharacterState: Jump
        Hit
    };

    //!< 攻撃用のデータの構造体
    struct AttackInfo
    {
        f32 inTime;    //!< 攻撃有効フレーム時間(スタート)
        f32 outTime;   //!< 攻撃無効フレーム時間(エンド)
        s32 damage;    //!< アタックダメージ
    };

    //!< 攻撃データのリスト
    //!< @note TODO: ファイルから読み込む
    const std::map<EMonsterBearState, AttackInfo> ATTACK_INFO_LIST = {
        { EMonsterBearState::Swiping, { 1.25f, 1.5f, 10 } },
        { EMonsterBearState::Punch, { 0.25f, 0.4f, 20 } },
        { EMonsterBearState::SandStormAttack, {1.35f,0.0f ,10} } // only use inTime, special handing
    };

private:
    //---------------------------------------------------------------------------
    // private 継承関数
    //---------------------------------------------------------------------------
    virtual bool OnInit() override;   //!< 初期化

public:
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    EMonsterBearState GetActionState() const { return _currentActionState; }
    void              SetActionState(const EMonsterBearState actionState) { _currentActionState = actionState; }
    bool              IsActionState(EMonsterBearState actionState) const { return actionState == _currentActionState; }

    AttackInfo GetAttackInfo(EMonsterBearState actionState) { return ATTACK_INFO_LIST.at(actionState); }
    //---------------------------------------------------------------------------
    // public 変数
    //---------------------------------------------------------------------------
protected:
    //---------------------------------------------------------------------------
    // protected 関数
    //---------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    // protected 変数
    //---------------------------------------------------------------------------

private:
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    EMonsterBearState _currentActionState = EMonsterBearState::Idle;   //!< プレイヤーのステート
};
}   // namespace component
