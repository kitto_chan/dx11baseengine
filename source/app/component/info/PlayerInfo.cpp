﻿//---------------------------------------------------------------------------
//!	@file	CharacterInfo.h
//!	@brief	キャラクターのアクション処理
//---------------------------------------------------------------------------
#include "PlayerInfo.h"

#include "component/Transform.h"

#include "entity/Entity.h"
namespace component {
namespace {
}
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
PlayerInfo::PlayerInfo()
{
    _name = _NAME;
}

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool PlayerInfo::OnInit()
{
    return true;
}

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void PlayerInfo::OnUpdate()
{
    CharacterInfo::OnUpdate();
}

//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void PlayerInfo::OnRender()
{
}

//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void PlayerInfo::OnFinalize()
{
}
//---------------------------------------------------------------------------
//!< 死にましたか
//---------------------------------------------------------------------------
bool PlayerInfo::IsDeath()
{
    return _currentHP <= 0 || _pOwner->GetComponent<component::Transform>()->GetPosition().y <= -5;
}
}   // namespace component
