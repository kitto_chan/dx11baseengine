﻿//---------------------------------------------------------------------------
//!	@file	PlayerInfo.h
//!	@brief	プレイヤーが所有するステータスのみ
//---------------------------------------------------------------------------
#pragma once
#include "component/info/CharacterInfo.h"
using namespace state;

namespace component {
class Transform;
class Animator;

class PlayerInfo : public CharacterInfo
{
    const std::string _NAME = "PlayerInfo";

public:
    PlayerInfo();              //!< コンストラクタ
    ~PlayerInfo() = default;   //!< デストラクタ

    //! プレイヤーのステート
    enum class EPlayerState
    {
        Idle,   //!< 待機
        // ECharacterState: Move
        Walk,   //!< 歩く
        Run,    //!< 走る
        // ECharacterState: Attack
        Slash1,   //!< 普通攻撃コンボ-1
        Slash2,   //!< 普通攻撃コンボ-2
        Slash3,   //!< 普通攻撃コンボ-3
        // ECharacterState: Jump
        JumpIn,    //!< ジャンプ開始
        Jumping,   //!< ジャンプ中
        JumpOut,   //!< ジャンプ終了
        // ECharacterState: Roll
        Roll,   //!< ロール(回避)
        // ECharacterState: Hit
        Hit

    };

    //!< 攻撃用のデータの構造体
    struct AttackInfo
    {
        f32 inTime;    //!< 攻撃有効フレーム時間(スタート)
        f32 outTime;   //!< 攻撃無効フレーム時間(エンド)
        s32 damage;    //!< アタックダメージ
    };

    //!< 攻撃データのリスト
    //!< @note TODO: ファイルから読み込む
    const std::map<EPlayerState, AttackInfo> ATTACK_INFO_LIST = {
        { EPlayerState::Slash1, { 0.40f, 0.65f, 10 } },
        { EPlayerState::Slash2, { 0.70f, 0.95f, 20 } },
        { EPlayerState::Slash3, { 1.10f, 1.60f, 30 } },
    };

private:
    //---------------------------------------------------------------------------
    // private 継承関数
    //---------------------------------------------------------------------------
    virtual bool OnInit() override;       //!< 初期化
    virtual void OnUpdate() override;     //!< 更新
    virtual void OnRender() override;     //!< 描画
    virtual void OnFinalize() override;   //!< 解放

public:
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------

    //! プレイヤーステートを取得
    EPlayerState GetPlayerState() const { return _currentPlayerState; }
    //! プレイヤーステートを設定
    void SetPlayerState(const EPlayerState playerState) { _currentPlayerState = playerState; }
    //! プレイヤーステートをチェック
    bool IsPlayerState(EPlayerState playerState) const { return playerState == _currentPlayerState; }

    //!< 攻撃のデータを取得
    AttackInfo GetAttackInfo(EPlayerState playerState) { return ATTACK_INFO_LIST.at(playerState); }
    //---------------------------------------------------------------------------
    // public 変数
    //---------------------------------------------------------------------------
    bool IsDeath();   //!< 死にましたか

    //! 現在のMPを取得
    f32 GetCurrentMP() { return _currentMP; }
    //! MPを追加
    void AddCurrentMP(f32 value)
    {
        _currentMP += value;
        _currentMP = std::clamp(_currentMP, 0.0f, _maxMP);
    }
    //! MP足りるかどうか
    bool IsEnoughMP(f32 value)
    {
        return _currentMP >= value;
    }
    //! 現在のHp Rateを取得(0.f - 1.f);
    f32 GetCurrentMPRate() { return static_cast<f32>(_currentMP) / _maxMP; };

protected:
    //---------------------------------------------------------------------------
    // protected 関数
    //---------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    // protected 変数
    //---------------------------------------------------------------------------

private:
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
    f32 _maxMP     = 100.0f;   //!< キャラの最大MP
    f32 _currentMP = _maxMP;   //!< キャラ現在のMP
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    EPlayerState _currentPlayerState = EPlayerState::Idle;   //!< プレイヤーのステート
};
}   // namespace component
