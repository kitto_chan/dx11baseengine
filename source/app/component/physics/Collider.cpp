﻿//---------------------------------------------------------------------------
//!	@file	Collider.h
//!	@brief	すべてのコライダーの基盤となるクラスです。
//! @note	様々な形状のコリジョンを作成することができ、
//!			この形状を使って剛体の運動（衝突）やゲーム用判定を行う
//!---------------------------------------------------------------------------
#include "Collider.h"
#include "entity/Entity.h"
#include "component/ModelRenderer.h"

namespace component {
namespace {

}
Collider::Collider()
{
}
//===========================================================================
//! Collider
//! @brief すべてのコライダーの基盤となるクラスです。
//===========================================================================
Collider::Collider(const float3& offset)
: _offset(offset)
{
}

//===========================================================================
// Box Collider
//===========================================================================
void Collider::OnRenderImgui()
{
    //Offset
    {
        imgui::ImguiColumn2FormatBegin("Offset");
        imgui::ImguiDragXYZ("##offset", _offset);
        imgui::ImguiColumn2FormatEnd();
    }
}
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
BoxCollider::BoxCollider(const float3& size, const float3& offset)
: Collider(offset)
{
    _name        = "BoxCollider";
    _halfExtents = size;
}

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool BoxCollider::OnInit()
{
    _btShape = std::make_shared<btBoxShape>(btVector3(_halfExtents.x, _halfExtents.y, _halfExtents.z));
    return true;
}
//---------------------------------------------------------------------------
//! ImGui描画
//---------------------------------------------------------------------------
void BoxCollider::OnRenderImgui()
{
    imgui::ImguiDragXYZ(u8"サイズ", _halfExtents);
    /* btVector3 bv = math::CastToBtVec3(_halfExtents);
        _btShape->setLocalScaling(bv);*/
}

//===========================================================================
// Sphere Collider
//===========================================================================

//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
SphereCollider::SphereCollider(f32 radius, const float3& offset)
: Collider(offset)
{
    _name   = "SphereCollider";
    _radius = radius;
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool SphereCollider::OnInit()
{
    _btShape = std::make_shared<btSphereShape>(_radius);
    return true;
}
//---------------------------------------------------------------------------
//! ImGui描画
//---------------------------------------------------------------------------
void SphereCollider::OnRenderImgui()
{
    // 半径
    {
        imgui::ImguiColumn2FormatBegin("半径");
        ImGui::DragFloat("##半径", &_radius);
        imgui::ImguiColumn2FormatEnd();
    }
}

//===========================================================================
// Cylinder Collider
//===========================================================================
//---------------------------------------------------------------------------
//!< コンストラクタ
//---------------------------------------------------------------------------
CylinderCollider::CylinderCollider(const float3& scaler, const float3& offset)
: Collider(offset)
{
    _name   = "CylinderCollider";
    _scaler = scaler;
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool CylinderCollider::OnInit()
{
    math::CastToBtVec3(_scaler);
    _btShape = std::make_shared<btCylinderShape>(math::CastToBtVec3(_scaler));
    return true;
}
//---------------------------------------------------------------------------
//! ImGui描画
//---------------------------------------------------------------------------
void CylinderCollider::OnRenderImgui()
{
    imgui::ImguiDragXYZ("scaler", _scaler);
}
//===========================================================================
// CapsuleZ Collider
//===========================================================================
//---------------------------------------------------------------------------
//!< コンストラクタ
//---------------------------------------------------------------------------
CapsuleZCollider::CapsuleZCollider(f32 radius, f32 height, const float3& offset)
: Collider(offset)
{
    _name   = "CapsuleZCollider";
    _radius = radius;
    _height = height;
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool CapsuleZCollider::OnInit()
{
    _btShape = std::make_shared<btCapsuleShapeZ>(_radius, _height);
    return true;
}
//---------------------------------------------------------------------------
//! ImGui描画
//---------------------------------------------------------------------------
void CapsuleZCollider::OnRenderImgui()
{
    // 半径
    {
        imgui::ImguiColumn2FormatBegin(u8"半径");
        ImGui::DragFloat(u8"##半径", &_radius);
        imgui::ImguiColumn2FormatEnd();
    }
    // 高さ
    {
        imgui::ImguiColumn2FormatBegin(u8"高さ");
        ImGui::DragFloat(u8"##高さ", &_height);
        imgui::ImguiColumn2FormatEnd();
    }
}
//===========================================================================
// Mesh Collider
//===========================================================================
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
MeshCollider::MeshCollider(const float3& offset)
: Collider(offset)
{
    _name = "MeshCollider";
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool MeshCollider::OnInit()
{
    _pModelRenderer = _pOwner->GetComponent<ModelRenderer>();
    if(!_pModelRenderer) {
        ASSERT_MESSAGE(false, "MeshColliderはModelRendererが必要だ!");
        return false;
    }
    raw_ptr model = _pModelRenderer->GetModel();

    std::vector<u32> _triangleIndices;   //!< TriangleMesh用インデックス配列
    _triangles = std::make_unique<btTriangleIndexVertexArray>();
    //-----------------------------------------------------------------------
    // インデックス配列を生成
    //-----------------------------------------------------------------------
    {
        const u32 triangleCount = model->vertexCount() / 3;

        _triangleIndices.reserve(triangleCount * 3);
        for(u32 i = 0; i < triangleCount; ++i) {
            _triangleIndices.push_back(i * 3 + 0);
            _triangleIndices.push_back(i * 3 + 1);
            _triangleIndices.push_back(i * 3 + 2);
        }

        // メッシュ情報を登録
        btIndexedMesh part;

        part.m_vertexBase          = reinterpret_cast<const unsigned char*>(model->vertices());   // 頂点配列の先頭
        part.m_vertexStride        = sizeof(float3);                                              // 1頂点あたりの間隔
        part.m_numVertices         = model->vertexCount();                                        // 頂点数
        part.m_triangleIndexBase   = reinterpret_cast<const unsigned char*>(model->indices());    // 三角形インデックス配列の先頭
        part.m_triangleIndexStride = sizeof(u32) * 3;                                             // 三角形インデックス1個あたりの間隔
        part.m_numTriangles        = model->indexCount() / 3;                                     // 三角形数
        part.m_indexType           = PHY_INTEGER;                                                 // インデックスはu32

        _triangles->addIndexedMesh(part, PHY_INTEGER);
    }

    //-----------------------------------------------------------------------
    // シェイプの生成
    //-----------------------------------------------------------------------
    constexpr bool useQuantizedAabbCompression = true;

    _btShape = std::make_shared<btBvhTriangleMeshShape>(_triangles.get(), useQuantizedAabbCompression);
    return true;
}
//---------------------------------------------------------------------------
//! ImGui描画
//---------------------------------------------------------------------------
void MeshCollider::OnRenderImgui()
{
}
}   // namespace component
