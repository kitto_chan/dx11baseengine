﻿//---------------------------------------------------------------------------
//!	@file	Collider.h
//!	@brief	すべてのコライダー形状の定義
//---------------------------------------------------------------------------
#pragma once
#include "component/Component.h"

namespace component {
//===========================================================================
//! すべてのコライダーの基盤となるクラスです。
//===========================================================================
class Collider : public Component
{
public:
    Collider();                                                  //!< コンストラクタ
    Collider(const float3& offset = float3(0.0f, 0.0f, 0.0f));   //!< コンストラクタ
    virtual ~Collider() = default;                               //!< デストラクタ

    //! コライダー形状を取得
    virtual shr_ptr<btCollisionShape> GetBtCollisionShape() const { return _btShape; };

    virtual float3 GetOffset() const { return _offset; };                  //!< コライダーのオフセットを取得
    virtual void   SetOffset(const float3& offset) { _offset = offset; }   //!< コライダーのオフセットをセット

protected:
    virtual void OnRenderImgui() override;   //!< Imguiの描画

    shr_ptr<btCollisionShape> _btShape = nullptr;                //!< bulletシェープ
    float3                    _offset  = { 0.0f, 0.0f, 0.0f };   //!< オブジェクトのローカル空間でのコライダーの位置
};

//===========================================================================
//! ボックスコライダーの基盤となるクラスです。
//===========================================================================
class BoxCollider final : public Collider
{
public:
    BoxCollider() = delete;                                                             //!< コンストラクタ
    BoxCollider(const float3& size, const float3& offset = float3(0.0f, 0.0f, 0.0f));   //!< コンストラクタ
    ~BoxCollider() = default;                                                           //!< デストラクタ
private:
    bool OnInit() override;          //!< 初期化
    void OnRenderImgui() override;   //!< ImGui描画
private:
    float3 _halfExtents = 0.0f;   //!< ボックスの辺長
};
//===========================================================================
//!　球コライダーの基盤となるクラスです。
//===========================================================================
class SphereCollider final : public Collider
{
public:
    SphereCollider() = delete;                                                     //!< コンストラクタ
    SphereCollider(f32 radius, const float3& offset = float3(0.0f, 0.0f, 0.0f));   //!< コンストラクタ
    ~SphereCollider() = default;                                                   //!< デストラクタ

private:
    bool OnInit() override;          //!< 初期化
    void OnRenderImgui() override;   //!< ImGui描画
private:
    f32 _radius = 0.0f;   //!< 半径
};
//===========================================================================
//! 円筒コライダーの基盤となるクラスです。
//===========================================================================
class CylinderCollider final : public Collider
{
public:
    CylinderCollider() = delete;                                                               //!< コンストラクタ
    CylinderCollider(const float3& scaler, const float3& offset = float3(0.0f, 0.0f, 0.0f));   //!< コンストラクタ
    ~CylinderCollider() = default;                                                             //!< デストラクタ

private:
    bool OnInit() override;          //!< 初期化
    void OnRenderImgui() override;   //!< ImGui描画
private:
    float3 _scaler = { 1.0f, 1.0f, 1.0f };
};
//===========================================================================
//! カプセルZ軸コライダーの基盤となるクラスです。
//===========================================================================
class CapsuleZCollider final : public Collider
{
public:
    CapsuleZCollider() = delete;                                                                 //!< コンストラクタ
    CapsuleZCollider(f32 radius, f32 height, const float3& offset = float3(0.0f, 0.0f, 0.0f));   //!< コンストラクタ
    ~CapsuleZCollider() = default;                                                               //!< デストラクタ
private:
    bool OnInit() override;          //!< 初期化
    void OnRenderImgui() override;   //!< ImGui描画
private:
    f32 _radius = 0.0f;   //!< 半径
    f32 _height = 0.0f;   //!< 高さの半分
};
//===========================================================================
//! メッシュコライダー(モデルと同じ形のコライダー)の基盤となるクラスです。
//===========================================================================
class ModelRenderer;
class MeshCollider final : public Collider
{
public:
    MeshCollider(const float3& offset = float3(0.0f, 0.0f, 0.0f));   //!< コンストラクタ
    ~MeshCollider() = default;                                       //!< デストラクタ

private:
    bool OnInit() override;          //!< 初期化
    void OnRenderImgui() override;   //!< ImGui描画
private:
    raw_ptr<ModelRenderer> _pModelRenderer;   //!< ModelRendererを参照

    // 三角形メッシュのために使用中はメモリを保持しておく必要がある
    uni_ptr<btTriangleIndexVertexArray> _triangles;   //!< 三角形リスト
};

}   // namespace component
