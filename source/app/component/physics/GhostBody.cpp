﻿//---------------------------------------------------------------------------
//!	@file	GhostBody.cpp
//!	@brief	剛体などの他のコリジョンオブジェクトから影響を受けず、衝突状態だけを検出する
//---------------------------------------------------------------------------
#include "GhostBody.h"
#include "Collider.h"
#include "component/Transform.h"
#include "entity/Entity.h"

namespace component {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
GhostBody::GhostBody()
{
    _name = _NAME;
}
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
GhostBody::~GhostBody()
{
}

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool GhostBody::OnInit()
{
    //==========================================================
    // 必要なコンポーネントを取得
    //==========================================================
    _pTransform = _pOwner->GetComponent<Transform>();
    _pCollider  = _pOwner->GetComponent<Collider>();

    if(!(_pTransform && _pCollider)) {
        ASSERT_MESSAGE(_pTransform, "RigidBodyはTransformが必要です");
        ASSERT_MESSAGE(_pCollider, "RigidBodyはColliderが必要です");
        return false;
    }
    //==========================================================
    // ゴーストオブジェクトを設定する
    //==========================================================
    _pGhostbody = std::make_unique<btPairCachingGhostObject>();
    // 形状設定
    _pGhostbody->setCollisionShape(_pCollider->GetBtCollisionShape().get());

    // 他のオブジェクトへの反応なし
    // CF_NO_CONTACT_RESPONSEを指定しないと剛体と衝突する
    _pGhostbody->setCollisionFlags(_pGhostbody->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE);

    // 位置設定
    SetWorldTransform(mul(_defaultMatrix, _pTransform->GetLocalToWorldMatrix()));

    // CCD (Continuous Collision Detection) 有効
    // 当たり抜け発生を抑制
    _pGhostbody->setCcdMotionThreshold(1.0f);
    _pGhostbody->setCcdSweptSphereRadius(0.2f);

    // デバッグ描画色
    _pGhostbody->setCustomDebugColor({ 1.0f, 0.0f, 0.0f });

    // 当たり判定のCallBackを追加
    physics::PhysicsEngine::instance()->dynamicsWorld()->getBroadphase()->getOverlappingPairCache()->setInternalGhostPairCallback(new btGhostPairCallback());

    // 自定義のユーザーポインタを設定
    _pGhostbody->setUserPointer(_pOwner);

    // 物理ワールドに追加
    entity::Layer layer          = _pOwner->GetLayer();
    s32           collisionGroup = physics::GetCollisionGroup(layer);
    physics::PhysicsEngine::instance()->dynamicsWorld()->addCollisionObject(_pGhostbody.get(), layer, collisionGroup);

    return true;
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void GhostBody::OnRenderImgui()
{
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void GhostBody::OnUpdate()
{
    // 位置設定
    SetWorldTransform(mul(_defaultMatrix, _pTransform->GetLocalToWorldMatrix()));
}
//---------------------------------------------------------------------------
//! レイト更新
//---------------------------------------------------------------------------
void GhostBody::OnLateUpdate()
{
    //// 位置設定
    //SetWorldTransform(mul(_defaultMatrix, _pTransform->GetLocalToWorldMatrix()));
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void GhostBody::OnFinalize()
{
    if(_pGhostbody.get()) {
        auto* dynamicsWorld = physics::PhysicsEngine::instance()->dynamicsWorld();
        // ワールドから登録解除
        dynamicsWorld->removeCollisionObject(_pGhostbody.get());
    }
    _pGhostbody.reset();
}
//---------------------------------------------------------------------------
//!< ワールド座標を設定
//---------------------------------------------------------------------------
void GhostBody::SetWorldTransform(const matrix& worldMatrix)
{
    float3 offset    = _pCollider->GetOffset();
    matrix newMatrix = mul(math::translate(offset), worldMatrix);

    Transform trans;
    trans.DecomposeMatrix(newMatrix);
    trans.SetScale(float3(1.0f, 1.0f, 1.0f));

    // 位置設定
    btTransform btTrans = math::CastToBtTrans(trans.GetLocalMatrix());
    _pGhostbody->setWorldTransform(btTrans);
}
}   // namespace component
