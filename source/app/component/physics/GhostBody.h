﻿//---------------------------------------------------------------------------
//!	@file	GhostBody.cpp
//!	@brief	剛体などの他のコリジョンオブジェクトから影響を受けず、衝突状態だけを検出する
//---------------------------------------------------------------------------
#pragma once
#include "component/Component.h"
//--------------------------------------------------------------
// Bullet Physics SDK
//--------------------------------------------------------------
#pragma warning(push)
#pragma warning(disable : 26495)
#include <BulletDynamics/Character/btKinematicCharacterController.h>
#include <BulletCollision/CollisionDispatch/btGhostObject.h>
#pragma warning(pop)

namespace component {
class Collider;
class Transform;

class GhostBody : public Component
{
    const std::string _NAME = "GhostBody";

public:
    GhostBody();    //!< コンストラクタ
    ~GhostBody();   //!< デストラクタ
protected:
    //---------------------------------------------------------------------------
    // 継承関数
    //---------------------------------------------------------------------------
    bool OnInit();          //!< 初期化
    void OnRenderImgui();   //!< ImGui描画
    void OnUpdate();        //!< 更新
    void OnLateUpdate();    //!< レイト更新
    void OnFinalize();      //!< 解放

public:
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    //!< ワールド座標を設定
    void SetWorldTransform(const matrix& worldMatrix);

    raw_ptr<btPairCachingGhostObject> GetBtGhostBody() const { return _pGhostbody.get(); }

	void SetDefaultMatrix(const matrix& m) { _defaultMatrix = m; };

	matrix GetWorldMatrix() const { return math::CastToMatrix(_pGhostbody->getWorldTransform()); };
    //---------------------------------------------------------------------------
    // public 変数
    //---------------------------------------------------------------------------

private:
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
protected:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    raw_ptr<Transform> _pTransform = nullptr;   //!< TransformComponentの参照
    raw_ptr<Collider>  _pCollider  = nullptr;   //!< ColliderComponentの参照

    uni_ptr<btPairCachingGhostObject> _pGhostbody;   //!< Bullet physicsのゴーストオブジェクト

	matrix _defaultMatrix = math::identity(); //!< デフォルトこのゴーストを持てる行列
};
}   // namespace component
