﻿//---------------------------------------------------------------------------
//!	@file	GhostBodyCharacter.cpp
//!	@brief	剛体などの他のコリジョンオブジェクトから影響を受けず、衝突状態だけを検出する
//---------------------------------------------------------------------------
#include "GhostBodyCharacter.h"
#include "Collider.h"
#include "component/Transform.h"
#include "component/ModelRenderer.h"
#include "entity/Entity.h"

namespace component {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
GhostBodyCharacter::GhostBodyCharacter()
{
    _name = _NAME;
}
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
GhostBodyCharacter::~GhostBodyCharacter()
{
}

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool GhostBodyCharacter::OnInit()
{
    GhostBody::OnInit();
    //==========================================================
    // 必要なコンポーネントを取得
    //==========================================================
    _pCapsuleZCollider = _pOwner->GetComponent<CapsuleZCollider>();

    if(!(_pTransform && _pCollider && _pCapsuleZCollider)) {
        ASSERT_MESSAGE(_pCapsuleZCollider, "CharacterGhostBodyはCapsuleZColliderが必要です");
        return false;
    }
    //==========================================================
    // キネマティックキャラクター作成
    //==========================================================
    {
        _pGhostbody->setCollisionFlags(btCollisionObject::CF_CHARACTER_OBJECT | btCollisionObject::CF_KINEMATIC_OBJECT);   // キャラクターオブ

        auto* dynamicsWorld = physics::PhysicsEngine::instance()->dynamicsWorld();

        shr_ptr<btCollisionShape> collisionShape = _pCapsuleZCollider->GetBtCollisionShape();

        shr_ptr<btCapsuleShapeZ> pbtCapsuleShapeZ = std::dynamic_pointer_cast<btCapsuleShapeZ>(collisionShape);
        ASSERT_MESSAGE(pbtCapsuleShapeZ, "現在このシェープはサポートしてない");
        btCollisionShape_ = std::move(pbtCapsuleShapeZ);

        constexpr f32 stepHeight = 0.1f;   // 地面に張り付く判定になる高さ 0.1m以内

        characterController_ = std::make_unique<btKinematicCharacterController>(_pGhostbody.get(), btCollisionShape_.get(), stepHeight);

        // パラメーター設定
        characterController_->setGravity(dynamicsWorld->getGravity());   // 重力
        characterController_->setMaxPenetrationDepth(0.0f);

        // キャラクターをワールドに登録
        dynamicsWorld->addCharacter(characterController_.get());
    }

    return true;
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void GhostBodyCharacter::OnRenderImgui()
{
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void GhostBodyCharacter::OnUpdate()
{
    //物理のポジションとコンポーネントの位置を統一する
    float3 phyTrans = math::makeTranslation(GetWorldMatrix());

    phyTrans -= _pCapsuleZCollider->GetOffset();
    _pTransform->SetPosition(phyTrans);
}
//---------------------------------------------------------------------------
//! レイト更新
//---------------------------------------------------------------------------
void GhostBodyCharacter::OnLateUpdate()
{
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void GhostBodyCharacter::OnFinalize()
{
    GhostBody::OnFinalize();
    auto* dynamicsWorld = physics::PhysicsEngine::instance()->dynamicsWorld();

    // ワールドから登録解除
    dynamicsWorld->removeCharacter(characterController_.get());
}
}   // namespace component
