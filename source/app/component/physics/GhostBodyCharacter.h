﻿//---------------------------------------------------------------------------
//!	@file	GhostBodyCharacter.cpp
//!	@brief	キャラクター用の当たり判定処理クラス
//---------------------------------------------------------------------------
#pragma once
#include "component/Component.h"
#include "GhostBody.h"

namespace component {
class Collider;
class Transform;
class CapsuleZCollider;

class GhostBodyCharacter : public GhostBody
{
    const std::string _NAME = "GhostBodyCharacter";

public:
    GhostBodyCharacter();    //!< コンストラクタ
    ~GhostBodyCharacter();   //!< デストラクタ
private:
    //---------------------------------------------------------------------------
    // 継承関数
    //---------------------------------------------------------------------------
    bool OnInit();          //!< 初期化
    void OnRenderImgui();   //!< ImGui描画
    void OnUpdate();        //!< 更新
    void OnLateUpdate();    //!< レイト更新
    void OnFinalize();      //!< 解放

public:
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    //----------------------------------------------------------
    //! @name   更新
    //----------------------------------------------------------
    //@{

    //! 移動
    void walk(const float3& walkDirection) { characterController_->setWalkDirection(math::CastToBtVec3(walkDirection)); }

    //! 停止移動
     void stop()  { characterController_->setWalkDirection(math::CastToBtVec3(float3(0.0f, 0.0f, 0.0f))); }

    //! ワープ(座標の設定)
     void warp(const float3& position) { characterController_->warp(math::CastToBtVec3(position)); }

    //! ジャンプ
     void jump(const float3& v = float3(0.0f, 0.0f, 0.0f)) { characterController_->jump(math::CastToBtVec3(v)); }

    //! 撃力を与える
     void applyImpulse(const float3& v) { characterController_->applyImpulse(math::CastToBtVec3(v)); }

    //@}
    //----------------------------------------------------------
    //! @name   設定
    //----------------------------------------------------------
    //@{

    //! 角速度の設定
     void setAngularVelocity(const float3& velocity) { characterController_->setAngularVelocity(math::CastToBtVec3(velocity)); }

    //! 速度の設定
     void setLinearVelocity(const float3& velocity) { characterController_->setLinearVelocity(math::CastToBtVec3(velocity)); }

    //! 角速度のダンパー設定
     void setAngularDamping(f32 d)  { characterController_->setAngularDamping(d); }

    //! 速度のダンパー設定
     void setLinearDamping(f32 d)  { characterController_->setLinearDamping(d); }

    //! 落下速度制限設定
     void setFallSpeed(f32 fallSpeed)  { characterController_->setFallSpeed(fallSpeed); }

    //! ジャンプ速度(上昇速度)制限設定
     void setJumpSpeed(f32 jumpSpeed)  { characterController_->setJumpSpeed(jumpSpeed); }

    //! 重力設定
     void setGravity(const float3& gravity) { characterController_->setGravity(math::CastToBtVec3(gravity)); }

    //! 斜面の最大傾斜角設定
    //! @note この指定角度以上の斜面は滑り落ちるようになります (初期値:45°)
     void setMaxSlope(f32 slopeDegree = 45.0f)  { characterController_->setMaxSlope(btRadians(slopeDegree)); }

    //@}
    //----------------------------------------------------------
    //! @name   参照
    //----------------------------------------------------------
    //@{

    //! 角速度取得
     const float3 getAngularVelocity() const { return math::CastToFloat3(characterController_->getAngularVelocity()); }

    //! 速度取得
     float3 getLinearVelocity() const { return math::CastToFloat3(characterController_->getLinearVelocity()); }

    //! 角速度のダンパー取得
     f32 getAngularDamping() const  { return characterController_->getAngularDamping(); }

    //! 速度のダンパー取得
     f32 getLinearDamping() const  { return characterController_->getLinearDamping(); }

    //! 落下速度制限取得
     f32 getFallSpeed() const  { return characterController_->getFallSpeed(); }

    //! ジャンプ速度(上昇速度)制限取得
     f32 getJumpSpeed() const  { return characterController_->getJumpSpeed(); }

    //! 重力取得
     float3 getGravity() const { return math::CastToFloat3(characterController_->getGravity()); }

    //! 斜面の最大傾斜角取得
     f32 getMaxSlope() const  { return characterController_->getMaxSlope(); }

    //! ジャンプ可能かどうか
     bool canJump() const  { return characterController_->canJump(); }

    //! 地面に接地しているかどうか
     bool onGround() const  { return characterController_->onGround(); }

    //! Bullet ゴーストオブジェクトを取得
    //! @note キネマティックキャラクターに対応するコリジョンオブジェクト
     btPairCachingGhostObject* ghostObject() const  { return _pGhostbody.get(); }

    //! Bullet キャラクターコントローラーを取得
     btKinematicCharacterController* characterController() const  { return characterController_.get(); }

    //@}
    //---------------------------------------------------------------------------
    // public 変数
    //---------------------------------------------------------------------------

private:
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
private:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    std::shared_ptr<btConvexShape>                  btCollisionShape_;      //!< Bullet 凸コリジョンシェイプ
    std::unique_ptr<btKinematicCharacterController> characterController_;   //!< Bullet キャラクターコントローラー

    raw_ptr<CapsuleZCollider> _pCapsuleZCollider = nullptr;   //!< CapsuleZColliderコンポーネント参照
};
}   // namespace component
