﻿//---------------------------------------------------------------------------
//!	@file	RigidBody.cpp
//!	@brief	ゲームオブジェクト を物理特性によって制御する事ができるようになります。\n
//!			BulletPhysics物理エンジンを通して他のオブジェクトと相互作用させるためには、\n
//!			ゲームオブジェクトにリジッドボディを加える必要があります。
//---------------------------------------------------------------------------
#include "RigidBody.h"
#include "component/Transform.h"
#include "component/ModelRenderer.h"
#include "Collider.h"

#include "entity/Entity.h"

namespace component {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
RigidBody::RigidBody()
{
    _name = _NAME;
}
//---------------------------------------------------------------------------
//! コンストラクタ
//! @param	[in]	mass 質量
//---------------------------------------------------------------------------
RigidBody::RigidBody(f32 mass, bool isKinematic)
{
    _name        = _NAME;
    _mass        = mass;
    _isKinematic = isKinematic;
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
RigidBody::~RigidBody()
{
    _btRigidBody.reset();
}

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool RigidBody::OnInit()
{
    //==========================================================
    // 必要なコンポーネントを取得
    //==========================================================
    _pTransform = _pOwner->GetComponent<Transform>();
    _pCollider  = _pOwner->GetComponent<Collider>();

    if(!(_pTransform && _pCollider)) {
        ASSERT_MESSAGE(_pTransform, "RigidBodyはTransformが必要です");
        ASSERT_MESSAGE(_pCollider, "RigidBodyはColliderが必要です");
        return false;
    }
    //----------------------------------------------------------
    // リジッドボディを生成
    //----------------------------------------------------------
    {
        //コリションの形状を取得
        raw_ptr<btCollisionShape> pBtShape = _pOwner->GetComponent<Collider>()->GetBtCollisionShape();

        //!< ワールド座標を設定
        Transform trans(*_pTransform.get());
        trans.SetScale(float3(1.0f, 1.0f, 1.0f));
        btTransform transform = math::CastToBtTrans(trans.GetLocalToWorldMatrix());

        btAssert((!pBtShape || pBtShape->getShapeType() != INVALID_SHAPE_PROXYTYPE));

        // シェイプと質量から慣性モーメントを自動計算
        bool      isDynamic = (_mass != 0.0f);      // 動的オブジェクトかどうか
        btVector3 localInertia(0.0f, 0.0f, 0.0f);   // 慣性モーメント
        if(isDynamic) {
            pBtShape->calculateLocalInertia(_mass, localInertia);
        }

        //----------------------------------------------------------
        // MotionState作成
        //----------------------------------------------------------
        // オブジェクトのワールド変換行列をプログラムに受け渡すインターフェース
        _btMotionState = std::make_unique<btDefaultMotionState>(transform);

        //----------------------------------------------------------
        // 剛体の作成
        //----------------------------------------------------------
        btRigidBody::btRigidBodyConstructionInfo info(_mass, _btMotionState.get(), pBtShape.get(), localInertia);
        _btRigidBody = std::make_unique<btRigidBody>(info);

        // CCD (Continuous Collision Detection) 有効
        // 当たり抜け発生を抑制
        _btRigidBody->setCcdMotionThreshold(1.0f);
        _btRigidBody->setCcdSweptSphereRadius(0.2f);

        _btRigidBody->setFriction(0.5f);      // 摩擦係数
        _btRigidBody->setRestitution(0.6f);   // 跳ね返り係数

        _btRigidBody->setUserIndex(-1);

        _btRigidBody->setUserPointer(_pOwner);

        // 剛体をワールドに登録
        physics::PhysicsEngine::instance()->registerRigidBody(_btRigidBody.get(), _pOwner->GetLayer());

        // キネマティックを設定
        if(_isKinematic) {
            SetKinematic();
        }
    }

    return true;
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void RigidBody::OnRenderImgui()
{
    //! Mass
    {
        imgui::ImguiColumn2FormatBegin("Mass");
        ImGui::Text("%d", _mass);
        imgui::ImguiColumn2FormatEnd();
    }
    //! Kinematic
    {
        imgui::ImguiColumn2FormatBegin("Kinematic");
        ImGui::Checkbox("##Kinematic", &_isKinematic);
        imgui::ImguiColumn2FormatEnd();
    }
}
//---------------------------------------------------------------------------
//!更新
//---------------------------------------------------------------------------
void RigidBody::OnUpdate()
{
    if(_isKinematic) {
        _btRigidBody->setWorldTransform(math::CastToBtTrans(_pTransform->GetLocalToWorldMatrix()));
    }
}

//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void RigidBody::OnFinalize()
{
    Remove();
}

//---------------------------------------------------------------------------
//!< キネマティックを設定
//---------------------------------------------------------------------------
void RigidBody::SetKinematic()
{
    _btRigidBody->setCollisionFlags(_btRigidBody->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT);
    _btRigidBody->setActivationState(DISABLE_DEACTIVATION);
    _isKinematic = true;
}
//---------------------------------------------------------------------------
//! 剛体を削除
//---------------------------------------------------------------------------
void RigidBody::Remove()
{
    if(_btRigidBody) {
        physics::PhysicsEngine::instance()->unregisterRigidBody(_btRigidBody.get());
        _btRigidBody.reset();
    }
}
}   // namespace component
