﻿//---------------------------------------------------------------------------
//!	@file	RigidBody.h
//!	@brief	ゲームオブジェクト を物理特性によって制御する事ができるようになります。\n
//!			BulletPhysics物理エンジンを通して他のオブジェクトと相互作用させるためには、\n
//!			ゲームオブジェクトにリジッドボディを加える必要があります。
//---------------------------------------------------------------------------
#pragma once
#include "component/Component.h"

namespace component {
class Collider;
class Transform;

class RigidBody : public Component
{
    const std::string _NAME = "RigidBody";

public:
    RigidBody();                                     //!< コンストラクタ
    RigidBody(f32 mass, bool isKinematic = false);   //!< コンストラクタ
    ~RigidBody();                                    //!< デストラクタ

private:
    //---------------------------------------------------------------------------
    // 継承関数
    //---------------------------------------------------------------------------
    bool OnInit();          //!< 初期化
    void OnRenderImgui();   //!< ImGui描画
    void OnUpdate();        //!< 更新
    void OnFinalize();      //!< 解放

public:
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    void SetKinematic();   //!< キネマティックを設定
    void Remove();         //!< 剛体を削除

    //---------------------------------------------------------------------------
    // public 変数
    //---------------------------------------------------------------------------

private:
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
private:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    raw_ptr<Transform> _pTransform;   //!< TransformComponent
    raw_ptr<Collider>  _pCollider;    //!< 剛体

    //===========================================================================
    //! @defgroup bullet phy 物理計算用変数
    //! bullet phyのデフォルトと同じ
    //===========================================================================
    //@{
    bool _isKinematic = false;   //!< キネマティック
    f32  _mass        = 0.0f;    //!< 物体の質量(kg)【0 = 静的オブジェクト】

    std::unique_ptr<btRigidBody>   _btRigidBody;     //!< Bullet 剛体
    std::unique_ptr<btMotionState> _btMotionState;   //!< Bullet モーションステート

    //----
    // 三角形メッシュのために使用中はメモリを保持しておく必要がある
    std::unique_ptr<btTriangleIndexVertexArray> _triangles;         //!< 三角形リスト
    std::vector<u32>                            _triangleIndices;   //!< TriangleMesh用インデックス配列

    // TODO
    //f32 _friction         = 0.5f;   //!< 摩擦係数
    //f32 _restitution      = 0.0f;   //!< 跳ね返り係数
    //f32 _rollingFriction  = 0.0f;   //!< 転がり摩擦
    //f32 _spinningFriction = 0.0f;   //!< スピン摩擦

    //@}
};

}   // namespace component
