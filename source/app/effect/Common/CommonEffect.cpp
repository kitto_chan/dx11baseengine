﻿//---------------------------------------------------------------------------
//!	@file	CommonEffect.cpp
//! @brief	CommonEffect
//---------------------------------------------------------------------------
#include "CommonEffect.h"

namespace effect {
namespace {

cb::BlurCB     _blurCB{};
cb::DissolveCB _dissolveCB{};
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
CommonEffect::CommonEffect()
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
CommonEffect::~CommonEffect()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool CommonEffect::Init()
{
    if(_inited) {
        return true;
    }

    u32 width    = gpu::swapChain()->backBuffer()->desc().width_;
    u32 height   = gpu::swapChain()->backBuffer()->desc().height_;
    _workTexture = gpu::createTargetTexture(width, height, DXGI_FORMAT_R8G8B8A8_UNORM);

    // ノイズテクスチャー
    _noiseTexture = gpu::createTextureFromFile("gameSource/noiseTexture/PerlinNoise.png");
    _blackTexture = gpu::createTextureFromFile("ui/loading/04.png");

    _shaderVs_2D      = gpu::createShader("framework/vs_quad2d.fx", "main", "vs_5_0");
    _shaderPs_Texture = gpu::createShader("framework/ps_texture.fx", "main", "ps_5_0");

    _dissolvePs = gpu::createShader("gameSource/Dissolve_PS.fx", "PS", "ps_5_0");        //!< ディゾルブ
    _roarBlurPs = gpu::createShader("gameSource/blur/RoarBlur_PS.fx", "PS", "ps_5_0");   //!< ブラー
    _blurPs     = gpu::createShader("gameSource/blur/CommonBlur_PS.fx", "PS", "ps_5_0");

    _pIntputLayout = gpu::createInputLayout(vertex::VertexPosUv::inputLayout,
                                            std::size(vertex::VertexPosUv::inputLayout));   // 2D入力レイアウト
    // Default設定
    _dissolveCB.dissolve = 0.0f;

    _blurCB.power = 0.0f;
    _inited       = true;

    return true;
}
void CommonEffect::Update()
{
    switch(_currentEffect) {
        case effect::RoarBlur:
            _blurCB.power += 1.0f;
            if(_blurCB.power > 15.0f) {
                _blurCB.power = 5.0f;
            }
            break;
    }
}
//---------------------------------------------------------------------------
//! ImGui描画
//---------------------------------------------------------------------------
void CommonEffect::RenderImGui()
{

}
//---------------------------------------------------------------------------
//! ポストエフェクト開始
//---------------------------------------------------------------------------
void CommonEffect::BeginPostEffect(raw_ptr<gpu::Texture> currentTexture)
{
    if(_currentEffect == EPostEffect::None) return;

    //----------------------------------------------------------
    // ポスト処理
    //----------------------------------------------------------
    gpu::setRenderTarget(0, _workTexture);                           // ポスト描画開始
    gpu::setDepthStencil(nullptr);                                   // デプステクスチャクリア
    gpu::clearColor(_workTexture, float4(0.5f, 0.5f, 0.8f, 1.0f));   // カラーバッファ

    // ポストエフェクトために特殊なシェーダです頂点バッファはいらない
    gpu::setVertexBuffer(0, nullptr);
    gpu::setInputLayout(_pIntputLayout);

    // シェーダー
    gpu::vs::setShader(_shaderVs_2D);   // VS 頂点シェーダー (framework/vs_quad2d.fx)

    switch(_currentEffect) {
        case EPostEffect::Disslove:
            gpu::ps::setShader(_dissolvePs);   // PS ピクセルシェーダー (ポスト用シェーダー)
            gpu::ps::setTexture(1, _blackTexture);
            // ノイズテクスチャ設定
            gpu::ps::setTexture(2, _noiseTexture);

            // シェーダー定数
            dx11::setConstantBuffer("DissolveCB", _dissolveCB);
            break;
        case EPostEffect::RoarBlur:
            gpu::ps::setShader(_roarBlurPs);   // PS ピクセルシェーダー (ポスト用シェーダー)
            dx11::setConstantBuffer("BlurCB", _blurCB);
            break;
        case EPostEffect::Blur:
            gpu::ps::setShader(_blurPs);   // PS ピクセルシェーダー (ポスト用シェーダー)
            dx11::setConstantBuffer("BlurCB", _blurCB);
            break;
    }

    gpu::ps::setTexture(0, currentTexture);
    gpu::ps::setSamplerState(0, gpu::commonStates().LinearWrap());

    gpu::draw(gpu::Primitive::TriangleStrip, 4);
}
//---------------------------------------------------------------------------
//! ポストエフェクト終了、元に戻す
//---------------------------------------------------------------------------
void CommonEffect::EndPostEffect(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    if(_currentEffect == EPostEffect::None) return;
    //----------------------------------------------------------
    // 元に戻す
    //----------------------------------------------------------
    gpu::setRenderTarget(0, colorTexture);
    gpu::setDepthStencil(depthTexture);

    // 画像をオリジナルのテクスチャに書き戻す
    gpu::vs::setShader(_shaderVs_2D);        // VS 頂点シェーダー (framework/vs_quad2d.fx)
    gpu::ps::setShader(_shaderPs_Texture);   // PS ピクセルシェーダー (単に貼るだけのシェーダー)

    // テクスチャ設定
    gpu::ps::setTexture(0, _workTexture);
    gpu::ps::setSamplerState(0, gpu::commonStates().LinearWrap());

    gpu::draw(gpu::Primitive::TriangleStrip, 4);
}
//---------------------------------------------------------------------------
//!< ディゾルブエフェクト開始
//---------------------------------------------------------------------------
void CommonEffect::BeginDissolveEffect(raw_ptr<gpu::Texture> currentTexture)
{
    _currentEffect = EPostEffect::Disslove;
    BeginPostEffect(currentTexture);
}
//---------------------------------------------------------------------------
//!< ディゾルブエフェクト終了
//---------------------------------------------------------------------------
void CommonEffect::EndDissolveEffect(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    EndPostEffect(colorTexture, depthTexture);
    _currentEffect = EPostEffect::None;
}
//---------------------------------------------------------------------------
//!  ブラー開始
//---------------------------------------------------------------------------
void CommonEffect::BeginBlurEffect(raw_ptr<gpu::Texture> currentTexture)
{
    _currentEffect = EPostEffect::RoarBlur;
    BeginPostEffect(currentTexture);
}
//---------------------------------------------------------------------------
//! ブラー終了
//---------------------------------------------------------------------------
void CommonEffect::EndBlurEffect(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    EndPostEffect(colorTexture, depthTexture);
    _currentEffect = EPostEffect::None;
}
//---------------------------------------------------------------------------
//! ディゾルブの値を設定
//---------------------------------------------------------------------------
void CommonEffect::SetDissolve(f32 value)
{
    //! 0 ~ 1 の間
    value = std::clamp(value, 0.0f, 1.0f);

    _dissolveCB.dissolve = value;
}
//---------------------------------------------------------------------------
//! ブラーの値を設定
//---------------------------------------------------------------------------
void CommonEffect::SetBlur(f32 value)
{
    _blurCB.power = value;
}
//---------------------------------------------------------------------------
//! ディゾルブの値を設定
//---------------------------------------------------------------------------
CommonEffect* CommonEffectInstance()
{
    return CommonEffect::Instance();
}
}   // namespace effect
