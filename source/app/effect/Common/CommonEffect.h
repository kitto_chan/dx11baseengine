﻿//---------------------------------------------------------------------------
//!	@file	CommonEffect.h
//! @brief	CommonEffect
//---------------------------------------------------------------------------
#pragma once

namespace effect {
enum EPostEffect
{
    None,
    Disslove,
    RoarBlur,
    Blur,
};
class CommonEffect : public Singleton<CommonEffect>
{
public:
    //! コンストラクタ
    //! @param  [in]    threadCount ワーカースレッド数(default:CPUの論理スレッド数)
    CommonEffect();

    //! デストラクタ
    virtual ~CommonEffect();

    bool Init();          //!< 初期化
    void Update();        //!< 更新
    void RenderImGui();   //!< ImGui描画

    void BeginPostEffect(raw_ptr<gpu::Texture> currentTexture);                                   //!< ポストエフェクト開始
    void EndPostEffect(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture);   //!< ポストエフェクト終了

    void BeginDissolveEffect(raw_ptr<gpu::Texture> currentTexture);                                   //!< ディゾルブエフェクト開始
    void EndDissolveEffect(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture);   //!< ディゾルブエフェクト終了

    void BeginBlurEffect(raw_ptr<gpu::Texture> currentTexture);                                   //!< ブラー開始
    void EndBlurEffect(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture);   //!< ブラー終了

    void SetDissolve(f32 value);   //!< ディゾルブ値を設定
    void SetBlur(f32 value);       //!< ブラー値を設定

    //! ポストエフェクト設定
    void        SetEffect(EPostEffect effect) { _currentEffect = effect; };
    EPostEffect GetCurrentEffect() { return _currentEffect; };

private:
    // 共用テクスチャ(ポスト用)
    shr_ptr<gpu::Texture> _workTexture;    //!< ポスト用テクスチャー
    shr_ptr<gpu::Texture> _noiseTexture;   //!< ノイズのテクスチャ
    shr_ptr<gpu::Texture> _blackTexture;   //!< ノイズのテクスチャ

    shr_ptr<gpu::InputLayout> _pIntputLayout;   //!< 2D入力レイアウト

    // シェーダー
    shr_ptr<gpu::Shader> _shaderVs_2D;        //!< VS 3D頂点シェーダー
    shr_ptr<gpu::Shader> _shaderPs_Texture;   //!< PS テクスチャありピクセルシェーダー

    shr_ptr<gpu::Shader> _dissolvePs;   //!< PS テクスチャありピクセルシェーダー
    shr_ptr<gpu::Shader> _roarBlurPs;   //!< PS ブラーピクセルシェーダー
    shr_ptr<gpu::Shader> _blurPs;       //!< PS ブラーピクセルシェーダー

    bool _inited = false;   //! 初期化したか

    EPostEffect _currentEffect = EPostEffect::None;   //!< なんのエフェクトで描画するか
};

//! スレッドプールを取得
CommonEffect* CommonEffectInstance();
}   // namespace effect
