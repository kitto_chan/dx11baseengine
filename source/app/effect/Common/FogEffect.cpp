﻿//---------------------------------------------------------------------------
//!	@file	FogEffect.cpp
//! @brief	霧のエフェクト
//---------------------------------------------------------------------------
#include "FogEffect.h"

namespace effect {
namespace {
cb::FogCB _fogCB;
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
FogEffect::FogEffect()
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
FogEffect::~FogEffect()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool FogEffect::Init()
{
    return true;
}
void FogEffect::Update()
{
    if(_isUseFog) {
        _currentTexel += _speed;
        ApplyFogConstantBuffer();
    }
}
//---------------------------------------------------------------------------
//! ImGui描画
//---------------------------------------------------------------------------
void FogEffect::RenderImGui()
{
    if(!_isRenderImgui) return;

    // Fog設定のパネル
    ImGui::SetNextWindowSize(ImVec2(350, 200));
    if(!ImGui::Begin("FogEffect", &_isRenderImgui)) {
        ImGui::End();
        return;
    }

    // Imgui
    imgui::ImguiColumn2FormatBegin("IsUseFog");
    ImGui::Checkbox("##IsUseFog", &_isUseFog);
    imgui::ImguiColumn2FormatEnd();

    imgui::ImguiColumn2FormatBegin("Color");
    ImGui::ColorEdit3("##Color", (float*)&_fogColor);
    imgui::ImguiColumn2FormatEnd();

    imgui::ImguiDragXY("Speed", _speed);
    imgui::ImguiDragXY("Texel", _currentTexel);

    imgui::ImguiColumn2FormatBegin("HeightRange");
    ImGui::DragFloatRange2("##HeightRange", &_minHeight, &_maxHeight, 0.01f);
    imgui::ImguiColumn2FormatEnd();

    ImGui::End();

    ApplyFogConstantBuffer();
}
//---------------------------------------------------------------------------
//! ImGui描画
//---------------------------------------------------------------------------
void FogEffect::ApplyFogConstantBuffer()
{
    _fogCB.isUseFog  = _isUseFog;
    _fogCB.fogColor  = math::CastToXMFLOAT3(_fogColor);
    _fogCB.fogTexel  = math::CastToXMFLOAT2(_currentTexel);
    _fogCB.minHeight = _minHeight;
    _fogCB.maxHeight = _maxHeight;
    gpu::setConstantBuffer("FogCB", _fogCB);
}
}   // namespace effect
