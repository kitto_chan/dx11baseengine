﻿//---------------------------------------------------------------------------
//!	@file	FogEffect.h
//! @brief	霧のエフェクト設定
//---------------------------------------------------------------------------
#pragma once

namespace effect {
class FogEffect
{
public:
    //! コンストラクタ
    FogEffect();

    //! デストラクタ
    virtual ~FogEffect();

    bool Init();          //!< 初期化
    void Update();        //!< 更新
    void RenderImGui();   //!< ImGui描画

    void SetIsUseFog(bool isUseFog) { _isUseFog = isUseFog; }       //!< 霧をつかうか
    void SetSpeed(const float2& speed) { _speed = speed; }          //!< 霧の速さを設定
    void SetFogColor(const float3& color) { _fogColor = color; }    //!< 霧の色設定
    void SetMinHeight(f32 minHeight) { _minHeight = minHeight; }    //!< 霧の最低設定
    void SetMaxHeight(f32 maxHeight) { _maxHeight = maxHeight; }    //!< 霧の最高設定
    void SetTexel(const float2& texel) { _currentTexel = texel; }   //!< 霧の現在テクセルを設定

    void ApplyFogConstantBuffer();   //!< 霧の定数バッファを設定

    // !< Imgui描画かどうかのフラグを設定
    void SetIsRenderImgui(bool isRenderImgui) { _isRenderImgui = isRenderImgui; }

private:
    shr_ptr<gpu::Texture> _noiseTexture;   //!< ノイズのテクスチャ

    float2 _speed        = { 0.0f, 0.0f };         //!< 霧の速さ
    float2 _currentTexel = { 0.0f, 0.0f };         //!< 霧の現在テクセルを設定
    float3 _fogColor     = { 1.0f, 1.0f, 1.0f };   //!< 霧の色

    f32 _minHeight = 0.0f;   //!< 霧の最低高度
    f32 _maxHeight = 1.0f;   //!< 霧の最高高度

    bool _isUseFog      = false;   //!< 霧エフェクトを使うかどうか
    bool _isRenderImgui = false;   //!< 霧のImgui設定パネルを開くか
};
}   // namespace effect
