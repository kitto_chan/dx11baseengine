﻿//---------------------------------------------------------------------------
//!	@file	BaseEffect.cpp
//!	@brief	通用エフェクトクラス
//---------------------------------------------------------------------------
#include "Effect.h"

namespace effect {

class BasicEffectImpl final : public BasicEffect
{
public:
    struct WorldCB
    {
        matrix _matWorld;
        matrix _matInvWorld;
    };

    struct CameraCB
    {
        matrix _matView;
        matrix _matProj;
    };

    struct DrawingMatCB
    {
        renderer::Material _material;
        float3           _eyePos;
    };

    struct DrawingStatesCB
    {
        bool _isReflection;
        bool _isShadow;
        bool _isReflectionMat;
        bool _isTextureUsed;
    };

    struct ChangesRarelyCB
    {
        matrix                   _reflection;
        matrix                   _shadow;
        matrix                   _refShadow;
        renderer::DirectionalLight _dirLight[5];
        renderer::PointLight       _pointLight[5];
        renderer::SpotLight        _spotLight[5];
    };

    struct FogCB
    {
        float4 _fogColor;

        bool  _fogEnabled;
        float _fogStart;
        float _fogRange;
        float _pad;
    };

public:
    BasicEffectImpl()  = default;
    ~BasicEffectImpl() = default;

    //! 実体を取得
    static BasicEffectImpl* Instance();

public:
    //---------------------------------------------------------------------------
    //! 定数バッファ
    //---------------------------------------------------------------------------
    CBuffer<0, WorldCB>         _worldCB;           //!< 描画ためにの定数バッファ(Per Frame)
    CBuffer<1, CameraCB>        _cameraCB;          //!< カメラ用
    CBuffer<2, DrawingMatCB>    _drawingMatCB;      //!< 状態変更用
    CBuffer<3, DrawingStatesCB> _drawingStatesCB;   //!< 状態変更用
    CBuffer<4, ChangesRarelyCB> _changesRarelyCB;   //!< あんまり変更しない定数バッファ
    CBuffer<5, FogCB>           _fogCB;             //!< 霧

    std::vector<raw_ptr<CBufferBase>> _pCBuffers;   //!< 以上定数バッファを集中管理する
    //---------------------------------------------------------------------------
    //! シェーダ
    //---------------------------------------------------------------------------
    std::shared_ptr<gpu::Shader> _pVS3D;   //!< 3D頂点シェーダー
    std::shared_ptr<gpu::Shader> _pPS3D;   //!< 3Dピクセルシェーダー

    std::shared_ptr<gpu::Shader> _pVS2D;   //!< 2D頂点シェーダー
    std::shared_ptr<gpu::Shader> _pPS2D;   //!< 2Dピクセルシェーダー
    //---------------------------------------------------------------------------
    //! 入力レイアウト
    //---------------------------------------------------------------------------
    shr_ptr<gpu::InputLayout> _pIntputLayout3D;   //!< 3D入力レイアウト

    shr_ptr<gpu::InputLayout> _pIntputLayout2D;   //!< 2D入力レイアウト
    //---------------------------------------------------------------------------
    //! テクスチャー
    //---------------------------------------------------------------------------
    std::shared_ptr<gpu::Texture> _pTexture;   //!< テクスチャー

    bool _isDirty = false;
};

//==========================================================================
//! Basic Effect
//==========================================================================

//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
BasicEffect::BasicEffect()
{
}
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
BasicEffect::~BasicEffect()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool BasicEffect::InitAll()
{
    if(_inited) return true;
	using namespace vertex;

    pImpl = BasicEffectImpl::Instance();

    // D3D頂点シェーダーを生成
    pImpl->_pVS3D = gpu::createShader("gameSource/basic/basic3D/Basic_3D_VS.hlsl", "VS", "vs_5_0");   // 3D頂点シェーダー
    pImpl->_pVS2D = gpu::createShader("gameSource/basic/basic2D/Basic_2D_VS.hlsl", "VS", "vs_5_0");   // 2D頂点シェーダー
    if(!(pImpl->_pVS3D || pImpl->_pVS2D)) {
        ASSERT_MESSAGE(false, "Basic VShader init fail");
        return false;
    }
	
    // 入力レイアウト
    pImpl->_pIntputLayout3D = gpu::createInputLayout(VertexPosNormalUv::inputLayout, std::size(VertexPosNormalUv::inputLayout)); // 3D
    pImpl->_pIntputLayout2D = gpu::createInputLayout(VertexPosNormalUv::inputLayout, std::size(VertexPosNormalUv::inputLayout));   // 2D

    pImpl->_pPS3D = dx11::createShader("gameSource/basic/basic3D/Basic_3D_PS.hlsl", "PS", "ps_5_0");   // ピクセルシェーダー
    pImpl->_pPS2D = dx11::createShader("gameSource/basic/basic2D/Basic_2D_PS.hlsl", "PS", "ps_5_0");   // ピクセルシェーダー
    if(!(pImpl->_pPS3D || pImpl->_pPS2D)) {
        ASSERT_MESSAGE(false, "Basic PShader init fail");
        return false;
    }

    //　定数バッファ
    pImpl->_pCBuffers.assign({ &pImpl->_worldCB,
                               &pImpl->_cameraCB,
                               &pImpl->_drawingMatCB,
                               //&pImpl->_drawingStatesCB,
                               &pImpl->_changesRarelyCB,
                               &pImpl->_fogCB });

    // 定数バッファをバインド
    pImpl->_worldCB.Bind("FullWorldCB");
    pImpl->_cameraCB.Bind("CameraCB");
    pImpl->_drawingMatCB.Bind("DrawingMatCB");
    //pImpl->_drawingStatesCB.Bind("DrawingStatesCB");
    pImpl->_changesRarelyCB.Bind("ChangesRarelyCB");
    pImpl->_fogCB.Bind("FogCB");

    // バッファを生成
    for(auto& pBuffer : pImpl->_pCBuffers) {
        pBuffer->CreateDynamicBuffer();
    }

    //----------
    // デフォルトの設定

    SetWorldMatrix(math::identity());

    // 環境光
    renderer::DirectionalLight dirLight{};
    dirLight.ambient   = math::CastToXMFLOAT4(float4(0.5f, 0.5f, 0.5f, 1.0f));
    dirLight.diffuse   = math::CastToXMFLOAT4(float4(0.8f, 0.8f, 0.8f, 1.0f));
    dirLight.specular  = math::CastToXMFLOAT4(float4(0.5f, 0.5f, 0.5f, 1.0f));
    dirLight.direction = math::CastToXMFLOAT3(float3(0.0f, -1.0f, 0.0f));
    SetDirLight(0, dirLight);

    // デフォルト マテリアル
    renderer::Material material{};
    material.ambient  = math::CastToXMFLOAT4(float4(1.0f, 1.0f, 1.0f, 1.0f));
    material.diffuse  = math::CastToXMFLOAT4(float4(0.5f, 0.5f, 0.5f, 1.0f));
    material.specular = math::CastToXMFLOAT4(float4(0.8f, 0.8f, 0.8f, 16.0f));
    material.reflect  = math::CastToXMFLOAT4(float4(1.0f, 1.0f, 1.0f, 1.0f));
    SetMaterial(material);

    //霧
    SetFogState(true);
    SetFogColor(float4(0.0f, 0.0f, 0.0f, 1.0f));
    SetFogStart(5.0f);
    SetFogRange(10.0f);

	_inited = true;
    return true;
}
//---------------------------------------------------------------------------
//!  デフォルトレンダー (3D)
//---------------------------------------------------------------------------
void BasicEffect::SetRenderDefault()
{
    gpu::setInputLayout(pImpl->_pIntputLayout3D);
    gpu::vs::setShader(pImpl->_pVS3D);
    gpu::ps::setShader(pImpl->_pPS3D);
    gpu::ps::setSamplerState(0, renderer::RenderStatesIns()->_pSSLinearWrap);
    gpu::setRasterizerState(renderer::RenderStatesIns()->_pRSNoCull);
    gpu::setBlendState(nullptr);
}
//---------------------------------------------------------------------------
//!  デフォルトレンダー (2D)
//---------------------------------------------------------------------------
void BasicEffect::SerRenderDefault2D()
{
    gpu::setInputLayout(pImpl->_pIntputLayout2D);
    gpu::vs::setShader(pImpl->_pVS2D);
    gpu::ps::setShader(pImpl->_pPS2D);
    gpu::ps::setSamplerState(0, renderer::RenderStatesIns()->_pSSPointWrap);
    gpu::setRasterizerState(renderer::RenderStatesIns()->_pRSNoCull);
    gpu::setBlendState(gpu::commonStates().AlphaBlend());
}
//---------------------------------------------------------------------------
//! ワールド行列を設定
//---------------------------------------------------------------------------
void BasicEffect::SetWorldMatrix(const matrix& matWorld)
{
    auto& cBuffer          = pImpl->_worldCB;
    cBuffer.data._matWorld = matWorld;

    matrix matInvWorld        = matWorld;
    matInvWorld._41_42_43_44  = { 0.0f, 0.0f, 0.0f, 1.0f };
    cBuffer.data._matInvWorld = inverse(matInvWorld);

    pImpl->_isDirty = cBuffer._isDirty = true;
}
//---------------------------------------------------------------------------
//! ビュー行列を設定
//---------------------------------------------------------------------------
void BasicEffect::SetViewMatrix(const matrix& matView)
{
    auto& cBuffer         = pImpl->_cameraCB;
    cBuffer.data._matView = matView;
    pImpl->_isDirty = cBuffer._isDirty = true;
}
//---------------------------------------------------------------------------
//! 射影変換行列座標を設定
//---------------------------------------------------------------------------
void BasicEffect::SetProjMatrix(const matrix& matProj)
{
    auto& cBuffer         = pImpl->_cameraCB;
    cBuffer.data._matProj = matProj;
    pImpl->_isDirty = cBuffer._isDirty = true;
}
//---------------------------------------------------------------------------
//! ワールド行列を設定
//---------------------------------------------------------------------------
void BasicEffect::SetReflectionMatrix(const matrix& R)
{
    auto& cBuffer            = pImpl->_changesRarelyCB;
    cBuffer.data._reflection = R;
    pImpl->_isDirty = cBuffer._isDirty = true;
}
//---------------------------------------------------------------------------
//! ビュー行列を設定
//---------------------------------------------------------------------------
void BasicEffect::SetShadowMatrix(const matrix& S)
{
    auto& cBuffer        = pImpl->_changesRarelyCB;
    cBuffer.data._shadow = S;
    pImpl->_isDirty = cBuffer._isDirty = true;
}
//---------------------------------------------------------------------------
//! 射影変換座標を設定
//---------------------------------------------------------------------------
void BasicEffect::SetRefShadowMatrix(const matrix& RefS)
{
    auto& cBuffer           = pImpl->_changesRarelyCB;
    cBuffer.data._refShadow = RefS;
    pImpl->_isDirty = cBuffer._isDirty = true;
}
//---------------------------------------------------------------------------
//! 方向光を設定
//---------------------------------------------------------------------------
void BasicEffect::SetDirLight(size_t pos, const renderer::DirectionalLight& dirLight)
{
    auto& cBuffer               = pImpl->_changesRarelyCB;
    cBuffer.data._dirLight[pos] = dirLight;
    pImpl->_isDirty = cBuffer._isDirty = true;
}
//---------------------------------------------------------------------------
//! 点光源を設定
//---------------------------------------------------------------------------
void BasicEffect::SetPointLight(size_t pos, const renderer::PointLight& pointLight)
{
    auto& cBuffer                 = pImpl->_changesRarelyCB;
    cBuffer.data._pointLight[pos] = pointLight;
    pImpl->_isDirty = cBuffer._isDirty = true;
}
//---------------------------------------------------------------------------
//! スポットライトを設定
//---------------------------------------------------------------------------
void BasicEffect::SetSpotLight(size_t pos, const renderer::SpotLight& spotLight)
{
    auto& cBuffer                = pImpl->_changesRarelyCB;
    cBuffer.data._spotLight[pos] = spotLight;
    pImpl->_isDirty = cBuffer._isDirty = true;
}
//---------------------------------------------------------------------------
//! 材質を設定
//---------------------------------------------------------------------------
void BasicEffect::SetMaterial(const renderer::Material& material)
{
    auto& cBuffer          = pImpl->_drawingMatCB;
    cBuffer.data._material = material;
    pImpl->_isDirty = cBuffer._isDirty = true;
}
//---------------------------------------------------------------------------
//! テクスチャー設置
//---------------------------------------------------------------------------
void BasicEffect::SetTexture(std::shared_ptr<dx11::Texture> texture)
{
    pImpl->_pTexture = texture;
    SetIsTextureUsed(true);
}
//---------------------------------------------------------------------------
//! テクスチャーフラッグ設定
//---------------------------------------------------------------------------
void BasicEffect::SetIsTextureUsed(bool isUsed)
{
    auto& cBuffer               = pImpl->_drawingStatesCB;
    cBuffer.data._isTextureUsed = isUsed;
    pImpl->_isDirty = cBuffer._isDirty = true;
}
//---------------------------------------------------------------------------
//! 目の位置を設定
//---------------------------------------------------------------------------
void BasicEffect::SetEyePos(const float3& eyePos)
{
    auto& cBuffer        = pImpl->_drawingMatCB;
    cBuffer.data._eyePos = eyePos;
    pImpl->_isDirty = cBuffer._isDirty = true;
}
//---------------------------------------------------------------------------
//! 反射の描画設定
//---------------------------------------------------------------------------
void BasicEffect::SetReflectionState(bool isOn)
{
    auto& cBuffer              = pImpl->_drawingStatesCB;
    cBuffer.data._isReflection = isOn;
    pImpl->_isDirty = cBuffer._isDirty = true;
}
//---------------------------------------------------------------------------
//! 反射の材質を設定
//---------------------------------------------------------------------------
void BasicEffect::SetReflectionMat(bool isOn)
{
    auto& cBuffer                 = pImpl->_drawingStatesCB;
    cBuffer.data._isReflectionMat = isOn;
    pImpl->_isDirty = cBuffer._isDirty = true;
}
//---------------------------------------------------------------------------
//! 影の描画設定
//---------------------------------------------------------------------------
void BasicEffect::SetShadowState(bool isOn)
{
    auto& cBuffer          = pImpl->_drawingStatesCB;
    cBuffer.data._isShadow = isOn;
    pImpl->_isDirty = cBuffer._isDirty = true;
}
//---------------------------------------------------------------------------
//! 霧エフェクトを使うかどうか
//---------------------------------------------------------------------------
void BasicEffect::SetFogState(bool isOn)
{
    auto& cBuffer            = pImpl->_fogCB;
    cBuffer.data._fogEnabled = isOn;
    pImpl->_isDirty = cBuffer._isDirty = true;
}
//---------------------------------------------------------------------------
//! 霧のスタートポジション
//---------------------------------------------------------------------------
void BasicEffect::SetFogStart(f32 value)
{
    auto& cBuffer          = pImpl->_fogCB;
    cBuffer.data._fogStart = value;
    pImpl->_isDirty = cBuffer._isDirty = true;
}
//---------------------------------------------------------------------------
//! 霧の色
//---------------------------------------------------------------------------
void BasicEffect::SetFogColor(const float4& fogColor)
{
    auto& cBuffer          = pImpl->_fogCB;
    cBuffer.data._fogColor = fogColor;
    pImpl->_isDirty = cBuffer._isDirty = true;
}
//---------------------------------------------------------------------------
//! 霧の範囲
//---------------------------------------------------------------------------
void BasicEffect::SetFogRange(f32 fogRange)
{
    auto& cBuffer          = pImpl->_fogCB;
    cBuffer.data._fogRange = fogRange;
    pImpl->_isDirty = cBuffer._isDirty = true;
}
//---------------------------------------------------------------------------
//! 設定を適用する
//---------------------------------------------------------------------------
void BasicEffect::Apply()
{
    auto& pCBuffers = pImpl->_pCBuffers;

    gpu::ps::setTexture(0, pImpl->_pTexture);
    if(pImpl->_isDirty) {
        pImpl->_isDirty = false;

        for(auto& pCBuffer : pCBuffers) {
            pCBuffer->SetnUpdateBuffer();
        }
    }
}

//---------------------------------------------------------------------------
//! 実体を取得
//---------------------------------------------------------------------------
effect::BasicEffectImpl* BasicEffectImpl::Instance()
{
    static effect::BasicEffectImpl Instance;   // gpu::Renderのシングルトン実体
    return &Instance;
}

//------------------------------------------------------------------------
//! BasicEffect 管理クラスを取得
//------------------------------------------------------------------------
effect::BasicEffect* BasicEffect::Instance()
{
    return BasicEffectImpl::Instance();
}

effect::BasicEffect* BasicEffectIns()
{
    return BasicEffect::Instance();
}
}   // namespace effect
