﻿//---------------------------------------------------------------------------
//!	@file	BaseEffect.h
//!	@brief	通用エフェクトクラス
//---------------------------------------------------------------------------
#pragma once
#include "EffectHelper.h"

namespace effect {
//---------------------------------------------------------------------------
// エフェクトの純粋クラス
//---------------------------------------------------------------------------
class IEffect
{
public:
    IEffect() = default;

    virtual ~IEffect()      = default;
    IEffect(const IEffect&) = delete;
    IEffect& operator=(const IEffect&) = delete;
    IEffect(IEffect&&)                 = default;
    IEffect& operator=(IEffect&&) = default;

    virtual void Apply() = 0;
};
//---------------------------------------------------------------------------
// 通用エフェクトのクラス
//---------------------------------------------------------------------------
class BasicEffect : public IEffect
{
public:
    BasicEffect();
    virtual ~BasicEffect() override;

    /*  BasicEffect(BasicEffect&& moveFrom) noexcept;
    BasicEffect& operator=(BasicEffect&& moveFrom) noexcept;*/

    //! 実体を取得
    static BasicEffect* Instance();

    //! 初期化
    bool InitAll();
 
    //---------------------------------------------------------------------------
    //! Render レンダー
    //---------------------------------------------------------------------------
    void SetRenderDefault();   //!< 3Dデフォルトレンダー

	void SerRenderDefault2D(); //!< 2Dデフォルトレンダー
    //---------------------------------------------------------------------------
    //! Matrix メトリクス
    //---------------------------------------------------------------------------
    void SetWorldMatrix(const matrix& w);   //!< ワールド行列を設定
    void SetViewMatrix(const matrix& v);    //!< ビュー行列を設定
    void SetProjMatrix(const matrix& p);    //!< 射影変換行列座標を設定

    void SetReflectionMatrix(const matrix& r);     //!< ワールド行列を設定
    void SetShadowMatrix(const matrix& s);         //!< ビュー行列を設定
    void SetRefShadowMatrix(const matrix& refS);   //!< 射影変換座標を設定

    //---------------------------------------------------------------------------
    //! Light, material, texture
    //---------------------------------------------------------------------------
    static const int MAX_LIGHTS = 5;   //!< 最大ライト数

    void SetDirLight(u64 id, const renderer::DirectionalLight& dirLight);   //!< 方向光を設定
    void SetPointLight(u64 id, const renderer::PointLight& pointLight);     //!< 点光源を設定
    void SetSpotLight(u64 id, const renderer::SpotLight& spotLight);        //!< スポットライトを設定

    void SetMaterial(const renderer::Material& material);   //!< 材質を設定

    void SetTexture(std::shared_ptr<dx11::Texture> texture);   //!< テクスチャー設置
    void SetIsTextureUsed(bool isUsed);                        //!< テクスチャーフラッグ設定

    void SetEyePos(const float3& eyePos);   //!< 目の位置を設定

    //---------------------------------------------------------------------------
    //! state Switch
    //---------------------------------------------------------------------------
    void SetReflectionState(bool isOn);   //!< 反射の描画設定
    void SetReflectionMat(bool isOn);     //!< 反射の材質を設定
    void SetShadowState(bool IsOn);       //!< 影を描画設定

    //Fog 霧
    void SetFogState(bool isOn);                //!< 霧エフェクトを使うかどうか
    void SetFogStart(f32 fogStart);             //!< 霧のスタートポジション
    void SetFogColor(const float4& fogColor);   //!< 霧の色
    void SetFogRange(f32 fogRange);             //!< 霧の範囲

    void Apply() override;   //!< 設定を適用する

	bool _inited = false;

private:
    class BasicEffectImpl* pImpl;
};

effect::BasicEffect* BasicEffectIns();
}   // namespace effect
