﻿//---------------------------------------------------------------------------
//!	@file	SwordTrailEffect.cpp
//! @brief	剣の軌跡のエフェクト実装クラス
//---------------------------------------------------------------------------
#include "SwordTrailEffect.h"
using namespace DirectX;
namespace effect {
namespace {
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
SwordTrailEffect::SwordTrailEffect()
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
SwordTrailEffect::~SwordTrailEffect()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool SwordTrailEffect::Init()
{
    _swordNodeList.resize(_trailFrame);

    _swordTrailShader_Vs    = gpu::createShader("gameSource/swordTrail/SwordTrail_VS.hlsl", "VS", "vs_5_0");
    _swordTrailShader_Ps    = gpu::createShader("gameSource/swordTrail/SwordTrail_PS.hlsl", "PS", "ps_5_0");
    _swordTrailIntputLayout = gpu::createInputLayout(vertex::VertexPosUv::inputLayout, std::size(vertex::VertexPosUv::inputLayout));

    if(!_swordTrailTexture) {
        ASSERT_MESSAGE(false, "テクスチャー設定してない");
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void SwordTrailEffect::Update()
{
    if(!_isActive) return;

    //データを更新
    for(size_t i = _swordNodeList.size() - 1; i > 0; --i) {
        _swordNodeList[i] = _swordNodeList[i - 1];
    }
    _swordNodeList.front() = _currentSwordNode;
    _currentSwordNode      = SwordTrailNode();

    std::vector<SwordTrailNode> usedSwordNodes = GetUsingNodes();
    //曲線を作る
    if(usedSwordNodes.empty()) return;
    CreateCurveVertex(usedSwordNodes);

    //頂点データを更新する
    f32 amount = 1.0f / (usedSwordNodes.size() - 1);   // 計算uv用
    f32 v      = 0;
    _swordTrailVertices.clear();
    _swordTrailVertices.resize(usedSwordNodes.size() * 2);

    for(size_t i = 0, j = 0; i < _swordTrailVertices.size() && j < usedSwordNodes.size(); i += 2, ++j) {
        // 剣の先
        _swordTrailVertices[i].pos = usedSwordNodes[j].head;
        _swordTrailVertices[i].uv  = DirectX::XMFLOAT2(1.0f, v);
        // 剣の端
        _swordTrailVertices[i + 1].pos = usedSwordNodes[j].tail;
        _swordTrailVertices[i + 1].uv  = DirectX::XMFLOAT2(0.0f, v);
        v += amount;
    }
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void SwordTrailEffect::Render()
{
    if(!_isActive) return;

    dx11::setConstantBuffer("WorldCB", math::identity());   // GPUへ変更を反映
    //------------------
    // シェーダ設定
    //------------------
    gpu::vs::setShader(_swordTrailShader_Vs);
    gpu::ps::setShader(_swordTrailShader_Ps);
    gpu::setInputLayout(_swordTrailIntputLayout);

    gpu::setVertexBuffer(0, _swordTrailBuffer_Vs, sizeof(vertex::VertexPosUv));   // 頂点バッファ

    gpu::ps::setTexture(0, _swordTrailTexture);
    gpu::ps::setSamplerState(0, gpu::commonStates().LinearWrap());
    gpu::setBlendState(gpu::commonStates().AlphaBlend());

    // 描画
    gpu::drawUserPrimitive(gpu::Primitive::TriangleStrip, (u32)_swordTrailVertices.size(), _swordTrailVertices.data());

    // 元に戻す
    gpu::setBlendState(gpu::commonStates().Opaque());
}
//---------------------------------------------------------------------------
//! ImGui描画
//---------------------------------------------------------------------------
void SwordTrailEffect::RenderImGui()
{
}
//---------------------------------------------------------------------------
//! 座標設定
//---------------------------------------------------------------------------
void SwordTrailEffect::SetPos(const float3& head, const float3& tail)
{
    if(!_isActive) return;
    _currentSwordNode.head    = math::CastToXMFLOAT3(head);
    _currentSwordNode.tail    = math::CastToXMFLOAT3(tail);
    _currentSwordNode.isUsing = true;
}
//---------------------------------------------------------------------------
//! 剣の軌跡のテクスチャーを設定
//! @param trailTexture テクスチャー
//---------------------------------------------------------------------------
void SwordTrailEffect::SetTrailTexture(std::string_view path)
{
    _swordTrailTexture = gpu::createTextureFromFile(path);
    ASSERT_MESSAGE(_swordTrailTexture, "テクスチャーのパス間違っている");
}
//---------------------------------------------------------------------------
//!< リセット,　保存しているノードをすべて廃棄
//---------------------------------------------------------------------------
void SwordTrailEffect::Reset()
{
    _currentSwordNode = {};

    _swordNodeList.clear();
    _swordNodeList.resize(_trailFrame);

    _isActive = false;
}
//---------------------------------------------------------------------------
//! 剣の軌跡使うかどうか
//---------------------------------------------------------------------------
void SwordTrailEffect::SetSwordTrailIsActive(bool isUsing)
{
    if(!isUsing) {
        Reset();
    }

    _isActive = isUsing;
}
//---------------------------------------------------------------------------
//! 曲面頂点設定
//! 剣の軌跡を滑らかに補完する
//! 軌跡を滑らかにするには、各頂点の間を結ぶカーブを求めて、頂点を増やす（分割する）必要があります。
//---------------------------------------------------------------------------
void SwordTrailEffect::CreateCurveVertex(std::vector<SwordTrailNode>& usedSwordNodes)
{
    if(usedSwordNodes.size() < 3 || _curveSplite < 1) {
        return;
    }

    std::vector<SwordTrailNode> newSwordPosNodes;
    newSwordPosNodes.reserve(usedSwordNodes.size() + (usedSwordNodes.size() - 1) * _curveSplite);
    const f32 amount = 1.0f / (_curveSplite + 1);

    SwordTrailNode newNode;
    newSwordPosNodes.push_back(usedSwordNodes.front());

    for(size_t i = 0; i < usedSwordNodes.size() - 1; ++i) {
        f32 ratio = amount;
        //---------------------------------------------------
        //  CatMulに使う4つの点を作る（p0, p3がない時の処理も書く）
        //---------------------------------------------------
        // head
        float3 v1head = math::CastToFloat3(usedSwordNodes[1].head);
        float3 v2head = math::CastToFloat3(usedSwordNodes[2].head);

        float3 p0Head = i == 0 ? (v1head + v2head) * 0.5f : math::CastToFloat3(usedSwordNodes[i - 1].head);
        float3 p1Head = math::CastToFloat3(usedSwordNodes[i].head);
        float3 p2Head = math::CastToFloat3(usedSwordNodes[i + 1].head);
        float3 p3Head = i == usedSwordNodes.size() - 2 ? (p0Head + p2Head) * 0.5f : math::CastToFloat3(usedSwordNodes[i + 2].head);

        // tail
        float3 v1tail = math::CastToFloat3(usedSwordNodes[1].tail);
        float3 v2tail = math::CastToFloat3(usedSwordNodes[2].tail);

        float3 p0tail = i == 0 ? (v1tail + v2tail) * 0.5f : math::CastToFloat3(usedSwordNodes[i - 1].tail);
        float3 p1tail = math::CastToFloat3(usedSwordNodes[i].tail);
        float3 p2tail = math::CastToFloat3(usedSwordNodes[i + 1].tail);
        float3 p3tail = i == usedSwordNodes.size() - 2 ? (p0tail + p2tail) * 0.5f : math::CastToFloat3(usedSwordNodes[i + 2].tail);

        // 曲面のため、追加の頂点
        for(size_t j = 0; j < static_cast<size_t>(_curveSplite - 1); ++j) {
            newNode = SwordTrailNode();
            // head
            DirectX::XMVECTOR p0h = math::CastToXMVECTOR(p0Head);
            DirectX::XMVECTOR p1h = math::CastToXMVECTOR(p1Head);
            DirectX::XMVECTOR p2h = math::CastToXMVECTOR(p2Head);
            DirectX::XMVECTOR p3h = math::CastToXMVECTOR(p3Head);
            DirectX::XMStoreFloat3(&newNode.head, DirectX::XMVectorCatmullRom(p0h, p1h, p2h, p3h, ratio));
            // tail
            DirectX::XMVECTOR p0t = math::CastToXMVECTOR(p0tail);
            DirectX::XMVECTOR p1t = math::CastToXMVECTOR(p1tail);
            DirectX::XMVECTOR p2t = math::CastToXMVECTOR(p2tail);
            DirectX::XMVECTOR p3t = math::CastToXMVECTOR(p3tail);
            DirectX::XMStoreFloat3(&newNode.tail, DirectX::XMVectorCatmullRom(p0t, p1t, p2t, p3t, ratio));

            newSwordPosNodes.push_back(newNode);
            ratio += amount;
        }

        newSwordPosNodes.push_back(usedSwordNodes[i + 1]);
    }

    usedSwordNodes = newSwordPosNodes;
}
//---------------------------------------------------------------------------
// 使ってるNodeを取得
//---------------------------------------------------------------------------
std::vector<SwordTrailEffect::SwordTrailNode> SwordTrailEffect::GetUsingNodes()
{
    // TODO: 他のいいやり方があると思う
    std::vector<SwordTrailEffect::SwordTrailNode> usedNode;
    for(auto& node : _swordNodeList) {
        if(node.isUsing) {
            usedNode.push_back(node);
        }
    }
    return usedNode;
}
}   // namespace effect
