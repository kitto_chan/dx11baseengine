﻿//---------------------------------------------------------------------------
//!	@file	SwordTrailEffect.h
//! @brief	剣の軌跡のエフェクト実装クラス
//---------------------------------------------------------------------------
#pragma once

namespace effect {
class SwordTrailEffect
{
public:
    //! 剣の軌跡のデータノード
    struct SwordTrailNode
    {
        DirectX::XMFLOAT3 head;   //剣の先端の位置
        DirectX::XMFLOAT3 tail;   //剣の末端の位置
        bool              isUsing = false;
    };

    //! コンストラクタ
    SwordTrailEffect();
    //! デストラクタ
    virtual ~SwordTrailEffect();

    bool Init();          //!< 初期化
    void Update();        //!< 更新
    void Render();        //!< 描画
    void RenderImGui();   //!< ImGui描画

    /// 現在剣の軌跡の剣の先の端を設定
    /// @param head 剣の先
    /// @param tail 剣の端
    void SetPos(const float3& head, const float3& tail);

    //! 剣の軌跡のテクスチャーを設定
    //! @param trailTexture テクスチャー
    void SetTrailTexture(shr_ptr<gpu::Texture> trailTexture) { _swordTrailTexture = trailTexture; };

    //! 剣の軌跡のテクスチャーを設定
    //! @param trailTexture テクスチャーのパス
    void SetTrailTexture(std::string_view path);

    void Reset();   //!< リセット,　保存しているノードをすべて廃棄

    //! 剣の軌跡使うかどうか
    void SetSwordTrailIsActive(bool isActive);

private:
    //------------------------------------------------------
    // private 変数
    //------------------------------------------------------
    std::vector<SwordTrailNode>      _swordNodeList;        //!< 剣の位置を保存するバッファ
    std::vector<vertex::VertexPosUv> _swordTrailVertices;   //!< 頂点バッファ
    SwordTrailNode                   _currentSwordNode;     //!< 現在のノード(sword Trailのデータ)

    //!< 剣の軌跡もっど丸めに見えるように
    //!< フレーム1とフレーム2間追加の補間数
    u32 _curveSplite = 16;

    u32 _trailFrame = 8;   //!< 軌跡のサイズ(フレーム)

    bool _isActive = false;   //!< 剣の軌跡の描画する？
    //----------
    // シェーダ / 描画に関して
    shr_ptr<gpu::Shader> _swordTrailShader_Vs;   //!< 頂点シェーダ
    shr_ptr<gpu::Shader> _swordTrailShader_Ps;   //!< ピクセルシェーダ

    shr_ptr<gpu::InputLayout> _swordTrailIntputLayout;   //!< 2D入力レイアウト

    shr_ptr<gpu::Texture> _swordTrailTexture;   //!< 剣のテクスチャー

    shr_ptr<gpu::Buffer> _swordTrailBuffer_Vs;   //!< 頂点バッファ
    //------------------------------------------------------
    // private 関数
    //------------------------------------------------------

    //! 剣の軌跡の曲面生成
    void CreateCurveVertex(std::vector<SwordTrailNode>& usedswordBuffer);

    // 使ってるNodeを取得
    std::vector<SwordTrailEffect::SwordTrailNode> GetUsingNodes();
};
}   // namespace effect
