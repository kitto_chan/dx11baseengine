﻿//---------------------------------------------------------------------------
//!	@file	Entity.cpp
//!	@brief	エンティティ
//---------------------------------------------------------------------------
#include "Entity.h"
#include <imgui/misc/cpp/imgui_stdlib.h>
#include <magic_enum/include/magic_enum.hpp>
namespace entity {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
Entity::Entity()
{
}

Entity::Entity(const std::string& name)
: _name(name)
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
Entity::~Entity()
{
    RemoveAllComponents();
}

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool Entity::Init()
{
    // 初期化チェック
    if(_isInited) {
        ASSERT_MESSAGE(false, "Entity二回目初期化している");
        return false;
    }

    // 初期化
    OnInit();

    // 持っているコンポーネントを初期化
    for(auto& component : _components) {
        if(!_isInited) {
            component->Init();
        }
    }

    _isInited = true;
    return true;
}

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void Entity::Update()
{
    if(!IsReady())
        return;

    OnUpdate();

    for(auto& component : _components) {
        if(!component->_isInited) {
            component->Init();
        }

        if(!component->IsEnable())
            continue;

        component->Update();
    }
}
//---------------------------------------------------------------------------
//! 後期更新
//---------------------------------------------------------------------------
void Entity::LateUpdate()
{
    if(!IsReady())
        return;

    OnLateUpdate();

    //! @note	LateUpdate() は 関数が呼び出された後に実行されます。
    //!			これはスクリプトの実行順を決める時に便利です。
    //!			例えばカメラを追従するには Update で移動された結果を基に常に
    //!			LateUpdate で位置を更新する必要があります。
    for(auto& component : _components) {
        if(!component->IsEnable())
            continue;
        component->LateUpdate();
    }
}

//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void Entity::Render()
{
    if(!IsReady())
        return;

    OnRender();

    for(auto& component : _components) {
        if(!component->IsEnable())
            continue;
        component->Render();
    }
}

//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void Entity::RenderImgui()
{
    //if(!IsReady()) return;

    ImGui::Checkbox("##Enable", &_isEnable);
    ImGui::SameLine();
    ImGui::PushItemWidth(ImGui::GetColumnWidth());
    ImGui::InputText("##Name", &_name);

	imgui::ImGuiEnumCombo<Layer>("Layer", _layer);
    imgui::ImGuiEnumCombo<Tag>("Tag", _tag);

    //{
    //    constexpr auto tagNames        = magic_enum::enum_names<Tag>();
    //    auto           selectedTagName = magic_enum::enum_name(_tag);
    //    if(ImGui::BeginCombo("combo 1", selectedTagName.data())) {
    //        for(int n = 0; n < tagNames.size(); n++) {
    //            const bool is_selected = (magic_enum::enum_integer(_tag) == n);
    //            if(ImGui::Selectable(tagNames[n].data(), is_selected))
    //                _tag = magic_enum::enum_value<Tag>(n);

    //            // Set the initial focus when opening the combo (scrolling + keyboard navigation focus)
    //            if(is_selected)
    //                ImGui::SetItemDefaultFocus();
    //        }
    //        ImGui::EndCombo();
    //    }
    //}

    ImGui::Separator();

    OnRenderImgui();

    for(auto& component : _components) {
        if(ImGui::CollapsingHeader(component->GetName().c_str())) {
            if(component) {
                component->RenderImgui();
            }
        }
    }
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void Entity::Finalize()
{
    _isRelease = true;
}
void Entity::Release()
{
    _isRelease = true;
}
//---------------------------------------------------------------------------
//! 全部持ってるコンポーネントを削除する
//---------------------------------------------------------------------------
void Entity::RemoveAllComponents()
{
    for(auto itr = _components.rbegin(); itr != _components.rend(); ++itr) {
        auto component = (*itr);
        component->Finalize();
        component.reset();
    }
    _components.clear();
}
//---------------------------------------------------------------------------
//! 名前を取得
//---------------------------------------------------------------------------
std::string Entity::GetName() const
{
    return _name;
}
//---------------------------------------------------------------------------
//! レイヤーを取得
//---------------------------------------------------------------------------
Layer Entity::GetLayer() const
{
    return _layer;
}

}   // namespace entity
