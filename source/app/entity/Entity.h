﻿//---------------------------------------------------------------------------
//!	@file	Entity.h
//!	@brief	エンティティ
//---------------------------------------------------------------------------
#pragma once
#include "component/Component.h"
namespace manager {
class EntityManager;
};

namespace entity {

class Entity
{
    friend class manager::EntityManager;

public:
    Entity();
    Entity(const std::string& name);
    virtual ~Entity();

    void Release();   //!< エンティティマネジャーから外す
protected:
    //---------------------------------------------------------------------------
    // 関数
    //---------------------------------------------------------------------------
    bool Init();          //!< 初期化
    void Update();        //!< 更新
    void LateUpdate();    //!< 後期更新
    void Render();        //!< 描画
    void RenderImgui();   //!< ImGui描画
    void Finalize();      //!< 解放

public:
    //---------------------------------------------------------------------------
    // コンポーネントのAccessor
    //---------------------------------------------------------------------------

    //!追加コンポーネント
    template<typename T, typename... TArgs>
    raw_ptr<T> AddComponent(TArgs&&... args);

    //!ゲットコンポーネント
    template<class T>
    raw_ptr<T> GetComponent();

    //template<class T>
    //const raw_ptr<T> GetComponent();

    //! 削除コンポーネント
    template<class T>
    void RemoveComponent();

    //! 既定のコンポーネントすべて削除
    void RemoveAllComponents();

    //!< 用意できたか？　出来たら更新と描画実行する
    bool IsReady() { return _isEnable && _isInited; };

    //! 名前を取得
    std::string GetName() const;

    //! レイヤーを取得
    Layer GetLayer() const;
    //! レイヤーを設定
    void SetLayer(Layer layer) { _layer = layer; };

    //!< タグを取得
    Tag GetTag() const { return _tag; };
    //!< タグを設定
    void SetTag(Tag tag) { _tag = tag; }

    virtual void Disable() { _isEnable = false; }   //!< 無効にする
    virtual void Enable() { _isEnable = true; }     //!< 有効にする

protected:
    //---------------------------------------------------------------------------
    // 内部変数
    //---------------------------------------------------------------------------

    // 継承用
    virtual bool OnInit() { return true; };   //!< 初期化
    virtual void OnUpdate(){};                //!< 更新
    virtual void OnLateUpdate(){};            //!< 後期更新
    virtual void OnRender(){};                //!< 描画
    virtual void OnRenderImgui(){};           //!< ImGui描画
    virtual void OnFinalize(){};              //!< 解放

    bool _isEnable  = true;    //!< 有効にする(Update/Render)
    bool _isInited  = false;   //!< 初期化した
    bool _isRelease = false;   //!< エンティティマネジャーから外す
    bool _isRemoved = false;   //!< 完全に削除

    std::vector<shr_ptr<component::Component>> _components;   //!< コンポーネントリスト

    std::string _name;                                //!< 名前
    s32         _type = gameApp::EntityType_Entity;   //!< タイプ

    //! レイヤー は、カメラ がシーンの一部のみを描画するできると、
    //! 衝突を発生させるためにも使用することができます。
    Layer _layer = Layer::Default;

    //! タグ は、1 つまたは複数の ゲームオブジェクト に割り当てることができる参照ラベルです
    Tag _tag = Tag::Untagged;
};

//===========================================================================
//!	@defgroup	コンポーネントに関して
//===========================================================================
//@{

//---------------------------------------------------------------------------
//! コンポーネントを追加する
//---------------------------------------------------------------------------
template<typename T, typename... TArgs>
inline raw_ptr<T> Entity::AddComponent(TArgs&&... args)
{
    shr_ptr<T> newComponent(std::make_shared<T>(std::forward<TArgs>(args)...));
    newComponent->SetOwner(this);
    _components.emplace_back(newComponent);
    return newComponent.get();
    //_components.emplace_back(std::move(newComponent));
    //return _components.back().get();
}

//---------------------------------------------------------------------------
//! コンポーネントを取得する
//---------------------------------------------------------------------------
template<class T>
inline raw_ptr<T> Entity::GetComponent()
{
    for(auto& component : _components) {
        if(dynamic_cast<T*>(component.get())) {
            return static_cast<T*>(component.get());
            //return component.get();
        }
    }
    return nullptr;
}

//---------------------------------------------------------------------------
//! コンポーネントを削除する
//---------------------------------------------------------------------------
template<class T>
inline void Entity::RemoveComponent()
{
    for(auto itr = _components.begin(); itr != _components.end(); ++itr) {
        if(dynamic_cast<T*>(*itr)) {
            _components.erase(itr);
            break;
        }
    }
}
// @}
}   // namespace entity
