﻿#include "Cube.h"
#include "component/Transform.h"
#include "utilityGPU/Geometry.h"
#include "effect/Effect.h"
#include "component/physics/RigidBody.h"
#include "component/physics/Collider.h"

namespace entity::go {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
Cube::Cube(const std::string& name, const float3& pos, const float3& scale, const float3& rotation)
: GameObject(name, pos, scale, rotation)
{
};
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
Cube::~Cube()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool Cube::OnInit()
{
    InitGPU();

    return true;
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void Cube::OnRender()
{
    // TODO Meshコンポネントクラス
    matrix worldMatrix = GetComponent<component::Transform>()->GetLocalToWorldMatrix();
    effect::BasicEffectIns()->SetWorldMatrix(worldMatrix);

    // 頂点データー
    gpu::setVertexBuffer(0, _pVertexBuffer, sizeof(vertex::VertexPosNormalUv));   // 頂点バッファ
    gpu::setIndexBuffer(_pIndexBuffer);                                            // インデックスバッファ

    // TODO: Image Loader
    static shr_ptr<gpu::Texture> texture;
    if(!texture) {
        texture = gpu::createTextureFromFile("pattern/Grid.png");
    }
    effect::BasicEffectIns()->SetTexture(texture);

    effect::BasicEffectIns()->SetRenderDefault();
    effect::BasicEffectIns()->Apply();

    gpu::drawIndexed(gpu::Primitive::TriangleList, _indexCount);
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void Cube::OnUpdate()
{
}
//---------------------------------------------------------------------------
//! 初期化GPU
//---------------------------------------------------------------------------
bool Cube::InitGPU()
{
    geometry::MeshData                      meshdata = geometry::CreateBox(_size, float4(1.0f, 1.0f, 1.0f, 1.0f));
    std::vector<vertex::VertexPosNormalUv> vertices;
    std::vector<s32>                        indices;

    vertices.resize(meshdata.vertexData.size());
    indices.resize(meshdata.indexData.size());

    for(u64 i = 0; i < meshdata.vertexData.size(); i++) {
        vertices[i] = { meshdata.vertexData[i].pos,
                        meshdata.vertexData[i].nor,
                        meshdata.vertexData[i].tex };
    };

    for(u64 i = 0; i < meshdata.indexData.size(); i++) {
        indices[i] = meshdata.indexData[i];
    };

    _indexCount = (u32)meshdata.indexData.size();

    _pVertexBuffer = gpu::createBuffer({ vertices.size() * sizeof(vertex::VertexPosNormalUv), D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_IMMUTABLE }, vertices.data());
    _pIndexBuffer  = gpu::createBuffer({ indices.size() * sizeof(u32), D3D11_BIND_INDEX_BUFFER, D3D11_USAGE_IMMUTABLE }, indices.data());

    return true;
}
}   // namespace entity::go
