﻿//---------------------------------------------------------------------------
//!	@file	GameObject.h
//!	@brief	ゲームオブジェクトの基底クラス
//---------------------------------------------------------------------------
#include "GameObject.h"
#include "Component/Transform.h"

namespace entity::go {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
GameObject::GameObject(const std::string& name, const float3& pos, const float3& scale, const float3& rotation)
: Entity(name)
{
    _pTransform = AddComponent<component::Transform>(pos, scale, rotation);
};
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
GameObject::~GameObject()
{
	auto childs = GetChildEntts();
    if(!childs.empty()) {
        for(auto child : childs) {
            child->Release();
            if(child->GetComponent<component::Transform>()) {
                child->GetComponent<component::Transform>()->SetParent(nullptr);
			}
		}
	}
    if(GetParent()) {
        _pTransform->GetParent()->RemoveChild(_pTransform);
    }

}
//---------------------------------------------------------------------------
//!< 子物件あるかどうか
//---------------------------------------------------------------------------
bool GameObject::HasChild() const
{
    return _pTransform->HasChild();
}
//---------------------------------------------------------------------------
//!< 親物件あるかどうか
//---------------------------------------------------------------------------
bool GameObject::HasParent() const
{
    return _pTransform->HasParent();
}
//---------------------------------------------------------------------------
//!< 子物件を設定
//---------------------------------------------------------------------------
void GameObject::SetParent(raw_ptr<GameObject> childObj)
{
    _pTransform->SetParent(childObj->_pTransform);
}
//---------------------------------------------------------------------------
//!< 親を取得
//---------------------------------------------------------------------------
raw_ptr<GameObject> GameObject::GetParent() const
{
    if(_pTransform->GetParent()) {
        return dynamic_cast<GameObject*>(_pTransform->GetParent()->GetOwner());
    }
    return nullptr;
}
//---------------------------------------------------------------------------
//! 子物件を取得(Entity形式)
//---------------------------------------------------------------------------
std::vector<raw_ptr<Entity>> GameObject::GetChildEntts() const
{
    std::vector<raw_ptr<Entity>> childEntt;

    for(auto childTrans : _pTransform->GetChildTransform()) {
        childEntt.emplace_back(childTrans->GetOwner());
    }

    return childEntt;
}
//---------------------------------------------------------------------------
//! 無効にする（下の子物件も一緒に）
//---------------------------------------------------------------------------
void GameObject::Disable()
{
    for(auto& child : GetChildObjs()) {
        child->Disable();
	}
    Entity::Disable();
}
//---------------------------------------------------------------------------
//! 有効にする（下の子物件も一緒に）
//---------------------------------------------------------------------------
void GameObject::Enable()
{
    for(auto& child : GetChildObjs()) {
        child->Enable();
    }
    Entity::Enable();
}
//---------------------------------------------------------------------------
//!< 子物件を取得(GameObject形式)
//---------------------------------------------------------------------------
std::vector<raw_ptr<GameObject>> GameObject::GetChildObjs() const
{
    std::vector<raw_ptr<GameObject>> childObjs;

    for(auto childTrans : _pTransform->GetChildTransform()) {
        raw_ptr<GameObject> childObj = dynamic_cast<GameObject*>(childTrans->GetOwner());
        childObjs.emplace_back(childObj);
    }

    return childObjs;
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void GameObject::OnRenderImgui()
{
}
void GameObject::OnFinalize()
{
}
}   // namespace entity::go
