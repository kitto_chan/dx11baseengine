﻿//---------------------------------------------------------------------------
//!	@file	GameObject.h
//!	@brief	ゲームオブジェクトの基底クラス
//---------------------------------------------------------------------------
#pragma once
#include "entity/Entity.h"
#include "component/Transform.h"

//namespace component {
//class Transform;
//};

using namespace gameobject;
namespace entity::go {
class GameObject : public Entity
{
public:
    GameObject(const std::string& name     = "GameObject",
               const float3&      pos      = float3(0.0f, 0.0f, 0.0f),
               const float3&      scale    = float3(1.0f, 1.0f, 1.0f),
               const float3&      rotation = float3(0.0f, 0.0f, 0.0f));
    virtual ~GameObject();

    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    bool HasChild() const;    //!< 子物件あるかどうか
    bool HasParent() const;   //!< 親物件あるかどうか

    void SetParent(raw_ptr<GameObject> childObj);   //!< 子物件を設定

    raw_ptr<GameObject>              GetParent() const;       //!< 親を取得
    std::vector<raw_ptr<GameObject>> GetChildObjs() const;    //!< 子物件を取得(GameObject形式)
    std::vector<raw_ptr<Entity>>     GetChildEntts() const;   //!< 子物件を取得(Entity形式)

    float3 GetPosition() const { return _pTransform->GetPosition(); }   //!< 座標を取得
    float3 GetRotation() const { return _pTransform->GetRotation(); }   //!< 回転角度を取得

    void Disable() override;   //!< 無効にする（下の子物件も一緒に）
    void Enable() override;    //!< 有効にする（下の子物件も一緒に）

protected:
    //---------------------------------------------------------------------------
    //! public 変数
    //---------------------------------------------------------------------------

    raw_ptr<component::Transform> _pTransform;   //!< オブジェクトの位置、回転、スケールを扱うクラス
private:
    //---------------------------------------------------------------------------
    //! private 関数
    //---------------------------------------------------------------------------
    // 継承用
    //bool OnInit()        override;  //!< 初期化
    //void OnRender()      override;  //!< 描画
    //void OnUpdate()      override;  //!< 更新

    virtual void OnRenderImgui() override;   //!< ImGui描画
    virtual void OnFinalize() override;      //!< 解放
};
}   // namespace entity::go
