﻿//---------------------------------------------------------------------------
//!	@file	Line.cpp
//!	@brief	Line描画
//---------------------------------------------------------------------------
#include "Line.h"
#include "component/Transform.h"
#include "utilityGPU/Geometry.h"
#include "effect/Effect.h"
#include "component/physics/RigidBody.h"
#include "component/physics/Collider.h"
#include "manager/CameraManager.h"
namespace entity::go {
namespace {
cb::MeshColorCB colorCb = {};
//====================
// GPU用
//====================
shr_ptr<gpu::Shader>      _pVsShader;
shr_ptr<gpu::Shader>      _pPsShader;
shr_ptr<gpu::InputLayout> _pILPosUv;   //!< 入力レイアウト

// 線の描画なので、UVは使ってない
constexpr vertex::VertexPosUv vertices[]{
    { DirectX::XMFLOAT3(-0.5f, 0.0f, 0.0f), DirectX::XMFLOAT2(0.0f, 0.0f) },
    { DirectX::XMFLOAT3(+0.5f, 0.0f, 0.0f), DirectX::XMFLOAT2(0.0f, 0.0f) },
};
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
Line::Line(const std::string& name, const float3& pos, const float3& scale, const float3& rotation)
: GameObject(name, pos, scale, rotation){};
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
Line::~Line()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool Line::OnInit()
{
    InitGPU();
    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void Line::OnUpdate()
{
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void Line::OnRender()
{
    BeginRender();
    EndRender();
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void Line::OnRenderImgui()
{
}
//---------------------------------------------------------------------------
//! 2D描画を開始
//---------------------------------------------------------------------------
void Line::BeginRender()
{
    //-----------------------------------------------------------------
    // 設定
    //-----------------------------------------------------------------

    //====================
    // シェーダの設定
    //====================
    gpu::vs::setShader(_pVsShader);
    gpu::ps::setShader(_pPsShader);

    gpu::setVertexBuffer(0, _pVertexBuffer);   // 頂点バッファ
    //gpu::setIndexBuffer(_pIndexBuffer);        // インデックスバッファ

    //----------
    // カメラ行列(view & projection)
    //gpu::setConstantBuffer("CameraCB", cameraCB);

    //----------
    // ワールド行列
    gpu::setConstantBuffer("WorldCB", _pTransform->GetLocalToWorldMatrix());

    //-----------
    // 色
    colorCb.color = _color;
    gpu::setConstantBuffer("ColorCB", colorCb);

    //====================
    // 入力レイアウト
    //====================
    gpu::setInputLayout(_pILPosUv);

    gpu::drawUserPrimitive(gpu::Primitive::LineList, static_cast<u32>(std::size(vertices)), vertices);
}
//---------------------------------------------------------------------------
//! スプライトを描画
//---------------------------------------------------------------------------
void Line::EndRender()
{
    gpu::setBlendState(gpu::commonStates().Opaque());
}
//---------------------------------------------------------------------------
//! GPU初期化
//---------------------------------------------------------------------------
void Line::InitGPU()
{
    //=====================================
    // メッシュを作る
    //=====================================

    _pVertexBuffer = gpu::createBuffer(vertices, D3D11_BIND_VERTEX_BUFFER);

    //=====================================
    // シェーダを生成
    //=====================================
    if(!_pVsShader)
        _pVsShader = gpu::createShader("dx11/vs_3d.fx", "main", "vs_5_0");
    if(!_pPsShader)
        _pPsShader = gpu::createShader("dx11/ps_flat.fx", "main", "ps_5_0");   // ピクセルシェーダー
    if(!_pILPosUv)
        _pILPosUv = gpu::createInputLayout(vertex::VertexPosUv::inputLayout,
                                           std::size(vertex::VertexPosUv::inputLayout));   // 2D入力レイアウト
}
}   // namespace entity::go
