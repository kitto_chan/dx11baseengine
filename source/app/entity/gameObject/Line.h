﻿//---------------------------------------------------------------------------
//!	@file	Line.h
//!	@brief	Line描画
//---------------------------------------------------------------------------
#pragma once
#include "entity/gameObject/GameObject.h"

namespace entity::go {
class Line : public GameObject
{
public:
    //! コンストラクタ
    Line(const std::string& name     = "Line",
          const float3&      pos      = float3(0.0f, 0.0f, 0.0f),
          const float3&      scale    = float3(1.0f, 1.0f, 1.0f),
          const float3&      rotation = float3(0.0f, 0.0f, 0.0f));
    ~Line();   //!< デストラクタ

    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    void SetColor(const float4& color) { _color = color; };
private:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    shr_ptr<gpu::Buffer> _pVertexBuffer;   //!< 頂点バッファ
    shr_ptr<gpu::Buffer> _pIndexBuffer;    //!< インデックスバッファ
    u32                  _indexCount;      //!< インデックス数

    float4 _color = float4(1.0f, 1.0f, 1.0f, 1.0f);   //!< テクスチャー色
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------

    bool OnInit() override;          //!< 初期化
    void OnUpdate() override;        //!< 更新
    void OnRender() override;        //!< 描画
    void OnRenderImgui() override;   //!< Imguiの描画

    void BeginRender();    //!< 2D描画を開始
    void EndRender();      //!< 2D描画を終わる

    void InitGPU(); //!< GPU初期化
};
}   // namespace entity::go
