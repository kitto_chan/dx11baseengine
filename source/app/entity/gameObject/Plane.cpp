﻿//---------------------------------------------------------------------------
//!	@file	Plane.cpp
//!	@brief	平面クラス
//---------------------------------------------------------------------------
#include "Plane.h"
#include "component/Transform.h"
#include "utilityGPU/Geometry.h"
#include "effect/Effect.h"
#include "component/physics/RigidBody.h"
#include "component/physics/Collider.h"
#include "manager/CameraManager.h"
namespace entity::go {
namespace {
struct PhongMaterialCB
{
    DirectX::XMFLOAT4 _ambient;
    DirectX::XMFLOAT4 _diffuse;
    DirectX::XMFLOAT3 _specular;
    f32               _specularShininess;
};

struct TextureMappingFlag
{
    int hasNormalMap = 0;
    int useless[3];
};

std::map<std::string, shr_ptr<gpu::Texture>> _textureMap;

cb::MeshColorCB    colorCb            = {};
TextureMappingFlag textureMappingFlag = {};
//====================
// GPU用
//====================
shr_ptr<gpu::Shader>      _pVsShader;
shr_ptr<gpu::Shader>      _pPsShader;
shr_ptr<gpu::InputLayout> _pILPosNorTanUv;   //!< 入力レイアウト

}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
Plane::Plane(const std::string& name, const float3& pos, const float3& scale, const float3& rotation)
: GameObject(name, pos, scale, rotation){};
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
Plane::~Plane()
{
}
//---------------------------------------------------------------------------
//! テクスチャを設定
//---------------------------------------------------------------------------
void Plane::SetTexture(std::string_view path)
{
    _pTexture = gpu::createTextureFromFile(path);
}
//---------------------------------------------------------------------------
//! テクスチャを設定
//---------------------------------------------------------------------------
void Plane::SetTexture(shr_ptr<gpu::Texture> path)
{
    _pTexture = path;
}
void Plane::SetNormalMap(std::string_view path)
{
    _pNormalMap = gpu::createTextureFromFile(path);
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool Plane::OnInit()
{
    effect::BasicEffectIns()->InitAll();
    InitGPU();

    textureMappingFlag.hasNormalMap = true;
    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void Plane::OnUpdate()
{
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void Plane::OnRender()
{
    BeginRender();
    RenderSprite();
    EndRender();
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void Plane::OnRenderImgui()
{
    imgui::ImguiColumn2FormatBegin("TextureWidth");
    ImGui::AlignTextToFramePadding();
    ImGui::Text("%d", _pTexture->desc().width_);
    imgui::ImguiColumn2FormatEnd();

    imgui::ImguiColumn2FormatBegin("TextureHeight");
    ImGui::AlignTextToFramePadding();
    ImGui::Text("%d", _pTexture->desc().height_);
    imgui::ImguiColumn2FormatEnd();
}
//---------------------------------------------------------------------------
//! 2D描画を開始
//---------------------------------------------------------------------------
void Plane::BeginRender()
{
    //-----------------------------------------------------------------
    // 設定
    //-----------------------------------------------------------------

    //====================
    // シェーダの設定
    //====================
    gpu::vs::setShader(_pVsShader);
    gpu::ps::setShader(_pPsShader);

    gpu::setVertexBuffer(0, _pVertexBuffer, sizeof(vertex::VertexPosNorTanUv));   // 頂点バッファ
    gpu::setIndexBuffer(_pIndexBuffer);                                           // インデックスバッファ

    //----------
    // カメラ行列(view & projection)
    //gpu::setConstantBuffer("CameraCB", cameraCB);

    //----------
    // ワールド行列
    gpu::setConstantBuffer("WorldCB", _pTransform->GetLocalToWorldMatrix());

    //-----------
    // 色
    colorCb.color = _color;
    gpu::setConstantBuffer("ColorCB", colorCb);

    gpu::setBlendState(gpu::commonStates().NonPremultiplied());

    cb::PhongMaterial _phongMaterial;
    _phongMaterial._ambient           = float4(0.5f, 0.5f, 0.5f, 1.0f);
    _phongMaterial._diffuse           = float4(1.0f, 1.0f, 1.0f, 1.0f);
    _phongMaterial._specular          = float3(0.2f, 0.2f, 0.2f);
    _phongMaterial._specularShininess = 1.0f;

    PhongMaterialCB phongMaterialCB{
        math::CastToXMFLOAT4(_phongMaterial._ambient),
        math::CastToXMFLOAT4(_phongMaterial._diffuse),
        math::CastToXMFLOAT3(_phongMaterial._specular),
        _phongMaterial._specularShininess
    };
    gpu::setConstantBuffer("PhongMaterialCB", phongMaterialCB);

    gpu::setConstantBuffer("TextureMappingFlagCB", textureMappingFlag);

    //====================
    // 入力レイアウト
    //====================
    gpu::setInputLayout(_pILPosNorTanUv);
}
//---------------------------------------------------------------------------
//! スプライトを描画
//---------------------------------------------------------------------------
void Plane::RenderSprite()
{
    gpu::ps::setTexture(0, _pTexture);
    gpu::ps::setTexture(1, _pNormalMap);
    gpu::ps::setSamplerState(0, gpu::commonStates().LinearWrap());
    gpu::drawIndexed(gpu::Primitive::TriangleList, _indexCount);
}
//---------------------------------------------------------------------------
//! スプライトを描画
//---------------------------------------------------------------------------
void Plane::EndRender()
{
    gpu::setBlendState(gpu::commonStates().Opaque());
}
void Plane::InitGPU()
{
    //=====================================
    // メッシュを作る
    //=====================================
    geometry::MeshData                     meshData = geometry::CreatePlane2D(0.0f, 0.0, 1.0f, 1.0f);
    std::vector<vertex::VertexPosNorTanUv> vertices;
    std::vector<s32>                       indices;

    vertices.resize(meshData.vertexData.size());
    indices.resize(meshData.indexData.size());

    for(u64 i = 0; i < meshData.vertexData.size(); i++) {
        vertices[i] = { meshData.vertexData[i].pos,
                        meshData.vertexData[i].nor,
                        meshData.vertexData[i].tan,
                        meshData.vertexData[i].tex };
    };
    for(u64 i = 0; i < meshData.indexData.size(); i++) {
        indices[i] = meshData.indexData[i];
    };

    _indexCount = (u32)meshData.indexData.size();

    _pVertexBuffer = gpu::createBuffer(
        { vertices.size() * sizeof(vertex::VertexPosNorTanUv),
          D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_IMMUTABLE },
        vertices.data());
    _pIndexBuffer = gpu::createBuffer(
        { indices.size() * sizeof(u32),
          D3D11_BIND_INDEX_BUFFER, D3D11_USAGE_IMMUTABLE },
        indices.data());

    //=====================================
    // シェーダを生成
    //=====================================
    if(!_pVsShader)
        _pVsShader = gpu::createShader("gameSource/Advance3d_VS.fx", "VS", "vs_5_0");
    if(!_pPsShader)
        _pPsShader = gpu::createShader("gameSource/PhongModel_PS.fx", "main", "ps_5_0");
    if(!_pILPosNorTanUv)
        _pILPosNorTanUv = gpu::createInputLayout(vertex::VertexPosNorTanUv::inputLayout,
                                                 std::size(vertex::VertexPosNorTanUv::inputLayout));   // 2D入力レイアウト
}
}   // namespace entity::go
