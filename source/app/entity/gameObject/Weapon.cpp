﻿#include "Weapon.h"
#include "effect/Effect.h"

#include "component/Transform.h"
#include "component/physics/Collider.h"
#include "component/physics/GhostBody.h"
#include "component/ModelRenderer.h"

namespace entity::go {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
Weapon::Weapon()
: GameObject()
{
    _name = "Weapon";
}
Weapon::Weapon(const std::string& name)
: GameObject(name)
{
    _name = name;
}
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
Weapon::~Weapon()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool Weapon::OnInit()
{
    _pModelRender = GetComponent<component::ModelRenderer>();
    _pGhostBody   = GetComponent<component::GhostBody>();

    return true;
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void Weapon::OnRender()
{
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void Weapon::OnUpdate()
{
    // Debug
    //   {
    //    const ImS32 s32_one = 1;
    //    ImGui::Begin("WeaponMatrix");
    //    ImGui::InputScalar("input s32", ImGuiDataType_S32, &_bindBoneId, &s32_one, NULL, "%d");
    //    ImGui::DragInt("m", &_bindBoneId);
    //    ImGui::End();
    //}

    if(_bindBoneId != -1) {
        matrix boneMatrix = GetParent()->GetComponent<component::ModelRenderer>()->GetBoneMatrix(_bindBoneId);

        if(_pModelRender) {
            _pModelRender->SetBoneMatrix(boneMatrix);
        }
        _pGhostBody->SetDefaultMatrix(boneMatrix);
    }
}
}   // namespace entity::go
