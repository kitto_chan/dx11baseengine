﻿//---------------------------------------------------------------------------
//!	@file	Weapon.h
//!	@brief	武器のクラス
//---------------------------------------------------------------------------
#pragma once
#include "GameObject.h"

namespace component {
class ModelRenderer;
class GhostBody;
}   // namespace component

namespace entity::go {
class Weapon : public GameObject
{
    friend class Player;

public:
    Weapon();                          //!< コンストラクタ
    Weapon(const std::string& name);   //!< コンストラクタ
    ~Weapon();                         //!< デストラクタ
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    void SetBindBoneId(const s32 id) { _bindBoneId = id; }   //!< バインディング用のボーンIDをセット

private:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    float3 _size = float3(1.f, 1.f, 1.f);   //!< キューブのサイズ

    s32 _bindBoneId = -1;   //!< バインディング用のボーンID

    raw_ptr<component::ModelRenderer> _pModelRender;   //!< ModelRender コンポーネント 参照
    raw_ptr<component::GhostBody>     _pGhostBody;     //!< GhostBody コンポーネント 参照

    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
    bool OnInit() override;     //!< 初期化
    void OnRender() override;   //!< 描画
    void OnUpdate() override;   //!< 更新
};
}   // namespace entity::go
