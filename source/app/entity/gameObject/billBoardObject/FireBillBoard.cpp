﻿//---------------------------------------------------------------------------
//!	@file	FireBillBoard.cpp
//!	@brief	ビルボード描画
//---------------------------------------------------------------------------
#include "FireBillBoard.h"
#include "component/Transform.h"
#include "utilityGPU/Geometry.h"
#include "effect/Effect.h"
#include "component/physics/RigidBody.h"
#include "component/physics/Collider.h"
#include "manager/CameraManager.h"
namespace entity::go {
namespace {
struct FireCB
{
    f32 time;
    f32 speed;
    s32 useless[2];
};
FireCB          _fireCB = {};
cb::MeshColorCB colorCb = {};
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
FireBillBoard::FireBillBoard(const std::string& name, const float3& pos, const float3& scale, const float3& rotation)
: SpriteBillBoard(name, pos, scale, rotation)
{
    _isWriteDepth = false;
};
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
FireBillBoard::~FireBillBoard()
{
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void FireBillBoard::OnUpdate()
{
    SpriteBillBoard::OnUpdate();

    _fireCB.time = timer::TimerIns()->TotalTime();
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void FireBillBoard::OnRenderImgui()
{
    SpriteBillBoard::OnRenderImgui();
}
//---------------------------------------------------------------------------
//! ピクセルシェーダを初期化
//---------------------------------------------------------------------------
bool FireBillBoard::InitPSShader()
{
    if(!_pPixelShader2D) {
        _pPixelShader2D = gpu::createShader("gameSource/effect/Fire_PS.fx", "PS", "ps_5_0");

        if(!_pPixelShader2D) return false;
    }

    return true;
}
//---------------------------------------------------------------------------
//! テクスチャーを初期化
//---------------------------------------------------------------------------
bool FireBillBoard::InitTexture()
{
    _pMainTexture  = gpu::createTextureFromFile("textures/shaders/fire01.dds");
    _pNoiseTexture = gpu::createTextureFromFile("textures/shaders/noise01.dds");
    _pMaskTexture  = gpu::createTextureFromFile("textures/shaders/alpha01.dds");

    if(!(_pMainTexture && _pNoiseTexture && _pMaskTexture))
        return false;

    return true;
}
//---------------------------------------------------------------------------
//! テクスチャーを設定
//---------------------------------------------------------------------------
void FireBillBoard::BindTexture()
{
    // テクスチャ設定
    gpu::ps::setTexture(0, _pMainTexture);
    gpu::ps::setTexture(1, _pNoiseTexture);
    gpu::ps::setTexture(2, _pMaskTexture);
    // サンプラ
    gpu::ps::setSamplerState(0, gpu::commonStates().LinearWrap());
    gpu::ps::setSamplerState(1, gpu::commonStates().LinearClamp());
}
//---------------------------------------------------------------------------
//! 頂点バッファを設定
//---------------------------------------------------------------------------
void FireBillBoard::BindExtraConstantBuffer()
{
    gpu::setConstantBuffer("FireCB", _fireCB);
}
}   // namespace entity::go
