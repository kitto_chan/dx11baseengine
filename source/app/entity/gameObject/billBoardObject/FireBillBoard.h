﻿//---------------------------------------------------------------------------
//!	@file	FireBillBoard.h
//!	@brief	火の描画
//---------------------------------------------------------------------------
#pragma once
#include "SpriteBillBoard.h"

namespace entity::go {
class FireBillBoard : public SpriteBillBoard
{
public:
    //! コンストラクタ
    FireBillBoard(const std::string& name     = "FireBillBoard",
                  const float3&      pos      = float3(0.0f, 0.0f, 0.0f),
                  const float3&      scale    = float3(1.0f, 1.0f, 1.0f),
                  const float3&      rotation = float3(0.0f, 0.0f, 0.0f));
    ~FireBillBoard();   //!< デストラクタ
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
protected:
    void OnUpdate() override;        //!< 更新
    void OnRenderImgui() override;   //!< Imguiの描画

private:
    bool InitPSShader() override;   //!< ピクセルシェーダ初期化
    bool InitTexture() override;    //!< テクスチャー初期化

    void BindTexture() override;               //<! テクスチャーをシェーダに設定
    void BindExtraConstantBuffer() override;   //<! 定数バッファを設定

    shr_ptr<gpu::Texture> _pNoiseTexture;   //!< ノイズ画像
    shr_ptr<gpu::Texture> _pMaskTexture;    //!< マスク画像
};
}   // namespace entity::go
