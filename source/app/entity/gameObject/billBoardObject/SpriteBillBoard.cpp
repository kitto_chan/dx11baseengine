﻿//---------------------------------------------------------------------------
//!	@file	SpriteBillBoard.cpp
//!	@brief	ビルボード描画
//---------------------------------------------------------------------------
#include "SpriteBillBoard.h"
#include "component/Transform.h"
#include "utilityGPU/Geometry.h"
#include "effect/Effect.h"
#include "component/physics/RigidBody.h"
#include "component/physics/Collider.h"
#include "manager/CameraManager.h"
namespace entity::go {
namespace {
std::map<std::string, shr_ptr<gpu::Texture>> _textureMap;

cb::MeshColorCB colorCb = {};
//====================
// DirectX 変数
//====================
shr_ptr<gpu::Shader> _pPixelShaderFlat;   //!< 2Dピクセルシェーダー(テクスチャーなし)
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
SpriteBillBoard::SpriteBillBoard(const std::string& name, const float3& pos, const float3& scale, const float3& rotation)
: GameObject(name, pos, scale, rotation){};
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
SpriteBillBoard::~SpriteBillBoard()
{
}
//---------------------------------------------------------------------------
//! テクスチャを設定
//---------------------------------------------------------------------------
void SpriteBillBoard::SetTexture(std::string_view path)
{
    auto itr = _textureMap.find(path.data());
    if(itr != _textureMap.end()) {
        _pMainTexture = itr->second;
        return;
    }
    _pMainTexture = gpu::createTextureFromFile(path);
    _textureMap.insert(std::pair(path, _pMainTexture));
}
//---------------------------------------------------------------------------
//! テクスチャを設定
//---------------------------------------------------------------------------
void SpriteBillBoard::SetTexture(shr_ptr<gpu::Texture> path)
{
    _pMainTexture = path;
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool SpriteBillBoard::OnInit()
{
    if(!InitVSShader()) return false;

    if(!InitPSShader()) return false;

    if(!InitInputLayout()) return false;

    if(!InitTexture()) return false;

    // Debug 用
    if(!_pPixelShaderFlat)
        _pPixelShaderFlat = gpu::createShader("dx11/ps_flat.fx", "main", "ps_5_0");

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void SpriteBillBoard::OnUpdate()
{
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void SpriteBillBoard::OnRender()
{
    BeginRender();
    RenderSprite();
    EndRender();
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void SpriteBillBoard::OnRenderImgui()
{
    imgui::ImguiColumn2FormatBegin("Color");
    ImGui::ColorEdit4("##Color", (float*)&_color, ImGuiColorEditFlags_NoInputs);
    imgui::ImguiColumn2FormatEnd();

    imgui::ImguiColumn2FormatBegin("Width");
    ImGui::Text("%d", _pMainTexture->desc().width_);
    imgui::ImguiColumn2FormatEnd();

    imgui::ImguiColumn2FormatBegin("Height");
    ImGui::Text("%d", _pMainTexture->desc().height_);
    imgui::ImguiColumn2FormatEnd();

    imgui::ImguiCheckBox("IsRenderDebug", _isRenderDebug);
}
//---------------------------------------------------------------------------
//! 頂点シェーダ初期化
//---------------------------------------------------------------------------
bool SpriteBillBoard::InitVSShader()
{
    if(!_pVertexShader2D) {
        _pVertexShader2D = gpu::createShader("dx11/vs_3d.fx", "main", "vs_5_0");   // 2D頂点シェーダー

        if(!_pVertexShader2D) return false;
    }
    return true;
}
//---------------------------------------------------------------------------
//! ピクセルシェーダ初期化
//---------------------------------------------------------------------------
bool SpriteBillBoard::InitPSShader()
{
    if(!_pPixelShader2D) {
        _pPixelShader2D = gpu::createShader("dx11/ps_texture.fx", "main", "ps_5_0");   // 2Dピクセルシェーダー

        if(!_pPixelShader2D) return false;
    }
    return true;
}
//---------------------------------------------------------------------------
//! 入力レイアウト初期化
//---------------------------------------------------------------------------
bool SpriteBillBoard::InitInputLayout()
{
    if(!_pIntputLayout2D) {
        _pIntputLayout2D = gpu::createInputLayout(vertex::VertexPosUv::inputLayout,
                                                  std::size(vertex::VertexPosUv::inputLayout));   // 2D入力レイアウト

        if(!_pIntputLayout2D) return false;
    }
    return true;
}
//---------------------------------------------------------------------------
//! テクスチャーを設定
//---------------------------------------------------------------------------
void SpriteBillBoard::BindTexture()
{
    // テクスチャ設定
    gpu::ps::setTexture(0, _pMainTexture);
    gpu::ps::setSamplerState(0, gpu::commonStates().PointWrap());
}

//---------------------------------------------------------------------------
//! 2D描画を開始
//---------------------------------------------------------------------------
void SpriteBillBoard::BeginRender()
{
    //-----------------------------------------------------------------
    // 設定
    //-----------------------------------------------------------------

    //====================
    // シェーダの設定
    //====================
    gpu::vs::setShader(_pVertexShader2D);
    gpu::ps::setShader(_pPixelShader2D);

    gpu::setIndexBuffer(nullptr);       // インデックスバッファ
    gpu::setVertexBuffer(0, nullptr);   // 頂点バッファ

    //----------
    // ワールド行列
    auto matWorld = _pTransform->GetLocalToWorldMatrix();

    // 親のスケールは使わない
    // matrix * inv scaleより速いはず
    if(_isUseLocalScaleOnly) {
        float3 localScale = _pTransform->GetScale();
        matWorld._11      = localScale.f32[0];
        matWorld._22      = localScale.f32[1];
        matWorld._33      = localScale.f32[2];
    }
    gpu::setConstantBuffer("WorldCB", matWorld);

    //-----------
    // 色
    colorCb.color = _color;
    gpu::setConstantBuffer("ColorCB", colorCb);

    BindExtraConstantBuffer();

    gpu::setBlendState(gpu::commonStates().NonPremultiplied());

    // デプスON / デプス書き込みOFF
    dx11::setDepthStencilState(dx11::commonStates().DepthRead());
    //====================
    // 入力レイアウト
    //====================
    gpu::setInputLayout(_pIntputLayout2D);
}
//---------------------------------------------------------------------------
//! スプライトを描画
//---------------------------------------------------------------------------
void SpriteBillBoard::RenderSprite()
{
    //-----------------------------------------------------------------
    // ピクセル座標系、スクリーン座標系、変換行列を作成
    //-----------------------------------------------------------------
    auto matCameraWorld = manager::GetCurrentCameraMgr()->GetCurrentCamera()->GetLocalToWorldMatrix();   // カメラのワールド行列

    float3 position = float3(0.0f, 0.0f, 0.0f);
    float3 rightDir = matCameraWorld._11_12_13;
    float3 upDir    = matCameraWorld._21_22_23;

    static vertex::VertexPosUv vertices[]{
        { DirectX::XMFLOAT3(-1.0f, +1.0f, 0.0f), DirectX::XMFLOAT2(0.0f, 0.0f) },
        { DirectX::XMFLOAT3(+1.0f, +1.0f, 0.0f), DirectX::XMFLOAT2(1.0f, 0.0f) },
        { DirectX::XMFLOAT3(-1.0f, -1.0f, 0.0f), DirectX::XMFLOAT2(0.0f, 1.0f) },
        { DirectX::XMFLOAT3(+1.0f, -1.0f, 0.0f), DirectX::XMFLOAT2(1.0f, 1.0f) },
    };

    //float3 scale = _pTransform->GetScale();
    float3 p[4];
    p[0] = position - rightDir + upDir;   // 左上
    p[1] = position + rightDir + upDir;   // 右上
    p[2] = position - rightDir - upDir;   // 左下
    p[3] = position + rightDir - upDir;   // 右下

    for(u32 i = 0; i < 4; ++i) {
        vertices[i].pos.x = p[i].x;
        vertices[i].pos.y = p[i].y;
        vertices[i].pos.z = p[i].z;
    }

    BindTexture();

    if(_isUseDepth) {
        if(_isWriteDepth) {
            // デプスON / デプス書き込みON
            dx11::setDepthStencilState(dx11::commonStates().DepthDefault());
        }
        else {
            // デプスON / デプス書き込みOFF
            dx11::setDepthStencilState(dx11::commonStates().DepthRead());
        }
    }
    else {
        // デプスOFF
        dx11::setDepthStencilState(dx11::commonStates().DepthNone());
    }

    gpu::drawUserPrimitive(gpu::Primitive::TriangleStrip, 4, vertices);

    if(_isRenderDebug) {
        //==========================================
        // デバッグ画像の枠を描画する
        //==========================================
        constexpr u32 lineIndices[]{
            0, 1, 3, 2, 0
        };
        gpu::ps::setShader(_pPixelShaderFlat);
        gpu::drawIndexedUserPrimitive(gpu::Primitive::LineStrip, 4, 5, lineIndices, vertices);
    }
}
//---------------------------------------------------------------------------
//! スプライトを描画
//---------------------------------------------------------------------------
void SpriteBillBoard::EndRender()
{
    gpu::setBlendState(gpu::commonStates().Opaque());
    dx11::setDepthStencilState(dx11::commonStates().DepthDefault());
}
}   // namespace entity::go
