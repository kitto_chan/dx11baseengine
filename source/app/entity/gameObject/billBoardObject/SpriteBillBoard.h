﻿//---------------------------------------------------------------------------
//!	@file	SpriteBillBoard.h
//!	@brief	ビルボード描画
//---------------------------------------------------------------------------
#pragma once
#include "entity/gameObject/GameObject.h"

namespace entity::go {
class SpriteBillBoard : public GameObject
{
public:
    //! コンストラクタ
    SpriteBillBoard(const std::string& name     = "SpriteBillBoard",
                    const float3&      pos      = float3(0.0f, 0.0f, 0.0f),
                    const float3&      scale    = float3(1.0f, 1.0f, 1.0f),
                    const float3&      rotation = float3(0.0f, 0.0f, 0.0f));
    ~SpriteBillBoard();   //!< デストラクタ

    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    void SetTexture(std::string_view path);                   //!< 描画テクスチャーを設定
    void SetTexture(shr_ptr<gpu::Texture> path);              //!< 描画テクスチャーを設定
    void SetColor(const float4& color) { _color = color; };   //!< 色設定

protected:
    bool OnInit() override;          //!< 初期化
    void OnUpdate() override;        //!< 更新
    void OnRender() override;        //!< 描画
    void OnRenderImgui() override;   //!< Imguiの描画

    virtual bool InitVSShader();                   //!< 頂点シェーダ初期化
    virtual bool InitPSShader();                   //!< ピクセルシェーダ初期化
    virtual bool InitTexture() { return true; };   //!< テクスチャー初期化
    virtual bool InitInputLayout();                //!< 入力レイアウト初期化

    virtual void BindTexture();                 //!<  テクスチャーをシェーダに設定
    virtual void BindExtraConstantBuffer(){};   //<! 定数バッファを設定

    float4 _color = float4(1.0f, 1.0f, 1.0f, 1.0f);   //!< テクスチャー色

    shr_ptr<gpu::Shader>      _pVertexShader2D;   //!< 2D頂点シェーダー
    shr_ptr<gpu::Shader>      _pPixelShader2D;    //!< 2Dピクセルシェーダー(テクスチャーあり)
    shr_ptr<gpu::InputLayout> _pIntputLayout2D;   //!< 2D入力レイアウト

    shr_ptr<gpu::Texture> _pMainTexture;   //!< メインテクスチャー
protected:
    bool _isRenderDebug = false;   //!< デバッグ描画かどうか

    bool _isWriteDepth        = true;    //!< デプス書き込み ON / OFF
    bool _isUseDepth          = true;    //!< デプスON / OFF
    bool _isUseLocalScaleOnly = false;   //!< 親のスケールを使うか

private:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    void BeginRender();    //!< 2D描画を開始
    void RenderSprite();   //!< スプライト描画
    void EndRender();      //!< 2D描画を終わる
};
}   // namespace entity::go
