﻿//---------------------------------------------------------------------------
//!	@file	BaseCamera.h
//!	@brief	カメラの基底クラス
//---------------------------------------------------------------------------
#include "BaseCamera.h"
#include "component/Transform.h"

namespace entity::go {
cb::CameraCB _cameraCb;
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
BaseCamera::BaseCamera()
{
    _name = "BaseCamera";

    float3 to = { 0.0f, 0.0f, 0.0f };
    float3 up = { 0.0f, 1.0f, 0.0f };
    _pTransform->SetPosition(float3(0.0f, 0.0f, -1.0f));
    //_pTransform->LookAt(to, up);
}
//---------------------------------------------------------------------------
//! コンストラクタ
//! [in] name ゲームオブジェクトの名前
//---------------------------------------------------------------------------
BaseCamera::BaseCamera(const std::string& name)
: GameObject(name)
{
    float3 to = { 0.0f, 0.0f, 0.0f };
    float3 up = { 0.0f, 1.0f, 0.0f };
    _pTransform->SetPosition(float3(0.0f, 0.0f, -1.0f));
    //_pTransform->LookAt(to, up);
}
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
BaseCamera::~BaseCamera()
{
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void BaseCamera::OnUpdate()
{
    Shake();
}   // namespace entity::go
//---------------------------------------------------------------------------
//! ローカルからワールド変換
//---------------------------------------------------------------------------
matrix BaseCamera::GetLocalToWorldMatrix() const
{
    return _pTransform->GetLocalToWorldMatrix();
}
//---------------------------------------------------------------------------
//! ビュー行列
//---------------------------------------------------------------------------
matrix BaseCamera::GetViewMatrix() const
{
    return _pTransform->GetWorldToLocalMatrix();
}
//---------------------------------------------------------------------------
//! 投影行列
//---------------------------------------------------------------------------
matrix BaseCamera::GetProjMatrix() const
{
    // エディターモードにしたら、Aspectは Imgui のcontentのAspect
    if(SystemSettingsMgr()->IsEditorMode()) {
        float2 sceneViewPortSize   = SystemSettingsMgr()->GetSceneViewportSize();
        f32    sceneViewportAspect = sceneViewPortSize.f32[0] / sceneViewPortSize.f32[1];
        return math::perspectiveFovRH(_fovY, sceneViewportAspect, _nearZ, _farZ);
    }

    return math::perspectiveFovRH(_fovY, _aspect, _nearZ, _farZ);
}
//---------------------------------------------------------------------------
//! カメラの対象を取得
//---------------------------------------------------------------------------
void BaseCamera::SetTarget(raw_ptr<GameObject> pTarget)
{
    _pTarget = pTarget;
    _pTransform->FaceAt(pTarget->GetPosition());
}
//---------------------------------------------------------------------------
//! カメラの振動時間を設定
//! @param frame 振動のフレーム（1秒 = 60frame）
//---------------------------------------------------------------------------
void BaseCamera::SetShakeTime(s32 frame)
{
    _shakeTime = frame;
}
//---------------------------------------------------------------------------
//! カメラの定数バッファを設定
//---------------------------------------------------------------------------
void BaseCamera::SetCameraCB()
{
    _cameraCb.matProj = GetProjMatrix();
    _cameraCb.matView = GetViewMatrix();
    _cameraCb.eyePos  = GetPosition();

    gpu::setConstantBuffer("CameraCB", _cameraCb);
}
//---------------------------------------------------------------------------
//! カメラ対象のtransformを取得
//---------------------------------------------------------------------------
raw_ptr<component::Transform> BaseCamera::GetTargetTransform() const
{
    return _pTarget->GetComponent<component::Transform>();
}
//---------------------------------------------------------------------------
//!< カメラを振動する
//---------------------------------------------------------------------------
void BaseCamera::Shake()
{
    f32 shakeY = math::GetRandomF(-0.2f, 0.2f);
    if(_shakeTime >= 0) {
        _shakeTime--;

        _pTransform->SetPositionY(_pTransform->GetPosition().y + shakeY);
    }
}
}   // namespace entity::go
