﻿//---------------------------------------------------------------------------
//!	@file	BaseCamera.h
//!	@brief	カメラの基底クラス
//---------------------------------------------------------------------------
#pragma once
#include "entity/gameObject/GameObject.h"
namespace component {
class Transform;
};

namespace manager {
class CameraManager;
}

namespace entity::go {
class BaseCamera : public GameObject
{
    friend class manager::CameraManager;

    struct ShakeInfo
    {
        float3 originPos;   //!< 本来の座標
    };

public:
    BaseCamera();
    BaseCamera(const std::string& name);
    virtual ~BaseCamera();

    //---------------------------------------------------------------------------
    //! public 関数
    //---------------------------------------------------------------------------

    // 行列
    matrix GetLocalToWorldMatrix() const;   //!< ローカルからワールド変換
    matrix GetViewMatrix() const;           //!< ビュー行列
    matrix GetProjMatrix() const;           //!< 投影行列

    void SetTarget(raw_ptr<GameObject> pTarget);   //!< カメラの対象を取得

    void SetShakeTime(s32 frame);   //!< カメラの振動時間を設定

    //! カメラ制御可能かどうかを設定する
    void SetIsControllable(bool isControllable) { _isControllable = isControllable; }

    void SetCameraCB();   //!< カメラ定数バッファ設定
protected:
    //---------------------------------------------------------------------------
    // protected 変数
    //---------------------------------------------------------------------------
    f32 _fovY   = 45.0f * (math::PI / 180.0f);   //!< 画角
    f32 _aspect = 16.0f / 9.0f;                  //!< アスペクト
    f32 _nearZ  = 0.01f;                         //!< 最近距離
    f32 _farZ   = 1000.0f;                       //!< 最遠距離

    s32                 _shakeTime = -1;   //!< カメラ振動のフレーム数 -1以下は停止
    raw_ptr<GameObject> _pTarget;          //!< カメラの対象

    bool _isControllable = true;   //!< このカメラ制御可能ですか
    //---------------------------------------------------------------------------
    // protected 関数
    //---------------------------------------------------------------------------
    virtual void OnUpdate() override;   //!< 更新

    //! カメラ対象のtransformを取得
    raw_ptr<component::Transform> GetTargetTransform() const;

private:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
    void Shake();   //!< カメラを振動する
};
}   // namespace entity::go
