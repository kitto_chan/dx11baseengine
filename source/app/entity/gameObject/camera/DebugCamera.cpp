﻿//---------------------------------------------------------------------------
//!	@file	DubugCamera.cpp
//!	@brief	デバッグ用カメラ
//---------------------------------------------------------------------------
#include "utility/SystemSettings.h"

#include "DebugCamera.h"
#include "component/Transform.h"

namespace entity::go {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
DebugCamera::DebugCamera()
{
    _name = "Debug";
}
//---------------------------------------------------------------------------
//! コンストラクタ
//! [in] name ゲームオブジェクトの名前
//---------------------------------------------------------------------------
DebugCamera::DebugCamera(const std::string& name)
: BaseCamera(name)
{
}
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
DebugCamera::~DebugCamera()
{
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void DebugCamera::OnUpdate()
{
    if(ImGui::IsAnyItemHovered() || ImGui::IsAnyItemFocused() || ImGui::IsAnyItemActive()) return;
    if(ImGuizmo::IsUsing()) return;

    if(SystemSettingsMgr()->IsPauseState()) {
        if(!MouseMgr()->IsAbsoluteMode()) {
            MouseMgr()->SetAbsoluteMode();
        }

		if(!MouseMgr()->GetMouse()->IsVisible()) {
            MouseMgr()->GetMouse()->SetVisible(true);
		}
    }
    else {
        return;
    }
  
    // 現在のマウス座標
    int2 currentMousePos = { MouseMgr()->GetPosX(),
                             MouseMgr()->GetPosY() };

    // 移動差分
    f32 dx      = static_cast<f32>(currentMousePos.x - lastPos.x) * 0.5f;
    f32 dy      = static_cast<f32>(currentMousePos.y - lastPos.y) * 0.5f;
    f32 wheelDx = static_cast<f32>(MouseMgr()->GetScrollWheelValue()) * 0.05f;

    // マウスホイールを押している時
    if(MouseMgr()->IsHeldMiddleButton()) {
        _pTransform->Translate(_pTransform->GetUpAxis(), timer::DeltaTime() * dy);
        _pTransform->Translate(_pTransform->GetRightAxis(), timer::DeltaTime() * -dx);
    }
    // マウスホイールをスクロール時
    else if(wheelDx != 0) {
        MoveFB(wheelDx);
    }
    // マウス左ボタンを押している時
    else if(MouseMgr()->IsHeldLeftButton()) {
        // マウス ←/→ 移動は回転
        _pTransform->RotateYAxis(-dx * timer::DeltaTime() * _rotSpeed);
        // マウス ↑/↓ 移動は前進/後退
        MoveFB(-dy);
    }
    // マウス右ボタンを押している時
    else if(MouseMgr()->IsHeldRightButton()) {
        LookControl(-dx, -dy);
    }

    // ラストフレームのマウス座標を保存
    lastPos.x = MouseMgr()->GetPosX();
    lastPos.y = MouseMgr()->GetPosY();

    BaseCamera::OnUpdate();
}
//---------------------------------------------------------------------------
//! 移動処理
//---------------------------------------------------------------------------
void DebugCamera::MoveControl()
{
    //前進
    if(KeyboardMgr()->IsKeyDown(DirectX::Keyboard::W)) {
        MoveFB(_moveSpeed);
    }
    //後退
    if(KeyboardMgr()->IsKeyDown(DirectX::Keyboard::S)) {
        MoveFB(_moveSpeed * -1);
    }
    //左移動
    if(KeyboardMgr()->IsKeyDown(DirectX::Keyboard::A)) {
        MoveLR(_moveSpeed * -1);
    }
    //右移動
    if(KeyboardMgr()->IsKeyDown(DirectX::Keyboard::D)) {
        MoveLR(_moveSpeed);
    }
}
//---------------------------------------------------------------------------
//! 視点処理
//---------------------------------------------------------------------------
void DebugCamera::LookControl(f32 dx, f32 dy)
{
    f32 dt = timer::TimerIns()->DeltaTime();

    // 回転
    f32 rotRadX   = dy * dt * _rotSpeed;
    f32 rotationX = _pTransform->GetRotation().x;
    rotationX += rotRadX;

    // "倒立"しないように
    // 回転X角度　は [PI * 7/ 18] の範囲で制限します
    f32 rotRange = DirectX::XM_PI * 7 / 18;
    rotationX    = std::clamp(rotationX, -rotRange, rotRange);

    _pTransform->SetRotationX(rotationX);
    _pTransform->RotateYAxis(dx * dt * _rotSpeed);
}
//---------------------------------------------------------------------------
//! 前後移動(F-Forward B-Backward)
//---------------------------------------------------------------------------
void DebugCamera::MoveFB(f32 speed)
{
    f32 dt = timer::TimerIns()->DeltaTime();
    _pTransform->Translate(_pTransform->GetForwardAxis(), dt * speed);
}
//---------------------------------------------------------------------------
//! 左右移動(L-Left R-Right)
//---------------------------------------------------------------------------
void DebugCamera::MoveLR(f32 speed)
{
    f32 dt = timer::TimerIns()->DeltaTime();
    _pTransform->Translate(_pTransform->GetRightAxis(), dt * speed);
}
}   // namespace entity::go
