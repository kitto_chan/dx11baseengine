﻿//---------------------------------------------------------------------------
//!	@file	FirstPersonCamera.cpp
//!	@brief	一人称視点クラス
//---------------------------------------------------------------------------
#include "FirstPersonCamera.h"
#include "component/Transform.h"

namespace entity::go {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
FirstPersonCamera::FirstPersonCamera()
{
    _name = "FirstPersonCamera";
}
//---------------------------------------------------------------------------
//! コンストラクタ
//! [in] name ゲームオブジェクトの名前
//---------------------------------------------------------------------------
FirstPersonCamera::FirstPersonCamera(const std::string& name)
: BaseCamera(name)
{
}
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
FirstPersonCamera::~FirstPersonCamera()
{
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void FirstPersonCamera::OnUpdate()
{
    if(_isControllable) {
        MoveControl();
        if(!MouseMgr()->IsRelativeMode()) {
            MouseMgr()->SetRelativeMode();
		}
        LookControl();
    }
    BaseCamera::OnUpdate();
}
//---------------------------------------------------------------------------
//! 移動処理
//---------------------------------------------------------------------------
void FirstPersonCamera::MoveControl()
{
    //前進
    if(KeyboardMgr()->IsKeyDown(DirectX::Keyboard::W)) {
        MoveFB(_moveSpeed);
    }
    //後退
    if(KeyboardMgr()->IsKeyDown(DirectX::Keyboard::S)) {
        MoveFB(_moveSpeed * -1);
    }
    //左移動
    if(KeyboardMgr()->IsKeyDown(DirectX::Keyboard::A)) {
        MoveLR(_moveSpeed * -1);
    }
    //右移動
    if(KeyboardMgr()->IsKeyDown(DirectX::Keyboard::D)) {
        MoveLR(_moveSpeed);
    }

    // 前進
    //   const auto& m     = math::identity();
    //   float3      right = float3{ m._11, m._12, m._13 };           // 右方向のベクトル
    //   float3      top   = float3{ m._21, m._22, m._23 };           // 上方向のベクトル
    //   float3      front = float3{ m._31, m._32, m._33 } * -1.0f;   // 前方向のベクトル

    //   float3 dir(0.0f, 0.0f, 0.0f);
    //if(input::KeyboardMgr()->IsKeyDown(DirectX::Keyboard::W)) {
    //       dir += front;
    //   }
    //   //後退
    //   if(input::KeyboardMgr()->IsKeyDown(DirectX::Keyboard::S)) {
    //       dir -= front;
    //   }
    //   //左移動
    //   if(input::KeyboardMgr()->IsKeyDown(DirectX::Keyboard::A)) {
    //       dir -= right;
    //   }
    //   //右移動
    //   if(input::KeyboardMgr()->IsKeyDown(DirectX::Keyboard::D)) {
    //       dir += right;
    //   }
    //   if(dot(dir, dir).x > 0.0f) {
    //       dir = normalize(dir) * 0.2f;
    //   }

    //// 平行移動の場合は位置と注視点を同時に移動

    //   position_ += dir * 100.0f * deltaTime;
    //   lookAt_ += dir * 100.0f * deltaTime;
}
//---------------------------------------------------------------------------
//! 視点処理
//---------------------------------------------------------------------------
void FirstPersonCamera::LookControl()
{
    //if(!MouseMgr()->IsRelativeMode()) return;

    f32 dt = timer::TimerIns()->DeltaTime();

    // 回転
    f32 rotRadX   = -MouseMgr()->GetPosY() * dt * _rotSpeed;
    f32 rotationX = _pTransform->GetRotation().x;
    rotationX += rotRadX;

    // "倒立"しないように
    // 回転X角度　は [PI * 7/ 18] の範囲で制限します
    f32 rotRange = DirectX::XM_PI * 7 / 18;
    rotationX    = std::clamp(rotationX, -rotRange, rotRange);

    _pTransform->SetRotationX(rotationX);
    _pTransform->RotateYAxis(-MouseMgr()->GetPosX() * dt * _rotSpeed);
}
//---------------------------------------------------------------------------
//! 前後移動(F-Forward B-Backward)
//---------------------------------------------------------------------------
void FirstPersonCamera::MoveFB(f32 speed)
{
    f32 dt = timer::TimerIns()->DeltaTime();
    _pTransform->Translate(_pTransform->GetForwardAxis(), dt * speed);
}
//---------------------------------------------------------------------------
//! 左右移動(L-Left R-Right)
//---------------------------------------------------------------------------
void FirstPersonCamera::MoveLR(f32 speed)
{
    f32 dt = timer::TimerIns()->DeltaTime();
    _pTransform->Translate(_pTransform->GetRightAxis(), dt * speed);
}
}   // namespace entity::go
