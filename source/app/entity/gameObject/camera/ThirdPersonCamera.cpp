﻿//---------------------------------------------------------------------------
//!	@file	ThirdPersonCamera.cpp
//!	@brief	三人称視点クラス
//---------------------------------------------------------------------------
#include "ThirdPersonCamera.h"
#include "component/Transform.h"

namespace entity::go {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
ThirdPersonCamera::ThirdPersonCamera()
{
    _name = "ThirdPersonCamera";
}
//---------------------------------------------------------------------------
//! コンストラクタ
//! [in] name ゲームオブジェクトの名前
//---------------------------------------------------------------------------
ThirdPersonCamera::ThirdPersonCamera(const std::string& name)
: BaseCamera(name)
{
}
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
ThirdPersonCamera::~ThirdPersonCamera()
{
}
bool ThirdPersonCamera::OnInit()
{
    ASSERT_MESSAGE(_pTarget, "目標が設定されてない");
    Approach(-MouseMgr()->GetScrollWheelValue() / 120.f * 1.0f);

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void ThirdPersonCamera::OnUpdate()
{
    if(_isControllable) {
        if(!MouseMgr()->IsRelativeMode()) {
            MouseMgr()->SetRelativeMode();
        }
        HandleControl();
    }
}
//---------------------------------------------------------------------------
//! ターゲットを中心点して、垂直方向のラジアン角
//---------------------------------------------------------------------------
void ThirdPersonCamera::RotateXAroundTarget(f32 rad)
{
    float3 rotation  = _pTransform->GetRotation();
    f32    rotationX = rotation.x + rad;

    ClampRotateXAroundTargetRatation(rotationX);
    rotation.x = rotationX;

    RotateAroundTarget(rotation);
}
//---------------------------------------------------------------------------
//! ターゲットを中心点して、水平回転のラジアン角
//---------------------------------------------------------------------------

void ThirdPersonCamera::RotateYAroundTarget(f32 rad)
{
    float3 rotation = _pTransform->GetRotation();

    // y軸のラジアン角度は[ -XM_PI から　 XM_PI] の間にする
    rotation.y = DirectX::XMScalarModAngle(rotation.y + rad);

    RotateAroundTarget(rotation);
}
//---------------------------------------------------------------------------
//! カメラとターゲットの距離を調整する
//---------------------------------------------------------------------------
void ThirdPersonCamera::Approach(f32 dist)
{
    _distance += dist;
    //! 距離を制限する
    _distance = std::clamp(_distance, _minDist, _maxDist);

    _pTransform->SetPosition(GetTargetTransform()->GetPosition() + float3(0.0f, 0.5f, 0.0f));
    _pTransform->Translate(_pTransform->GetForwardAxis(), -_distance);
}
//---------------------------------------------------------------------------
//! ターゲットを中心点して、垂直回転のラジアン角を設定する
//---------------------------------------------------------------------------
void ThirdPersonCamera::SetRotateXAroundTarget(f32 rad)
{
    float3 rotation  = _pTransform->GetRotation();
    f32    rotationX = rad;

    ClampRotateXAroundTargetRatation(rotationX);
    rotation.x += rotationX;

    RotateAroundTarget(rotation);
}
//---------------------------------------------------------------------------
//!ターゲットを中心点して、水平回転のラジアン角を設定する
//---------------------------------------------------------------------------
void ThirdPersonCamera::SetRotateYAroundTarget(f32 rad)
{
    float3 rotation = _pTransform->GetRotation();

    // y軸のラジアン角度は[ -XM_PI から XM_PI] の間にする
    rotation.y = DirectX::XMScalarModAngle(rad);

    RotateAroundTarget(rotation);
}
//---------------------------------------------------------------------------
//! 距離をセット
//---------------------------------------------------------------------------
void ThirdPersonCamera::SetDistance(f32 dist)
{
    _distance = dist;
}
//---------------------------------------------------------------------------
//! 最大距離と最小距離をセット
//---------------------------------------------------------------------------
void ThirdPersonCamera::SetDistanceMinMax(f32 minDist, f32 maxDist)
{
    _minDist = minDist;
    _maxDist = maxDist;
}
//---------------------------------------------------------------------------
//! ターゲットを中心点して、回転のラジアン角
//---------------------------------------------------------------------------
void ThirdPersonCamera::RotateAroundTarget(const float3& rotation)
{
    _pTransform->SetRotation(rotation);
    _pTransform->SetPosition(GetTargetTransform()->GetPosition());
    _pTransform->Translate(_pTransform->GetForwardAxis(), -_distance);
}
//---------------------------------------------------------------------------
//! 垂直方向のラジアン角は範囲内に収める
//---------------------------------------------------------------------------
void ThirdPersonCamera::ClampRotateXAroundTargetRatation(f32& x)
{
    // X軸のラジアン角度は[ -math::PI / 3.f から　 0] の間にする
    f32 maxRotation = 0.0f;
    f32 minRotation = -math::PI / 3.f;

    x = std::clamp(x, minRotation, maxRotation);
}
//---------------------------------------------------------------------------
//!< カメラ制御処理
//---------------------------------------------------------------------------
void ThirdPersonCamera::HandleControl()
{
#define mode 1
#if mode == 0
    if(!MouseMgr()->IsRelativeMode()) return;
    // 後ろから映る
    float3 targetPos = GetTargetTransform()->GetPosition();
    float3 targetRot = GetTargetTransform()->GetRotation();
    float3 pos       = _pTransform->GetPosition();

    pos.f32[0] = targetPos.x + _distance * sinf(targetRot.y + math::D2R(180.0f));
    pos.f32[2] = targetPos.z + _distance * cosf(targetRot.y + math::D2R(180.0f));
    pos.f32[1] = targetPos.y + 5.0f;
    _pTransform->SetPosition(pos);
    _pTransform->LookAt(targetPos, float3(0.0, 1.0f, 0.0f));
#elif mode == 1
    if(!MouseMgr()->IsRelativeMode()) return;
    // 自由角度調整ができる
    f32 dt = timer::TimerIns()->DeltaTime();

    // ゲームパッド処理
    if(input::IsGamePadConnected()) {
        static f32 padX = 0.0f;
        static f32 padY = 0.0f;

        static constexpr auto gamePadThumbSticks = [](f32 value, f32 thumbSticks) {
            constexpr f32 SPEED           = 0.5f;
            constexpr f32 MAX_SENSITIVITY = 10.0f;

            f32 thumbSticksSpeed = thumbSticks * SPEED;

            if(thumbSticks == 0) {
                return 0.0f;
            }

            if(value == 0) {
                value += thumbSticksSpeed;
            }
            else if(value < 0 && thumbSticks < 0) {
                value += thumbSticksSpeed;
            }
            else if(value > 0 && thumbSticks > 0) {
                value += thumbSticksSpeed;
            }
            else {
                value = 0.0f;
            }

            return std::clamp(value, -MAX_SENSITIVITY, MAX_SENSITIVITY);
        };

        padY = gamePadThumbSticks(padY, input::GamePadMgr()->GetRightThumbSticksY());
        padX = gamePadThumbSticks(padX, input::GamePadMgr()->GetRightThumbSticksX());

        RotateXAroundTarget(padY * dt * 0.3f);
        RotateYAroundTarget(padX * dt * 0.3f);
    }

    // マウス
    RotateXAroundTarget(-MouseMgr()->GetPosY() * dt * 0.3f);
    RotateYAroundTarget(-MouseMgr()->GetPosX() * dt * 0.3f);
#endif
    Approach(-MouseMgr()->GetScrollWheelValue() / 120.f * 0.5f);

    BaseCamera::OnUpdate();

    if(_pTarget) {
        btVector3 targetPos = math::CastToBtVec3(_pTransform->GetPosition());
        btVector3 seltPos   = math::CastToBtVec3(_pTarget->GetComponent<component::Transform>()->GetPosition());

        btCollisionWorld::ClosestRayResultCallback rayCallback(seltPos, targetPos);

        // 壁のレイヤーと判定する
        rayCallback.m_collisionFilterMask  = Layer::Wall;
        rayCallback.m_collisionFilterGroup = physics::GetCollisionGroup(Layer::Wall);

        physics::PhysicsEngine::instance()->dynamicsWorld()->rayTest(
            seltPos,
            targetPos,
            rayCallback);

        if(rayCallback.hasHit()) {
            _pTransform->SetPosition(math::CastToFloat3(rayCallback.m_hitPointWorld));
        }
        _pTransform->AddPositionY(1.5f);
    }
}
}   // namespace entity::go
