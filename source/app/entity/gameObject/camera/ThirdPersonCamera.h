﻿//---------------------------------------------------------------------------
//!	@file	ThirdPersonCamera.h
//!	@brief	三人称視点クラス
//---------------------------------------------------------------------------
#pragma once
#include "BaseCamera.h"
namespace component {
class Transform;
};

namespace entity::go {
class ThirdPersonCamera : public BaseCamera
{
public:
    ThirdPersonCamera();
    ThirdPersonCamera(const std::string& name);
    virtual ~ThirdPersonCamera();

    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------

    //! ターゲットを中心点して、垂直方向のラジアン角
    void RotateXAroundTarget(f32 rad);
    //! ターゲットを中心点して、水平回転のラジアン角
    void RotateYAroundTarget(f32 rad);
    //! カメラとターゲットの距離を調整する
    void Approach(f32 dist);
    //! ターゲットを中心点して、垂直回転のラジアン角を設定する
    void SetRotateXAroundTarget(f32 rad);
    //! ターゲットを中心点して、水平回転のラジアン角を設定する
    void SetRotateYAroundTarget(f32 rad);
    //! 距離をセット
    void SetDistance(f32 dist);
    //! 最大距離と最小距離をセット
    void SetDistanceMinMax(f32 minDist, f32 maxDist);

protected:
    //---------------------------------------------------------------------------
    // protected 変数
    //---------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    // protected 関数
    //---------------------------------------------------------------------------

private:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    f32 _distance = 6.0f;    //!< 今ターゲットとカメラの距離
    f32 _minDist  = 3.0f;   //!< ターゲットとカメラの最小距離
    f32 _maxDist  = 15.0f;   //!< ターゲットとカメラの最大距離
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------

    //----------
    //　継承
    bool OnInit() override;     //!< 初期化
    void OnUpdate() override;   //!< 更新

    //----------
    //! ターゲットを中心点して、回転のラジアン角
    void RotateAroundTarget(const float3& rotation);
    //! 垂直方向のラジアン角は範囲内に収める
    void ClampRotateXAroundTargetRatation(f32& x);

	void HandleControl(); //!< カメラ制御処理
};
}   // namespace entity::go
