﻿//---------------------------------------------------------------------------
//!	@file	Player.cpp
//!	@brief	キャラクタープレイヤーのクラス
//---------------------------------------------------------------------------
#include "Character.h"
#include <filesystem>
#include <fstream>

#include "entity/gameObject/Weapon.h"

#include "component/Transform.h"
#include "component/Animator.h"
#include "component/ModelRenderer.h"
#include "component/physics/Collider.h"
#include "component/info/CharacterInfo.h"
#include "component/action/PlayerAction.h"

#include "manager/EntityManager.h"
#include "entity/gameObject/ui/Sprite2D.h"
namespace entity::go {
//---------------------------------------------------------------------------
//! コンストラクタ
//! @param	[in] name オブジェクトの名前
//---------------------------------------------------------------------------
Character::Character(const std::string& name, const float3& pos, const float3& scale, const float3& rotation)
: GameObject(name, pos, scale, rotation)
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
Character::~Character()
{
}
//---------------------------------------------------------------------------
//! プレイヤーの死亡判定
//---------------------------------------------------------------------------
bool Character::IsDead() const
{
    return _isDead;
}
void Character::ReadAnim(std::string_view path)
{
    nlohmann::json jAnim;
    //=======================
    // ファイルを読み込む
    //=======================
    std::ifstream ifs(path);
    ifs >> jAnim;
    if(ifs.is_open())
        ifs.close();
    //=======================
    // ファイルを読み込む
    //=======================
    AddComponent<component::ModelRenderer>()->ReadModelJson(jAnim["Model"]);
}
//---------------------------------------------------------------------------
//! 外部からのdiffudse textureをセット
//---------------------------------------------------------------------------
void Character::SetExternalDiffuseTexture(std::string_view path)
{
    GetComponent<component::ModelRenderer>()->LoadExternalDiffuseTexture(path);
}
//---------------------------------------------------------------------------
//!< UIの表示かどうか設定
//---------------------------------------------------------------------------
void Character::SetDisplayHpUI(bool display)
{
    if(display) {
        _hpBar->Enable();
    }
    else {
        _hpBar->Disable();
    }
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void Character::OnUpdate()
{
    f32 hpRate = GetComponent<component::CharacterInfo>()->GetCurrentHPRate();
    if(isnan(hpRate)) return;

    _hpBar->SetFillRate(hpRate);
}
}   // namespace entity::go
