﻿//---------------------------------------------------------------------------
//!	@file	Character.h
//!	@brief	キャラクタープレイヤーのクラス
//---------------------------------------------------------------------------
#pragma once
#include "entity/gameObject/GameObject.h"

namespace component {
class Transform;
class Animator;
class ModelRenderer;
class PlayerAction;
class CapsuleZCollider;
}   // namespace component

namespace entity::go {
namespace ui {
class Sprite2D;
}
class Character : public GameObject
{
public:
    Character(const std::string& name     = "Character",
              const float3&      pos      = float3(0.0f, 0.0f, 0.0f),
              const float3&      scale    = float3(1.0f, 1.0f, 1.0f),
              const float3&      rotation = float3(0.0f, 0.0f, 0.0f));
    virtual ~Character();

public:
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    bool IsDead() const;                    //!< プレイヤーの死亡判定
    void SetToDead() { _isDead = true; };   //!< 死亡フラグ

    void ReadAnim(std::string_view path);   //!< アニメションを読み込む

    void SetExternalDiffuseTexture(std::string_view path);   //!< 外部からのdiffudse textureをセット

    virtual void SetDisplayHpUI(bool display);   //!< UIの表示かどうか設定
    //---------------------------------------------------------------------------
    // public 変数
    //---------------------------------------------------------------------------

protected:
    //---------------------------------------------------------------------------
    // protected 変数
    //---------------------------------------------------------------------------
    // 継承
    virtual void OnUpdate() override;   //!< 更新

    //---------------------------------------------------------------------------
    // protected 関数
    //---------------------------------------------------------------------------
    bool _isDead = false;

    raw_ptr<ui::Sprite2D> _hpBar;

private:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------

    //-----------------

    //void Finalize() override;   //!< 解放
};
}   // namespace entity::go
