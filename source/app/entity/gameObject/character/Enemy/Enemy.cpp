﻿//---------------------------------------------------------------------------
//!	@file	Enemy.h
//!	@brief	敵のクラス
//---------------------------------------------------------------------------
#include "Enemy.h"
#include "entity/gameObject/Weapon.h"

//#include "component/Transform.h"
//#include "component/PlayerAction.h"
//#include "component/Animator.h"
//#include "component/ModelRenderer.h"
//#include "component/physics/Collider.h"
//#include "component/info/CharacterInfo.h"

#include "manager/EntityManager.h"
#include "entity/gameObject/ui/Sprite2D.h"
namespace entity::go {
//---------------------------------------------------------------------------
//! コンストラクタ
//! @param	[in] name オブジェクトの名前
//---------------------------------------------------------------------------
Enemy::Enemy(const std::string& name, const float3& pos, const float3& scale, const float3& rotation)
: Character(name, pos, scale, rotation)
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
Enemy::~Enemy()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool Enemy::OnInit()
{
    _hpBar = new ui::Sprite2D("Hp", float3(0.0f, 0.9f, 0.0f), float3(0.6f, 0.05f, 1.0f));
    _hpBar->SetTexture("ui/health_bar.png");
    _hpBar->SetFillColor(float4(1.0f, 0.0f, 0.0f, 1.0f));
    _hpBar->SetFillRate(1.0f);
    manager::Regist(_hpBar.get());

	return true;
}
}   // namespace entity::go
