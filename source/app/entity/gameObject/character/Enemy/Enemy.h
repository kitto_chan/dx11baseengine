﻿//---------------------------------------------------------------------------
//!	@file	Enemy.h
//!	@brief	敵のクラス
//---------------------------------------------------------------------------
#pragma once
#include "entity/gameObject/character/Character.h"

namespace component {
}   // namespace component

namespace entity::go {
namespace ui {
class Sprite2D;
}
class Enemy : public Character
{
public:
    Enemy(const std::string& name     = "Enemy",
          const float3&      pos      = float3(0.0f, 0.0f, 0.0f),
          const float3&      scale    = float3(1.0f, 1.0f, 1.0f),
          const float3&      rotation = float3(0.0f, 0.0f, 0.0f));
    virtual ~Enemy();

public:
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    // public 変数
    //---------------------------------------------------------------------------

protected:
    //---------------------------------------------------------------------------
    // protected 変数
    //---------------------------------------------------------------------------
    // 継承
    virtual bool OnInit() override;     //!< 初期化

    //---------------------------------------------------------------------------
    // protected 関数
    //---------------------------------------------------------------------------

private:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------

    //-----------------

    //void Finalize() override;   //!< 解放
};
}   // namespace entity::go
