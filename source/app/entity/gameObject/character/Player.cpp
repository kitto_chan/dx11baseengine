﻿//---------------------------------------------------------------------------
//!	@file	Player.cpp
//!	@brief	キャラクタープレイヤーのクラス
//---------------------------------------------------------------------------
#include "Player.h"
#include "entity/gameObject/Weapon.h"
#include "entity/gameObject/ui/Sprite2D.h"

#include "component/Transform.h"
#include "component/info/PlayerInfo.h"
#include "component/Animator.h"
#include "component/ModelRenderer.h"
#include "component/physics/Collider.h"
#include "component/action/PlayerAction.h"

#include "manager/EntityManager.h"
#include "manager/CameraManager.h"
namespace entity::go {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
Player::Player(const std::string& name, const float3& pos, const float3& scale, const float3& rotation)
: Character(name, pos, scale, rotation)
{
}

//---------------------------------------------------------------------------
//! コンストラクタ
//! @param	[in] name オブジェクトの名前
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
Player::~Player()
{
}
//---------------------------------------------------------------------------
//! UIの表示かどうか設定
//---------------------------------------------------------------------------
void Player::SetDisplayHpUI(bool display)
{
    if(display) {
        _hpBar->Enable();
        _mpBar->Enable();
    }
    else {
        _hpBar->Disable();
        _mpBar->Disable();
    }
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool Player::OnInit()
{
    {
        _hpBar = new ui::Sprite2D("PlayerHp", float3(-0.7f, -0.7f, 0.0f), float3(0.2f, 0.03f, 1.0f), float3(0.0f, 0.0f, math::D2R(15.0f)));
        _hpBar->SetTexture("ui/health_bar.png");
        _hpBar->SetFillColor(float4(0.0f, 1.0f, 0.0f, 1.0f));
        _hpBar->SetFillRate(1.0f);
        manager::Regist(_hpBar.get());
    }

    {
        _mpBar = new ui::Sprite2D("PlayerSp", float3(-0.7f, -0.8f, 0.0f), float3(0.2f, 0.03f, 1.0f), float3(0.0f, 0.0f, math::D2R(15.0f)));
        _mpBar->SetTexture("ui/health_bar.png");
        _mpBar->SetFillColor(float4(0.0f, 0.0f, 1.0f, 1.0f));
        _mpBar->SetFillRate(1.0f);
        manager::Regist(_mpBar.get());
    }

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void Player::OnUpdate()
{
    Character::OnUpdate();
    f32 mpRate = GetComponent<component::PlayerInfo>()->GetCurrentMPRate();
    if(isnan(mpRate)) return;
    _mpBar->SetFillRate(mpRate);

    // y 座標 -10 以下 = 落ちる,　ゲームオーバーとなります
    if(_pTransform->GetPosition().f32[1] <= -5.0f) {
        _isDead = true;
    }
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
//void Player::OnRender()
//{
//    Character::OnRender();
//}
//---------------------------------------------------------------------------
//! 後期更新
//---------------------------------------------------------------------------
void Player::OnLateUpdate()
{
    /*  if(!GetComponent<component::PlayerInfo>()->IsCharaState(ECharacterState::Attack))
        _pTransform->SetRotationY(manager::GetCurrentCameraMgr()->GetCurrentCamera()->GetRotation().y + math::PI);*/
}
}   // namespace entity::go
