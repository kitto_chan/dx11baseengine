﻿//---------------------------------------------------------------------------
//!	@file	DirectionalLight.cpp
//!	@brief	DirectionalLightの基底クラス
//---------------------------------------------------------------------------
#include "DirectionalLight.h"
#include "component/Transform.h"

namespace entity::go {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
DirectionalLight::DirectionalLight()
{
    _name = "DirectionalLight";
    SetTexture("textures/icon/sun.png");
}
//---------------------------------------------------------------------------
//! コンストラクタ
//! [in] name ゲームオブジェクトの名前
//---------------------------------------------------------------------------
DirectionalLight::DirectionalLight(const std::string& name)
: ILight(name)
{
    SetTexture("textures/icon/sun.png");
}
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
DirectionalLight::~DirectionalLight()
{
}
//---------------------------------------------------------------------------
//! Phong Lighting Model用の頂点バッファを返す
//---------------------------------------------------------------------------
cb::PhongDirectionalLightCB DirectionalLight::GetPhongDirectionalLightCB()
{
    cb::PhongDirectionalLightCB dirLightCB = {
        math::CastToXMFLOAT4(_ambient),
        math::CastToXMFLOAT4(_diffuse),
        math::CastToXMFLOAT4(_specular),
        math::CastToXMFLOAT3(_pTransform->GetForwardAxis()),
        0
    };
    return dirLightCB;
}
//---------------------------------------------------------------------------
//! PBR Lighting Model用の頂点バッファを返す
//---------------------------------------------------------------------------
cb::PBRDirectionalLightCB DirectionalLight::GetPBRDirectionalLightCB()
{
    cb::PBRDirectionalLightCB dirLightCB = {
        math::CastToXMFLOAT3(_ambient.xyz),
        0,
        math::CastToXMFLOAT3(_pTransform->GetForwardAxis()),
        0
    };
    return dirLightCB;
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool DirectionalLight::OnInit()
{
    //!< ライト画像表示用, スケールはライティング計算としては関係ないです
    _pTransform->SetScale(0.1f, 0.1f, 0.1f);
    ILight::OnInit();

    return false;
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void DirectionalLight::OnRender()
{
    ILight::OnRender();
    if(_isRenderDebug) {
        debug::drawArrow(_pTransform->GetPosition(), _pTransform->GetForwardAxis(), _ambient);
    }
}

}   // namespace entity::go
