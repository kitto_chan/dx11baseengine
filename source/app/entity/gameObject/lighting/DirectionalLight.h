﻿//---------------------------------------------------------------------------
//!	@file	DirectionalLight.h
//!	@brief	DirectionalLightの基底クラス
//---------------------------------------------------------------------------
#pragma once
#include "ILight.h"
namespace cb {
// Phong Lighting Model用の頂点バッファ
struct PhongDirectionalLightCB
{
    DirectX::XMFLOAT4 ambient;
    DirectX::XMFLOAT4 diffuse;
    DirectX::XMFLOAT4 specular;
    DirectX::XMFLOAT3 direction;
    s32               useless;
};

// PBR Lighting Model用の頂点バッファ
struct PBRDirectionalLightCB
{
    DirectX::XMFLOAT3 color;
    int               useless1;

    DirectX::XMFLOAT3 direction;
    int               useless2;
};
}   // namespace cb

namespace entity::go {
class DirectionalLight : public ILight
{
public:
    DirectionalLight();
    DirectionalLight(const std::string& name);
    virtual ~DirectionalLight();

    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    //! Phong Lighting Model用の頂点バッファを返す
    cb::PhongDirectionalLightCB GetPhongDirectionalLightCB();
    //! PBR Lighting Model用の頂点バッファを返す
    cb::PBRDirectionalLightCB GetPBRDirectionalLightCB();

protected:
    //---------------------------------------------------------------------------
    // protected 変数
    //---------------------------------------------------------------------------

private:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    virtual bool OnInit() override;     //!< 初期化
    virtual void OnRender() override;   //!< 描画
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
};
}   // namespace entity::go
