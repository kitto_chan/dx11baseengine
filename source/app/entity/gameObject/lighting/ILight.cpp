﻿//---------------------------------------------------------------------------
//!	@file	ILight.cpp
//!	@brief	ライトの基底クラス
//---------------------------------------------------------------------------
#include "ILight.h"
#include "component/Transform.h"

namespace entity::go {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
ILight::ILight()
{
    _name                = "Light";
    _isUseLocalScaleOnly = true;
}
//---------------------------------------------------------------------------
//! コンストラクタ
//! [in] name ゲームオブジェクトの名前
//---------------------------------------------------------------------------
ILight::ILight(const std::string& name)
: SpriteBillBoard(name)
{
    _isUseLocalScaleOnly = true;
}
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
ILight::~ILight()
{
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void ILight::OnRender()
{
    _color = _ambient;
    if(_isRenderDebug) {
        SpriteBillBoard::OnRender();
    }
}
//---------------------------------------------------------------------------
//! ImGui描画
//---------------------------------------------------------------------------
void ILight::OnRenderImgui()
{
    imgui::ImguiColumn2FormatBegin("Ambient");
    ImGui::ColorEdit4("##Ambient", (float*)&_ambient, ImGuiColorEditFlags_NoInputs);
    ImGui::SameLine();
    imgui::HelpMarker(u8"環境光 (ライトの色)");
    imgui::ImguiColumn2FormatEnd();

    imgui::ImguiColumn2FormatBegin("Diffuse");
    ImGui::ColorEdit4("##Diffuse", (float*)&_diffuse, ImGuiColorEditFlags_NoInputs);
    ImGui::SameLine();
    imgui::HelpMarker(u8"拡散照明の色(PBRで使ってない)");
    imgui::ImguiColumn2FormatEnd();

    imgui::ImguiColumn2FormatBegin("Specular");
    ImGui::ColorEdit4("##Specular", (float*)&_specular, ImGuiColorEditFlags_NoInputs);
    ImGui::SameLine();
    imgui::HelpMarker(u8"反射の色(PBRで使ってない)");
    imgui::ImguiColumn2FormatEnd();

    SpriteBillBoard::OnRenderImgui();
}
}   // namespace entity::go
