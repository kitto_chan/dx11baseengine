﻿//---------------------------------------------------------------------------
//!	@file	ILight.h
//!	@brief	Lightの基底クラス
//---------------------------------------------------------------------------
#pragma once
#include "entity/gameObject/GameObject.h"
#include "entity/gameObject/billBoardObject/SpriteBillBoard.h"
namespace component {
class Transform;
};

namespace manager {
class LightingManager;
}

namespace entity::go {
class ILight : public SpriteBillBoard
{
    friend class manager::LightingManager;

public:
    ILight();
    ILight(const std::string& name);
    virtual ~ILight();

    //---------------------------------------------------------------------------
    //! public 関数
    //---------------------------------------------------------------------------
    void SetAmbient(const float4& ambient) { _ambient = ambient; };       //!< 環境反射係数を設定
    void SetDiffuse(const float4& diffuse) { _diffuse = diffuse; };       //!< 拡散反射係数を設定
    void SetSpecular(const float4& specular) { _specular = specular; };   //!< 鏡面反射係数を設定

    //---------------------------------------------------------------------------
    //! public 変数
    //---------------------------------------------------------------------------
    float4 _ambient  = float4(1.0f, 1.0, 1.0, 1.0f);   //!< 環境反射係数
    float4 _diffuse  = float4(1.0f, 1.0, 1.0, 1.0f);   //!< 拡散反射係数
    float4 _specular = float4(1.0f, 1.0, 1.0, 1.0f);   //!< 鏡面反射係数

protected:
    //---------------------------------------------------------------------------
    // protected 変数
    //---------------------------------------------------------------------------
    virtual void OnRender() override;        //!< 描画
    virtual void OnRenderImgui() override;   //!< ImGui描画

private:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
};
}   // namespace entity::go
