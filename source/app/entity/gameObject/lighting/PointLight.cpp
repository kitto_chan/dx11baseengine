﻿//---------------------------------------------------------------------------
//!	@file	PointLight.h
//!	@brief	PointLightの基底クラス
//---------------------------------------------------------------------------
#include "PointLight.h"
#include "component/Transform.h"

namespace entity::go {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
PointLight::PointLight()
{
    _name = "PointLight";
    SetTexture("textures/icon/sun.png");
}
//---------------------------------------------------------------------------
//! コンストラクタ
//! [in] name ゲームオブジェクトの名前
//---------------------------------------------------------------------------
PointLight::PointLight(const std::string& name)
: ILight(name)
{
    SetTexture("textures/icon/sun.png");
}
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
PointLight::~PointLight()
{
}
//---------------------------------------------------------------------------
//! Phong Lighting Model用の頂点バッファを返す
//---------------------------------------------------------------------------
cb::PhongPointLightCB PointLight::GetPhongPointightCB()
{
    cb::PhongPointLightCB dirLightCB = {
        math::CastToXMFLOAT4(_ambient),
        math::CastToXMFLOAT4(_diffuse),
        math::CastToXMFLOAT4(_specular),

        math::CastToXMFLOAT3(_pTransform->GetLocalToWorldMatrix()._41_42_43),
        _range,

        math::CastToXMFLOAT3(_attenuation),
        0
    };
    return dirLightCB;
}
//---------------------------------------------------------------------------
//! PBR Lighting Model用の頂点バッファを返す
//---------------------------------------------------------------------------
cb::PBRPointLightCB PointLight::GetPBRPointLightCB()
{
    cb::PBRPointLightCB dirLightCB = {
        math::CastToXMFLOAT3(_ambient.xyz),
        _range,

        math::CastToXMFLOAT3(_pTransform->GetLocalToWorldMatrix()._41_42_43),
        _decay
    };
    return dirLightCB;
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool PointLight::OnInit()
{
    //!< ライト画像表示用, スケールはライティング計算としては関係ないです
    _pTransform->SetScale(0.1f, 0.1f, 0.1f);
    ILight::OnInit();

    return true;
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void PointLight::OnRender()
{
    ILight::OnRender();
    if(_isRenderDebug) {
        debug::drawSphere(_pTransform->GetLocalToWorldMatrix()._41_42_43, _range, _ambient);
    }
}
//---------------------------------------------------------------------------
//! ImGui描画
//---------------------------------------------------------------------------
void PointLight::OnRenderImgui()
{
    ILight::OnRenderImgui();

    imgui::ImguiColumn2FormatBegin("Range");
    ImGui::DragFloat("##Range", &_range, 0.1f);
    imgui::ImguiColumn2FormatEnd();
    ImGui::SameLine();
    imgui::HelpMarker(u8"光の強さ");

	imgui::ImguiColumn2FormatBegin("Decay");
    ImGui::DragFloat("##Decay", &_decay, 0.1f);
    imgui::ImguiColumn2FormatEnd();
    ImGui::SameLine();
    imgui::HelpMarker(u8"光の強さ");

    imgui::ImguiDragXYZ("Attenuation", _attenuation);
    ImGui::SameLine();
    imgui::HelpMarker(u8"衰弱変数");
}

}   // namespace entity::go
