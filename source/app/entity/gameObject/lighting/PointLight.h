﻿//---------------------------------------------------------------------------
//!	@file	PointLight.h
//!	@brief	PointLightの基底クラス
//---------------------------------------------------------------------------
#pragma once
#include "ILight.h"
namespace cb {
// Phong Lighting Model用の頂点バッファ
struct PhongPointLightCB
{
    DirectX::XMFLOAT4 ambient;
    DirectX::XMFLOAT4 diffuse;
    DirectX::XMFLOAT4 specular;
    DirectX::XMFLOAT3 position;
    f32               range;

    DirectX::XMFLOAT3 attenuation;
    s32               useless;
};
// PBR Lighting Model用の頂点バッファ
struct PBRPointLightCB
{
    DirectX::XMFLOAT3 color;
    f32               range;

    DirectX::XMFLOAT3 position;
    f32               decay;
};
}   // namespace cb

namespace entity::go {
class PointLight : public ILight
{
public:
    PointLight();
    PointLight(const std::string& name);
    virtual ~PointLight();

    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    //! Phong Lighting Model用の頂点バッファを返す
    cb::PhongPointLightCB GetPhongPointightCB();
    //! PBR Lighting Model用の頂点バッファを返す
    cb::PBRPointLightCB GetPBRPointLightCB();

    //!< 光の衰弱を設定
    void SetAttenuation(const float3& att) { _attenuation = att; }

    // ポイントライトの距離を設定
    void SetRange(f32 range) { _range = range; }

protected:
    //---------------------------------------------------------------------------
    // protected 変数
    //---------------------------------------------------------------------------

private:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    f32    _range       = 10.0f;                      //!< 光の強さ(距離)
    float3 _attenuation = float3(0.0f, 0.1f, 0.0f);   //!< 光の衰弱
    f32    _decay       = 0.1f;
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
    virtual bool OnInit() override;          //!< 初期化
    virtual void OnRender() override;        //!< 描画
    virtual void OnRenderImgui() override;   //!< ImGui描画
};
}   // namespace entity::go
