﻿//---------------------------------------------------------------------------
//!	@file	SpotLight.h
//!	@brief	SpotLightの基底クラス
//---------------------------------------------------------------------------
#include "SpotLight.h"
#include "component/Transform.h"

namespace entity::go {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
SpotLight::SpotLight()
{
    _name = "SpotLight";
    SetTexture("textures/icon/sun.png");
}
//---------------------------------------------------------------------------
//! コンストラクタ
//! [in] name ゲームオブジェクトの名前
//---------------------------------------------------------------------------
SpotLight::SpotLight(const std::string& name)
: ILight(name)
{
    SetTexture("textures/icon/sun.png");
}
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
SpotLight::~SpotLight()
{
}
//---------------------------------------------------------------------------
//! Phong Lighting Model用の頂点バッファを返す
//---------------------------------------------------------------------------
cb::PhongSpotLightCB SpotLight::GetPhongSpotLightCB()
{
    cb::PhongSpotLightCB dirLightCB = {
        math::CastToXMFLOAT4(_ambient),
        math::CastToXMFLOAT4(_diffuse),
        math::CastToXMFLOAT4(_specular),

        math::CastToXMFLOAT3(_pTransform->GetLocalToWorldMatrix()._41_42_43),
        _range,

        math::CastToXMFLOAT3(_pTransform->GetForwardAxis()),
        _spot,

        math::CastToXMFLOAT3(_attenuation),
        0
    };
    return dirLightCB;
}
//---------------------------------------------------------------------------
//! PBR Lighting Model用の頂点バッファを返す (TODO:)
//! 実装してない
//---------------------------------------------------------------------------
cb::PBRSpotLightCB SpotLight::GetPBRSpotLightCB()
{
    // TODO
    cb::PBRSpotLightCB dirLightCB = {
        math::CastToXMFLOAT3(float3(_ambient.xyz)),
        0,   // useless
        math::CastToXMFLOAT3(_pTransform->GetBackwardAxis()),
        0   // useless
    };
    return dirLightCB;
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool SpotLight::OnInit()
{
    //!< ライト画像表示用, スケールはライティング計算としては関係ないです
    _pTransform->SetScale(0.1f, 0.1f, 0.1f);
    ILight::OnInit();

    return false;
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void SpotLight::OnRender()
{
    ILight::OnRender();
    if(_isRenderDebug) {
        debug::drawArrow(_pTransform->GetPosition(), _pTransform->GetForwardAxis(), _ambient);
    }
}
//---------------------------------------------------------------------------
//! ImGui描画
//---------------------------------------------------------------------------
void SpotLight::OnRenderImgui()
{
    ILight::OnRenderImgui();

    imgui::ImguiColumn2FormatBegin("Range");
    ImGui::DragFloat("##Range", &_range, 0.1f);
    imgui::ImguiColumn2FormatEnd();
    ImGui::SameLine();
    imgui::HelpMarker(u8"光の強さ");

    imgui::ImguiColumn2FormatBegin("Spot");
    ImGui::DragFloat("##Spot", &_spot, 0.1f);
    imgui::ImguiColumn2FormatEnd();
    ImGui::SameLine();
    imgui::HelpMarker(u8"スポットの強さ");

    imgui::ImguiDragXYZ("Attenuation", _attenuation);
    ImGui::SameLine();
    imgui::HelpMarker(u8"衰弱変数");
}

}   // namespace entity::go
