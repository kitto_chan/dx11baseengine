﻿//---------------------------------------------------------------------------
//!	@file	SpotLight.h
//!	@brief	SpotLightの基底クラス
//---------------------------------------------------------------------------
#pragma once
#include "ILight.h"
namespace cb {
// Phong Lighting Model用の頂点バッファ
struct PhongSpotLightCB
{
    DirectX::XMFLOAT4 ambient;
    DirectX::XMFLOAT4 diffuse;
    DirectX::XMFLOAT4 specular;

    DirectX::XMFLOAT3 postion;
    f32               range;

    DirectX::XMFLOAT3 direction;
    f32               spot;

    DirectX::XMFLOAT3 attenuation;
    s32               useless;
};
// PBR Lighting Model用の頂点バッファ
struct PBRSpotLightCB
{
    DirectX::XMFLOAT3 color;
    int               useless1;

    DirectX::XMFLOAT3 direction;
    int               useless2;
};
}   // namespace cb

namespace entity::go {
class SpotLight : public ILight
{
public:
    SpotLight();
    SpotLight(const std::string& name);
    virtual ~SpotLight();

    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    //! Phong Lighting Model用の頂点バッファを返す
    cb::PhongSpotLightCB GetPhongSpotLightCB();
    //! PBR Lighting Model用の頂点バッファを返す
    //! @note実装してない
    cb::PBRSpotLightCB GetPBRSpotLightCB();

protected:
    //---------------------------------------------------------------------------
    // protected 変数
    //---------------------------------------------------------------------------

private:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    f32    _range       = 25.0f;
    float3 _attenuation = float3(0.1f, 0.1f, 0.1f);
    f32    _spot        = 1000.0f;
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
    virtual bool OnInit() override;          //!< 初期化
    virtual void OnRender() override;        //!< 描画
    virtual void OnRenderImgui() override;   //!< ImGui描画
};
}   // namespace entity::go
