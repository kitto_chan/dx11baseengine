﻿//---------------------------------------------------------------------------
//!	@file	Sprite2D.h
//!	@brief	プレイン2Dのクラス
//---------------------------------------------------------------------------
#pragma once
#include "entity/gameObject/GameObject.h"

namespace entity::go::ui {
class AdvancedSprite : public GameObject
{
public:
    enum class eEffect
    {
        None,
        Blur,
        Disslove,
		ProcessBar
    };

    //! コンストラクタ
    AdvancedSprite(const std::string& name     = "Sprite2D",
                   const float3&      pos      = float3(0.0f, 0.0f, 0.0f),
                   const float3&      scale    = float3(1.0f, 1.0f, 1.0f),
                   const float3&      rotation = float3(0.0f, 0.0f, 0.0f));
    ~AdvancedSprite();   //!< デストラクタ

    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    void SetTexture(std::string_view path);                           //!< 描画テクスチャーを設定
    void SetTexture(shr_ptr<gpu::Texture> path);                      //!< 描画テクスチャーを設定
    void SetColor(const float4& color) { _colorCb.color = color; };   //!< 色設定

    void SetBlurPower(f32 power) { _blurCB.power = power; };   //!< ブラーの強さ
    void SetDissolve(f32 value) { _dissolveCB.dissolve = value; }
	void SetProcessFillAmount(f32 fillAmount) { _processBarCB.fillAmount = fillAmount; };

    void SetCurrentEffect(eEffect effect) { _currentEffect = effect; };   //!< エフェクトタイプ
    // サンプラステート設定
    void SetSamplerState(gpu::eSamplerState samplerState) { _samplerState = samplerState; }

    void OnRender() override;        //!< 描画

	bool draw = true;
private:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    shr_ptr<gpu::Texture> _pTexture;        //!< メインテクスチャー
    shr_ptr<gpu::Texture> _pTextureBlack;   //!< メインテクスチャー
    shr_ptr<gpu::Texture> _pTextureNoise;   //!< ノイズテクスチャ
    shr_ptr<gpu::Texture> _pTextureColor;   //!< ノイズテクスチャ

    // 頂点バッファ
    cb::MeshColorCB  _colorCb      = {};
    cb::BlurCB       _blurCB       = {};
    cb::DissolveCB   _dissolveCB   = {};
    cb::ProcessBarCB _processBarCB = {};

    // 何のエフェクトを使うか
    eEffect            _currentEffect = eEffect::None;
    gpu::eSamplerState _samplerState  = gpu::eSamplerState::PointWrap;

    bool _isRenderDebug = false;   //!< デバッグ描画

    bool OnInit() override;          //!< 初期化
    void OnUpdate() override;        //!< 更新
    void OnRenderImgui() override;   //!< Imguiの描画

    void Begin2D();        //!< 2D描画を開始
    void RenderSprite();   //!< スプライト描画
    void End2D();          //!< 2D描画を終わる
};
}   // namespace entity::go::ui
