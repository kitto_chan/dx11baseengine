﻿//---------------------------------------------------------------------------
//!	@file	Sprite.h
//!	@brief	プレイン2Dのクラス
//---------------------------------------------------------------------------
#include "Sprite.h"
#include "component/Transform.h"
#include "utilityGPU/Geometry.h"
#include "effect/Effect.h"
#include "component/physics/RigidBody.h"
#include "component/physics/Collider.h"
#include "manager/CameraManager.h"
namespace entity::go::ui {
namespace {
std::map<std::string, shr_ptr<gpu::Texture>> _textureMap;

cb::MeshColorCB colorCb  = {};
cb::CameraCB    cameraCB = {};
//====================
// DirectX 変数
//====================
shr_ptr<gpu::Shader> _pVertexShader2D;    //!< 2D頂点シェーダー
shr_ptr<gpu::Shader> _pPixelShader2D;     //!< 2Dピクセルシェーダー(テクスチャーあり)
shr_ptr<gpu::Shader> _pPixelShaderFlat;   //!< 2Dピクセルシェーダー(テクスチャーなし)

shr_ptr<gpu::InputLayout> _pIntputLayout2D;   //!< 2D入力レイアウト

std::shared_ptr<gpu::Buffer> _vertexBuffer;   //!< 頂点バッファ
std::shared_ptr<gpu::Buffer> _indexBuffer;    //!< インデックスバッファ
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
Sprite::Sprite(const std::string& name, const float3& pos, const float3& scale, const float3& rotation)
: GameObject(name, pos, scale, rotation){};
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
Sprite::~Sprite()
{
}
//---------------------------------------------------------------------------
//! テクスチャを設定
//---------------------------------------------------------------------------
void Sprite::SetTexture(std::string_view path)
{
    auto itr = _textureMap.find(path.data());
    if(itr != _textureMap.end()) {
        _pTexture = itr->second;
        return;
    }
    _pTexture = gpu::createTextureFromFile(path);
    _textureMap.insert(std::pair(path, _pTexture));
}
//---------------------------------------------------------------------------
//! テクスチャを設定
//---------------------------------------------------------------------------
void Sprite::SetTexture(shr_ptr<gpu::Texture> path)
{
    _pTexture = path;
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool Sprite::OnInit()
{
    if(!_pVertexShader2D)
        _pVertexShader2D = gpu::createShader("dx11/vs_3d.fx", "main", "vs_5_0");   // 2D頂点シェーダー

    if(!_pPixelShader2D)
        _pPixelShader2D = gpu::createShader("dx11/ps_texture.fx", "main", "ps_5_0");   // 2Dピクセルシェーダー

    if(!_pPixelShaderFlat)
        _pPixelShaderFlat = gpu::createShader("dx11/ps_flat.fx", "main", "ps_5_0");

    if(!_pIntputLayout2D)
        _pIntputLayout2D = gpu::createInputLayout(vertex::VertexPosUv::inputLayout,
                                                  std::size(vertex::VertexPosUv::inputLayout));   // 2D入力レイアウト

    // 頂点 (Pos, Uv)
    constexpr vertex::VertexPosUv vertices[]{
        { DirectX::XMFLOAT3(-1.0f, +1.0f, 0.0f), DirectX::XMFLOAT2(0.0f, 0.0f) },
        { DirectX::XMFLOAT3(+1.0f, +1.0f, 0.0f), DirectX::XMFLOAT2(1.0f, 0.0f) },
        { DirectX::XMFLOAT3(+1.0f, -1.0f, 0.0f), DirectX::XMFLOAT2(1.0f, 1.0f) },
        { DirectX::XMFLOAT3(-1.0f, -1.0f, 0.0f), DirectX::XMFLOAT2(0.0f, 1.0f) },
    };

    // インデックス
    constexpr u32 indices[]{
        0, 1, 2,
        2, 3, 0
    };

    // 頂点バッファの作成
    if(!_vertexBuffer) {
        _vertexBuffer = gpu::createBuffer(vertices, D3D11_BIND_VERTEX_BUFFER);
    }

    // インデックスバッファの作成
    if(!_indexBuffer) {
        _indexBuffer = gpu::createBuffer(indices, D3D11_BIND_INDEX_BUFFER);
    }

    if(!(_pVertexShader2D || _pPixelShader2D)) {
        ASSERT_MESSAGE(false, "Spriteのシェーダー初期化失敗した");
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void Sprite::OnUpdate()
{
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void Sprite::OnRender()
{
    Begin2D();
    RenderSprite();
    End2D();
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void Sprite::OnRenderImgui()
{
    imgui::ImguiColumn2FormatBegin("Color");
    ImGui::ColorEdit4("##Color", (float*)&_color, ImGuiColorEditFlags_NoInputs);
    imgui::ImguiColumn2FormatEnd();

	imgui::ImguiHierarchyImageSelecter("Sprite", _pTexture);

    imgui::ImguiCheckBox("RenderDebug", _isRenderDebug);
}
//---------------------------------------------------------------------------
//! 2D描画を開始
//---------------------------------------------------------------------------
void Sprite::Begin2D()
{
    //gpu::swapChain()->backBuffer()->desc().width_
    s32 screenW = sys::WINDOW_WIDTH;    // スクリーンの幅
    s32 screenH = sys::WINDOW_HEIGHT;   // スクリーンの高さ

    //-----------------------------------------------------------------
    // ピクセル座標系、スクリーン座標系、変換行列を作成
    //-----------------------------------------------------------------
    matrix matProj2D = math::identity();

    // (1) 単位の変換
    matrix scale = math::scale(float3(2.0f / screenW, 2.0 / screenH, 1.0f));
    // (2) Y方向の反転
    matrix fixAxisY = math::scale(float3(1.0f, -1.0f, 1.0f));
    // (3) 原点の移動
    matrix offset = math::translate(float3(-1.0f, 1.0f, 0.0f));

    matProj2D = mul(scale, mul(fixAxisY, offset));

    //-----------------------------------------------------------------
    // 設定
    //-----------------------------------------------------------------

    //====================
    // シェーダの設定
    //====================
    gpu::vs::setShader(_pVertexShader2D);
    gpu::ps::setShader(_pPixelShader2D);

    gpu::setIndexBuffer(nullptr);       // インデックスバッファ
    gpu::setVertexBuffer(0, nullptr);   // 頂点バッファ

    //----------
    // カメラ行列(view & projection)
    cameraCB.matView = math::identity();
    cameraCB.matProj = matProj2D;
    gpu::setConstantBuffer("CameraCB", cameraCB);

    //----------
    // ワールド行列
    gpu::setConstantBuffer("WorldCB", _pTransform->GetLocalMatrix());

    //-----------
    // 色
    colorCb.color = _color;
    gpu::setConstantBuffer("ColorCB", colorCb);

    gpu::setBlendState(gpu::commonStates().NonPremultiplied());

    //====================
    // 入力レイアウト
    //====================
    gpu::setInputLayout(_pIntputLayout2D);
}
//---------------------------------------------------------------------------
//! スプライトを描画
//---------------------------------------------------------------------------
void Sprite::RenderSprite()
{
    // テクスチャ設定
    gpu::ps::setTexture(0, _pTexture);
    gpu::ps::setSamplerState(0, gpu::commonStates().PointWrap());

    f32 w = _pTexture->desc().width_ / 2.f;
    f32 h = _pTexture->desc().height_ / 2.f;

    float3 pos = float3(0.0f, 0.0f, 0.0f);

    vertex::VertexPosUv v[4] = {
        { { pos.x - w, pos.y - h, 0.0f }, { 0.0f, 0.0f } },   //!< 左上
        { { pos.x + w, pos.y - h, 0.0f }, { 1.0f, 0.0f } },   //!< 右上
        { { pos.x - w, pos.y + h, 0.0f }, { 0.0f, 1.0f } },   //!< 左下
        { { pos.x + w, pos.y + h, 0.0f }, { 1.0f, 1.0f } },   //!< 右下
    };
    gpu::drawUserPrimitive(gpu::Primitive::TriangleStrip, 4, v);

    if(_isRenderDebug) {
        //==========================================
        // デバッグ画像の枠を描画する
        //==========================================
        constexpr u32 lineIndices[]{
            0, 1, 3, 2, 0
        };
        gpu::ps::setShader(_pPixelShaderFlat);
        gpu::drawIndexedUserPrimitive(gpu::Primitive::LineStrip, 4, 5, lineIndices, v);
    }
}
//---------------------------------------------------------------------------
//! スプライトを描画
//---------------------------------------------------------------------------
void Sprite::End2D()
{
    gpu::setBlendState(gpu::commonStates().Opaque());
    // カメラ定数も度に戻す
    manager::GetCurrentCameraMgr()->GetCurrentCamera()->SetCameraCB();
}
}   // namespace entity::go::ui
