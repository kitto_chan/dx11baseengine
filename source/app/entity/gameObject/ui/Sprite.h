﻿//---------------------------------------------------------------------------
//!	@file	Sprite.h
//!	@brief	プレイン2Dのクラス
//---------------------------------------------------------------------------
#pragma once
#include "entity/gameObject/GameObject.h"

namespace entity::go::ui {
class Sprite : public GameObject
{
public:
    //! コンストラクタ
    Sprite(const std::string& name     = "Sprite2D",
           const float3&      pos      = float3(0.0f, 0.0f, 0.0f),
           const float3&      scale    = float3(1.0f, 1.0f, 1.0f),
           const float3&      rotation = float3(0.0f, 0.0f, 0.0f));
    ~Sprite();   //!< デストラクタ

    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    void SetTexture(std::string_view path);                   //!< 描画テクスチャーを設定
    void SetTexture(shr_ptr<gpu::Texture> path);              //!< 描画テクスチャーを設定
    void SetColor(const float4& color) { _color = color; };   //!< 色設定

protected:
    bool _isRenderDebug = false;   //!< デバッグ描画かどうか

private:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    shr_ptr<gpu::Texture> _pTexture;   //!< メインテクスチャー

    float4 _color = float4(1.0f, 1.0f, 1.0f, 1.0f);   //!< テクスチャー色

    bool OnInit() override;          //!< 初期化
    void OnUpdate() override;        //!< 更新
    void OnRender() override;        //!< 描画
    void OnRenderImgui() override;   //!< Imguiの描画

    void Begin2D();        //!< 2D描画を開始
    void RenderSprite();   //!< スプライト描画
    void End2D();          //!< 2D描画を終わる
};
}   // namespace entity::go::ui
