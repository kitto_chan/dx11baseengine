﻿//---------------------------------------------------------------------------
//!	@file	Sprite2D.h
//!	@brief	プレイン2Dのクラス
//---------------------------------------------------------------------------
#include "Sprite2D.h"
#include "component/Transform.h"
#include "utilityGPU/Geometry.h"
#include "effect/Effect.h"
#include "component/physics/RigidBody.h"
#include "component/physics/Collider.h"

namespace entity::go::ui {
namespace {
struct Sprite2DCB
{
    float4 _fillColor   = float4(1.0f, 1.0f, 1.0f, 1.0f);
    float4 _fillColor2  = float4(1.0f, 1.0f, 1.0f, 1.0f);
    f32    _filledRate  = 1.0f;
    f32    _filledRate2 = 1.0f;
};

std::map<std::string, shr_ptr<gpu::Texture>> _textureMap;

cb::MeshColorCB colorCb = {};

Sprite2DCB sprite2DCB = {};
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
Sprite2D::Sprite2D(const std::string& name, const float3& pos, const float3& scale, const float3& rotation)
: GameObject(name, pos, scale, rotation){};
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
Sprite2D::~Sprite2D()
{
}
//---------------------------------------------------------------------------
//!< 描画テクスチャーを設定
//---------------------------------------------------------------------------
void Sprite2D::SetTexture(std::string_view path)
{
    auto itr = _textureMap.find(path.data());
    if(itr != _textureMap.end()) {
        _pTexture = itr->second;
        return;
    }
    _pTexture = gpu::createTextureFromFile(path);
    _textureMap.insert(std::pair(path, _pTexture));
}
//---------------------------------------------------------------------------
//!  描画テクスチャーを設定
//---------------------------------------------------------------------------
void Sprite2D::SetTexture(shr_ptr<gpu::Texture> texture)
{
    _pTexture = texture;
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool Sprite2D::OnInit()
{
    effect::BasicEffectIns()->InitAll();

    InitGPU();
    return true;
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void Sprite2D::OnRender()
{
    // TODO Meshコンポネントクラス
    matrix worldMatrix = GetComponent<component::Transform>()->GetLocalToWorldMatrix();
    effect::BasicEffectIns()->SetWorldMatrix(worldMatrix);

    // 頂点データー
    gpu::setVertexBuffer(0, _pVertexBuffer, sizeof(vertex::VertexPosNormalUv));   // 頂点バッファ
    gpu::setIndexBuffer(_pIndexBuffer);                                           // インデックスバッファ

    // TODO: Image Loader
    static shr_ptr<gpu::Texture> texture;
    if(!_pTexture && !texture) {
        _pTexture = gpu::createTextureFromFile("pattern/Grid.png");
    }

    //----- ColorCB
    colorCb.color = _color;
    gpu::setConstantBuffer("ColorCB", colorCb);

    //----- SpriteCB
    sprite2DCB._filledRate     = _isFilled ? _fillRate : 1.0f;
    sprite2DCB._filledRate2    = _isFilled ? _attenuationRate : 1.0f;
    sprite2DCB._fillColor      = _fillColor;
    sprite2DCB._fillColor2.xyz = _fillColor.xyz * 0.55f;
    gpu::setConstantBuffer("Sprite2DCB", sprite2DCB);

    effect::BasicEffectIns()->SetTexture(_pTexture);
    effect::BasicEffectIns()->SerRenderDefault2D();
    effect::BasicEffectIns()->Apply();

    gpu::drawIndexed(gpu::Primitive::TriangleList, _indexCount);
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void Sprite2D::OnUpdate()
{
    if(_attenuationCount > 0.0f) {
        _attenuationCount -= timer::DeltaTime();
        _attenuationCount = std::clamp(_attenuationCount, 0.0f, 1.0f);
    }
    _attenuationRate = hlslpp::lerp(float1(_fillRate), float1(_maxAttenuationRate), _attenuationCount);
}
//---------------------------------------------------------------------------
//! ImGui描画
//---------------------------------------------------------------------------
void Sprite2D::OnRenderImgui()
{
    static ImVec4 color = ImVec4(_color.f32[0], _color.f32[1], _color.f32[2], _color.f32[3]);
    ImGui::ColorEdit4("MyColor##2f", (float*)&color, ImGuiColorEditFlags_Float);
    _color.f32[0] = color.x;
    _color.f32[1] = color.y;
    _color.f32[2] = color.z;
    _color.f32[3] = color.w;
}
//---------------------------------------------------------------------------
//! 初期化GPU
//---------------------------------------------------------------------------
bool Sprite2D::InitGPU()
{
    geometry::MeshData                     meshdata = geometry::CreatePlane2D(-1.f, -1.f, 1.f, 1.f, float4(1.0f, 1.0f, 1.0f, 1.0f));
    std::vector<vertex::VertexPosNormalUv> vertices;
    std::vector<s32>                       indices;

    vertices.resize(meshdata.vertexData.size());
    indices.resize(meshdata.indexData.size());

    for(u64 i = 0; i < meshdata.vertexData.size(); i++) {
        vertices[i] = { meshdata.vertexData[i].pos,
                        meshdata.vertexData[i].nor,
                        meshdata.vertexData[i].tex };
    };

    for(u64 i = 0; i < meshdata.indexData.size(); i++) {
        indices[i] = meshdata.indexData[i];
    };

    _indexCount = (u32)meshdata.indexData.size();

    _pVertexBuffer = gpu::createBuffer({ vertices.size() * sizeof(vertex::VertexPosNormalUv), D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_IMMUTABLE }, vertices.data());
    _pIndexBuffer  = gpu::createBuffer({ indices.size() * sizeof(u32), D3D11_BIND_INDEX_BUFFER, D3D11_USAGE_IMMUTABLE }, indices.data());

    return true;
}
}   // namespace entity::go::ui
