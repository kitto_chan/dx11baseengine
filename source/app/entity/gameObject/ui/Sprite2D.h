﻿//---------------------------------------------------------------------------
//!	@file	Sprite2D.h
//!	@brief	プレイン2Dのクラス
//---------------------------------------------------------------------------
#pragma once
#include "entity/gameObject/GameObject.h"

namespace entity::go::ui {
class Sprite2D : public GameObject
{
public:
    //! コンストラクタ
    Sprite2D(const std::string& name     = "Sprite2D",
             const float3&      pos      = float3(0.0f, 0.0f, 0.0f),
             const float3&      scale    = float3(1.0f, 1.0f, 1.0f),
             const float3&      rotation = float3(0.0f, 0.0f, 0.0f));
    ~Sprite2D();   //!< デストラクタ

    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    void SetTexture(std::string_view path);           //!< 描画テクスチャーを設定
    void SetTexture(shr_ptr<gpu::Texture> texture);   //!< 描画テクスチャーを設定
    void SetColor(const float4& color) { _color = color; };

    //!< 画像表示のパーセントを設定
    void SetFillRate(f32 value)
    {
        if(value != _fillRate) {
            _attenuationCount   = 1.0f;
            _maxAttenuationRate = _lastFillRate;
        }
        _lastFillRate = _fillRate;
        _fillRate     = value;
    };
    //!< 塗りつぶすの色
    void SetFillColor(const float4& fillColor) { _fillColor = fillColor; }

private:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    // GPU関連
    shr_ptr<gpu::Buffer> _pVertexBuffer;   //!< 頂点バッファ
    shr_ptr<gpu::Buffer> _pIndexBuffer;    //!< インデックスバッファ
    u32                  _indexCount;      //!< インデックス数

    shr_ptr<gpu::Texture> _pTexture;   //!< SRV

    float4 _color = float4(1.0f, 1.0f, 1.0f, 1.0f);   //!< テクスチャー色

    f32    _fillRate     = 1.0f;                             //!< 0 ~ 1 画像表示のパーセント
    f32    _lastFillRate = _fillRate;                        //!< 0 ~ 1 最後画像表示のパーセント(衰弱用)
    float4 _fillColor    = float4(1.0f, 1.0f, 1.0f, 1.0f);   //!< 色

    f32 _attenuationRate    = 1.0f;   //!< 0 ~ 1 現在の衰弱アニメーションパーセント
    f32 _maxAttenuationRate = 1.0f;   //!< 0 ~ 1 fillRateからmaxAttenuationRateで衰弱する
    f32 _attenuationCount   = 1.0f;   //!< 0 ~ 1 衰弱計算カウント

    bool _isFilled            = true;   //!< 画像は塗りつぶすタイプですか
    bool _isUsedFillAnimation = true;   //!< 衰弱アニメーション
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
    bool OnInit() override;          //!< 初期化
    void OnUpdate() override;        //!< 更新
    void OnRender() override;        //!< 描画
    void OnRenderImgui() override;   //!< Imguiの描画

    bool InitGPU();   //!< GPU関連の初期化
};
}   // namespace entity::go::ui
