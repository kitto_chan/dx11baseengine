﻿//---------------------------------------------------------------------------
//!	@file	StaticSkyBox.h
//!	@brief	静的なスカイボックス
//---------------------------------------------------------------------------
#include "StaticSkyBox.h"
#include "utilityGPU/Geometry.h"
#include "manager/CameraManager.h"
namespace entity {
namespace {
struct MVP_CB
{
    matrix worldViewProj;
};
MVP_CB _mvpCb{};
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
StaticSkyBox::StaticSkyBox()
{
    _name = "StaticSkyBox";
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
StaticSkyBox::~StaticSkyBox()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool StaticSkyBox::Init()
{
    if(_pTextureCube == nullptr) {
        ASSERT_MESSAGE(false, "ファイルまだ読み込んでない");
        return false;
    }

    InitResource();
    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void StaticSkyBox::Update()
{
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void StaticSkyBox::Render()
{
    // 注視点を計算する
    matrix viewMatrix = manager::GetCurrentCameraViewMatrix();
    matrix projMatrix = manager::GetCurrentCameraProjMatrix();
    // スカイマップは永遠に中心だから、移動成分はいらないです
    viewMatrix._41_42_43_44 = float4(0.f, 0.f, 0.f, 1.f);
    _mvpCb.worldViewProj    = mul(viewMatrix, projMatrix);
    gpu::setConstantBuffer("CBVertex", _mvpCb);

    //---------
    // gpu 設定
    gpu::setVertexBuffer(0, _pVertexBuffer, sizeof(vertex::VertexPos));
    gpu::setIndexBuffer(_pIndexBuffer);

    //----------
    // 頂点シェーダ設定
    gpu::setInputLayout(_pILVertexPos);
    gpu::vs::setShader(_pVSSkyBox);

    //----------
    // ピクセルシェーダの設定
    gpu::ps::setShader(_pPSSkyBox);
    gpu::ps::setTexture(0, _pTextureCube);
    gpu::ps::setSamplerState(0, gpu::commonStates().LinearWrap());

    //----------
    // ラスタライザ
    gpu::setRasterizerState(gpu::commonStates().CullNone());

    //----------
    // Zバッファ
    gpu::setDepthStencilState(renderer::RenderStatesIns()->_pDSSLessEqual.Get());
    gpu::setBlendState(nullptr);

    // 最後描画
    gpu::drawIndexed(gpu::Primitive::TriangleList, _indexCount);
    gpu::setDepthStencilState(gpu::commonStates().DepthDefault());
    gpu::setBlendState(gpu::commonStates().Opaque());
}
//---------------------------------------------------------------------------
//! ファイルを読み込む　支援(dds / png)
//---------------------------------------------------------------------------
void StaticSkyBox::ReadFile(const std::string& filePath)
{
    std::string fileFormat = filePath.substr(filePath.size() - 3);

    if(fileFormat == "dds") {
        _pTextureCube = gpu::createTextureFromFile(filePath);
        ASSERT_MESSAGE(_pTextureCube, "Failed");
    }
    else if(fileFormat == "png") {
        if(!ReadPngFormat(filePath)) {
            ASSERT_MESSAGE(false, "Failed");
        }
    }
    else {
        ASSERT_MESSAGE(false, "サポートしていないフォンマットを読み込んだ");
    }
}
//---------------------------------------------------------------------------
// gpuのリソースの初期設定
//---------------------------------------------------------------------------
bool StaticSkyBox::InitResource()
{
    //--------
    // 球体のジオメトリを生成
    geometry::MeshData             meshdata = geometry::CreateSphere(1.0f);
    std::vector<vertex::VertexPos> vertexPos;
    std::vector<s32>               indices;

    vertexPos.resize(meshdata.vertexData.size());
    indices.resize(meshdata.indexData.size());

    // 頂点
    // TODO: コピーの方法あるかな?
    for(size_t i = 0; i < meshdata.vertexData.size(); i++) {
        vertexPos[i] = { meshdata.vertexData[i].pos };
    };

    // インデックス
    // TODO: コピーの方法あるかな?
    for(size_t i = 0; i < meshdata.indexData.size(); i++) {
        indices[i] = meshdata.indexData[i];
    };

    _indexCount = (u32)meshdata.indexData.size();

    //----------
    // バッファ
    _pVertexBuffer = gpu::createBuffer({ vertexPos.size() * sizeof(vertex::VertexPos), D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_IMMUTABLE }, vertexPos.data());
    _pIndexBuffer  = gpu::createBuffer({ indices.size() * sizeof(s32), D3D11_BIND_INDEX_BUFFER, D3D11_USAGE_IMMUTABLE }, indices.data());

    //----------
    // シェーダ
    _pVSSkyBox = gpu::createShader("gameSource/skyBox/StaticSkyBox_VS.hlsl", "VS", "vs_5_0");   // 頂点シェーダー
    _pPSSkyBox = gpu::createShader("gameSource/skyBox/StaticSkyBox_PS.hlsl", "PS", "ps_5_0");   // ピクセルシェーダー

    //----------
    // 入力レイアウト
    _pILVertexPos = gpu::createInputLayout(vertex::VertexPos::inputLayout, std::size(vertex::VertexPos::inputLayout));

    return true;
}
//---------------------------------------------------------------------------
// スカイボックスの Png Formatを読み込む
//---------------------------------------------------------------------------
bool StaticSkyBox::ReadPngFormat(const std::string& filePath)
{
    std::shared_ptr<gpu::Texture> srcTex = gpu::createTextureFromFile(filePath);
    if(srcTex == nullptr)
        return false;

    D3D11_TEXTURE2D_DESC texDesc, texArrayDesc;
    auto*                d3dTexture = static_cast<ID3D11Texture2D*>(srcTex->d3dResource());
    d3dTexture->GetDesc(&texDesc);

    //　高さと幅の割合は4:3
    if(texDesc.Width * 3 != texDesc.Height * 4) {
        //TODO: srcTexを解放
        return false;
    }

    // 立方体(SkyBox)一面の長さ "png"を分割する
    u32 squareLength = texDesc.Width / 4;

    //Texturecube
    texArrayDesc.Width              = squareLength;
    texArrayDesc.Height             = squareLength;
    texArrayDesc.MipLevels          = /*(generateMips ? texDesc.MipLevels - 2 :*/ 1;   // 立方体のmip
    texArrayDesc.ArraySize          = 6;
    texArrayDesc.Format             = texDesc.Format;
    texArrayDesc.SampleDesc.Count   = 1;
    texArrayDesc.SampleDesc.Quality = 0;
    texArrayDesc.Usage              = D3D11_USAGE_DEFAULT;
    texArrayDesc.BindFlags          = D3D11_BIND_SHADER_RESOURCE;
    texArrayDesc.CPUAccessFlags     = 0;
    texArrayDesc.MiscFlags          = D3D11_RESOURCE_MISC_TEXTURECUBE;

    ID3D11Texture2D* texArray = nullptr;
    if(FAILED(gpu::d3dDevice()->CreateTexture2D(&texArrayDesc, nullptr, &texArray))) {
        //TODO: srcTexを解放
        return false;
    };

    D3D11_BOX skybox{};
    // D3D11_BOXの座標：
    //    front
    //   /
    //  /_____right
    //  |
    //  |
    //  bottom
    skybox.front = 0;
    skybox.back  = 1;

    // Mipの計算
    for(u32 i = 0; i < texArrayDesc.MipLevels; i++) {
        skybox.left   = squareLength * 2;
        skybox.top    = squareLength;
        skybox.right  = squareLength * 3;
        skybox.bottom = squareLength * 2;

        // +X面コピー
        gpu::immediateContext()->CopySubresourceRegion(
            texArray,
            D3D11CalcSubresource(i, D3D11_TEXTURECUBE_FACE_POSITIVE_X, texArrayDesc.MipLevels),
            0, 0, 0,
            static_cast<ID3D11Texture2D*>(srcTex->d3dResource()),
            i,
            &skybox);

        // -X面コピー
        skybox.left   = 0;
        skybox.top    = squareLength;
        skybox.right  = squareLength;
        skybox.bottom = squareLength * 2;
        gpu::immediateContext()->CopySubresourceRegion(
            texArray,
            D3D11CalcSubresource(i, D3D11_TEXTURECUBE_FACE_NEGATIVE_X, texDesc.MipLevels),
            0, 0, 0,
            static_cast<ID3D11Texture2D*>(srcTex->d3dResource()),
            i,
            &skybox);

        // +Y面コピー
        skybox.left   = squareLength;
        skybox.top    = 0;
        skybox.right  = squareLength * 2;
        skybox.bottom = squareLength;
        gpu::immediateContext()->CopySubresourceRegion(
            texArray,
            D3D11CalcSubresource(i, D3D11_TEXTURECUBE_FACE_POSITIVE_Y, texArrayDesc.MipLevels),
            0, 0, 0,
            static_cast<ID3D11Texture2D*>(srcTex->d3dResource()),
            i,
            &skybox);

        // -Y面コピー
        skybox.left   = squareLength;
        skybox.top    = squareLength * 2;
        skybox.right  = squareLength * 2;
        skybox.bottom = squareLength * 3;
        gpu::immediateContext()->CopySubresourceRegion(
            texArray,
            D3D11CalcSubresource(i, D3D11_TEXTURECUBE_FACE_NEGATIVE_Y, texArrayDesc.MipLevels),
            0, 0, 0,
            static_cast<ID3D11Texture2D*>(srcTex->d3dResource()),
            i,
            &skybox);

        // +Z面コピー
        skybox.left   = squareLength;
        skybox.top    = squareLength;
        skybox.right  = squareLength * 2;
        skybox.bottom = squareLength * 2;
        gpu::immediateContext()->CopySubresourceRegion(
            texArray,
            D3D11CalcSubresource(i, D3D11_TEXTURECUBE_FACE_POSITIVE_Z, texArrayDesc.MipLevels),
            0, 0, 0,
            static_cast<ID3D11Texture2D*>(srcTex->d3dResource()),
            i,
            &skybox);

        // -Z面コピー
        skybox.left   = squareLength * 3;
        skybox.top    = squareLength;
        skybox.right  = squareLength * 4;
        skybox.bottom = squareLength * 2;
        gpu::immediateContext()->CopySubresourceRegion(
            texArray,
            D3D11CalcSubresource(i, D3D11_TEXTURECUBE_FACE_NEGATIVE_Z, texArrayDesc.MipLevels),
            0, 0, 0,
            static_cast<ID3D11Texture2D*>(srcTex->d3dResource()),
            i,
            &skybox);

        // 次mipLevelは本来の半分
        squareLength /= 2;
    }

    D3D11_SHADER_RESOURCE_VIEW_DESC viewDesc;
    viewDesc.Format                      = texArrayDesc.Format;
    viewDesc.ViewDimension               = D3D11_SRV_DIMENSION_TEXTURECUBE;
    viewDesc.TextureCube.MostDetailedMip = 0;
    viewDesc.TextureCube.MipLevels       = texArrayDesc.MipLevels;

    _pTextureCube = gpu::createTexture(texArray);

    texArray->Release();

    return true;
}
}   // namespace entity
