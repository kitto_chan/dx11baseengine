﻿//---------------------------------------------------------------------------
//!	@file	StaticSkyBox.h
//!	@brief	静的なスカイボックス
//---------------------------------------------------------------------------
#pragma once
#include "../Entity.h"
namespace component {
class Transform;
};

namespace entity {
class StaticSkyBox
{
public:
    StaticSkyBox();
    virtual ~StaticSkyBox();

    // コピー禁止 / ムーブ禁止
    StaticSkyBox(StaticSkyBox&) = delete;
    StaticSkyBox operator=(const StaticSkyBox&) = delete;
    StaticSkyBox(StaticSkyBox&&)                = default;
    StaticSkyBox& operator=(StaticSkyBox&&) = default;

    //---------------------------------------------------------------------------
    //! public 関数
    //---------------------------------------------------------------------------

    bool Init();                                 //!< 初期化
    void Update();                               //!< 更新
    void Render();								 //!< 描画

    void ReadFile(const std::string& filepath);   //<! ファイルを読み込む

private:
    //---------------------------------------------------------------------------
    //! private 変数
    //---------------------------------------------------------------------------

    // GPU関連
    shr_ptr<gpu::Buffer> _pVertexBuffer;   //!< 頂点バッファ
    shr_ptr<gpu::Buffer> _pIndexBuffer;    //!< インデックスバッファ
    u32                  _indexCount;      //!< インデックス数

    //---------------------------------------------------------------------------
    //! シェーダ
    //---------------------------------------------------------------------------
    shr_ptr<gpu::Shader> _pVSSkyBox;   //!< 頂点シェーダ
    shr_ptr<gpu::Shader> _pPSSkyBox;   //!< ピクセルシェーダ

    //---------------------------------------------------------------------------
    //! 入力レイアウト
    //---------------------------------------------------------------------------
    shr_ptr<gpu::InputLayout> _pILVertexPos;   //!< 入力レイアウト

    //---------------------------------------------------------------------------
    //! テクスチャー
    //---------------------------------------------------------------------------
    shr_ptr<gpu::Texture> _pTextureCube;   //!< SRV

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    std::string _name;   //!< 名前

    //---------------------------------------------------------------------------
    //! private 変数
    //---------------------------------------------------------------------------
    bool InitResource();                               //!< gpuのリソースの初期設定
    bool ReadPngFormat(const std::string& filePath);   //!< Pngフォンマットを読み込む
};
}   // namespace entity
