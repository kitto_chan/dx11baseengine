﻿//---------------------------------------------------------------------------
//!	@file	CameraManager.h
//!	@brief	ゲームカメラ管理
//---------------------------------------------------------------------------
#pragma once
#include "CameraManager.h"
#include "entity/gameObject/Camera/FirstPersonCamera.h"
#include "entity/gameObject/Camera/ThirdPersonCamera.h"
#include "entity/gameObject/camera/DebugCamera.h"

#include "component/Transform.h"
namespace manager {
namespace {
raw_ptr<CameraManager> _pCurrentCameraMgr;

sys::SystemState lastState = sys::SystemState::Play;   //!< 前フレームのステートを保存する
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
CameraManager::CameraManager(bool setToCurrentCamera)
{
    _isSetToCurrentCamera = setToCurrentCamera;
    /* if(setToCurrentCamera)
		_pCurrentCameraMgr = this;*/
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
CameraManager::~CameraManager()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool CameraManager::Init()
{
    if(_isSetToCurrentCamera) _pCurrentCameraMgr = this;
    // デバッグ用カメラ
    // デバッグカメラ特別で作るので、
    // EntityManager と cameraリストに含まれてないです
    _pDebugCamera = std::make_unique<gameobject::DebugCamera>("Debug Camera");
    _pDebugCamera->Init();

    // デフォルトカメラ
    if(!_pCurrentCamera) {
        _pCurrentCamera = _pDebugCamera;
        //ASSERT_MESSAGE(false, "カメラつけてないです");
    }

    return true;
}

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void CameraManager::Update()
{
    if(lastState != SystemSettingsMgr()->GetSystemState()) {
        //!< デバッグカメラの位置シーンカメラ
        SyncDebugCameraSceneCamera();
    }

    // カメラバッファ更新
    if(SystemSettingsMgr()->IsPauseState()) {
        _pDebugCamera->Update();   // デバッグカメラ更新
    }

    // カメラバッファ更新
    GetCurrentCamera()->SetCameraCB();

    lastState = SystemSettingsMgr()->GetSystemState();
}

//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void CameraManager::Render()
{
}

void CameraManager::RenderImgui()
{
    //ImGui::Begin("DebugCamera");
    //imgui::ImguiDragXYZ("Pos", _pDebugCamera->GetPosition());
    //imgui::ImguiDragXYZ("Rot", _pDebugCamera->GetRotation());
    //ImGui::End();
}

//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void CameraManager::Finalize()
{
}
//---------------------------------------------------------------------------
//! 現在シーンのカメラを取得
//---------------------------------------------------------------------------
raw_ptr<gameobject::BaseCamera> CameraManager::GetCurrentCamera() const
{
    if(SystemSettingsMgr()->IsPauseState()) {
        return _pDebugCamera;
    }
    else {
        return _pCurrentCamera;
    }
}
//---------------------------------------------------------------------------
//! カメラを追加する
//---------------------------------------------------------------------------
void CameraManager::AddCamera(raw_ptr<gameobject::BaseCamera> newCamera, bool setToCurrent)
{
    _cameras.emplace_back(newCamera);
    if(setToCurrent) _pCurrentCamera = newCamera;
}
//---------------------------------------------------------------------------
//! デバッグカメラとシーンのカメラシンクロする
//---------------------------------------------------------------------------
void CameraManager::SyncDebugCameraSceneCamera()
{
    *_pDebugCamera->GetComponent<component::Transform>().get() = *_pCurrentCamera->GetComponent<component::Transform>().get();
}

//---------------------------------------------------------------------------
//! カメラ管理クラスを取得
//---------------------------------------------------------------------------
raw_ptr<manager::CameraManager> GetCurrentCameraMgr()
{
    return _pCurrentCameraMgr;
}
//---------------------------------------------------------------------------
//! 現在のカメラを取得
//---------------------------------------------------------------------------
raw_ptr<gameobject::BaseCamera> GetCurrentCamera()
{
    return GetCurrentCameraMgr()->GetCurrentCamera();
}
//---------------------------------------------------------------------------
//! カメラView Matrixを取得
//---------------------------------------------------------------------------
matrix GetCurrentCameraViewMatrix()
{
    return _pCurrentCameraMgr->GetCurrentCamera()->GetViewMatrix();
}
//---------------------------------------------------------------------------
//! カメラProjection Matrixを取得
//---------------------------------------------------------------------------
matrix GetCurrentCameraProjMatrix()
{
    return _pCurrentCameraMgr->GetCurrentCamera()->GetProjMatrix();
}
//---------------------------------------------------------------------------
//! カメラの頂点バッファを設定
//---------------------------------------------------------------------------
void SetCurrentCameraCB()
{
    _pCurrentCameraMgr->GetCurrentCamera()->SetCameraCB();
}
}   // namespace manager
