﻿//---------------------------------------------------------------------------
//!	@file	EntityManager.cpp
//!	@brief	エンティティマネージャー
//---------------------------------------------------------------------------
#include "EntityManager.h"
#include "LightingManager.h"

#include "Entity/Entity.h"
#include "entity/gameObject/GameObject.h"

#include "component/Transform.h"
#include "component/ModelRenderer.h"
#include "renderer/ShadowMap.h"

#include "CameraManager.h"
namespace manager {
namespace {
raw_ptr<manager::EntityManager> _pCurrentWorld;   // 現在のワールド

ImGuiTreeNodeFlags baseNodeFlags = ImGuiTreeNodeFlags_None | ImGuiTreeNodeFlags_SpanAvailWidth;
s32                selectionMask = (1 << 2);
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
EntityManager::EntityManager(bool setToCurrentWorld)
{
    _isSetToCurrentWorld = setToCurrentWorld;
    //if(setToCurrentWorld)
    //    _pCurrentWorld = this;
}
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
EntityManager::~EntityManager()
{
    UnregistAll();
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool EntityManager::Init()
{
    if(_isSetToCurrentWorld) _pCurrentWorld = this;
    InitEntities();
    HandleWaitingEntities();
    return true;
}

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void EntityManager::Update()
{
    UpdateEntities();
    HandleWaitingEntities();
}

//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void EntityManager::Render()
{
    RenderEntities();
}

//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void EntityManager::RenderImgui()
{
    // Hierarchy Ui
    imgui::ImGuiHierarchy(GetRawEntities(), *this, false);

    // Inspector Ui
    RenderEntitiesImgui();
}

//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void EntityManager::Finalize()
{
    FinializeEntities();
}

//---------------------------------------------------------------------------
//! エンティティを登録
//! @params		[in]	生ポインタ
//---------------------------------------------------------------------------
void EntityManager::Regist(raw_ptr<Entity> entity)
{
    shr_ptr<Entity> pEntity(entity.get());
    Regist(std::move(pEntity));
}
//---------------------------------------------------------------------------
//! エンティティを登録
//! @params		[in]	Shared ポインタ
//---------------------------------------------------------------------------
void EntityManager::Regist(shr_ptr<Entity> entity)
{
    if(_isAccessingEntities) {
        _waitingEntities.push(entity);
    }
    else {
        _entities.push_back(entity);
    }
}
//---------------------------------------------------------------------------
//! 特定エンティティの登録を解除
//---------------------------------------------------------------------------
void EntityManager::Unregist(raw_ptr<Entity> entity)
{
    for(auto itr = _entities.begin(); itr != _entities.end(); ++itr) {
        if((*itr).get() == entity.get()) {
            shr_ptr<Entity> pEntity = *itr;

            if(pEntity->_isRelease) {
                pEntity->Finalize();
                //pEntity.reset();
            }
            // *** shared ptrなので resetはただこのpEntity ref count -1意味がないので
            //     そのまま管理している配列から削除したら、自動的に削除します。
            // (x) pEntity.reset();
            _entities.erase(itr);
            break;
        }
    }
}
//---------------------------------------------------------------------------
//! 既存のエンティティの登録を解除
//---------------------------------------------------------------------------
void EntityManager::UnregistAll()
{
    // TODO....Have Bug removed.
    // 後ろから削除
    // ref https://stackoverflow.com/questions/1830158/how-to-call-erase-with-a-reverse-iterator

    for(auto itr = _entities.begin(); itr != _entities.end();) {
        shr_ptr<Entity> pEntity = std::move(*itr);

        // ** 他のところもう使っていないを確保する **
        // _entitiesリストといまのpEntityなので use count = 1
        ASSERT_MESSAGE(pEntity.use_count() == 1, "他のところまだこのEntity使っている");

        ASSERT_MESSAGE(!pEntity->_isRemoved, "削除したはずEntityまだ生きている");

        if(!pEntity->_isRemoved) {
            pEntity->_isRemoved = true;

            // *** shared ptrなので resetはただこのpEntity ref count -1意味がないので
            //     そのまま管理している配列から削除したら、自動的に削除します。
            // (x) pEntity.reset();
            itr = _entities.erase(itr);
            pEntity.reset();
            //_entities.erase(std::next(itr).base());
        }
        else {
            ++itr;
        }
    }

    _entities.clear();
}
//---------------------------------------------------------------------------
//! エンティティのリストを取得
//---------------------------------------------------------------------------
std::vector<shr_ptr<Entity>>& EntityManager::GetEntities()
{
    return _entities;
}
//---------------------------------------------------------------------------
//!< 生ポインタのエンティティリストを取得
//---------------------------------------------------------------------------
std::vector<raw_ptr<Entity>> EntityManager::GetRawEntities()
{
    std::vector<raw_ptr<Entity>> enttList;
    for(auto& entity : _entities) {
        enttList.push_back(entity);
    }
    return enttList;
}
//---------------------------------------------------------------------------
//! 選択しているエンティティのリストを取得
//---------------------------------------------------------------------------
raw_ptr<Entity> EntityManager::GetSelectedEntities() const
{
    return _selectedEntity;
}
//---------------------------------------------------------------------------
//! 選択しているエンティティのリストを設定
//---------------------------------------------------------------------------
void EntityManager::SetSelectedEntity(const raw_ptr<Entity>& selected)
{
    _selectedEntity = selected;
}
//---------------------------------------------------------------------------
//! エンティティを初期化する
//---------------------------------------------------------------------------
bool EntityManager::InitEntities()
{
    _isAccessingEntities = true;
    for(auto& _entity : _entities) {
        if(!_entity->_isInited) {
            _entity->Init();
        }
    }
    _isAccessingEntities = false;

    return true;
}
//---------------------------------------------------------------------------
//! エンティティを更新する
//---------------------------------------------------------------------------
void EntityManager::UpdateEntities()
{
    _isAccessingEntities = true;

    // TODO:
    // Layer順番からUpdate
    for(auto& entity : _entities) {
        if(!entity) continue;
        // もし初期化してないから先に初期化する
        if(!entity->_isInited) {
            entity->Init();
        }
        // もしリリースしたらメインリストから削除
        if(entity->_isRelease) {
            Unregist(entity);
            continue;
        }
        // 更新を行う
        entity->Update();
    }

    // LateUpdateを行う
    for(auto& entity : _entities) {
        if(!entity) continue;

        entity->LateUpdate();
    }

    _isAccessingEntities = false;
}
//---------------------------------------------------------------------------
//! エンティティを描画するする
//---------------------------------------------------------------------------
void EntityManager::RenderEntities()
{
    _isAccessingEntities = true;

    gpu::setRasterizerState(dx11::commonStates().CullNone());

    for(auto entity : _entities) {
        entity->Render();
    }
    _isAccessingEntities = false;
}
//---------------------------------------------------------------------------
//! 影を描画する
//---------------------------------------------------------------------------
void EntityManager::RenderEntitiesShadow()
{
    _isAccessingEntities = true;
    for(auto entity : _entities) {
        // TODO: Layerから判定する
        // entity->GetLayer() == Wall | Player
        if(entity->GetComponent<component::ModelRenderer>()) {
            entity->Render();
        }
    }
    _isAccessingEntities = false;
}
//---------------------------------------------------------------------------
//! エンティティのImguiを描画する
//---------------------------------------------------------------------------

void EntityManager::RenderEntitiesImgui()
{
    ImGui::Begin("Inspector");
    if(_selectedEntity != nullptr) {
        GetSelectedEntities()->RenderImgui();
    }
    ImGui::End();
}
//---------------------------------------------------------------------------
//! エンティティを解放する
//---------------------------------------------------------------------------

void EntityManager::FinializeEntities()
{
    for(auto entity : _entities) {
        if(!entity->_isRelease)
            entity->Finalize();
    }
}
//---------------------------------------------------------------------------
//! 選択しているエンティティのリストを設定
//---------------------------------------------------------------------------
void EntityManager::HandleWaitingEntities()
{
    while(!_waitingEntities.empty()) {
        _entities.emplace_back(std::move(_waitingEntities.front()));
        _waitingEntities.pop();
    }
}
//---------------------------------------------------------------------------
//!< 今のワールド取得
//---------------------------------------------------------------------------
raw_ptr<EntityManager>& GetCurrentWorld()
{
    return _pCurrentWorld;
}
//---------------------------------------------------------------------------
//! エンティティを名前で検索する
//! @params name 名前
//---------------------------------------------------------------------------
raw_ptr<Entity> FindEntityWithName(std::string_view name)
{
    if(!_pCurrentWorld) return nullptr;
    for(auto entity : _pCurrentWorld->GetEntities()) {
        if(entity->GetName() == name) {
            return entity;
        }
    }

    return nullptr;
}
//---------------------------------------------------------------------------
//! エンティティをタグで検索する
//! @params name タグ
//---------------------------------------------------------------------------
std::vector<raw_ptr<Entity>> FindEntitiesWithTag(Tag tag)
{
    std::vector<raw_ptr<Entity>> entityList;

    if(!_pCurrentWorld) return entityList;

    for(auto& entity : _pCurrentWorld->GetEntities()) {
        if(entity->GetTag() == tag) {
            entityList.push_back(entity);
        }
    }

    return entityList;
}
//---------------------------------------------------------------------------
//! 現在のワールドに登録する(生ポインタ)
//---------------------------------------------------------------------------
void Regist(raw_ptr<Entity> entity)
{
    _pCurrentWorld->Regist(entity);
}
//---------------------------------------------------------------------------
//! 現在のワールドに登録する(シェアポインタ)
//---------------------------------------------------------------------------
void Regist(shr_ptr<Entity> entity)
{
    _pCurrentWorld->Regist(entity);
}
//---------------------------------------------------------------------------
//!	エンティティの登録解除
//---------------------------------------------------------------------------
void Unregist(raw_ptr<Entity> entity)
{
    _pCurrentWorld->Unregist(entity);
}
}   // namespace manager
