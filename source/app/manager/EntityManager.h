﻿//---------------------------------------------------------------------------
//!	@file	EntityManager.h
//!	@brief	エンティティマネージャー
//---------------------------------------------------------------------------
#pragma once
#include <queue>

namespace entity {
class Entity;
}
using namespace entity;

namespace manager {
class EntityManager
{
public:
    EntityManager(bool setToCurrentWorld = true);
    ~EntityManager();

    //---------------------------------------------------------------------------
    //! public 関数
    //---------------------------------------------------------------------------
    bool Init();                   //!< 初期化
    void Update();                 //!< 更新
    void Render();                 //!< 描画
    void RenderEntitiesShadow();   //!< 影描画
    void RenderImgui();            //!< Imgui描画
    void Finalize();               //!< 解放

    void Regist(raw_ptr<Entity> entity);     //!< エンティティを登録 (生ポインタ)
    void Regist(shr_ptr<Entity> entity);     //!< エンティティを登録 (シェアポインタ)
    void Unregist(raw_ptr<Entity> entity);   //!< エンティティの登録解除
    void UnregistAll();                      //!< すべてエンティティの登録解除

    std::vector<shr_ptr<Entity>>& GetEntities();      //!< エンティティズを取得
    std::vector<raw_ptr<Entity>>  GetRawEntities();   //!< 生ポインタのエンティティリストを取得

    //! エンティティの生ポインターを取得
    //! @note すべてのエンティティ所有権はここで管理する、生ポインターしか取得できる
    raw_ptr<Entity> GetSelectedEntities() const;
    void            SetSelectedEntity(const raw_ptr<Entity>& selected);   //!< 選ばれたエンティティを設定

private:
    //---------------------------------------------------------------------------
    //! private関数
    //---------------------------------------------------------------------------
    bool InitEntities();          //!< エンティティを初期化する
    void UpdateEntities();        //!< エンティティを更新する
    void RenderEntities();        //!< エンティティを描画するする
    void RenderEntitiesImgui();   //!< エンティティのImguiを描画する
    void FinializeEntities();     //!< エンティティを解放する

    void HandleWaitingEntities();   //!< 待処理リストにいるエンティティを処理する
    //---------------------------------------------------------------------------
    //! private変数
    //---------------------------------------------------------------------------
    std::vector<shr_ptr<Entity>> _entities;   //!< メインエンティティのリスト

    //! 待処理リスト
    //! @note たとえば更新中にエンティティを追加する時は
    //!		  メインエンティティリストはアクセスしているので、
    //!       登録ときは先にこの待処理リストに追加して、
    //!		　メインエンティティリスト処理終わったら移行する
    std::queue<shr_ptr<Entity>> _waitingEntities;

    raw_ptr<Entity> _selectedEntity;   //!< 選んでいるエンティティ

    bool _isSetToCurrentWorld = true;   //!< 現在のワールドに設定するか

    bool _isAccessingEntities = false;   //!< エンティティのリストがアクセスしてるかどうか
};

raw_ptr<EntityManager>&      GetCurrentWorld();                           //!< 今のワールド取得
raw_ptr<Entity>              FindEntityWithName(std::string_view name);   //!< エンティティを検索する(名前)
std::vector<raw_ptr<Entity>> FindEntitiesWithTag(Tag tag);                //!< エンティティを検索する(タグ)

void Regist(raw_ptr<Entity> entity);     //!< 現在のワールドに登録する(生ポインタ)
void Regist(shr_ptr<Entity> entity);     //!< 現在のワールドに登録する(シェアポインタ)
void Unregist(raw_ptr<Entity> entity);   //!< エンティティの登録解除
}   // namespace manager
