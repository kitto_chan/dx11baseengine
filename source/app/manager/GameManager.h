﻿//---------------------------------------------------------------------------
//!	@file	GameManager.h
//!	@brief	ゲームの状態管理
//! @note 　このクラスのデザインちょっとおかしいかも、TODO: Better Design
//---------------------------------------------------------------------------
#pragma once

namespace manager {
using namespace gameApp;
class GameManager : public Singleton<GameManager>
{
public:
    GameManager();    //!< コンストラクタ
    ~GameManager();   //!< デストラクタ

    //---------------------------------------------------------------------------
    // 関数
    //---------------------------------------------------------------------------
    std::string _playerAnim = "anim/PlayerVampire.anim";
    std::string _playerModelName    = "vampire";
    u32         _playerWeaponBoneId = 0;

private:
    //---------------------------------------------------------------------------
    // 内部変数
    //---------------------------------------------------------------------------
};
//! カメラ管理クラスを取得
manager::GameManager* GameMgr();

}   // namespace manager
