﻿//---------------------------------------------------------------------------
//!	@file	LightingManager.h
//!	@brief	ライトの管理クラス
//---------------------------------------------------------------------------
#pragma once
#include "LightingManager.h"
#include "CameraManager.h"
#include "component/Transform.h"
#include "EntityManager.h"
namespace manager {
namespace {
raw_ptr<LightingManager> _pCurrentLightingManager;

cb::PhongLightingCB _phongLightingCB;
cb::PBRLightingCB   _pbrLightingCB;
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
LightingManager::LightingManager(bool setToCurrent)
{
    _isSetToCurrent = setToCurrent;
}
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
LightingManager::~LightingManager()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool LightingManager::Init()
{
    if(_isSetToCurrent) _pCurrentLightingManager = this;

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void LightingManager::Update()
{
    for(int i = 0; i < MAX_LIGHTS; i++) {
        if(i < _directionalLightList.size()) {
            auto& light = _directionalLightList[i];
            if(light) {
                _phongLightingCB.dirLights[i] = light->GetPhongDirectionalLightCB();
                _pbrLightingCB.dirLights[i]   = light->GetPBRDirectionalLightCB();
            }
        }

        if(i < _pointLightList.size()) {
            auto& light = _pointLightList[i];
            if(light) {
                _phongLightingCB.pointLights[i] = light->GetPhongPointightCB();
                _pbrLightingCB.pointLights[i]   = light->GetPBRPointLightCB();
            }
        }

        if(i < _spotLightList.size()) {
            auto& light = _spotLightList[i];
            if(light) {
                _phongLightingCB.spotLights[i] = light->GetPhongSpotLightCB();
                //TODO PBR spotLight
            }
        }
    }

    u32 passDirSize   = std::min(MAX_LIGHTS, (u32)_directionalLightList.size());
    u32 passPointSize = std::min(MAX_LIGHTS, (u32)_pointLightList.size());
    u32 passSpotSize  = std::min(MAX_LIGHTS, (u32)_spotLightList.size());

    _phongLightingCB.dirLightNum = _pbrLightingCB.dirLightNum = passDirSize;
    _phongLightingCB.pointLightNum = _pbrLightingCB.pointLightNum = passPointSize;
    _phongLightingCB.spotLightNum                                 = passSpotSize;

    // ここで毎回変わるのを処理重いと思う、ライトは常に変わるではない
    // TODO： dirty flag
    gpu::setConstantBuffer("PhongLightingCB", _phongLightingCB);
    gpu::setConstantBuffer("PBRLightingCB", _pbrLightingCB);
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void LightingManager::Render()
{
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void LightingManager::RenderImgui()
{
    //-------------------------------
    // メニューバーからライトを生成する
    //-------------------------------
    ImGui::Begin("MainDockspace");
    if(ImGui::BeginMenuBar()) {
        if(ImGui::BeginMenu("Game Object")) {
            if(ImGui::BeginMenu("Lighting")) {
                if(ImGui::MenuItem("DirectionalLight")) {
                    CreateDirectionalLight();
                }
                if(ImGui::MenuItem("PointLight")) {
                    CreatePointLight();
                }
                if(ImGui::MenuItem("SpotLight")) {
                    CreateSpotLight();
                }
                ImGui::EndMenu();
            }
            ImGui::EndMenu();
        }
        ImGui::EndMenuBar();
    }
    ImGui::End();
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void LightingManager::Finalize()
{
}
//---------------------------------------------------------------------------
//! Directional Lightを登録
//---------------------------------------------------------------------------
void LightingManager::Regist(raw_ptr<gameobject::DirectionalLight> light)
{
    ASSERT_MESSAGE(_directionalLightList.size() < MAX_LIGHTS, "ライト多すぎる");
    _directionalLightList.push_back(light);
    manager::Regist(light.get());
}
//---------------------------------------------------------------------------
//! Point Lightを登録
//---------------------------------------------------------------------------
void LightingManager::Regist(raw_ptr<gameobject::PointLight> light)
{
    ASSERT_MESSAGE(_pointLightList.size() < MAX_LIGHTS, "ライト多すぎる");
    _pointLightList.push_back(light);
    manager::Regist(light.get());
}
void LightingManager::Regist(shr_ptr<gameobject::PointLight> light)
{
    ASSERT_MESSAGE(_pointLightList.size() < MAX_LIGHTS, "ライト多すぎる");
    _pointLightList.push_back(light);
    manager::Regist(light);
}
//---------------------------------------------------------------------------
//! Spot Lightを登録
//---------------------------------------------------------------------------
void LightingManager::Regist(raw_ptr<gameobject::SpotLight> light)
{
    ASSERT_MESSAGE(_spotLightList.size() < MAX_LIGHTS, "ライト多すぎる");
    _spotLightList.push_back(light);
    manager::Regist(light.get());
}
//---------------------------------------------------------------------------
//! Directional Lightを生成
//---------------------------------------------------------------------------
raw_ptr<gameobject::DirectionalLight> LightingManager::CreateDirectionalLight()
{
    gameobject::DirectionalLight* light = new gameobject::DirectionalLight();

    SetLightAlignToCamera(light);
    Regist(light);

    return light;
}
//---------------------------------------------------------------------------
//! Point Lightを生成
//---------------------------------------------------------------------------
raw_ptr<gameobject::PointLight> LightingManager::CreatePointLight()
{
    gameobject::PointLight* light = new gameobject::PointLight();

    SetLightAlignToCamera(light);
    Regist(light);

    return light;
}
//---------------------------------------------------------------------------
//! Spot Lightを生成
//---------------------------------------------------------------------------
raw_ptr<gameobject::SpotLight> LightingManager::CreateSpotLight()
{
    gameobject::SpotLight* light = new gameobject::SpotLight();

    SetLightAlignToCamera(light);
    Regist(light);

    return light;
}
//---------------------------------------------------------------------------
//! ライトの座標とカメラ座標を整列する
//---------------------------------------------------------------------------
void LightingManager::SetLightAlignToCamera(raw_ptr<gameobject::ILight> light)
{
    auto   cameraTrans = manager::GetCurrentCameraMgr()->GetCurrentCamera()->GetComponent<component::Transform>();
    float3 cameraPos   = cameraTrans->GetPosition();

    cameraPos += normalize(cameraTrans->GetForwardAxis());
    light->GetComponent<component::Transform>()->SetRotation(cameraTrans->GetRotation());
    light->GetComponent<component::Transform>()->SetPosition(cameraPos);
}
//---------------------------------------------------------------------------
//! //! ライト管理クラスを取得
//---------------------------------------------------------------------------
raw_ptr<manager::LightingManager> GetCurrentLightingMgr()
{
    return _pCurrentLightingManager;
}
raw_ptr<gameobject::DirectionalLight> GetDefaultLight()
{
    return GetCurrentLightingMgr()->GetDefaultLight();
}
}   // namespace manager
