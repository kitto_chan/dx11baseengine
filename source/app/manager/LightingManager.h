﻿//---------------------------------------------------------------------------
//!	@file	LightingManager.h
//!	@brief	ゲームライト管理
//---------------------------------------------------------------------------
#pragma once
#include "manager/EntityManager.h"

#include "entity/gameObject/lighting/DirectionalLight.h"
#include "entity/gameObject/lighting/PointLight.h"
#include "entity/gameObject/lighting/SpotLight.h"

//! 最大ライティングの数
//! @note これ変更するとシェーダに変更する必要がある
//! TODO シェーダ Reflection
static constexpr u32 MAX_LIGHTS = 2;   //!< 最大ライト数 (ライト別)

namespace cb {
//! Phongの反射モデル用頂点バッファ
struct PhongLightingCB
{
    PhongDirectionalLightCB dirLights[MAX_LIGHTS];     //!< ディレクショナルライト
    PhongPointLightCB       pointLights[MAX_LIGHTS];   //!< ポイントライト
    PhongSpotLightCB        spotLights[MAX_LIGHTS];    //!< スポットライト

    s32 dirLightNum;     //!< ディレクショナルライトの数
    s32 pointLightNum;   //!< ポイントライトの数
    s32 spotLightNum;    //!< スポットライトの数

    s32 useless;
};
//! PBRの反射モデル用頂点バッファ
struct PBRLightingCB
{
    PBRDirectionalLightCB dirLights[MAX_LIGHTS];     //!< ディレクショナルライト
    PBRPointLightCB       pointLights[MAX_LIGHTS];   //!< ポイントライト

    s32 dirLightNum;     //!< ディレクショナルライトの数
    s32 pointLightNum;   //!< ポイントライトの数

    s32 useless[2];
};
}   // namespace cb
namespace manager {

class LightingManager
{
public:
    //! ライトの種類
    enum Type
    {
        DirectionalLight,   //!< ディレクショナルライト
        PointLight,         //!< ポイントライト
        SpotLight           //!< スポットライトの数
    };

    LightingManager(bool setToCurrent = true);   //!< コンストラクタ
    ~LightingManager();                          //!< デストラクタ

    //---------------------------------------------------------------------------
    // 関数
    //---------------------------------------------------------------------------
    bool Init();          //!< 初期化
    void Update();        //!< 更新
    void Render();        //!< 描画
    void RenderImgui();   //!< Imgui描画
    void Finalize();      //!< 解放

    void Regist(raw_ptr<gameobject::DirectionalLight> light);   //!< Directional Lightを登録
    void Regist(raw_ptr<gameobject::PointLight> light);         //!< Point Lightを登録
    void Regist(shr_ptr<gameobject::PointLight> light);         //!< Point Lightを登録
    void Regist(raw_ptr<gameobject::SpotLight> dirLight);       //!< Spot Lightを登録

    raw_ptr<gameobject::DirectionalLight> CreateDirectionalLight();   //!< Directional Lightを生成
    raw_ptr<gameobject::PointLight>       CreatePointLight();         //!< Point Lightを生成
    raw_ptr<gameobject::SpotLight>        CreateSpotLight();          //!< Spot Lightを生成

    //! ライトの座標とカメラ座標を整列する
    static void SetLightAlignToCamera(raw_ptr<gameobject::ILight> light);

	void SetDefaultLight(raw_ptr<gameobject::DirectionalLight> light) { _defaultLight = light; }

	raw_ptr<gameobject::DirectionalLight> GetDefaultLight() { return _defaultLight; }

private:
    //---------------------------------------------------------------------------
    // 内部変数
    //---------------------------------------------------------------------------
    //!< Directional Lightのリスト
    std::vector<raw_ptr<gameobject::DirectionalLight>> _directionalLightList;
    //!< Point Lightのリスト
    std::vector<raw_ptr<gameobject::PointLight>> _pointLightList;
    //!< Spot Lightのリスト
    std::vector<raw_ptr<gameobject::SpotLight>> _spotLightList;

    bool _isSetToCurrent = true;

    raw_ptr<gameobject::DirectionalLight> _defaultLight;
};
//! ライト管理クラスを取得
raw_ptr<manager::LightingManager> GetCurrentLightingMgr();
raw_ptr<gameobject::DirectionalLight> GetDefaultLight();

}   // namespace manager
