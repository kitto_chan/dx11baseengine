﻿//---------------------------------------------------------------------------
//!	@file	scene.cpp
//!	@brief	シーン管理
//---------------------------------------------------------------------------
#include "manager/SceneManager.h"
#include "manager/EntityManager.h"

#include <thread>
namespace scene {
class TitleScene;
class TutorialScene;
class GameScene;
class GameClearScene;
class GameOverScene;

class TestScene;
class NetworkGameScene;

class LoadingSceneMultiThread;
class LoadingScene;

class AnimationConfigScene;
}   // namespace scene

extern template std::unique_ptr<scene::BaseScene> scene::createNewScene<scene::GameScene>();
extern template std::unique_ptr<scene::BaseScene> scene::createNewScene<scene::TitleScene>();
extern template std::unique_ptr<scene::BaseScene> scene::createNewScene<scene::TutorialScene>();
extern template std::unique_ptr<scene::BaseScene> scene::createNewScene<scene::GameClearScene>();
extern template std::unique_ptr<scene::BaseScene> scene::createNewScene<scene::GameOverScene>();

extern template std::unique_ptr<scene::BaseScene> scene::createNewScene<scene::AnimationConfigScene>();

extern template std::unique_ptr<scene::BaseScene> scene::createNewScene<scene::LoadingSceneMultiThread>();
extern template std::unique_ptr<scene::BaseScene> scene::createNewScene<scene::LoadingScene>();
extern template std::unique_ptr<scene::BaseScene> scene::createNewScene<scene::TestScene>();
extern template std::unique_ptr<scene::BaseScene> scene::createNewScene<scene::NetworkGameScene>();

namespace manager {

namespace {

std::unique_ptr<scene::BaseScene> _scene;       //!<　今のシーン
std::unique_ptr<scene::BaseScene> _sceneNext;   //!<　次のシーン

std::unique_ptr<scene::BaseScene> _loadingScene;   //!< ロード画面用のシーン

std::thread* _loadingThread;   //!< ロード用のスレッド

constexpr bool USE_LOADING_THREAD = FALSE;   //!< ロード用のスレッドを使うかどうか
}   // namespace

//--------------------------------------------------------------------------
//! 初期化
//--------------------------------------------------------------------------
bool onInitialize([[maybe_unused]] u32 width, [[maybe_unused]] u32 height)
{
    if constexpr(USE_LOADING_THREAD) {
        _loadingScene = scene::createNewScene<scene::LoadingSceneMultiThread>();
        _loadingScene->Init();
    }
    //----------------------------------------------------------
    // 初期シーンの作成
    //----------------------------------------------------------
    _sceneNext = scene::createNewScene<scene::TitleScene>();
    if(!_sceneNext)
        return false;

    return true;
}

//--------------------------------------------------------------------------
//! 更新
//! @attention ここでは描画発行は行わないこと
//--------------------------------------------------------------------------
void onUpdate(f32 deltaTime)
{
    if(_sceneNext) {
        if(_scene && _scene->IsInited()) {
            _scene->Finalize();
        }
        _scene = std::move(_sceneNext);

        if constexpr(USE_LOADING_THREAD) {
            if(_loadingThread) {
                if(_loadingThread->joinable()) {
                    _loadingThread->join();
                    delete _loadingThread;
                }
            }
            // 初期化
            SystemSettingsMgr()->SetLoading(true);
            _loadingThread = new std::thread([] { _scene->Init(); });
        }
        else {
            if(!_scene->Init()) {
                _scene.reset();   // 解放
            }
        }
    }

    //----------------------------------------------------------
    // 更新実行
    //----------------------------------------------------------
    // loading処理
    if constexpr(USE_LOADING_THREAD) {
        if(SystemSettingsMgr()->IsLoading()) {
            _loadingScene->Update(deltaTime);
        }
        else if(_scene) {
            if(_loadingThread && _loadingThread->joinable()) {
                // thread終わったかどうかを確保する
                _loadingThread->join();
            }
            _scene->Update(deltaTime);
        }
    }
    // loadingを終わったらシーンの処理
    else {
        _scene->Update(deltaTime);
    }

    //====================
    // ホットキー
    //====================
    // ヒント
    if(input::IsKeyPress(DirectX::Keyboard::F5)) {
        SystemSettingsMgr()->SwapShowHintOverlay();
    }
    // ゲーム
    if(input::IsKeyPress(DirectX::Keyboard::F6)) {
        SystemSettingsMgr()->SetSystemState(sys::SystemState::Play);
        SetNextScene(scene::EScene::Title);
    }
    // PBR
    if(input::IsKeyPress(DirectX::Keyboard::F7)) {
        SystemSettingsMgr()->SetSystemState(sys::SystemState::Play);
        SetNextScene(scene::EScene::PBRLighting);
    }
    // アニメーション
    if(input::IsKeyPress(DirectX::Keyboard::F8)) {
        SystemSettingsMgr()->SetSystemState(sys::SystemState::Play);
        SetNextScene(scene::EScene::AnimationEditor);
    }
}

//--------------------------------------------------------------------------
//! 描画
//! @attention ここでは移動更新は行わないこと
//--------------------------------------------------------------------------
void onRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    if constexpr(USE_LOADING_THREAD) {
        if(SystemSettingsMgr()->IsLoading()) {
            _loadingScene->Render(colorTexture, depthTexture);
            if(SystemSettingsMgr()->IsEditorMode()) {
                _loadingScene->RenderImgui(colorTexture);
            }
        }
        else if(_scene) {
            _scene->Render(colorTexture, depthTexture);
            if(SystemSettingsMgr()->IsEditorMode()) {
                _scene->RenderImgui(colorTexture);
            }
        }
    }
    else {
        if(_scene) {
            _scene->Render(colorTexture, depthTexture);
            if(SystemSettingsMgr()->IsEditorMode()) {
                _scene->RenderImgui(colorTexture);
            }
        }
    }

    if(SystemSettingsMgr()->IsShowHintOverlay()) {
        imgui::ImGuiHintOverlay();
    }
}

//--------------------------------------------------------------------------
//! 解放
//--------------------------------------------------------------------------
void onFinalize()
{
    if constexpr(USE_LOADING_THREAD) {
        if(_loadingThread && _loadingThread->joinable()) {
            _loadingThread->join();
        }
        _loadingScene->Finalize();
        _loadingScene.reset();
    }

    _scene->Finalize();
    _scene.reset();
    _sceneNext.reset();
}
//--------------------------------------------------------------------------
//!　次のシーンを設定
//--------------------------------------------------------------------------
void SetNextScene(scene::EScene scene)
{
    if constexpr(USE_LOADING_THREAD) {
        ChangeScene(scene);
    }
    else {
        _sceneNext = scene::createNewScene<scene::LoadingScene>();
        _sceneNext->SetNextScene(scene);
    }
}
void SetNextSceneWithFade(scene::EScene sceneName)
{
    _scene->ChangeScene(sceneName);
}
//--------------------------------------------------------------------------
//!　シーンを切り替える
//--------------------------------------------------------------------------
void ChangeScene(scene::EScene nextScene)
{
    switch(nextScene) {
        case scene::EScene::Title: _sceneNext = scene::createNewScene<scene::TitleScene>(); break;
        case scene::EScene::Tutorial: _sceneNext = scene::createNewScene<scene::TutorialScene>(); break;
        case scene::EScene::Game: _sceneNext = scene::createNewScene<scene::GameScene>(); break;
        case scene::EScene::Clear: _sceneNext = scene::createNewScene<scene::GameClearScene>(); break;
        case scene::EScene::Over: _sceneNext = scene::createNewScene<scene::GameOverScene>(); break;

        case scene::EScene::AnimationEditor: _sceneNext = scene::createNewScene<scene::AnimationConfigScene>(); break;

        case scene::EScene::PBRLighting: _sceneNext = scene::createNewScene<scene::TestScene>(); break;
    }
}
}   // namespace manager
