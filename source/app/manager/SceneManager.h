﻿//---------------------------------------------------------------------------
//!	@file	SceneManager.h
//!	@brief	シーンマネージャー
//---------------------------------------------------------------------------
#include "scene/BaseScene.h"

namespace manager {

//===========================================================================
//!	@defgroup	フレームワークからのコールバック
//===========================================================================
//@{

//! 初期化
bool onInitialize(u32 width, u32 height);

//! 更新
//! @param  [in]    deltaTime   進行時間(単位:秒)
void onUpdate(f32 deltaTime);

//! 描画
void onRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture);

//! 解放
void onFinalize();

//! 次の更新でシーンを切り替える
//! @note ローディング画面 あり
//! @note フェード なし
void SetNextScene(scene::EScene sceneName);

//! 次の更新でシーンを切り替える
//! @note ローディング画面 あり
//! @note フェード あり
void SetNextSceneWithFade(scene::EScene sceneName);

//! シーンを切り替える
void ChangeScene(scene::EScene nextScene);
//@}
}   // namespace manager
