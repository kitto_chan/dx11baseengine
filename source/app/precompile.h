﻿//---------------------------------------------------------------------------
//!	@file	precompile.h
//!	@brief	プリコンパイルヘッダー
//---------------------------------------------------------------------------
#pragma once

#include "utility/GameDef.h"   //!< ゲーム用の定義

#include "framework.h"
#include "network.h"

//! DirectX11実装をgpuとして名前をエイリアス
namespace gpu = dx11;

#include "GameSettings.h"
#include "utility/ImGuiGameView.h"

#include "manager/GameManager.h"   //!< ゲームマネージャー
#include "manager/EntityManager.h"
#include "manager/EffekseerManager.h"

namespace entity {
namespace go {
namespace ui {
}
}
}
namespace gameobject = entity::go;
namespace ui = entity::go::ui;
