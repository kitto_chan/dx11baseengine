﻿//---------------------------------------------------------------------------
//!	@file	BaseScene.cpp
//!	@brief	ゲームマネージャー
//---------------------------------------------------------------------------
#include "BaseScene.h"
#include "manager/EntityManager.h"
#include "manager/CameraManager.h"
#include "manager/SceneManager.h"
#include "manager/LightingManager.h"
#include "manager/AudioManager.h"

#include "renderer/ShadowMap.h"

#include "entity/Entity.h"
#include "entity/gameObject/camera/FirstPersonCamera.h"
#include "entity/gameObject/lighting/DirectionalLight.h"

#include "effect/Common/CommonEffect.h"
#include "effect/Common/FogEffect.h"

#include "component/Transform.h"

#include "utility/SettingPanel.h"
namespace scene {
namespace {
//! デプスバッファの情報を入れる用テクスチャ
std::unique_ptr<ShadowMap> shadowmap;
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
BaseScene::BaseScene(bool withoutEntityManager)
{
    if(!withoutEntityManager) {
        _pEntityMgr   = std::make_unique<manager::EntityManager>();
        _pCameraMgr   = std::make_unique<manager::CameraManager>();
        _pLightingMgr = std::make_unique<manager::LightingManager>();
    }
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
BaseScene::~BaseScene()
{
    //Finalize();
    _pEntityMgr.reset();
    //_pCameraMgr.reset();
    // 文字クリア
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool BaseScene::Init()
{
    //-----------------------
    // フォグ設定
    //-----------------------
    _fogEffect = std::make_unique<effect::FogEffect>();

    //-----------------------
    // シャドウ設定
    //-----------------------
    if(!shadowmap) {
        shadowmap = std::make_unique<ShadowMap>(4096, DXGI_FORMAT_D32_FLOAT);
    }

    //-----------------------
    // とりあえず一回リセット
    //-----------------------
    gpu::setInputLayout(nullptr);
    //MouseMgr()->SetRelativeMode();

    SystemSettingsMgr()->SetLoading(true);

    // カメラの初期化
    _pCameraMgr->Init();

    // エンティティ　初期化
    if(!_pEntityMgr->Init()) {
        return false;
    };

    if(_pLightingMgr)
        _pLightingMgr->Init();
    {
        gameobject::DirectionalLight* defaultLight = new gameobject::DirectionalLight("DefaultLighting");
        _pLightingMgr->Regist(defaultLight);
        _pLightingMgr->SetDefaultLight(defaultLight);
    }

    // 各シーンの初期化
    OnInit();
    _pSettingPanel = std::make_unique<settings::SettingPanel>();
    _pSettingPanel->Init(_pEntityMgr);

    SystemSettingsMgr()->SetLoading(false);

    // 変数を渡すために、1フレームだけ先に更新
    _pEntityMgr->Update();

    // 霧設定
    _fogEffect->SetIsUseFog(false);
    _fogEffect->ApplyFogConstantBuffer();

    // Late Init
    OnAfterInit();

    _isInited = true;

    FadeIn();

    return true;
}

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void BaseScene::Update([[maybe_unused]] f32 deltaTime)
{
    FadeHandle();

    if(_fadeState == EFadeState::fadeOut) return;

    if(!_isInited) return;

    // 自由視角(Debug Mode)
    if(KeyboardMgr()->IsKeyPress(DirectX::Keyboard::F1)) {
        SystemSettingsMgr()->SwapMode();
        _pCameraMgr->SyncDebugCameraSceneCamera();
    }

    // ゲームステート
    if(SystemSettingsMgr()->IsPlayState()) {
        // ゲームに関してアップデートループ処理
        if(_isUpdate) {
            _pEntityMgr->Update();
            OnUpdate();
            _fogEffect->Update();
            manager::EffekseerMgr()->Update(_pCameraMgr->GetCurrentCamera()->GetProjMatrix(),
                                            _pCameraMgr->GetCurrentCamera()->GetViewMatrix());
        }

        // システムに関してアップデート
        if(input::IsKeyPress(DirectX::Keyboard::Escape) ||
           input::GamePadMgr()->IsViewPressed()) {
            if(_pSettingPanel->IsOpen()) {
                _pSettingPanel->CloseSettingPanel();
            }
            else {
                _pSettingPanel->OpenSettingPanel();
            }
        }
        _pSettingPanel->Update();
        _isUpdate = !_pSettingPanel->IsOpen();
        SystemSettingsMgr()->SetIsGameUpdate(_isUpdate);
        manager::EffekseerMgr()->SetPausedToAllEffects(!_isUpdate);
    }
    // デバッグモード
    // デバッグカメラ設定
    else {
        manager::EffekseerMgr()->SetCamera(_pCameraMgr->GetCurrentCamera()->GetProjMatrix(),
                                           _pCameraMgr->GetCurrentCamera()->GetViewMatrix());
    }

    // カメラ更新
    _pCameraMgr->Update();

    // ライティング更新処理
    if(_pLightingMgr)
        _pLightingMgr->Update();

    // ポストエフェクト処理(フェード)
    effect::CommonEffectInstance()->Update();
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void BaseScene::Render(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    if(!_isInited) return;

    _pCameraMgr->Render();

    // シーンの描画
    OnRender(colorTexture, depthTexture);

    // エンティティの影描画
    if(_isRenderShadow) {
        auto camera = manager::GetCurrentCameraMgr()->GetCurrentCamera();
        //auto player = FindEntityWithName("Player");

        // TODO マチルライティングのシャドウマップ
        auto light = manager::GetDefaultLight();

        if(camera && light) {
            gpu::setRasterizerState(dx11::commonStates().CullNone());

            shadowmap->Begin(camera->GetComponent<component::Transform>()->GetPosition(),
                             light->GetComponent<component::Transform>()->GetBackwardAxis());

            _pEntityMgr->RenderEntitiesShadow();

            shadowmap->End();
            gpu::setRenderTarget(0, colorTexture);
            gpu::setDepthStencil(depthTexture);
        }
    }
    // エンティティの描画
    {
        manager::SetCurrentCameraCB();
        _pEntityMgr->Render();
    }

    manager::EffekseerMgr()->Render();

    renderer::FontRendererIns()->Render();

    // ポストエフェクト描画（フェード）
    effect::CommonEffectInstance()->BeginPostEffect(colorTexture);
    effect::CommonEffectInstance()->EndPostEffect(colorTexture, depthTexture);

    // 設定パネル
    _pSettingPanel->Render(colorTexture, depthTexture);
}
//---------------------------------------------------------------------------
//! Imguiの描画
//---------------------------------------------------------------------------
void BaseScene::RenderImgui(raw_ptr<gpu::Texture> colorTexture)
{
    //----------------------
    // メニューバー描画
    ImGui::Begin("MainDockspace");
    if(ImGui::BeginMenuBar()) {
        if(ImGui::BeginMenu("Scene")) {
            // メインゲーム
            if(ImGui::MenuItem("Main Game")) {
                SystemSettingsMgr()->SetSystemState(sys::SystemState::Play);
                manager::SetNextScene(scene::EScene::Title);
            }
            // アニメーションエディタ
            if(ImGui::MenuItem("Animation Editor")) {
                SystemSettingsMgr()->SetSystemState(sys::SystemState::Play);
                manager::SetNextScene(scene::EScene::AnimationEditor);
            }
            // PBR Lighting
            if(ImGui::MenuItem("PBR Lighting Demo")) {
                SystemSettingsMgr()->SetSystemState(sys::SystemState::Play);
                manager::SetNextScene(scene::EScene::PBRLighting);
            }
            ImGui::EndMenu();
        }

        // シーンの設定
        if(ImGui::BeginMenu("SceneSettings")) {
            // 霧の設定
            if(ImGui::MenuItem("Fog Settings")) {
                _fogEffect->SetIsRenderImgui(true);
            }

            // 影の設定
            if(ImGui::MenuItem("Shadow Settings")) {
                shadowmap->SetIsRenderImgui(true);
            }
            ImGui::EndMenu();
        }

        ImGui::EndMenuBar();
    }
    ImGui::End();

    // フレームワークの太いMenuBar
    imgui::RenderMenuBar();

    // 霧設定
    _fogEffect->RenderImGui();

    //----------------------
    // Guizmonの描画
    if(_pEntityMgr->GetSelectedEntities()) {
        imgui::ImguiGuizmoView(_pCameraMgr->GetCurrentCamera()->GetViewMatrix(),
                               _pCameraMgr->GetCurrentCamera()->GetProjMatrix(),
                               _pEntityMgr->GetSelectedEntities()->GetComponent<component::Transform>());
    }

    // 子たちImGuiの描画
    OnRenderImgui();

    // エンティティのImGui描画
    _pEntityMgr->RenderImgui();
    _pLightingMgr->RenderImgui();

    // アクションバーの描画
    imgui::ImguiActionBar();

    effect::CommonEffectInstance()->RenderImGui();

    // シーンの描画
    if(SystemSettingsMgr()->IsEditorMode()) {
        ImGui::Begin("Scene");
        ImVec2                    contentSize = ImGui::GetContentRegionAvail();   // ウインドウズの"中身"のサイズ
        ID3D11ShaderResourceView* srv         = *colorTexture;                    // テクスチャへのレンダー（Render-To-Texture）

        SystemSettingsMgr()->SetSceneViewportSize({ contentSize.x, contentSize.y });   // ビューポートのサイズを設定

        ImGui::Image((void*)srv, contentSize);   // 今画面のテクスチャーImgui DockSpaceに設置
        ImGui::End();
    }
}

//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void BaseScene::Finalize()
{
    OnFinalize();

    _pEntityMgr->Finalize();
    _pSettingPanel->Finalize();
}
//---------------------------------------------------------------------------
//! entity managerを取得
//---------------------------------------------------------------------------
raw_ptr<manager::EntityManager> BaseScene::GetEntityMgr()
{
    return _pEntityMgr.get();
}
//---------------------------------------------------------------------------
//! シーン変わる処理
//---------------------------------------------------------------------------
void BaseScene::ChangeScene(EScene nextScene)
{
    _nextScene = nextScene;
    FadeOut();
}
//---------------------------------------------------------------------------
//! フェードイン開始
//---------------------------------------------------------------------------
void scene::BaseScene::FadeIn()
{
    _fadeTime  = 1.0f;
    _fadeState = EFadeState::fadeIn;
}
//---------------------------------------------------------------------------
//! フェードアウト開始
//---------------------------------------------------------------------------
void scene::BaseScene::FadeOut()
{
    _fadeTime  = 0.0f;
    _fadeState = EFadeState::fadeOut;
}
//---------------------------------------------------------------------------
//! フェード処理
//---------------------------------------------------------------------------
void scene::BaseScene::FadeHandle()
{
    switch(_fadeState) {
        case EFadeState::fadeIn:
            effect::CommonEffectInstance()->SetEffect(effect::Disslove);
            if(_fadeTime > 0.0f) {
                _fadeTime -= FADE_SPEED;
                if(_fadeTime < 0.0f) {
                    _fadeState = EFadeState::none;
                }
            }
            effect::CommonEffectInstance()->SetDissolve(_fadeTime);
            break;
        case EFadeState::fadeOut:
            effect::CommonEffectInstance()->SetEffect(effect::Disslove);
            if(_fadeTime < 1.0f) {
                _fadeTime += FADE_SPEED;
                if(_fadeTime > 1.0f) {
                    _fadeState = EFadeState::none;
                    manager::SetNextScene(_nextScene);
                }
            }
            effect::CommonEffectInstance()->SetDissolve(_fadeTime);
            break;
        case EFadeState::none:
            if(effect::CommonEffectInstance()->GetCurrentEffect() == effect::EPostEffect::Disslove) {
                effect::CommonEffectInstance()->SetEffect(effect::EPostEffect::None);
            }
            break;
    }
}
}   // namespace scene
