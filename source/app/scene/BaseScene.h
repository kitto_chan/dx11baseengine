﻿//---------------------------------------------------------------------------
//!	@file	BaseScene.h
//!	@brief	シーンの基底クラス
//---------------------------------------------------------------------------
#pragma once
#include "IScene.h"
#include "Entity/Entity.h"

namespace entity::go {
};
namespace gameobject = entity::go;

namespace manager {
class EntityManager;
class CameraManager;
class LightingManager;
}   // namespace manager

namespace effect {
class FogEffect;
}
namespace settings {
class SettingPanel;
}
namespace scene {
enum class EFadeState
{
    none,
    fadeOut,
    fadeIn
};
class BaseScene : IScene
{
    const f32 FADE_SPEED = 0.02f;

public:
    BaseScene(bool withoutEntityManager = false);
    virtual ~BaseScene();

    //---------------------------------------------------------------------------
    //! 関数
    //---------------------------------------------------------------------------

    //! 初期化
    virtual bool Init() override;

    //! 更新
    virtual void Update(f32 deltaTime) override;

    //! 描画
    virtual void Render(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture) override;

    //! Imgui描画
    virtual void RenderImgui(raw_ptr<gpu::Texture> colorTexture) override;

    //! 解放
    virtual void Finalize() override;

    bool IsInited() const { return _isInited; };

    raw_ptr<manager::EntityManager> GetEntityMgr();

    virtual void SetNextScene([[maybe_unused]] scene::EScene scene){};

    void ChangeScene(EScene nextScene);

protected:
    uni_ptr<manager::EntityManager>   _pEntityMgr;     //!< ゲームシーンのアクター（役者）たち
    uni_ptr<manager::CameraManager>   _pCameraMgr;     //!< カメラマネージャー
    uni_ptr<manager::LightingManager> _pLightingMgr;   //!< カメラマネージャー
    uni_ptr<settings::SettingPanel>   _pSettingPanel;

    bool _isInited = false;   //!< 初期化したか

    bool _isUpdate = true;   //!< 更新かどうか

    bool _isRenderShadow = true;   //!< シャドウを描画かどうか

    //! フェードイン/アウトの時間
    //! @note 0.0 > 1.0 = fadeIn
    //! @note 1.0 > 0.0 = fadeout
    f32 _fadeTime = 0.0f;

    EFadeState _fadeState = EFadeState::none;

    uni_ptr<effect::FogEffect> _fogEffect;

    //===========================================================================
    //!	@defgroup	継承用関数
    //===========================================================================
    //@{

    //!　初期化
    virtual bool OnInit() = 0;

    //! 初期化後でやること
    //! 例えば全体的エンティティ生成した後で初期の再設定とか(座標など)
    virtual void OnAfterInit(){};

    //! アップデート
    virtual void OnUpdate() = 0;

    //! 描画
    virtual void OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture) = 0;

    //! Imgui描画
    virtual void OnRenderImgui() = 0;

    //! 解放
    virtual void OnFinalize() = 0;

    //@}
private:
    void FadeIn();       //!< フェードイン開始
    void FadeOut();      //!< フェードアウト開始
    void FadeHandle();   //!< フェードイン/アウト処理

    EScene _nextScene = EScene::Title;
};
}   // namespace scene
