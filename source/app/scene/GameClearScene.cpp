﻿//---------------------------------------------------------------------------
//!	@file	TutorialScene.h
//!	@brief	ゲームチュートリアル
//---------------------------------------------------------------------------

#include "GameClearScene.h"
#include "manager/SceneManager.h"
#include "manager/CameraManager.h"

#include "entity/gameObject/GameObject.h"
#include "entity/gameObject/ui/Sprite.h"
#include "entity/gameObject/ui/Sprite2D.h"

#include "component/Transform.h"
#include "component/ModelRenderer.h"
#include "component/Animator.h"

#include "effect/Effect.h"
namespace scene {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
GameClearScene::GameClearScene()
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
GameClearScene::~GameClearScene()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool GameClearScene::OnInit()
{
    effect::BasicEffectIns()->InitAll();

    // 文字を読み込む
    _fontJiyucho = std::make_unique<DirectX::SpriteFont>(dx11::d3dDevice(), L"font/Jiyucho16.spritefont");

    _pCameraMgr->GetCurrentCamera()->GetComponent<component::Transform>()->SetPosition(float3(0.0f, 0.0f, 1.0f));

    static const cb::PhongMaterial characterMaterial{
        float4(1.0f, 1.0f, 1.0f, 1.0f),
        float4(1.0f, 1.0f, 1.0f, 1.0f),
        float3(0.2f, 0.2f, 0.2f),
        16,
    };

    // Player
    {
        const std::string path = "model/" + manager::GameMgr()->_playerModelName + "/Victory.fbx";

        const Animation::Desc desc[] = {
            // 名前,  アニメーションファイル名
            { "Victory", path },
        };
        const float3 initPos = float3(0.0f, -1.0f, -5.0f);

        _character = new gameobject::GameObject("Character", initPos);
        _character->AddComponent<component::ModelRenderer>(desc[0].path_.data())
            ->SetPhongMaterial(characterMaterial);
        _character->AddComponent<component::Animator>(desc, std::size(desc));
        _pEntityMgr->Regist(_character.get());
    }

    {
        ui::Sprite* spriteTitle = new ui::Sprite("Congrad", float3(sys::WINDOW_CENTER_X, 150, 1.0f));
        spriteTitle->SetTexture("ui/Congeratulations.png");
        _pEntityMgr->Regist(spriteTitle);
    }
    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void GameClearScene::OnUpdate()
{
    // スペースキーを押したら、ゲームシーンに切り替えます
    if(KeyboardMgr()->IsKeyPress(DirectX::Keyboard::R) || input::GamePadMgr()->IsBPressed()) {
        ChangeScene(scene::EScene::Title);
        return;
    }

    _fontScalePress = 0.4f + abs(sin(timer::TimerIns()->TotalTime())) / 3.0f;

    _backToTitleCounter += timer::DeltaTime();
    if(_backToTitleCounter >= 60.f) {
        ChangeScene(scene::EScene::Title);
    }
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void GameClearScene::OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    gpu::setRenderTarget(0, colorTexture);
    gpu::setDepthStencil(depthTexture);

    // カラーとデプスバッファをクリア
    gpu::clearColor(colorTexture, float4(0.0f, 0.0f, 0.0f, 1.0f));   // カラーバッファ
    gpu::clearDepth(depthTexture, 1.0f);                             // デプスバッファ

    gpu::setRasterizerState(gpu::commonStates().CullNone());

    //文字描画
    {
        renderer::BeginDrawString();

        wchar_t* hint = input::IsGamePadConnected() ? L"Press 'B' To Continue" :
                                                      L"Press 'R' To Continue";
        renderer::DrawString(float2(sys::WINDOW_CENTER_X, 700), hint, WHITE, _fontScalePress);

        //renderer::ChangeFont(_fontJiyucho.get());
        //renderer::DrawString(float2(sys::WINDOW_CENTER_X, 150), L"討伐成功 !!", WHITE, 3.5f);
        renderer::EndDrawString();
    }
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void GameClearScene::OnRenderImgui()
{
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void GameClearScene::OnFinalize()
{
}

//---------------------------------------------------------------------------
//! シーンの生成
//! @note テンプレートの特殊化で実装します
//---------------------------------------------------------------------------
template<>
std::unique_ptr<BaseScene> createNewScene<GameClearScene>()
{
    return std::make_unique<GameClearScene>();
}

}   // namespace scene
