﻿//---------------------------------------------------------------------------
//!	@file	GameClearScene.h
//!	@brief	ゲームチュートリアル
//---------------------------------------------------------------------------
#pragma once
#include "BaseScene.h"

namespace entity::go {
class GameObject;
}

namespace scene {
class GameClearScene final : public BaseScene
{
public:
    GameClearScene();    //! コンストラクター
    ~GameClearScene();   //! デストラクター
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
protected:
private:
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
    raw_ptr<gameobject::GameObject> _character;

    bool _isReadyToRetry = false;

    uni_ptr<DirectX::SpriteFont> _fontJiyucho;             //!< Jiyuchoフォント
    f32                          _fontScalePress = 1.0f;   //!< Pressの文字列スケール

    f32 _backToTitleCounter = 0.0f;
    //----------
    // 継承
    bool OnInit() override;                                                                           //!< 初期化
    void OnUpdate() override;                                                                         //!< 更新
    void OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture) override;   //!< 描画
    void OnRenderImgui() override;                                                                    //!< Imgui描画
    void OnFinalize() override;                                                                       //!< 解放
};
}   // namespace scene
