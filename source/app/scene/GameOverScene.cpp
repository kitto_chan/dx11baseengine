﻿//---------------------------------------------------------------------------
//!	@file	GameOverScene.h
//!	@brief	ゲームチュートリアル
//---------------------------------------------------------------------------

#include "GameOverScene.h"
#include "manager/SceneManager.h"
#include "manager/CameraManager.h"
#include "manager/LightingManager.h"

#include "entity/gameObject/GameObject.h"
#include "entity/gameObject/ui/Sprite.h"
#include "entity/gameObject/ui/Sprite2D.h"

#include "component/Transform.h"
#include "component/ModelRenderer.h"
#include "component/Animator.h"

#include "effect/Effect.h"

#include "effect/Common/FogEffect.h"
namespace scene {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
GameOverScene::GameOverScene()
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
GameOverScene::~GameOverScene()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool GameOverScene::OnInit()
{
    effect::BasicEffectIns()->InitAll();

    _pCameraMgr->GetCurrentCamera()->GetComponent<component::Transform>()->SetPosition(float3(0.0f, 0.0f, 1.0f));

    auto defaultLight = _pLightingMgr->GetDefaultLight();
    defaultLight->SetAmbient(float4(0.5f, 0.5f, 0.5f, 1.0f));
    defaultLight->SetDiffuse(float4(0.5f, 0.5f, 0.5f, 1.0f));
    defaultLight->SetSpecular(float4(0.5f, 0.5f, 0.5f, 1.0f));

    static const cb::PhongMaterial characterMaterial{
        float4(1.0f, 1.0f, 1.0f, 1.0f),
        float4(1.0f, 1.0f, 1.0f, 1.0f),
        float3(0.2f, 0.2f, 0.2f),
        16,
    };

    // Player
    {
        std::string path = "model/" + manager::GameMgr()->_playerModelName + "/Getting Up.fbx";

        const Animation::Desc desc[] = {
            // 名前,  アニメーションファイル名
            { "GetUp", path },
        };
        const float3 initPos = float3(0.0f, -1.0f, -5.0f);

        _character = new gameobject::GameObject("Character", initPos);
        _character->AddComponent<component::ModelRenderer>(desc[0].path_.data())
            ->SetPhongMaterial(characterMaterial);
        _character->AddComponent<component::Animator>(desc, std::size(desc))
            ->SetPlayType(Animation::PlayType::Once);
        _pEntityMgr->Regist(_character.get());
    }
    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void GameOverScene::OnUpdate()
{
    UpdateBackgroundAnimation();

    // 文字アニメション
    _fontScalePress = 0.4f + abs(sin(timer::TimerIns()->TotalTime())) / 3.0f;

    // スペースキーを押したら、ゲームシーンに切り替えます
    if(KeyboardMgr()->IsKeyPress(DirectX::Keyboard::R) || input::GamePadMgr()->IsBPressed()) {
        _character->GetComponent<component::Animator>()->ContinueAnimation();
        _isReadyToRetry = true;
    }

    _backToTitleCounter += timer::DeltaTime();
    if(_backToTitleCounter >= 60.f) {
        ChangeScene(scene::EScene::Title);
    }
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void GameOverScene::OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    gpu::setRenderTarget(0, colorTexture);
    gpu::setDepthStencil(depthTexture);

    // カラーとデプスバッファをクリア
    gpu::clearColor(colorTexture, float4(0.0f, 0.0f, 0.0f, 1.0f));   // カラーバッファ
    gpu::clearDepth(depthTexture, 1.0f);                             // デプスバッファ

    gpu::setRasterizerState(gpu::commonStates().CullNone());

    // 文字描画
    {
        renderer::BeginDrawString();
        renderer::DrawString(float2(sys::WINDOW_CENTER_X, 200), L"Game Over", RED, 3.0f);
        // タイトル、ヒント
        if(!_isReadyToRetry) {
            wchar_t* hint = input::IsGamePadConnected() ? L"Press 'B' To Try Again" :
                                                          L"Press 'R' To Continue";
            renderer::DrawString(float2(sys::WINDOW_CENTER_X, 700), hint, WHITE, _fontScalePress);
        }
        renderer::EndDrawString();
    }
}
//---------------------------------------------------------------------------
//! ImGui描画
//---------------------------------------------------------------------------
void GameOverScene::OnRenderImgui()
{
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void GameOverScene::OnFinalize()
{
}
//---------------------------------------------------------------------------
//! 背景のアニメション描画
//---------------------------------------------------------------------------
void GameOverScene::UpdateBackgroundAnimation()
{
    if(_isReadyToRetry) {
        if(_character->GetComponent<component::Animator>()->IsLastFrame(0.5f)) {
            ChangeScene(scene::EScene::Game);
        }
    }
    else {
        _character->GetComponent<component::Animator>()->PauseAnimation(0);
    }
}

//---------------------------------------------------------------------------
//! シーンの生成
//! @note テンプレートの特殊化で実装します
//---------------------------------------------------------------------------
template<>
std::unique_ptr<BaseScene> createNewScene<GameOverScene>()
{
    return std::make_unique<GameOverScene>();
}

}   // namespace scene
