﻿//---------------------------------------------------------------------------
//!	@file	GameOverScene.h
//!	@brief	ゲームチュートリアル
//---------------------------------------------------------------------------
#pragma once
#include "BaseScene.h"

namespace entity::go {
class GameObject;
}

namespace scene {
class GameOverScene final : public BaseScene
{
public:
    GameOverScene();    //! コンストラクター
    ~GameOverScene();   //! デストラクター
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
protected:
private:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    raw_ptr<gameobject::GameObject> _character;   //!< キャラクターの参照

    bool _isReadyToRetry = false;   //!< もう一回やるか

    f32 _fontScalePress = 1.0f;   //! Push文字のスケール

    f32 _backToTitleCounter = 0.0f;   //!< タイトルに戻る
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
    //----------
    // 継承
    bool OnInit() override;                                                                           //!< 初期化
    void OnUpdate() override;                                                                         //!< 更新
    void OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture) override;   //!< 描画
    void OnRenderImgui() override;                                                                    //!< Imgui描画
    void OnFinalize() override;                                                                       //!< 解放

    void UpdateBackgroundAnimation();
};
}   // namespace scene
