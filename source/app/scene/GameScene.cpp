﻿//---------------------------------------------------------------------------
//!	@file	GameScene.h
//!	@brief	ゲームタイトル画面
//---------------------------------------------------------------------------

#include "GameScene.h"
// マネージャー
#include "manager/SceneManager.h"
#include "manager/CameraManager.h"
#include "manager/EffekseerManager.h"
#include "manager/LightingManager.h"

// エンティティ
#include "entity/skybox/StaticSkyBox.h"
#include "entity/gameObject/Camera/ThirdPersonCamera.h"
#include "entity/gameObject/character/Character.h"
#include "entity/gameObject/character/Player.h"
#include "entity/gameObject/character/Enemy/Enemy.h"
#include "entity/gameObject/Cube.h"
#include "entity/gameObject/Weapon.h"
#include "entity/gameObject/ui/Sprite2D.h"
#include "entity/gameObject/ui/Sprite.h"

// コンポーネント
#include "component/Transform.h"
#include "component/ModelRenderer.h"
#include "component/Animator.h"
#include "component/action/PlayerAction.h"

#include "component/physics/Collider.h"
#include "component/physics/RigidBody.h"
#include "component/physics/GhostBody.h"
#include "component/physics/GhostBodyCharacter.h"

#include "component/action/MonsterBearAction.h"
#include "component/action/PlayerWeaponAction.h"
#include "component/action/MonsterBearWeaponAction.h"
#include "component/action/SandStormAction.h"

#include "component/info/CharacterInfo.h"
#include "component/info/PlayerInfo.h"
#include "component/info/MonsterBearInfo.h"

#include "component/effect/SwordTrail.h"

#include "effect/Common/CommonEffect.h"
#include "effect/SwordTrailEffect.h"
#include "effect/Common/FogEffect.h"

namespace scene {
namespace {
f32 param = 0.0f;
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
GameScene::GameScene()
{
    //_isUpdate = false;
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
GameScene::~GameScene()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool GameScene::OnInit()
{
    MouseMgr()->SetRelativeMode();
    // スカイボックス
    _pSkyBox = std::make_unique<entity::StaticSkyBox>();
    _pSkyBox->ReadFile("gameSource/skyBox/Dark.dds");
    _pSkyBox->Init();

    // ライトとマテリアル設定
    auto defaultLight = _pLightingMgr->GetDefaultLight();
    defaultLight->SetAmbient(float4(0.45f, 0.45f, 0.45f, 1.0f));
    defaultLight->SetDiffuse(float4(0.5f, 0.5f, 0.5f, 1.0f));
    defaultLight->SetSpecular(float4(0.5f, 0.5f, 0.5f, 1.0f));
    defaultLight->GetComponent<component::Transform>()->SetRotationX(math::D2R(315));

    static const cb::PhongMaterial characterMaterial{
        float4(1.0f, 1.0f, 1.0f, 1.0f),
        float4(1.0f, 1.0f, 1.0f, 1.0f),
        float3(0.2f, 0.2f, 0.2f),
        16,
    };
    static const cb::PhongMaterial groundMaterial{
        float4(1.0f, 1.0f, 1.0f, 1.0f),
        float4(1.0f, 1.0f, 1.0f, 1.0f),
        float3(0.3f, 0.3f, 0.3f),
        16,
    };

    //=========================================
    // プレイヤー
    //=========================================
    {
        const float3 initPos   = float3(+8.0f, 0.5f, 8.0f);
        const float3 initScale = float3(1.0f, 1.0f, 1.0f);

        _player = new gameobject::Player("Player", initPos, initScale);
        _player->SetLayer(Layer::Player);
        _player->SetTag(Tag::Player);
        _player->AddComponent<component::PlayerInfo>()->SetMaxHP(100);

        _player->AddComponent<component::ModelRenderer>(manager::GameMgr()->_playerAnim)
            ->SetPhongMaterial(characterMaterial);
        _player->AddComponent<component::Animator>(manager::GameMgr()->_playerAnim);
        _player->AddComponent<component::CapsuleZCollider>(0.3f, 1.0f, float3(0.0f, 0.8f, 0.0f));
        _player->AddComponent<component::GhostBodyCharacter>();
        _player->AddComponent<component::PlayerAction>();
        _pEntityMgr->Regist(_player);
    }

    //=========================================
    // カメラ
    //=========================================
    // 三人称のプレイヤーのカメラ
    {
        _tpCamera = new gameobject::ThirdPersonCamera("PlayerCamera");

        _tpCamera->SetTarget(_player);
        _pCameraMgr->AddCamera(_tpCamera, true);
        _pEntityMgr->Regist(_tpCamera);
    }
    // シーンアニメション用のカメラ
    {
        _sceneCamera = new gameobject::FirstPersonCamera("Scene Camera");
        _sceneCamera->SetIsControllable(false);
        _pCameraMgr->AddCamera(_sceneCamera, true);
        _pEntityMgr->Regist(_sceneCamera);
    }
    //=========================================
    // 敵
    //=========================================
    {
        const float3 initPos   = float3(-6.0f, -0.1f, -6.0f);
        const float3 initScale = float3(1.3f, 1.3f, 1.3f);

        _enemyBoss = new gameobject::Enemy("Enemy", initPos, initScale);
        _enemyBoss->SetLayer(Layer::Enemy);
        _enemyBoss->SetTag(Tag::Enemey);
        _enemyBoss->AddComponent<component::MonsterBearInfo>()->SetMaxHP(400);
        _enemyBoss->AddComponent<component::ModelRenderer>(ANIM_PATH "Monster.anim")
            ->SetPhongMaterial(characterMaterial);
        _enemyBoss->AddComponent<component::Animator>(ANIM_PATH "Monster.anim");
        _enemyBoss->AddComponent<component::CapsuleZCollider>(0.6f, 1.2f, float3(0.0f, +1.2f, 0.0f));
        _enemyBoss->AddComponent<component::GhostBodyCharacter>();
        _enemyBoss->AddComponent<component::MonsterBearAction>()->Disable();
        _pEntityMgr->Regist(_enemyBoss);
    }
    {
        gameobject::Weapon* pMonsterRightHand = new gameobject::Weapon("RightHand");
        pMonsterRightHand->SetBindBoneId(18);
        pMonsterRightHand->SetLayer(Layer::EnemyWeapon);
        pMonsterRightHand->AddComponent<component::BoxCollider>(float3(0.5f, 0.25f, 0.25f), float3(0.0f, 0.0f, 0.0f));
        pMonsterRightHand->AddComponent<component::GhostBody>();
        pMonsterRightHand->AddComponent<component::MonsterBearWeaponAction>()
            ->SetCollisionMode(component::MonsterBearWeaponAction::CollisionMode::RightHand);
        pMonsterRightHand->SetParent(_enemyBoss);
        _pEntityMgr->Regist(pMonsterRightHand);
    }
    {
        gameobject::Weapon* pMonsterLeftHand = new gameobject::Weapon("LeftHand");
        pMonsterLeftHand->SetBindBoneId(8);
        pMonsterLeftHand->SetLayer(Layer::EnemyWeapon);
        pMonsterLeftHand->AddComponent<component::BoxCollider>(float3(0.5f, 0.25f, 0.25f), float3(0.0f, 0.0f, 0.0f));
        pMonsterLeftHand->AddComponent<component::GhostBody>();
        pMonsterLeftHand->AddComponent<component::MonsterBearWeaponAction>()
            ->SetCollisionMode(component::MonsterBearWeaponAction::CollisionMode::LeftHand);
        pMonsterLeftHand->SetParent(_enemyBoss);
        _pEntityMgr->Regist(pMonsterLeftHand);
    }
    //==========================
    // 環境設定
    //==========================
    // Prop
    {
        gameobject::GameObject* enviro = new gameobject::GameObject("Prop");
        enviro->SetLayer(Layer::Environment);
        enviro->AddComponent<component::ModelRenderer>("model/environment/jungles/mesh_arena_jungle_prop_01.FBX")
            ->LoadExternalDiffuseTexture("model/environment/jungles/tex_arena_01.png");
        enviro->AddComponent<component::MeshCollider>();
        enviro->AddComponent<component::RigidBody>(0.0f, false);
        _pEntityMgr->Regist(enviro);
    }

    // ground
    {
        gameobject::GameObject* enviro = new gameobject::GameObject("Ground");
        enviro->SetLayer(Layer::Floor);
        enviro->AddComponent<component::ModelRenderer>("model/environment/jungles/mesh_hatchery_jungle_ground_01.FBX", 2.0f)
            ->LoadExternalDiffuseTexture("model/environment/jungles/tex_jungle_01.png");
        enviro->GetComponent<component::ModelRenderer>()->SetPhongMaterial(groundMaterial);
        enviro->AddComponent<component::MeshCollider>();
        enviro->AddComponent<component::RigidBody>(0.0f, false);
        _pEntityMgr->Regist(enviro);
    }

    // wall
    {
        gameobject::GameObject* enviro = new gameobject::GameObject("Wall");
        enviro->SetLayer(Layer::Wall);
        enviro->AddComponent<component::ModelRenderer>("model/environment/jungles/mesh_arena_jungle_wall_01.FBX")
            ->LoadExternalDiffuseTexture("model/environment/jungles/tex_jungle_01.png");
        enviro->AddComponent<component::MeshCollider>();
        enviro->AddComponent<component::RigidBody>(0.0f, false);
        _pEntityMgr->Regist(enviro);
    }

    {
        _spriteBlurOverlay = new ui::Sprite("spriteGameStartBG", { sys::WINDOW_CENTER_X, sys::WINDOW_CENTER_Y, 0.f }, { 1.15f, 0.15f, 1.0f });
        _spriteBlurOverlay->SetTexture("ui/blur-overlay-png-3.png");
        _pEntityMgr->Regist(_spriteBlurOverlay);
    }

    {
        _spriteGameStart = new ui::Sprite("spriteGameStart", { sys::WINDOW_CENTER_X, sys::WINDOW_CENTER_Y, 0.f });
        _spriteGameStart->SetTexture("ui/GameStart.png");
        _pEntityMgr->Regist(_spriteGameStart);
    }

    // 武器
    {
        _sword = new gameobject::Weapon("Sword");
        _sword->SetBindBoneId(manager::GameMgr()->_playerWeaponBoneId);
        _sword->SetLayer(Layer::PlayerWeapon);
        _sword->AddComponent<component::ModelRenderer>("model/DogPBR/Sword2.fbx")->LoadExternalDiffuseTexture("model/DogPBR/Albedo.png");
        _sword->AddComponent<component::BoxCollider>(float3(0.05f, 0.05f, 0.8f), float3(0.0f, 0.0f, +0.6f));
        _sword->AddComponent<component::GhostBody>();
        _sword->AddComponent<component::PlayerWeaponAction>();
        _sword->AddComponent<component::SwordTrail>(8, 14)->SetTrailTexture("textures/Swoosh01.png");
        _sword->SetParent(_player);
        _pEntityMgr->Regist(_sword);
    }

    //-------------------------------
    // カメラバッファ更新
    _pCameraMgr->GetCurrentCamera()->SetCameraCB();
    return true;
}
//---------------------------------------------------------------------------
// 初期化(Init後で実行)
//---------------------------------------------------------------------------
void GameScene::OnAfterInit()
{
    SceneInitSetting();
    ChangeToBossAppearanceState();

    // 霧を設定
    _fogEffect->SetIsUseFog(true);
    _fogEffect->SetFogColor({ 1.0f, 1.0f, 1.0f });
    _fogEffect->SetSpeed({ 0.0013f, 0.0017f });
    _fogEffect->SetMinHeight(0.0f);
    _fogEffect->SetMaxHeight(3.0f);
    _fogEffect->SetTexel({ 0.0f, 0.5f });
    _fogEffect->ApplyFogConstantBuffer();
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void GameScene::OnUpdate()
{
    if(MouseMgr()->IsAbsoluteMode()) {
        MouseMgr()->SetRelativeMode();
    }

    switch(_currentStep) {
        case eSceneState::BossAppearance:
            UpdateBossAppearanceState();
            break;
        case eSceneState::GameStartHintBegin:
            UpdateGameStartHintBeginState();
            break;
        case eSceneState::GameStartHintHold:
            UpdateGameStartHintHoldState();
            break;
        case eSceneState::GameStartHintEnd:
            UpdateGameStartHintEndState();
            break;
    };

    // 念のために、タイトル戻るキーを用意します
    if(KeyboardMgr()->IsKeyPress(DirectX::Keyboard::O)) {
        ChangeScene(EScene::Title);
    }

    if(_player->GetComponent<component::Transform>()->GetPosition().y <= -5) {
        ChangeScene(EScene::Over);
    }
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void GameScene::OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    gpu::setRenderTarget(0, colorTexture);
    gpu::setDepthStencil(depthTexture);

    // カラーとデプスバッファをクリア
    gpu::clearColor(colorTexture, float4(0.0f, 0.0f, 0.0f, 1.0f));   // カラーバッファ
    gpu::clearDepth(depthTexture, 1.0f);                             // デプスバッファ

    gpu::setRasterizerState(gpu::commonStates().CullNone());

    _pSkyBox->Render();
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void GameScene::OnRenderImgui()
{
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void GameScene::OnFinalize()
{
    //_pSkyBox.reset();
}
//---------------------------------------------------------------------------
//! 初期設定
//---------------------------------------------------------------------------
void GameScene::SceneInitSetting()
{
    //-------------------------------
    // プレイヤーと敵お互い相手に向く
    //-------------------------------
    const auto playerTransform = _player->GetComponent<component::Transform>();
    const auto enemyTransform  = _enemyBoss->GetComponent<component::Transform>();

    enemyTransform->FaceAtWithoutY(playerTransform->GetPosition());
    playerTransform->FaceAtWithoutY(enemyTransform->GetPosition());

    // ボスに向く
    _tpCamera->GetComponent<component::Transform>()->LookAt(enemyTransform->GetPosition());
}
//---------------------------------------------------------------------------
//! ボス登場ステートへ移動する
//---------------------------------------------------------------------------
void GameScene::ChangeToBossAppearanceState()
{
    // 行動しないように
    _player->GetComponent<component::PlayerAction>()->Disable();
    _enemyBoss->GetComponent<component::MonsterBearAction>()->Disable();

    // UI表示しないように
    _player->SetDisplayHpUI(false);
    _enemyBoss->SetDisplayHpUI(false);

    // 最初は透明になる
    param = 0.0f;
    _spriteBlurOverlay->SetColor(float4(1.0f, 1.0f, 1.0f, param));
    _spriteGameStart->SetColor(float4(1.0f, 1.0f, 1.0f, param));

    // プレイヤーカメラからシーンカメラに切替る
    _tpCamera->Disable();
    _pCameraMgr->SetCurrentCamera(_sceneCamera);

    // ボス登場アニメション
    _enemyBoss->GetComponent<component::Animator>()->Play("Landing", Animation::PlayType::Loop);

    // シーン用カメラ設定
    _sceneCamera->GetComponent<component::Transform>()->SetPosition(-7.09f, 6.36f, -2.6f);
    _sceneCamera->GetComponent<component::Transform>()->SetRotation(-0.92f, -0.27f, 0.0f);
}
//---------------------------------------------------------------------------
//! ゲームスタートヒント表示ステートへ移動する
//---------------------------------------------------------------------------
void GameScene::ChangeToGameStartHintBeginState()
{
    // 第一人称カメラに戻る
    _pCameraMgr->SetCurrentCamera(_tpCamera);

    // ブラーエフェクト停止
    effect::CommonEffectInstance()->SetEffect(effect::None);

    // カウントリセット
    counter = 0;

    // 次のステップへ移動
    _currentStep = eSceneState::GameStartHintBegin;
}
//---------------------------------------------------------------------------
//! ゲームスタートヒント表示持続するステートへ移動する
//---------------------------------------------------------------------------
void GameScene::ChangeToGameStartHintHoldState()
{
    counter      = 0;
    _currentStep = eSceneState::GameStartHintHold;
}
//---------------------------------------------------------------------------
//! ゲームスタートヒント表示終わるステートへ移動する
//---------------------------------------------------------------------------
void GameScene::ChangeToGameStartHintEndState()
{
    counter      = 0;
    _currentStep = eSceneState::GameStartHintEnd;
}
//---------------------------------------------------------------------------
//! ボス登場ステート更新
//---------------------------------------------------------------------------
void GameScene::UpdateBossAppearanceState()
{
    constexpr u32 NEXT_STEP_COUNT = 160;
    // カウンター
    counter++;
    //-------------------------------
    // ボスアニメションの時の設定
    //-------------------------------
    if(_enemyBoss->GetComponent<component::Animator>()->IsAnimationLastFrame("Landing"), 0.1f) {
        _enemyBoss->GetComponent<component::Animator>()->Play("PowerUp", Animation::PlayType::Loop);
    }

    // Blure Effect
    if(effect::CommonEffectInstance()->GetCurrentEffect() == effect::EPostEffect::None) {
        effect::CommonEffectInstance()->SetEffect(effect::RoarBlur);
    }

    if(counter > NEXT_STEP_COUNT) {
        ChangeToGameStartHintBeginState();
    }
}
//---------------------------------------------------------------------------
//! ゲームスタートヒント表示ステート更新
//---------------------------------------------------------------------------
void GameScene::UpdateGameStartHintBeginState()
{
    counter++;
    constexpr u32 COUNT_INTERVAL = 3;
    if(counter > COUNT_INTERVAL) {
        param += 0.1f;
        counter = 0;
        if(param >= 2.0f) {
            ChangeToGameStartHintHoldState();
        }
    }

    _spriteBlurOverlay->SetColor(float4(1.0f, 1.0f, 1.0f, param));
    _spriteGameStart->SetColor(float4(1.0f, 1.0f, 1.0f, param));
}
//---------------------------------------------------------------------------
//! ゲームスタートヒント表示持続するステート更新
//---------------------------------------------------------------------------
void GameScene::UpdateGameStartHintHoldState()
{
    constexpr u32 NEXT_STEP_COUNT = 60;

    counter++;

    if(counter > NEXT_STEP_COUNT) {
        ChangeToGameStartHintEndState();
    }
}
//---------------------------------------------------------------------------
//! ゲームスタートヒント表示終わるステート更新
//---------------------------------------------------------------------------
void GameScene::UpdateGameStartHintEndState()
{
    counter++;
    if(counter > 1) {
        param -= 0.1f;
        counter = 0;

        if(param <= 0.0f) {
            param = 0.0f;
            // Game Start UI非表示
            _spriteBlurOverlay->Disable();
            _spriteGameStart->Disable();

            // UI表示
            _player->SetDisplayHpUI(true);
            _enemyBoss->SetDisplayHpUI(true);

            // キャラが行動できるように
            _player->GetComponent<component::PlayerAction>()->Enable();
            _enemyBoss->GetComponent<component::MonsterBearAction>()->Enable();

            // プレイヤーのカメラON
            _tpCamera->Enable();

            // 終了
            _currentStep = eSceneState::None;
        }
    }

    _spriteBlurOverlay->SetColor(float4(1.0f, 1.0f, 1.0f, param));
    _spriteGameStart->SetColor(float4(1.0f, 1.0f, 1.0f, param));
}
//---------------------------------------------------------------------------
//! シーンの生成
//! @note テンプレートの特殊化で実装します
//---------------------------------------------------------------------------
template<>
std::unique_ptr<BaseScene> createNewScene<GameScene>()
{
    return std::make_unique<GameScene>();
}

}   // namespace scene
