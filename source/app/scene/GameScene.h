﻿//---------------------------------------------------------------------------
//!	@file	GameScene.h
//!	@brief	ゲームシーン
//---------------------------------------------------------------------------
#pragma once
#include "BaseScene.h"

namespace entity {
class StaticSkyBox;

namespace go {
class Cube;
class Player;
class Weapon;
class Character;
class ThirdPersonCamera;
class FirstPersonCamera;
namespace ui {
class Sprite;
}
}   // namespace go
}   // namespace entity

namespace scene {
class GameScene final : public BaseScene
{
public:
    GameScene();    //! コンストラクター
    ~GameScene();   //! デストラクター
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
protected:
private:
    enum eSceneState
    {
        BossAppearance,       //!< ボス登場
        GameStartHintBegin,   //!< ゲームスタートヒントh表示
        GameStartHintHold,    //!< ゲームスタートヒント持続表示
        GameStartHintEnd,     //!< ゲームスタートヒント表示が終わる
        None
    };
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    gameobject::Player*            _player;        //!< プレイヤー
    gameobject::Character*         _enemyBoss;     //!< 敵
    gameobject::Weapon*            _sword;         //!< プレイヤーの剣
    gameobject::ThirdPersonCamera* _tpCamera;      //!< 三人称カメラ
    gameobject::FirstPersonCamera* _sceneCamera;   //!< シーンの演出用のカメラ

    ui::Sprite* _spriteBlurOverlay;   //!< ゲームスタートフォントの背景
    ui::Sprite* _spriteGameStart;     //!  ゲームスタートフォントの文字画像

    uni_ptr<entity::StaticSkyBox> _pSkyBox;   //!< スカイボックス

    s32 counter = 0;   //!< カウンター

    eSceneState _currentStep = eSceneState::BossAppearance;   //!< 今のステート

    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
    //--------------------
    // シーンステート状態
    void SceneInitSetting();   //!< 初期設定

    void ChangeToBossAppearanceState();       //!< ボス登場ステートへ移動する
    void ChangeToGameStartHintBeginState();   //!< ゲームスタートヒント表示ステートへ移動する
    void ChangeToGameStartHintHoldState();    //!< ゲームスタートヒント表示持続するステートへ移動する
    void ChangeToGameStartHintEndState();     //!< ゲームスタートヒント表示終わるステートへ移動する

    void UpdateBossAppearanceState();       //!< ボス登場ステート更新
    void UpdateGameStartHintBeginState();   //!< ゲームスタートヒント表示ステート更新
    void UpdateGameStartHintHoldState();    //!< ゲームスタートヒント表示持続するステート更新
    void UpdateGameStartHintEndState();     //!< ゲームスタートヒント表示終わるステート更新

    //----------
    // 継承
    bool OnInit() override;                                                                           //!< 初期化
    void OnAfterInit() override;                                                                      //!< 初期化後
    void OnUpdate() override;                                                                         //!< 更新
    void OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture) override;   //!< 描画
    void OnRenderImgui() override;                                                                    //!< Imgui描画
    void OnFinalize() override;
    //!< 解放
};
}   // namespace scene
