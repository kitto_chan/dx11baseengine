﻿//---------------------------------------------------------------------------
//!	@file	IScene.h
//!	@brief	シーンのインターフェース
//---------------------------------------------------------------------------
#pragma once
//===========================================================================
//!	シーン基底
//===========================================================================
namespace scene {
class BaseScene;
class IScene
{
public:
    //! デストラクタ
    virtual ~IScene() = default;

    //! 初期化
    virtual bool Init() = 0;

    //! 更新
    //! @param  [in]    deltaTime   進行時間(単位:秒)
    virtual void Update(f32 deltaTime) = 0;

    //! 描画
    virtual void Render(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture) = 0;

    //! Imgui描画
    virtual void RenderImgui(raw_ptr<gpu::Texture> colorTexture) = 0;

    //! 解放
    virtual void Finalize() = 0;
};

//--------------------------------------------------------------
//! シーンの生成テンプレート
//! @note 生成関数をテンプレート化することで該当クラスの#includeが不要になります
//! @note 各シーンクラスのcppでcreateScene()の特殊化を記述します
//--------------------------------------------------------------
template<typename T>
std::unique_ptr<BaseScene> createNewScene()
{
    return nullptr;
}
}   // namespace scene
