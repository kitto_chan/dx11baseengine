﻿//---------------------------------------------------------------------------
//!	@file	ChangeScene.h
//!	@brief	ゲームタイトル画面
//---------------------------------------------------------------------------

#include "LoadingScene.h"
#include "manager/SceneManager.h"
#include "manager/CameraManager.h"
#include "manager/EntityManager.h"

#include "effect/Effect.h"
#include "effect/Common/CommonEffect.h"

#include "entity/gameObject/ui/Sprite2D.h"
#include "entity/gameObject/ui/Sprite.h"
#include "effect/Effect.h"

#include "entity/gameObject/Cube.h"
#include "entity/gameObject/GameObject.h"
#include "component/Transform.h"
namespace scene {
namespace {

}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
LoadingScene::LoadingScene()
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
LoadingScene::~LoadingScene()
{
}

//---------------------------------------------------------------------------
//! 次のシーンを設定
//---------------------------------------------------------------------------
void LoadingScene::SetNextScene(scene::EScene scene)
{
    _nextScene   = scene;
    _changeScene = true;
}

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool LoadingScene::OnInit()
{
    _fontMPlus = renderer::CreateSpriteFont(L"font/mFontBold24.spritefont");

    // エフェクトの初期化
    effect::BasicEffectIns()->InitAll();
    effect::BasicEffectIns()->SetFogState(false);

    // 開始座標
    _pCameraMgr->GetCurrentCamera()->GetComponent<component::Transform>()->SetPosition(float3(0.0f, 1.5f, -3.0f));

    // 背景
    {
        ui::Sprite2D* background = new ui::Sprite2D("Background", float3(0.0f, 0.0f, 1.0f));
        background->SetTexture("ui/loading/04.png");
        _pEntityMgr->Regist(background);
    }

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void LoadingScene::OnUpdate()
{
    _fadeState = EFadeState::none;
    _fadeTime  = 0.0f;
    effect::CommonEffectInstance()->SetDissolve(_fadeTime);

    if(_changeScene) {
        manager::ChangeScene(_nextScene);
    }
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void LoadingScene::OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    gpu::setRenderTarget(0, colorTexture);
    gpu::setDepthStencil(depthTexture);

    // カラーとデプスバッファをクリア
    gpu::clearColor(colorTexture, float4(0.0f, 0.0f, 0.0f, 1.0f));   // カラーバッファ
    gpu::clearDepth(depthTexture, 1.0f);                             // デプスバッファ

    // 表裏カリングOFF
    gpu::setRasterizerState(gpu::commonStates().CullNone());

    renderer::BeginDrawString();
    renderer::DrawString(float2(sys::WINDOW_CENTER_X * 1.6f, sys::WINDOW_CENTER_Y * 1.8f),
                         L"Now Loading . . .", WHITE, 1.0f);
    renderer::ChangeFont(_fontMPlus);
    renderer::DrawString(float2(250.0f, sys::WINDOW_CENTER_Y * 1.8f),
                         L"ヒント：走ると回避はスタミナを消費する。", WHITE, 0.7f);
    renderer::EndDrawString();
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void LoadingScene::OnRenderImgui()
{
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void LoadingScene::OnFinalize()
{
}

//---------------------------------------------------------------------------
//! シーンの生成
//! @note テンプレートの特殊化で実装します
//---------------------------------------------------------------------------
template<>
std::unique_ptr<BaseScene> createNewScene<LoadingScene>()
{
    return std::make_unique<LoadingScene>();
}

}   // namespace scene
