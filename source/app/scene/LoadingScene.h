﻿//---------------------------------------------------------------------------
//!	@file	LoadingScene.h
//!	@brief	ゲームタイトル画面
//---------------------------------------------------------------------------
#pragma once
#include "BaseScene.h"
namespace entity::go::ui {
class Sprite;
}

namespace scene {
class LoadingScene final : public BaseScene
{
public:
    LoadingScene();    //! コンストラクター
    ~LoadingScene();   //! デストラクター

    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    void SetNextScene(scene::EScene scene) override;

protected:
private:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    scene::EScene _nextScene = scene::EScene::Title;   // Default

    uni_ptr<DirectX::SpriteFont> _fontMPlus;   //!< Msというフォント

    bool _changeScene = false;
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------

    //----------
    // 継承
    bool OnInit() override;                                                                           //!< 初期化
    void OnUpdate() override;                                                                         //!< 更新
    void OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture) override;   //!< 描画
    void OnRenderImgui() override;                                                                    //!< Imgui描画
    void OnFinalize() override;                                                                       //!< 解放
};
}   // namespace scene
