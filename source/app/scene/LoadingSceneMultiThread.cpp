﻿//---------------------------------------------------------------------------
//!	@file	LoadingScene.h
//!	@brief	ゲームローディング画面（Multi Thread用）
//---------------------------------------------------------------------------

#include "LoadingSceneMultiThread.h"
#include "manager/SceneManager.h"
#include "manager/CameraManager.h"
#include "manager/EntityManager.h"

#include "effect/Effect.h"
#include "entity/gameObject/ui/Sprite2D.h"
#include "entity/gameObject/ui/Sprite.h"
#include "effect/Effect.h"

#include "entity/gameObject/Cube.h"
#include "entity/gameObject/GameObject.h"
#include "component/Transform.h"
namespace scene {
namespace {

}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
LoadingSceneMultiThread::LoadingSceneMultiThread()
{
    _pEntityMgr = std::make_unique<manager::EntityManager>(false);
    _pCameraMgr = std::make_unique<manager::CameraManager>(false);
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
LoadingSceneMultiThread::~LoadingSceneMultiThread()
{
}
void LoadingSceneMultiThread::Update([[maybe_unused]] f32 deltaTime)
{
    if(!_isInited) return;

    OnUpdate();
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool LoadingSceneMultiThread::OnInit()
{
    // エフェクトの初期化
    effect::BasicEffectIns()->InitAll();
    effect::BasicEffectIns()->SetFogState(false);

    f32 screenCenterX = gpu::swapChain()->backBuffer()->desc().width_ / 2.f;
    f32 screenCenterY = gpu::swapChain()->backBuffer()->desc().height_ / 2.f;

    float3 initPos    = float3(screenCenterX, screenCenterY, 0.0f);
    float3 initScale  = float3(0.3f, 0.3f, 0.3f);
    float3 initRotate = float3(0.0f, 0.0f, 0.0f);

    gameobject::GameObject* spinnerParents = new gameobject::GameObject("Spinner");

    for(u64 i = 0; i < _loadingSprites.size(); ++i) {
        initRotate.f32[2]  = math::D2R(i * 30.f);
        _loadingSprites[i] = new ui::Sprite("LoadingSpinner", initPos, initScale, initRotate);
        _loadingSprites[i]->SetTexture("ui/loading/spinner.png");
        _loadingSprites[i]->SetParent(spinnerParents);
        _pEntityMgr->Regist(_loadingSprites[i].get());
    }

    _pEntityMgr->Regist(std::move(spinnerParents));

    {
        raw_ptr<gameobject::Cube> cube = new gameobject::Cube("Temp");
        _pEntityMgr->Regist(cube.get());
    }

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void LoadingSceneMultiThread::OnUpdate()
{
    static s32 counter = 0;
    counter++;
    s32 currentSpinner = counter / 10 % _loadingSprites.size();

    for(u64 i = 0; i < _loadingSprites.size(); ++i) {
        f32 initPosX = gpu::swapChain()->backBuffer()->desc().width_ / 2.f * 1.85f;
        f32 initPosY = gpu::swapChain()->backBuffer()->desc().height_ / 2.f * 1.8f;
        _loadingSprites[i]->GetComponent<component::Transform>()->SetPosition(float3(initPosX, initPosY, 0.0f));
        if(i == currentSpinner) {
            _loadingSprites[i]->SetColor(float4(1.0f, 1.0f, 1.0f, 1.0f));
        }
        else {
            _loadingSprites[i]->SetColor(float4(1.0f, 1.0f, 1.0f, 0.5f));
        }
    }
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void LoadingSceneMultiThread::OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    gpu::setRenderTarget(0, colorTexture);
    gpu::setDepthStencil(depthTexture);

    // カラーとデプスバッファをクリア
    gpu::clearColor(colorTexture, float4(0.0f, 0.0f, 0.0f, 1.0f));   // カラーバッファ
    gpu::clearDepth(depthTexture, 1.0f);                             // デプスバッファ

    // 表裏カリングOFF
    gpu::setRasterizerState(gpu::commonStates().CullNone());

    //_fontRenderer->Begin();
    //_fontRenderer->DrawString(float2(sys::WINDOW_CENTER_X * 1.5f, sys::WINDOW_CENTER_Y * 1.8f),
    //                          L"Now Loading", WHITE, 1.0f);
    //_fontRenderer->End();
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void LoadingSceneMultiThread::OnRenderImgui()
{
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void LoadingSceneMultiThread::OnFinalize()
{
}

//---------------------------------------------------------------------------
//! シーンの生成
//! @note テンプレートの特殊化で実装します
//---------------------------------------------------------------------------
template<>
std::unique_ptr<BaseScene> createNewScene<LoadingSceneMultiThread>()
{
    return std::make_unique<LoadingSceneMultiThread>();
}

}   // namespace scene
