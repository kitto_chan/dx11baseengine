﻿//---------------------------------------------------------------------------
//!	@file	LoadingScene.h
//!	@brief	ゲームローディング画面（Multi Thread用）
//---------------------------------------------------------------------------
#pragma once
#include "BaseScene.h"
namespace entity::go::ui {
class Sprite;
}

namespace scene {
class LoadingSceneMultiThread final : public BaseScene
{
public:
    LoadingSceneMultiThread();    //! コンストラクター
    ~LoadingSceneMultiThread();   //! デストラクター

    void Update(f32 deltaTime) override; //!< 更新
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
protected:
private:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    std::array<raw_ptr<ui::Sprite>, 12> _loadingSprites;   //!< グルグルローディング画像

    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------

    //----------
    // 継承
    bool OnInit() override;                                                                           //!< 初期化
    void OnUpdate() override;                                                                         //!< 更新
    void OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture) override;   //!< 描画
    void OnRenderImgui() override;                                                                    //!< Imgui描画
    void OnFinalize() override;                                                                       //!< 解放
};
}   // namespace scene
