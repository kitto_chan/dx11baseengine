﻿//---------------------------------------------------------------------------
//!	@file	NetworkGameScene.h
//!	@brief	多人協力シーン
//---------------------------------------------------------------------------
#include "NetworkGameScene.h"
#include "entity/skybox/StaticSkyBox.h"
#include "Game/Client/GameClient.h"

namespace scene {
namespace {
uni_ptr<entity::StaticSkyBox> _pSkyBox;   //!< スカイボックス
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
NetworkGameScene::NetworkGameScene()
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
NetworkGameScene::~NetworkGameScene()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool NetworkGameScene::OnInit()
{
    // スカイボックス
    _pSkyBox = std::make_unique<entity::StaticSkyBox>();
    _pSkyBox->ReadFile("gameSource/skyBox/Sky.png");
    _pSkyBox->Init();

    _client = std::make_unique<GameClient>();

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void NetworkGameScene::OnUpdate()
{
    if(_client->IsStateConnect()) {
        _client->UpdatePollFD();
    }
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void NetworkGameScene::OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    gpu::setRenderTarget(0, colorTexture);
    gpu::setDepthStencil(depthTexture);

    // カラーとデプスバッファをクリア
    gpu::clearColor(colorTexture, float4(0.0f, 0.0f, 0.0f, 1.0f));   // カラーバッファ
    gpu::clearDepth(depthTexture, 1.0f);                             // デプスバッファ

    _pSkyBox->Render();
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void NetworkGameScene::OnRenderImgui()
{
    ImGui::Begin("Network");
    if(ImGui::Button("Client")) {
        _client->Connect();
    }

    ImGui::End();
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void NetworkGameScene::OnFinalize()
{
    //_panel->Finalize();
}

//---------------------------------------------------------------------------
//! シーンの生成
//! @note テンプレートの特殊化で実装します
//---------------------------------------------------------------------------
template<>
std::unique_ptr<BaseScene> createNewScene<NetworkGameScene>()
{
    return std::make_unique<NetworkGameScene>();
}
}   // namespace scene
