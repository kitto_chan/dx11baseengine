﻿//---------------------------------------------------------------------------
//!	@file	NetworkGameScene.h
//!	@brief	多人協力シーン
//---------------------------------------------------------------------------
#pragma once
#include "BaseScene.h"
class GameClient;
namespace entity::go {
}   // namespace entity::go

namespace scene {
class NetworkGameScene final : public BaseScene
{
public:
    NetworkGameScene();    //! コンストラクター
    ~NetworkGameScene();   //! デストラクター
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
protected:
private:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    uni_ptr<GameClient> _client;

    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
    //----------
    // 継承
    bool OnInit() override;                                                                           //!< 初期化
    void OnUpdate() override;                                                                         //!< 更新
    void OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture) override;   //!< 描画
    void OnRenderImgui() override;                                                                    //!< Imgui描画
    void OnFinalize() override;                                                                       //!< 解放
};
}   // namespace scene
