﻿//---------------------------------------------------------------------------
//!	@file	TitleScene.h
//!	@brief	ゲームタイトル画面
//---------------------------------------------------------------------------

#include "TestScene.h"

#include "entity/skybox/StaticSkyBox.h"

#include "entity/gameObject/Shpere.h"
#include "manager/CameraManager.h"
#include "manager/LightingManager.h"
#include "component/physics/RigidBody.h"
#include "component/physics/Collider.h"
#include "component/ModelRenderer.h"
#include "entity/gameObject/billBoardObject/FireBillBoard.h"
#include "entity/gameObject/Plane.h"
#include "entity/gameObject/billBoardObject/FireBillBoard.h"
#include "entity/gameObject/ui/AdvancedSprite.h"
#include "entity/gameObject/ui/Sprite.h"
#include "utility/SettingPanel.h"
namespace scene {
namespace {
cb::WorldCB _worldCB;
cb::WorldCB _invWorldCB;

shr_ptr<gpu::Buffer> _pVertexBuffer;   //!< 頂点バッファ
shr_ptr<gpu::Buffer> _pIndexBuffer;    //!< インデックスバッファ
u32                  _indexCount;      //!< インデックス数

shr_ptr<gpu::InputLayout>    _pIntputLayout3D;   //!< 3D入力レイアウト
std::shared_ptr<gpu::Shader> _pVS3D;             //!< 3D頂点シェーダー
std::shared_ptr<gpu::Shader> _pPS3D;             //!< 3Dピクセルシェーダー

shr_ptr<gpu::Texture> _pTexture;   //!< SRV

std::vector<s32>                       indices;
std::vector<vertex::VertexPosNormalUv> vertices;
uni_ptr<entity::StaticSkyBox>          _pSkyBox;   //!< スカイボックス
//uni_ptr<settings::SettingPanel>        _panel;

//ui::AdvancedSprite* s;
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
TestScene::TestScene()
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
TestScene::~TestScene()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool TestScene::OnInit()
{
    if(SystemSettingsMgr()->IsPlayState()) {
        SystemSettingsMgr()->SwapSysState();
    }

    MouseMgr()->SetAbsoluteMode();
    _pCameraMgr->GetCurrentCamera()->GetComponent<component::Transform>()->SetPosition(float3(0.0f, 0.0f, 3.0f));

    // スカイボックス
    _pSkyBox = std::make_unique<entity::StaticSkyBox>();
    _pSkyBox->ReadFile("gameSource/skyBox/Sky.png");
    _pSkyBox->Init();

    gameobject::GameObject* gun = new gameobject::GameObject();
    gun->GetComponent<component::Transform>()->SetRotationY(math::D2R(90.f));
    gun->AddComponent<component::ModelRenderer>("model/Cerberus_by_Andrew_Maximov/Cerberus_LP.FBX");
    gun->GetComponent<component::ModelRenderer>()->SetPBRTexture(Model::PBRTextureType::Normal, "model/Cerberus_by_Andrew_Maximov/Textures/Cerberus_N.tga");
    gun->GetComponent<component::ModelRenderer>()->SetPBRTexture(Model::PBRTextureType::Metalness, "model/Cerberus_by_Andrew_Maximov/Textures/Cerberus_M.tga");
    gun->GetComponent<component::ModelRenderer>()->SetPBRTexture(Model::PBRTextureType::Roughness, "model/Cerberus_by_Andrew_Maximov/Textures/Cerberus_R.tga");
    _pEntityMgr->Regist(gun);

    //s = new ui::AdvancedSprite();
    //s->SetTexture("textures/terrains/grass_03.png");
    //s->GetComponent<component::Transform>()->SetPosition(float3(720.f, 405.f, 0.0f));
    ////s->GetComponent<component::Transform>()->SetScale(float3(0.8f, 0.8f, 0.0f));
    ////plane->SetNormalMap("textures/terrains/grass_03_normal.png");
    //_pEntityMgr->Regist(s);

    geometry::MeshData meshdata = geometry::CreateSphere(1.0f);

    vertices.resize(meshdata.vertexData.size());
    indices.resize(meshdata.indexData.size());

    for(u64 i = 0; i < meshdata.vertexData.size(); i++) {
        vertices[i] = { meshdata.vertexData[i].pos,
                        meshdata.vertexData[i].nor,
                        meshdata.vertexData[i].tex };
    };

    for(u64 i = 0; i < meshdata.indexData.size(); i++) {
        indices[i] = meshdata.indexData[i];
    };

    _indexCount = (u32)meshdata.indexData.size();

    _pVertexBuffer = gpu::createBuffer({ vertices.size() * sizeof(vertex::VertexPosNormalUv), D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_IMMUTABLE }, vertices.data());
    _pIndexBuffer  = gpu::createBuffer({ indices.size() * sizeof(u32), D3D11_BIND_INDEX_BUFFER, D3D11_USAGE_IMMUTABLE }, indices.data());

    _pVS3D           = gpu::createShader("gameSource/Base3d_VS.fx", "VS", "vs_5_0");                                                        // 3D頂点シェーダー
    _pIntputLayout3D = gpu::createInputLayout(vertex::VertexPosNormalUv::inputLayout, std::size(vertex::VertexPosNormalUv::inputLayout));   // 3D
    _pPS3D           = dx11::createShader("gameSource/MatCap_PS.fx", "PS", "ps_5_0");                                                       // ピクセルシェーダー

    _pTexture            = gpu::createTextureFromFile("textures/MatCap02.jpg");
    _worldCB.matWorld    = math::identity();
    _invWorldCB.matWorld = inverse(_worldCB.matWorld);

    //_panel = std::make_unique<settings::SettingPanel>();
    //   _panel->Init(_pEntityMgr);
    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void TestScene::OnUpdate()
{
    //_panel->Update();
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void TestScene::OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    gpu::setRenderTarget(0, colorTexture);
    gpu::setDepthStencil(depthTexture);

    // カラーとデプスバッファをクリア
    gpu::clearColor(colorTexture, float4(0.0f, 0.0f, 0.0f, 1.0f));   // カラーバッファ
    gpu::clearDepth(depthTexture, 1.0f);                             // デプスバッファ

    gpu::setRasterizerState(gpu::commonStates().CullNone());

    // 頂点データー
    gpu::setVertexBuffer(0, _pVertexBuffer, sizeof(vertex::VertexPosNormalUv));   // 頂点バッファ
    gpu::setIndexBuffer(_pIndexBuffer);                                           // インデックスバッファ

    gpu::setInputLayout(_pIntputLayout3D);
    gpu::vs::setShader(_pVS3D);
    gpu::ps::setShader(_pPS3D);
    gpu::ps::setSamplerState(0, renderer::RenderStatesIns()->_pSSLinearWrap);
    gpu::setRasterizerState(renderer::RenderStatesIns()->_pRSNoCull);
    gpu::setBlendState(nullptr);

    gpu::setConstantBuffer("WorldCB", _worldCB);
    gpu::setConstantBuffer("InvWorldCB", _invWorldCB);

    gpu::ps::setTexture(0, _pTexture);
    _pSkyBox->Render();
    //_panel->Render(colorTexture, depthTexture);
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void TestScene::OnRenderImgui()
{
    //_panel->RenderImgui();
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void TestScene::OnFinalize()
{
    //_panel->Finalize();
}

//---------------------------------------------------------------------------
//! シーンの生成
//! @note テンプレートの特殊化で実装します
//---------------------------------------------------------------------------
template<>
std::unique_ptr<BaseScene> createNewScene<TestScene>()
{
    return std::make_unique<TestScene>();
}
}   // namespace scene
