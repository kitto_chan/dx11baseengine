﻿//---------------------------------------------------------------------------
//!	@file	TestScene.h
//!	@brief	開発用のテスト
//! @note   TODO:ビルドから除外
//---------------------------------------------------------------------------
#pragma once
#include "BaseScene.h"
namespace entity::go {
class Cube;
class Player;
class Weapon;
class Character;
class ThirdPersonCamera;
}

namespace scene {
class TestScene final : public BaseScene
{
public:
    TestScene();     //! コンストラクター
    ~TestScene();   //! デストラクター
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
protected:
private:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
    //----------
    // 継承
    bool OnInit() override;                                                                           //!< 初期化
    void OnUpdate() override;                                                                         //!< 更新
    void OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture) override;   //!< 描画
    void OnRenderImgui() override;                                                                    //!< Imgui描画
    void OnFinalize() override;                                                                       //!< 解放
};
}   // namespace scene
