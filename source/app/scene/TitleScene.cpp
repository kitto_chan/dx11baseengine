﻿//---------------------------------------------------------------------------
//!	@file	TitleScene.h
//!	@brief	ゲームタイトル画面
//---------------------------------------------------------------------------

#include "TitleScene.h"
#include "manager/SceneManager.h"
#include "manager/CameraManager.h"
#include "manager/LightingManager.h"

#include "entity/gameObject/Cube.h"
#include "entity/gameObject/ui/Sprite.h"
#include "entity/gameObject/ui/AdvancedSprite.h"
#include "entity/gameObject/billBoardObject/FireBillBoard.h"
#include "entity/gameObject/lighting/PointLight.h"
#include "entity/gameObject/Plane.h"

#include "component/Transform.h"
#include "component/ModelRenderer.h"
#include "component/Animator.h"

#include "effect/Effect.h"
#include "utility/SettingPanel.h"

namespace scene {
namespace {
enum ESelection
{
    Start,
    Menu,
    Quit
};
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
TitleScene::TitleScene()
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
TitleScene::~TitleScene()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool TitleScene::OnInit()
{
    MouseMgr()->SetRelativeMode();
    // 開始座標
    _pCameraMgr->GetCurrentCamera()->GetComponent<component::Transform>()->SetPosition(float3(0.0f, 1.5f, 5.0f));
    auto defaultLight = _pLightingMgr->GetDefaultLight();
    defaultLight->SetAmbient(float4(0.0f, 0.0f, 0.0f, 1.0f));
    defaultLight->SetDiffuse(float4(0.0f, 0.0f, 0.0f, 1.0f));
    defaultLight->SetSpecular(float4(0.0f, 0.0f, 0.0f, 1.0f));

    // フォントを読み込む
    _fontMPlus  = renderer::CreateSpriteFont(L"font/mFontBold24.spritefont");
    _fontMsFont = renderer::CreateSpriteFont(L"font/Ms36B.spritefont");

    {
        _spriteTitle = new ui::AdvancedSprite("TitleFont", float3(sys::WINDOW_CENTER_X, 150, 1.0f));
        _spriteTitle->SetTexture("ui/TitleCroped.png");
        _spriteTitle->SetCurrentEffect(ui::AdvancedSprite::eEffect::Blur);
        _pEntityMgr->Regist(_spriteTitle.get());
    }

    {
        _gamePadIcon = new ui::Sprite("GamePadIcon", float3(50.f, 780.0f, 0.0f));
        _gamePadIcon->SetTexture("ui/control/gamepad/fill/controller_xbox.png");
        _pEntityMgr->Regist(_gamePadIcon.get());
    }

    {
        shr_ptr<gameobject::Plane> plane = std::make_shared<gameobject::Plane>("Floor");
        plane->SetTexture("textures/terrains/grass_03.png");
        plane->SetNormalMap("textures/terrains/grass_03_normal.png");
        plane->GetComponent<component::Transform>()->SetPosition(float3(-5.f, -0.1f, -5.0f));
        plane->GetComponent<component::Transform>()->SetRotationX(math::D2R(90.f));
        plane->GetComponent<component::Transform>()->SetScale(float3(10.f, 10.f, 10.f));
        _pEntityMgr->Regist(plane);
    }

    static const cb::PhongMaterial characterMaterial{
        float4(1.0f, 1.0f, 1.0f, 1.0f),
        float4(1.0f, 1.0f, 1.0f, 1.0f),
        float3(0.2f, 0.2f, 0.2f),
        16,
    };

    // Player
    {
        constexpr Animation::Desc desc[] = {
            // 名前,  アニメーションファイル名
            { "SittingIdle", "model/archer/Sitting Idle.fbx" },
            { "Stand", "model/archer/Stand.fbx" }
        };
        const float3 initPos   = float3(1.2f, 0.0f, 0.0f);
        const float3 initScale = float3(1.0f, 1.0f, 1.0f);
        const float3 initRot   = float3(0.0f, math::D2R(-45.0f), 0.0f);
        _characherGirl         = std::make_shared<gameobject::GameObject>("Character", initPos, initScale, initRot);
        _characherGirl->AddComponent<component::ModelRenderer>(desc[0].path_.data())
            ->SetPhongMaterial(characterMaterial);
        _characherGirl->AddComponent<component::Animator>(desc, std::size(desc));
        _pEntityMgr->Regist(_characherGirl);
    }

    {
        constexpr Animation::Desc desc[] = {
            // 名前,  アニメーションファイル名
            { "SittingIdle", "model/vampire/Sitting Idle.fbx" },
            { "Stand", "model/vampire/Stand.fbx" }
        };
        const float3 initPos   = float3(-1.3f, 0.0f, 0.0f);
        const float3 initScale = float3(1.0f, 1.0f, 1.0f);
        const float3 initRot   = float3(0.0f, math::D2R(45.0f), 0.0f);

        _characherMen = std::make_shared<gameobject::GameObject>("Character", initPos, initScale, initRot);
        _characherMen->AddComponent<component::ModelRenderer>(desc[0].path_.data())
            ->SetPhongMaterial(characterMaterial);
        _characherMen->AddComponent<component::Animator>(desc, std::size(desc));
        _pEntityMgr->Regist(_characherMen);
    }

    {
        _arrowRight = new ui::Sprite("ArrowRight", float3(sys::WINDOW_CENTER_X + 135.0f, sys::WINDOW_CENTER_Y + 40.0f, 0.0f));
        _arrowRight->SetTexture("ui/ArrowsRight.png");
        _arrowRight->Disable();
        _pEntityMgr->Regist(_arrowRight.get());
    }

    {
        _arrowLeft = new ui::Sprite("ArrowLeft", float3(sys::WINDOW_CENTER_X - 135.0f, sys::WINDOW_CENTER_Y + 40.0f, 0.0f));
        _arrowLeft->GetComponent<component::Transform>()->SetRotationZ(math::D2R(180.0f));
        _arrowLeft->SetTexture("ui/ArrowsRight.png");
        _arrowLeft->Disable();
        _pEntityMgr->Regist(_arrowLeft.get());
    }

    {
        const float3                       initPos   = float3(0.0f, 0.75f, 1.0f);
        const float3                       initScale = float3(0.7f, 0.7f, 0.7f);
        shr_ptr<gameobject::FireBillBoard> fire      = std::make_shared<gameobject::FireBillBoard>("FireEffect", initPos, initScale);
        _pEntityMgr->Regist(fire);
    }

    {
        _fireLight = std::make_shared<gameobject::PointLight>("FirePointLight");
        _fireLight->SetRange(5.0f);
        _fireLight->SetAmbient(Color::GetColorFromRGBA(float4(168.f, 168.f, 168.f, 255.f)));
        _fireLight->SetDiffuse(Color::GetColorFromRGBA(float4(242.f, 125.f, 12.f, 255.f)));
        _fireLight->SetSpecular(Color::GetColorFromRGBA(float4(242.f, 125.f, 12.f, 255.f)));
        manager::GetCurrentLightingMgr()->Regist(_fireLight);
    }

    {
        const float3                    initPos   = float3(0.0f, 0.0f, 1.0f);
        const float3                    initScale = float3(25.0f, 25.0f, 25.0f);
        shr_ptr<gameobject::GameObject> fireWood  = std::make_shared<gameobject::GameObject>("FireWood", initPos, initScale);
        fireWood->AddComponent<component::ModelRenderer>("model/props/Firewood02.fbx")
            ->LoadExternalDiffuseTexture("model/props/Firewood02.png");
        _pEntityMgr->Regist(fireWood);
    }

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void TitleScene::OnUpdate()
{
    SceneUpdate();

    // ライトの強さが火のようにゆらゆらする
    f32 att = math::SinWave(0.3f, 0.6f);
    _fireLight->SetAttenuation({ 0.0f, att, 0.0f });

    HandleGamePadIcon();
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void TitleScene::OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    gpu::setRenderTarget(0, colorTexture);
    gpu::setDepthStencil(depthTexture);

    // カラーとデプスバッファをクリア
    gpu::clearColor(colorTexture, float4(0.0f, 0.0f, 0.0f, 1.0f));   // カラーバッファ
    gpu::clearDepth(depthTexture, 1.0f);                             // デプスバッファ

    // 表裏カリングOFF
    gpu::setRasterizerState(gpu::commonStates().CullNone());

    // 文字描画
    {
        renderer::BeginDrawString();
        // タイトル、ヒント
        //renderer::DrawString(float2(sys::WINDOW_CENTER_X, 200), L"Tales of Sword", WHITE, 3.0f);
        /*   f32 scale = 0.4f + abs(sin(timer::TimerIns()->TotalTime())) / 3.0f;

        wchar_t* hint = input::IsGamePadConnected() ? L"Press 'B' To Continu e" :
                                                      L"Press Space Key To Continue";*/
        //renderer::DrawString(float2(sys::WINDOW_CENTER_X, 700), hint, WHITE, scale);

        switch(_currentSceneState) {
            case eSceneState::SelectMenuState:
            {
                renderer::ChangeFont(_fontMPlus);
                // 選択肢、選択したら緑色をつける
                float4 color = _currentSelect == ESelection::Start ? GREEN : WHITE;
                renderer::DrawString(float2(sys::WINDOW_CENTER_X, 400), L"ゲームスタート", color, 1.0f);
                color = _currentSelect == ESelection::Menu ? GREEN : WHITE;
                renderer::DrawString(float2(sys::WINDOW_CENTER_X, 480), L"ゲーム設定", color, 1.0f);
                color = _currentSelect == ESelection::Quit ? GREEN : WHITE;
                renderer::DrawString(float2(sys::WINDOW_CENTER_X, 560), L"ゲーム終了", color, 1.0f);
            } break;
            case eSceneState::SelectPlayerState:
            {
                renderer::ChangeFont(_fontMPlus);
                renderer::DrawString(float2(sys::WINDOW_CENTER_X, 240), L"キャラクター選択", WHITE, 1.2f);
            } break;
        }
        renderer::EndDrawString();
    }
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void TitleScene::OnRenderImgui()
{
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void TitleScene::OnFinalize()
{
}
//---------------------------------------------------------------------------
//! シーンの更新ステージ
//---------------------------------------------------------------------------
void TitleScene::SceneUpdate()
{
    // タイトルアニメション
    if(_fontAnimCounter < 1.0f) {
        constexpr f32 SPEED      = 0.005f;   // 速度値
        constexpr f32 MAX_BLUR   = 10.0f;    // マックスブラー値
        constexpr f32 INIT_SCALE = 2.0f;     // 初期スケール

        _fontAnimCounter += SPEED;
        _spriteTitle->SetBlurPower(MAX_BLUR - _fontAnimCounter * MAX_BLUR);
        _spriteTitle->SetColor(float4(1.0f, 1.0f, 1.0f, _fontAnimCounter));   //　透明から実体

        f32 scale = INIT_SCALE - _fontAnimCounter;
        _spriteTitle->GetComponent<component::Transform>()->SetScale(float3(scale, scale, scale));
    }

    switch(_currentSceneState) {
        case eSceneState::SelectMenuState:
            HandleSelectMenu();
            break;
        case eSceneState::SelectPlayerState:
            HandleSelectCharacter();
            break;
    }
}
//---------------------------------------------------------------------------
//! フォント選択肢の処理
//---------------------------------------------------------------------------
void TitleScene::HandleSelectMenu()
{
    // 選択肢
    if(input::IsPressSelectKeyBackward()) {
        _currentSelect--;
        if(_currentSelect < 0) _currentSelect = ESelection::Quit;
    }
    if(input::IsPressSelectKeyForward()) {
        _currentSelect++;
        if(_currentSelect > 2) _currentSelect = ESelection::Start;
    }

    // 選択する
    // スペースキーを押したら、ゲームシーンに切り替えます
    if(input::IsPressSelectKeyConform()) {
        if(_currentSelect == 0) {
            _currentSceneState = eSceneState::SelectPlayerState;
            _currentSelect     = 0;
            CheckSelectCharacter(_currentSelect);
        }
        else if(_currentSelect == 1) {
            //ChangeScene(scene::EScene::Tutorial);
            _pSettingPanel->OpenSettingPanel();
        }
        else {
            SystemSettingsMgr()->QuitGame();
        }
    }
}
//---------------------------------------------------------------------------
//! キャラ選択肢の処理
//---------------------------------------------------------------------------
void TitleScene::HandleSelectCharacter()
{
    static constexpr s32 MEN  = 0;
    static constexpr s32 GIRL = 1;

    // 選択肢
    if(input::IsPressSelectKeyBackward() || input::GamePadMgr()->IsDPadRightPressed()) {
        _currentSelect++;
        if(_currentSelect > GIRL) _currentSelect = MEN;

        CheckSelectCharacter(_currentSelect);
    }
    if(input::IsPressSelectKeyForward() || input::GamePadMgr()->IsDPadLeftPressed()) {
        _currentSelect--;
        if(_currentSelect < MEN) _currentSelect = GIRL;

        CheckSelectCharacter(_currentSelect);
    }

    static f32 lookAtAnimation = 0.5f;
    if(_currentSelect == MEN) {
        lookAtAnimation += timer::DeltaTime();
    }
    else if(_currentSelect == GIRL) {
        lookAtAnimation -= timer::DeltaTime();
    }
    lookAtAnimation = std::clamp(lookAtAnimation, 0.0f, 1.0f);

    if(lookAtAnimation == 1.0f) {
        _arrowRight->Enable();
        _arrowRight->GetComponent<component::Transform>()
            ->SetPositionX(sys::WINDOW_CENTER_X + 135.0f + math ::SinWave(-20.0f, 20.0f, 35.0f));
        _arrowLeft->Disable();
    }
    else if(lookAtAnimation == 0.0f) {
        _arrowRight->Disable();
        _arrowLeft->GetComponent<component::Transform>()
            ->SetPositionX(sys::WINDOW_CENTER_X - 135.0f + math ::SinWave(-20.0f, 20.0f, 35.0f));
        _arrowLeft->Enable();
    }
    else {
        _arrowLeft->Disable();
        _arrowRight->Disable();
    }

    auto cameraTransform = manager::GetCurrentCamera()->GetComponent<component::Transform>();
    auto f1              = _characherGirl->GetPosition();
    f1.f32[1]            = cameraTransform->GetPosition().f32[1];
    auto f2              = _characherMen->GetPosition();
    f2.f32[1]            = cameraTransform->GetPosition().f32[1];
    float3 c             = hlslpp::lerp(f1, f2, lookAtAnimation);

    cameraTransform->LookAt(c);

    // 選択する
    // スペースキーを押したら、ゲームシーンに切り替えます
    if(input::IsPressSelectKeyConform()) {
        if(_currentSelect == MEN) {
            manager::GameMgr()->_playerAnim         = "anim/PlayerVampire.anim";
            manager::GameMgr()->_playerWeaponBoneId = 26;
            manager::GameMgr()->_playerModelName    = "vampire";
        }
        else if(_currentSelect == GIRL) {
            manager::GameMgr()->_playerAnim         = "anim/PlayerArcher.anim";
            manager::GameMgr()->_playerModelName    = "archer";
            manager::GameMgr()->_playerWeaponBoneId = 63;
        }

        ChangeScene(scene::EScene::Game);
    }
}
//---------------------------------------------------------------------------
//!<  ゲームアイコン処理
//!<  ゲームパッドを繋がったらアイコンを表示
//---------------------------------------------------------------------------
void TitleScene::HandleGamePadIcon()
{
    input::IsGamePadConnected() ? _gamePadIcon->Enable() : _gamePadIcon->Disable();
}
void TitleScene::CheckSelectCharacter(s32 id)
{
    if(id == 0) {   // MEN
        _characherMen->GetComponent<component::ModelRenderer>()->SetIsRenderOutline(true);
        _characherMen->GetComponent<component::Animator>()->Play("Stand", Animation::PlayType::Once);

        _characherGirl->GetComponent<component::ModelRenderer>()->SetIsRenderOutline(false);
        _characherGirl->GetComponent<component::Animator>()->Play("SittingIdle", Animation::PlayType::Loop);

        auto   cameraTransform = manager::GetCurrentCamera()->GetComponent<component::Transform>();
        float3 lookat          = _characherMen->GetPosition();
        lookat.f32[1]          = cameraTransform->GetPosition().f32[1];
        //cameraTransform->LookAt(lookat);
    }
    else if(id == 1) {   // GIRL
        _characherMen->GetComponent<component::ModelRenderer>()->SetIsRenderOutline(false);
        _characherMen->GetComponent<component::Animator>()->Play("SittingIdle", Animation::PlayType::Loop);

        _characherGirl->GetComponent<component::ModelRenderer>()->SetIsRenderOutline(true);
        _characherGirl->GetComponent<component::Animator>()->Play("Stand", Animation::PlayType::Once);
        //manager::GetCurrentCamera()->GetComponent<component::Transform>()->LookAt(_characherGirl->GetPosition());

        auto   cameraTransform = manager::GetCurrentCamera()->GetComponent<component::Transform>();
        float3 lookat          = _characherGirl->GetPosition();
        lookat.f32[1]          = cameraTransform->GetPosition().f32[1];
    }
    else {
        ASSERT_MESSAGE(false, "ID存在しない");
    }
}
//---------------------------------------------------------------------------
//! シーンの生成
//! @note テンプレートの特殊化で実装します
//---------------------------------------------------------------------------
template<>
std::unique_ptr<BaseScene> createNewScene<TitleScene>()
{
    return std::make_unique<TitleScene>();
}

}   // namespace scene
