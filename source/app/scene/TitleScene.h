﻿//---------------------------------------------------------------------------
//!	@file	TitleScene.h
//!	@brief	ゲームタイトル画面
//---------------------------------------------------------------------------
#pragma once
#include "BaseScene.h"
namespace entity::go {
class GameObject;
class PointLight;

namespace ui {
class Sprite;
class AdvancedSprite;
}   // namespace ui

}   // namespace entity::go

namespace scene {
class TitleScene final : public BaseScene
{
public:
    TitleScene();    //! コンストラクター
    ~TitleScene();   //! デストラクター
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
protected:
private:
    enum class eSceneState
    {
        SelectMenuState,
        SelectPlayerState,
        Count
    };
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    uni_ptr<DirectX::SpriteFont> _fontMPlus;    //< Jiyuchoというフォント
    uni_ptr<DirectX::SpriteFont> _fontMsFont;   //!< Msというフォント

    s32 _currentSelect = 0;   //!< 0 = ゲームスタート | 1 = ゲームチュートリアル

    raw_ptr<ui::Sprite>         _gamePadIcon;   //!< ゲームパッドのアイコン
    raw_ptr<ui::Sprite>         _arrowRight;    //!< 右矢印のアイコン
    raw_ptr<ui::Sprite>         _arrowLeft;     //!< 左矢印のアイコン
    raw_ptr<ui::AdvancedSprite> _spriteTitle;   //!< タイトル画像

    f32 _fontAnimCounter = 0.0f;   //! 初期アニメション用のカウンター

    eSceneState _currentSceneState = eSceneState::SelectMenuState;   //!< 現在のシーンステージ

    shr_ptr<gameobject::GameObject> _characherGirl;   //!< 男性主人公
    shr_ptr<gameobject::GameObject> _characherMen;    //!< 女性主人公
    shr_ptr<gameobject::PointLight> _fireLight;       //!< キャンプファイヤー

	Effekseer::EffectRef _shockWaveWffect;   //!< 衝撃波みたいないエフェクト
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------

    //----------
    // 継承
    bool OnInit() override;                                                                           //!< 初期化
    void OnUpdate() override;                                                                         //!< 更新
    void OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture) override;   //!< 描画
    void OnRenderImgui() override;                                                                    //!< Imgui描画
    void OnFinalize() override;                                                                       //!< 解放

    void SceneUpdate();             //!< シーンのステート更新
    void HandleSelectMenu();        //!< メニュー選択肢の処理
    void HandleSelectCharacter();   //!< キャラ選択肢の処理
    void HandleGamePadIcon();       //!< ゲームアイコン処理

    void CheckSelectCharacter(s32 id);   //!< キャラ選択
};
}   // namespace scene
