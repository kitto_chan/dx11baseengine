﻿//---------------------------------------------------------------------------
//!	@file	TutorialScene.h
//!	@brief	ゲームチュートリアル
//---------------------------------------------------------------------------

#include "TutorialScene.h"

#include <fstream>
#include <filesystem>

#include "manager/SceneManager.h"
#include "manager/CameraManager.h"

#include "component/Transform.h"

#include "effect/Effect.h"

#include "entity/gameObject/ui/Sprite.h"
namespace scene {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
TutorialScene::TutorialScene()
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
TutorialScene::~TutorialScene()
{
}

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool TutorialScene::OnInit()
{
    // フォントを読み込む
    _fontJiyucho = renderer::CreateSpriteFont(L"font/Jiyucho16.spritefont");
    _fontMsFont  = renderer::CreateSpriteFont(L"font/Ms36B.spritefont");

    LoadControllerIconInDir("ui/control/mouse/fill");
    LoadControllerIconInDir("ui/control/keyboard/fill");
    LoadControllerIconInDir("ui/control/gamepad/fill");

    _page1 = new gameobject::GameObject("Page1");
    _page2 = new gameobject::GameObject("Page2");
    _pEntityMgr->Regist(_page1.get());
    _pEntityMgr->Regist(_page2.get());

    // 攻撃
    {
        ui::Sprite* icon = new ui::Sprite("Icon Mouse Left", float3(240.0f, 300.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["mouse_left"]);
        icon->SetParent(_page1);
        _pEntityMgr->Regist(icon);
    }
    {
        ui::Sprite* icon = new ui::Sprite("Button X", float3(1220.0f, 300.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["button_x"]);
        icon->SetParent(_page1);
        _pEntityMgr->Regist(icon);
    }

    // カメラ操作
    {
        ui::Sprite* icon = new ui::Sprite("Mouse", float3(240.0f, 420.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["mouse_none"]);
        icon->SetParent(_page1);
        _pEntityMgr->Regist(icon);
    }
    {
        ui::Sprite* icon = new ui::Sprite("JoyStick R", float3(1220.0f, 420.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["joystick3_right"]);
        icon->SetParent(_page1);
        _pEntityMgr->Regist(icon);
    }

    // 選択
    {
        ui::Sprite* icon = new ui::Sprite("Keyboard Space", float3(240.0f, 540.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["keyboard_Space"]);
        icon->SetParent(_page1);
        _pEntityMgr->Regist(icon);
    }
    {
        ui::Sprite* icon = new ui::Sprite("Button B", float3(1220.0f, 540.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["button_b"]);
        icon->SetParent(_page1);
        _pEntityMgr->Regist(icon);
    }

    // 移動
    {
        ui::Sprite* icon = new ui::Sprite("KeyBoard AWSD", float3(240.0f, 300.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["awsd"]);
        icon->SetParent(_page2);
        _pEntityMgr->Regist(icon);
    }
    {
        ui::Sprite* icon = new ui::Sprite("JoyStick R", float3(1220.0f, 300.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["joystick3_right"]);
        icon->SetParent(_page2);
        _pEntityMgr->Regist(icon);
    }

    // ダッシュ
    {
        ui::Sprite* icon = new ui::Sprite("LShift", float3(240.0f, 420.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["keyboard_LCtrl"]);
        icon->SetParent(_page2);
        _pEntityMgr->Regist(icon);
    }
    {
        ui::Sprite* icon = new ui::Sprite("RB", float3(1220.0f, 420.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["rb"]);
        icon->SetParent(_page2);
        _pEntityMgr->Regist(icon);
    }

    // ジャンプ
    {
        ui::Sprite* icon = new ui::Sprite("Keyboard Space", float3(240.0f, 540.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["keyboard_Space"]);
        icon->SetParent(_page2);
        _pEntityMgr->Regist(icon);
    }
    {
        ui::Sprite* icon = new ui::Sprite("Button B", float3(1220.0f, 540.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["keyboard_Space"]);
        icon->SetParent(_page2);
        _pEntityMgr->Regist(icon);
    }

    // 回避
    {
        ui::Sprite* icon = new ui::Sprite("Keyboard LShift", float3(240.0f, 660.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["keyboard_LShift"]);
        icon->SetParent(_page2);
        _pEntityMgr->Regist(icon);
    }
    {
        ui::Sprite* icon = new ui::Sprite("Button B", float3(1220.0f, 660.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["button_b"]);
        icon->SetParent(_page2);
        _pEntityMgr->Regist(icon);
    }
    // next
    {
        ui::Sprite* icon = new ui::Sprite("Keyboard LShift", float3(1230.0f, 750.0f, 1.0f), float3(1.0f, 0.8f, 0.8f));
        icon->SetTexture(_controllericonTextures["keyboard_Space"]);
        _pEntityMgr->Regist(icon);
    }
    {
        ui::Sprite* icon = new ui::Sprite("Button B", float3(1300.0f, 750.0f, 1.0f), float3(0.7f, 0.7f, 0.7f));
        icon->SetTexture(_controllericonTextures["button_b"]);
        _pEntityMgr->Regist(icon);
    }
    _page1->Enable();
    _page2->Disable();
    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void TutorialScene::OnUpdate()
{
    if(input::IsKeyPress(DirectX::Keyboard::Space) || input::GamePadMgr()->IsBPressed()) {
        if(_currentPage == 0) {
            _currentPage += 1;
            _page1->Disable();
            _page2->Enable();
        }
        else if(_currentPage == 1) {
            ChangeScene(scene::EScene::Title);
        }
    }
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void TutorialScene::OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    gpu::setRenderTarget(0, colorTexture);
    gpu::setDepthStencil(depthTexture);

    // カラーとデプスバッファをクリア
    gpu::clearColor(colorTexture, float4(0.0f, 0.0f, 0.0f, 1.0f));   // カラーバッファ
    gpu::clearDepth(depthTexture, 1.0f);                             // デプスバッファ

    // TODO:
    // 操作説明は画像で伝わる

    // 文字描画
    renderer::BeginDrawString();
    {
        renderer::ChangeFont(_fontMsFont.get());
        renderer::DrawString(float2(sys::WINDOW_CENTER_X, 100), L"操作方法", WHITE, 1.8f);
        if(_currentPage == 0) {
            renderer::DrawString(float2(sys::WINDOW_CENTER_X, 300.f), L"攻撃", WHITE, 1.0f);
            renderer::DrawString(float2(sys::WINDOW_CENTER_X, 420.f), L"カメラ操作", WHITE, 1.0f);
            renderer::DrawString(float2(sys::WINDOW_CENTER_X, 540.f), L"選択", WHITE, 1.0f);
        }
        else if(_currentPage == 1) {
            renderer::DrawString(float2(sys::WINDOW_CENTER_X, 300.f), L"移動", WHITE, 1.0f);
            renderer::DrawString(float2(sys::WINDOW_CENTER_X, 420.f), L"ダッシュ", WHITE, 1.0f);
            renderer::DrawString(float2(sys::WINDOW_CENTER_X, 540.f), L"ジャンプ", WHITE, 1.0f);
            renderer::DrawString(float2(sys::WINDOW_CENTER_X, 660.f), L"回避", WHITE, 1.0f);
        }
        renderer::DrawString(float2(1380.f, 750.f), L"next", WHITE, 0.7f);
    }
    renderer::EndDrawString();
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void TutorialScene::OnRenderImgui()
{
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void TutorialScene::OnFinalize()
{
}
//---------------------------------------------------------------------------
//! フォルダ中のアイコン画像を読み込む
//---------------------------------------------------------------------------
void TutorialScene::LoadControllerIconInDir(std::string path)
{
    for(auto const& dirEntry : std::filesystem::directory_iterator{ path }) {
        std::string fileName = string::convertUnicodeToUtf8(dirEntry.path().stem().c_str());
        std::string filePath = string::convertUnicodeToUtf8(dirEntry.path().c_str());

        std::shared_ptr<gpu::Texture> icon = gpu::createTextureFromFile(filePath);

        if(!icon) {
            ASSERT_MESSAGE(icon, "Icon not find");
            continue;
        }

        _controllericonTextures[fileName.data()] = std::move(icon);
    }
}
//---------------------------------------------------------------------------
//! シーンの生成
//! @note テンプレートの特殊化で実装します
//---------------------------------------------------------------------------
template<>
std::unique_ptr<BaseScene> createNewScene<TutorialScene>()
{
    return std::make_unique<TutorialScene>();
}

}   // namespace scene
