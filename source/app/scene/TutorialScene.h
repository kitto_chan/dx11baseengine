﻿//---------------------------------------------------------------------------
//!	@file	TutorialScene.h
//!	@brief	ゲームチュートリアル
//---------------------------------------------------------------------------
#pragma once
#include "BaseScene.h"
namespace entity::go {
class GameObject;
namespace ui {
class Sprite;
}
}   // namespace entity::go
namespace scene {
class TutorialScene final : public BaseScene
{
public:
    TutorialScene();    //! コンストラクター
    ~TutorialScene();   //! デストラクター
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
protected:
private:
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
    //----------
    // 継承
    bool OnInit() override;                                                                           //!< 初期化
    void OnUpdate() override;                                                                         //!< 更新
    void OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture) override;   //!< 描画
    void OnRenderImgui() override;                                                                    //!< Imgui描画
    void OnFinalize() override;                                                                       //!< 解放

    //----------
    //! フォルダ中のアイコン画像を読み込む
    void LoadControllerIconInDir(std::string path);

    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    uni_ptr<DirectX::SpriteFont> _fontJiyucho;   //< Jiyuchoというフォント
    uni_ptr<DirectX::SpriteFont> _fontMsFont;    //!< Msというフォント

    raw_ptr<gameobject::GameObject> _page1;
    raw_ptr<gameobject::GameObject> _page2;

    s32 _currentPage = 0;

    std::map<std::string, std::shared_ptr<gpu::Texture>> _controllericonTextures;
};
}   // namespace scene
