﻿//---------------------------------------------------------------------------
//!	@file	AnimationConfigScene.h
//!	@brief	アニメション設定シーン
//---------------------------------------------------------------------------
#include "AnimationConfigScene.h"
#include "utility/SystemHelper.h"
#include "manager/SceneManager.h"
#include "manager/CameraManager.h"

#include "component/Transform.h"
#include "component/ModelRenderer.h"
#include "component/Animator.h"

#include "effect/Effect.h"
#include "renderer/GeometryRenderer.h"

#include <filesystem>
#include <nlohmann/json.hpp>
#include <imgui/imgui_internal.h>
#include <imgui/misc/cpp/imgui_stdlib.h>
#include <ImguiFileDialog/ImGuiFileDialog.h>

#include "entity/gameObject/ui/Sprite.h"
#include "entity/gameObject/Line.h"
namespace scene {
namespace {
cb::WorldCB _worldCB{};
//---------------------------------------------------------------------------
//! グリッド線を描画
//---------------------------------------------------------------------------
void DrawGrid()
{
    //constexpr f32 gridSize = 32.0f;   // グリッドの大きさ

    //for(s32 i = (s32)-gridSize; i < gridSize; i++) {
    //    // 軸の描画
    //    renderer::DrawLine(float3(-gridSize, 0.f, i * 1.f), float3(+gridSize, 0.f, i * 1.f), float4(1.f, 0.f, 0.f, 1.f));   // X軸
    //    renderer::DrawLine(float3(i * 1.f, 0.f, -gridSize), float3(i * 1.f, 0.f, +gridSize), float4(0.f, 0.f, 1.f, 1.f));   // Z軸
    //}
    //renderer::DrawLine(float3(0.f, -gridSize, 0.f), float3(0.f, +gridSize, 0.f), float4(0.f, 1.f, 0.f, 1.f));   // Y軸
}
//---------------------------------------------------------------------------
//! プロジェクトのリソースパスに変更
//---------------------------------------------------------------------------
std::string GetRelativePath(std::string_view fullPath)
{
    u64         currentPathSize = std::filesystem::current_path().string().size() + 1;
    std::string relativePath    = std::string(fullPath).substr(currentPathSize, fullPath.size());

    return relativePath;
}
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
AnimationConfigScene::AnimationConfigScene()
{
    _isRenderShadow = false;
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
AnimationConfigScene::~AnimationConfigScene()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool AnimationConfigScene::OnInit()
{
    MouseMgr()->SetAbsoluteMode();

    renderer::Init();

    _pCameraMgr->GetCurrentCamera()->GetComponent<component::Transform>()->SetPosition(float3(0.0f, 5.0f, 5.0f));
    _pCameraMgr->GetCurrentCamera()->GetComponent<component::Transform>()->SetRotationX(math::D2R(-45.0f));

    {
        ui::Sprite* sprite = new ui::Sprite("AnimationEditor", { 68.0f, 760.0f, 0.0f });
        sprite->SetTexture("ui/AnimationEditor.png");
        _pEntityMgr->Regist(sprite);
    }
    gameobject::GameObject* grid = new gameobject::GameObject("Grid", float3(0.0f, 0.0f, 0.0f), float3(32.0f, 32.0f, 32.0f));
    _pEntityMgr->Regist(grid);

    constexpr f32 gridSize = 10.0f;   // グリッドの大きさ

    for(s32 i = (s32)-gridSize; i < gridSize; i++) {
        {
            gameobject::Line* line = new gameobject::Line("Line", float3(0.0f, 0.0f, i * 0.05f));
            line->SetParent(grid);
            line->SetColor(float4(1.0f, 0.0f, 0.0f, 1.0f));
            _pEntityMgr->Regist(line);
        }
        {
            gameobject::Line* line = new gameobject::Line("Line", float3(i * 0.05f, 0.0f, 0.0f));
            line->GetComponent<component::Transform>()->SetRotationY(math::D2R(90.0f));
            line->SetColor(float4(0.0f, 0.0f, 1.0f, 1.0f));
            line->SetParent(grid);
            _pEntityMgr->Regist(line);
        }
    }

    {
        gameobject::Line* line = new gameobject::Line("Line", float3(0.0f, 0.0f, 0.0f));
        line->GetComponent<component::Transform>()->SetRotationZ(math::D2R(90.0f));
        line->SetColor(float4(0.0f, 1.0f, 0.0f, 1.0f));
        line->SetParent(grid);
        _pEntityMgr->Regist(line);
    }
    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void AnimationConfigScene::OnUpdate()
{
    if(_pModel && _currentBlend) {
        const raw_ptr<component::Animator>& animator = _pModel->GetComponent<component::Animator>();
        if(animator) {
            if(animator->IsAnimationName(_currentBlend->_from)) {
                if(animator->IsLastFrame(_currentBlend->_blendTime)) {
                    animator->Play(_currentBlend->_to, Animation::PlayType::Once);
                }
            }
            else {
                if(animator->IsLastFrame()) {
                    animator->Play(_currentBlend->_from, Animation::PlayType::Once);
                }
            }
        }
    }
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void AnimationConfigScene::OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    gpu::setRenderTarget(0, colorTexture);
    gpu::setDepthStencil(depthTexture);

    // カラーとデプスバッファをクリア
    gpu::clearColor(colorTexture, float4(0.5f, 0.5f, 0.8f, 1.0f));   // カラーバッファ
    gpu::clearDepth(depthTexture, 1.0f);                             // デプスバッファ

    // 表裏カリングOFF
    gpu::setRasterizerState(gpu::commonStates().CullNone());

    gpu::setConstantBuffer("WorldCB", _worldCB);
    manager::SetCurrentCameraCB();

    DrawGrid();
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void AnimationConfigScene::OnRenderImgui()
{
    // AnimationConfig パネル
    ImGui::Begin("AnimationConfig");
    HandleConfigLeftPanel();
    ImGui::SameLine();
    HandleConfigRightPanel();
    ImGui::End();

    HandleMenuBar();
}
//---------------------------------------------------------------------------
//! モデルを選択
//---------------------------------------------------------------------------
void AnimationConfigScene::HandleChooseModel()
{
    // モデルファイル名
    imgui::ImguiColumn2FormatBegin("Model");
    ImGui::Text(_modelPath.c_str());
    imgui::ImguiColumn2FormatEnd();

    // ファイル選択ボタン
    imgui::ImguiColumn2FormatBegin("");
    if(ImGui::Button(ICON_FA_FOLDER " Select Model")) {
        ImGuiFileDialog::Instance()->OpenModal("ChooseModel",
                                               "ChooseModel",
                                               ".fbx{.fbx,.FBX}", ".");
        SetNextPopUpModal();
    }
    imgui::ImguiColumn2FormatEnd();

    // ファイルダイアログを表示
    if(ImGuiFileDialog::Instance()->Display("ChooseModel",
                                            ImGuiWindowFlags_NoCollapse,
                                            MIN_SIZE,
                                            MAX_SIZE)) {
        // action if OK
        if(ImGuiFileDialog::Instance()->IsOk()) {
            std::string filePathName = ImGuiFileDialog::Instance()->GetFilePathName();
            std::string fileName     = ImGuiFileDialog::Instance()->GetCurrentFileName();
            std::string relatedPath  = GetRelativePath(filePathName);

            // action
            Reset();

            _modelPath = relatedPath;
            _pModel    = std::make_shared<GameObject>(fileName);
            _pModel->AddComponent<component::ModelRenderer>(relatedPath);
            _pEntityMgr->Regist(_pModel);
        }
        // close
        ImGuiFileDialog::Instance()->Close();
    }
}
//---------------------------------------------------------------------------
//! 外部のから読み込むのテクスチャーを選択
//---------------------------------------------------------------------------
void AnimationConfigScene::HandleChooseExtenalTexture()
{
    // テクスチャー名
    imgui::ImguiColumn2FormatBegin("Texture");
    ImGui::Text(_diffuseTexture.c_str());
    imgui::ImguiColumn2FormatEnd();

    // ファイル選択ボタン
    imgui::ImguiColumn2FormatBegin("");
    if(ImGui::Button(ICON_FA_FOLDER " Select Texture")) {
        ImGuiFileDialog::Instance()->OpenModal("ChooseTexture",
                                               "ChooseTexture",
                                               ".png {.png,.PNG,.tga}", ".");
        SetNextPopUpModal();
    }
    imgui::ImguiColumn2FormatEnd();

    // ファイルダイアログを表示
    if(ImGuiFileDialog::Instance()->Display("ChooseTexture",
                                            ImGuiWindowFlags_NoCollapse,
                                            MIN_SIZE,
                                            MAX_SIZE)) {
        // action if OK
        if(ImGuiFileDialog::Instance()->IsOk()) {
            std::string filePathName = ImGuiFileDialog::Instance()->GetFilePathName();
            // action
            _pModel->GetComponent<component::ModelRenderer>()->LoadExternalDiffuseTexture(filePathName);
            _diffuseTexture = GetRelativePath(filePathName);
        }

        // close
        ImGuiFileDialog::Instance()->Close();
    }
}
//---------------------------------------------------------------------------
//! アニメションを選択
//---------------------------------------------------------------------------
void AnimationConfigScene::HandleChooseAnimations()
{
    // ファイル選択ボタン(多選)
    imgui::ImguiColumn2FormatBegin("Animation");
    if(ImGui::Button(ICON_FA_FOLDER " Select Animations")) {
        ImGuiFileDialog::Instance()->OpenModal("ChooseAnimations",   // Key
                                               "ChooseAnimations",   // Title
                                               ".fbx {.fbx,.FBX}",   // フィルター
                                               ".", 0);
        SetNextPopUpModal();
    }
    imgui::ImguiColumn2FormatEnd();

    // ファイルダイアログを表示
    if(ImGuiFileDialog::Instance()->Display("ChooseAnimations",
                                            ImGuiWindowFlags_NoCollapse,
                                            MIN_SIZE,
                                            MAX_SIZE)) {
        // action if OK
        if(ImGuiFileDialog::Instance()->IsOk()) {
            auto selections = ImGuiFileDialog::Instance()->GetSelection();

            // action
            for(auto& selection : selections) {
                static const std::string FIND_PATH = "resource\\";

                const std::string& fileName = selection.first;
                const std::string& filePath = selection.second;

                // 念のために、ディレクトリなどのファイルを除外
                if(fileName.length() <= 4) continue;

                std::string relatedPath = GetRelativePath(filePath);

                // .fbxを捨てる
                std::string key = fileName.substr(0, fileName.length() - 4);

                // key - path のマッピング
                _animationMapping.emplace_back(std::pair(key, relatedPath));
            }

            // マッピングリストをつくる
            Animation::Desc* desc  = new Animation::Desc[_animationMapping.size()];
            int              count = 0;
            for(const auto& aniMap : _animationMapping) {
                desc[count] = { aniMap.first.c_str(), aniMap.second };
                count++;
            }
            // アニメションを生成
            if(_pModel) {
                _pModel->AddComponent<component::Animator>(desc, _animationMapping.size());
            }
        }
        // close
        ImGuiFileDialog::Instance()->Close();
    }

    // 選んだアニメションをテーブルで表示
    static ImGuiTableFlags tableFlags = ImGuiTableFlags_SizingFixedFit |
                                        ImGuiTableFlags_RowBg |
                                        ImGuiTableFlags_Borders |
                                        ImGuiTableFlags_Resizable |
                                        ImGuiTableFlags_Reorderable |
                                        ImGuiTableFlags_Hideable;

    if(ImGui::BeginTable("Table", 2, tableFlags)) {
        ImGui::TableSetupColumn("Key", ImGuiTableColumnFlags_WidthStretch);
        ImGui::TableSetupColumn("Path", ImGuiTableColumnFlags_WidthStretch);
        ImGui::TableHeadersRow();
        int id = 0;
        for(auto& animation : _animationMapping) {
            ImGui::PushID(id);
            ImGui::TableNextRow();
            ImGui::TableSetColumnIndex(0);

            // キー
            ImGui::PushItemWidth(ImGui::GetColumnWidth());
            std::string keyCache = animation.first;
            ImGui::InputText("##AnimationKey", &animation.first);
            // キーが変更したら、_blendListも同期する
            if(ImGui::IsItemEdited()) {
                for(auto& blend : _blendList) {
                    // Fromキーをチェック
                    if(blend._from == keyCache) {
                        blend._from = animation.first;
                    }
                    // Toキーをチェック
                    if(blend._to == keyCache) {
                        blend._to = animation.first;
                    }
                }
            }
            // Hovered Text;
            if(ImGui::IsItemHovered())
                ImGui::SetTooltip(animation.first.c_str());
            ImGui::TableSetColumnIndex(1);

            // Path
            ImGui::PushItemWidth(ImGui::GetColumnWidth());
            ImGui::Text(animation.second.c_str());
            if(ImGui::IsItemHovered())
                ImGui::SetTooltip(animation.second.c_str());

            ImGui::PopID();
            id++;
        }
        ImGui::EndTable();
    }
}
//---------------------------------------------------------------------------
//!< ブレンド設定のパネル
//---------------------------------------------------------------------------
void AnimationConfigScene::HandleBlendPanel()
{
    if(!(_animationMapping.empty() && _blendList.empty())) {
        {
            static ImGuiTableFlags flags = ImGuiTableFlags_SizingFixedFit |
                                           ImGuiTableFlags_RowBg |
                                           ImGuiTableFlags_Borders |
                                           ImGuiTableFlags_Resizable |
                                           ImGuiTableFlags_Reorderable |
                                           ImGuiTableFlags_Hideable;
            // 以下のようにテーブルを表示
            // | From | To | BlendTime | Play | Remove|
            ImGui::BeginTable("table2", 5, flags);
            ImGui::TableSetupColumn("From", ImGuiTableColumnFlags_WidthStretch);
            ImGui::TableSetupColumn("To", ImGuiTableColumnFlags_WidthStretch);
            ImGui::TableSetupColumn("BlendTime", ImGuiTableColumnFlags_WidthStretch);
            ImGui::TableSetupColumn("Play", ImGuiTableColumnFlags_WidthStretch);
            ImGui::TableSetupColumn("Remove", ImGuiTableColumnFlags_WidthStretch);
            ImGui::TableHeadersRow();

            for(int i = 0; i < _blendList.size(); i++) {
                // Form
                ImGui::TableNextColumn();
                BlendListComboFrom(i, _blendList[i]);
                // TO
                ImGui::TableNextColumn();
                BlendListComboTo(i, _blendList[i]);
                // BlendTime
                ImGui::TableNextColumn();
                BlendListBlendTime(i, _blendList[i]);
                // Play Button
                ImGui::TableNextColumn();
                BlendListPlayButton(i, _blendList[i]);
                // Remove Button
                ImGui::TableNextColumn();
                BlendListRemoveButton(i);
            }
            ImGui::EndTable();
        }

        // 追加ブレンドのボタン
        if(ImGui::Button(u8"追加")) {
            BlendDesc newBlend{ _animationMapping.begin()->first,
                                _animationMapping.begin()->first,
                                0.25f };

            _blendList.emplace_back(newBlend);
        }
    }
}
//---------------------------------------------------------------------------
//!< アニメション設定を読み込む
//---------------------------------------------------------------------------
void AnimationConfigScene::HandleNewButton()
{
    ImVec2 center = ImGui::GetMainViewport()->GetCenter();
    ImGui::SetNextWindowPos(center, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));
    if(ImGui::BeginPopupModal("New", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {
        ImGui::Text("新規？変更内容が保存されない可能性があります。");
        ImGui::Separator();

        if(ImGui::Button("OK", ImVec2(120, 0))) {
            Reset();
            ImGui::CloseCurrentPopup();
        }
        ImGui::SetItemDefaultFocus();
        ImGui::SameLine();
        if(ImGui::Button("Cancel", ImVec2(120, 0))) {
            ImGui::CloseCurrentPopup();
        }
        ImGui::EndPopup();
    }
}
//---------------------------------------------------------------------------
//!< アニメション設定を読み込む
//---------------------------------------------------------------------------
void AnimationConfigScene::HandleOpenButton()
{
    // ファイルダイアログを表示
    if(ImGuiFileDialog::Instance()->Display("OpenAnim",
                                            ImGuiWindowFlags_NoCollapse,
                                            MIN_SIZE,
                                            MAX_SIZE)) {
        // action if OK
        if(ImGuiFileDialog::Instance()->IsOk()) {
            Reset();

            std::string filePathName = ImGuiFileDialog::Instance()->GetFilePathName();

            // ファイルを読み込む
            std::ifstream  i(filePathName);
            nlohmann::json jAnim;
            i >> jAnim;
            if(i.is_open())
                i.close();
            //=========================================
            // アニメションの情報を保存します
            //=========================================
            _modelPath                = jAnim["Model"]["Path"];
            _diffuseTexture           = jAnim["Model"]["ExternalTexture"]["Diffuse"];
            nlohmann::json jKeyMap    = jAnim["Animation"]["KeyMap"];
            nlohmann::json jBlendList = jAnim["Animation"]["BlendList"];
            //---------
            // モデル処理
            _pModel = std::make_shared<gameobject::GameObject>();
            _pModel->AddComponent<component::ModelRenderer>(_modelPath);

            //---------
            // テクスチャー処理
            if(!_diffuseTexture.empty()) {
                _pModel->GetComponent<component::ModelRenderer>()->LoadExternalDiffuseTexture(_diffuseTexture);
            }

            //---------
            // アニメション処理
            for(auto& element : jKeyMap.items()) {
                _animationMapping.emplace_back(std::pair(element.key(), element.value()));
            }

            _pModel->AddComponent<component::Animator>(filePathName);
            //---------
            // ブレンドリスト処理
            for(auto& element : jBlendList.items()) {
                BlendDesc blend;
                blend._from = element.key();

                json blendDes = element.value();
                for(auto& desc : blendDes) {
                    blend._to        = desc["To"].get<std::string>();
                    blend._blendTime = desc["BlendTime"].get<float>();
                    _blendList.emplace_back(blend);
                }
            }

            manager::Regist(_pModel);
        }
        // Dialog close
        ImGuiFileDialog::Instance()->Close();
    }
}
//---------------------------------------------------------------------------
//! アニメション設定のJson(anim)ファイルを作成
//---------------------------------------------------------------------------
void AnimationConfigScene::HandleSaveButton()
{
    // ファイルダイアログを表示
    if(ImGuiFileDialog::Instance()->Display("SaveAnim",
                                            ImGuiWindowFlags_NoCollapse,
                                            MIN_SIZE,
                                            MAX_SIZE)) {
        // action if OK
        if(ImGuiFileDialog::Instance()->IsOk()) {
            std::string filePathName = ImGuiFileDialog::Instance()->GetFilePathName();

            //=========================================
            // アニメションの情報を保存します
            //=========================================
            nlohmann::json jAnimConfig;
            jAnimConfig["Model"]["Path"]                       = _modelPath;
            jAnimConfig["Model"]["ExternalTexture"]["Diffuse"] = _diffuseTexture;

            // ブレントリスト
            std::unordered_multimap<std::string, Animation::BlendDesc> _blendDescList;
            Animation::BlendDesc                                       aniBlendDesc;
            for(auto& blend : _blendList) {
                aniBlendDesc._blendTime = blend._blendTime;
                aniBlendDesc._to        = blend._to;

                _blendDescList.emplace(blend._from, aniBlendDesc);
            };

            // アニメションキーマップ
            std::map<std::string, std::string> animationMap;
            for(auto& anim : _animationMapping) {
                animationMap[anim.first] = anim.second;
            }

            jAnimConfig["Animation"] = Animation::GenerateAnimJson(animationMap, _blendDescList);

            // 輸出
            std::ofstream out(filePathName);
            out << std::setw(4) << jAnimConfig << std::endl;

            // File close
            if(out.is_open()) out.close();
        }
        // Dialog close
        ImGuiFileDialog::Instance()->Close();
    }
}
//---------------------------------------------------------------------------
//! Fromアニメションコンボリスト
//---------------------------------------------------------------------------
void AnimationConfigScene::BlendListComboFrom(const s32 id, BlendDesc& blendDesc)
{
    ImGui::PushID(id);
    ImGui::PushItemWidth(ImGui::GetColumnWidth());

    if(ImGui::BeginCombo("##From", blendDesc._from.c_str())) {
        for(auto& anim : _animationMapping) {
            bool isSelected = blendDesc._from == anim.first;
            if(ImGui::Selectable(anim.first.c_str(), isSelected)) {
                blendDesc._from = anim.first;
            }

            if(isSelected) {
                ImGui::SetItemDefaultFocus();
            }
        }
        ImGui::EndCombo();
    }
    // Hovered Text
    if(ImGui::IsItemHovered())
        ImGui::SetTooltip(blendDesc._from.c_str());

    ImGui::PopID();
}
//---------------------------------------------------------------------------
//!  To アニメションコンボリスト
//---------------------------------------------------------------------------
void AnimationConfigScene::BlendListComboTo(const s32 id, BlendDesc& blendDesc)
{
    ImGui::PushID(id);
    ImGui::PushItemWidth(ImGui::GetColumnWidth());
    if(ImGui::BeginCombo("##To", blendDesc._to.c_str())) {
        for(auto& anim : _animationMapping) {
            bool isSelected = blendDesc._to == anim.first;
            if(ImGui::Selectable(anim.first.c_str(), isSelected)) {
                blendDesc._to = anim.first;
            }

            if(isSelected) {
                ImGui::SetItemDefaultFocus();
            }
        }
        ImGui::EndCombo();
    }
    // Hovered Text
    if(ImGui::IsItemHovered())
        ImGui::SetTooltip(blendDesc._to.c_str());

    ImGui::PopID();
}
//---------------------------------------------------------------------------
//! ブレンドタイムを設定
//---------------------------------------------------------------------------
void AnimationConfigScene::BlendListBlendTime(const s32 id, BlendDesc& blendDesc)
{
    ImGui::PushID(id);
    ImGui::PushItemWidth(ImGui::GetColumnWidth());
    ImGui::SliderFloat("##Animation", &blendDesc._blendTime, 0.0f, 1.0f, "%.3f");
    ImGui::PopID();
}
//---------------------------------------------------------------------------
//! 選択したブレンドアニメションを再生
//---------------------------------------------------------------------------
void AnimationConfigScene::BlendListPlayButton(const s32 id, BlendDesc& blendDesc)
{
    ImGui::PushID(id);
    ImGui::PushItemWidth(ImGui::GetColumnWidth());
    if(ImGui::Button("Play")) {
        if(_pModel) {
            _pModel->GetComponent<component::Animator>()->Play(blendDesc._from,
                                                               Animation::PlayType::Loop);
            _currentBlend = &blendDesc;
        }
    }
    ImGui::PopID();
}
//---------------------------------------------------------------------------
//! このブレンドリストを削除
//---------------------------------------------------------------------------
void AnimationConfigScene::BlendListRemoveButton(const s32 id)
{
    BlendDesc& blendDesc = _blendList[id];
    ImGui::PushID(id);
    ImGui::PushItemWidth(ImGui::GetColumnWidth());
    if(ImGui::Button("Remove")) {
        _blendList.erase(std::remove_if(_blendList.begin(), _blendList.end(),
                                        [&blendDesc](const BlendDesc& b) -> bool {
                                            return &b == &blendDesc;
                                        }),
                         _blendList.end());
    }
    ImGui::PopID();
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void AnimationConfigScene::OnFinalize()
{
}
//---------------------------------------------------------------------------
//! シーンをリセット
//---------------------------------------------------------------------------
void AnimationConfigScene::Reset()
{
    _pEntityMgr->SetSelectedEntity(nullptr);
    if(_pModel) {
        _pEntityMgr->Unregist(_pModel.get());
        _pModel.reset();
    }

    _modelPath      = "";
    _diffuseTexture = "";

    _blendList.clear();
    _animationMapping.clear();
}
//---------------------------------------------------------------------------
//! PopUp Modalの設定
//---------------------------------------------------------------------------
void AnimationConfigScene::SetNextPopUpModal()
{
    ImGui::SetNextWindowSize(MAX_SIZE);
    ImVec2 center = ImGui::GetMainViewport()->GetCenter();
    ImGui::SetNextWindowPos(center, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));
}
//---------------------------------------------------------------------------
//! メニューバー
//---------------------------------------------------------------------------
void AnimationConfigScene::HandleMenuBar()
{
    ImGui::Begin("MainDockspace");
    if(ImGui::BeginMenuBar()) {
        if(ImGui::BeginMenu("File")) {
            // 新規
            if(ImGui::MenuItem(ICON_FA_FILE " New", nullptr)) {
                //ImGui::OpenPopup("New");
                Reset();
            }
            // ファイルを開く
            if(ImGui::MenuItem(ICON_FA_FILE_ALT " Open", nullptr)) {
                ImGuiFileDialog::Instance()->OpenModal("OpenAnim",
                                                       " Choose a File", ".anim",
                                                       ".", "", 1);
                SetNextPopUpModal();
            }
            // 保存
            if(ImGui::MenuItem(ICON_FA_SAVE " Save", nullptr)) {
                ImGuiFileDialog::Instance()->OpenModal("SaveAnim",
                                                       " Choose Directory", ".anim",
                                                       ".", "", 1, nullptr,
                                                       ImGuiFileDialogFlags_ConfirmOverwrite);
                SetNextPopUpModal();
            }
            ImGui::EndMenu();
        }
        ImGui::EndMenuBar();
    }
    ImGui::End();

    //HandleNewButton();
    HandleOpenButton();
    HandleSaveButton();
}
//---------------------------------------------------------------------------
//! 左側のパネル
//---------------------------------------------------------------------------
void AnimationConfigScene::HandleConfigLeftPanel()
{
    // 左パネル
    f32 width = ImGui::GetContentRegionAvailWidth() / 2.0f;
    ImGui::BeginChild("LeftPanel", { width, 0.0f });
    HandleChooseModel();
    ImGui::Separator();
    HandleChooseExtenalTexture();
    ImGui::Separator();
    HandleChooseAnimations();
    ImGui::EndChild();
}
//---------------------------------------------------------------------------
//! 右側のパネル
//---------------------------------------------------------------------------
void AnimationConfigScene::HandleConfigRightPanel()
{
    // 右パネル
    ImGui::BeginChild("RightPanel");
    HandleBlendPanel();
    ImGui::EndChild();
}
//---------------------------------------------------------------------------
//! AnimationConfigSceneシーン
//---------------------------------------------------------------------------
template<>
std::unique_ptr<BaseScene> createNewScene<AnimationConfigScene>()
{
    return std::make_unique<AnimationConfigScene>();
}

}   // namespace scene
