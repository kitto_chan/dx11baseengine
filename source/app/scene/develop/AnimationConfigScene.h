﻿//---------------------------------------------------------------------------
//!	@file	AnimationConfigScene.h
//!	@brief	アニメション設定シーン
//---------------------------------------------------------------------------
#pragma once
#include "scene/BaseScene.h"

#include "renderer/animation.h"

namespace entity::go {
class GameObject;

namespace ui {
class Sprite;
}
}   // namespace entity::go

namespace scene {
class AnimationConfigScene final : public BaseScene
{
public:
    AnimationConfigScene();    //! コンストラクター
    ~AnimationConfigScene();   //! デストラクター
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
protected:
private:
    struct BlendDesc
    {
        std::string _from;
        std::string _to;
        f32         _blendTime = 0.25f;
    };

    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    std::string _modelPath      = "";   //!< 現在のモデルのパース
    std::string _diffuseTexture = "";   //!< 外部からdiffuseTextureを読み込む

    //!< アニメションのマッピング
    //!< @notify mapを使うはずですが、GUIでキーは変更できるので、vector<pair>にします
    std::vector<std::pair<std::string, std::string>> _animationMapping;         //!< アニメションのマッピング
    std::vector<BlendDesc>                           _blendList;                //!< ブレンドリスト
    BlendDesc*                                       _currentBlend = nullptr;   //!< 選ばれたブレんどデータ

    shr_ptr<gameobject::GameObject> _pModel;   //!< モデル

    static constexpr char* NOT_SELECTED = u8"未選択";
    const ImVec2           MAX_SIZE     = ImVec2(960.f, 540.0f);                      // The full display area
    const ImVec2           MIN_SIZE     = { MAX_SIZE.x * 0.7f, MAX_SIZE.y * 0.7f };   // Half the display area
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
    //----------
    // 継承
    bool OnInit() override;                                                                           //!< 初期化
    void OnUpdate() override;                                                                         //!< 更新
    void OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture) override;   //!< 描画
    void OnRenderImgui() override;                                                                    //!< Imgui描画
    void OnFinalize() override;                                                                       //!< 解放

    //----------
    void Reset();   //!< シーンをリセット

    //============================================================================
    // Uiに関して
    //============================================================================
    void SetNextPopUpModal();   //!< Modalの設定

    void HandleMenuBar();   //!< メニューバー

    // アニメション設定のパネル
    void HandleConfigLeftPanel();    //!< 左側のパネル
    void HandleConfigRightPanel();   //!< 右側のパネル

    //----------
    // モデル・アニメションを選択する
    void HandleChooseModel();            //!< モデルを選択
    void HandleChooseExtenalTexture();   //!< 外部のから読み込むのテクスチャーを選択
    void HandleChooseAnimations();       //!< アニメションを選択
    void HandleBlendPanel();             //!< ブレンド設定のパネル

    //----------
    // メニューバーボタン
    void HandleNewButton();    //!< 新規アニメション
    void HandleOpenButton();   //!< アニメション設定を読み込む
    void HandleSaveButton();   //!< アニメション設定のJsonを生成

    //----------
    // ブレンドリスト UI　（右パネル）
    //!< Fromアニメションコンボリスト
    void BlendListComboFrom(const s32 id, BlendDesc& blendDesc);
    //!< To アニメションコンボリスト
    void BlendListComboTo(const s32 id, BlendDesc& blendDesc);
    //!< ブレンドタイムを設定
    void BlendListBlendTime(const s32 id, BlendDesc& blendDesc);
    //!< 選択したブレンドアニメションを再生
    void BlendListPlayButton(const s32 id, BlendDesc& blendDesc);
    //!< このブレンドリストを削除
    void BlendListRemoveButton(const s32 id);
};
}   // namespace scene
