﻿//---------------------------------------------------------------------------
//!	@file	GameDef.h
//!	@brief	ゲームの定義 enumとか、structとか
//---------------------------------------------------------------------------
#pragma once

namespace scene {
enum class EScene
{
    Title,
    Tutorial,
    Game,
    Clear,
    Over,

    //----------
    //開発用
    AnimationEditor,

    //----------
    // Test用
    PBRLighting
};
}

namespace gameApp {

enum class GameLevel
{
    Level_1,
    Level_2,
    Level_3,
    Level_4,
    Level_MAX,
};

enum EntityType
{
    EntityType_Entity     = 0,
    EntityType_GameObject = 1 << 0,
    EntityType_Character  = 1 << 1,
};

}   // namespace gameApp
