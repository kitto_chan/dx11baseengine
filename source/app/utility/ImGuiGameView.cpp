﻿//---------------------------------------------------------------------------
//!	@file	ImguiWidget.cpp
//!	@brief	自分の描画スタイル
//---------------------------------------------------------------------------
#include "ImGuiGameView.h"
#include "imgui/imgui.h"
#include "imgui/imgui_internal.h"
#include "IconFont/IconsFontAwesome5.h"

#include "manager/EntityManager.h"
#include "entity/Entity.h"

#include "entity/gameObject/GameObject.h"
#include "component/Transform.h"

#include <filesystem>
#include <imgui/imgui_internal.h>
#include <imgui/misc/cpp/imgui_stdlib.h>
#include <ImguiFileDialog/ImGuiFileDialog.h>

namespace imgui {
//----------
// Hierarchy用
namespace hierarchy {
ImGuiTreeNodeFlags baseNodeFlags = ImGuiTreeNodeFlags_None | ImGuiTreeNodeFlags_SpanAvailWidth;
s32                selectionMask = (1 << 2);
s32                selectedId    = 0;
}   // namespace hierarchy

//---------------------------------------------------------------------------
//! ImguiGuizmoViewを描画する
//---------------------------------------------------------------------------
void ImguiGuizmoView(const matrix& viewMat, const matrix& projMat, const raw_ptr<component::Transform>& transCom)
{
    if(!transCom) return;

    ImGui::Begin("Scene");
    ImGuizmo::SetOrthographic(false);
    ImGuizmo::SetDrawlist(ImGui::GetForegroundDrawList());
    ImGuizmo::SetRect(ImGui::GetWindowPos().x, ImGui::GetWindowPos().y,
                      ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y);

    matrix worldMat = transCom->GetLocalToWorldMatrix();   // 子行列->親行列

    // cast matrix -> float[16]
    float* viewF16  = math::CastTof32Matrix(viewMat);
    float* projF16  = math::CastTof32Matrix(projMat);
    float* worldF16 = math::CastTof32Matrix(worldMat);

    //  GuizmoのManipulate
    switch(SystemSettingsMgr()->GetImGuizmoAction()) {
        case sys::ImGuizmoAction::Transform:
            ImGuizmo::Manipulate(viewF16, projF16, ImGuizmo::OPERATION::TRANSLATE,
                                 ImGuizmo::WORLD, worldF16);
            break;
        case sys::ImGuizmoAction::Rotate:
            ImGuizmo::Manipulate(viewF16, projF16, ImGuizmo::OPERATION::ROTATE,
                                 ImGuizmo::WORLD, worldF16);
            break;
        case sys::ImGuizmoAction::Scale:
            ImGuizmo::Manipulate(viewF16, projF16, ImGuizmo::OPERATION::SCALE,
                                 ImGuizmo::WORLD, worldF16);
            break;
        default:
            ASSERT_MESSAGE(false, "不可能なEnum");
            break;
    }

    // Guizmo の判定
    if(ImGuizmo::IsUsing()) {
        float matrixTranslation[3], matrixRotation[3], matrixScale[3];

        // もし対象は子物件は子行列計算必要がある
        if(transCom->GetParent()) {
            matrix movedMat = math::CastToMatrix(worldF16);

            matrix childMat = mul(worldMat, hlslpp::inverse(movedMat));                     // 移動量計算
            childMat        = mul(transCom->GetLocalMatrix(), hlslpp::inverse(childMat));   // 子行列に変換

            float* childF16 = math::CastTof32Matrix(childMat);

            //　行列を分解
            ImGuizmo::DecomposeMatrixToComponents(childF16, matrixTranslation, matrixRotation, matrixScale);
        }
        // 親対象(そのまま)
        else {
            //　行列を分解
            ImGuizmo::DecomposeMatrixToComponents(worldF16, matrixTranslation, matrixRotation, matrixScale);
        }

        // 回転はオイラー角保存してるので
        float3 rot = transCom->GetRotation();
        rot.x      = math::D2R(matrixRotation[0]);
        rot.y      = math::D2R(matrixRotation[1]);
        rot.z      = math::D2R(matrixRotation[2]);

        transCom->SetPosition(float3(matrixTranslation[0], matrixTranslation[1], matrixTranslation[2]));
        transCom->SetScale(float3(matrixScale[0], matrixScale[1], matrixScale[2]));
        transCom->SetRotation(rot);
    }
    ImGui::End();
}
//---------------------------------------------------------------------------
//! ImGuiのAction描画
//---------------------------------------------------------------------------
void ImguiActionBar()
{
    // Padding
    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, { 0.5f, 0.5f });
    ImGui::Begin("Actions");

    // アクションアイコンは真ん中で描画
    {
        ImGui::SameLine(ImGui::GetWindowSize().x * 0.5f + 15.0f);
        std::string actionIcon;
        if(SystemSettingsMgr()->GetSystemState() == sys::SystemState::Play) {
            actionIcon = ICON_FA_PLAY;
        }
        else {
            actionIcon = ICON_FA_PAUSE;
        }

        if(ImGui::Button(actionIcon.c_str(), { 30.0f, 30.0f })) {
            SystemSettingsMgr()->SwapSysState();
            manager::EffekseerMgr()->SwapIsPlayAllEffect();
        };
    }

    // imguizmo 描画
    {
        static std::string type[3]    = { "Transform", "Rotation", "Scale" };
        s32                selected   = static_cast<s32>(SystemSettingsMgr()->GetImGuizmoAction());
        f32                alignRight = ImGui::GetWindowSize().x * 0.8f;
        ImGui::SameLine(alignRight);
        ImGui::Text("");
        for(int n = 0; n < 3; n++) {
            ImGui::SameLine();
            if(ImGui::Selectable(type[n].c_str(), selected == n, ImGuiSelectableFlags_None, ImVec2(80, 10)))
                SystemSettingsMgr()->SetImGuizmoAction(sys::ImGuizmoAction(n));
        }
    }

    ImGui::PopStyleVar();
    ImGui::End();
}
//---------------------------------------------------------------------------
//! ImGuiのHierarchyを描画
//! Recursive(リカーシブ)関数
//---------------------------------------------------------------------------
void ImGuiHierarchy(const std::vector<raw_ptr<entity::Entity>>& entts, manager::EntityManager& enttMgr, bool isChild)
{
    using namespace hierarchy;
    ImGui::Begin("Hierarchy");
    s32 nodeClicked = -1;

    for(s32 i = 0; i < entts.size(); i++) {
        //if(!entts[i]->IsReady()) continue;

        // entity -> gameObject
        gameobject::GameObject* parentObj = dynamic_cast<gameobject::GameObject*>(entts[i].get());

        //　(リカーシブ)関数なのでChildの2次描画しないように
        if(!isChild && parentObj->HasParent()) continue;

        ImGuiTreeNodeFlags nodeFlags  = baseNodeFlags;                                        //!<　デフォルトフラッグ
        const bool         isSelected = (selectionMask & (1 << i)) != 0 && selectedId == i;   //!< SelectMask
        std::string        itemId     = entts[i]->GetName();                                  //!< ネームはIDとして

        // 子物件が選ばないように
        // 子物件を選んだときは色変わるだけ
        if(!isChild && isSelected) {
            nodeFlags |= ImGuiTreeNodeFlags_Selected;
        }

        bool node_open = false;
        // もし子物件あるなら、子物件のTreeNodeも作ります
        if(!parentObj->HasChild()) {
            //--------------------------------
            // 選択オブジェクトの色処理
            //--------------------------------
            // push color
            if(enttMgr.GetSelectedEntities() == parentObj) {
                ImGui::PushStyleColor(ImGuiCol_Text, { 1, 0, 0, 1 });
            }
            else if(!entts[i]->IsReady()) {
                ImGui::PushStyleColor(ImGuiCol_Text, { 0.5f, 0.5f, 0.5, 1 });
            }
            node_open = ImGui::TreeNodeEx((void*)(intptr_t)i, nodeFlags, itemId.c_str());

            // pop color
            if(enttMgr.GetSelectedEntities() == parentObj ||
               !entts[i]->IsReady()) {
                ImGui::PopStyleColor();
            }

            //---------------------------------
            if(ImGui::IsItemClicked()) {
                if(!isChild) {
                    selectedId  = i;
                    nodeClicked = i;
                }
                enttMgr.SetSelectedEntity(parentObj);
            }

            // 開いたら子のTreeNode生成する
            if(node_open) {
                ImGuiHierarchy(parentObj->GetChildEntts(), enttMgr, true);
                ImGui::TreePop();
            }
        }
        else {
            // The only reason we use TreeNode at all is to allow selection of the leaf. Otherwise we can
            // use BulletText() or advance the cursor by GetTreeNodeToLabelSpacing() and call Text().
            // もしも子物件ないなら現在のTreeNodeで描画する
            nodeFlags |= ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen;   // ImGuiTreeNodeFlags_Bullet

            //--------------------------------
            // 選択オブジェクトの色処理
            //--------------------------------
            // push color
            if(enttMgr.GetSelectedEntities() == parentObj) {
                ImGui::PushStyleColor(ImGuiCol_Text, { 1, 0, 0, 1 });
            }
            else if(!entts[i]->IsReady()) {
                ImGui::PushStyleColor(ImGuiCol_Text, { 0.5f, 0.5f, 0.5, 1 });
            }
            node_open = ImGui::TreeNodeEx((void*)(intptr_t)i, nodeFlags, itemId.c_str());

            // pop color
            if(enttMgr.GetSelectedEntities() == parentObj ||
               !entts[i]->IsReady()) {
                ImGui::PopStyleColor();
            }

            if(ImGui::IsItemClicked()) {
                if(!isChild) {
                    selectedId  = i;
                    nodeClicked = i;
                }

                if(enttMgr.GetSelectedEntities() == parentObj) {
                    enttMgr.SetSelectedEntity(nullptr);
                }
                else {
                    enttMgr.SetSelectedEntity(parentObj);
                }
            }
        }
    }

    // セレクション更新
    if(!isChild && nodeClicked != -1) {
        // Update selection state
        // (process outside of tree loop to avoid visual inconsistencies during the clicking frame)
        selectionMask = (1 << nodeClicked);   // Click to single-select
    }
    ImGui::End();
}
//---------------------------------------------------------------------------
//! ヒントメーセジを描画する
//---------------------------------------------------------------------------
void ImGuiHintOverlay()
{
    static int       corner       = 0;
    ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav;
    if(corner != -1) {
        const float          PAD       = 10.0f;
        const ImGuiViewport* viewport  = ImGui::GetMainViewport();
        ImVec2               work_pos  = viewport->WorkPos;   // Use work area to avoid menu-bar/task-bar, if any!
        ImVec2               work_size = viewport->WorkSize;
        ImVec2               window_pos, window_pos_pivot;
        window_pos.x       = (corner & 1) ? (work_pos.x + work_size.x - PAD) : (work_pos.x + PAD);
        window_pos.y       = (corner & 2) ? (work_pos.y + work_size.y - PAD) : (work_pos.y + PAD);
        window_pos_pivot.x = (corner & 1) ? 1.0f : 0.0f;
        window_pos_pivot.y = (corner & 2) ? 1.0f : 0.0f;
        ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
        ImGui::SetNextWindowViewport(viewport->ID);
        window_flags |= ImGuiWindowFlags_NoMove;
    }

    ImGui::SetNextWindowBgAlpha(0.35f);   // Transparent background
    if(ImGui::Begin("overlay", nullptr, window_flags)) {
        ImGui::Text(u8"F1キーでエディターモードのオン/オフが変更できる\n");
        ImGui::Separator();
        ImGui::Text(u8"F2キーでマウスカーソルの表示/非表示が変更できる\n");
        ImGui::Separator();
        ImGui::Text(u8"F3キーでエディターモードで当たり判定ボックスの表示/非表示が変更できる\n");
    }
    ImGui::End();
}
//---------------------------------------------------------------------------
//! 画像選択
//---------------------------------------------------------------------------
void ImguiImageSelecter(std::string_view name, shr_ptr<gpu::Texture> texture, bool isChanageable)
{
    if(!texture) return;

    std::string modalName = "Modal" + std::string(name);

    ImVec2   pos = ImGui::GetCursorScreenPos();
    ImGuiIO& io  = ImGui::GetIO();

    static constexpr const f32 Tex_Width  = 32.f;
    static constexpr const f32 Tex_Height = 32.f;

    static constexpr const f32 Scale_Region = 16.f;
    static constexpr const f32 Zoom         = 16.0f;

    // 画像描画
    ImGui::Image(static_cast<ID3D11ShaderResourceView*>(*texture), ImVec2(Tex_Width, Tex_Height));

    // マウスが画像の上で置くと、さらに詳しいデータを表示する
    if(ImGui::IsItemHovered()) {
        ImGui::BeginTooltip();
        float region_x = io.MousePos.x - pos.x - Scale_Region * 0.5f;
        float region_y = io.MousePos.y - pos.y - Scale_Region * 0.5f;
        if(region_x < 0.0f) {
            region_x = 0.0f;
        }
        else if(region_x > Tex_Width - Scale_Region) {
            region_x = Tex_Width - Scale_Region;
        }
        if(region_y < 0.0f) {
            region_y = 0.0f;
        }
        else if(region_y > Tex_Height - Scale_Region) {
            region_y = Tex_Height - Scale_Region;
        }
        ImGui::Text(u8"元画像のサイズ: (%u, %u)", texture->desc().width_, texture->desc().height_);
        ImVec2 uv0 = ImVec2((region_x) / Tex_Width, (region_y) / Tex_Height);
        ImVec2 uv1 = ImVec2((region_x + Scale_Region) / Tex_Width, (region_y + Scale_Region) / Tex_Height);
        ImGui::Image(static_cast<ID3D11ShaderResourceView*>(*texture), ImVec2(Scale_Region * Zoom, Scale_Region * Zoom), uv0, uv1);

        ImGui::EndTooltip();
    }

    if(!isChanageable) return;

    // 画像変更処理
    ImGui::SameLine();

    if(ImGui::Button(ICON_FA_FOLDER, ImVec2(Tex_Width, Tex_Height))) {
        ImGuiFileDialog::Instance()->OpenModal(modalName,
                                               "ChooseTexture",
                                               ".png {.png,.PNG,.tga}", ".");
    }
    const ImVec2 MAX_SIZE = ImVec2(960.f, 540.0f);                      // The full display area
    const ImVec2 MIN_SIZE = { MAX_SIZE.x * 0.7f, MAX_SIZE.y * 0.7f };   // Half the display area
    // ファイルダイアログを表示
    if(ImGuiFileDialog::Instance()->Display(modalName,
                                            ImGuiWindowFlags_NoCollapse,
                                            MIN_SIZE,
                                            MAX_SIZE)) {
        // action if OK
        if(ImGuiFileDialog::Instance()->IsOk()) {
            std::string filePathName    = ImGuiFileDialog::Instance()->GetFilePathName();
            u64         currentPathSize = std::filesystem::current_path().string().size() + 1;
            std::string relativePath    = std::string(filePathName).substr(currentPathSize, filePathName.size());

            texture->newPath = (relativePath);
        }
        // close
        ImGuiFileDialog::Instance()->Close();
    }
}
//---------------------------------------------------------------------------
//! Hierarchyスタイルの画像選択ツール
//---------------------------------------------------------------------------
void ImguiHierarchyImageSelecter(std::string_view name, shr_ptr<gpu::Texture> texture, bool isChangeable)
{
    ImguiColumn2FormatBegin(name);
    ImguiImageSelecter(name, texture, isChangeable);
	ImguiColumn2FormatEnd();
}
}   // namespace imgui
