﻿//---------------------------------------------------------------------------
//!	@file	ImguiWidget.h
//!	@brief	自分の描画スタイル
//---------------------------------------------------------------------------
#pragma once

namespace component {
class Transform;
}

namespace entity {
class Entity;
}

namespace manager {
class EntityManager;
}

namespace imgui {
//! ImguiGuizmoViewを描画する
void ImguiGuizmoView(const matrix& view, const matrix& proj, const raw_ptr<component::Transform>& transCom);

//! ActionBar描画
void ImguiActionBar();

//! Hierarchyを描画
void ImGuiHierarchy(const std::vector<raw_ptr<entity::Entity>>& entts, manager::EntityManager& enttMgr, bool isChild);

//! ヒントメーセジを描画する
void ImGuiHintOverlay();

//! 画像選択
void ImguiImageSelecter(std::string_view name, shr_ptr<gpu::Texture> texture, bool isChangeable = true);

// Hierarchyスタイルの画像選択ツール
void ImguiHierarchyImageSelecter(std::string_view name, shr_ptr<gpu::Texture> texture, bool isChangeable = true);
}   // namespace imgui
