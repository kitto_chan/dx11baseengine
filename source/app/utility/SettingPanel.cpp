﻿//---------------------------------------------------------------------------
//!	@file	SettingPanel.h
//!	@brief	ゲーム設定のパネル
//---------------------------------------------------------------------------
#include "SettingPanel.h"
#include <fstream>
#include <filesystem>
#include "component/Transform.h"
#include "entity/gameObject/ui/Sprite.h"
#include "entity/gameObject/ui/AdvancedSprite.h"
#include "effect/Common/CommonEffect.h"
#include "manager/EntityManager.h"
namespace settings {
struct BlurCB
{
    f32 power;   // ブラーの値
    u32 padding[3];
};

BlurCB _blurCB{};
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
SettingPanel::SettingPanel()
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
SettingPanel::~SettingPanel()
{
}

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool SettingPanel::Init(raw_ptr<manager::EntityManager> enttMgr)
{
    _fontRender = std::make_unique<renderer::FontRenderer>();
    _fontRender->Init();

    // SettingPanelためのエンティティマネージャー
    _settingPanelEnttMgr = std::make_unique<manager::EntityManager>(false);
    _settingPanel        = new ui::AdvancedSprite("SystemPanel");
    _settingPanel->SetCurrentEffect(ui::AdvancedSprite::eEffect::Disslove);

    _settingPanel->GetComponent<component::Transform>()->SetPosition(float3(sys::WINDOW_CENTER_X, sys::WINDOW_CENTER_Y, 0.0f));
    _settingPanel->GetComponent<component::Transform>()->SetScale(float3(0.7f, 0.7f, 0.0f));
    _settingPanel->SetSamplerState(dx11::eSamplerState::LinearWrap);

    // 画像生成
    //u32 width     = gpu::swapChain()->backBuffer()->desc().width_;
    //u32 height    = gpu::swapChain()->backBuffer()->desc().height_;
    u32 width     = sys::WINDOW_WIDTH;
    u32 height    = sys::WINDOW_HEIGHT;
    _panelTexture = gpu::createTargetTexture(width, height, DXGI_FORMAT_R8G8B8A8_UNORM);

    // ノイズテクスチャー
    _noiseTexture = gpu::createTextureFromFile("gameSource/noiseTexture/PerlinNoise.png");

    _shaderVs_2D      = gpu::createShader("framework/vs_quad2d.fx", "main", "vs_5_0");
    _shaderPs_Texture = gpu::createShader("framework/ps_texture.fx", "main", "ps_5_0");

    _blurPs = gpu::createShader("gameSource/blur/Mosaic_Ps.fx", "PS", "ps_5_0");   //!< ブラー

    _pIntputLayout = gpu::createInputLayout(vertex::VertexPosUv::inputLayout,
                                            std::size(vertex::VertexPosUv::inputLayout));   // 2D入力レイアウト

    _blurCB.power = 0.0f;

    shr_ptr<gpu::Texture> soundBarBg    = gpu::createTextureFromFile("ui/ProgressBar/BarV8_Bar.png");
    shr_ptr<gpu::Texture> soundBarValue = gpu::createTextureFromFile("ui/ProgressBar/BarV8_ProgressBar.png");
    //-------------------------------------------
    // フォントを読み込む
    _fontJiyucho = _fontRender->CreateSpriteFont(L"font/Jiyucho16.spritefont");
    _fontMsFont  = _fontRender->CreateSpriteFont(L"font/Ms36B.spritefont");

    LoadControllerIconInDir("ui/control/mouse/fill");
    LoadControllerIconInDir("ui/control/keyboard/fill");
    LoadControllerIconInDir("ui/control/gamepad/fill");

    _keyConfigPanel1 = new gameobject::GameObject("KeyConfigPanel1");
    _keyConfigPanel2 = new gameobject::GameObject("KeyConfigPanel2");
    _soundPanel      = new gameobject::GameObject("SoundPanel");
    _settingPanelEnttMgr->Regist(_keyConfigPanel1.get());
    _settingPanelEnttMgr->Regist(_keyConfigPanel2.get());
    _settingPanelEnttMgr->Regist(_soundPanel.get());

    // 攻撃
    {
        ui::Sprite* icon = new ui::Sprite("Icon Mouse Left", float3(240.0f, 300.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["mouse_left"]);
        icon->SetParent(_keyConfigPanel1);
        _settingPanelEnttMgr->Regist(icon);
    }
    {
        ui::Sprite* icon = new ui::Sprite("Button X", float3(1220.0f, 300.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["button_x"]);
        icon->SetParent(_keyConfigPanel1);
        _settingPanelEnttMgr->Regist(icon);
    }

    // カメラ操作
    {
        ui::Sprite* icon = new ui::Sprite("Mouse", float3(240.0f, 420.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["mouse_none"]);
        icon->SetParent(_keyConfigPanel1);
        _settingPanelEnttMgr->Regist(icon);
    }
    {
        ui::Sprite* icon = new ui::Sprite("JoyStick R", float3(1220.0f, 420.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["joystick3_right"]);
        icon->SetParent(_keyConfigPanel1);
        _settingPanelEnttMgr->Regist(icon);
    }

    // 選択
    {
        ui::Sprite* icon = new ui::Sprite("Keyboard Space", float3(240.0f, 540.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["keyboard_Space"]);
        icon->SetParent(_keyConfigPanel1);
        _settingPanelEnttMgr->Regist(icon);
    }
    {
        ui::Sprite* icon = new ui::Sprite("Button B", float3(1220.0f, 540.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["button_b"]);
        icon->SetParent(_keyConfigPanel1);
        _settingPanelEnttMgr->Regist(icon);
    }

    // 移動
    {
        ui::Sprite* icon = new ui::Sprite("KeyBoard AWSD", float3(240.0f, 300.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["awsd"]);
        icon->SetParent(_keyConfigPanel2);
        _settingPanelEnttMgr->Regist(icon);
    }
    {
        ui::Sprite* icon = new ui::Sprite("JoyStick R", float3(1220.0f, 300.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["joystick3_right"]);
        icon->SetParent(_keyConfigPanel2);
        _settingPanelEnttMgr->Regist(icon);
    }

    // ダッシュ
    {
        ui::Sprite* icon = new ui::Sprite("LShift", float3(240.0f, 420.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["keyboard_LCtrl"]);
        icon->SetParent(_keyConfigPanel2);
        _settingPanelEnttMgr->Regist(icon);
    }
    {
        ui::Sprite* icon = new ui::Sprite("RB", float3(1220.0f, 420.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["rb"]);
        icon->SetParent(_keyConfigPanel2);
        _settingPanelEnttMgr->Regist(icon);
    }

    // ジャンプ
    {
        ui::Sprite* icon = new ui::Sprite("Keyboard Space", float3(240.0f, 540.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["keyboard_Space"]);
        icon->SetParent(_keyConfigPanel2);
        _settingPanelEnttMgr->Regist(icon);
    }
    {
        ui::Sprite* icon = new ui::Sprite("Button A", float3(1220.0f, 540.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["button_a"]);
        icon->SetParent(_keyConfigPanel2);
        _settingPanelEnttMgr->Regist(icon);
    }

    // 回避
    {
        ui::Sprite* icon = new ui::Sprite("Keyboard LShift", float3(240.0f, 660.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["keyboard_LShift"]);
        icon->SetParent(_keyConfigPanel2);
        _settingPanelEnttMgr->Regist(icon);
    }
    {
        ui::Sprite* icon = new ui::Sprite("Button B", float3(1220.0f, 660.0f, 1.0f));
        icon->SetTexture(_controllericonTextures["button_b"]);
        icon->SetParent(_keyConfigPanel2);
        _settingPanelEnttMgr->Regist(icon);
    }
    // next
    {
        ui::Sprite* icon = new ui::Sprite("Keyboard LShift", float3(1230.0f, 750.0f, 1.0f), float3(1.0f, 0.8f, 0.8f));
        icon->SetTexture(_controllericonTextures["keyboard_Space"]);
        _settingPanelEnttMgr->Regist(icon);
    }
    {
        ui::Sprite* icon = new ui::Sprite("Button B", float3(1300.0f, 750.0f, 1.0f), float3(0.7f, 0.7f, 0.7f));
        icon->SetTexture(_controllericonTextures["button_b"]);
        _settingPanelEnttMgr->Regist(icon);
    }

    // Sound Panel Asset
    // BGM
    {
        ui::Sprite* icon = new ui::Sprite("ProgressBarBG", float3(sys::WINDOW_CENTER_X + 75, 300.f, 1.0f), float3(2.0f, 1.0f, 1.0f));
        icon->SetTexture(soundBarBg);
        icon->SetParent(_soundPanel);
        _settingPanelEnttMgr->Regist(icon);
    }
    {
        _soundBGMValue = new ui::AdvancedSprite("ProgressBarValue", float3(sys::WINDOW_CENTER_X + 75, 300.f, 1.0f), float3(2.0f, 1.0f, 1.0f));
        _soundBGMValue->SetTexture(soundBarValue);
        _soundBGMValue->SetCurrentEffect(ui::AdvancedSprite::eEffect::ProcessBar);
        _soundBGMValue->SetParent(_soundPanel);
        _settingPanelEnttMgr->Regist(_soundBGMValue.get());
    }
    //SE
    {
        ui::Sprite* icon = new ui::Sprite("ProgressBarBG", float3(sys::WINDOW_CENTER_X + 75, 420.f, 1.0f), float3(2.0f, 1.0f, 1.0f));
        icon->SetTexture(soundBarBg);
        icon->SetParent(_soundPanel);
        _settingPanelEnttMgr->Regist(icon);
    }
    {
        _soundSEValue = new ui::AdvancedSprite("ProgressBarValue", float3(sys::WINDOW_CENTER_X + 75, 420.f, 1.0f), float3(2.0f, 1.0f, 1.0f));
        _soundSEValue->SetTexture(soundBarValue);
        _soundSEValue->SetCurrentEffect(ui::AdvancedSprite::eEffect::ProcessBar);
        _soundSEValue->SetParent(_soundPanel);
        _settingPanelEnttMgr->Regist(_soundSEValue.get());
    }

    //_settingPanelEnttMgr->Regist(_settingPanel.get());
    enttMgr->Regist(_settingPanel.get());
    DisableAllPanel();

    _settingPanelEnttMgr->Init();
    _settingPanel->Disable();
    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void SettingPanel::Update()
{
    if(!_settingPanel->IsReady()) return;
    HandleAnim();

    _settingPanelEnttMgr->Update();

    if(_animState != EState_Anim::None) return;

    switch(_currentState) {
        case EState::OpenChoose:
        {
            DisableAllPanel();

            s32 curr = static_cast<s32>(_menuState);
            if(input::IsPressSelectKeyForward()) {
                curr++;
                if(curr >= static_cast<s32>(EState_Menu::Count)) {
                    curr = 0;
                }
                _menuState = static_cast<EState_Menu>(curr);
            }
            else if(input::IsPressSelectKeyBackward()) {
                curr--;
                if(curr < 0) {
                    curr = static_cast<s32>(EState_Menu::Count) - 1;
                }
                _menuState = static_cast<EState_Menu>(curr);
            }

            if(input::IsPressSelectKeyConform()) {
                switch(_menuState) {
                    case EState_Menu::Resume:
                        CloseSettingPanel();
                        break;
                    case EState_Menu::Bgm:
                        Reset();
                        _currentState = EState::SoundConfig;
                        _soundPanel->Enable();
                        break;
                    case EState_Menu::KeyConfing:
                        Reset();
                        _currentState = EState::KeyConfigPage1;
                        _keyConfigPanel1->Enable();
                        break;
                }
            }
        } break;

        case EState::KeyConfigPage1:
            if(!(input::IsPressSelectKeyRight() || input::IsPressSelectKeyConform()))
                return;

            _currentState = EState::KeyConfigPage2;
            DisableAllPanel();
            _keyConfigPanel2->Enable();
            break;
        case EState::KeyConfigPage2:
            if(input::IsPressSelectKeyLeft()) {
                Reset();
                _currentState = EState::KeyConfigPage1;
                _keyConfigPanel1->Enable();
            }

            if(input::IsPressSelectKeyConform()) {
                CloseSettingPanel();
            }
            break;
        case EState::SoundConfig:
        {
            static constexpr f32 Sound_Scaler = 0.01f;
            switch(_soundState) {
                case EState_Sound::BGM:
                    if(input::IsPressingSelectKeyRight()) {
                        manager::AudioMgr()->AddBgmVolume(Sound_Scaler);
                    }
                    if(input::IsPressingSelectKeyLeft()) {
                        manager::AudioMgr()->AddBgmVolume(-Sound_Scaler);
                    }
                    _soundBGMValue->SetProcessFillAmount(manager::AudioMgr()->GetBgmVolume());
                    break;
                case EState_Sound::SE:
                    if(input::IsPressingSelectKeyRight()) {
                        manager::AudioMgr()->AddSEVolume(Sound_Scaler);
                    }
                    if(input::IsPressingSelectKeyLeft()) {
                        manager::AudioMgr()->AddSEVolume(-Sound_Scaler);
                    }
                    _soundBGMValue->SetProcessFillAmount(manager::AudioMgr()->GetSEVolume());
                case EState_Sound::Back:
                    if(input::IsPressSelectKeyConform()) {
                        CloseSettingPanel();
                    }
                    break;
            }
            _soundBGMValue->SetProcessFillAmount(manager::AudioMgr()->GetBgmVolume());
            _soundSEValue->SetProcessFillAmount(manager::AudioMgr()->GetSEVolume());

            s32 curr = static_cast<s32>(_soundState);
            if(input::IsPressSelectKeyDown()) {
                curr++;
                if(curr >= static_cast<s32>(EState_Sound::Count)) {
                    curr = 0;
                }
                _soundState = static_cast<EState_Sound>(curr);
            }
            if(input::IsPressSelectKeyUp()) {
                curr--;
                if(curr < 0) {
                    curr = static_cast<s32>(EState_Sound::Count) - 1;
                }
                _soundState = static_cast<EState_Sound>(curr);
            }
        } break;
    }
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void SettingPanel::Render(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    if(!_settingPanel->IsReady()) return;
    //----------------------------------------------------------
    // ポスト処理
    //----------------------------------------------------------
    gpu::setRenderTarget(0, _panelTexture);                           // ポスト描画開始
    gpu::setDepthStencil(nullptr);                                    // デプステクスチャクリア
    gpu::clearColor(_panelTexture, float4(0.0f, 0.0f, 0.0f, 0.8f));   // カラーバッファ
    _settingPanelEnttMgr->Render();
    // 文字描画
    _fontRender->Begin();
    {
        switch(_currentState) {
            case EState::OpenChoose:
            {
                _fontRender->ChangeFont(_fontMsFont.get());
                _fontRender->DrawString(float2(sys::WINDOW_CENTER_X, 100), L"メニュー", WHITE, 1.8f);
                // オプション
                float4 color = _menuState == EState_Menu::Resume ? GREEN : WHITE;
                _fontRender->DrawString(float2(sys::WINDOW_CENTER_X, 300.f), L"ゲームに戻る", color, 1.0f);

                color = _menuState == EState_Menu::Bgm ? GREEN : WHITE;
                _fontRender->DrawString(float2(sys::WINDOW_CENTER_X, 420.f), L"音声調整", color, 1.0f);

                color = _menuState == EState_Menu::KeyConfing ? GREEN : WHITE;
                _fontRender->DrawString(float2(sys::WINDOW_CENTER_X, 540.f), L"操作方法", color, 1.0f);
                _fontRender->DrawString(float2(1380.f, 750.f), L"確認", WHITE, 0.7f);
            } break;
            case EState::KeyConfigPage1:
                // タイトル
                _fontRender->ChangeFont(_fontMsFont.get());
                _fontRender->DrawString(float2(sys::WINDOW_CENTER_X, 100), L"操作方法", WHITE, 1.8f);

                _fontRender->DrawString(float2(sys::WINDOW_CENTER_X, 300.f), L"攻撃", WHITE, 1.0f);
                _fontRender->DrawString(float2(sys::WINDOW_CENTER_X, 420.f), L"カメラ操作", WHITE, 1.0f);
                _fontRender->DrawString(float2(sys::WINDOW_CENTER_X, 540.f), L"選択", WHITE, 1.0f);

                _fontRender->DrawString(float2(1380.f, 750.f), L"next", WHITE, 0.7f);
                break;
            case EState::KeyConfigPage2:
                // タイトル
                _fontRender->ChangeFont(_fontMsFont.get());
                _fontRender->DrawString(float2(sys::WINDOW_CENTER_X, 100), L"操作方法", WHITE, 1.8f);
                // オプション
                _fontRender->DrawString(float2(sys::WINDOW_CENTER_X, 300.f), L"移動", WHITE, 1.0f);
                _fontRender->DrawString(float2(sys::WINDOW_CENTER_X, 420.f), L"ダッシュ", WHITE, 1.0f);
                _fontRender->DrawString(float2(sys::WINDOW_CENTER_X, 540.f), L"ジャンプ", WHITE, 1.0f);
                _fontRender->DrawString(float2(sys::WINDOW_CENTER_X, 660.f), L"回避", WHITE, 1.0f);

                _fontRender->DrawString(float2(1380.f, 750.f), L"確認", WHITE, 0.7f);
                break;
            case EState::SoundConfig:
                // タイトル
                _fontRender->ChangeFont(_fontMsFont.get());
                _fontRender->DrawString(float2(sys::WINDOW_CENTER_X, 100), L"サウンド", WHITE, 1.8f);

                float4 color = _soundState == EState_Sound::BGM ? GREEN : WHITE;
                _fontRender->DrawString(float2(sys::WINDOW_CENTER_X / 2.f, 300.f), L"BGM", color, 1.0f);

                color = _soundState == EState_Sound::SE ? GREEN : WHITE;
                _fontRender->DrawString(float2(sys::WINDOW_CENTER_X / 2.f, 420.f), L"SE", color, 1.0f);

                color = _soundState == EState_Sound::Back ? GREEN : WHITE;
                _fontRender->DrawString(float2(sys::WINDOW_CENTER_X, 540.f), L"戻る", color, 1.0f);

                _fontRender->DrawString(float2(1380.f, 750.f), L"確認", WHITE, 0.7f);
                break;
        }
    }

    _fontRender->End();
    _fontRender->Render();
    gpu::ps::setTexture(0, nullptr);
    gpu::setRenderTarget(0, colorTexture);
    gpu::setDepthStencil(depthTexture);

    _settingPanel->SetTexture(_panelTexture);
    _settingPanel->draw = true;
    _settingPanel->OnRender();
    _settingPanel->draw = false;
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void SettingPanel::RenderImgui()
{
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void SettingPanel::Finalize()
{
}
shr_ptr<gpu::Texture> SettingPanel::GetSystemPanelTexture(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    Render(colorTexture, depthTexture);
    return _panelTexture;
}
//---------------------------------------------------------------------------
//! フォルダ中のアイコン画像を読み込む
//---------------------------------------------------------------------------
void SettingPanel::LoadControllerIconInDir(std::string path)
{
    for(auto const& dirEntry : std::filesystem::directory_iterator{ path }) {
        std::string fileName = string::convertUnicodeToUtf8(dirEntry.path().stem().c_str());
        std::string filePath = string::convertUnicodeToUtf8(dirEntry.path().c_str());

        std::shared_ptr<gpu::Texture> icon = gpu::createTextureFromFile(filePath);

        if(!icon) {
            ASSERT_MESSAGE(icon, "Icon not find");
            continue;
        }

        _controllericonTextures[fileName.data()] = std::move(icon);
    }
}
//---------------------------------------------------------------------------
//! 全部のパネルを閉じる
//---------------------------------------------------------------------------
void SettingPanel::DisableAllPanel()
{
    _keyConfigPanel1->Disable();
    _keyConfigPanel2->Disable();
    _soundPanel->Disable();
}
//---------------------------------------------------------------------------
//! パネルのステートをリセット
//---------------------------------------------------------------------------
void SettingPanel::ResetAllPanelState()
{
    _currentState = EState::OpenChoose;
    _menuState    = EState_Menu::Resume;
    _soundState   = EState_Sound::BGM;
    _animState    = EState_Anim::None;
}
//---------------------------------------------------------------------------
//! 設定をリセット
//---------------------------------------------------------------------------
void SettingPanel::Reset()
{
    ResetAllPanelState();
    DisableAllPanel();
}
//---------------------------------------------------------------------------
//! パネルのアニメション処理
//---------------------------------------------------------------------------
void SettingPanel::HandleAnim()
{
    switch(_animState) {
        case EState_Anim::None:
            break;
        case EState_Anim::Open:
            _dissolveValue -= timer::DeltaTime() * 1.0f;
            _settingPanel->SetDissolve(_dissolveValue);

            _blurBackgroundValue += timer::DeltaTime() * 5.f;
            effect::CommonEffectInstance()->SetEffect(effect::EPostEffect::Blur);
            effect::CommonEffectInstance()->SetBlur(_blurBackgroundValue);

            // 終了
            if(_dissolveValue <= 0.0f) {
                _currentState;
                _animState = EState_Anim::None;
                effect::CommonEffectInstance()->SetBlur(5.0f);
                MouseMgr()->SetAbsoluteMode();
            }

            break;
        case EState_Anim::Close:
            _dissolveValue += timer::DeltaTime() * 1.0f;
            _settingPanel->SetDissolve(_dissolveValue);

            _blurBackgroundValue -= timer::DeltaTime() * 5.f;
            effect::CommonEffectInstance()->SetEffect(effect::EPostEffect::Blur);
            effect::CommonEffectInstance()->SetBlur(_blurBackgroundValue);

			// 終了
            if(_dissolveValue >= 1.0f) {
                Reset();
                _settingPanel->SetDissolve(1.0f);
                _animState = EState_Anim::None;
                _settingPanel->Disable();

                _blurBackgroundValue = 0.0f;
                effect::CommonEffectInstance()->SetBlur(_blurBackgroundValue);
                MouseMgr()->SetRelativeMode();
            }
            break;
    }
}
//---------------------------------------------------------------------------
//! 設定パネルを開く
//---------------------------------------------------------------------------
void SettingPanel::OpenSettingPanel()
{
    Reset();
    _settingPanel->Enable();
    _currentState        = EState::OpenChoose;
    _dissolveValue       = 1.0f;
    _blurBackgroundValue = 0.0f;
    _animState           = EState_Anim::Open;
}
//---------------------------------------------------------------------------
//! 設定パネルを閉じる
//---------------------------------------------------------------------------
void SettingPanel::CloseSettingPanel()
{
    _dissolveValue       = 0.0f;
    _blurBackgroundValue = 5.0f;
    _animState           = EState_Anim::Close;
}
//---------------------------------------------------------------------------
//! 設定パネルが開いてるか
//---------------------------------------------------------------------------
bool SettingPanel::IsOpen()
{
    return _settingPanel->IsReady();
}
}   // namespace settings
