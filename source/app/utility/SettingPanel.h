﻿//---------------------------------------------------------------------------
//!	@file	SettingPanel.h
//!	@brief	ゲーム設定のパネル
//---------------------------------------------------------------------------
#pragma once
namespace manager {
class EntityManager;
}
namespace entity::go {
class GameObject;
class PointLight;

namespace ui {
class Sprite;
class AdvancedSprite;
}   // namespace ui
}   // namespace entity::go

namespace settings {
class SettingPanel
{
public:
    // 全体的のステート
    enum class EState
    {
        OpenChoose,   //!< 最初のメニュー選択

        KeyConfigPage1,   //!< キーコンフィグページ１
        KeyConfigPage2,   //!< キーコンフィグページ2

        SoundConfig,   //!< サンド
    };

    // メニューのステート
    enum class EState_Menu
    {
        Resume,       //!< 再開
        Bgm,          //!< 背景音
        KeyConfing,   //!< キーコンフィグ
        Count
    };

    // Sound パネルのステート
    enum class EState_Sound
    {
        BGM,
        SE,
        Back,
        Count
    };

    enum class EState_Anim
    {
        None,
        Open,
        Close,
    };

    SettingPanel();    //! コンストラクター
    ~SettingPanel();   //! デストラクター
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    bool Init(raw_ptr<manager::EntityManager> enttMgr);                                    //!< 初期化
    void Update();                                                                         //!< 更新
    void Render(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture);   //!< 描画
    void RenderImgui();                                                                    //!< Imgui描画
    void Finalize();

    shr_ptr<gpu::Texture> GetSystemPanelTexture(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture);

    void OpenSettingPanel();    //!< 設定パネルを開く
    void CloseSettingPanel();   //!< 設定パネルを閉じる

    bool IsOpen();   //!< 設定パネルが開いてるか

protected:
private:
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------

    //!< 解放

    //----------
    //! フォルダ中のアイコン画像を読み込む
    void LoadControllerIconInDir(std::string path);
    void DisableAllPanel();      //!< 全部のパネルを閉じる
    void ResetAllPanelState();   //!< パネルのステートをリセット
    void Reset();                //!< リセット

    void HandleAnim();   //!< パネルのアニメション処理
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    // 共用テクスチャ(ポスト用)
    shr_ptr<gpu::Texture> _panelTexture;   //!< ポスト用テクスチャー
    shr_ptr<gpu::Texture> _noiseTexture;   //!< ノイズのテクスチャ

    shr_ptr<gpu::InputLayout> _pIntputLayout;   //!< 2D入力レイアウト

    // シェーダー
    shr_ptr<gpu::Shader> _shaderVs_2D;        //!< VS 3D頂点シェーダー
    shr_ptr<gpu::Shader> _shaderPs_Texture;   //!< PS テクスチャありピクセルシェーダー

    shr_ptr<gpu::Shader> _blurPs;   //!< PS ブラーピクセルシェーダー

    uni_ptr<DirectX::SpriteFont> _fontJiyucho;   //!< Jiyuchoというフォント
    uni_ptr<DirectX::SpriteFont> _fontMsFont;    //!< Msというフォント

    uni_ptr<renderer::FontRenderer> _fontRender;   //!<　パネルの文字描画

    // UI パネル(親オブジェクトとして)
    raw_ptr<ui::AdvancedSprite> _settingPanel;

    raw_ptr<gameobject::GameObject> _keyConfigPanel1;   //!< 操作説明Page1
    raw_ptr<gameobject::GameObject> _keyConfigPanel2;   //!< 操作説明Page2
    raw_ptr<gameobject::GameObject> _soundPanel;        //!< 音量調整

    raw_ptr<ui::AdvancedSprite> _soundSEValue;    //!< BGM音量の表示イメージ
    raw_ptr<ui::AdvancedSprite> _soundBGMValue;   //!< SE音量の表示イメージ

    EState       _currentState = EState::OpenChoose;    //!<　パネールのステート
    EState_Menu  _menuState    = EState_Menu::Resume;   //!<　メニューのステート
    EState_Sound _soundState   = EState_Sound::BGM;     //!<　サウンドのステート
    EState_Anim  _animState    = EState_Anim::None;     //!<　アニメションのステート

    f32 _dissolveValue       = 0.0f;   //!< ディゾルブエフェクトの値
    f32 _blurBackgroundValue = 0.0f;   //!< ブラーエフェクトの値

    std::map<std::string, std::shared_ptr<gpu::Texture>> _controllericonTextures;   //!< キーコンフィグのイメージ

    uni_ptr<manager::EntityManager> _settingPanelEnttMgr;   //!< 設定パネルのエンティティ
};
}   // namespace settings
