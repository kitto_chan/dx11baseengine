﻿//---------------------------------------------------------------------------
//!	@file	D3DApp.cpp
//!	@brief	アプリケーションメイン
//---------------------------------------------------------------------------
#include "D3DApp.h"

#include "graphics/graphics_imgui.h"

#include "imgui/imgui.h"
#include "manager/EffekseerManager.h"

namespace {
// This is just used to forward Windows messages from a global window
// procedure to our member function window procedure because we cannot
// assign a member function to WNDCLASS::lpfnWndProc.
raw_ptr<D3DApp> g_pd3dApp = nullptr;   //!< このD3DApp実体
HWND            _hwnd     = nullptr;   //!< ウィンドウハンドル

RECT _windowedRect_ = {};   //!< ウィンドウモード時のウィンドウ位置
bool _isSuspending  = false;
bool _isMinimized   = false;
bool _isFullScreen  = false;   //!< フルスクリーンモードかどうか
bool _isWindowMode  = true;
}   // namespace
bool ToggleFullScreen()
{
    _isFullScreen = !_isFullScreen;
    auto hwnd     = application::hwnd();

    constexpr LONG toggleStyle = WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX;

    auto style = GetWindowLong(hwnd, GWL_STYLE);
    SetWindowLong(hwnd, GWL_STYLE, style ^ toggleStyle);

    HWND windowOrder = HWND_NOTOPMOST;
    s32  x, y, w, h;
    if(_isFullScreen) {
        //------------------------------------------------------
        // フルスクリーンモード
        //------------------------------------------------------
        x = 0;
        y = 0;
        w = GetSystemMetrics(SM_CXSCREEN);   // 幅　 - 画面解像度
        h = GetSystemMetrics(SM_CYSCREEN);   // 高さ - 画面解像度

        windowOrder = HWND_TOP;   // 最前面

        //------------------------------------------------------
        // マルチモニターのフルスクリーン対応
        // 利便性の向上。現在のウィンドウ位置のモニターでフルスクリーン化
        //------------------------------------------------------
        constexpr bool supportForMultiMonitor = true;

        if constexpr(supportForMultiMonitor) {
            // 現在のモニターを列挙
            std::vector<RECT> monitorRects;

            // ※Windowsコールバックにラムダ式を渡す場合はキャプチャ指定不可
            MONITORENUMPROC MyInfoEnumProc = [](HMONITOR monitor, [[maybe_unused]] HDC hdcMonitor, RECT* rectMonitor, LPARAM args) -> BOOL {
                MONITORINFOEX monitorInfo{};
                monitorInfo.cbSize = sizeof(monitorInfo);
                GetMonitorInfo(monitor, &monitorInfo);

                auto* monitorRects = reinterpret_cast<std::vector<RECT>*>(args);

                if(monitorInfo.dwFlags == DISPLAY_DEVICE_MIRRORING_DRIVER) {
                    return true;
                }
                else {
                    monitorRects->push_back(*rectMonitor);
                };
                return true;
            };

            // モニター列挙
            EnumDisplayMonitors(nullptr, nullptr, MyInfoEnumProc, reinterpret_cast<LPARAM>(&monitorRects));

            // 元に戻すときのために現在のウィンドウの位置とサイズを保存
            GetWindowRect(hwnd, &_windowedRect_);

            // 中心座標を計算
            s32 centerX = (_windowedRect_.left + _windowedRect_.right) >> 1;
            s32 centerY = (_windowedRect_.bottom + _windowedRect_.top) >> 1;

            // 現在のウィンドウ中心位置がどのモニター上にあるか判定してフルスクリーン化するモニターを選択
            auto rect = std::find_if(monitorRects.begin(),
                                     monitorRects.end(),
                                     [&](RECT& r) { return r.left <= centerX && centerX < r.right &&
                                                           r.top <= centerY && centerY < r.bottom; });
            if(rect != monitorRects.end()) {
                x = rect->left;
                y = rect->top;
                w = rect->right - rect->left;
                h = rect->bottom - rect->top;
            }
        }
    }
    else {
        //----------------------------------------------
        // ウィンドウモード
        //----------------------------------------------
        // 元に戻す
        x = _windowedRect_.left;
        y = _windowedRect_.top;
        w = _windowedRect_.right - _windowedRect_.left;
        h = _windowedRect_.bottom - _windowedRect_.top;
    }
    // ウィンドウ位置と優先度を更新
    SetWindowPos(hwnd,                                    // [in] ウィンドウハンドル
                 windowOrder,                             // [in] ウィンドウの表示優先順位
                 x, y,                                    // [in] 位置
                 w, h,                                    // [in] 幅高さ
                 SWP_FRAMECHANGED | SWP_NOOWNERZORDER);   // [in] 変更対象の選択フラグ (ウィンドウスタイルの変更)

    return true;
}
//---------------------------------------------------------------------------
//! Forward hwnd on because we can get messages (e.g., WM_CREATE)
//! before CreateWindow returns, and thus before m_hMainWnd is valid.
//---------------------------------------------------------------------------
LRESULT CALLBACK
MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    return g_pd3dApp->WndProc(hwnd, msg, wParam, lParam);
}

//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
D3DApp::D3DApp(HINSTANCE hInstance)
: _hAppInstance(hInstance)
{
    g_pd3dApp = this;
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
D3DApp::~D3DApp()
{
}

//---------------------------------------------------------------------------
//! 実体を取得
//---------------------------------------------------------------------------
HINSTANCE D3DApp::AppInstance() const
{
    return _hAppInstance;
}

//---------------------------------------------------------------------------
//! ウィンドウハンドルを取得
//---------------------------------------------------------------------------
HWND D3DApp::MainWnd()
{
    return _hwnd;
}

//---------------------------------------------------------------------------
//! スクリーン解像度を取得
//---------------------------------------------------------------------------
f32 D3DApp::AspectRatio() const
{
    return static_cast<float>(_wndWidth) / _wndHeight;
}
//---------------------------------------------------------------------------
//! メインループ
//---------------------------------------------------------------------------
int D3DApp::Run()
{
    MSG msg{};

    timer::TimerIns()->Reset();

    while(msg.message != WM_QUIT) {
        if(PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        else {
            // タイム計算
            timer::TimerIns()->Tick();

            // ゲームループ
            if(!_appPaused) {
                // フレームを表示
                DisplayFrameInWnd();

                //----------------------------------------------------------
                // 更新
                //----------------------------------------------------------
                // 入力処理
                KeyboardMgr()->Update();
                MouseMgr()->Update();
                input::GamePadMgr()->Update();

                //|| input::GamePadMgr()->IsViewPressed()
                // ゲーム更新
                Update();
                if(SystemSettingsMgr()->IsPauseState()) {
                    if(KeyboardMgr()->IsKeyDown(DirectX::Keyboard::Escape)) {
                        break;
                    }
                }

                if(SystemSettingsMgr()->IsQuitGame()) {
                    break;
                }

                manager::AudioMgr()->Update();
                // TODO： Imgui更新

                //----------------------------------------------------------
                // 描画
                //----------------------------------------------------------
                // ゲーム描画
                Render();

                //----------------------------------------------------------
                // リセット
                //----------------------------------------------------------
                MouseMgr()->ResetScrollWheelValue();

                //----------------------------------------------------------
                // フレーム更新
                //----------------------------------------------------------
            }
            else {
                Sleep(100);
            }
        }
    }

    input::GamePadMgr()->Suspend();

    Finalize();
    // COMライブラリの解放
    CoUninitialize();
    // WM_QUITメッセージの戻り値を終了コードとして返す
    return static_cast<int>(msg.wParam);
}

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool D3DApp::Init()
{
    // Win32関しての初期化
    if(!InitMainWnd()) {
        ASSERT_MESSAGE(false, "WIN32初期化失敗");
        return false;
    }
    // DirectX関しての初期化
    if(!InitD3D()) {
        ASSERT_MESSAGE(false, "DirectX11初期化失敗");
        return false;
    }

    MouseMgr()->Init(_hwnd);
    manager::AudioMgr()->Init();
    renderer::FontRendererIns()->Init();
    // エフェクトシア初期化
    manager::EffekseerMgr()->Init();
    return true;
}
//---------------------------------------------------------------------------
//! ウインドウズResize
//---------------------------------------------------------------------------
void D3DApp::OnResize()
{
    // #TODO
}
//---------------------------------------------------------------------------
//! アプリ再開
//---------------------------------------------------------------------------
void D3DApp::Resuming()
{
    if(_isSuspending) {
        manager::AudioMgr()->Resume();
        input::GamePadMgr()->Resume();
    }
    _isSuspending = false;
}
//---------------------------------------------------------------------------
//! アプリ一時停止
//---------------------------------------------------------------------------
void D3DApp::Suspending()
{
    if(!_isSuspending) {
        manager::AudioMgr()->Suspend();
        input::GamePadMgr()->Suspend();
    }
    _isSuspending = true;
}
//---------------------------------------------------------------------------
//! ウィンドウプロシージャ
//!	@param	[in]	hwnd	ウィンドウハンドル
//!	@param	[in]	message	ウィンドウメッセージ
//!	@param	[in]	wparam	パラメーター1
//!	@param	[in]	lparam	パラメーター2
//!	@return ウィンドウプロシージャのそれぞれのメッセージ処理後の値
//---------------------------------------------------------------------------
LRESULT D3DApp::WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    //----------------------------------------------------------
    // ImGuiのウィンドウプロシージャ処理
    //----------------------------------------------------------
    if(graphics::imgui::windowProc(hwnd, msg, wParam, lParam)) {
        return 0;
    }

    //----------------------------------------------------------
    // ウィンドウメッセージ処理
    //----------------------------------------------------------
    switch(msg) {
        case WM_ACTIVATEAPP:
            if(wParam) {
                MouseMgr()->GetMouse()->ProcessMessage(msg, wParam, lParam);
                KeyboardMgr()->GetKeyboard()->ProcessMessage(msg, wParam, lParam);
                Resuming();
            }
            else {
                Suspending();
            }
            break;
        case WM_CREATE:   // ウィンドウ生成時(コンストラクタ相当)
        {
            // ここでGWLP_USERDATAにthisポインタを保存
            auto* cs = reinterpret_cast<CREATESTRUCT*>(lParam);
            SetWindowLongPtr(hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(cs->lpCreateParams));
            return 0;
        }
        //入力
        case WM_MOUSEACTIVATE:
            return MA_ACTIVATEANDEAT;
        case WM_INPUT:
        //マウス入力処理
        case WM_LBUTTONDOWN:
        case WM_MBUTTONDOWN:
        case WM_RBUTTONDOWN:
        case WM_XBUTTONDOWN:

        case WM_LBUTTONUP:
        case WM_MBUTTONUP:
        case WM_RBUTTONUP:
        case WM_XBUTTONUP:

        case WM_MOUSEWHEEL:
        case WM_MOUSEHOVER:
        case WM_MOUSEMOVE:
            MouseMgr()->GetMouse()->ProcessMessage(msg, wParam, lParam);
            break;

        //キーボード入力
        case WM_KEYDOWN:
        case WM_SYSKEYDOWN:
        {
            auto vk = wParam;          // 現在押されている仮想キーコード
            if(lParam & (1 << 29)) {   // Altキーが押されているかどうか
                // Alt+Enter
                if(vk == VK_RETURN) {
                    // フルスクリーンモードを切り替え
                    ToggleFullScreen();
                    return 0;
                }
            }
        }
        case WM_KEYUP:
        case WM_SYSKEYUP:
            KeyboardMgr()->GetKeyboard()->ProcessMessage(msg, wParam, lParam);
            return 0;
            //ウィンドウサイズ変更
        case WM_SIZE:
        {
            if(wParam == SIZE_MINIMIZED) {
                if(!_isMinimized) {
                    _isMinimized = true;
                    Suspending();
                }
            }
            else if(_isMinimized) {
                _isMinimized = false;
                Resuming();
            }

            auto width  = static_cast<u32>(lParam) & 0xffff;           // 幅
            auto height = (static_cast<u32>(lParam) >> 16) & 0xffff;   // 高さ
            dx11::swapChain()->resizeBuffers(width, height);           // バックバッファの解像度変更
        } break;
        case WM_POWERBROADCAST:
            switch(wParam) {
                case PBT_APMQUERYSUSPEND:
                    Suspending();
                    return TRUE;

                case PBT_APMRESUMESUSPEND:
                    if(!_isMinimized) {
                        Resuming();
                    }
                    return TRUE;
            }
            break;
        case WM_DESTROY:   // ウィンドウ破棄時(デストラクタ相当)
            PostQuitMessage(0);
            return 0;
    }

    //-------------------------------------------------------------
    // デフォルトのウィンドウプロシージャ
    // ※注意※ UNICODE版CreateWindowWを利用する場合はこの関数もUNICODE版にする必要あり
    //-------------------------------------------------------------
    return DefWindowProcW(hwnd, msg, wParam, lParam);
}
//---------------------------------------------------------------------------
//! Win32の初期化
//---------------------------------------------------------------------------
bool D3DApp::InitMainWnd()
{
    // 高DPIモード対応
    // ウィンドウUIのみDPIスケーリングされ、クライアント領域はdot-by-dot
    SetProcessDpiAwarenessContext(DPI_AWARENESS_CONTEXT_PER_MONITOR_AWARE_V2);

    // COMライブラリを初期化
    CoInitializeEx(nullptr, COINIT_MULTITHREADED);

    //----------------------------------------------------------
    // ウィンドウクラスの登録
    //----------------------------------------------------------
    WNDCLASSEXW windowClass{};
    windowClass.cbSize        = sizeof(windowClass);           // 構造体サイズ
    windowClass.style         = CS_HREDRAW | CS_VREDRAW;       // クラススタイル
    windowClass.lpfnWndProc   = MainWndProc;                   // ウィンドウプロシージャ
    windowClass.hInstance     = _hAppInstance;                 // アプリケーションインスタンスハンドル
    windowClass.hCursor       = LoadCursor(NULL, IDC_CROSS);   // カーソルの種類
    windowClass.lpszClassName = L"DX11SampleClass";            // クラス名

    RegisterClassExW(&windowClass);

    //----------------------------------------------------------
    // ウィンドウを作成
    //----------------------------------------------------------
    u32 style   = WS_OVERLAPPEDWINDOW;   // ウィンドウスタイル
    u32 styleEx = 0;                     // 拡張ウィンドウスタイル
    s32 x       = CW_USEDEFAULT;
    s32 y       = CW_USEDEFAULT;
    s32 w       = CW_USEDEFAULT;
    s32 h       = CW_USEDEFAULT;

    // ウィンドウサイズをUIサイズを考慮して補正
    {
        RECT windowRect{ 0, 0, static_cast<LONG>(_wndWidth), static_cast<LONG>(_wndHeight) };

        AdjustWindowRectEx(&windowRect, style, false, styleEx);
        w = windowRect.right - windowRect.left;
        h = windowRect.bottom - windowRect.top;
    }

    // ウィンドウを作成
    _hwnd = CreateWindowExW(
        styleEx,                     // 拡張ウィンドウスタイル
        windowClass.lpszClassName,   // ウィンドウクラス名
        L"Direct3D11 Game Engine",   // タイトル名
        style,                       // ウィンドウスタイル
        x,                           // X座標
        y,                           // Y座標
        w,                           // ウィンドウの幅
        h,                           // ウィンドウの高さ
        nullptr,                     // 親ウィンドウ(なし)
        nullptr,                     // メニューハンドル(なし)
        _hAppInstance,               // アプリケーションインスタンスハンドル
        nullptr);                    // WM_CREATEへの引数(任意)

    return true;
}
//---------------------------------------------------------------------------
//! DirectXの初期化
//---------------------------------------------------------------------------
bool D3DApp::InitD3D()
{
    //----------------------------------------------------------
    // 初期化
    //----------------------------------------------------------
    if(!graphics::render()->initialize(_wndWidth, _wndHeight)) {
        MessageBox(_hwnd, "起動に失敗しました.", "初期化エラー", MB_OK);
        return 1;
    }

    if(!_isWindowMode) {
        ToggleFullScreen();
    }

    // ウィンドウを表示
    {
        // GetWindowPlacementからWinMainの引数のnCmdShowを取得可能。
        WINDOWPLACEMENT placement;
        GetWindowPlacement(application::hwnd(), &placement);
        ShowWindow(application::hwnd(), static_cast<int>(placement.showCmd));
    }

    return true;
}
//---------------------------------------------------------------------------
//! フレームを表す
//---------------------------------------------------------------------------
void D3DApp::DisplayFrameInWnd()
{
    static int   frameCnt    = 0;
    static float timeElapsed = 0.0f;

    frameCnt++;

    if((timer::TimerIns()->TotalTime() - timeElapsed) >= 1.0f) {
        float fps  = (float)frameCnt / 1.f;   // fps = frameCnt / 1
        float mspf = 1000.0f / fps;

        std::stringstream sstream;
        sstream << _wndTitle << "    "
                << "FPS: " << fps << "    "
                << "Frame Time: " << mspf << " (ms)";

        SetWindowText(_hwnd, sstream.str().c_str());
        // Reset for next average.
        frameCnt = 0;
        timeElapsed += 1.0f;
    }
}

namespace application {
//---------------------------------------------------------------------------
//! ウィンドウハンドルを取得
//---------------------------------------------------------------------------
HWND hwnd()
{
    return _hwnd;
}

//---------------------------------------------------------------------------
//! アプリケーションインスタンスハンドルを取得
//---------------------------------------------------------------------------
HINSTANCE instance()
{
    return GetModuleHandle(nullptr);
}

}   // namespace application
