﻿//---------------------------------------------------------------------------
//!	@file	D3DApp.h
//!	@brief	アプリケーションメイン
//---------------------------------------------------------------------------
#pragma once

namespace application {
HWND hwnd();
}   // namespace application

class D3DApp
{
public:
    D3DApp(HINSTANCE hInstance);   //!< コンストラクタ
    virtual ~D3DApp();             //!< デストラクタ

    //! ムーブ禁止 / コピー禁止
    D3DApp(const D3DApp&) = delete;
    D3DApp(D3DApp&&)      = delete;
    D3DApp& operator=(const D3DApp&) = delete;
    D3DApp& operator=(D3DApp&&) = delete;

public:
    HINSTANCE AppInstance() const;   //! アプリケーションインスタンスハンドルを取得
    HWND      MainWnd();             //!< ウィンドウハンドルを取得
    f32       AspectRatio() const;   //!< スクリーン解像度

    int Run();   //!< メインループ

    //===========================================================================
    //!	@defgroup	フレームワークからのコールバック
    //===========================================================================
    //@{

    virtual bool Init();           //!< 全体的な初期化
    virtual void OnResize();       //!< ウィンドウサイズ変更するとき
    virtual void Update()   = 0;   //!< 全体的な更新
    virtual void Render()   = 0;   //!< 全体的な描画
    virtual void Finalize() = 0;   //!< 全体的な解放

    void Resuming();    //!  アプリ再開
    void Suspending();   //! アプリ一時停止
    //@}

    virtual LRESULT WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

protected:
    //---------------------------------------------------------------------------
    // protected 関数
    //---------------------------------------------------------------------------
    bool InitMainWnd();   //!< ウィンドの初期化
    bool InitD3D();       //!< DirectXの初期化

    void DisplayFrameInWnd();   //!< フレームを表す
    //---------------------------------------------------------------------------
    // protected 変数
    //---------------------------------------------------------------------------
    HINSTANCE _hAppInstance;
    //HWND      _hMainWnd;
    bool _appPaused    = false;   //!< アプリ一時停止
    bool _useMinimized = false;   //!< アプリ最小化
    bool _useMaxmized  = true;   //!< アプリ全画面表示
    bool _useResize    = false;   //!< アプリスクリーンサイズ調整できるかどうか

    std::string _wndTitle = sys::GAME_TITLE;   //<! アプリ名前

    s32 _wndWidth  = sys::WINDOW_WIDTH;    //!< ウィンドの幅
    s32 _wndHeight = sys::WINDOW_HEIGHT;   //!< ウィンドの高さ
};
