﻿//---------------------------------------------------------------------------
//! @file    SystemSettings.h
//! @brief   システム設定
//---------------------------------------------------------------------------
#pragma once
namespace sys {
constexpr s32 WINDOW_WIDTH  = 1440;   //!< 初期window幅サイズ
constexpr s32 WINDOW_HEIGHT = 810;    //!< 初期window長サイズ

constexpr f32 ASPECT_RATIO  = static_cast<f32>(WINDOW_WIDTH) / WINDOW_HEIGHT;

constexpr f32 WINDOW_CENTER_X = WINDOW_WIDTH / 2.0f;
constexpr f32 WINDOW_CENTER_Y = WINDOW_HEIGHT / 2.0f;

constexpr bool FULLSCREEN_MODE = false;
constexpr char GAME_TITLE[]    = "Tales of Sword";

constexpr bool IsDebug = true;
}   // namespace sys

#define ANIM_PATH "anim/"
