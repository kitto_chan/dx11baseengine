﻿//---------------------------------------------------------------------------
//!	@file	dx11.h
//!	@brief	DirectX11
//---------------------------------------------------------------------------
#pragma once

namespace dx11 {

constexpr bool USE_DEBUG_LAYER = true;   //!< D3D11デバッグレイヤーを利用するかどうか

class Texture;       // テクスチャ
class InputLayout;   // 入力レイアウト

//----------------------------------------------------------
// プリミティブの種類
//----------------------------------------------------------
enum class Primitive
{
    Undefined,          //!< 未定義
    PointList,          //!< ポイントリスト
    LineList,           //!< ラインリスト
    LineStrip,          //!< ラインSTRIP
    TriangleList,       //!< 三角形リスト
    TriangleStrip,      //!< 三角形STRIP
    LineListAdj,        //!< 隣接データを持つラインリスト
    LineStripAdj,       //!< 隣接データを持つラインSTRIP
    TriangleListAdj,    //!< 隣接データを持つ三角形リスト
    TriangleStripAdj,   //!< 隣接データを持つ三角形STRIP
};

//! プリミティブの種類をD3D11_PRIMITIVE_TOPOLOGYに変換
//! @param  [in]    type    プリミティブの種類
D3D11_PRIMITIVE_TOPOLOGY makePrimitiveTopology(dx11::Primitive type);

//! D3D11_USAGEからCPUアクセス権フラグ取得
u32 makeD3DCpuAccessFlags(D3D11_USAGE usage);

//! カラーターゲットに適したフォーマットか判定
bool isColorTargetFormat(DXGI_FORMAT dxgiFormat);

//! デプスバッファに適したフォーマットか判定
bool isDepthTargetFormat(DXGI_FORMAT dxgiFormat);

//! ステンシルバッファフォーマットか判定
bool isStencilFormat(DXGI_FORMAT dxgiFormat);

//! TYPELESS形式のフォーマットに変換
DXGI_FORMAT makeTypelessFormat(DXGI_FORMAT dxgiFormat);

//! カラー形式のフォーマットに変換
DXGI_FORMAT makeColorFormat(DXGI_FORMAT dxgiFormat);

//! 1ピクセルあたりのサイズを取得
size_t bpp(DXGI_FORMAT dxgiFormat);

// 解像度からミップ段数を計算
size_t countMips(size_t width, size_t height);

}   // namespace dx11

namespace dx11::callback {

extern std::function<bool(u32, u32)>                                                                 onInitialize;
extern std::function<void(f32)>                                                                      onUpdate;
extern std::function<void(raw_ptr<dx11::Texture> colorTexture, raw_ptr<dx11::Texture> depthTexture)> onRender;
extern std::function<void()>                                                                         onFinalize;

}   // namespace dx11::callback
