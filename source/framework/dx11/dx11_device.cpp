﻿//---------------------------------------------------------------------------
//!	@file	dx11_device.cpp
//!	@brief	GPUデバイス
//---------------------------------------------------------------------------
#include "dx11_device.h"

namespace dx11 {

//---- D3Dデバイスでサポートする機能レベル。作成試行順序
static constexpr D3D_FEATURE_LEVEL featureLevels[]{
    D3D_FEATURE_LEVEL_11_1,   // DirectX 11.1 (定数バッファの制限撤廃, 低精度高速シェーディング, 全てのシェーダーでUAV利用, 3D立体視)
    D3D_FEATURE_LEVEL_11_0,   // DirectX 11.0 (テセレーター, ComputeShader, 遅延コンテキスト)
    D3D_FEATURE_LEVEL_10_0,   // DirectX 10.0 (ジオメトリシェーダー, ストリーム出力)
};

//---------------------------------------------------------------------------
//! ハードウェアアダプターを検出取得
//!	@return 取得したハードウェアアダプター。(nullptrの場合は失敗)
//---------------------------------------------------------------------------
com_ptr<IDXGIAdapter1> getHardwareAdapter(IDXGIFactory4* dxgiFactory)
{
    com_ptr<IDXGIAdapter1> adapter;

    // すべてのアダプターを検索
    // IntelHDGraphics/GeForce/Radeonなど複数持つ環境もあるため
    // ここで使用するディスプレイアダプターを選択する。ソフトウェアレンダラーは読み飛ばし
    for(u32 adapterIndex = 0; dxgiFactory->EnumAdapters1(adapterIndex, &adapter) != DXGI_ERROR_NOT_FOUND; ++adapterIndex) {
        DXGI_ADAPTER_DESC1 desc;
        adapter->GetDesc1(&desc);

        // ソフトウェアレンダラーを除外
        if(desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE) {
            continue;
        }

        // デバイスの生成試行
        // ※この段階では実際に生成せず、生成可能かどうかのみをチェック(最後の引数がnullptr)
        auto hr = D3D11CreateDevice(adapter.Get(),                                // [in]	対象のディスプレイアダプタGPU
                                    D3D_DRIVER_TYPE_UNKNOWN,                      // [in]	デバイスの種類(アダプタ指定の場合はUNKNOWN)
                                    nullptr,                                      // [in]	ソフトウェアラスタライザ
                                    0,                                            // [in]	オプションフラグ
                                    featureLevels,                                // [in]	試行する機能レベルの配列
                                    static_cast<u32>(std::size(featureLevels)),   // [in]	　　　　〃　　　　　配列数
                                    D3D11_SDK_VERSION,                            // [in]	SDKバージョン (D3D11_SDK_VERSION固定)
                                    nullptr,                                      // [out]	作成されたDirect3Dデバイス
                                    nullptr,                                      // [out]	作成されたデバイスの機能レベル
                                    nullptr);                                     // [out]	作成されたイミディエイトコンテキスト

        if(SUCCEEDED(hr)) {
            OutputDebugString("GPU=");
            OutputDebugStringW(desc.Description);

            break;
        }
    }
    return adapter;
}

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool Device::initialize([[maybe_unused]] u32 width, [[maybe_unused]] u32 height)
{
    u32 dxgiFactoryFlags  = 0;
    u32 createDeviceFlags = 0;

    //----------------------------------------------------------
    // デバッグレイヤー
    //----------------------------------------------------------
    if constexpr(USE_DEBUG_LAYER) {
        // デバッグレイヤーの有効化 (Windows10のオプション機能 : グラフィックツール の追加が必要)
        // [設定] - [アプリと機能] - オプション機能
        // 設定を切り替えた後にデバイスを生成することでデバッグレイヤーが有効になります。

        // （パフォーマンスと引き換えにエラー詳細メッセージ出力を行う）
        createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;

        // DXGIFactory生成にもフラグが必要
        dxgiFactoryFlags |= DXGI_CREATE_FACTORY_DEBUG;
    }

    //----------------------------------------------------------
    // ファクトリーの生成
    //
    // DXGIFactoryからアダプター取得し、スワップチェイン生成が可能
    //  +--------------+          +---------------+
    //  | DXGIFactory2 |>>>>>>>>>>| IDXGIAdapter1 |
    //  +--------------+  Create  +---------------+
    //          v                      ↑ 参照
    //          v                 +=================+
    //           >>>>>>>>>>>>>>>>>| IDXGISwapChain1 |
    //                    Create  +=================+
    //----------------------------------------------------------
    if(FAILED(CreateDXGIFactory2(dxgiFactoryFlags, IID_PPV_ARGS(&dxgiFactory_)))) {
        return false;
    }

    //-------------------------------------------------------------
    // Direct3D11デバイス作成
    // 同時に直接コンテキストが作成される
    //-------------------------------------------------------------
    // ハードウェアデバイスで初期化
    com_ptr<IDXGIAdapter1> adapter = getHardwareAdapter(dxgiFactory_.Get());
    {
        com_ptr<ID3D11Device>        d3dDevice;
        com_ptr<ID3D11DeviceContext> d3dImmediateContext;

        auto hr = D3D11CreateDevice(adapter.Get(),                                // [in]	対象のディスプレイアダプタGPU
                                    D3D_DRIVER_TYPE_UNKNOWN,                      // [in]	デバイスの種類
                                    nullptr,                                      // [in]	ソフトウェアラスタライザ
                                    createDeviceFlags,                            // [in]	オプションフラグ
                                    featureLevels,                                // [in]	試行する機能レベルの配列
                                    static_cast<u32>(std::size(featureLevels)),   // [in]	　　　　〃　　　　　配列数
                                    D3D11_SDK_VERSION,                            // [in]	SDKバージョン (D3D11_SDK_VERSION固定)
                                    &d3dDevice,                                   // [out]	作成されたDirect3Dデバイス
                                    &featureLevel_,                               // [out]	作成されたデバイスの機能レベル
                                    &d3dImmediateContext);                        // [out]	作成されたイミディエイトコンテキスト

        // ハードウェアデバイスで生成失敗した場合はWARPデバイスで初期化
        if(FAILED(hr)) {
            // WARP(Windows Advanced Rasterization Platform) デバイス
            // 高速ソフトウェアレンダリング
            com_ptr<IDXGIAdapter> warpAdapter;
            if(FAILED(dxgiFactory_->EnumWarpAdapter(IID_PPV_ARGS(&warpAdapter)))) {
                return false;
            }

            hr = D3D11CreateDevice(warpAdapter.Get(),                            // [in]	対象のディスプレイアダプタGPU
                                   D3D_DRIVER_TYPE_UNKNOWN,                      // [in]	デバイスの種類
                                   nullptr,                                      // [in]	ソフトウェアラスタライザ
                                   createDeviceFlags,                            // [in]	オプションフラグ
                                   featureLevels,                                // [in]	試行する機能レベルの配列
                                   static_cast<u32>(std::size(featureLevels)),   // [in]	　　　　〃　　　　　配列数
                                   D3D11_SDK_VERSION,                            // [in]	SDKバージョン (D3D11_SDK_VERSION固定)
                                   &d3dDevice,                                   // [out]	作成されたDirect3Dデバイス
                                   &featureLevel_,                               // [out]	作成されたデバイスの機能レベル
                                   &d3dImmediateContext);                        // [out]	作成されたイミディエイトコンテキスト

            if(FAILED(hr)) {
                return false;
            }
        }
        // 初期化時はDirectX11.0初期化したため、DirectX11.4に切り替え
        d3dDevice.As(&d3d11Device_);                     // ID3DDevice→ID3DDevice5
        d3dImmediateContext.As(&d3dImmediateContext_);   // ID3DDeviceContext→ID3DDeviceContext4
    }

    //----------------------------------------------------------
    // デバッグレイヤーでエラー発生時デバッグブレークするように設定
    //----------------------------------------------------------
    if constexpr(USE_DEBUG_LAYER) {
        com_ptr<ID3D11Debug> d3dDebug;
        if(SUCCEEDED(d3d11Device_.As(&d3dDebug))) {
            com_ptr<ID3D11InfoQueue> d3dInfoQueue;
            if(SUCCEEDED(d3dDebug.As(&d3dInfoQueue))) {
                // エラー発生時にブレークする設定
                d3dInfoQueue->SetBreakOnSeverity(D3D11_MESSAGE_SEVERITY_CORRUPTION, true);   // クリティカルな破損エラー
                d3dInfoQueue->SetBreakOnSeverity(D3D11_MESSAGE_SEVERITY_ERROR, true);        // エラー出力
                //d3dInfoQueue->SetBreakOnSeverity(D3D11_MESSAGE_SEVERITY_WARNING, true);      // 警告出力

                // パラメーター設定変更したときの情報表示をOFFにする
                D3D11_MESSAGE_ID hide[]{
                    D3D11_MESSAGE_ID_SETPRIVATEDATA_CHANGINGPARAMS,
                };
                D3D11_INFO_QUEUE_FILTER filter{};
                filter.DenyList.NumIDs  = static_cast<u32>(std::size(hide));
                filter.DenyList.pIDList = hide;
                d3dInfoQueue->AddStorageFilterEntries(&filter);
            }
        }
    }

    return true;
}

}   // namespace dx11
