﻿//---------------------------------------------------------------------------
//!	@file	dx11_helper.cpp
//!	@brief	DirectX11ヘルパーユーティリティー
//---------------------------------------------------------------------------
#include "dx11_helper.h"
#include "dx11_constant_buffer_pool.h"
#include "../graphics/graphics_dynamic_vertex.h"
#include <bitset>

namespace dx11 {

constexpr u32 VERTEX_SLOT_MAX        = 4 /*D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT*/;   // 頂点バッファスロット数
constexpr u32 RENDER_TARGET_SLOT_MAX = D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT;         // 描画ターゲットスロット数
void          preDraw();
namespace {

//--------------------------------------------------------------
// 内部キャッシュ
//--------------------------------------------------------------
bool                         isChangedRenderTargets_ = false;
bool                         isChangedViewports_     = false;
bool                         isChangedInputLayout_   = false;
std::bitset<VERTEX_SLOT_MAX> isChangedVertices_;   // boolではなくビットフィールドで管理
bool                         isChangedIndices_ = false;

std::array<com_ptr<ID3D11RenderTargetView>, RENDER_TARGET_SLOT_MAX> d3dRenderTargets_;   // 描画ターゲット(MRT : Multiple Render Target)
com_ptr<ID3D11DepthStencilView>                                     d3dDepthStencil_;    // デプスステンシル
D3D11_VIEWPORT                                                      d3dViewport_{};      // ビューポート設定

// 頂点入力(IA : Input Assembler)
raw_ptr<dx11::InputLayout>                         inputLayout_;   // 入力レイアウト
std::array<raw_ptr<ID3D11Buffer>, VERTEX_SLOT_MAX> d3dBufferVb_;   // 頂点バッファ
std::array<u32, VERTEX_SLOT_MAX>                   vertexStride_;
std::array<u32, VERTEX_SLOT_MAX>                   vertexOffset_;
raw_ptr<ID3D11Buffer>                              d3dBufferIb_;   // インデックスバッファ
u32                                                indexOffset_ = 0;

raw_ptr<dx11::Shader> shaderVs_;   // 頂点シェーダー
raw_ptr<dx11::Shader> shaderPs_;   // ピクセルシェーダー
raw_ptr<dx11::Shader> shaderGs_;   // ジオメトリシェーダー
raw_ptr<dx11::Shader> shaderCs_;   // コンピュートシェーダー

}   // namespace

//--------------------------------------------------------------
// ヘルパーユーティリティーのキャッシュを破棄
//--------------------------------------------------------------
void invalidateHelperCache()
{
    isChangedRenderTargets_ = true;
    isChangedViewports_     = true;
    isChangedInputLayout_   = true;
    isChangedVertices_ |= std::bitset<VERTEX_SLOT_MAX>(static_cast<u64>(-1));   // 論理和
    isChangedIndices_ = true;
}

//===========================================================================
//! @defgourp   頂点入力
//===========================================================================
//@{

//---------------------------------------------------------------------------
//! 入力レイアウト設定
//---------------------------------------------------------------------------
void setInputLayout(raw_ptr<dx11::InputLayout> inputLayout)
{
    // ここではポインタだけを保存し、描画直前で解決させる
    inputLayout_          = inputLayout;
    isChangedInputLayout_ = true;
}

//---------------------------------------------------------------------------
//! 入力レイアウトを取得
//---------------------------------------------------------------------------
raw_ptr<dx11::InputLayout> getInputLayout()
{
    return inputLayout_;
}

//---------------------------------------------------------------------------
//! 頂点バッファを設定
//---------------------------------------------------------------------------
void setVertexBuffer(u32 slot, raw_ptr<dx11::Buffer> buffer, u32 stride, u32 offset)
{
    if(buffer) {
        // ここではパラメーター保存し、描画直前で解決させる
        if(d3dBufferVb_[slot] != static_cast<ID3D11Buffer*>(*buffer) || vertexStride_[slot] != stride || vertexOffset_[slot] != offset) {
            d3dBufferVb_[slot]  = static_cast<ID3D11Buffer*>(*buffer);
            vertexStride_[slot] = stride;
            vertexOffset_[slot] = offset;
            isChangedVertices_.set(slot);
        }
    }
    else {
        d3dBufferVb_[slot]  = nullptr;
        vertexStride_[slot] = stride;
        vertexOffset_[slot] = offset;
        isChangedVertices_.set(slot);
    }
}

//---------------------------------------------------------------------------
//! インデックスバッファを設定
//---------------------------------------------------------------------------
void setIndexBuffer(raw_ptr<dx11::Buffer> buffer, u32 offset)
{
    if(buffer) {
        // ここではパラメーター保存し、描画直前で解決させる
        if(d3dBufferIb_ != static_cast<ID3D11Buffer*>(*buffer) || indexOffset_ != offset) {
            d3dBufferIb_      = static_cast<ID3D11Buffer*>(*buffer);
            indexOffset_      = offset;
            isChangedIndices_ = true;
        }
    }
    else {
        d3dBufferIb_ = nullptr;
        offset       = 0;
    }
}

//@}
//===========================================================================
//! @defgroup   頂点シェーダー
//===========================================================================
//@{

namespace vs {

//---------------------------------------------------------------------------
//! シェーダーの設定
//---------------------------------------------------------------------------
void setShader(raw_ptr<dx11::Shader> shader)
{
    ID3D11VertexShader* vs = (shader) ? static_cast<ID3D11VertexShader*>(*shader) : nullptr;
    dx11::context()->VSSetShader(vs, nullptr, 0);
    if(shader && shaderVs_ != shader) {
        shaderVs_             = shader;
        isChangedInputLayout_ = true;
    }
}

//---------------------------------------------------------------------------
//! テクスチャ(Shader Resource View)の設定
//---------------------------------------------------------------------------
void setTexture(u32 slot, raw_ptr<dx11::Texture> texture)
{
    preDraw();

    ID3D11ShaderResourceView* srvs[]{ *texture };
    dx11::context()->VSSetShaderResources(slot, 1, srvs);
}

//---------------------------------------------------------------------------
//! サンプラーステートの設定
//---------------------------------------------------------------------------
void setSamplerState(u32 slot, raw_ptr<ID3D11SamplerState> sampler)
{
    ID3D11SamplerState* samplers[]{ sampler.get() };
    dx11::context()->VSSetSamplers(slot, 1, samplers);
}

}   // namespace vs

//@}
//===========================================================================
//! @defgroup   ピクセルシェーダー
//===========================================================================
//@{

namespace ps {

//---------------------------------------------------------------------------
//! テクスチャ(Shader Resource View)の設定
//---------------------------------------------------------------------------
void setTexture(u32 slot, raw_ptr<dx11::Texture> texture)
{
    //前のリソースをクリアします
    ID3D11ShaderResourceView* const pSRV[1] = { nullptr };
    dx11::context()->PSSetShaderResources(slot, 1, pSRV);

    preDraw();
    ID3D11ShaderResourceView* srvs[]{ (texture) ? static_cast<ID3D11ShaderResourceView*>(*texture) : nullptr };
    dx11::context()->PSSetShaderResources(slot, 1, srvs);
}

//---------------------------------------------------------------------------
//! シェーダーの設定
//---------------------------------------------------------------------------
void setShader(raw_ptr<dx11::Shader> shader)
{
    ID3D11PixelShader* ps = (shader) ? static_cast<ID3D11PixelShader*>(*shader) : nullptr;
    dx11::context()->PSSetShader(ps, nullptr, 0);
    if(shader && shaderPs_ != shader) {
        shaderPs_ = shader;
    }
}

//---------------------------------------------------------------------------
//! サンプラーステートの設定
//---------------------------------------------------------------------------
void setSamplerState(u32 slot, raw_ptr<ID3D11SamplerState> sampler)
{
    ID3D11SamplerState* samplers[]{ sampler.get() };
    dx11::context()->PSSetSamplers(slot, 1, samplers);
}

}   // namespace ps

//@}
//===========================================================================
//! @defgroup   ジオメトリシェーダー
//===========================================================================
//@{

namespace gs {

//---------------------------------------------------------------------------
//! シェーダーの設定
//---------------------------------------------------------------------------
void setShader(raw_ptr<dx11::Shader> shader)
{
    ID3D11GeometryShader* gs = (shader) ? static_cast<ID3D11GeometryShader*>(*shader) : nullptr;
    dx11::context()->GSSetShader(gs, nullptr, 0);
    if(shader && shaderGs_ != shader) {
        shaderGs_ = shader;
    }
}

//---------------------------------------------------------------------------
//! テクスチャ(Shader Resource View)の設定
//---------------------------------------------------------------------------
void setTexture(u32 slot, raw_ptr<dx11::Texture> texture)
{
    void preDraw();

    ID3D11ShaderResourceView* srvs[]{ *texture };
    dx11::context()->GSSetShaderResources(slot, 1, srvs);
}

//---------------------------------------------------------------------------
//! サンプラーステートの設定
//---------------------------------------------------------------------------
void setSamplerState(u32 slot, raw_ptr<ID3D11SamplerState> sampler)
{
    ID3D11SamplerState* samplers[]{ sampler.get() };
    dx11::context()->GSSetSamplers(slot, 1, samplers);
}

}   // namespace gs

//@}
//===========================================================================
//! @defgroup   コンピュートシェーダー
//===========================================================================
//@{

namespace cs {

//---------------------------------------------------------------------------
//! テクスチャ(Shader Resource View)の設定
//---------------------------------------------------------------------------
void setTexture(u32 slot, raw_ptr<dx11::Texture> texture)
{
    preDraw();

    ID3D11ShaderResourceView* srvs[]{ *texture };
    dx11::context()->CSSetShaderResources(slot, 1, srvs);
}

//---------------------------------------------------------------------------
//! リードライトテクスチャ(Unordered Access View)の設定
//---------------------------------------------------------------------------
void setTextureRW(u32 slot, raw_ptr<dx11::Texture> texture)
{
    ID3D11UnorderedAccessView* uavs[]{ *texture };
    dx11::context()->CSSetUnorderedAccessViews(slot, 1, uavs, nullptr);
}

//---------------------------------------------------------------------------
//! シェーダーの設定
//---------------------------------------------------------------------------
void setShader(raw_ptr<dx11::Shader> shader)
{
    dx11::context()->CSSetShader((shader) ? static_cast<ID3D11ComputeShader*>(*shader) : nullptr, nullptr, 0);

    ID3D11ComputeShader* cs = (shader) ? static_cast<ID3D11ComputeShader*>(*shader) : nullptr;
    dx11::context()->CSSetShader(cs, nullptr, 0);
    if(shader && shaderCs_ != shader) {
        shaderCs_ = shader;
    }
}

//---------------------------------------------------------------------------
//! サンプラーステートの設定
//---------------------------------------------------------------------------
void setSamplerState(u32 slot, raw_ptr<ID3D11SamplerState> sampler)
{
    ID3D11SamplerState* samplers[]{ sampler.get() };
    dx11::context()->CSSetSamplers(slot, 1, samplers);
}

}   // namespace cs

//@}
//===========================================================================
//! @defgroup   ラスタライザー関連
//===========================================================================
//@{

//---------------------------------------------------------------------------
//! 描画ターゲットの設定
//---------------------------------------------------------------------------
void setRenderTarget(u32 slot, raw_ptr<dx11::Texture> colorTarget)
{
    ID3D11RenderTargetView* rtv = (colorTarget) ? static_cast<ID3D11RenderTargetView*>(*colorTarget) : nullptr;
    if(d3dRenderTargets_[slot].Get() != rtv) {
        d3dRenderTargets_[slot] = rtv;
        isChangedRenderTargets_ = true;   // 変更を記録

        if(rtv) {
            // ビューポート設定（描画ターゲットの変更時はビューポートの更新も行う）
            D3D11_VIEWPORT viewport;
            viewport.TopLeftX = 0.0f;                                            // 左上X
            viewport.TopLeftY = 0.0f;                                            // 左上Y
            viewport.Width    = static_cast<f32>(colorTarget->desc().width_);    // 幅
            viewport.Height   = static_cast<f32>(colorTarget->desc().height_);   // 高さ
            viewport.MinDepth = 0.0f;                                            // Z最小値
            viewport.MaxDepth = 1.0f;                                            // Z最大値

            dx11::setViewport(&viewport);
        }
    }
}

//---------------------------------------------------------------------------
//! デプスステンシルの設定
//---------------------------------------------------------------------------
void setDepthStencil(raw_ptr<dx11::Texture> depthStencil)
{
    ID3D11DepthStencilView* dsv = (depthStencil) ? static_cast<ID3D11DepthStencilView*>(*depthStencil) : nullptr;
    if(d3dDepthStencil_.Get() != dsv) {
        d3dDepthStencil_        = dsv;
        isChangedRenderTargets_ = true;   // 変更を記録

        if(dsv) {
            // ビューポート設定（描画ターゲットの変更時はビューポートの更新も行う）
            D3D11_VIEWPORT viewport;
            viewport.TopLeftX = 0.0f;                                             // 左上X
            viewport.TopLeftY = 0.0f;                                             // 左上Y
            viewport.Width    = static_cast<f32>(depthStencil->desc().width_);    // 幅
            viewport.Height   = static_cast<f32>(depthStencil->desc().height_);   // 高さ
            viewport.MinDepth = 0.0f;                                             // Z最小値
            viewport.MaxDepth = 1.0f;                                             // Z最大値

            dx11::setViewport(&viewport);
        }
    }
}

//---------------------------------------------------------------------------
// ビューポートの設定
//---------------------------------------------------------------------------
void setViewport(D3D11_VIEWPORT* viewport)
{
    // ここでは直接設定は行わず、preDraw()内で反映
    d3dViewport_        = *viewport;
    isChangedViewports_ = true;
}

//---------------------------------------------------------------------------
//! シザー領域の設定
//---------------------------------------------------------------------------
void setScissorRect(const RECT& rect)
{
    dx11::context()->RSSetScissorRects(1, &rect);
}

//@}
//===========================================================================
//! @defgroup   ステート設定関連
//===========================================================================
//@{

//---------------------------------------------------------------------------
//! ブレンドステートの設定
//---------------------------------------------------------------------------
void setBlendState(raw_ptr<ID3D11BlendState> d3dBlendState, float4 blendFactor, u32 sampleMask)
{
    dx11::context()->OMSetBlendState(d3dBlendState.get(), blendFactor.f32, sampleMask);
}

//---------------------------------------------------------------------------
// デプスステンシルステートの設定
//---------------------------------------------------------------------------
void setDepthStencilState(raw_ptr<ID3D11DepthStencilState> d3dDepthStencilState, u32 stencilRef)
{
    dx11::context()->OMSetDepthStencilState(d3dDepthStencilState.get(), stencilRef);
}

//---------------------------------------------------------------------------
// ラスタライザーステートの設定
//---------------------------------------------------------------------------
void setRasterizerState(raw_ptr<ID3D11RasterizerState> d3dRasterizerState)
{
    dx11::context()->RSSetState(d3dRasterizerState.get());
}

//@}
//===========================================================================
//! @defgroup   バッファ関連
//===========================================================================
//@{

//---------------------------------------------------------------------------
//! カラーのクリア
//---------------------------------------------------------------------------
void clearColor(raw_ptr<dx11::Texture> colorTarget, float4 clearColor)
{
    f32 color[4]{ clearColor.x, clearColor.y, clearColor.z, clearColor.w };
    dx11::context()->ClearRenderTargetView(*colorTarget, color);
}

//---------------------------------------------------------------------------
//! デプスバッファのクリア
//---------------------------------------------------------------------------
void clearDepth(raw_ptr<dx11::Texture> depthStencilTarget, f32 clearDepth)
{
    dx11::context()->ClearDepthStencilView(*depthStencilTarget, D3D11_CLEAR_DEPTH, clearDepth, 0);
}

//---------------------------------------------------------------------------
//! ステンシルバッファのクリア
//---------------------------------------------------------------------------
void clearStencil(raw_ptr<dx11::Texture> depthStencilTarget, u8 clearStencil)
{
    dx11::context()->ClearDepthStencilView(*depthStencilTarget, D3D11_CLEAR_STENCIL, 1.0f, clearStencil);
}

//@}
//===========================================================================
//! @defgroup   描画発行
//===========================================================================
//@{

//---------------------------------------------------------------------------
// 描画発行前のパラメーター反映
//---------------------------------------------------------------------------
void preDraw()
{
    //----------------------------------------------------------
    // 描画ターゲットとデプスステンシルの設定
    //----------------------------------------------------------
    if(isChangedRenderTargets_) {
        ID3D11RenderTargetView* rtvs[D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT]{};
        ID3D11DepthStencilView* dsv = d3dDepthStencil_.Get();
        // 先頭から有効なスロット数を数える
        u32 slotCount = 0;
        for(; slotCount < d3dRenderTargets_.size() && d3dRenderTargets_[slotCount]; ++slotCount) {
            rtvs[slotCount] = d3dRenderTargets_[0].Get();
        }

        // 設定
        dx11::context()->OMSetRenderTargets(slotCount, rtvs, dsv);
    }

    // ビューポート設定（描画ターゲットの変更時はビューポートの更新も行う）
    if(isChangedViewports_) {
        dx11::context()->RSSetViewports(1, &d3dViewport_);
    }

    // 入力レイアウト設定
    if(isChangedInputLayout_) {
        if(inputLayout_) {
            // 入力レイアウト要素と頂点シェーダーの組み合わせで即時生成
            auto* d3dInputLayout = inputLayout_->d3dInputLayout(shaderVs_);
            dx11::context()->IASetInputLayout(d3dInputLayout);
        }
        else {
            dx11::context()->IASetInputLayout(nullptr);
        }
    }

    // 頂点バッファ設定
    if(isChangedVertices_.any()) {
        // 入力レイアウトからデフォルト値を取得
        auto defaultStride = inputLayout_ ? inputLayout_->vertexStride() : 0;

        // 頂点バッファ
        for(u32 i = 0; i < isChangedVertices_.size(); ++i) {
            if(!isChangedVertices_[i])
                continue;

            ID3D11Buffer* d3dBuffer = d3dBufferVb_[i].get();
            auto          stride    = vertexStride_[i] ? vertexStride_[i] : defaultStride;
            auto          offset    = vertexOffset_[i];

            dx11::context()->IASetVertexBuffers(i, 1, &d3dBuffer, &stride, &offset);
        }
    }

    // インデックスバッファ設定
    if(isChangedIndices_) {
        ID3D11Buffer* d3dBuffer = d3dBufferIb_.get();
        // 32bit形式インデックス
        dx11::context()->IASetIndexBuffer(d3dBuffer, DXGI_FORMAT_R32_UINT, indexOffset_);
    }

    //----------------------------------------------------------
    // シェーダーの定数バッファ設定
    // スロット番号と定数バッファのペアを取得して設定
    //----------------------------------------------------------
    if(shaderVs_) {   // VS 頂点シェーダー
        for(auto& x : *shaderVs_->constantBufferList()) {
            auto* cb   = static_cast<ID3D11Buffer*>(*x.first);
            auto& slot = x.second;
            dx11::context()->VSSetConstantBuffers(slot, 1, &cb);
        }
    }
    if(shaderPs_) {   // PS ピクセルシェーダー
        for(auto& x : *shaderPs_->constantBufferList()) {
            auto* cb   = static_cast<ID3D11Buffer*>(*x.first);
            auto& slot = x.second;
            dx11::context()->PSSetConstantBuffers(slot, 1, &cb);
        }
    }
    if(shaderGs_) {   // GS ジオメトリシェーダー
        for(auto& x : *shaderGs_->constantBufferList()) {
            auto* cb   = static_cast<ID3D11Buffer*>(*x.first);
            auto& slot = x.second;
            dx11::context()->PSSetConstantBuffers(slot, 1, &cb);
        }
    }
    if(shaderCs_) {   // CS コンピュートシェーダー
        for(auto& x : *shaderGs_->constantBufferList()) {
            auto* cb   = static_cast<ID3D11Buffer*>(*x.first);
            auto& slot = x.second;
            dx11::context()->CSSetConstantBuffers(slot, 1, &cb);
        }
    }

    // キャッシュ適用済にする
    isChangedRenderTargets_ = false;
    isChangedViewports_     = false;
    isChangedInputLayout_   = false;
    isChangedVertices_      = 0;
    isChangedIndices_       = false;
}

//---------------------------------------------------------------------------
// インデックス指定で描画発行
//---------------------------------------------------------------------------
void drawIndexed(dx11::Primitive primitiveType, u32 indexCount, u32 startIndexLocation, s32 baseVertexLocation)
{
    preDraw();
    dx11::context()->IASetPrimitiveTopology(dx11::makePrimitiveTopology(primitiveType));
    dx11::context()->DrawIndexed(indexCount,            // [in] インデックス数
                                 startIndexLocation,    // [in] インデックバッファの開始位置（番号指定）
                                 baseVertexLocation);   // [in] 頂点バッファ　　　の開始位置（番号指定）
}

//---------------------------------------------------------------------------
//! 描画発行
//---------------------------------------------------------------------------
void draw(dx11::Primitive primitiveType, u32 vertexCount, s32 startVertexLocation)
{
    preDraw();
    dx11::context()->IASetPrimitiveTopology(dx11::makePrimitiveTopology(primitiveType));
    dx11::context()->Draw(vertexCount, startVertexLocation);
}

//---------------------------------------------------------------------------
//! インデックス指定でインスタンス描画発行
//---------------------------------------------------------------------------
void drawIndexedInstanced(
    u32 indexCountPerInstance,
    u32 instanceCount,
    u32 startIndexLocation,
    s32 baseVertexLocation,
    u32 startInstanceLocation)
{
    preDraw();
    dx11::context()->DrawIndexedInstanced(
        indexCountPerInstance,
        instanceCount,
        startIndexLocation,
        baseVertexLocation,
        startInstanceLocation);
}

//---------------------------------------------------------------------------
//! インスタンス描画発行
//---------------------------------------------------------------------------
void drawInstanced(u32 vertexCountPerInstance, u32 instanceCount, u32 startVertexLocation, u32 startInstanceLocation)
{
    preDraw();
    dx11::context()->DrawInstanced(
        vertexCountPerInstance,
        instanceCount,
        startVertexLocation,
        startInstanceLocation);
}

//---------------------------------------------------------------------------
//! インデックス指定でインスタンス描画Indirect発行
//---------------------------------------------------------------------------
void drawIndexedInstancedIndirect(raw_ptr<dx11::Buffer> bufferForArgs, u32 offset)
{
    preDraw();
    dx11::context()->DrawIndexedInstancedIndirect(*bufferForArgs, offset);
}

//---------------------------------------------------------------------------
//! インスタンス描画Indirect発行
//---------------------------------------------------------------------------
void drawInstancedIndirect(raw_ptr<dx11::Buffer> bufferForArgs, u32 offset)
{
    preDraw();
    dx11::context()->DrawInstancedIndirect(*bufferForArgs, offset);
}

//---------------------------------------------------------------------------
//! ユーザープリミティブ描画
//---------------------------------------------------------------------------
void drawUserPrimitive(dx11::Primitive type, u32 vertexCount, const void* vertices, u32 vertexStride)
{
    preDraw();
    graphics::DynamicVertex::instance().drawUserPrimitive(
        type,
        vertexCount,
        vertices,
        vertexStride);
}

//---------------------------------------------------------------------------
//! インデックスありユーザープリミティブ描画
//---------------------------------------------------------------------------
void drawIndexedUserPrimitive(dx11::Primitive type, u32 vertexCount, u32 indexCount, const void* indices, const void* vertices, u32 vertexStride)
{
    preDraw();
    graphics::DynamicVertex::instance().drawIndexedUserPrimitive(
        type,
        vertexCount,
        indexCount,
        indices,
        vertices,
        vertexStride);
}

//@}
//===========================================================================
//! @defgroup コンピュート発行(コンピュートシェーダー実行)
//===========================================================================
//@{

//---------------------------------------------------------------------------
//! コンピュート実行
//---------------------------------------------------------------------------
void dispatch(u32 threadGroupCountX, u32 threadGroupCountY, u32 threadGroupCountZ)
{
    dx11::context()->Dispatch(threadGroupCountX, threadGroupCountY, threadGroupCountZ);
}

//---------------------------------------------------------------------------
//! コンピュートIndirect実行
//---------------------------------------------------------------------------
void dispatchIndirect(raw_ptr<dx11::Buffer> bufferForArgs, u32 offset)
{
    dx11::context()->DispatchIndirect(*bufferForArgs, offset);
}

//@}
//===========================================================================
//! @defgroup 定数バッファ
//===========================================================================
//@{

//---------------------------------------------------------------------------
// 定数バッファを設定
//---------------------------------------------------------------------------
void setConstantBuffer(const char* name, const void* data, size_t size)
{
    std::shared_ptr<dx11::Buffer> cb = dx11::getShaderConstantBuffer(name);
    if(!cb)
        return;
    ASSERT_MESSAGE(cb, "定数バッファ定義が見つかりません。名前の確認をしてください。");
    ASSERT_MESSAGE(cb->desc().size_ == size, "定数バッファ定義と書き込みサイズが一致しません。シェーダー側の定義も確認してください。");

    auto p = cb->map();
    if(p) {
        memcpy_s(p, cb->desc().size_, data, size);
        cb->unmap();
    }
}

//@}

}   // namespace dx11
