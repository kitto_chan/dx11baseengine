﻿//---------------------------------------------------------------------------
//!	@file	dx11_helper.h
//!	@brief	DirectX11ヘルパーユーティリティー
//---------------------------------------------------------------------------
#pragma once

namespace dx11 {
enum class eSamplerState
{
    PointWrap,
    LinearWrap,
    // Todo.
};

// ヘルパーユーティリティーのキャッシュを破棄
void invalidateHelperCache();

//===========================================================================
//! @defgourp   頂点入力
//===========================================================================
//@{

//! 入力レイアウト設定
//! @param  [in]    inputLayout 入力レイアウトオブジェクト
void setInputLayout(raw_ptr<dx11::InputLayout> inputLayout);

//! 入力レイアウトを取得
raw_ptr<dx11::InputLayout> getInputLayout();

//! 頂点バッファを設定
//! @param  [in]    slot    スロット番号(0-15)
//! @param  [in]    buffer  バッファオブジェクト
//! @param  [in]    stride  1頂点あたりのサイズ(省略時はInputLayoutから自動計算)
//! @param  [in]    offset  先頭からのオフセット(単位:バイト)
void setVertexBuffer(u32 slot, raw_ptr<dx11::Buffer> buffer, u32 stride = 0, u32 offset = 0);

//! インデックスバッファを設定
//! @param  [in]    buffer  バッファオブジェクト
//! @param  [in]    offset  先頭からのオフセット(単位:バイト)
void setIndexBuffer(raw_ptr<dx11::Buffer> buffer, u32 offset = 0);

//@}
//===========================================================================
//! @defgroup   頂点シェーダー
//===========================================================================
//@{

namespace vs {

//! シェーダーの設定
//! @param  [in]    shader  シェーダーオブジェクト(nullptrで無効化)
void setShader(raw_ptr<dx11::Shader> shader);

//! テクスチャ(Shader Resource View)の設定
//! @param  [in]    slot    テクスチャスロット番号(t0～)
//! @param  [in]    texture テクスチャオブジェクト(nullptrで解除)
void setTexture(u32 slot, raw_ptr<dx11::Texture> texture);

//! サンプラーステートの設定
//! @param  [in]    slot    サンプラースロット番号(t0～)
//! @param  [in]    sampler サンプラーオブジェクト(nullptrで解除)
void setSamplerState(u32 slot, raw_ptr<ID3D11SamplerState> sampler);

}   // namespace vs

//@}
//===========================================================================
//! @defgroup   ピクセルシェーダー
//===========================================================================
//@{

namespace ps {

//! テクスチャ(Shader Resource View)の設定
//! @param  [in]    slot    テクスチャスロット番号(t0～)
//! @param  [in]    texture テクスチャオブジェクト(nullptrで解除)
void setTexture(u32 slot, raw_ptr<dx11::Texture> texture);

//! シェーダーの設定
//! @param  [in]    shader  シェーダーオブジェクト(nullptrで無効化)
void setShader(raw_ptr<dx11::Shader> shader);

//! サンプラーステートの設定
//! @param  [in]    slot    サンプラースロット番号(t0～)
//! @param  [in]    sampler サンプラーオブジェクト(nullptrで解除)
void setSamplerState(u32 slot, raw_ptr<ID3D11SamplerState> sampler);

}   // namespace ps

//@}
//===========================================================================
//! @defgroup   ジオメトリシェーダー
//===========================================================================
//@{

namespace gs {

//! シェーダーの設定
//! @param  [in]    shader  シェーダーオブジェクト(nullptrで無効化)
void setShader(raw_ptr<dx11::Shader> shader);

//! テクスチャ(Shader Resource View)の設定
//! @param  [in]    slot    テクスチャスロット番号(t0～)
//! @param  [in]    texture テクスチャオブジェクト(nullptrで解除)
void setTexture(u32 slot, raw_ptr<dx11::Texture> texture);

//! サンプラーステートの設定
//! @param  [in]    slot    サンプラースロット番号(t0～)
//! @param  [in]    sampler サンプラーオブジェクト(nullptrで解除)
void setSamplerState(u32 slot, raw_ptr<ID3D11SamplerState> sampler);

}   // namespace gs

//@}
//===========================================================================
//! @defgroup   コンピュートシェーダー
//===========================================================================
//@{

namespace cs {

//! テクスチャ(Shader Resource View)の設定
//! @param  [in]    slot    テクスチャスロット番号(t0～)
//! @param  [in]    texture テクスチャオブジェクト(nullptrで解除)
void setTexture(u32 slot, raw_ptr<dx11::Texture> texture);

//! リードライトテクスチャ(Unordered Access View)の設定
//! @param  [in]    slot    UAVスロット番号(u0～)
//! @param  [in]    texture テクスチャオブジェクト(nullptrで解除)
void setTextureRW(u32 slot, raw_ptr<dx11::Texture> texture);

//! シェーダーの設定
//! @param  [in]    shader  シェーダーオブジェクト(nullptrで無効化)
void setShader(raw_ptr<dx11::Shader> shader);

//! サンプラーステートの設定
//! @param  [in]    slot    サンプラースロット番号(t0～)
//! @param  [in]    sampler サンプラーオブジェクト(nullptrで解除)
void setSamplerState(u32 slot, raw_ptr<ID3D11SamplerState> sampler);

}   // namespace cs

//@}
//===========================================================================
//! @defgroup   ラスタライザー関連
//===========================================================================
//@{

//! 描画ターゲットの設定
//! @param  [in]    slot            RenderTargetスロット番号(0-7)
//! @param  [in]    colorTarget     描画ターゲットテクスチャ
void setRenderTarget(u32 slot, raw_ptr<dx11::Texture> colorTarget);

//! デプスステンシルの設定
//! @param  [in]    depthStencil    デプスステンシルテクスチャ
void setDepthStencil(raw_ptr<dx11::Texture> depthStencil);

// ビューポートの設定
//! @param  [in]    viewport    ビューポート
void setViewport(D3D11_VIEWPORT* viewport);

//! シザー領域の設定
//! @param  [in]    rect    シザー矩形
void setScissorRect(const RECT& rect);

//@}
//===========================================================================
//! @defgroup   ステート設定関連
//===========================================================================
//@{

//! ブレンドステートの設定
//! @param  [in]    d3dBlendState   ブレンドステート
//! @param  [in]    blendFactor     ブレンド係数(定数値を使用する場合のみ有効)
//! @param  [in]    sampleMask      ピクセルサンプルマスク(マルチサンプル利用時のみ)
void setBlendState(raw_ptr<ID3D11BlendState> d3dBlendState, float4 blendFactor = float4(1.0f, 1.0f, 1.0f, 1.0f), u32 sampleMask = 0xfffffffful);

// デプスステンシルステートの設定
//! @param  [in]    d3dDepthStencilState    デプスステンシルステート
//! @param  [in]    stencilRef              ステンシルバッファの参照値(ステンシル利用時のみ)
void setDepthStencilState(raw_ptr<ID3D11DepthStencilState> d3dDepthStencilState, u32 stencilRef = 0);

// ラスタライザーステートの設定
void setRasterizerState(raw_ptr<ID3D11RasterizerState> d3dRasterizerState);

//@}
//===========================================================================
//! @defgroup   バッファ関連
//===========================================================================
//@{

//! カラーのクリア
//! @param  [in]    colorTarget カラーRenderTargetテクスチャ
//! @param  [in]    clearColor  クリアカラー
void clearColor(raw_ptr<dx11::Texture> colorTarget, float4 clearColor);

//! デプスバッファのクリア
//! @param  [in]    depthStencilTarget  デプスステンシルテクスチャ
//! @param  [in]    clearDepth          クリアデプス値
void clearDepth(raw_ptr<dx11::Texture> depthStencilTarget, f32 clearDepth);

//! ステンシルバッファのクリア
//! @param  [in]    depthStencilTarget  デプスステンシルテクスチャ
//! @param  [in]    clearStencil        クリアステンシル値
void clearStencil(raw_ptr<dx11::Texture> depthStencilTarget, u8 clearStencil);

//@}
//===========================================================================
//! @defgroup   描画発行
//===========================================================================
//@{

// インデックス指定で描画発行
//! @param  [in]    primitiveType       プリミティブの種類
//! @param  [in]    indexCount          インデックス数
//! @param  [in]    startIndexLocation  インデックバッファの開始位置（番号指定）
//! @param  [in]    startVertexLocation 頂点バッファの開始位置（番号指定）
void drawIndexed(dx11::Primitive primitiveType, u32 indexCount, u32 startIndexLocation = 0, s32 baseVertexLocation = 0);

//! 描画発行
//! @param  [in]    primitiveType       プリミティブの種類
//! @param  [in]    vertexCount         頂点数
//! @param  [in]    startVertexLocation 頂点バッファの開始位置（番号指定）
void draw(dx11::Primitive primitiveType, u32 vertexCount, s32 startVertexLocation = 0);

//! インデックス指定でインスタンス描画発行
//! @param  [in]    indexCountPerInstance   インスタンスあたりのインデックス数
//! @param  [in]    instanceCount           インスタンス数
//! @param  [in]    startIndexLocation      インデックバッファの開始位置（番号指定）
//! @param  [in]    baseVertexLocation      頂点バッファの開始位置（番号指定）
//! @param  [in]    startInstanceLocation   インスタンスの開始位置（番号指定）
void drawIndexedInstanced(u32 indexCountPerInstance, u32 instanceCount, u32 startIndexLocation, s32 baseVertexLocation = 0, u32 startInstanceLocation = 0);

//! インスタンス描画発行
//! @param  [in]    vertexCountPerInstance  インスタンスあたりの頂点数
//! @param  [in]    instanceCount           インスタンス数
//! @param  [in]    startVertexLocation     頂点バッファの開始位置（番号指定）
//! @param  [in]    startInstanceLocation   インスタンスの開始位置（番号指定）
void drawInstanced(u32 vertexCountPerInstance, u32 instanceCount, u32 startVertexLocation = 0, u32 startInstanceLocation = 0);

//! インデックス指定でインスタンス描画Indirect発行
//! @param  [in]    bufferForArgs       引数用のバッファ
//! @param  [in]    offset              引数がある位置への先頭からのオフセット
//! @note drawIndexedInstanced()実行をバッファ内の引数で起動します
void drawIndexedInstancedIndirect(raw_ptr<dx11::Buffer> bufferForArgs, u32 offset = 0);

//! インスタンス描画Indirect発行
//! @param  [in]    bufferForArgs       引数用のバッファ
//! @param  [in]    offset              引数がある位置への先頭からのオフセット
//! @note drawInstanced()実行をバッファ内の引数で起動します
void drawInstancedIndirect(raw_ptr<dx11::Buffer> bufferForArgs, u32 offset = 0);

//! ユーザープリミティブ描画
//! @param  [in]    type            プリミティブの種類
//! @param  [in]    vertexCount     プリミティブ数
//! @param  [in]    vertices        頂点配列の先頭
//! @param  [in]    vertexStride    1頂点あたりのサイズ(省略時はInputLayoutから自動計算)
void drawUserPrimitive(dx11::Primitive type, u32 vertexCount, const void* vertices, u32 vertexStride = 0);

//! インデックスありユーザープリミティブ描画
//! @param  [in]    type            プリミティブの種類
//! @param  [in]    vertexCount     頂点数
//! @param  [in]    indexCount      インデックス数
//! @param  [in]    indices         インデックス配列の先頭
//! @param  [in]    vertices        頂点配列の先頭
//! @param  [in]    vertexStride    1頂点あたりのサイズ(省略時はInputLayoutから自動計算)
void drawIndexedUserPrimitive(dx11::Primitive type, u32 vertexCount, u32 indexCount, const void* indices, const void* vertices, u32 vertexStride = 0);

//@}
//===========================================================================
//! @defgroup コンピュート発行(コンピュートシェーダー実行)
//===========================================================================
//@{

//! コンピュート実行
//! @param  [in]    threadGroupCountX   スレッドグループ数X
//! @param  [in]    threadGroupCountY   スレッドグループ数Y
//! @param  [in]    threadGroupCountZ   スレッドグループ数Z
void dispatch(u32 threadGroupCountX, u32 threadGroupCountY, u32 threadGroupCountZ);

//! コンピュートIndirect実行
//! @param  [in]    bufferForArgs       引数用のバッファ
//! @param  [in]    offset              引数がある位置への先頭からのオフセット
//! @note dispatch()実行をバッファ内の引数で起動します
void dispatchIndirect(raw_ptr<dx11::Buffer> bufferForArgs, u32 offset = 0);

//@}
//===========================================================================
//! @defgroup 定数バッファ
//===========================================================================
//@{

// 定数バッファを設定
//! @param  [in]    name    定数バッファの名前
//! @param  [in]    p       設定するデーターの先頭アドレス
//! @param  [in]    size    設定するデーターのバイトサイズ
void setConstantBuffer(const char* name, const void* data, size_t size);

// 定数バッファを設定
//! @param  [in]    name    定数バッファの名前
//! @param  [in]    data    設定するデーター
template<typename T>
void setConstantBuffer(const char* name, const T& data)
{
    dx11::setConstantBuffer(name, &data, sizeof(T));
}

//@}

}   // namespace dx11
