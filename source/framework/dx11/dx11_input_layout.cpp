﻿//---------------------------------------------------------------------------
//!	@file	dx11_input_layout.cpp
//!	@brief	入力レイアウト
//---------------------------------------------------------------------------
#include "dx11_input_layout.h"

namespace dx11 {

//===========================================================================
// 入力レイアウト実装部
//===========================================================================
class InputLayoutImpl final : public dx11::InputLayout
{
public:
    //! コンストラクタ
    InputLayoutImpl(const D3D11_INPUT_ELEMENT_DESC* elements, size_t elementCount);

    virtual ~InputLayoutImpl() = default;

    //! 入力要素配列を取得
    virtual const std::vector<D3D11_INPUT_ELEMENT_DESC>* d3dInputElements() const override { return &elements_; }

    //! ハッシュ値を取得
    virtual u64 hash() const override { return hash_; }

    //! 頂点シェーダーに対応するD3D入力レイアウトを取得
    //! @param  [in]    shaderVs    頂点シェーダー
    virtual ID3D11InputLayout* d3dInputLayout(raw_ptr<dx11::Shader> shaderVs) const override;

    //! 頂点サイズを取得
    virtual u32 vertexStride() const override { return vertexStride_; }

private:
    // コピー禁止/move禁止
    InputLayoutImpl(const InputLayoutImpl&) = delete;
    InputLayoutImpl(InputLayoutImpl&&)      = delete;
    InputLayoutImpl& operator=(const InputLayoutImpl&) = delete;
    InputLayoutImpl& operator=(InputLayoutImpl&&) = delete;

private:
    std::vector<D3D11_INPUT_ELEMENT_DESC> elements_;           //!< 入力要素配列
    u64                                   hash_         = 0;   //!< 要素のハッシュ値
    u32                                   vertexStride_ = 0;   //!< 頂点サイズ

    // 入力レイアウトプール
    mutable std::unordered_map<u64, com_ptr<ID3D11InputLayout>> d3dInputLayoutPool_;    //< 入力レイアウトの全体プール
    mutable u64                                                 shaderVsHash_ = 0;      //< 直前で取得した頂点シェーダーのハッシュ値(キャッシュ)
    mutable ID3D11InputLayout*                                  d3dInputLayoutCache_;   //< 直前で取得した頂点シェーダーに対応するD3D入力レイアウト(キャッシュ)
};

//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
InputLayoutImpl::InputLayoutImpl(const D3D11_INPUT_ELEMENT_DESC* elements, size_t elementCount)
{
    // 要素の保存
    for(u32 i = 0; i < elementCount; ++i) {
        elements_.push_back(elements[i]);
    }

    // ハッシュ値の計算
    std::string_view s(reinterpret_cast<const char*>(elements), sizeof(D3D11_INPUT_ELEMENT_DESC) * elementCount);
    hash_ = std::hash<std::string_view>()(s);

    // 頂点サイズの計算
    for(auto& x : elements_) {
        u32 offset    = x.AlignedByteOffset;
        u32 size      = static_cast<u32>(dx11::bpp(x.Format) / 8);   // bit数 → byte数
        vertexStride_ = std::max(vertexStride_, offset + size);
    }
}

//---------------------------------------------------------------------------
//! 頂点シェーダーに対応するD3D入力レイアウトを取得
//---------------------------------------------------------------------------
ID3D11InputLayout* InputLayoutImpl::d3dInputLayout(raw_ptr<dx11::Shader> shaderVs) const
{
    // 直前キャッシュと同じものを参照した場合は検索せず即時取得
    if(shaderVs->hash() == shaderVsHash_) {
        return d3dInputLayoutCache_;
    }
    // プールから検索して見つかった場合はそのまま使用
    com_ptr<ID3D11InputLayout> d3dInputLayout;

    auto it = d3dInputLayoutPool_.find(shaderVs->hash());
    if(it == std::end(d3dInputLayoutPool_)) {
        // 見つからなかったら新規作成
        if(FAILED(dx11::d3dDevice()->CreateInputLayout(
               elements_.data(),                     // [in]  入力要素配列の先頭
               static_cast<u32>(elements_.size()),   // [in]  要素数
               shaderVs->byteCode(),                 // [in]  頂点シェーダーのバイトコード先頭
               shaderVs->byteCodeSize(),             // [in]  頂点シェーダーのバイトコードサイズ
               &d3dInputLayout))) {                  // [out] 作成されたD3D入力レイアウト
            return nullptr;
        }
    }
    else {
        // 見つかった場合
        d3dInputLayout = it->second;
    }
    // キャッシュに登録
    d3dInputLayoutCache_ = d3dInputLayout.Get();
    shaderVsHash_        = shaderVs->hash();

    // 新規登録
    d3dInputLayoutPool_[shaderVsHash_] = d3dInputLayout;

    return d3dInputLayout.Get();
}

//---------------------------------------------------------------------------
//! 入力レイアウトを作成
//---------------------------------------------------------------------------
std::shared_ptr<dx11::InputLayout> createInputLayout(const D3D11_INPUT_ELEMENT_DESC* elements, size_t elementCount)
{
    return std::make_shared<dx11::InputLayoutImpl>(elements, elementCount);
}

}   // namespace dx11
