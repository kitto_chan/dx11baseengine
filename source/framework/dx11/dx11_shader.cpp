﻿//---------------------------------------------------------------------------
//!	@file   dx11_shader.cpp
//!	@brief	GPUシェーダー
//---------------------------------------------------------------------------
#include "dx11_shader.h"
#include "dx11_constant_buffer_pool.h"
#include <d3dcompiler.h>
#include <iostream>
#include <fstream>

namespace dx11 {

//===========================================================================
//! シェーダー内部実装
//===========================================================================
class ShaderImpl final : public dx11::Shader
{
public:
    // シェーダーの種類
    enum class Type
    {
        Unknown,   //!< 未定義
        Vs,        //!< 頂点シェーダー
        Ps,        //!< ピクセルシェーダー
        Gs,        //!< ジオメトリシェーダー
        Hs,        //!< ハルシェーダー
        Ds,        //!< ドメインシェーダー
        Cs,        //!< 演算シェーダー
    };

    ShaderImpl() = default;

    virtual ~ShaderImpl() = default;

    //! 初期化
    //! @param  [in]    byteCode        シェーダーバイトコードの先頭アドレス
    //! @param  [in]    byteCodeSize    シェーダーバイトコードのサイズ(単位:bytes)
    bool initialize(const void* byteCode, size_t byteCodeSize);

    //! シェーダーリフレクション情報を構築
    bool buildShaderReflection();

    //----------------------------------------------------------
    //! @name キャスト
    //----------------------------------------------------------
    //@{

    virtual operator ID3D11VertexShader*() const override { return d3dShaderVs_.Get(); }
    virtual operator ID3D11PixelShader*() const override { return d3dShaderPs_.Get(); }
    virtual operator ID3D11GeometryShader*() const override { return d3dShaderGs_.Get(); }
    virtual operator ID3D11HullShader*() const override { return d3dShaderHs_.Get(); }
    virtual operator ID3D11DomainShader*() const override { return d3dShaderDs_.Get(); }
    virtual operator ID3D11ComputeShader*() const override { return d3dShaderCs_.Get(); }

    //@}
    //----------------------------------------------------------
    //! @name 参照
    //----------------------------------------------------------
    //@{

    //! シェーダーバイトコードを取得
    virtual const void* byteCode() const override
    {
        return byteCode_.data();
    }

    //! シェーダーバイトコードのサイズを取得
    virtual size_t byteCodeSize() const override
    {
        return byteCode_.size();
    }

    //! ハッシュ値を取得
    virtual u64 hash() const override
    {
        return hash_;
    }

    //! 定数バッファリストを取得
    virtual const std::vector<Shader::ConstantBufferInfo>* constantBufferList() const override
    {
        return &constantBuffers_;
    }

    //@}
private:
    // コピー禁止/move禁止
    ShaderImpl(const ShaderImpl&) = delete;
    ShaderImpl(ShaderImpl&&)      = delete;
    ShaderImpl& operator=(const ShaderImpl&) = delete;
    ShaderImpl& operator=(ShaderImpl&&) = delete;

private:
    u64                    hash_ = 0;
    Type                   type_ = Type::Unknown;   //!< シェーダーの種類
    std::vector<std::byte> byteCode_;               //!< シェーダーバイトコード

    com_ptr<ID3D11VertexShader>   d3dShaderVs_;   //!< 頂点シェーダー
    com_ptr<ID3D11PixelShader>    d3dShaderPs_;   //!< ピクセルシェーダー
    com_ptr<ID3D11GeometryShader> d3dShaderGs_;   //!< ジオメトリシェーダー
    com_ptr<ID3D11HullShader>     d3dShaderHs_;   //!< ハルシェーダー
    com_ptr<ID3D11DomainShader>   d3dShaderDs_;   //!< ドメインシェーダー
    com_ptr<ID3D11ComputeShader>  d3dShaderCs_;   //!< 演算シェーダー

    // 定数バッファ情報
    std::vector<Shader::ConstantBufferInfo> constantBuffers_;
};

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool ShaderImpl::initialize(const void* byteCode, size_t byteCodeSize)
{
    //----------------------------------------------------------
    // バイトコードの保存
    //----------------------------------------------------------
    byteCode_.resize(byteCodeSize);
    memcpy(byteCode_.data(), byteCode, byteCodeSize);

    //! シェーダーリフレクション情報を構築
    if(!buildShaderReflection()) {
        return false;
    }
    // ハッシュ値の計算
    std::string_view s(reinterpret_cast<const char*>(byteCode_.data()), byteCode_.size());
    hash_ = std::hash<std::string_view>()(s);

    //----------------------------------------------------------
    // シェーダーの作成
    //----------------------------------------------------------
    switch(type_) {
        case Type::Vs:
            if(FAILED(dx11::d3dDevice()->CreateVertexShader(byteCode, byteCodeSize, nullptr, &d3dShaderVs_))) {
                return false;
            }
            break;
        case Type::Ps:
            if(FAILED(dx11::d3dDevice()->CreatePixelShader(byteCode, byteCodeSize, nullptr, &d3dShaderPs_))) {
                return false;
            }
            break;
        case Type::Gs:
            if(FAILED(dx11::d3dDevice()->CreateGeometryShader(byteCode, byteCodeSize, nullptr, &d3dShaderGs_))) {
                return false;
            }
            break;
        case Type::Hs:
            if(FAILED(dx11::d3dDevice()->CreateHullShader(byteCode, byteCodeSize, nullptr, &d3dShaderHs_))) {
                return false;
            }
            break;
        case Type::Ds:
            if(FAILED(dx11::d3dDevice()->CreateDomainShader(byteCode, byteCodeSize, nullptr, &d3dShaderDs_))) {
                return false;
            }
            break;
        case Type::Cs:
            if(FAILED(dx11::d3dDevice()->CreateComputeShader(byteCode, byteCodeSize, nullptr, &d3dShaderCs_))) {
                return false;
            }
            break;
        default:
            return false;
    }
    return true;
}

//---------------------------------------------------------------------------
//! シェーダーリフレクション情報を構築
//! シェーダーバージョンや内部のパラメーター定義を参照することができます
//! @note 現時点では必要最小限の参照のみ
//---------------------------------------------------------------------------
bool ShaderImpl::buildShaderReflection()
{
    //----------------------------------------------------------
    // シェーダーリフレクションの生成
    //----------------------------------------------------------
    com_ptr<ID3D11ShaderReflection> d3dShaderReflection;

    if(FAILED(D3DReflect(byteCode(), byteCodeSize(), IID_PPV_ARGS(&d3dShaderReflection)))) {
        return false;
    }

    D3D11_SHADER_DESC desc;
    d3dShaderReflection->GetDesc(&desc);

    //----------------------------------------------------------
    // シェーダーモデルバージョン
    //----------------------------------------------------------
    auto                  shaderType         = D3D11_SHVER_GET_TYPE(desc.Version);    // シェーダーの種類
    [[maybe_unused]] auto shaderVersionMajor = D3D11_SHVER_GET_MAJOR(desc.Version);   // メジャーバージョン
    [[maybe_unused]] auto shaderVersionMinor = D3D11_SHVER_GET_MINOR(desc.Version);   // マイナーバージョン

    switch(shaderType) {
        case D3D11_SHVER_PIXEL_SHADER: type_ = ShaderImpl::Type::Ps; break;
        case D3D11_SHVER_VERTEX_SHADER: type_ = ShaderImpl::Type::Vs; break;
        case D3D11_SHVER_GEOMETRY_SHADER: type_ = ShaderImpl::Type::Gs; break;
        case D3D11_SHVER_HULL_SHADER: type_ = ShaderImpl::Type::Hs; break;
        case D3D11_SHVER_DOMAIN_SHADER: type_ = ShaderImpl::Type::Ds; break;
        case D3D11_SHVER_COMPUTE_SHADER: type_ = ShaderImpl::Type::Cs; break;
        default: type_ = Type::Unknown; break;
    }

    //----------------------------------------------------------
    // 入力アトリビュート
    // セマンティックを取得できます
    //----------------------------------------------------------
    for(u32 i = 0; i < desc.InputParameters; ++i) {
        D3D11_SIGNATURE_PARAMETER_DESC paramDesc;
        d3dShaderReflection->GetInputParameterDesc(i, &paramDesc);

        [[maybe_unused]] auto semanticName  = paramDesc.SemanticName;    // セマンティック名
        [[maybe_unused]] auto semanticIndex = paramDesc.SemanticIndex;   // セマンティック番号
        [[maybe_unused]] auto registerIndex = paramDesc.Register;        // レジスタ番号

        [[maybe_unused]] D3D_REGISTER_COMPONENT_TYPE componentType = paramDesc.ComponentType;   // コンポーネントの種類(u32/s32/f32)

        // 要素数
        s32 elementCount = 0;   // 要素数
        if(paramDesc.Mask == 0b0001)
            elementCount = 1;
        else if(paramDesc.Mask == 0b0011)
            elementCount = 2;
        else if(paramDesc.Mask == 0b0111)
            elementCount = 3;
        else if(paramDesc.Mask == 0b1111)
            elementCount = 4;

        // 最小精度
        [[maybe_unused]] D3D_MIN_PRECISION minPrecision = paramDesc.MinPrecision;
    }

    //----------------------------------------------------------
    // 定数バッファ
    // 定数バッファのメンバ変数を取得できます
    //----------------------------------------------------------
    for(u32 i = 0; i < desc.ConstantBuffers; ++i) {
        auto reflectionCb = d3dShaderReflection->GetConstantBufferByIndex(i);

        // バッファ自体の情報を取得
        D3D11_SHADER_BUFFER_DESC bufferDesc;
        reflectionCb->GetDesc(&bufferDesc);

        //------------------------------------------------------
        // 定数バッファを自動生成
        //------------------------------------------------------
        // 定数バッファとスロット番号のペアを作成して登録
        std::shared_ptr<dx11::Buffer> cb   = dx11::createShaderConstantBuffer(bufferDesc.Name, bufferDesc.Size);
        u32                           slot = static_cast<u32>(-1);   // 登録時にはスロット番号はまだ決定していないため-1を入れておく

        Shader::ConstantBufferInfo bindSlot = std::make_pair(cb, slot);
        constantBuffers_.emplace_back(std::move(bindSlot));   // 新規登録

        //------------------------------------------------------
        // メンバ変数の情報を取得
        //------------------------------------------------------
        void* p = cb->map();   // メモリの先頭
        ASSERT_MESSAGE(p, "定数バッファの書き込みに失敗しました.");

        for(u32 n = 0; n < bufferDesc.Variables; ++n) {
            auto                       variable = reflectionCb->GetVariableByIndex(n);
            D3D11_SHADER_VARIABLE_DESC variableDesc;
            variable->GetDesc(&variableDesc);

            [[maybe_unused]] auto name         = variableDesc.Name;           // 変数名
            [[maybe_unused]] auto defaultValue = variableDesc.DefaultValue;   // デフォルト値
            [[maybe_unused]] auto offset       = variableDesc.StartOffset;    // 構造体先頭からのオフセット
            [[maybe_unused]] auto size         = variableDesc.Size;           // サイズ
            [[maybe_unused]] auto flags        = variableDesc.uFlags;         // フラグ (D3D_SHADER_VARIABLE_FLAGSの組み合わせ)

            // デフォルト値が設定されている場合は書き込み
            if(defaultValue) {
                memcpy_s(reinterpret_cast<void*>(reinterpret_cast<uintptr>(p) + offset), size, defaultValue, size);
            }
        }
        cb->unmap();
    }

    //----------------------------------------------------------
    // リソーススロットの番号(定数バッファb/テクスチャt/サンプラーs)
    // 使用しているスロット番号を取得できます。
    //----------------------------------------------------------
    for(u32 i = 0; i < desc.BoundResources; ++i) {
        D3D11_SHADER_INPUT_BIND_DESC inputDesc;
        d3dShaderReflection->GetResourceBindingDesc(i, &inputDesc);

        [[maybe_unused]] auto name  = inputDesc.Name;
        [[maybe_unused]] auto slot  = inputDesc.BindPoint;
        [[maybe_unused]] auto count = inputDesc.BindCount;

        D3D_SHADER_INPUT_TYPE type = inputDesc.Type;
        switch(type) {
                //----------------------------------------------------------
                // 定数バッファ
                //----------------------------------------------------------
            case D3D_SIT_CBUFFER:
            {
                // 定数バッファのスロット番号を決定
                std::shared_ptr<dx11::Buffer> cb = dx11::getShaderConstantBuffer(inputDesc.Name);

                auto result = std::find_if(std::begin(constantBuffers_), std::end(constantBuffers_), [&](auto& x) { return x.first == cb; });
                ASSERT_MESSAGE(result != std::end(constantBuffers_), "定数バッファの自動設定に失敗しました.");

                // スロット番号を登録
                result->second = slot;
            } break;
            default:
                break;
        }
    }
    return true;
}

//---------------------------------------------------------------------------
//! シェーダーをコンパイル
//! @param  [in]    path    ファイルパス
//! @param  [in]    entrypoint    関数名
//! @param  [in]    target    シェーダーモデル名 "vs_5_0" "ps_5_0" etc...
//---------------------------------------------------------------------------
std::shared_ptr<dx11::Shader> createShader(const char* path, const char* entrypoint, const char* target)
{
    assert(path);
    assert(entrypoint);
    assert(target);

    //---------------------------------------------------------------------
    // ファイルからソースファイルを読み込み
    //---------------------------------------------------------------------
    std::vector<std::byte> source;
    {
        // ファイルから読み込み
        std::ifstream file(std::string(path), std::ios::in | std::ios::binary | std::ios::ate);   // ateを指定すると最初からファイルポインタが末尾に移動
        if(!file.is_open()) {
            return nullptr;
        }
        auto size = file.tellg();
        source.resize(static_cast<size_t>(size));

        file.seekg(0, std::ios::beg);
        file.read(reinterpret_cast<char*>(source.data()), size);
        file.close();
    }

    //----------------------------------------------------------
    // シェーダーをコンパイル
    //----------------------------------------------------------
    u32 compileFlags = D3DCOMPILE_OPTIMIZATION_LEVEL3;

#if defined(_DEBUG)
    // GraphicDebuggingツール用にシェーダーデバッグ向け設定
    compileFlags |= D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#endif
    const D3D_SHADER_MACRO* defines = nullptr;

    com_ptr<ID3DBlob> byteCode = nullptr;
    com_ptr<ID3DBlob> errors;

    //D3DCompileFromFile(string::convertUtf8ToUnicode(path).c_str(),   // [in]  ファイルパス
    //                   defines,                                      // [in]  プリプロセッサマクロ定義
    //                   D3D_COMPILE_STANDARD_FILE_INCLUDE,            // [in]  カスタムインクルード処理(D3D_COMPILE_STANDARD_FILE_INCLUDE指定でデフォルト動作)
    //                   entrypoint,                                   // [in]  関数名
    //                   target,                                       // [in]  シェーダーモデル名
    //                   compileFlags,                                 // [in]  コンパイラフラグ  (D3DCOMPILE_xxxx)
    //                   0,                                            // [in]  コンパイラフラグ2 (D3DCOMPILE_FLAGS2_xxxx)
    //                   &byteCode,                                    // [out] コンパイルされたバイトコード
    //                   &errors);                                     // [out] エラーメッセージ

    auto hr = D3DCompile(
        source.data(),                       // [in]  ソースコードのメモリ上のアドレス
        source.size(),                       // [in]  ソースコードサイズ
        path,                                // [in]  ソースのファイルパス
        defines,                             // [in]  プリプロセッサマクロ定義
        D3D_COMPILE_STANDARD_FILE_INCLUDE,   // [in]  カスタムインクルード処理(D3D_COMPILE_STANDARD_FILE_INCLUDE指定でデフォルト動作)
        entrypoint,                          // [in]  関数名
        target,                              // [in]  シェーダーモデル名
        compileFlags,                        // [in]  コンパイラフラグ  (D3DCOMPILE_xxxx)
        0,                                   // [in]  コンパイラフラグ2 (D3DCOMPILE_FLAGS2_xxxx)
        &byteCode,                           // [out] コンパイルされたバイトコード
        &errors);                            // [out] エラーメッセージ

    // エラー出力
    if(errors != nullptr) {
        MessageBox(application::hwnd(), static_cast<char*>(errors->GetBufferPointer()), path, MB_OK);
        OutputDebugStringA((char*)errors->GetBufferPointer());
        return nullptr;
    }

    if(FAILED(hr)) {
        return nullptr;
    }

#if 0   // シェーダーリフレクションから取得するため無効化
    //----------------------------------------------------------
    // シェーダーの種類を検出
    // シェーダーリフレクションから取得するほうが好ましい
    //----------------------------------------------------------
    ShaderImpl::Type type = ShaderImpl::Type::Unknown;
    switch(target[0]) {   // シェーダーモデル名の先頭文字で識別 "vs" "ps" "gs" など
        case 'v': type = ShaderImpl::Type::Vs; break;
        case 'p': type = ShaderImpl::Type::Ps; break;
        case 'g': type = ShaderImpl::Type::Gs; break;
        case 'h': type = ShaderImpl::Type::Hs; break;
        case 'd': type = ShaderImpl::Type::Ds; break;
        case 'c': type = ShaderImpl::Type::Cs; break;
        default:
            return nullptr;   // エラー: 不正な種類
    }
#endif

    //----------------------------------------------------------
    // クラスを初期化
    //----------------------------------------------------------
    auto p = std::make_shared<dx11::ShaderImpl>();
    if(p) {
        if(!p->initialize(byteCode->GetBufferPointer(), byteCode->GetBufferSize())) {
            p.reset();
        }
    }
    return p;
}

}   // namespace dx11
