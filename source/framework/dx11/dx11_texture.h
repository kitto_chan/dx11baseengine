﻿//---------------------------------------------------------------------------
//!	@file	dx11_texture.h
//!	@brief	GPUテクスチャ
//---------------------------------------------------------------------------
#pragma once

namespace dx11 {

//----------------------------------------------------------
//! テクスチャ情報
//----------------------------------------------------------
struct TextureDesc
{
    u32         width_        = 0;                     //!< 幅
    u32         height_       = 0;                     //!< 高さ
    u32         depth_        = 0;                     //!< 高さ(3Dテクスチャの場合)
    DXGI_FORMAT dxgiFormat_   = DXGI_FORMAT_UNKNOWN;   //!< ピクセル形式
    u32         mipLevels_    = 1;                     //!< ミップレベル
    u32         arrayCount_   = 1;                     //!< 配列数
    u32         d3dBindFlags_ = 0;                     //!< テクスチャの設定可能スロット
    D3D11_USAGE d3dUsage_     = D3D11_USAGE_DEFAULT;   //!< テクスチャの配置場所(メインメモリ or ビデオメモリ)
    bool        isCubemap_    = false;                 //!< キューブマップかどうか
};

//===========================================================================
//! テクスチャ
//===========================================================================
class Texture
{
public:
    //----------------------------------------------------------
    //! @name キャスト
    //----------------------------------------------------------
    //@{

    //! SRV参照
    virtual operator ID3D11ShaderResourceView*() const = 0;

    //! RTV参照
    virtual operator ID3D11RenderTargetView*() const = 0;

    //! DSV参照
    virtual operator ID3D11DepthStencilView*() const = 0;

    //! UAV参照
    virtual operator ID3D11UnorderedAccessView*() const = 0;

    //@}
    //----------------------------------------------------------
    //! @name 参照
    //----------------------------------------------------------
    //@{

    //! テクスチャ情報を取得
    virtual const dx11::TextureDesc& desc() const = 0;

    //! D3Dリソースを取得
    virtual ID3D11Resource* d3dResource() const = 0;

    //@}

	std::string newPath;
protected:
    virtual ~Texture() = default;
};

//! テクスチャを作成
//! @param  [in]    desc    オプション設定
[[nodiscard]] std::shared_ptr<dx11::Texture> createTexture(const dx11::TextureDesc& desc);

//! ターゲットテクスチャを作成
//! RenderTaget / DepthStencil を作成します
//! @param  [in]    width       幅
//! @param  [in]    height      高さ
//! @param  [in]    dxgiFormat  ピクセルフォーマット
[[nodiscard]] std::shared_ptr<dx11::Texture> createTargetTexture(u32 width, u32 height, DXGI_FORMAT dxgiFormat);

//! テクスチャをファイルから作成
//! @param  [in]    path    ファイルパス
[[nodiscard]] std::shared_ptr<dx11::Texture> createTextureFromFile(std::string_view path);

//! D3Dリソースから作成
//! @param  [in]    d3dResource     対象のD3Dリソース
//! @param  [in]    overrideFormat  フォーマット型の上書き(普段は指定なし)
//! @note 引数のリソースは内部で参照カウンタで所有します。
[[nodiscard]] std::shared_ptr<dx11::Texture> createTexture(ID3D11Resource* d3dResource, DXGI_FORMAT overrideFormat = DXGI_FORMAT_UNKNOWN);

}   // namespace dx11
