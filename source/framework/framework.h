﻿//---------------------------------------------------------------------------
//!	@file	framework.h
//!	@brief	フレームワーク共通ヘッダー
//---------------------------------------------------------------------------
#pragma once

//--------------------------------------------------------------
//!	@defgroup	Windowsヘッダー
//--------------------------------------------------------------
//@{

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN   // 使用頻度の低いWindowsAPIを省略してヘッダー軽量化
#endif

#define NOMINMAX   // std::min std::maxとWindowsSDKのmin/maxマクロが衝突するためWindowsSDK側を無効化
#define STRICT     // Windowsオブジェクトの型を厳密に扱う

#include <windows.h>
#include <wrl.h>   // ComPtr用

//@}
//--------------------------------------------------------------
//!	@defgroup	STL(Standard Template Library)
//--------------------------------------------------------------
//@{

#include <array>
#include <vector>
#include <string>
#include <string_view>
#include <memory>
#include <functional>
#include <algorithm>

#include <sstream>
//@}
//--------------------------------------------------------------
//!	@defgroup	DirectX関連
//--------------------------------------------------------------
//@{

#include <d3d11_4.h>
#include <dxgi1_4.h>

//---- DirectX Math
#include <DirectXMath.h>

//---- DirectXツールキット
#include <DirectXTK/Inc/GamePad.h>
#include <DirectXTK/Inc/Keyboard.h>
#include <DirectXTK/Inc/Mouse.h>
#include <DirectXTK/Inc/CommonStates.h>
#include <DirectXTK/Inc/Audio.h>

//---- ベクトル演算ライブラリ hlslpp
#pragma warning(push)
#pragma warning(disable : 26495)
#include <hlslpp/include/hlsl++.h>
using namespace hlslpp;
#pragma warning(pop)

//---- ImGui
#include <imgui/imgui.h>
#include <ImGuizmo/ImGuizmo.h>

//---- Json
#include <nlohmann/json.hpp>
using json = nlohmann::json;

//---- cereal
#include <cereal/cereal.hpp>          // シリアライザ
#include <cereal/archives/json.hpp>   // シリアライザ(Jsonフォンマット)
//@}
//--------------------------------------------------------------
//!	@defgroup	アプリケーション
//--------------------------------------------------------------
//@{

#include "debug/debug.h"   // デバッグ関連


#include "utility/typedef.h"               // 型定義
#include "utility/enumDef.h"			   // enum定義
#include "utility/raw_ptr.h"               // 生ポインタ
#include "utility/string_encode.h"         // 文字列UTF8 ←→ UNICODE 相互変換
#include "utility/color.h"                 // カラー
#include "utility/performance_counter.h"   // パフォーマンスカウンター
#include "utility/thread_pool.h"           // スレッドプール

#include "utility/time/Timer.h"            // タイム

#include "math/MathCommon.h"               // 共通の数学クラス
#include "math/MathMatrix.h"               // 行列ユーティリティー
#include "math/MathDirectX.h"              // DirectX <->　hlslpp のキャスト
#include "math/MathRandom.h"               // ランダムヘルパー
#include "math/MathImGui.h"                // Imgui <-> hlslpp
#include "utility/StructDef.h"             // 構造体定義

#include "SystemSetting.h"            // システム設定
#include "pattern/Singleton.h"        // シングルトン
#include "utility/SystemSettings.h"	  // ゲーム設定	   
#include "utilityGPU/ImGuiWidget.h"   // Imguiの自定義Widget
#include "utilityGPU/Geometry.h"      // ジオメトリ

#include "utilityGPU/LightHelper.h"   // ライティング設定
#include "utilityGPU/RenderState.h"   // 描画
#include "utilityGPU/Vertex.h"        // 頂点データ

#include "dx11/dx11.h"                // ユーティリティー
#include "dx11/dx11_shader.h"         // シェーダー
#include "dx11/dx11_texture.h"        // テクスチャ
#include "dx11/dx11_buffer.h"         // バッファ
#include "dx11/dx11_swap_chain.h"     // スワップチェイン
#include "dx11/dx11_input_layout.h"   // 入力レイアウト
#include "dx11/dx11_device.h"         // デバイス
#include "dx11/dx11_helper.h"         // ヘルパーユーティリティー

#include "graphics/graphics_render.h"   // レンダリング管理
#include "debug/debug_draw.h"           // デバッグ描画
#include "renderer/FontRenderer.h"      // フォント描画

#include "physics/physics_engine.h"       // 物理:シミュレーションエンジン
#include "Math/MathBulletPhysics.h"       // bulletPhysisc <-> hlslpp のキャスト

#include "renderer/animation.h"         // アニメーション
#include "renderer/animation_layer.h"   // アニメーションレイヤー
#include "renderer/model.h"             // 3Dモデル

#include "manager/input/KeyboardManager.h" // キーボードを管理
#include "manager/input/MouseManager.h"    // マウスを管理
#include "manager/input/GamePadManager.h"
#include "manager/input/InputHelper.h"

#include "manager/AudioManager.h"

#include "D3DApp.h"
//@}
