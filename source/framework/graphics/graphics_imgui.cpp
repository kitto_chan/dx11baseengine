﻿//---------------------------------------------------------------------------
//!	@file	graphics_imgui.h
//!	@brief	Dear ImGui 組み込みユーティリティー
//---------------------------------------------------------------------------
#include "graphics_imgui.h"
#include <imgui/imgui_impl_win32.h>
#include <imgui/imgui_impl_dx11.h>
#include <imgui/imgui_internal.h>
#include <IconFont/IconsFontAwesome5.h>
#include <IconFont/IconsKenney.h>

extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

namespace graphics::imgui {

namespace {
bool useDockSpace = false;
}   // namespace

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool initialize()
{
    //----------------------------------------------------------
    // Dear ImGuiコンテキストを作成
    //----------------------------------------------------------
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;   // キーボード有効
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;    // ゲームパッド有効

    // Dear ImGui スタイルを設定
    ImGui::StyleColorsDark();   // ダークスタイル

    //----------------------------------------------------------
    // 日本語フォント対応
    //----------------------------------------------------------
    ImFontConfig config;
    config.MergeMode = true;

    io.Fonts->AddFontFromFileTTF("c:/Windows/Fonts/consola.ttf", 14.0f, nullptr, io.Fonts->GetGlyphRangesDefault());
    io.Fonts->AddFontFromFileTTF("font/Kosugi_Maru/KosugiMaru-Regular.ttf", 14.f, &config, io.Fonts->GetGlyphRangesJapanese());
    io.Fonts->AddFontFromFileTTF("c:/Windows/Fonts/meiryo.ttc", 14.0f, &config, io.Fonts->GetGlyphRangesJapanese());

    static const ImWchar icons_ranges[] = { ICON_MIN_FA, ICON_MAX_FA, 0 };
    ImFontConfig         icons_config;
    icons_config.MergeMode  = true;
    icons_config.PixelSnapH = true;
    icons_config.GlyphOffset.y += 5.0f;
    io.Fonts->AddFontFromFileTTF("font/fontIcon/fa-solid-900.ttf", 20.0f, &icons_config, icons_ranges);
    //io.Fonts->AddFontFromFileTTF("font/fontaudio/fontaudio.ttf", 15.0f, &icons_config, icons_ranges);
    icons_config.GlyphOffset.y = 2.0f;
    io.Fonts->AddFontFromFileTTF("font/kenny/kenney-icon-font.ttf", 14.0f, &icons_config, icons_ranges);
    //----------------------------------------------------------
    // ドッキングウィンドウ対応
    //----------------------------------------------------------
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;     // ドッキングウィンドウ対応
    io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;   // ビューポート有効

    io.ConfigDockingWithShift = false;   // Shiftキーでドッキング操作するかどうか

    // スタイルの詳しい設定
    ImGuiStyle& style                            = ImGui::GetStyle();
    style.Colors[ImGuiCol_Text]                  = ImVec4(1.000f, 1.000f, 1.000f, 1.000f);
    style.Colors[ImGuiCol_TextDisabled]          = ImVec4(0.500f, 0.500f, 0.500f, 1.000f);
    style.Colors[ImGuiCol_WindowBg]              = ImVec4(0.13f, 0.14f, 0.15f, 1.00f);
    style.Colors[ImGuiCol_ChildBg]               = ImVec4(0.13f, 0.14f, 0.15f, 1.00f);
    style.Colors[ImGuiCol_PopupBg]               = ImVec4(0.313f, 0.313f, 0.313f, 1.000f);
    style.Colors[ImGuiCol_Border]                = ImVec4(0.266f, 0.266f, 0.266f, 1.000f);
    style.Colors[ImGuiCol_BorderShadow]          = ImVec4(0.000f, 0.000f, 0.000f, 0.000f);
    style.Colors[ImGuiCol_FrameBg]               = ImVec4(0.160f, 0.160f, 0.160f, 1.000f);
    style.Colors[ImGuiCol_FrameBgHovered]        = ImVec4(0.200f, 0.200f, 0.200f, 1.000f);
    style.Colors[ImGuiCol_FrameBgActive]         = ImVec4(0.280f, 0.280f, 0.280f, 1.000f);
    style.Colors[ImGuiCol_TitleBg]               = ImVec4(0.148f, 0.148f, 0.148f, 1.000f);
    style.Colors[ImGuiCol_TitleBgActive]         = ImVec4(0.148f, 0.148f, 0.148f, 1.000f);
    style.Colors[ImGuiCol_TitleBgCollapsed]      = ImVec4(0.148f, 0.148f, 0.148f, 1.000f);
    style.Colors[ImGuiCol_MenuBarBg]             = ImVec4(0.195f, 0.195f, 0.195f, 1.000f);
    style.Colors[ImGuiCol_ScrollbarBg]           = ImVec4(0.160f, 0.160f, 0.160f, 1.000f);
    style.Colors[ImGuiCol_ScrollbarGrab]         = ImVec4(0.277f, 0.277f, 0.277f, 1.000f);
    style.Colors[ImGuiCol_ScrollbarGrabHovered]  = ImVec4(0.300f, 0.300f, 0.300f, 1.000f);
    style.Colors[ImGuiCol_ScrollbarGrabActive]   = ImVec4(1.000f, 0.391f, 0.000f, 1.000f);
    style.Colors[ImGuiCol_CheckMark]             = ImVec4(1.000f, 1.000f, 1.000f, 1.000f);
    style.Colors[ImGuiCol_SliderGrab]            = ImVec4(0.391f, 0.391f, 0.391f, 1.000f);
    style.Colors[ImGuiCol_SliderGrabActive]      = ImVec4(1.000f, 0.391f, 0.000f, 1.000f);
    style.Colors[ImGuiCol_Button]                = ImVec4(1.000f, 1.000f, 1.000f, 0.000f);
    style.Colors[ImGuiCol_ButtonHovered]         = ImVec4(1.000f, 1.000f, 1.000f, 0.156f);
    style.Colors[ImGuiCol_ButtonActive]          = ImVec4(1.000f, 1.000f, 1.000f, 0.391f);
    style.Colors[ImGuiCol_Header]                = ImVec4(0.313f, 0.313f, 0.313f, 1.000f);
    style.Colors[ImGuiCol_HeaderHovered]         = ImVec4(0.469f, 0.469f, 0.469f, 1.000f);
    style.Colors[ImGuiCol_HeaderActive]          = ImVec4(0.469f, 0.469f, 0.469f, 1.000f);
    style.Colors[ImGuiCol_Separator]             = style.Colors[ImGuiCol_Border];
    style.Colors[ImGuiCol_SeparatorHovered]      = ImVec4(0.391f, 0.391f, 0.391f, 1.000f);
    style.Colors[ImGuiCol_SeparatorActive]       = ImVec4(1.000f, 0.391f, 0.000f, 1.000f);
    style.Colors[ImGuiCol_ResizeGrip]            = ImVec4(1.000f, 1.000f, 1.000f, 0.250f);
    style.Colors[ImGuiCol_ResizeGripHovered]     = ImVec4(1.000f, 1.000f, 1.000f, 0.670f);
    style.Colors[ImGuiCol_ResizeGripActive]      = ImVec4(1.000f, 0.391f, 0.000f, 1.000f);
    style.Colors[ImGuiCol_Tab]                   = ImVec4(0.098f, 0.098f, 0.098f, 1.000f);
    style.Colors[ImGuiCol_TabHovered]            = ImVec4(0.352f, 0.352f, 0.352f, 1.000f);
    style.Colors[ImGuiCol_TabActive]             = ImVec4(0.195f, 0.195f, 0.195f, 1.000f);
    style.Colors[ImGuiCol_TabUnfocused]          = ImVec4(0.098f, 0.098f, 0.098f, 1.000f);
    style.Colors[ImGuiCol_TabUnfocusedActive]    = ImVec4(0.195f, 0.195f, 0.195f, 1.000f);
    style.Colors[ImGuiCol_DockingPreview]        = ImVec4(1.000f, 0.391f, 0.000f, 0.781f);
    style.Colors[ImGuiCol_DockingEmptyBg]        = ImVec4(0.180f, 0.180f, 0.180f, 1.000f);
    style.Colors[ImGuiCol_PlotLines]             = ImVec4(0.469f, 0.469f, 0.469f, 1.000f);
    style.Colors[ImGuiCol_PlotLinesHovered]      = ImVec4(1.000f, 0.391f, 0.000f, 1.000f);
    style.Colors[ImGuiCol_PlotHistogram]         = ImVec4(0.586f, 0.586f, 0.586f, 1.000f);
    style.Colors[ImGuiCol_PlotHistogramHovered]  = ImVec4(1.000f, 0.391f, 0.000f, 1.000f);
    style.Colors[ImGuiCol_TextSelectedBg]        = ImVec4(1.000f, 1.000f, 1.000f, 0.156f);
    style.Colors[ImGuiCol_DragDropTarget]        = ImVec4(1.000f, 0.391f, 0.000f, 1.000f);
    style.Colors[ImGuiCol_NavHighlight]          = ImVec4(1.000f, 0.391f, 0.000f, 1.000f);
    style.Colors[ImGuiCol_NavWindowingHighlight] = ImVec4(1.000f, 0.391f, 0.000f, 1.000f);
    style.Colors[ImGuiCol_NavWindowingDimBg]     = ImVec4(0.000f, 0.000f, 0.000f, 0.586f);
    style.Colors[ImGuiCol_ModalWindowDimBg]      = ImVec4(0.000f, 0.000f, 0.000f, 0.586f);

    style.ChildRounding     = 4.0f;
    style.FrameBorderSize   = 1.0f;
    style.PopupRounding     = 2.0f;
    style.TabBorderSize     = 1.0f;
    style.TabRounding       = 0.0f;
    style.WindowRounding    = 5.0f;
    style.FramePadding      = ImVec2(5, 5);
    style.FrameRounding     = 4.0f;
    style.ItemSpacing       = ImVec2(12, 8);
    style.ItemInnerSpacing  = ImVec2(8, 6);
    style.IndentSpacing     = 25.0f;
    style.ScrollbarSize     = 15.0f;
    style.ScrollbarRounding = 9.0f;
    style.GrabMinSize       = 5.0f;
    style.GrabRounding      = 3.0f;

    if(io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
        style.WindowRounding              = 0.0f;
        style.Colors[ImGuiCol_WindowBg].w = 1.0f;
    }

    //----------------------------------------------------------
    // プラットフォームとレンダラのバックエンドを初期化
    //----------------------------------------------------------
    if(!ImGui_ImplWin32_Init(application::hwnd())) {
        return false;
    }

    ASSERT_MESSAGE(dx11::d3dDevice(), "D3Dがまだ初期化されていません.");
    if(!ImGui_ImplDX11_Init(dx11::d3dDevice(), dx11::immediateContext())) {
        return false;
    }

    return true;
}

//---------------------------------------------------------------------------
//! フレーム更新開始
//---------------------------------------------------------------------------
void beginFrame()
{
    ImGui_ImplDX11_NewFrame();
    ImGui_ImplWin32_NewFrame();
    ImGui::NewFrame();
    ImGuizmo::BeginFrame();

    useDockSpace       = SystemSettingsMgr()->IsEditorMode();   // ドックスペースを利用するかどうか,
    static bool inited = false;
    if(useDockSpace) {
        static ImGuiDockNodeFlags dockspaceFlags = ImGuiDockNodeFlags_None;

        // ImGuiWindowFlags_NoDocking フラグを使用して、親ウィンドウをドッキングできないようにしています。
        // 2つのドッキングターゲットがお互いの中にあると混乱してしまうからです。
        ImGuiWindowFlags windowFlags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;

        //----------------------------------------------------------
        // ドックウィンドウの位置を計算
        //----------------------------------------------------------
        RECT clientRect;
        GetClientRect(application::hwnd(), &clientRect);

        // ウィンドウの左上のスクリーン座標(デスクトップ座標)
        POINT position{ clientRect.left, clientRect.top };
        ClientToScreen(application::hwnd(), &position);

        const bool hasViewports = ImGui::GetIO().ConfigFlags & ImGuiConfigFlags_ViewportsEnable;
        f32        x            = hasViewports ? static_cast<f32>(position.x) : 0.0f;
        f32        y            = hasViewports ? static_cast<f32>(position.y) : 0.0f;
        f32        w            = static_cast<f32>(clientRect.right - clientRect.left);
        f32        h            = static_cast<f32>(clientRect.bottom - clientRect.top);

        ImGui::SetNextWindowSize(ImVec2(w, h));
        ImGui::SetNextWindowPos(ImVec2(x, y));

        //----------------------------------------------------------
        // 土台のドックウィンドウを作成
        //----------------------------------------------------------
        windowFlags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse |
                       ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove |
                       ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;

        ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
        ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
        ImGui::Begin("MainDockspace", nullptr, windowFlags);
        ImGui::PopStyleVar(2);

        ImGuiID dockspaceID = ImGui::GetID("MyDockspace");
        if(!inited) {
            ImGui::DockBuilderRemoveNode(dockspaceID);
            ImGui::DockBuilderAddNode(dockspaceID, dockspaceFlags);

            ImGuiID dock_main_id  = dockspaceID;
            ImGuiID dock_up_id    = ImGui::DockBuilderSplitNode(dock_main_id, ImGuiDir_Up, 0.03f, nullptr, &dock_main_id);
            ImGuiID dock_right_id = ImGui::DockBuilderSplitNode(dock_main_id, ImGuiDir_Right, 0.2f, nullptr, &dock_main_id);
            //ImGuiID dock_left_id       = ImGui::DockBuilderSplitNode(dock_main_id, ImGuiDir_Left, 0.2f, nullptr, &dock_main_id);
            ImGuiID dock_down_id       = ImGui::DockBuilderSplitNode(dock_main_id, ImGuiDir_Down, 0.3f, nullptr, &dock_main_id);
            ImGuiID dock_down_right_id = ImGui::DockBuilderSplitNode(dock_down_id, ImGuiDir_Right, 0.6f, nullptr, &dock_down_id);
            ImGuiID dock_right_down_id = ImGui::DockBuilderSplitNode(dock_right_id, ImGuiDir_Down, 0.5f, nullptr, &dock_right_id);

            // TODO: マップ id + name
            ImGui::DockBuilderDockWindow("Actions", dock_up_id);
            ImGui::DockBuilderDockWindow("Hierarchy", dock_right_id);
            ImGui::DockBuilderDockWindow("Inspector", dock_right_down_id);
            ImGui::DockBuilderDockWindow("Shadowmap", dock_right_down_id);
            ImGui::DockBuilderDockWindow("Console", dock_down_id);
            ImGui::DockBuilderDockWindow("AnimationConfig", dock_down_id);
            ImGui::DockBuilderDockWindow("Project", dock_down_right_id);
            ImGui::DockBuilderDockWindow("Scene", dock_main_id);

            // Disable tab bar for custom toolbar
            ImGuiDockNode* node = ImGui::DockBuilderGetNode(dock_up_id);
            node->LocalFlags |= ImGuiDockNodeFlags_NoTabBar | ImGuiDockNodeFlags_NoResizeFlagsMask_;

            ImGui::DockBuilderFinish(dock_main_id);
            inited = true;
        }
        ImGui::DockSpace(dockspaceID, ImVec2(0.0f, 0.0f), dockspaceFlags);
    }
    else {
        inited = false;
    }
}

//---------------------------------------------------------------------------
//! フレーム更新終了
//---------------------------------------------------------------------------
void endFrame()
{
    if(useDockSpace) {
        ImGui::End();
    }
    // デバッグ用のデモウィンドウを表示
    if(SystemSettingsMgr()->IsUseImguiDemo()) {
        // デモウィンドウ
        ImGui::ShowDemoWindow();
    }

    // 描画情報を生成
    ImGui::Render();
}

//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void render()
{
    //----------------------------------------------------------
    // 描画発行
    //----------------------------------------------------------
    ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());

    //----------------------------------------------------------
    // マルチウィンドウ描画
    //----------------------------------------------------------
    if(ImGui::GetIO().ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
        ImGui::UpdatePlatformWindows();
        ImGui::RenderPlatformWindowsDefault(nullptr, nullptr);
    }
}

//---------------------------------------------------------------------------
//! ウィンドウプロシージャ
//---------------------------------------------------------------------------
LRESULT windowProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{
    return ImGui_ImplWin32_WndProcHandler(hwnd, message, wparam, lparam);
}

//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void finalize()
{
    ImGui_ImplDX11_Shutdown();
    ImGui_ImplWin32_Shutdown();
    ImGui::DestroyContext();
}

}   // namespace graphics::imgui
