﻿//---------------------------------------------------------------------------
//!	@file	graphics_imgui.h
//!	@brief	Dear ImGui 組み込みユーティリティー
//---------------------------------------------------------------------------
#pragma once

namespace graphics::imgui {

//! 初期化
bool initialize();

//! フレーム更新開始
void beginFrame();

//! フレーム更新終了
void endFrame();

//! 描画
void render();

//! 解放
void finalize();

//! ウィンドウプロシージャ
LRESULT windowProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lPpram);

}   // namespace graphics::imgui
