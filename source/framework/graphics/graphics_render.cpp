﻿//---------------------------------------------------------------------------
//!	@file	gpu_render.cpp
//!	@brief	レンダリング管理
//---------------------------------------------------------------------------
#include "graphics_render.h"
#include "graphics_imgui.h"
#include "impl/debug_draw_impl.h"
#include <DirectXTK/Inc/CommonStates.h>

namespace graphics {

namespace {

dx11::Device                           device11_;       //!< D3D11デバイス
std::unique_ptr<dx11::SwapChain>       swapChain_;      //!< スワップチェイン
std::unique_ptr<DirectX::CommonStates> commonStates_;   //!< 共通ステート管理

}   // namespace

//===========================================================================
//! レンダリング管理実装部
//! このクラスはシングルトンです。
//===========================================================================
class RenderImpl final : public graphics::Render
{
public:
    RenderImpl() = default;

    //! 初期化
    virtual bool initialize(u32 width, u32 height) override;

    //! 更新
    virtual void update() override;

    //! 描画
    virtual void render() override;

    //! 解放
    virtual void finalize() override;

    //! 実体を取得
    static RenderImpl* instance();

private:
    // コピー禁止/move禁止
    RenderImpl(const RenderImpl&) = delete;
    RenderImpl(Render&&)          = delete;
    RenderImpl& operator=(const Render&) = delete;
    RenderImpl& operator=(Render&&) = delete;

private:
    //----------------------------------------------------------
    //! @name アプリケーション向けリソース
    //----------------------------------------------------------
    //@{

    //@}

    u32                            width_  = 0;     //!< 解像度 (幅)
    u32                            height_ = 0;     //!< 解像度 (高さ)
    std::shared_ptr<dx11::Texture> colorTexture_;   //!< 描画ターゲット
    std::shared_ptr<dx11::Texture> depthTexture_;   //!< デプスステンシル

    // イメージコピー用のシェーダー
    std::shared_ptr<dx11::Shader> imageCopyShaderVs_;
    std::shared_ptr<dx11::Shader> imageCopyShaderPs_;

    u64 previousCounter_ = 0;   //!< 1フレーム前のパフォーマンスカウンター値
};

//===========================================================================
// 内部実装
//===========================================================================

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool RenderImpl::initialize(u32 width, u32 height)
{
    width_  = width;
    height_ = height;

    //-------------------------------------------------------------
    // D3Dデバイス作成
    //-------------------------------------------------------------
    if(!device11_.initialize(width_, height_)) {
        return false;
    }

    // レンダリング用バッファ作成
    colorTexture_ = dx11::createTargetTexture(width_, height_, DXGI_FORMAT_R8G8B8A8_UNORM);
    depthTexture_ = dx11::createTargetTexture(width_, height_, DXGI_FORMAT_D32_FLOAT);
    if(!colorTexture_ || !depthTexture_) {
        return false;
    }

    // 共通ステート
    commonStates_ = std::make_unique<DirectX::CommonStates>(dx11::d3dDevice());

    // シェーダー初期化
    imageCopyShaderVs_ = dx11::createShader("framework/vs_quad2d.fx", "main", "vs_5_0");
    imageCopyShaderPs_ = dx11::createShader("framework/ps_texture.fx", "main", "ps_5_0");

    //----------------------------------------------------------
    // スワップチェインの作成
    //----------------------------------------------------------
    static constexpr u32 FRAME_COUNT = 3;   // バックバッファ数

    swapChain_ = dx11::createSwapChain(width_, height_, DXGI_FORMAT_B8G8R8A8_UNORM, FRAME_COUNT);
    if(!swapChain_) {
        return false;
    }

    //----------------------------------------------------------
    // ImGui初期化
    //----------------------------------------------------------
    if(!graphics::imgui::initialize()) {
        return false;
    }

    // デバッグ描画
    debug::initializeDebugDraw();

    //----------------------------------------------------------
    // 物理シミュレーション初期化
    //----------------------------------------------------------
    if(!physics::PhysicsEngine::instance()->initialize()) {
        return false;
    }

    //----------------------------------------------------------
    // 初期化
    //----------------------------------------------------------
    if(dx11::callback::onInitialize) {
        dx11::callback::onInitialize(width_, height_);
    }

    return true;
}

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void RenderImpl::update()
{
    graphics::imgui::beginFrame();   // フレーム更新開始

    //----------------------------------------------------------
    // 直前のフレームからの経過時間を計測
    //----------------------------------------------------------
    //u64 currentCounter = performance_counter::now();

    //// 経過時間
    //f64 duration = static_cast<f64>(currentCounter - previousCounter_) / static_cast<f64>(performance_counter::frequency());

    //f32 deltaTime = deltaTime = std::min(1.0f, static_cast<f32>(duration));

    //// 次のフレームのためにカウンター値を保存
    //previousCounter_ = currentCounter;

    f32 deltaTime = timer::DeltaTime();

    //----------------------------------------------------------
    // 更新
    //----------------------------------------------------------
    if(dx11::callback::onUpdate) {
        dx11::callback::onUpdate(deltaTime);
    }

    //----------------------------------------------------------
    // 物理シミュレーション更新
    //----------------------------------------------------------
    //    f32 deltaTime = 1.0f / 60.0f;   // 時間経過

    if(!SystemSettingsMgr()->IsLoading()) {
        if(SystemSettingsMgr()->IsPlayState() && SystemSettingsMgr()->GetIsGameUpdate()) {
            physics::PhysicsEngine::instance()->update(deltaTime);
        }
    }
}

//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void RenderImpl::render()
{
    auto* c = dx11::immediateContext();

    // ヘルパーユーティリティー内のキャッシュ情報をクリア
    dx11::invalidateHelperCache();

    // 描画
    if(dx11::callback::onRender) {
        dx11::callback::onRender(colorTexture_, depthTexture_);
    }

	/*renderer::FontRendererIns()->Render();*/
    manager::AudioMgr()->RenderImgui();

    // デバッグ描画
    debug::flushDebugDraw();

    //----------------------------------------------------------
    // バックバッファに最終コピー
    //----------------------------------------------------------
    auto* backBuffer = dx11::swapChain()->backBuffer();

    {
        ID3D11RenderTargetView* rtv = *backBuffer;
        c->OMSetRenderTargets(1, &rtv, nullptr);

        D3D11_VIEWPORT viewport;

        viewport.TopLeftX = 0;
        viewport.TopLeftY = 0;
        viewport.Width    = static_cast<f32>(backBuffer->desc().width_);
        viewport.Height   = static_cast<f32>(backBuffer->desc().height_);
        viewport.MinDepth = 0.0f;
        viewport.MaxDepth = 1.0f;
        c->RSSetViewports(1, &viewport);
    }
    {
        // 空の頂点バッファ / インデックスバッファ
        ID3D11Buffer* vb{};
        u32           stride = 0;
        u32           offset = 0;
        c->IASetVertexBuffers(0, 1, &vb, &stride, &offset);
        c->IASetIndexBuffer(nullptr, DXGI_FORMAT_UNKNOWN, 0);
        c->IASetInputLayout(nullptr);
    }
    // イメージコピー用シェーダー
    c->VSSetShader(*imageCopyShaderVs_, nullptr, 0);
    c->PSSetShader(*imageCopyShaderPs_, nullptr, 0);
    // コピー元テクスチャ
    ID3D11ShaderResourceView* rtvs[]{ *colorTexture_ };
    ID3D11SamplerState*       samplers[]{ commonStates_->LinearClamp() };   // バイリニアフィルタリング
    c->PSSetShaderResources(0, 1, rtvs);
    c->PSSetSamplers(0, 1, samplers);

    // 描画
    c->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
    c->Draw(3, 0);

    //----------------------------------------------------------
    // ImGuiを描画
    //----------------------------------------------------------
    graphics::imgui::endFrame();   // フレーム更新終了

    {
        ID3D11RenderTargetView* rtv = *backBuffer;
        c->OMSetRenderTargets(1, &rtv, nullptr);
    }

    graphics::imgui::render();

    if(SystemSettingsMgr()->IsPhyDebug()) {
        physics::PhysicsEngine::instance()->renderDebug();
    }
    //----------------------------------------------------------
    // フレーム更新
    //----------------------------------------------------------
    swapChain_->present(1);
}

//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void RenderImpl::finalize()
{
    // 確実にGPUがデストラクタによってクリーンアップされようとしているリソースを
    // 参照しないようにするために描画完了を待つ
    swapChain_->waitForGpu();

    // デバイスコンテキストにセットされているオブジェクトをすべて解除する。
    dx11::immediateContext()->ClearState();

    // デバッグ描画解放
    debug::finalizeDebugDraw();

    //----------------------------------------------------------
    // ImGuiを解放
    //----------------------------------------------------------
    graphics::imgui::finalize();

    //----------------------------------------------------------
    // 解放
    //----------------------------------------------------------
    if(dx11::callback::onFinalize) {
        dx11::callback::onFinalize();
    }

    // 物理シミュレーション解放
    physics::PhysicsEngine::instance()->finalize();
}

//---------------------------------------------------------------------------
//! 実体を取得
//---------------------------------------------------------------------------
graphics::RenderImpl* RenderImpl::instance()
{
    static graphics::RenderImpl instance;   // シングルトン実体
    return &instance;
}

//------------------------------------------------------------------------
//! レンダリング管理クラスを取得
//------------------------------------------------------------------------
graphics::Render* render()
{
    return graphics::RenderImpl::instance();
}

}   // namespace graphics

namespace dx11 {

//------------------------------------------------------------------------
//! DXGIFactoryを取得
//------------------------------------------------------------------------
IDXGIFactory4* dxgiFactory()
{
    return graphics::device11_.dxgiFactory();
}

//------------------------------------------------------------------------
//! D3D11デバイスを取得
//------------------------------------------------------------------------
ID3D11Device5* d3dDevice()
{
    return graphics::device11_.d3dDevice();
}

//------------------------------------------------------------------------
//! 直接コンテキストを取得
//------------------------------------------------------------------------
ID3D11DeviceContext4* immediateContext()
{
    return graphics::device11_.immediateContext();
}

//------------------------------------------------------------------------
//! スワップチェインを取得
//------------------------------------------------------------------------
dx11::SwapChain* swapChain()
{
    return graphics::swapChain_.get();
}

//------------------------------------------------------------------------
//! 共通ステートを取得
//------------------------------------------------------------------------
const DirectX::CommonStates& commonStates()
{
    return *graphics::commonStates_.get();
}

//------------------------------------------------------------------------
//! 現在のコンテキストを取得
//------------------------------------------------------------------------
ID3D11DeviceContext4* context()
{
    return dx11::immediateContext();
}

}   // namespace dx11
