﻿//---------------------------------------------------------------------------
//!	@file	graphics_render.h
//!	@brief	レンダリング管理
//---------------------------------------------------------------------------
#pragma once

namespace graphics {

//===========================================================================
//! レンダリング管理
//===========================================================================
class Render
{
public:
    virtual ~Render() = default;

    //! 初期化
    virtual bool initialize(u32 width, u32 height) = 0;

    //! 更新
    virtual void update() = 0;

    //! 描画
    virtual void render() = 0;

    //! 解放
    virtual void finalize() = 0;
};

//! レンダリング管理クラスを取得
graphics::Render* render();

}   // namespace graphics

//---------------------------------------------------------------------------
// DirectX11ショートカットAPI
//---------------------------------------------------------------------------
namespace dx11 {

//! 前方宣言
class Texture;
class SwapChain;
class Buffer;

//! DXGIFactoryを取得
IDXGIFactory4* dxgiFactory();

//! D3D11デバイスを取得
ID3D11Device5* d3dDevice();

//! 直接コンテキストを取得
ID3D11DeviceContext4* immediateContext();

//! スワップチェインを取得
dx11::SwapChain* swapChain();

//! 共通ステートを取得
const DirectX::CommonStates& commonStates();

//! 現在のコンテキストを取得
ID3D11DeviceContext4* context();

}   // namespace dx11