﻿//---------------------------------------------------------------------------
//!	@file	animation_impl.h
//!	@brief	3Dアニメーション(実装部)
//---------------------------------------------------------------------------
#pragma once

//===========================================================================
//! 3Dアニメーション(実装部)
//===========================================================================
class AnimationImpl final : public Animation
{
public:
    //! デフォルトコンストラクタ
    AnimationImpl() = default;

    //! デストラクタ
    virtual ~AnimationImpl() = default;

    //! アニメーションレイヤー個数を取得
    size_t layerCount() const { return _animationLayers.size(); }

    //! 更新
    //! @param  [in]    t       進める時間(単位:秒)
    virtual void update(f32 t) override;

    //! アニメーション再生リクエスト
    //! @param  [in]    name    名前
    //! @param  [in]    playType    アニメーション再生タイプ
    virtual bool play(const char* name, Animation::PlayType playType, f32 speed) override;

    //! アニメーションレイヤーを追加
    //! @param  [in]    name    アニメーション名(任意)
    //! @param  [in]    layer   アニメーションレイヤー
    virtual void appendLayer(const char* name, std::shared_ptr<AnimationLayer> layer) override;

    //! アニメーションレイヤーの数を取得
    virtual size_t animationLayerCount() const override;

    //! アニメーションレイヤーを取得
    //! @param  [in]    name    アニメーション名
    virtual const AnimationLayer* animationLayer(const char* name) const override;

    //! ポーズ情報の作成
    void buildPose(Pose& pose, const ModelImpl* model);

    //! 現在のアニメションはラストフレームかどうか
    bool IsLastFrame(f32 blendTime = 0.0f) const override;

    //!< 現在のアニメションの長さ
    f32 GetCurrentLayerAnimationLength() const override;

    //!< 現在のアニメションの再生時間
    f32 GetCurretLayerAnimationTime() const override;

    //!< アニメーションタイムを設定
    void SetAnimationTime(const f32 time) override;

    //!< ルートボーンIdを設定
    void SetRootBoneId(const s32 rootBoneId) override;

    //!< アニメションの再生プレイタイプを変更
    void SetPlayType(Animation::PlayType playType) override;

    //! アニメションを読み込む
    //! @param jAnim jsonのモデルデータ
    bool BindBlendList(const json& jAnim) override;

private:
    static constexpr f32 MAX_BLEND_TIME = 0.25f;   //!< ブレンド時、最大のアニメーションの遷移時間

    s32                 _rootBoneId  = -1;        //!<  //!< アニメションの常に原点にいるように(Id = root boneの位置)
    f32                 _time        = 0.0f;      //!< 現在の再生時間
    f32                 _speed       = 0.0f;      //!< アニメションの再生速度
    AnimationLayerImpl* _activeLayer = nullptr;   //!< 現在再生中のアニメーションレイヤー
    AnimationLayerImpl* _lastLayer   = nullptr;   //!< 前のアニメーションレイヤー
    Animation::PlayType _playType;                //!< アニメーション再生タイプ

    bool _lastFrame = false;   //!< 現在のアニメーションはラストフレームか
    f32  _blendTime = 0.0f;    //!< アニメーションの遷移時間

    //! アニメーションレイヤー
    std::unordered_map<std::string, std::shared_ptr<AnimationLayerImpl>> _animationLayers;

    // 高速化のために unordered_mapを使う
    // <From, <To, BlendDesc>>
    std::unordered_multimap<std::string, BlendDesc> _blendDescList;
};
