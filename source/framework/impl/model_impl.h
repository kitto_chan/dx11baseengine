﻿//---------------------------------------------------------------------------
//!	@file	model_impl.h
//!	@brief	3Dモデル
//---------------------------------------------------------------------------
#pragma once

class AnimationImpl;
class ModelImpl;

// 頂点フォーマット
struct ModelVertex
{
    DirectX::XMFLOAT3 position_;     //!< POSITION      座標
    Color             color_;        //!< COLOR         カラー
    DirectX::XMFLOAT2 uv_;           //!< TEXCOORD      テクスチャ座標
    DirectX::XMFLOAT3 normal_;       //!< NORMAL        法線
    DirectX::XMFLOAT3 tangent_;      //!< TANGENT       接線
    DirectX::XMINT4   skinIndex_;    //!< BLENDINDICES  スキニング番号
    DirectX::XMFLOAT4 skinWeight_;   //!< BLENDWEIGHT   スキニングウェイト
};
//---------------------------------------------------------------------------
//! メッシュ情報
//---------------------------------------------------------------------------
struct Mesh
{
    u32 vertexCount_         = 0;    //!< 頂点数
    u32 startVertexLocation_ = 0;    //!< 頂点の開始番号
    u32 indexCount_          = 0;    //!< インデックス数
    u32 startIndexLocation_  = 0;    //!< インデックスの開始番号
    s32 materialIndex_       = -1;   //!< マテリアル番号
};
//---------------------------------------------------------------------------
//! ローカル座標変換
//---------------------------------------------------------------------------
struct LocalTransform
{
    LocalTransform inverse() const
    {
        LocalTransform result;
        result.rotation_ = conjugate(rotation_);
        result.position_ = result.rotation_ * -position_;

        return result;
    }

    LocalTransform operator*(const LocalTransform& rhs) const
    {
        return { (rotation_ * rhs.position_) + position_, rotation_ * rhs.rotation_ };
    }

    matrix makeMatrix() const
    {
        auto m      = matrix(conjugate(rotation_));
        m._41_42_43 = position_;
        return m;
    }

    float3     position_{ 0.0f, 0.0f, 0.0f };         //!< 位置
    quaternion rotation_{ 0.0f, 0.0f, 0.0f, 1.0f };   //!< 回転
};

//---------------------------------------------------------------------------
//! ボーン情報
//---------------------------------------------------------------------------
struct Bone
{
public:
    std::string name_;               //!< 名前
    s32         index_       = -1;   //!< ボーン番号
    s32         parentIndex_ = -1;   //!< 親のボーン番号(-1の場合は親なし)

    LocalTransform transform_;           //!< 通常
    LocalTransform transformRelative_;   //!< 相対
    LocalTransform transformInverse_;    //!< 通常の逆変換
};

//---------------------------------------------------------------------------
//! テクスチャ情報
//---------------------------------------------------------------------------
class Texture
{
public:
    enum Type
    {
        DIFFUSE,      //!< ディフューズ
        NORMAL,       //!< 法線
        SPECULAR,     //!< スペキュラー
        SHININESS,    //!< シャイニネス
        AMBIENT,      //!< アンビエント
        EMISSIVE,     //!< エミッシブ発光
        REFLECTION,   //!< 反射率
        //----
        COUNT,
    };

    //! テクスチャの読み込み
    bool load()
    {
        texture_ = dx11::createTextureFromFile(path_);
        if(!texture_)
            return false;

        return true;
    }

    std::string                    path_;      //!< テクスチャのファイルパス
    std::shared_ptr<dx11::Texture> texture_;   //!< テクスチャ
};
//---------------------------------------------------------------------------
//! テクスチャ情報
//---------------------------------------------------------------------------
class PBRTexture : public Texture
{
public:
    //enum Type
    //{
    //    Normal,             //!< 法線
    //    AmbientOcclusion,   //!< AO MAP
    //    Roughness,          //!< ラフネス (Roughness / Glossiness / Shininess )
    //    EMISSIVE,           //!< エミッシブ発光
    //    //----
    //    COUNT,
    //};
};
//---------------------------------------------------------------------------
//! マテリアル情報
//---------------------------------------------------------------------------
struct Material
{
    std::shared_ptr<Texture>    textures_[Texture::COUNT];                    //!< テクスチャ
    std::shared_ptr<PBRTexture> pbrTextures_[Model::PBRTextureType::COUNT];   //!< テクスチャ
    float3                      diffuseColor_;                                //!< ディフューズカラー
    float3                      specularColor_;                               //!< スペキュラーカラー
    float3                      emissiveColor_;                               //!< エミッシブカラー
};

//===========================================================================
//! ポーズ
//===========================================================================
struct Pose
{
    //! デフォルトコンストラクタ
    Pose() = default;

    //! コンストラクタ
    Pose(size_t size)
    {
        positions_.resize(size);
        rotations_.resize(size);
    }

    //! 絶対位置を計算
    //! @param  [in]    ボーン情報を持つモデル
    void computeAbsolute(const ModelImpl& model);

    //! 相対位置を計算
    //! @param  [in]    ボーン情報を持つモデル
    void computeRelative(const ModelImpl& model);

    //! ポーズ同士を補間
    //! @param  [in]    rhs 右辺値のポーズ
    //! @param  [in]    weight  ブレンドウェイト(0.0f～1.0f)
    void blend(Pose& rhs, f32 weight);

    //! ボーン配列数を取得
    size_t size() const { return positions_.size(); }

    s32                     firstNonRootBoneIndex_ = -1;   //!< 最初の非ルートボーン番号
    std::vector<float3>     positions_;                    //!< Translation (ボーン数と同じ数)
    std::vector<quaternion> rotations_;                    //!< Rotation    (ボーン数と同じ数)
};

//===========================================================================
//! 3Dモデル
//===========================================================================
class ModelImpl final : public Model
{
public:
    static constexpr u32 JOINT_COUNT_MAX = 512;   // 関節数上限個数

    ModelImpl()          = default;
    virtual ~ModelImpl() = default;

    //! 更新
    virtual void update() override;

    //! 描画
    virtual void render() override;

    //! デバッグ描画
    virtual void renderDebug() override;

    //! アニメーションを設定
    virtual void bindAnimation(std::shared_ptr<Animation> animation) override;

    //! ワールド行列を設定
    virtual void setWorldMatrix(const matrix& matWorld) override;

    //! ボーンを取得
    const Bone* bone(s32 index) const { return &bones_[index]; }

    // ボーン番号を名前ハッシュで取得
    const s32 boneIndexFromHash(u32 nameHash) const
    {
        s32 boneIndex = -1;
        if(auto it = boneHashMap_.find(nameHash); it != std::end(boneHashMap_)) {
            boneIndex = it->second;
        }
        return boneIndex;
    }

    //! 初期化
    bool initialize();

    //! 頂点数を取得
    virtual u32 vertexCount() const override { return static_cast<u32>(geometryVertices_.size()); }

    //! 頂点配列を取得
    virtual const float3* vertices() const override { return geometryVertices_.data(); }

    //! インデックス数を取得
    virtual u32 indexCount() const override { return static_cast<u32>(geometryIndices_.size()); }

    //! インデックス配列を取得
    virtual const u32* indices() const override { return geometryIndices_.data(); }

    //! 外部（別の）DiffuseTexture設定する、fbx Fileのテクスチャ載ってない(かも)
    virtual void loadExternalDiffuseTexture(std::string_view texturePath) override;

    virtual bool SetPBRTexture(PBRTextureType type, std::string_view path) override;

    //! 特定のボーンの行列を取得
    virtual matrix GetBoneMatrix(s32 index) const override
    {
        if(!(index < bones_.size() && index >= 0)) ASSERT_MESSAGE(false, "ボーンのインデックスが不正確");
        if(bones_.size() == 0) return math::identity();

        return mul(bones_[index].transform_.makeMatrix(), matrices_[index]);
    };

    //! モデルなかに特定の頂点行列を取得
    virtual matrix GetGeometryVertexMatrix(s32 index) override;

    //! PBRモデルかどうかを設定する
    void SetIsPBRModel(bool isPBRModel) { _isPBRModel = isPBRModel; };

    void SetPhongMaterial(const cb::PhongMaterial& phongMaterial) override;

    virtual void SetIsRenderOutline(bool isRenderOutline) override { _isRenderOutLine = isRenderOutline; };

    virtual const std::vector<float3>* GetGeometryVertices() { return &geometryVertices_; };
    virtual const std::vector<u32>*    GetGeometryIndices() { return &geometryIndices_; };

private:
    // コピー禁止/move禁止
    ModelImpl(const ModelImpl&) = delete;
    ModelImpl(ModelImpl&&)      = delete;
    ModelImpl& operator=(const ModelImpl&) = delete;
    ModelImpl& operator=(ModelImpl&&) = delete;

public:
    matrix matWorld_ = math::identity();   //!< ワールド行列

    std::shared_ptr<dx11::Buffer> bufferVb_;   //!< 頂点バッファ
    std::shared_ptr<dx11::Buffer> bufferIb_;   //!< インデックスバッファ

    std::vector<float3> geometryVertices_;   //!< 衝突判定三角形情報
    std::vector<u32>    geometryIndices_;    //!< 衝突判定三角形インデックス情報

    std::vector<Mesh>              meshes_;      //!< メッシュ情報
    std::vector<Material>          materials_;   //!< マテリアル情報
    std::shared_ptr<AnimationImpl> animation_;   //!< アニメーション

    std::vector<Bone>            bones_;         //!< ボーン
    std::unordered_map<u32, s32> boneHashMap_;   //!< 名前ハッシュからボーン番号を索引
    Pose                         pose_;          //!< ポーズ情報

    std::shared_ptr<dx11::Shader>      shaderVs_3D_;              //!< VS 3D頂点シェーダー
    std::shared_ptr<dx11::Shader>      shaderVs_Model_4Skin_;     //!< VS スキニングモデル 4weight
    std::shared_ptr<dx11::Shader>      shaderVs_Model_Outline_;   //!< VS スキニングモデル アウトライン用
    std::shared_ptr<dx11::Shader>      shaderPs_Texture_;         //!< PS テクスチャあり
    std::shared_ptr<dx11::Shader>      shaderPs_PBRTexture_;      //!< PS テクスチャあり(PBR)
    std::shared_ptr<dx11::Shader>      shaderPs_Flat_;            //!< PS テクスチャなし
    std::shared_ptr<dx11::InputLayout> inputLayout_;              //!< 入力レイアウト

    std::array<matrix, JOINT_COUNT_MAX> matrices_;

    cb::PhongMaterial _phongMaterial;             //!< phong Lighting Modele
    bool              _isPBRModel      = false;   //!< PBRのモデルかどうか
    bool              _isRenderOutLine = false;   //!< モデルアウトラインを描画するか
};
