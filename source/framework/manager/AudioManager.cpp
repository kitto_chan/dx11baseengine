﻿//---------------------------------------------------------------------------
//!	@file	AudioManager.h
//!	@brief	音声オーディオ管理
//---------------------------------------------------------------------------
#include "AudioManager.h"

namespace manager {
namespace {

}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
AudioManager::AudioManager()
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
AudioManager::~AudioManager()
{
    if(_pAudioEngine) {
        _pAudioEngine->Suspend();
    }
    _pSoundBGMInstance.reset();
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool AudioManager::Init()
{
    if(_inited) {
        ASSERT_MESSAGE(false, "2回目初期化してた");
        return true;
    }

    HRESULT hr = CoInitializeEx(nullptr, COINIT_MULTITHREADED);
    if(FAILED(hr))
        return false;

    DirectX::AUDIO_ENGINE_FLAGS eflags = DirectX::AUDIO_ENGINE_FLAGS::AudioEngine_Default;
#ifdef _DEBUG
    eflags |= DirectX::AudioEngine_Debug;
#endif
    _pAudioEngine = std::make_unique<DirectX::AudioEngine>(eflags);

    _pSoundBGM         = std::make_unique<DirectX::SoundEffect>(_pAudioEngine.get(), L"audio/bgm_fantasy.wav");
    _pSoundBGMInstance = _pSoundBGM->CreateInstance();

    _pSoundBGMInstance->Play(true);
    SetBgmVolume(_bgmVolume);

    _inited = true;
    return true;
}

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void AudioManager::Update()
{
    if(!_pAudioEngine->Update()) {
        // エラーがあったら、もう一度再生する
        if(_pAudioEngine->IsCriticalError()) {
            _pSoundBGMInstance->Play(true);
        }
    }
}

//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void AudioManager::Render()
{
}
//---------------------------------------------------------------------------
//! ImGui描画
//---------------------------------------------------------------------------
void AudioManager::RenderImgui()
{
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void AudioManager::Finalize()
{
    // 廃棄
}
//---------------------------------------------------------------------------
//! 一時停止
//---------------------------------------------------------------------------
void AudioManager::Suspend()
{
    _pAudioEngine->Suspend();
}
//---------------------------------------------------------------------------
//! 再生
//---------------------------------------------------------------------------
void AudioManager::Resume()
{
    _pAudioEngine->Resume();
}
//---------------------------------------------------------------------------
//! SE音量設定(値: 0 ~ 1);
//---------------------------------------------------------------------------
void AudioManager::SetSEVolume(f32 volume)
{
    _seVolume = std::clamp(volume, 0.0f, 1.0f);
}
//---------------------------------------------------------------------------
//! SE音量加減(値 : 0 ~ 1);
//---------------------------------------------------------------------------
void AudioManager::AddSEVolume(f32 add)
{
    _seVolume += add;
    SetSEVolume(_seVolume);
}
//---------------------------------------------------------------------------
//! BGM音量設定(値: 0 ~ 1);
//---------------------------------------------------------------------------
void AudioManager::SetBgmVolume(f32 volume)
{
    _bgmVolume = std::clamp(volume, 0.0f, 1.0f);
    _pSoundBGMInstance->SetVolume(_bgmVolume);
}
//---------------------------------------------------------------------------
//! BGM音量加減(値 : 0 ~ 1);
//---------------------------------------------------------------------------
void AudioManager::AddBgmVolume(f32 add)
{
    _bgmVolume += add;
    SetBgmVolume(_bgmVolume);
}
//---------------------------------------------------------------------------
//! Effectseer管理クラスを取得
//---------------------------------------------------------------------------
manager::AudioManager* AudioMgr()
{
    return AudioManager::Instance();
}
}   // namespace manager
