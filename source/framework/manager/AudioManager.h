﻿//---------------------------------------------------------------------------
//!	@file	AudioManager.h
//!	@brief	音声オーディオ管理
//---------------------------------------------------------------------------
#pragma once

namespace manager {

class AudioManager : public Singleton<AudioManager>
{
public:
    AudioManager();
    ~AudioManager();

    //---------------------------------------------------------------------------
    // 関数
    //---------------------------------------------------------------------------
    bool Init();          //!< 初期化
    void Update();        //!< 更新
    void Render();        //!< 描画
    void RenderImgui();   //!< Imgui描画
    void Finalize();      //!< 解放

    void Suspend();   //!< 一時停止
    void Resume();    //!< 再生

    void SetBgmVolume(f32 volume);               //!< BGM音量設定(値: 0 ~ 1);
    f32  GetBgmVolume() { return _bgmVolume; }   //!< BGM音量取得(値: 0 ~ 1);
    void AddBgmVolume(f32 add);                  //!< BGM音量加減(値: 0 ~ 1);

    void SetSEVolume(f32 volume);              //!< SE音量設定(値: 0 ~ 1);
    f32  GetSEVolume() { return _seVolume; }   //!< SE音量取得(値: 0 ~ 1);
    void AddSEVolume(f32 add);                 //!< SE音量加減(値: 0 ~ 1);
private:
    //---------------------------------------------------------------------------
    // 内部変数
    //---------------------------------------------------------------------------
    uni_ptr<DirectX::AudioEngine>         _pAudioEngine;        //!< オーディオエンジン全体的のメインクラス
    uni_ptr<DirectX::SoundEffect>         _pSoundBGM;           //!< SoundEffectInstanceを作成するために使用する
    uni_ptr<DirectX::SoundEffectInstance> _pSoundBGMInstance;   //!< サウンドの単一の再生、ループ、一時停止、または停止のインスタンスを提供します。

    bool _inited = false;   //!< 初期化した?

    f32 _bgmVolume = 0.0f;   //!< BGMサウンドエフェクトの音量
    f32 _seVolume  = 0.0f;   //!< SEサウンドエフェクトの音量
};
//! カメラ管理クラスを取得
manager::AudioManager* AudioMgr();

}   // namespace manager
