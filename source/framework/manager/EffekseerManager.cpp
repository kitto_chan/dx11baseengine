﻿//---------------------------------------------------------------------------
//!	@file	EffekseerManager.h
//!	@brief	ゲームカメラ管理
//---------------------------------------------------------------------------
#include "EffekseerManager.h"

namespace manager {
namespace {

}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
EffekseerManager::EffekseerManager()
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
EffekseerManager::~EffekseerManager()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool EffekseerManager::Init()
{
    _isPlayAllEffect = SystemSettingsMgr()->IsPlayState();

    //=====================================
    // EffkeseerのManagerとRendererを生成
    //=====================================
    ID3D11Device*        d3D11Device        = dx11::d3dDevice();
    ID3D11DeviceContext* d3D11DeviceContext = dx11::context();
    _effekseerRenderer                      = EffekseerRendererDX11::Renderer::Create(d3D11Device, d3D11DeviceContext, 8000);

    // エフェクトのマネージャーの作成
    _effekseerManager = ::Effekseer::Manager::Create(8000);

    // 描画モジュールの設定
    _effekseerManager->SetSpriteRenderer(_effekseerRenderer->CreateSpriteRenderer());
    _effekseerManager->SetRibbonRenderer(_effekseerRenderer->CreateRibbonRenderer());
    _effekseerManager->SetRingRenderer(_effekseerRenderer->CreateRingRenderer());
    _effekseerManager->SetTrackRenderer(_effekseerRenderer->CreateTrackRenderer());
    _effekseerManager->SetModelRenderer(_effekseerRenderer->CreateModelRenderer());

    // テクスチャ、モデル、カーブ、マテリアルローダーの設定する。
    // ユーザーが独自で拡張できる。現在はファイルから読み込んでいる。
    _effekseerManager->SetTextureLoader(_effekseerRenderer->CreateTextureLoader());
    _effekseerManager->SetModelLoader(_effekseerRenderer->CreateModelLoader());
    _effekseerManager->SetMaterialLoader(_effekseerRenderer->CreateMaterialLoader());
    _effekseerManager->SetCurveLoader(Effekseer::MakeRefPtr<Effekseer::CurveLoader>());
    return true;
}

void EffekseerManager::SetCamera(const matrix& projMatrix, const matrix& viewMatrix)
{
    // 投影行列を設定
    _effekseerRenderer->SetProjectionMatrix(math::CastToEffekseerMatrix44(projMatrix));

    // カメラ行列を設定
    _effekseerRenderer->SetCameraMatrix(math::CastToEffekseerMatrix44(viewMatrix));
}

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void EffekseerManager::Update(const matrix& projMatrix, const matrix& viewMatrix)
{
    //// 投影行列を設定
    _effekseerRenderer->SetProjectionMatrix(math::CastToEffekseerMatrix44(projMatrix));

    //// カメラ行列を設定
    _effekseerRenderer->SetCameraMatrix(math::CastToEffekseerMatrix44(viewMatrix));

    // マネージャーの更新
    _effekseerManager->Update();
	// 時間を更新する
	_effekseerRenderer->SetTime(timer::DeltaTime());
}

//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void EffekseerManager::Render()
{
    // エフェクトの描画開始処理を行う。
    _effekseerRenderer->BeginRendering();

    // エフェクトの描画を行う。
    _effekseerManager->Draw();

    // エフェクトの描画終了処理を行う。
    _effekseerRenderer->EndRendering();
}

//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void EffekseerManager::Finalize()
{
    // 廃棄
    _effekseerRenderer.Reset();
    _effekseerManager.Reset();
}
//---------------------------------------------------------------------------
//! エフェクト回転を設定
//---------------------------------------------------------------------------
void EffekseerManager::SetRotation(Effekseer::Handle handle, const float3& rot)
{
    _effekseerManager->SetRotation(handle, rot.f32[0], rot.f32[1], rot.f32[2]);
}
//---------------------------------------------------------------------------
//! エフェクトの拡大縮小を設定
//---------------------------------------------------------------------------
void EffekseerManager::SetScale(Effekseer::Handle handle, const float3& scale)
{
    _effekseerManager->SetScale(handle, scale.f32[0], scale.f32[1], scale.f32[2]);
}
//---------------------------------------------------------------------------
//! エフェクト再生
//---------------------------------------------------------------------------
Effekseer::Handle EffekseerManager::Play(const Effekseer::EffectRef& effect, const float3& pos)
{
    return _effekseerManager->Play(effect, pos.f32[0], pos.f32[1], pos.f32[2]);
}
//---------------------------------------------------------------------------
//! エフェクト生成
//---------------------------------------------------------------------------
Effekseer::EffectRef EffekseerManager::Create(std::u16string_view sv)
{
    return Effekseer::Effect::Create(_effekseerManager, sv.data());
}
//---------------------------------------------------------------------------
//! 停止
//---------------------------------------------------------------------------
void EffekseerManager::StopEffect(const Effekseer::Handle& handle)
{
    _effekseerManager->StopEffect(handle);
}
//---------------------------------------------------------------------------
//! 全体的なエフェクト再生、一時停止の設定
//---------------------------------------------------------------------------
void EffekseerManager::SetPausedToAllEffects(bool b)
{
    if(_effekseerManager != NULL)
        _effekseerManager->SetPausedToAllEffects(b);
}
void EffekseerManager::SwapIsPlayAllEffect()
{
    _isPlayAllEffect = !_isPlayAllEffect;

	if(_isPlayAllEffect) {
        SetPausedToAllEffects(false);
    }
    else {
        SetPausedToAllEffects(true);
	}
}
//---------------------------------------------------------------------------
//! Effectseer管理クラスを取得
//---------------------------------------------------------------------------
manager::EffekseerManager* EffekseerMgr()
{
    return EffekseerManager::Instance();
}
}   // namespace manager
