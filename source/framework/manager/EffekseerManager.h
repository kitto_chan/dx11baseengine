﻿//---------------------------------------------------------------------------
//!	@file	CameraManager.h
//!	@brief	ゲームカメラ管理
//---------------------------------------------------------------------------
#pragma once
#pragma warning(push)
#pragma warning(disable : 4100 26451 26812 26495)
#include "Effekseer/include/Effekseer/Effekseer.h"
#include "Effekseer/include/EffekseerRendererDX11/EffekseerRendererDX11.h"
#pragma warning(pop)

#include "math/MathEffekeer.h"

namespace manager {

class EffekseerManager : public Singleton<EffekseerManager>
{
public:
    EffekseerManager();    //!< コンストラクタ
    ~EffekseerManager();   //!< デストラクタ

    //---------------------------------------------------------------------------
    // 関数
    //---------------------------------------------------------------------------
    bool Init();                                                       //!< 初期化
	void SetCamera(const matrix& projMatrix, const matrix& viewMatrix);
    void Update(const matrix& projMatrix, const matrix& viewMatrix);   //!< 更新
    void Render();                                                     //!< 描画
    void Finalize();                                                   //!< 解放

    void SetRotation(Effekseer::Handle handle, const float3& rot);   //!< エフェクト回転を設定
    void SetScale(Effekseer::Handle handle, const float3& scale);    //!< エフェクトの拡大縮小を設定

    //! エフェクト再生
    Effekseer::Handle Play(const Effekseer::EffectRef& effect, const float3& pos);
    //! エフェクト生成
    Effekseer::EffectRef Create(std::u16string_view sv);
	//! エフェクト停止
	void StopEffect(const Effekseer::Handle& handle);

    //! 全体的なエフェクト再生、一時停止の設定
    void SetPausedToAllEffects(bool b);

	//!< _isPlayAllEffectフラグを true <=> falseにスワップ
    void SwapIsPlayAllEffect();


private:
    //---------------------------------------------------------------------------
    // 内部変数
    //---------------------------------------------------------------------------
    EffekseerRendererDX11::RendererRef _effekseerRenderer;   //!< エフェクトレンダラー
    Effekseer::ManagerRef              _effekseerManager;    //!< エフェクトマネージャー

    bool _isPlayAllEffect = false; //!< すべてのエフェクトを再生するか
};
//! カメラ管理クラスを取得
manager::EffekseerManager* EffekseerMgr();

}   // namespace manager
