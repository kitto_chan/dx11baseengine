﻿////---------------------------------------------------------------------------
////!	@file	ImguiManager.h
////!	@brief	Dear ImGui 組み込みユーティリティー
////---------------------------------------------------------------------------
//#pragma once
//namespace manager {
//
//class ImguiManager : public Singleton<ImguiManager>
//{
//public:
//    ImguiManager();
//    ~ImguiManager();
//
//    //---------------------------------------------------------------------------
//    //! 関数
//    //---------------------------------------------------------------------------
//    bool Init();         //! 初期化
//    void BeginFrame();   //! フレーム更新開始
//    void EndFrame();     //! フレーム更新終了
//    void Render();       //! 描画
//    void Finalize();     //! 解放
//
//    //! ウィンドウプロシージャ
//    LRESULT WindowProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam);
//
//private:
//    //---------------------------------------------------------------------------
//    //! 内部変数
//    //---------------------------------------------------------------------------
//};
//
////! ゲーム管理クラスを取得
//manager::ImguiManager* ImguiMgr();
//}   // namespace manager
