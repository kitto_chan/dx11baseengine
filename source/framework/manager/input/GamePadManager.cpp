﻿//---------------------------------------------------------------------------
//!	@file	GamePadManager.cpp
//!	@brief	キーボードマネジャー
//---------------------------------------------------------------------------
#include "GamePadManager.h"
namespace input {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
GamePadManager::GamePadManager()
{
    _pGamePad = std::make_unique<DirectX::GamePad>();
}
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
GamePadManager::~GamePadManager()
{
    if(_pGamePad) {
        _pGamePad->Suspend();
    }
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void GamePadManager::Update()
{
    _gamePadstate = _pGamePad->GetState(0);
    if(_gamePadstate.IsConnected()) {
        _gamepadTracker.Update(_gamePadstate);
    }
    else {
        _gamepadTracker.Reset();
    }

    //----------
    // 振動処理
    if(_vibrationFrame > 0) {
        _vibrationFrame--;
    }
    else {
        // 停止
        SetVibration(0.0, 0.0f, -1);
    }
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
bool GamePadManager::SetVibration(f32 right, f32 left, s32 frame)
{
    _vibrationFrame = frame;
    return _pGamePad->SetVibration(0, right, left);
}
bool IsGamePadConnected()
{
    return GamePadMgr()->IsConnected();
}
//---------------------------------------------------------------------------
//!  キーボード管理クラスを取得
//---------------------------------------------------------------------------
GamePadManager* GamePadMgr()
{
    return GamePadManager::Instance();
}
}   // namespace input
