﻿//---------------------------------------------------------------------------
//!	@file	KeyBoardManager.h
//!	@brief	キーボードマネジャー
//---------------------------------------------------------------------------
#pragma once

namespace input {
class GamePadManager : public Singleton<GamePadManager>
{
public:
    GamePadManager();    //!< コンストラクタ
    ~GamePadManager();   //!< デストラクタ

    //---------------------------------------------------------------------------
    //! 関数
    //---------------------------------------------------------------------------
    void Update();   //!< 更新

    void Suspend() { _pGamePad->Suspend(); };   //!< 一時停止
    void Resume() { _pGamePad->Resume(); };     //!< 再開

    bool IsConnected() { return _gamePadstate.IsConnected(); };   //!< ゲームパッド繋がているか

    //==========
    // ThumbSticks
    //==========
    float2 GetLeftThumbSticks() { return float2(GetLeftThumbSticksX(), GetLeftThumbSticksY()); };
    f32    GetLeftThumbSticksX() { return _gamePadstate.thumbSticks.leftX; };
    f32    GetLeftThumbSticksY() { return _gamePadstate.thumbSticks.leftY; };

    float2 GetRightThumbSticks() { return float2(GetRightThumbSticksX(), GetRightThumbSticksY()); };
    f32    GetRightThumbSticksX() { return _gamePadstate.thumbSticks.rightX; };
    f32    GetRightThumbSticksY() { return _gamePadstate.thumbSticks.rightY; };

    //==========
    // A B X Yキー
    //==========
    bool IsAPressing() { return _gamePadstate.IsAPressed(); };
    bool IsBPressing() { return _gamePadstate.IsBPressed(); };
    bool IsXPressing() { return _gamePadstate.IsXPressed(); };
    bool IsYPressing() { return _gamePadstate.IsYPressed(); };

    bool IsAPressed() { return _gamepadTracker.a == DirectX::GamePad::ButtonStateTracker::PRESSED; };
    bool IsBPressed() { return _gamepadTracker.b == DirectX::GamePad::ButtonStateTracker::PRESSED; };
    bool IsXPressed() { return _gamepadTracker.x == DirectX::GamePad::ButtonStateTracker::PRESSED; };
    bool IsYPressed() { return _gamepadTracker.y == DirectX::GamePad::ButtonStateTracker::PRESSED; };

    //==========
    // DPad
    //==========
    bool IsDPadLeftPressing() { return _gamePadstate.IsDPadLeftPressed(); };
    bool IsDPadRightPressing() { return _gamePadstate.IsDPadRightPressed(); };
    bool IsDPadDownPressing() { return _gamePadstate.IsDPadDownPressed(); };
    bool IsDPadUpPressing() { return _gamePadstate.IsDPadUpPressed(); };

    bool IsDPadLeftPressed() { return _gamepadTracker.dpadLeft == DirectX::GamePad::ButtonStateTracker::PRESSED; };
    bool IsDPadRightPressed() { return _gamepadTracker.dpadRight == DirectX::GamePad::ButtonStateTracker::PRESSED; };
    bool IsDPadDownPressed() { return _gamepadTracker.dpadDown == DirectX::GamePad::ButtonStateTracker::PRESSED; };
    bool IsDPadUpPressed() { return _gamepadTracker.dpadUp == DirectX::GamePad::ButtonStateTracker::PRESSED; };

    bool IsRPressed() { return _gamePadstate.IsRightShoulderPressed(); }

    bool IsViewPressed() { return _gamepadTracker.view == DirectX::GamePad::ButtonStateTracker::PRESSED; };

    //! @brief 振動設定
    //! @param right 右の振動の力
    //! @param left  左の振動の力
    //! @param frame 持続時間(フレーム)
    //! @return 成功か
    bool SetVibration(f32 right, f32 left, s32 frame);

private:
    uni_ptr<DirectX::GamePad>            _pGamePad;         //!<　DirectXTKのゲームパッドクラス
    DirectX::GamePad::State              _gamePadstate;     //!< ゲームパッド状態
    DirectX::GamePad::ButtonStateTracker _gamepadTracker;   //!< ゲームパッド状態追跡

    s32 _vibrationFrame = 0;   //!< 振動の持続時間(フレーム)
};

//! ゲームパッド検出
bool IsGamePadConnected();

GamePadManager* GamePadMgr();
}   // namespace input
