﻿//---------------------------------------------------------------------------
//!	@file	InputHelper.h
//!	@brief	入力用のヘルパー
//---------------------------------------------------------------------------
#include "InputHelper.h"

namespace input {
//---------------------------------------------------------------------------
//! 次の入力キーを一回押す
//! @note 下キー / 右キー/ GamePad - D Pad 下
//---------------------------------------------------------------------------
bool IsPressSelectKeyForward()
{
    return input::IsKeyPress(DirectX::Keyboard::Down) ||
           input::IsKeyPress(DirectX::Keyboard::Right) ||
           input::GamePadMgr()->IsDPadDownPressed();
}
//---------------------------------------------------------------------------
//! 次の入力キーを押している
//! @note 下キー / 右キー/ GamePad - D Pad 下
//---------------------------------------------------------------------------
bool IsPressingSelectKeyForward()
{
    return input::IsKeyDown(DirectX::Keyboard::Down) ||
           input::IsKeyDown(DirectX::Keyboard::Right) ||
           input::GamePadMgr()->IsDPadDownPressing();
}
//---------------------------------------------------------------------------
//! 確認入力キーを一回押す
//! @note Space / Enter / GamePad - B
//---------------------------------------------------------------------------
bool IsPressSelectKeyConform()
{
    return KeyboardMgr()->IsKeyPress(DirectX::Keyboard::Space) ||
           KeyboardMgr()->IsKeyPress(DirectX::Keyboard::Enter) ||
           input::GamePadMgr()->IsBPressed();
}
//---------------------------------------------------------------------------
//! 次の入力キーを一回押す
//! @note 上キー / 左キー/ GamePad - D Pad 上
//---------------------------------------------------------------------------
bool IsPressSelectKeyBackward()
{
    return input::IsKeyPress(DirectX::Keyboard::Up) ||
           input::IsKeyPress(DirectX::Keyboard::Left) ||
           input::GamePadMgr()->IsDPadUpPressed();
}
bool IsPressingSelectKeyBackward()
{
    return input::IsKeyDown(DirectX::Keyboard::Up) ||
           input::IsKeyDown(DirectX::Keyboard::Left) ||
           input::GamePadMgr()->IsDPadUpPressing();
}
bool IsPressSelectKeyUp()
{
    return input::IsKeyPress(DirectX::Keyboard::Up) ||
           input::GamePadMgr()->IsDPadUpPressed();
}
bool IsPressSelectKeyDown()
{
    return input::IsKeyPress(DirectX::Keyboard::Down) ||
           input::GamePadMgr()->IsDPadDownPressed();
}
bool IsPressSelectKeyLeft()
{
    return input::IsKeyPress(DirectX::Keyboard::Left) ||
           input::GamePadMgr()->IsDPadLeftPressed();
}
bool IsPressSelectKeyRight()
{
    return input::IsKeyPress(DirectX::Keyboard::Right) ||
           input::GamePadMgr()->IsDPadRightPressed();
}
bool IsPressingSelectKeyUp()
{
    return input::IsKeyDown(DirectX::Keyboard::Up) ||
           input::GamePadMgr()->IsDPadUpPressing();
}
bool IsPressingSelectKeyDown()
{
    return input::IsKeyDown(DirectX::Keyboard::Down) ||
           input::GamePadMgr()->IsDPadDownPressing();
}
bool IsPressingSelectKeyLeft()
{
    return input::IsKeyDown(DirectX::Keyboard::Left) ||
           input::GamePadMgr()->IsDPadLeftPressing();
}
bool IsPressingSelectKeyRight()
{
    return input::IsKeyDown(DirectX::Keyboard::Right) ||
           input::GamePadMgr()->IsDPadRightPressing();
}
}   // namespace input
