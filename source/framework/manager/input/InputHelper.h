﻿//---------------------------------------------------------------------------
//!	@file	InputHelper.h
//!	@brief	入力用のヘルパー
//---------------------------------------------------------------------------
#pragma once
namespace input {
//! 確認入力キーを一回押す
//! @note Space / Enter / GamePad - B
bool IsPressSelectKeyConform();

//! 次の入力キーを一回押す
//! @note 下キー / 右キー/ GamePad - D Pad 下
bool IsPressSelectKeyForward();
//! 次の入力キーを押している
//! @note 下キー / 右キー/ GamePad - D Pad 下
bool IsPressingSelectKeyForward();

//! 次の入力キーを一回押す
//! @note 上キー / 左キー/ GamePad - D Pad 上
bool IsPressSelectKeyBackward();
//! 次の入力キーを押している
//! @note 上キー / 左キー/ GamePad - D Pad 上
bool IsPressingSelectKeyBackward();

//! 一回押す: 上キー /  GamePad - D Pad 上
bool IsPressSelectKeyUp();
//! 一回押す: 下キー /  GamePad - D Pad 下
bool IsPressSelectKeyDown();
//! 一回押す: 左キー /  GamePad - D Pad 左
bool IsPressSelectKeyLeft();
//! 一回押す: 右キー /  GamePad - D Pad 右
bool IsPressSelectKeyRight();

//! 一回押す: 上キー /  GamePad - D Pad 上
bool IsPressingSelectKeyUp();
//! 一回押す: 下キー /  GamePad - D Pad 下
bool IsPressingSelectKeyDown();
//! 一回押す: 左キー /  GamePad - D Pad 左
bool IsPressingSelectKeyLeft();
//! 一回押す: 右キー /  GamePad - D Pad 右
bool IsPressingSelectKeyRight();
}   // namespace input
