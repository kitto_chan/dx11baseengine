﻿//---------------------------------------------------------------------------
//!	@file	KeyBoardManager.h
//!	@brief	キーボードマネジャー
//---------------------------------------------------------------------------
#pragma once

namespace input {
using dx_kb = DirectX::Keyboard;
class KeyboardManager : public Singleton<KeyboardManager>
{
public:
    KeyboardManager();    //!< コンストラクタ
    ~KeyboardManager();   //!< デストラクタ

    //---------------------------------------------------------------------------
    //! 関数
    //---------------------------------------------------------------------------
    //bool Init();     //! 初期化
    void               Update();                                  //!< 更新
    bool               IsKeyDown(DirectX::Keyboard::Keys key);    //!< 押している
    bool               IsKeyPress(DirectX::Keyboard::Keys key);   //!< 一回押す
    DirectX::Keyboard* GetKeyboard();                             //!< キーボードを取得

private:
    DirectX::Keyboard::State                _keyState;          //!< キーボードステートを取得
    DirectX::Keyboard::State                _lastKeyState;      //!< 前フレームのキーボードステートを取得
    DirectX::Keyboard::KeyboardStateTracker _keyboardTracker;   //!< 押すなど物理的な操作ステート

    uni_ptr<DirectX::Keyboard> _pKeyboard;   //!<　DirectXTKのキーボードクラス
};
//! 押している
bool IsKeyDown(DirectX::Keyboard::Keys key);
//! 一回押す
bool IsKeyPress(DirectX::Keyboard::Keys key);

}   // namespace input
input::KeyboardManager* KeyboardMgr();
