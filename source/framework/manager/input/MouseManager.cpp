﻿//---------------------------------------------------------------------------
//!	@file	MouseManager.cpp
//!	@brief	マウスマネジャー
//! @ref https://github.com/microsoft/DirectXTK/wiki/Mouse
//---------------------------------------------------------------------------
#include "MouseManager.h"

namespace input {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
MouseManager::MouseManager()
{
    _pMouse = std::make_unique<DirectX::Mouse>();
}
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
MouseManager::~MouseManager()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool MouseManager::Init(HWND hwnd)
{
    _pMouse->SetWindow(hwnd);

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void MouseManager::Update()
{
    _mouseState     = _pMouse->GetState();
    _lastMouseState = _mouseTracker.GetLastState();
    _mouseTracker.Update(_mouseState);

    //if(KeyboardMgr()->IsKeyPress(dx_kb::F2) &&
    //   IsRelativeMode()) {
    //    // Imguiのマウスキャプチャを無視する
    //    SetAbsoluteMode();
    //    SystemSettingsMgr()->SwapMode();
    //}
    //if(KeyboardMgr()->IsKeyPress(dx_kb::F2) &&
    //   IsAbsoluteMode()) {
    //    // Imguiのマウスキャプチャを有効する
    //    SetRelativeMode();
    //    SystemSettingsMgr()->SwapMode();
    //}

	if(KeyboardMgr()->IsKeyPress(dx_kb::F2)) {
        if(IsRelativeMode()) {
            SetAbsoluteMode();
            SystemSettingsMgr()->SwapSysState();
        }
        else if(IsAbsoluteMode()) {
            // Imguiのマウスキャプチャを有効する
            SetRelativeMode();
            SystemSettingsMgr()->SwapSysState();
		}
	}
}
//---------------------------------------------------------------------------
//!< マウスモードのステート
//---------------------------------------------------------------------------
void MouseManager::SetMode(DirectX::Mouse::Mode mode)
{
    _pMouse->SetMode(mode);
}
//---------------------------------------------------------------------------
//! マウスモード Relative
//! For 'mouse-look' behavior in games
//---------------------------------------------------------------------------
void MouseManager::SetRelativeMode()
{
    ImGui::GetIO().ConfigFlags |= ImGuiConfigFlags_NoMouse;
    SetMode(DirectX::Mouse::MODE_RELATIVE);
    
    //_pMouse->SetVisible(false);
}
//---------------------------------------------------------------------------
//! マウスモード Absolute
//! マウスX、Y座標の絶対値
//---------------------------------------------------------------------------
void MouseManager::SetAbsoluteMode()
{
    //Imguiのマウスキャプチャを無視する
    ImGui::GetIO().ConfigFlags &= ~ImGuiConfigFlags_NoMouse;
    SetMode(DirectX::Mouse::MODE_ABSOLUTE);
    //_pMouse->SetVisible(true);
}
//---------------------------------------------------------------------------
//!< チェックマウスモード RelativeMode
//---------------------------------------------------------------------------
bool MouseManager::IsRelativeMode()
{
    return _mouseState.positionMode == DirectX::Mouse::MODE_RELATIVE;
}
//---------------------------------------------------------------------------
//!< チェックマウスモード AbsoluteMode
//---------------------------------------------------------------------------
bool MouseManager::IsAbsoluteMode()
{
    return _mouseState.positionMode == DirectX::Mouse::MODE_ABSOLUTE;
}
//---------------------------------------------------------------------------
//!< マウス左ボタンを押したかどうか
//---------------------------------------------------------------------------
bool MouseManager::IsPressLeftButton()
{
    return _mouseTracker.leftButton == DirectX::Mouse::ButtonStateTracker::PRESSED;
}
//---------------------------------------------------------------------------
//!< マウス左ボタンを押しているかどうか
//---------------------------------------------------------------------------
bool MouseManager::IsHeldLeftButton()
{
    return _mouseTracker.leftButton == DirectX::Mouse::ButtonStateTracker::HELD;
}
//---------------------------------------------------------------------------
//!< マウス左ボタンはこのフレームでリリースしたかどうか
//---------------------------------------------------------------------------
bool MouseManager::IsReleasedLeftButton()
{
    return _mouseTracker.leftButton == DirectX::Mouse::ButtonStateTracker::RELEASED;
}
//---------------------------------------------------------------------------
//!< マウスホイールを押しているか
//---------------------------------------------------------------------------
bool MouseManager::IsHeldMiddleButton()
{
    return _mouseTracker.middleButton == DirectX::Mouse::ButtonStateTracker::HELD;
}
//---------------------------------------------------------------------------
//!< マウス右ボタンを押したかどうか
//---------------------------------------------------------------------------
bool MouseManager::IsPressRightButton()
{
    return _mouseTracker.rightButton == DirectX::Mouse::ButtonStateTracker::PRESSED;
}
//---------------------------------------------------------------------------
//!< マウス右ボタンを押しているか
//---------------------------------------------------------------------------
bool MouseManager::IsHeldRightButton()
{
    return _mouseTracker.rightButton == DirectX::Mouse::ButtonStateTracker::HELD;
}
//---------------------------------------------------------------------------
//!< マウス右ボタンはこのフレームでリリースしたかどうか
//---------------------------------------------------------------------------
bool MouseManager::IsReleasedRightButton()
{
    return _mouseTracker.rightButton == DirectX::Mouse::ButtonStateTracker::RELEASED;
}
//---------------------------------------------------------------------------
//!< スクロールホイール数値をリセット
//---------------------------------------------------------------------------
void MouseManager::ResetScrollWheelValue()
{
    _pMouse->ResetScrollWheelValue();
}
//---------------------------------------------------------------------------
//!< マウス X 座標を取得
//---------------------------------------------------------------------------
s32 MouseManager::GetPosX()
{
    return _mouseState.x;
}
//---------------------------------------------------------------------------
//!< マウス Y 座標を取得
//---------------------------------------------------------------------------
s32 MouseManager::GetPosY()
{
    return _mouseState.y;
}
//---------------------------------------------------------------------------
//!< マウス中のスクロール輪の数値
//---------------------------------------------------------------------------
s32 MouseManager::GetScrollWheelValue()
{
    return _mouseState.scrollWheelValue;
}
//---------------------------------------------------------------------------
//! マウスを取得
//---------------------------------------------------------------------------
DirectX::Mouse* MouseManager::GetMouse()
{
    return _pMouse.get();
}
//---------------------------------------------------------------------------
//! マウスの状態を取得
//---------------------------------------------------------------------------
DirectX::Mouse::State MouseManager::GetMouseState()
{
    return _mouseState;
}
//---------------------------------------------------------------------------
//!< マウス左ボタンを押した
//!--------------------------------------------------------------------------
bool IsPressLeftButton()
{
    return MouseMgr()->IsPressLeftButton();
}
}   // namespace input

//---------------------------------------------------------------------------
//! ゲーム管理クラスを取得
//---------------------------------------------------------------------------
input::MouseManager* MouseMgr()
{
    return input::MouseManager::Instance();
}
