﻿//---------------------------------------------------------------------------
//!	@file	MouseManager.h
//!	@brief	マウスマネジャー
//! @ref https://github.com/microsoft/DirectXTK/wiki/Mouse
//---------------------------------------------------------------------------
#pragma once
#include "pattern/Singleton.h"
namespace input {
class MouseManager : public Singleton<MouseManager>
{
public:
    MouseManager();
    ~MouseManager();

    //---------------------------------------------------------------------------
    // 関数
    //---------------------------------------------------------------------------

    //! 初期化
    bool Init(HWND hwnd);
    //! 更新
    void Update();

    //! マウスを取得
    DirectX::Mouse* GetMouse();
    //! マウスの状態を取得
    DirectX::Mouse::State GetMouseState();

    s32 GetPosX();               //!< マウス X 座標を取得
    s32 GetPosY();               //!< マウス Y 座標を取得
    s32 GetScrollWheelValue();   //!< マウス中のスクロール輪の数値

    //! マウスモード Relative
    //! For 'mouse-look' behavior in games
    void SetRelativeMode();
    //! マウスモード Absolute
    //! マウスX、Y座標の絶対値
    void SetAbsoluteMode();

    bool IsRelativeMode();   //!< チェックマウスモード RelativeMode
    bool IsAbsoluteMode();   //!< チェックマウスモード AbsoluteMode

    bool IsPressLeftButton();      //!< マウス左ボタンを押したか
    bool IsHeldLeftButton();       //!< マウス左ボタンを押しているか
    bool IsReleasedLeftButton();   //!< マウス左ボタンはこのフレームでリリースしたかどうか

    bool IsHeldMiddleButton();   //!< マウスホイールを押しているか

    bool IsPressRightButton();      //!< マウス右ボタンを押したか
    bool IsHeldRightButton();       //!< マウス右ボタンを押しているか
    bool IsReleasedRightButton();   //!< マウス右ボタンはこのフレームでリリースしたかどうか

    void ResetScrollWheelValue();   //!< スクロールホイール数値をリセット
private:
    void SetMode(DirectX::Mouse::Mode mode);   //!< マウスモードのステート

    DirectX::Mouse::ButtonStateTracker _mouseTracker;   //!< マウスのトラッカー
    uni_ptr<DirectX::Mouse>            _pMouse;         //!< DirectXTKのマウス

    DirectX::Mouse::State _mouseState;       //!< マウスステート
    DirectX::Mouse::State _lastMouseState;   //!< 前フレームのマウスステート
};
bool IsPressLeftButton();   //!< マウス左ボタンを押した
}   // namespace input
//! マウス管理クラスを取得
input::MouseManager* MouseMgr();
