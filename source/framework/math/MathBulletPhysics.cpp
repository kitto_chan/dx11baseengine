﻿//---------------------------------------------------------------------------
//!	@file	math_bulletPhyics.h
//!	@brief	hlslpp <-> bullet physics ヘルパークラス
//---------------------------------------------------------------------------
#include "MathBulletPhysics.h"

namespace math {
//---------------------------------------------------------------------------
//! cast matrix -> btTransform
//---------------------------------------------------------------------------
btTransform CastToBtTrans(const matrix& mat)
{
    btTransform btTrans;
    btTrans.setFromOpenGLMatrix(math::CastTof32Matrix(mat));
    return btTrans;
}
//---------------------------------------------------------------------------
//! cast float3 -> btVector3
//---------------------------------------------------------------------------
btVector3 CastToBtVec3(const float3& f3)
{
    return btVector3(f3.f32[0], f3.f32[1], f3.f32[2]);
}
//---------------------------------------------------------------------------
//! cast btVector3 -> float3
//---------------------------------------------------------------------------
float3 CastToFloat3(const btVector3& btV3)
{
    return float3(btV3.getX(), btV3.getY(), btV3.getZ());
}
//---------------------------------------------------------------------------
//! cast btTrans -> matrix
//---------------------------------------------------------------------------
matrix CastToMatrix(const btTransform& btTrans)
{
    f32 m[16];
    btTrans.getOpenGLMatrix(m);

    return matrix(m[0], m[1], m[2], m[3],
                  m[4], m[5], m[6], m[7],
                  m[8], m[9], m[10], m[11],
                  m[12], m[13], m[14], m[15]);
}
}   // namespace math
