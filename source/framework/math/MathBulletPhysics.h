﻿//---------------------------------------------------------------------------
//!	@file	math_bulletPhyics.h
//!	@brief	hlslpp <-> bullet physics ヘルパークラス
//---------------------------------------------------------------------------
#pragma once
namespace math {
btTransform CastToBtTrans(const matrix& f4);   //!< cast matrix -> btTransform
btVector3   CastToBtVec3(const float3& f3);    //!< cast float3 -> btVector3

float3 CastToFloat3(const btVector3& btV3);        //!< cast btVector3 -> float3
matrix CastToMatrix(const btTransform& btTrans);   //!< cast btTransform -> matrix
}   // namespace math
