﻿//---------------------------------------------------------------------------
//!	@file	MathCommon
//!	@brief	共通の数学クラス
//---------------------------------------------------------------------------
#include "MathCommon.h"

namespace math {
f32 SinWave(f32 min, f32 max, f32 speed)
{
    constexpr f32 FPS = 60.f;

    f32 half  = (max - min) / 2.0f;
    f32 ori   = min + half;
    f32 t     = math::PI * 2.f * timer::TotalTime() / FPS;
    f32 value = std::sinf(t * speed) * half + ori;
    return value;
}
}   // namespace math
