﻿//---------------------------------------------------------------------------
//!	@file	MathCommon
//!	@brief	共通の数学クラス
//---------------------------------------------------------------------------
#pragma once

// 色
#define WHITE float4(1.0f, 1.0f, 1.0f, 1.0f)
#define RED float4(1.0f, 0.0f, 0.0f, 1.0f)
#define GREEN float4(0.0f, 1.0f, 0.0f, 1.0f)
#define BLUE float4(0.0f, 0.0f, 1.0f, 1.0f)

namespace math {
// 2数値をノーマルする
constexpr f32 normalize(f32 value, f32 min, f32 max)
{
    return (value - min) / (max - min);
}

//===================
// 円に関して
//===================
constexpr f32 PI = 3.14159265359f;   //!< 円周率

constexpr f32 D2R(f32 degree)
{
    return (degree) * (PI / 180.0f);
}
constexpr f32 R2D(f32 radian)
{
    return (radian) * (180.0f / PI);
}

f32 SinWave(f32 min, f32 max, f32 speed = 1.0f);
}   // namespace math
