﻿//---------------------------------------------------------------------------
//!	@file	math_DirectX.cpp
//!	@brief	cast hlslpp <-> DirectX
//---------------------------------------------------------------------------
#include "MathDirectX.h"
namespace math {
//---------------------------------------------------------------------------
//! cast hlslpp::float2 -> DirectX::XMFLOAT2
//---------------------------------------------------------------------------
DirectX::XMFLOAT2 CastToXMFLOAT2(const float2& f2)
{
    return DirectX::XMFLOAT2(f2.f32[0], f2.f32[1]);
}
//---------------------------------------------------------------------------
//! cast DirectX::XMFLOAT4 -> hlslpp::float4
//---------------------------------------------------------------------------
DirectX::XMFLOAT4 CastToXMFLOAT4(const float4& f4)
{
    return DirectX::XMFLOAT4(f4.x, f4.y, f4.z, f4.w);
}
//---------------------------------------------------------------------------
//! cast DirectX::XMFLOAT3 -> hlslpp::float3
//---------------------------------------------------------------------------
DirectX::XMFLOAT3 CastToXMFLOAT3(const float3& f3)
{
    return DirectX::XMFLOAT3(f3.x, f3.y, f3.z);
}
//---------------------------------------------------------------------------
//! cast hlslpp::float4 -> DirectX::XMFLOAT4
//---------------------------------------------------------------------------
float4 CastToFloat4(const DirectX::XMFLOAT4& f4)
{
    return float4(f4.x, f4.y, f4.z, f4.w);
}
//---------------------------------------------------------------------------
//! cast hlslpp::float3 -> DirectX::XMFLOAT3
//---------------------------------------------------------------------------
float3 CastToFloat3(const DirectX::XMFLOAT3& f3)
{
    return float3(f3.x, f3.y, f3.z);
}
//---------------------------------------------------------------------------
//! cast hlslpp::float4 -> DirectX::XMVECTOR
//---------------------------------------------------------------------------
DirectX::XMVECTOR CastToXMVECTOR(const float4& f4)
{
    return DirectX::XMVECTOR({ f4.f32[0], f4.f32[1], f4.f32[2], f4.f32[3] });
}
DirectX::XMVECTOR CastToXMVECTOR(const float3& f3)
{
    return DirectX::XMVECTOR({ f3.f32[0], f3.f32[1], f3.f32[2] });
}
}   // namespace math
