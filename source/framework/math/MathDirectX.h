﻿//---------------------------------------------------------------------------
//!	@file	math_DirectX.h
//!	@brief	cast hlslpp <-> DirectX
//---------------------------------------------------------------------------
#pragma once
namespace math {
DirectX::XMFLOAT2 CastToXMFLOAT2(const float2& f2);   //!< DirectX::XMFLOAT2 -> hlslpp::float2
DirectX::XMFLOAT3 CastToXMFLOAT3(const float3& f3);   //!< DirectX::XMFLOAT3 -> hlslpp::float3
DirectX::XMFLOAT4 CastToXMFLOAT4(const float4& f4);   //!< DirectX::XMFLOAT4 -> hlslpp::float4

DirectX::XMVECTOR CastToXMVECTOR(const float4& f4);   //!< hlslpp::float4 -> DirectX::XMVECTOR
DirectX::XMVECTOR CastToXMVECTOR(const float3& f3);   //!< hlslpp::float4 -> DirectX::XMVECTOR

float4 CastToFloat4(const DirectX::XMFLOAT4& f4);   //!< DirectX::XMFLOAT4 -> hlslpp::float4
float3 CastToFloat3(const DirectX::XMFLOAT3& f3);   //!< DirectX::XMFLOAT3 -> hlslpp::float3

}   // namespace math
