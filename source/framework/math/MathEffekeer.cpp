﻿//---------------------------------------------------------------------------
//!	@file	MathEffekeer
//!	@brief	Effekeer用のヘルパークラス
//---------------------------------------------------------------------------
#include "MathEffekeer.h"
namespace math {
Effekseer::Matrix44 CastToEffekseerMatrix44(const matrix& maxtrix)
{
    Effekseer::Matrix44 effekMatrix;
    effekMatrix.Values[0][0] = maxtrix.f32_0[0];
    effekMatrix.Values[0][1] = maxtrix.f32_0[1];
    effekMatrix.Values[0][2] = maxtrix.f32_0[2];
    effekMatrix.Values[0][3] = maxtrix.f32_0[3];

    effekMatrix.Values[1][0] = maxtrix.f32_1[0];
    effekMatrix.Values[1][1] = maxtrix.f32_1[1];
    effekMatrix.Values[1][2] = maxtrix.f32_1[2];
    effekMatrix.Values[1][3] = maxtrix.f32_1[3];

    effekMatrix.Values[2][0] = maxtrix.f32_2[0];
    effekMatrix.Values[2][1] = maxtrix.f32_2[1];
    effekMatrix.Values[2][2] = maxtrix.f32_2[2];
    effekMatrix.Values[2][3] = maxtrix.f32_2[3];

    effekMatrix.Values[3][0] = maxtrix.f32_3[0];
    effekMatrix.Values[3][1] = maxtrix.f32_3[1];
    effekMatrix.Values[3][2] = maxtrix.f32_3[2];
    effekMatrix.Values[3][3] = maxtrix.f32_3[3];
    return effekMatrix;
}
//---------------------------------------------------------------------------
//! cast ImVec2 -> hlslpp::float2
//---------------------------------------------------------------------------
}   // namespace math
