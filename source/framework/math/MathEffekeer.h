﻿//---------------------------------------------------------------------------
//!	@file	MathEffekeer
//!	@brief	Effekeer用のヘルパークラス
//---------------------------------------------------------------------------
#pragma once
#pragma warning(push)
#pragma warning(disable : 4100 26451 26812 26495)
#include "Effekseer/include/Effekseer/Effekseer.h"
#pragma warning(pop)
namespace math {
Effekseer::Matrix44 CastToEffekseerMatrix44(const matrix& maxtrix);   //!< maxtrix -> EffekseerMatrix44
}   // namespace math
