﻿//---------------------------------------------------------------------------
//!	@file	MathImGui
//!	@brief	ImGui用のヘルパークラス
//---------------------------------------------------------------------------
#pragma once
namespace math {
float2 CastToFloat2(const ImVec2& im2);   //!< ImVec2 -> hlslpp::float2
ImVec2 CastToImVec2(const float2& f2);    //!< hlslpp::float2-> ImVec2
}   // namespace math
