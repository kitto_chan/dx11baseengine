﻿//---------------------------------------------------------------------------
//!	@file	math_matrix.cpp
//!	@brief	行列ユーティリティー
//---------------------------------------------------------------------------
#include "MathMatrix.h"

namespace math {
//---------------------------------------------------------------------------
//! キャスト matrix -> float[16] 4x4 行列
//---------------------------------------------------------------------------
float* CastTof32Matrix(const matrix& mat)
{
    f32* result = new f32[16];
    result[0]   = mat.f32_0[0];
    result[1]   = mat.f32_0[1];
    result[2]   = mat.f32_0[2];
    result[3]   = mat.f32_0[3];
    result[4]   = mat.f32_1[0];
    result[5]   = mat.f32_1[1];
    result[6]   = mat.f32_1[2];
    result[7]   = mat.f32_1[3];
    result[8]   = mat.f32_2[0];
    result[9]   = mat.f32_2[1];
    result[10]  = mat.f32_2[2];
    result[11]  = mat.f32_2[3];
    result[12]  = mat.f32_3[0];
    result[13]  = mat.f32_3[1];
    result[14]  = mat.f32_3[2];
    result[15]  = mat.f32_3[3];

    return result;
}
matrix CastToMatrix(const f32* f16)
{
    matrix mat;

    mat.f32_0[0]= f16[0];
    mat.f32_0[1] =f16[1];
    mat.f32_0[2] =f16[2];
    mat.f32_0[3] =f16[3];
    mat.f32_1[0] =f16[4];
    mat.f32_1[1] =f16[5];
    mat.f32_1[2] =f16[6];
    mat.f32_1[3] =f16[7];
    mat.f32_2[0] =f16[8];
    mat.f32_2[1] =f16[9];
    mat.f32_2[2] =f16[10];
    mat.f32_2[3] =f16[11];
    mat.f32_3[0] =f16[12];
    mat.f32_3[1] =f16[13];
    mat.f32_3[2] =f16[14];
    mat.f32_3[3] =f16[15];
    return mat;
   /* return {
        float4{ f16[0], f16[1], f16[2], f16[3] },
        float4{ f16[4], f16[5], f16[6], f16[7] },
        float4{ f16[8], f16[9], f16[10], f16[11] },
        float4{ f16[12], f16[13], f16[14], f16[15] },
    };*/
}
//---------------------------------------------------------------------------
//! 単位行列
//---------------------------------------------------------------------------
matrix identity()
{
    return {
        float4{ 1.0f, 0.0f, 0.0f, 0.0f },
        float4{ 0.0f, 1.0f, 0.0f, 0.0f },
        float4{ 0.0f, 0.0f, 1.0f, 0.0f },
        float4{ 0.0f, 0.0f, 0.0f, 1.0f },
    };
}

//---------------------------------------------------------------------------
//! 平行移動
//---------------------------------------------------------------------------
matrix translate(const float3& v)
{
    return {
        { 1.0f, 0.0f, 0.0f, 0.0f },
        { 0.0f, 1.0f, 0.0f, 0.0f },
        { 0.0f, 0.0f, 1.0f, 0.0f },
        { v.xyz, 1.0f },
    };
}

//---------------------------------------------------------------------------
//! スケール
//---------------------------------------------------------------------------
matrix scale(const float3& scale)
{
    auto s = scale;
    return {
        float4{ s.x, 0.0f, 0.0f, 0.0f },
        float4{ 0.0f, s.y, 0.0f, 0.0f },
        float4{ 0.0f, 0.0f, s.z, 0.0f },
        float4{ 0.0f, 0.0f, 0.0f, 1.0f },
    };
}

//---------------------------------------------------------------------------
//! X軸中心の回転
//---------------------------------------------------------------------------
matrix rotateX(f32 radian)
{
    float1 s;
    float1 c;
    sincos(float1(radian), s, c);
    return {
        float4{ 1.0f, 0.0f, 0.0f, 0.0f },
        float4{ 0.0f, c, s, 0.0f },
        float4{ 0.0f, -s, c, 0.0f },
        float4{ 0.0f, 0.0f, 0.0f, 1.0f },
    };
}

//---------------------------------------------------------------------------
//! Y軸中心の回転
//---------------------------------------------------------------------------
matrix rotateY(f32 radian)
{
    float1 s;
    float1 c;
    sincos(float1(radian), s, c);
    return {
        float4{ c, 0.0f, -s, 0.0f },
        float4{ 0.0f, 1.0f, 0.0f, 0.0f },
        float4{ s, 0.0f, c, 0.0f },
        float4{ 0.0f, 0.0f, 0.0f, 1.0f },
    };
}

//---------------------------------------------------------------------------
//! Z軸中心の回転
//---------------------------------------------------------------------------
matrix rotateZ(f32 radian)
{
    float1 s;
    float1 c;
    sincos(float1(radian), s, c);
    return {
        float4{ c, s, 0.0f, 0.0f },
        float4{ -s, c, 0.0f, 0.0f },
        float4{ 0.0f, 0.0f, 1.0f, 0.0f },
        float4{ 0.0f, 0.0f, 0.0f, 1.0f },
    };
}

//---------------------------------------------------------------------------
//! 任意の軸中心の回転
//---------------------------------------------------------------------------
matrix rotateAxis(const float3& v, f32 radian)
{
    float1 s;
    float1 c;
    sincos(float1(radian), s, c);

    float3 axis = normalize(v);
    float1 x    = axis.x;
    float1 y    = axis.y;
    float1 z    = axis.z;

    matrix m;
    m._11 = x * x * (1.0f - c) + c;
    m._12 = x * y * (1.0f - c) + z * s;
    m._13 = x * z * (1.0f - c) - y * s;
    m._14 = 0.0f;

    m._21 = x * y * (1.0f - c) - z * s;
    m._22 = y * y * (1.0f - c) + c;
    m._23 = y * z * (1.0f - c) + x * s;
    m._24 = 0.0f;

    m._31 = x * z * (1.0f - c) + y * s;
    m._32 = y * z * (1.0f - c) - x * s;
    m._33 = z * z * (1.0f - c) + c;
    m._34 = 0.0f;

    m._41 = 0.0f;
    m._42 = 0.0f;
    m._43 = 0.0f;
    m._44 = 1.0f;

    return m;
}

//---------------------------------------------------------------------------
//! ビュー行列　(右手座標系)
//---------------------------------------------------------------------------
matrix lookAtRH(const float3& position, const float3& lookAt, const float3& worldUp)
{
    float3 axisZ = normalize(position - lookAt);
    float3 axisX = normalize(cross(worldUp, axisZ));
    float3 axisY = cross(axisZ, axisX);

    f32 x = -dot(axisX, position);
    f32 y = -dot(axisY, position);
    f32 z = -dot(axisZ, position);

    return {
        float4{ axisX.x, axisY.x, axisZ.x, 0.0f },
        float4{ axisX.y, axisY.y, axisZ.y, 0.0f },
        float4{ axisX.z, axisY.z, axisZ.z, 0.0f },
        float4{ x, y, z, 1.0f },
    };
}

//---------------------------------------------------------------------------
//! 投影行列　(右手座標系)
//---------------------------------------------------------------------------
matrix perspectiveFovRH(f32 fovy, f32 aspectRatio, f32 nearZ, f32 farZ)
{
    f32 h  = 1.0f / tanf(fovy * 0.5f);
    f32 w  = h / aspectRatio;
    f32 nz = nearZ;
    f32 fz = farZ;

    return {
        float4{ w, 0.0f, 0.0f, 0.0f },
        float4{ 0.0f, h, 0.0f, 0.0f },
        float4{ 0.0f, 0.0f, fz / (nz - fz), -1.0f },
        float4{ 0.0f, 0.0f, nz * fz / (nz - fz), 0.0f }
    };
}

//---------------------------------------------------------------------------
// 行列から回転成分を取得
//---------------------------------------------------------------------------
quaternion makeRotation(const matrix& m)
{
    float4 q;
    f32    tr = m._11 + m._22 + m._33;

    if(tr > 0.0f) {
        f32 t = tr + 1.0f;
        f32 s = 1 / sqrtf(t) * 0.5f;

        q.w = s * t;
        q.z = (m._12 - m._21) * s;
        q.y = (m._31 - m._13) * s;
        q.x = (m._23 - m._32) * s;
    }
    else if((static_cast<f32>(m._11) > static_cast<f32>(m._22)) && (static_cast<f32>(m._11) > static_cast<f32>(m._33))) {
        f32 t = 1.0f + m._11 - m._22 - m._33;
        f32 s = 1.0f / sqrtf(t) * 0.5f;
        q.x   = s * t;
        q.y   = (m._12 + m._21) * s;
        q.z   = (m._13 + m._31) * s;
        q.w   = (m._23 - m._32) * s;
    }
    else if(static_cast<f32>(m._22) > static_cast<f32>(m._33)) {
        f32 t = 1.0f + m._22 - m._11 - m._33;
        f32 s = 1.0f / sqrtf(t) * 0.5f;
        q.w   = (m._31 - m._13) * s;
        q.x   = (m._12 + m._21) * s;
        q.y   = s * t;
        q.z   = (m._23 + m._32) * s;
    }
    else {
        f32 t = 1.0f + m._33 - m._11 - m._22;
        f32 s = 1.0f / sqrtf(t) * 0.5f;
        q.w   = (m._12 - m._21) * s;
        q.x   = (m._31 + m._13) * s;
        q.y   = (m._32 + m._23) * s;
        q.z   = s * t;
    }
    return q;
}

//---------------------------------------------------------------------------
//! 行列から平行移動成分を取得
//---------------------------------------------------------------------------
float3 makeTranslation(const matrix& m)
{
    return m._41_42_43;
}
//---------------------------------------------------------------------------
//! 回転 float3 -> matrix (z-y-x軸)
//---------------------------------------------------------------------------
matrix MatrixRotationRollPitchYaw(const float3& rotation)
{
    matrix roll  = rotateX(rotation.x);
    matrix pitch = rotateY(rotation.y);
    matrix yaw   = rotateZ(rotation.z);
    return mul(mul(roll, pitch), yaw);   // row * pitch * yaw; // zxy;

    //pitch * yaw * row;
    //return mul(mul(pitch, yaw), roll);
}
//---------------------------------------------------------------------------
//! 回転 quaternion -> float3
//---------------------------------------------------------------------------
float3 QuaternionToEulerAngles(const quaternion& q)
{
    float3 angles;
    // roll (x-axis rotation)
    f32 sinr_cosp = 2 * (q.w * q.x + q.y * q.z);
    f32 cosr_cosp = 1 - 2 * (q.x * q.x + q.y * q.y);
    angles.x      = std::atan2(sinr_cosp, cosr_cosp);

    // pitch (y-axis rotation)
    f32 sinp = 2 * (q.w * q.y - q.z * q.x);
    if(std::abs(sinp) >= 1)
        angles.y = std::copysign(PI / 2, sinp);   // use 90 degrees if out of range
    else
        angles.y = std::asin(sinp);

    // yaw (z-axis rotation)
    f32 siny_cosp = 2 * (q.w * q.z + q.x * q.y);
    f32 cosy_cosp = 1 - 2 * (q.y * q.y + q.z * q.z);
    angles.z      = std::atan2(siny_cosp, cosy_cosp);

    return angles;
}
}   // namespace math
