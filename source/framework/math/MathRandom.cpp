﻿//---------------------------------------------------------------------------
//!	@file	math_random.h
//!	@brief	ランダムのヘルパークラス
//---------------------------------------------------------------------------
#include "MathRandom.h"

namespace math {
//---------------------------------------------------------------------------------
//! ０～ i_max までのランダムな値の返す関数（ int 型）
//---------------------------------------------------------------------------------
s32 GetRandomI(s32 i_max)
{
    return rand() % (i_max + 1);
}
//---------------------------------------------------------------------------------
//!	i_from ～ i_to までのランダムな値を返す関数（ int 型）
//---------------------------------------------------------------------------------
s32 GetRandomI(s32 i_from, s32 i_to)
{
    int value  = i_to - i_from;
    int random = GetRandomI(value);
    return i_from + random;
}
//---------------------------------------------------------------------------------
//!	0.0 ～ 1.0 までのランダムな値を返す関数（ float 型）
//---------------------------------------------------------------------------------
f32 GetRandomF()
{
    f32 random = (f32)rand();
    return random / RAND_MAX;
}
//---------------------------------------------------------------------------------
//!	0.0 ～ f_max までのランダムな値を返す（ float 型）
//---------------------------------------------------------------------------------
f32 GetRandomF(f32 f_max)
{
    f32 random = GetRandomF();
    return random * f_max;
}
//---------------------------------------------------------------------------------
//!	f_from ～ f_to までのランダムな値を返す（ float 型）
//---------------------------------------------------------------------------------
f32 GetRandomF(f32 f_from, f32 f_to)
{
    f32 value  = f_to - f_from;
    f32 random = GetRandomF(value);
    return f_from + random;
}

}   // namespace math
