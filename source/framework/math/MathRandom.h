﻿//---------------------------------------------------------------------------
//!	@file	math_random.h
//!	@brief	ランダムのヘルパークラス
//---------------------------------------------------------------------------
#pragma once

namespace math {
//! 0 ~ i_max までのランダムな値の返す関数（ int 型）
s32 GetRandomI(s32 i_max);
//! i_from ～ i_to までのランダムな値を返す関数（ int 型）
s32 GetRandomI(s32 i_from, s32 i_to);
//!	0.0 ~ 1.0 までのランダムな値を返す関数（ float 型）
f32 GetRandomF();
//!	0.0 ~ f_max までのランダムな値を返す（ float 型）
f32 GetRandomF(f32 f_max);
//!	f_from ~ f_to までのランダムな値を返す（ float 型）
f32 GetRandomF(f32 f_from, f32 f_to);
}   // namespace math
