﻿//---------------------------------------------------------------------------
//!	@file	physics_engine.cpp
//!	@brief	物理シミュレーションエンジン
//---------------------------------------------------------------------------
#include "physics_engine.h"

#pragma warning(push)
#pragma warning(disable : 26495)
#include <BulletCollision/CollisionDispatch/btCollisionDispatcherMt.h>
#include <BulletDynamics/Dynamics/btDiscreteDynamicsWorldMt.h>
#include <BulletDynamics/ConstraintSolver/btSequentialImpulseConstraintSolverMt.h>
#pragma warning(pop)

namespace physics {

//===========================================================================
//! 物理シミュレーションエンジン (実装部)
//===========================================================================
class PhysicsEngineImpl final : public physics::PhysicsEngine
{
public:
    class DebugDrawer;            //! デバッグ描画
    class TaskSchedulerManager;   //!< Bulletタスクスケジューラー管理

    //! 初期化
    virtual bool initialize() override;

    //! 更新
    //! @param  [in]    deltaTime   進行時間(単位:秒)
    virtual void update(f32 deltaTime) override;

    //! 解放
    virtual void finalize() override;

    //! デバッグ描画
    virtual void renderDebug() override;

    //----------------------------------------------------------
    //! @name   剛体
    //----------------------------------------------------------
    //@{

    //! Bullet剛体を登録
    virtual void registerRigidBody(btRigidBody* rigidBody, entity::Layer layer = entity::Layer::Default) override;

    //! Bullet剛体を登録解除
    virtual void unregisterRigidBody(btRigidBody* rigidBody) override;

    //! Bulletワールドを取得
    virtual btDynamicsWorld* dynamicsWorld() const override;

    //! Bulletデバッグ描画を取得
    virtual btIDebugDraw* debugDrawer() const override;

    //@}

    //! 唯一のインスタンスを取得
    static physics::PhysicsEngine* instance()
    {
        static physics::PhysicsEngineImpl physicsEngine;
        return &physicsEngine;
    }

private:
    // コピー禁止/move禁止
    PhysicsEngineImpl(const PhysicsEngineImpl&) = delete;
    PhysicsEngineImpl(PhysicsEngineImpl&&)      = delete;
    PhysicsEngineImpl& operator=(const PhysicsEngineImpl&) = delete;
    PhysicsEngineImpl& operator=(PhysicsEngineImpl&&) = delete;

private:
    // new/delete 禁止
    PhysicsEngineImpl() = default;

    //! デストラクタ
    virtual ~PhysicsEngineImpl();

private:
    static constexpr bool isMultiThreaded_ = true;   //!< マルチスレッド動作 true:マルチスレッド false:シングルスレッド

    std::unique_ptr<btDefaultCollisionConfiguration> collisionConfiguration_;   //!< コリジョンのコンフィグ設定

    std::unique_ptr<btDbvtBroadphase>                    broadphase_;      //!< ブロードフェーズ
    std::unique_ptr<btCollisionDispatcher>               dispatcher_;      //!< コリジョンディスパッチャー(マルチスレッド)
    std::unique_ptr<btConstraintSolverPoolMt>            solverPool_;      //!< マルチスレッド用ソルバープール
    std::unique_ptr<btSequentialImpulseConstraintSolver> solver_;          //!< 拘束ソルバー(マルチスレッド)
    std::unique_ptr<btDiscreteDynamicsWorld>             dynamicsWorld_;   //!< ワールド
    std::unique_ptr<PhysicsEngineImpl::DebugDrawer>      debugDrawer_;     //!< デバッグ描画

    //! Bulletタスクスケジューラー管理
    std::unique_ptr<PhysicsEngineImpl::TaskSchedulerManager> taskSchedulerManager_;
};

//===========================================================================
//! Bulletタスクスケジューラー管理
//! 複数のタスクスケジューラーを切り替えできるマネージャー
//===========================================================================
class PhysicsEngineImpl::TaskSchedulerManager
{
    std::vector<btITaskScheduler*>                 taskSchedulers_;            //!< 参照するタスクスケジューラー
    std::vector<std::unique_ptr<btITaskScheduler>> allocatedTaskSchedulers_;   //!< 所有権を持つタスクスケジューラー(getではなく作成したもの)

public:
    //! コンストラクタ
    TaskSchedulerManager()
    {
        // デフォルトの直列スケジューラー
        // (主にベンチマーク用途で普段は使用しない)
        addTaskScheduler(btGetSequentialTaskScheduler());

        //------------------------------------------------------
        // 並列スケジューラー
        //------------------------------------------------------
        if(btITaskScheduler* taskScheduler = btCreateDefaultTaskScheduler()) {
            allocatedTaskSchedulers_.push_back(std::unique_ptr<btITaskScheduler>(taskScheduler));
            addTaskScheduler(taskScheduler);
        }

        //------------------------------------------------------
        // 外部のタスクスケジューラー(オプション)
        //------------------------------------------------------
        addTaskScheduler(btGetOpenMPTaskScheduler());
        addTaskScheduler(btGetTBBTaskScheduler());
        addTaskScheduler(btGetPPLTaskScheduler());

        // 利用可能であれば並列スケジューラーを優先して利用。
        // Bulletにタスクスケジューラーを設定
        if(taskSchedulerCount() > 1) {
            // 並列スケジューラー
            btSetTaskScheduler(taskSchedulers_[1]);
        }
        else {
            // 直列スケジューラー
            btSetTaskScheduler(taskSchedulers_[0]);
        }
    }

    //! タスクスケジューラーの登録数を取得
    size_t taskSchedulerCount() const { return taskSchedulers_.size(); }

    //! タスクスケジューラーを取得
    btITaskScheduler* taskScheduler(s32 index) const { return taskSchedulers_[index]; }

private:
    // タスクスケジューラーを追加
    void addTaskScheduler(btITaskScheduler* taskScheduler)
    {
        if(!taskScheduler)
            return;

        // 最大論理コア数でスレッドを初期化
        taskScheduler->setNumThreads(taskScheduler->getMaxNumThreads());

        // 登録
        taskSchedulers_.push_back(taskScheduler);
    }
};

//===========================================================================
//! Bulletデバッグ描画
//===========================================================================
class PhysicsEngineImpl::DebugDrawer final : public btIDebugDraw
{
public:
    //! コンストラクタ
    DebugDrawer() = default;

    //! デストラクタ
    virtual ~DebugDrawer() = default;

    //! 線分の描画
    virtual void drawLine(const btVector3& from, const btVector3& to, const btVector3& color) override
    {
        float3 p0(from.x(), from.y(), from.z());
        float3 p1(to.x(), to.y(), to.z());

        Color c;
        c.r_ = static_cast<u8>(color.x() * 255.5f);
        c.g_ = static_cast<u8>(color.y() * 255.5f);
        c.b_ = static_cast<u8>(color.z() * 255.5f);
        c.a_ = static_cast<u8>(255);

        debug::drawLineF(p0, p1, c, debug::DRAW_DEPTH_USE | debug::DRAW_DEPTH_WRITE);
    }

    //! 接触点の描画
    virtual void drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, [[maybe_unused]] btScalar distance, [[maybe_unused]] int lifeTime, [[maybe_unused]] const btVector3& color) override
    {
        float3 p(PointOnB.x(), PointOnB.y(), PointOnB.z());
        float3 n(normalOnB.x(), normalOnB.y(), normalOnB.z());

        Color c(255, 255, 255, 255);
        c.r_ = static_cast<u8>(color.x() * 255.5f);
        c.g_ = static_cast<u8>(color.y() * 255.5f);
        c.b_ = static_cast<u8>(color.z() * 255.5f);
        c.a_ = static_cast<u8>(255);

        debug::drawNormal(p, n, c);
    }

    //! 警告出力
    virtual void reportErrorWarning([[maybe_unused]] const char* warningString) override
    {
    }

    //! 3D空間へのテキスト描画
    virtual void draw3dText([[maybe_unused]] const btVector3& location, [[maybe_unused]] const char* textString) override
    {
    }

    //! デバッグモードの設定
    //! @param  [in]    debugMode   デバッグモード(btIDebugDraw::DebugDrawModesフラグの組み合わせ)
    virtual void setDebugMode(int debugMode) override
    {
        debugMode_ = debugMode;
    }

    //! デバッグモードの取得
    virtual int getDebugMode() const override
    {
        return debugMode_;
    }

private:
    u32 debugMode_ = 0;   //!< 現在のデバッグモード
};

//===========================================================================
//  physics::PhysicsEngineImpl
//===========================================================================

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
PhysicsEngineImpl::~PhysicsEngineImpl()
{
    finalize();
}

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool PhysicsEngineImpl::initialize()
{
    //----------------------------------------------------------
    // タスクスケジューラー管理を作成
    //----------------------------------------------------------
    // bulletにタスクスケジューラーを登録
    taskSchedulerManager_ = std::make_unique<PhysicsEngineImpl::TaskSchedulerManager>();

    //----------------------------------------------------------
    // bulletオブジェクトの生成
    //----------------------------------------------------------
    if(isMultiThreaded_) {
        // コンフィグ設定
        btDefaultCollisionConstructionInfo info{};
        info.m_defaultMaxPersistentManifoldPoolSize = 1000;
        info.m_defaultMaxCollisionAlgorithmPoolSize = 1000;

        collisionConfiguration_ = std::make_unique<btDefaultCollisionConfiguration>(info);
        //collisionConfiguration_->setConvexConvexMultipointIterations();
        collisionConfiguration_->setPlaneConvexMultipointIterations();
        broadphase_ = std::make_unique<btDbvtBroadphase>();

        // マルチスレッド版ディスパッチャー
        constexpr s32 grainSize = 20;   // 1スレッドあたりの一括処理数。多いほどオーバーヘッドが少ないが負荷が分散しない。デフォルト値は40。
        dispatcher_             = std::make_unique<btCollisionDispatcherMt>(collisionConfiguration_.get(), grainSize);

        // マルチスレッド版拘束ソルバー
        constexpr u32 maxThreadCount = BT_MAX_THREAD_COUNT;   // Bulletが利用する最大スレッド数

        solverPool_ = std::make_unique<btConstraintSolverPoolMt>(maxThreadCount);
        solver_     = std::make_unique<btSequentialImpulseConstraintSolverMt>();

        // マルチスレッド版ワールド
        dynamicsWorld_ = std::make_unique<btDiscreteDynamicsWorldMt>(dispatcher_.get(), broadphase_.get(), solverPool_.get(), solver_.get(), collisionConfiguration_.get());

        assert(btGetTaskScheduler() != nullptr);
    }
    else {
        // コンフィグ設定
        collisionConfiguration_ = std::make_unique<btDefaultCollisionConfiguration>();
        collisionConfiguration_->setConvexConvexMultipointIterations();

        broadphase_ = std::make_unique<btDbvtBroadphase>();
        dispatcher_ = std::make_unique<btCollisionDispatcher>(collisionConfiguration_.get());
        solver_     = std::make_unique<btSequentialImpulseConstraintSolver>();

        // ワールドの生成
        dynamicsWorld_ = std::make_unique<btDiscreteDynamicsWorld>(dispatcher_.get(), broadphase_.get(), solver_.get(), collisionConfiguration_.get());
    }
    // 重力の設定
    dynamicsWorld_->setGravity(btVector3(0.0f, -9.81f, 0.0f));             // 重力加速度G 9.81(m/s^2)
    dynamicsWorld_->getDispatchInfo().m_allowedCcdPenetration = 0.0001f;   // CCDのめり込み許容

    //----------------------------------------------------------
    // デバッグ描画ON
    //----------------------------------------------------------
    {
        debugDrawer_ = std::make_unique<PhysicsEngineImpl::DebugDrawer>();

        debugDrawer_->setDebugMode(btIDebugDraw::DBG_DrawWireframe | btIDebugDraw::DBG_DrawContactPoints | btIDebugDraw::DBG_DrawNormals /*| btIDebugDraw::DBG_DrawAabb*/);

        // デバッグ描画を登録
        dynamicsWorld_->setDebugDrawer(debugDrawer_.get());
    }

    return true;
}

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void PhysicsEngineImpl::update(f32 deltaTime)
{
    if(dynamicsWorld_) {
        constexpr s32 subSteps      = 8;               // サブステップ数
        constexpr f32 fixedTimeStep = 1.0f / 240.0f;   // 固定タイムステップ
        dynamicsWorld_->stepSimulation(deltaTime, subSteps, fixedTimeStep);
    }
}

//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void PhysicsEngineImpl::finalize()
{
    taskSchedulerManager_.reset();   // タスクスケジューラー管理
}

//---------------------------------------------------------------------------
//! デバッグ描画
//---------------------------------------------------------------------------
void PhysicsEngineImpl::renderDebug()
{
    dynamicsWorld_->debugDrawWorld();
}

//---------------------------------------------------------------------------
//! Bulletワールドを取得
//---------------------------------------------------------------------------
btDynamicsWorld* PhysicsEngineImpl::dynamicsWorld() const
{
    return dynamicsWorld_.get();
}

//---------------------------------------------------------------------------
//! Bullet剛体を登録
//---------------------------------------------------------------------------
void PhysicsEngineImpl::registerRigidBody(btRigidBody* rigidBody, entity::Layer layer)
{
    dynamicsWorld_->addRigidBody(rigidBody, layer, GetCollisionGroup(layer));
}

//---------------------------------------------------------------------------
//! Bullet剛体を登録解除
//---------------------------------------------------------------------------
void PhysicsEngineImpl::unregisterRigidBody(btRigidBody* rigidBody)
{
    dynamicsWorld_->removeRigidBody(rigidBody);
}

//---------------------------------------------------------------------------
//! Bulletデバッグ描画を取得
//---------------------------------------------------------------------------
btIDebugDraw* PhysicsEngineImpl::debugDrawer() const
{
    return debugDrawer_.get();
}

//===========================================================================
//  physics::PhysicsEngine
//===========================================================================

//---------------------------------------------------------------------------
//! 唯一のインスタンスを取得
//---------------------------------------------------------------------------
physics::PhysicsEngine* PhysicsEngine::instance()
{
    return physics::PhysicsEngineImpl::instance();
}

}   // namespace physics
