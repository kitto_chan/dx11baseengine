﻿//---------------------------------------------------------------------------
//!	@file	physics_engine.h
//!	@brief	物理シミュレーションエンジン
//---------------------------------------------------------------------------
#pragma once

//--------------------------------------------------------------
// Bullet Physics SDK
//--------------------------------------------------------------
#pragma warning(push)
#pragma warning(disable : 4127 26495)
#include <btBulletDynamicsCommon.h>
#pragma warning(pop)

namespace physics {

//===========================================================================
//! 物理シミュレーションエンジン
//===========================================================================
class PhysicsEngine
{
public:
    //! 初期化
    virtual bool initialize() = 0;

    //! 更新
    //! @param  [in]    deltaTime   進行時間(単位:秒)
    virtual void update(f32 deltaTime) = 0;

    //! 解放
    virtual void finalize() = 0;

    //! デバッグ描画
    virtual void renderDebug() = 0;

    //----------------------------------------------------------
    //! @name   剛体
    //----------------------------------------------------------
    //@{

    //! Bullet剛体を登録
    virtual void registerRigidBody(btRigidBody* rigidBody, entity::Layer layer = entity::Layer::Default) = 0;

    //! Bullet剛体を登録解除
    virtual void unregisterRigidBody(btRigidBody* rigidBody) = 0;

    //! Bulletワールドを取得
    virtual btDynamicsWorld* dynamicsWorld() const = 0;

    //! Bulletデバッグ描画を取得
    virtual btIDebugDraw* debugDrawer() const = 0;

    //@}

    //! 唯一のインスタンスを取得
    static physics::PhysicsEngine* instance();

private:
    // コピー禁止/move禁止
    PhysicsEngine(const PhysicsEngine&) = delete;
    PhysicsEngine(PhysicsEngine&&)      = delete;
    PhysicsEngine& operator=(const PhysicsEngine&) = delete;
    PhysicsEngine& operator=(PhysicsEngine&&) = delete;

protected:
    // new/delete禁止
    PhysicsEngine()          = default;
    virtual ~PhysicsEngine() = default;
};

}   // namespace physics
