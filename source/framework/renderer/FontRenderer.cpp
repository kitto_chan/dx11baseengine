﻿//---------------------------------------------------------------------------
//!	@file	FontRenderer.h
//!	@brief	DirectXTKの文字描画
//! @note   TODO: 自分の文字描画シェーダを作る予定がある
//---------------------------------------------------------------------------
#include "FontRenderer.h"
#include <sstream>
#include <iostream>
namespace renderer {
namespace {
uni_ptr<DirectX::SpriteBatch> _spriteBatch;   //!< バッチ
}   // namespace
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool FontRenderer::Init()
{
    if(!_gInit) {
        _spriteBatch = std::make_unique<DirectX::SpriteBatch>(dx11::context());

        _gInit = true;
    }
    //----- GLFont
    _defaultSpriteFont = std::make_unique<DirectX::SpriteFont>(dx11::d3dDevice(), L"font/GLFont34.spritefont");
    _gCurrentFont      = _defaultSpriteFont;

    // 処理
    _inited = true;
    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void FontRenderer::Update()
{
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void FontRenderer::Render()
{
    if(!_inited) return;

    _spriteBatch->Begin();

    for(auto& fontCache : _gFontCacheList) {
        SpirteFontDrawString(fontCache);
    }

    _spriteBatch->End();

    _gFontCacheList.clear();
}
void FontRenderer::RenderImmediate()
{
    _spriteBatch->Begin();

    for(auto& fontCache : _gFontCacheList) {
        SpirteFontDrawString(fontCache);
    }

    _spriteBatch->End();
    _gFontCacheList.clear();
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void FontRenderer::Finalize()
{
}
//---------------------------------------------------------------------------
//! フォントを作成
//---------------------------------------------------------------------------
uni_ptr<DirectX::SpriteFont> FontRenderer::CreateSpriteFont(std::wstring_view path)
{
    return std::make_unique<DirectX::SpriteFont>(dx11::d3dDevice(), path.data());
}
//---------------------------------------------------------------------------
//!< フォント描画開始
//---------------------------------------------------------------------------
void FontRenderer::Begin()
{
    _gCacheSpriteFont = _gCurrentFont;

    _isBeginning = true;
}
//---------------------------------------------------------------------------
//!< 文字描画
//---------------------------------------------------------------------------
void FontRenderer::DrawString(const float2& screenPos, std::wstring_view msg, const float4& color, f32 size)
{
    ASSERT_MESSAGE(_isBeginning, "FontRender::Begin関数読んでない");

    FontCache fontCache = {
        msg.data(),
        screenPos,
        color,
        size,
        _gCurrentFont
    };

    _gFontCacheList.emplace_back(fontCache);
}
//---------------------------------------------------------------------------
//!< フォント変更
//---------------------------------------------------------------------------
void FontRenderer::ChangeFont(raw_ptr<DirectX::SpriteFont> spriteFont)
{
    _gCurrentFont = spriteFont;
}
//---------------------------------------------------------------------------
//!< フォント描画終わる、元の設定を戻る
//---------------------------------------------------------------------------
void FontRenderer::End()
{
    _gCurrentFont = _gCacheSpriteFont;

    _isBeginning = true;
}
//---------------------------------------------------------------------------
//!< SpirteFontで文字描画
//---------------------------------------------------------------------------
void FontRenderer::SpirteFontDrawString(const FontCache& fontCache)
{
    // 描画座標
    DirectX::XMVECTOR renderPos = { fontCache._screenPos.f32[0], fontCache._screenPos.f32[1] };

    // 中心点を計算
    DirectX::XMVECTOR centerPos = fontCache._spriteFont->MeasureString(fontCache._msg.c_str());
    centerPos.m128_f32[0] /= 2.0f;
    centerPos.m128_f32[1] /= 2.0f;

    fontCache._spriteFont->DrawString(_spriteBatch.get(),
                                      fontCache._msg.c_str(),
                                      renderPos,
                                      math::CastToXMVECTOR(fontCache._color),
                                      0.0f,
                                      centerPos,
                                      fontCache._size);
}
renderer::FontRenderer* FontRendererIns()
{
    return FontRenderer::Instance();
}

void BeginDrawString()
{
    FontRendererIns()->Begin();
};
void DrawString(const float2& screenPos, std::wstring_view msg, const float4& color, f32 size)
{
    FontRendererIns()->DrawString(screenPos, msg, color, size);
};
void EndDrawString()
{
    FontRendererIns()->End();
};
void ChangeFont(raw_ptr<DirectX::SpriteFont> spriteFont)
{
    FontRendererIns()->ChangeFont(spriteFont);
};
//!< フォントを作成
uni_ptr<DirectX::SpriteFont> CreateSpriteFont(std::wstring_view path)
{
    return std::move(FontRendererIns()->CreateSpriteFont(path));
}
}   // namespace render
