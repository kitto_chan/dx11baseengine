﻿//---------------------------------------------------------------------------
//!	@file	FontRenderer.h
//!	@brief	DirectXTKの文字描画
//! @note   TODO: 自分の文字描画シェーダを作る予定がある
//---------------------------------------------------------------------------
#pragma once
#include "DirectXTK/Inc/SpriteFont.h"
#include <map>
namespace renderer {
//!< フォント描画キャッシュ
struct FontCache
{
    std::wstring                 _msg;          //!< 描画文字
    float2                       _screenPos;    //!< 描画座標(スクリーン座標)
    float4                       _color;        //!< 文字の色
    f32                          _size;         //!< サイズ
    raw_ptr<DirectX::SpriteFont> _spriteFont;   //!< 描画フォント
};
//===========================================================================
//! フォントレンダリング管理
//===========================================================================
class FontRenderer : public Singleton<FontRenderer>
{
public:
    FontRenderer()  = default;
    ~FontRenderer() = default;

    // ムーブ禁止/コピー禁止
    FontRenderer(const FontRenderer&) = delete;
    FontRenderer(FontRenderer&&)      = delete;
    FontRenderer& operator=(const FontRenderer&) = delete;
    FontRenderer& operator=(FontRenderer&&) = delete;

public:
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    bool Init();       //!< 初期化
    void Update();     //!< 更新
    void Render();     //!< 描画
    void RenderImmediate();
    void Finalize();   //!< 解放

    //!< フォントを作成
    uni_ptr<DirectX::SpriteFont> CreateSpriteFont(std::wstring_view path);

    //!< フォント描画開始
    void Begin();

    //!< 文字描画
    void DrawString(const float2&     screenPos,
                    std::wstring_view msg,
                    const float4&     color = float4(1.0f, 1.0f, 1.0f, 1.0f),
                    f32               size  = 1.0f);

    //!< フォント変更
    void ChangeFont(raw_ptr<DirectX::SpriteFont> spriteFont);
    //!< フォント描画終わる、元の設定を戻る
    void End();

private:
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
    //!< SpirteFontで文字描画
    void SpirteFontDrawString(const FontCache& fontCache);
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    bool                         _gInit = false;      //!< gobalの初期化
    raw_ptr<DirectX::SpriteFont> _gCacheSpriteFont;   //!< キャッシュしたフォント
    raw_ptr<DirectX::SpriteFont> _gCurrentFont;       //!< 現在のフォント

    std::vector<FontCache>       _gFontCacheList;      //!< フォントキャッシュ、後で描画用
    uni_ptr<DirectX::SpriteFont> _defaultSpriteFont;   //!< フォント

    bool _isBeginning = false;   //!< 文字描画開始
    bool _inited      = false;   //!< 初期化したか
};
renderer::FontRenderer* FontRendererIns();

//! 文字描画開始
void BeginDrawString();
//! 文字描画
void DrawString(const float2& screenPos, std::wstring_view msg, const float4& color, f32 size);
//! 文字描画終了
void EndDrawString();
//! フォント交換
void ChangeFont(raw_ptr<DirectX::SpriteFont> spriteFont);
//!< フォント生成
uni_ptr<DirectX::SpriteFont> CreateSpriteFont(std::wstring_view path);
}   // namespace render
