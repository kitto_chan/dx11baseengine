﻿#include "GeometryRenderer.h"
//---------------------------------------------------------------------------
//!	@file	Geomertry.cpp
//! @brief	ジオメトリーを描画
//---------------------------------------------------------------------------
namespace renderer {
namespace {
// シェーダー
std::shared_ptr<dx11::Shader> shaderVs_3D_;     //!< VS 3D頂点シェーダー
std::shared_ptr<dx11::Shader> shaderPs_Flat_;   //!< PS テクスチャなしピクセルシェーダー

// ジオメトリ形状
std::shared_ptr<dx11::InputLayout> inputLayout_;    //!< 入力レイアウト
std::shared_ptr<dx11::Buffer>      vertexBuffer_;   //!< 頂点バッファ
std::shared_ptr<dx11::Buffer>      indexBuffer_;    //!< インデックスバッファ
cb::MeshColorCB _colorCB{};
}   // namespace
bool Init()
{
    shaderVs_3D_   = dx11::createShader("dx11/vs_3d.fx", "main", "vs_5_0");     // 頂点シェーダー
    shaderPs_Flat_ = dx11::createShader("dx11/ps_flat.fx", "main", "ps_5_0");   // ピクセルシェーダー
    if(!shaderVs_3D_ || !shaderPs_Flat_)
        return false;

    //-------------------------------------------------------------
    // 頂点データ作成
    // 入力レイアウトと同じメンバ変数レイアウトの構造体の配列
    //-------------------------------------------------------------
    constexpr vertex::VertexPosUv vertices[]{
        { DirectX::XMFLOAT3(-1.0f, +1.0f, 0.0f), DirectX::XMFLOAT2(0.0f, 0.0f) },
        { DirectX::XMFLOAT3(+1.0f, +1.0f, 0.0f), DirectX::XMFLOAT2(1.0f, 0.0f) },
        { DirectX::XMFLOAT3(+1.0f, -1.0f, 0.0f), DirectX::XMFLOAT2(1.0f, 1.0f) },
        { DirectX::XMFLOAT3(-1.0f, -1.0f, 0.0f), DirectX::XMFLOAT2(0.0f, 1.0f) },
    };
    constexpr u32 indices[]{
        0, 1, 2,
        2, 3, 0
    };

    // 頂点バッファの作成
    vertexBuffer_ = dx11::createBuffer(vertices, D3D11_BIND_VERTEX_BUFFER);

    // インデックスバッファの作成
    indexBuffer_ = dx11::createBuffer(indices, D3D11_BIND_INDEX_BUFFER);

    //----------------------------------------------------------
    // 入力レイアウトの作成
    //----------------------------------------------------------
    inputLayout_ = dx11::createInputLayout(vertex::VertexPosUv::inputLayout, std::size(vertex::VertexPosUv::inputLayout));

    return true;
}
void DrawLine(const float3& startPos, const float3& endPos, const float4& color)
{
    dx11::setInputLayout(inputLayout_);    // 入力レイアウト
    dx11::vs::setShader(shaderVs_3D_);     // 頂点データー
    dx11::ps::setShader(shaderPs_Flat_);   // PS ピクセルシェーダー(テクスチャなし)
    dx11::ps::setTexture(0, nullptr);      // テクスチャー

    dx11::setVertexBuffer(0, vertexBuffer_);   // 頂点バッファ
    dx11::setIndexBuffer(nullptr);             // インデックスバッファ

    dx11::setConstantBuffer("ColorCB", cb::MeshColorCB{ color });

    vertex::VertexPosUv v[] = {
        { math::CastToXMFLOAT3(startPos), { 0.0f, 0.0f } },
        { math::CastToXMFLOAT3(endPos), { 0.0f, 0.0f } },
    };

    dx11::drawUserPrimitive(dx11::Primitive::LineList, static_cast<u32>(std::size(v)), v);
}
}   // namespace renderer
