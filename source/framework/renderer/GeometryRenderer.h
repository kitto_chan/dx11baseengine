﻿//---------------------------------------------------------------------------
//!	@file	Geomertry.h
//! @brief	ジオメトリーを描画
//---------------------------------------------------------------------------
namespace renderer {

bool Init();   //!< 初期化

void DrawLine(const float3& startPos, const float3& EndPos, const float4& color = float4(1.0f, 1.0f, 1.0f, 1.0f));
}   // namespace renderer
