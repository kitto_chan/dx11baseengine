﻿//---------------------------------------------------------------------------
//!	@file	ShadowMap.cpp
//!	@brief	シャドウマップ管理
//---------------------------------------------------------------------------
#include "ShadowMap.h"
namespace {
//!< シャドウ用定数バッファ
struct ShadowCB
{
    matrix matLightView     = math::identity();   //!< シャドウ用ビュー行列
    matrix matLightProj     = math::identity();   //!< シャドウ用投影行列
    matrix matLightViewProj = math::identity();   //!< シャドウ用ビュー×投影行列

    s32 sampleCount = 16;   //!< 影サンプリング回数
    int useless[3];
};
ShadowCB _shadowCB;

cb::CameraCB _cameraCB;
f32          _height      = 50.0f;
f32          _range       = 15.0f;
s32          _sampleCount = 16;
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//! @param resolution : 解像度
//! @param format     : ピクセルフォーマット
//---------------------------------------------------------------------------
ShadowMap::ShadowMap(u32 resolution, DXGI_FORMAT format)
: RESOLUTION(resolution)
{
    // 解像度は正方形でも長方形でもOK
    // カラーはダミーなのでどんなフォーマットでも行けるけど今回は1番軽いので
    _textureColor = dx11::createTargetTexture(RESOLUTION, RESOLUTION, DXGI_FORMAT_R8G8B8A8_UNORM);
    _textureDepth = dx11::createTargetTexture(RESOLUTION, RESOLUTION, format);

    // サンプラー
    //dx11::ps::setSamplerState(15, dx11::commonStates().LinearClamp());

    //----------------------------
    // シャドウ生成用ラスタライザーステート
    // シャドウバッファ描画の段階でハードウェア的にバイアスをかけておく
    //----------------------------
    constexpr f32 depthBias      = 0.0002f;
    constexpr f32 depthSlopeBias = 0.0f;
    {
        D3D11_RASTERIZER_DESC desc{};
        desc.FillMode              = D3D11_FILL_SOLID;   // 塗りつぶし3角形
        desc.CullMode              = D3D11_CULL_BACK;    // 裏面カリング
        desc.FrontCounterClockwise = false;              // 表面は反時計回り
        desc.DepthBias             = (INT)depthBias;     // デプスバイアス
        desc.DepthBiasClamp        = 0.0f;               //
        desc.SlopeScaledDepthBias  = depthSlopeBias;     // デプス傾斜バイアス
        desc.DepthClipEnable       = true;               // 0.0 = 1.0を超えた部分を描画しない
        desc.ScissorEnable         = false;              // シザーOFF
        desc.MultisampleEnable     = false;              // マルチサンプルアンチエイリアシング(MSAA) OFF
        desc.AntialiasedLineEnable = false;              // アンチエイリアスのライン描画OFF

        dx11::d3dDevice()->CreateRasterizerState(&desc, &_shadowRasterizerState);
    }

    //----------------------------
    // デプス比較用のサンプラステート
    //----------------------------
    {
        D3D11_SAMPLER_DESC desc{};
        desc.Filter         = D3D11_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR;   // デプス比較COMPARISON
        desc.AddressU       = D3D11_TEXTURE_ADDRESS_CLAMP;
        desc.AddressV       = D3D11_TEXTURE_ADDRESS_CLAMP;
        desc.AddressW       = D3D11_TEXTURE_ADDRESS_CLAMP;
        desc.MipLODBias     = 0.0f;
        desc.MaxAnisotropy  = 1;
        desc.ComparisonFunc = D3D11_COMPARISON_LESS;   // シャドウ比較的の条件式
        desc.BorderColor[0] = 1.0f;                    //R
        desc.BorderColor[1] = 1.0f;                    //G
        desc.BorderColor[2] = 1.0f;                    //B
        desc.BorderColor[3] = 1.0f;                    //A
        desc.MinLOD         = -FLT_MAX;
        desc.MaxLOD         = +FLT_MAX;

        dx11::d3dDevice()->CreateSamplerState(&desc, &_comparisonState);
    }

    dx11::ps::setSamplerState(15, _comparisonState);
}
//---------------------------------------------------------------------------
//! シャドウレンダリング開始
//! @param [in] center 影投影の中心
//! @param [in] ligDir 光源の角度
//---------------------------------------------------------------------------
void ShadowMap::Begin(const float3& center, const float3& ligDir)
{
    //! 変更前にクリアする
    dx11::setRenderTarget(0, nullptr);

    //===========================================
    // 描画先をシャドウバッファにする
    //===========================================
    dx11::ps::setTexture(15, nullptr);
    dx11::setRenderTarget(0, _textureColor);
    dx11::setDepthStencil(_textureDepth);
    // バッファクリア
    dx11::clearDepth(_textureDepth, 1.0f);

    dx11::setRasterizerState(_shadowRasterizerState);

    //===========================================
    // 光源位置からの視点でカメラの設定
    //===========================================
    float3 lightDirNor = normalize(ligDir);

    // カメラを置く高さ（モデルが収まる高さ）
    f32 height = _height;
    // 影を投影する高さ
    f32 range = _range;

    _cameraCB.eyePos  = center + lightDirNor * height;
    _cameraCB.matView = math::lookAtRH(_cameraCB.eyePos, center);
    _cameraCB.matProj = math::scale(float3(1.0f / range, 1.0f / range, -0.5f / height));
    dx11::setConstantBuffer("CameraCB", _cameraCB);

    _shadowCB.matLightView     = _cameraCB.matView;
    _shadowCB.matLightProj     = _cameraCB.matProj;
    _shadowCB.matLightViewProj = mul(_cameraCB.matView, _cameraCB.matProj);
    _shadowCB.sampleCount      = _sampleCount;
    dx11::setConstantBuffer("ShadowCB", _shadowCB);

    RenderImgui();
}
//---------------------------------------------------------------------------
//! シャドウレンダリング終了
//---------------------------------------------------------------------------
void ShadowMap::End()
{
    // シャドウバッファを外す
    dx11::setDepthStencil(nullptr);

    // テクスチャとしてシャドウバッファを設定
    dx11::ps::setTexture(15, _textureDepth);
    dx11::ps::setSamplerState(15, _comparisonState);

    // ラスタライザステート元に戻す
    dx11::setRasterizerState(nullptr);
    dx11::setRenderTarget(0, nullptr);
}
//---------------------------------------------------------------------------
//! ImGui描画
//---------------------------------------------------------------------------
void ShadowMap::RenderImgui()
{
    if(!_isRenderImgui) return;

    if(!ImGui::Begin("Shadowmap", &_isRenderImgui)) {
        ImGui::End();
        return;
    }
    // ImGuiのデバッグ表示
    {
        imgui::ImguiColumn2FormatBegin("Resolution");
        ImGui::AlignTextToFramePadding();
        ImGui::Text("%d", RESOLUTION);
        imgui::ImguiColumn2FormatEnd();

        imgui::ImguiColumn2FormatBegin("Height");
        ImGui::DragFloat("##Height", &_height);
        ImGui::SameLine();
        imgui::HelpMarker(u8"高さ");
        imgui::ImguiColumn2FormatEnd();

        imgui::ImguiColumn2FormatBegin("Range");
        ImGui::DragFloat("##Range", &_range);
        ImGui::SameLine();
        imgui::HelpMarker(u8"範囲");
        imgui::ImguiColumn2FormatEnd();

        imgui::ImguiColumn2FormatBegin("SampleCount");
        ImGui::DragInt("##SampleCount", &_sampleCount, 1, 64);
        ImGui::SameLine();
        imgui::HelpMarker(u8"サンプリング回数によって処理重くなる");
        imgui::ImguiColumn2FormatEnd();

        ImGui::Text(u8"シャドウマップ: ");
        f32 width = ImGui::GetWindowWidth();
        ImGui::Image(static_cast<ID3D11ShaderResourceView*>(*_textureDepth), ImVec2(width, width));
    }
    ImGui::End();
}
//---------------------------------------------------------------------------
//! 初期化したかどうか
//---------------------------------------------------------------------------
ShadowMap::operator bool()
{
    return static_cast<bool>(_textureDepth) && static_cast<bool>(_textureColor);
}
