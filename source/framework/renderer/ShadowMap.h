﻿//---------------------------------------------------------------------------
//!	@file	ShadowMap.h
//!	@brief	シャドウマップ管理
//---------------------------------------------------------------------------
#pragma once

class ShadowMap
{
public:
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------

    //! コンストラクタ
    //! @param resolution : 解像度
    //! @param format     : ピクセルフォーマット
    ShadowMap(u32 resolution, DXGI_FORMAT format);

    //! シャドウレンダリング開始
    //! @param [in] center 影投影の中心
    //! @param [in] ligDir 光源の角度
    void Begin(const float3& center, const float3& ligDir);
    //! シャドウレンダリング終了
    void End();

    void RenderImgui();

    // 初期化されているかどうか
    operator bool();

    //! デプスバッファを取得
    dx11::Texture* GetDepthTexture() const { return _textureDepth.get(); }

    // Imguiデバッグを描画かどうか
    void SetIsRenderImgui(bool isRenderImgui) { _isRenderImgui = isRenderImgui; };

private:
    const u32 RESOLUTION = 2048;
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    bool _isRenderImgui = false;   //!< Imguiを描画するかどうか

    //! シャドウマップ用カラーテクスチャ（ダミー）
    std::shared_ptr<dx11::Texture> _textureColor;
    //! シャドウマップ用デプステクスチャ
    std::shared_ptr<dx11::Texture> _textureDepth;

    //! シャドウ生成用ラスタライザーステート
    //! シャドウバッファ描画の段階でハードウェア的にバイアスをかけておく
    com_ptr<ID3D11RasterizerState> _shadowRasterizerState;
    //! デプス比較用のサンプラステート
    com_ptr<ID3D11SamplerState> _comparisonState;

private:
};
