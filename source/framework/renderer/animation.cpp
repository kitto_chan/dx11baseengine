﻿//---------------------------------------------------------------------------
//!	@file	animation.cpp
//!	@brief	3Dアニメーション
//---------------------------------------------------------------------------
#include "animation.h"
#include "animation_layer.h"
#include "impl/model_impl.h"
#include "impl/animation_impl.h"
#include "impl/animation_layer_impl.h"
#include "import_fbx.h"

#include <filesystem>
#include <fstream>

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void AnimationImpl::update(f32 t)
{
    _time += t * _speed;

    //----------------------------------------------------------
    // アニメーション再生ループ判定
    //----------------------------------------------------------
    if(_activeLayer) {
        auto animationLength = _activeLayer->animationLength();
        if(_time > animationLength) {
            // ループ再生時は先頭に戻す
            if(_playType == Animation::PlayType::Loop) {
                _time -= animationLength;
            }
            // 単発再生時は最終フレームでポーズ
            else {
                _time = animationLength;
            }
            _lastFrame = true;
        }
        else {
            _lastFrame = false;
        }
    }
}

//---------------------------------------------------------------------------
//! アニメーション再生リクエスト
//---------------------------------------------------------------------------
bool AnimationImpl::play(const char* name, Animation::PlayType playType, f32 speed)
{
    auto it = _animationLayers.find(name);
    if(it == std::end(_animationLayers)) {
        return false;
    }

    //!< アニメションを変わったら、ブレンドする
    if(_activeLayer) {
        _blendTime = _activeLayer->animationLength_ - _time;
        _blendTime = std::min(MAX_BLEND_TIME, _blendTime);
    }

    _time        = 0.0f;               // 現在の再生時間
    _lastLayer   = _activeLayer;       // ブレンドのため,前のレイヤーを記録する
    _activeLayer = it->second.get();   // 現在再生中のアニメーションレイヤー
    _playType    = playType;           // アニメーション再生タイプ
    _speed       = speed;              //!< 再生速度を設定
    return true;
}

//---------------------------------------------------------------------------
//! アニメーションレイヤーを追加
//---------------------------------------------------------------------------
void AnimationImpl::appendLayer(const char* name, std::shared_ptr<AnimationLayer> layer)
{
    _animationLayers[name] = std::static_pointer_cast<AnimationLayerImpl>(layer);
}

//---------------------------------------------------------------------------
//! アニメーションレイヤーの数を取得
//---------------------------------------------------------------------------
size_t AnimationImpl::animationLayerCount() const
{
    return _animationLayers.size();
}

//---------------------------------------------------------------------------
//! アニメーションレイヤーを取得
//---------------------------------------------------------------------------
const AnimationLayer* AnimationImpl::animationLayer(const char* name) const
{
    auto it = _animationLayers.find(name);

    if(it == std::end(_animationLayers)) {
        return nullptr;
    }

    return it->second.get();
}

//---------------------------------------------------------------------------
//! ポーズ情報の作成
//---------------------------------------------------------------------------
void AnimationImpl::buildPose(Pose& pose, const ModelImpl* model)
{
    if(!_activeLayer)
        return;

    f32 blendWeight = 1.0f;   // 既定のblendWeight, 1.0fはブレンドしない

    if(_blendTime > 0.0f && _time < _blendTime) {
        blendWeight = math::normalize(_time, 0.0f, _blendTime);
    }

    _activeLayer->updatePose(_time, pose, model, blendWeight);

    if(_blendTime > 0.0f && _time < _blendTime) {
        Pose lastPose  = model->pose_;
        f32  blendtime = _lastLayer->animationLength_ - _blendTime + _time;

        _lastLayer->updatePose(blendtime, lastPose, model, 1.0f - blendWeight);
        pose.blend(lastPose, 1.0f);
    }
    else {
        _blendTime = 0.0f;
    }
}
//---------------------------------------------------------------------------
//! 現在のアニメションはラストフレームかどうか
//---------------------------------------------------------------------------
bool AnimationImpl::IsLastFrame(f32 blendTime) const
{
    if(_activeLayer) {
        return _time >= _activeLayer->animationLength() - blendTime;
    }
    return false;
}
//---------------------------------------------------------------------------
//! 現在のアニメションの長さ
//---------------------------------------------------------------------------
f32 AnimationImpl::GetCurrentLayerAnimationLength() const
{
    return _activeLayer->animationLength();
}
//---------------------------------------------------------------------------
//! 現在のアニメションの再生時間
//---------------------------------------------------------------------------
f32 AnimationImpl::GetCurretLayerAnimationTime() const
{
    return _time;
}
//---------------------------------------------------------------------------
//! アニメーションタイムを設定
//---------------------------------------------------------------------------
void AnimationImpl::SetAnimationTime(const f32 time)
{
    _time = time;
}
//---------------------------------------------------------------------------
//! ルートボーンIdを設定
//---------------------------------------------------------------------------
void AnimationImpl::SetRootBoneId(const s32 rootBoneId)
{
    _rootBoneId = rootBoneId;
}
//---------------------------------------------------------------------------
//! PlayTypeを設定
//---------------------------------------------------------------------------
void AnimationImpl::SetPlayType(Animation::PlayType playType)
{
    _playType = playType;
}

//---------------------------------------------------------------------------
//! ブレントリストの処理
//---------------------------------------------------------------------------
bool AnimationImpl::BindBlendList(const json& jAnim)
{
    // ブレンド処理
    if(jAnim.contains("BlendList")) {
        json jBlendList = jAnim["BlendList"];
        for(auto& element : jBlendList.items()) {
            std::string from = element.key();

            json jBlendDes = element.value();
            for(auto& desc : jBlendDes) {
                BlendDesc blendDesc{
                    desc["To"].get<std::string>(),
                    desc["BlendTime"].get<float>()
                };
                _blendDescList.emplace(from, blendDesc);
            }
        }
    }

    return true;
}

//---------------------------------------------------------------------------
//! アニメーション作成
//---------------------------------------------------------------------------
std::shared_ptr<Animation> createAnimation(const Animation::Desc* desc, size_t count, s32 rootBoneId)
{
    std::shared_ptr<AnimationImpl> p = std::make_shared<AnimationImpl>();

    if(p) {
        // Descに指定されているファイルをインポート
        for(u32 i = 0; i < count; ++i) {
            std::unique_ptr<importer::ImportFbx> importFbx = importer::createImportFbx(desc[i].path_, 1.0f, importer::ImportFbx::FLAG_IMPORT_ANIMATION, rootBoneId);

            if(!importFbx) {
                continue;
            }

            auto animationLayer = importFbx->convertToAnimation(0);

            p->appendLayer(desc[i].name_, std::move(animationLayer));
        }
    }
    return p;
}
//---------------------------------------------------------------------------
//! アニメーション作成
//! @param  [in]	path .animファイルのパス
//---------------------------------------------------------------------------
std::shared_ptr<Animation> createAnimation(std::string_view path)
{
    json jAnim = Animation::ReadAnimFile(path);

    // マッピングリストをつくる
    json             jKeyMap = jAnim["KeyMap"];
    Animation::Desc* AniDesc = new Animation::Desc[jKeyMap.size()];
    int              count   = 0;
    for(auto& element : jKeyMap.items()) {
        AniDesc[count] = { element.key().c_str(), element.value() };
        count++;
    }

    // アニメーションを生成
    std::shared_ptr<Animation> animation = createAnimation(AniDesc, count);

    // ブレントをバインドする
    animation->BindBlendList(jAnim);

    // TODO: Fix
    // 今デフォルト Idleのアニメーション再生するけど、もしなかったバッグあるから
    //animation->play("Idle", Animation::PlayType::Loop, 1.0f);

    return animation;
}
//---------------------------------------------------------------------------
// .anim ファイルを読み込む
//---------------------------------------------------------------------------
json Animation::ReadAnimFile(std::string_view path)
{
    if(!string::IsPathExtension(path, L".anim")) {
        ASSERT_MESSAGE(false, ".animファイルではない、対応できない");
        return false;
    }

    nlohmann::json jModel;
    //---------
    // ファイルを読み込む
    std::ifstream ifs(path);
    ifs >> jModel;
    if(ifs.is_open())
        ifs.close();

    // Json中のアニメーションフィールド戻り値を返す
    return jModel["Animation"];
}
//---------------------------------------------------------------------------
// .anim ファイルを書き込む
//---------------------------------------------------------------------------
json Animation::GenerateAnimJson(const std::map<std::string, std::string>&              animationMapping,
                                 const std::unordered_multimap<std::string, BlendDesc>& blendDescList)
{
    json jAnimConfig;
    for(auto& anim : animationMapping) {
        jAnimConfig["KeyMap"].emplace(anim.first, anim.second);
    }

    jAnimConfig["BlendList"] = json::object();

    for(auto& element : blendDescList) {
        json blendJsonArray;
        json blendJson;

        BlendDesc blendDesc    = element.second;
        blendJson["To"]        = blendDesc._to;
        blendJson["BlendTime"] = blendDesc._blendTime;

        if(jAnimConfig["BlendList"].contains(element.first)) {
            blendJsonArray = jAnimConfig["BlendList"][element.first];
        }
        blendJsonArray.push_back(blendJson);
        jAnimConfig["BlendList"][element.first] = blendJsonArray;
    }

    return jAnimConfig;
}
