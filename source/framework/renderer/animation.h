﻿//---------------------------------------------------------------------------
//!	@file	animation.h
//!	@brief	3Dアニメーション
//---------------------------------------------------------------------------
#pragma once

class AnimationLayer;

//===========================================================================
//! 3Dアニメーション
//===========================================================================
class Animation
{
public:
    // アニメーション設定
    struct Desc
    {
        const char*      name_;   //!< アニメーション名(任意)
        std::string_view path_;   //!< アニメーションファイルパス
    };

    //! アニメーション再生タイプ
    enum class PlayType
    {
        Once,   //!< 一度だけ再生
        Loop,   //!< ループ再生
    };

    // ブレンド設定
    struct BlendDesc
    {
        std::string _to;
        f32         _blendTime;
    };

    //! デストラクタ
    virtual ~Animation() = default;

    //! 更新
    //! @param  [in]    t       進める時間(単位:秒)
    virtual void update(f32 t) = 0;

    //! アニメーション再生リクエスト
    //! @param  [in]    name    名前
    //! @param  [in]    playType    アニメーション再生タイプ
    virtual bool play(const char* name, Animation::PlayType playType, f32 speed) = 0;

    //! アニメーションレイヤーを追加
    //! @param  [in]    name    アニメーション名(任意)
    //! @param  [in]    layer   アニメーションレイヤー
    virtual void appendLayer(const char* name, std::shared_ptr<AnimationLayer> layer) = 0;

    //! アニメーションレイヤーの数を取得
    virtual size_t animationLayerCount() const = 0;

    //! アニメーションレイヤーを取得
    //! @param  [in]    name    アニメーション名
    virtual const AnimationLayer* animationLayer(const char* name) const = 0;

    //! 現在のアニメーションは最後のフレームかどうか
    virtual bool IsLastFrame(f32 frame) const = 0;

    //!< 現在のアニメションの長さ
    virtual f32 GetCurrentLayerAnimationLength() const = 0;

    //!< 現在アニメションの再生時間
    virtual f32 GetCurretLayerAnimationTime() const = 0;

    //! ルートのボーンIDを設定
    virtual void SetRootBoneId(const s32 rootBoneId) = 0;

    //!< アニメションのタイムを設定
    virtual void SetAnimationTime(const f32 time) = 0;

    //!< アニメションの再生プレイタイプを変更
    virtual void SetPlayType(Animation::PlayType playType) = 0;

    //! アニメションを読み込む
    //! @param jAnim jsonのモデルデータ
    virtual bool BindBlendList(const nlohmann::json& jAnim) = 0;

    // .animファイルを読み込む
    static json ReadAnimFile(std::string_view path);

    // .animファイルを書き込む
    static json GenerateAnimJson(const std::map<std::string, std::string>&              animationMapping,
                                 const std::unordered_multimap<std::string, BlendDesc>& blendDescList);

protected:
    Animation() = default;
};

//! アニメーション作成
//! @param  [in]    desc    アニメーションファイル一覧
//! @param  [in]    count   アニメーション個数
std::shared_ptr<Animation> createAnimation(const Animation::Desc* desc, size_t count, s32 rootBoneId = -1);

//! アニメーション作成
//! @param  [in]	path .animファイルのパス
std::shared_ptr<Animation> createAnimation(std::string_view path);
