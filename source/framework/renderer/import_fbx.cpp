﻿//---------------------------------------------------------------------------
//!	@file	importer_fbx.cpp
//!	@brief	FBXインポーター
//---------------------------------------------------------------------------
#include "impl/model_impl.h"
#include "impl/animation_impl.h"
#include "impl/animation_layer_impl.h"
#include "import_fbx.h"
#include "mesh_optimizer.h"

#include "OpenFBX/src/ofbx.h"
#include <set>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <numeric>

namespace importer {

class ImportAnimation;

//---------------------------------------------------------------------------
//! キャスト ofbx::Vec3 → float3
//---------------------------------------------------------------------------
float3 cast(const ofbx::Vec3& v)
{
    return float3(static_cast<f32>(v.x), static_cast<f32>(v.y), static_cast<f32>(v.z));
}

//---------------------------------------------------------------------------
//! キャスト ofbx::Matrix → matrix
//---------------------------------------------------------------------------
matrix cast(const ofbx::Matrix& fbxMatrix)
{
    matrix m;

    m._11_12_13_14 = float4(static_cast<f32>(fbxMatrix.m[0]),
                            static_cast<f32>(fbxMatrix.m[1]),
                            static_cast<f32>(fbxMatrix.m[2]),
                            static_cast<f32>(fbxMatrix.m[3]));

    m._21_22_23_24 = float4(static_cast<f32>(fbxMatrix.m[4]),
                            static_cast<f32>(fbxMatrix.m[5]),
                            static_cast<f32>(fbxMatrix.m[6]),
                            static_cast<f32>(fbxMatrix.m[7]));

    m._31_32_33_34 = float4(static_cast<f32>(fbxMatrix.m[8]),
                            static_cast<f32>(fbxMatrix.m[9]),
                            static_cast<f32>(fbxMatrix.m[10]),
                            static_cast<f32>(fbxMatrix.m[11]));

    m._41_42_43_44 = float4(static_cast<f32>(fbxMatrix.m[12]),
                            static_cast<f32>(fbxMatrix.m[13]),
                            static_cast<f32>(fbxMatrix.m[14]),
                            static_cast<f32>(fbxMatrix.m[15]));

    return m;
}

//--------------------------------------------------------------
// 座標系
//--------------------------------------------------------------
enum class Orientation
{
    Y_UP,
    Z_UP,
    Z_MINUS_UP,
    X_MINUS_UP,
    X_UP
};
Orientation orientation_ = Orientation::Y_UP;

//---------------------------------------------------------------------------
//! 座標系の補正(位置)
//---------------------------------------------------------------------------
float3 fixOrientation(const float3& v)
{
    switch(orientation_) {
        case Orientation::Y_UP: return float3(v.x, v.y, v.z);
        case Orientation::Z_UP: return float3(v.x, v.z, -v.y);
        case Orientation::Z_MINUS_UP: return float3(v.x, -v.z, v.y);
        case Orientation::X_MINUS_UP: return float3(v.y, -v.x, v.z);
        case Orientation::X_UP: return float3(-v.y, v.x, v.z);
    }
    assert(false);
    return v;
}

//---------------------------------------------------------------------------
//! 座標系の補正(回転)
//---------------------------------------------------------------------------
quaternion fixOrientation(const quaternion& v)
{
    switch(orientation_) {
        case Orientation::Y_UP: return quaternion(v.x, v.y, v.z, v.w);
        case Orientation::Z_UP: return quaternion(v.x, v.z, -v.y, v.w);
        case Orientation::Z_MINUS_UP: return quaternion(v.x, -v.z, v.y, v.w);
        case Orientation::X_MINUS_UP: return quaternion(v.y, -v.x, v.z, v.w);
        case Orientation::X_UP: return quaternion(-v.y, v.x, v.z, v.w);
    }
    assert(false);
    return v;
}

//===========================================================================
//! ジオメトリ情報
//===========================================================================
struct ImportGeometry
{
    const ofbx::Geometry*   fbxGeometry_ = nullptr;   //!< FBXジオメトリ
    std::vector<u32>        indices_;                 //!< インデックス
    std::vector<std::byte>  vertexData_;              //!< 頂点データー
    std::vector<ofbx::Vec3> computedTangents_;
    std::vector<ofbx::Vec3> computedNormals_;
};

//===========================================================================
//! メッシュ情報
//===========================================================================
struct ImportMesh : public Mesh
{
    const ofbx::Mesh*     fbxMesh_     = nullptr;   //!< FBXメッシュ
    const ofbx::Material* fbxMaterial_ = nullptr;   //!< FBXマテリアル
    bool                  isSkinned_   = false;     //!< スキニングありかどうか
    s32                   boneIndex_   = -1;        //!< ボーン番号
    s32                   submesh_     = -1;        //!< サブメッシュ番号(メッシュ内に複数のマテリアルがある場合に割り当て)

    std::vector<float3> geometryVertices_;   //!< 衝突判定三角形情報(並列処理用に分割)
    std::vector<u32>    geometryIndices_;    //!< 衝突判定三角形インデックス情報(並列処理用に分割)

    std::vector<ModelVertex> vertices_;   //!< 頂点配列(並列処理用に分割)
    std::vector<u32>         indices_;    //!< インデックス配列(並列処理用に分割)
};

//===========================================================================
//! テクスチャ情報
//===========================================================================
struct ImportTexture : public Texture
{
    const ofbx::Texture* fbxTexture_ = nullptr;   //!< FBXテクスチャ
    bool                 isExist_    = false;     //!< ファイル検出できたかどうか

    std::string originalPath_;   //!< FBXファイル内の元のパス情報
};

//===========================================================================
//! マテリアル情報
//===========================================================================
struct ImportMaterial : public Material
{
    const ofbx::Material* fbxMaterial_ = nullptr;
};

//===========================================================================
// キーフレーム
//===========================================================================
struct ImportKey
{
    float3     position_{ 0.0f, 0.0f, 0.0f };
    quaternion rotation_{ 0.0f, 0.0f, 0.0f, 1.0f };
    s64        time_ = 0;
};

//===========================================================================
//! 頂点スキン情報
//===========================================================================
struct ImportSkin
{
    std::pair<u16, f32> jointWeights_[4]{};   //!< first:スキニングウェイト second:関節(ボーン)番号
    s32                 count_ = 0;           //!< 個数
};

//===========================================================================
//! FBXインポーター
//===========================================================================
class ImportFbxImpl : public importer::ImportFbx
{
public:
    struct Config;

    //! 読み込み
    //! @param  [in]    path    FBXファイルパス
    //! @param  [in]    scale   全体スケール(default:1.0f)
    //! @param  [in]    flags   オプション設定(ImportFbx::FLAGSの組み合わせ)
    //! @param  [in]    rootBoneId   アニメションが常に原点にいるため、root boneをセットする(default:-1.0f(しない))
    bool load(std::string_view path, f32 scale, u32 flags, s32 rootBoneId = -1);

    //! モデルにコンバート
    virtual std::shared_ptr<ModelImpl> convertToModel() const override;

    //! アニメーションにコンバート
    //! @param  [in]    index   アニメーション番号
    virtual std::shared_ptr<AnimationLayerImpl> convertToAnimation(u32 index) const override;

    //! アニメーションの数を取得
    virtual size_t animationCount() const override { return animations_.size(); }

private:
    //! FBXファイルに埋め込まれているデーターを書き出し
    //! @param  [in]    fbxScene        FBXシーンオブジェクト
    //! @param  [in]    srcDirectory    書き出すフォルダパス
    void extractEmbedded(const ofbx::IScene* fbxScene, std::string_view srcDirectory);

    //! ジオメトリを収集
    //! @param  [in]    fbxScene    FBXシーンオブジェクト
    void gatherGeometries(const ofbx::IScene* fbxScene);

    //! メッシュを収集
    //! @param  [in]    fbxScene    FBXシーンオブジェクト
    void gatherMeshes(const ofbx::IScene* fbxScene);

    // マテリアルを収集
    //! @param  [in]    sourceDirectory 親ディレクトリ
    void gatherMaterials(std::string_view sourceDirectory);

    //! ボーンのソート
    //! @param  [in]    forceSkinned    強制スキニング
    void sortBones(bool forceSkinned);

    //! ボーンの収集
    void gatherBones(const ofbx::IScene* fbxScene, bool forceSkinned);

    //! アニメーションの収集
    void gatherAnimations(const ofbx::IScene* fbxScene);

    //! アニメーション情報の作成
    bool buildAnimations(const ofbx::IScene* fbxScene);

    //! スケルトン階層情報の構築
    void buildSkeletons();

    //! ボーンの構築
    bool buildBones();

    //! スキン情報を計算
    void writeSkinInfo(std::vector<ImportSkin>& skins, const ImportMesh& mesh);

    //! 頂点メッシュデーターの作成
    void buildMeshes();

    //! テクスチャの読み込み
    bool loadTextures();

private:
    std::shared_ptr<dx11::Buffer> bufferVb_;   //!< 頂点バッファ
    std::shared_ptr<dx11::Buffer> bufferIb_;   //!< インデックスバッファ

    std::vector<const ofbx::Object*> fbxBones_;               //!< FBXボーン情報
    f32                              fbxScale_      = 1.0f;   //!< 全体スケール値
    s32                              fbxRootBoneId_ = -1;     //!< アニメションの親のボーンId

    std::vector<float3> geometryVertices_;   //!< 衝突判定三角形情報
    std::vector<u32>    geometryIndices_;    //!< 衝突判定三角形インデックス情報

    std::vector<ImportGeometry>  geometries_;   //!< ジオメトリ配列
    std::vector<ImportMesh>      meshes_;       //!< メッシュ配列
    std::vector<ImportAnimation> animations_;   //!< アニメーション配列
    std::vector<ImportMaterial>  materials_;    //!< マテリアル配列

    std::vector<Bone>            bones_;         //!< ボーン情報
    std::unordered_map<u32, s32> boneHashMap_;   //!< ボーン情報のハッシュマップ(first:名前ハッシュ second:関節番号)
};

//===========================================================================
//! コンフィグ設定
//===========================================================================
struct Config
{
    f32 meshScale_     = 1.0f;
    f32 positionError_ = 0.02f;
    f32 rotationError_ = 0.001f;
};

Config config;

//===========================================================================
//! アニメーション
//===========================================================================
class ImportAnimation : public AnimationLayerImpl
{
public:
    //---------------------------------------------------------------------------
    //! アニメーション情報の作成
    //---------------------------------------------------------------------------
    bool build(const ofbx::IScene* fbxScene, const std::vector<const ofbx::Object*>& fbxBones, f32 fbxSclale = 1.0f);

public:
    s32                         fbxRootBoneId_     = -1;
    const ofbx::IScene*         fbxScene_          = nullptr;
    const ofbx::AnimationStack* fbxAnimationStack_ = nullptr;
    std::string                 name_              = "";

    std::vector<std::vector<ImportKey>> allKeys_;
};

//---------------------------------------------------------------------------
// カーブの値を取得
// 指定時間のキーフレームを取得。一致しない場合はキーを２つ選択してその間を線形補間。
//! @param  [in]    time    キー時間
//! @param  [in]    curve   FBXアニメーションカーブ
//---------------------------------------------------------------------------
f32 evalCurve(s64 time, const ofbx::AnimationCurve& curve)
{
    const s64* times  = curve.getKeyTime();
    const f32* values = curve.getKeyValue();
    const s32  count  = curve.getKeyCount();

    ASSERT_MESSAGE(count > 0, "キーがありません。");

    time = std::clamp(time, times[0], times[count - 1]);

    for(s32 i = 0; i < count; ++i) {
        if(time == times[i]) {
            return values[i];
        }
        if(time < times[i]) {
            assert(i > 0);
            assert(time > times[i - 1]);
            const f32 t = static_cast<f32>((time - times[i - 1]) / static_cast<f64>(times[i] - times[i - 1]));
            return values[i - 1] * (1.0f - t) + values[i] * t;
        }
    }
    assert(0);
    return 0.0f;
}

//---------------------------------------------------------------------------
// キーフレームを抽出
//---------------------------------------------------------------------------
void gatherKeyframe(const ofbx::Object& bone, f64 animationLength, const ofbx::AnimationLayer& layer, std::vector<ImportKey>& keys)
{
    const ofbx::AnimationCurveNode* translationNode = layer.getCurveNode(bone, "Lcl Translation");
    const ofbx::AnimationCurveNode* rotationNode    = layer.getCurveNode(bone, "Lcl Rotation");
    if(!translationNode && !rotationNode) {
        return;
    }

    //----------------------------------------------------------
    // 時間を書き込み
    //----------------------------------------------------------
    std::set<s64> times;   // std::set ソート済・値のみを持ち値の重複を許さない

    // 最初にキーフレームの先頭と末尾を登録
    times.insert(0);
    times.insert(ofbx::secondsToFbxTime(animationLength));

    // キーフレームの時間軸方向にソートされたキーを登録
    // 時間のみを書き込んだキーリストを生成。同一時間の値は持たせない。
    auto gatherKeyframeTimes = [&times](const ofbx::AnimationCurve* curve) {
        if(!curve)
            return;

        const s64* keyTimes = curve->getKeyTime();
        for(s32 i = 0; i < curve->getKeyCount(); ++i) {
            times.insert(keyTimes[i]);
        }
    };

    if(translationNode) {
        gatherKeyframeTimes(translationNode->getCurve(0));
        gatherKeyframeTimes(translationNode->getCurve(1));
        gatherKeyframeTimes(translationNode->getCurve(2));
    }

    if(rotationNode) {
        gatherKeyframeTimes(rotationNode->getCurve(0));
        gatherKeyframeTimes(rotationNode->getCurve(1));
        gatherKeyframeTimes(rotationNode->getCurve(2));
    }

    // 時間軸方向にソート済のキーを生成
    keys.reserve(times.size());
    for(auto& t : times) {
        ImportKey key;
        key.time_ = t;
        keys.emplace_back(key);
    }

    //----------------------------------------------------------
    // Rotation / Translationを書き込み
    //----------------------------------------------------------
    // Rotationを書き込み
    // クオータニオンではなくXYZのローテーション情報
    // 一時的にクオータニオンベクトルxyzにオイラー角を保存
    auto gatherKeyframeRotation = [&](u32 index, const ofbx::AnimationCurve* curve) {
        if(!curve) {
            const ofbx::Vec3 lclRotation = bone.getLocalRotation();

            for(auto& key : keys) {
                f32 value = float3(lclRotation.x, lclRotation.y, lclRotation.z).f32[index];

                float4 v      = key.rotation_.xyzw;
                v.f32[index]  = value;
                key.rotation_ = v;
            }
            return;
        }

        for(auto& key : keys) {
            f32 value = evalCurve(key.time_, *curve);

            float4 v      = key.rotation_.xyzw;
            v.f32[index]  = value;
            key.rotation_ = v;
        }
    };

    // Translationを書き込み
    auto gatherKeyframePosition = [&](u32 index, const ofbx::AnimationCurve* curve) {
        if(!curve) {
            const ofbx::Vec3 lclTranslation = bone.getLocalTranslation();
            for(auto& key : keys) {
                key.position_.f32[index] = float3(lclTranslation.x, lclTranslation.y, lclTranslation.z).f32[index];
            }
            return;
        }

        for(auto& key : keys) {
            key.position_.f32[index] = evalCurve(key.time_, *curve);
        }
    };

    gatherKeyframeRotation(0, rotationNode ? rotationNode->getCurve(0) : nullptr);
    gatherKeyframeRotation(1, rotationNode ? rotationNode->getCurve(1) : nullptr);
    gatherKeyframeRotation(2, rotationNode ? rotationNode->getCurve(2) : nullptr);

    gatherKeyframePosition(0, translationNode ? translationNode->getCurve(0) : nullptr);
    gatherKeyframePosition(1, translationNode ? translationNode->getCurve(1) : nullptr);
    gatherKeyframePosition(2, translationNode ? translationNode->getCurve(2) : nullptr);

    // 正規化後にXYZ回転をクオータニオンに変換
    for(auto& key : keys) {
        const ofbx::Matrix fbxMatrix = bone.evalLocal({ key.position_.x, key.position_.y, key.position_.z },
                                                      { key.rotation_.x, key.rotation_.y, key.rotation_.z });

        matrix m = cast(fbxMatrix);

        // スケールを正規化
        float3 scale(1.0f / length(float3(m._11, m._21, m._31)),
                     1.0f / length(float3(m._12, m._22, m._32)),
                     1.0f / length(float3(m._13, m._23, m._33)));

        m._11_12_13 *= scale;
        m._21_22_23 *= scale;
        m._31_32_33 *= scale;

        key.rotation_ = math::makeRotation(m);
        key.position_ = math::makeTranslation(m);
    }
}

//---------------------------------------------------------------------------
//! アニメーション情報の作成
//---------------------------------------------------------------------------
bool ImportAnimation::build(const ofbx::IScene* fbxScene, const std::vector<const ofbx::Object*>& fbxBones, f32 fbxSclale)
{
    const ofbx::AnimationStack* fbxAnimationStack = fbxAnimationStack_;
    const ofbx::AnimationLayer* fbxLayer          = fbxAnimationStack->getLayer(0);
    ASSERT_MESSAGE(fbxScene_ == fbxScene, "異なるFBXシーンが混在しています。");

    //------------------------------------------------------
    // フレームレート
    //------------------------------------------------------
    const f32             fps      = fbxScene->getSceneFrameRate();
    const ofbx::TakeInfo* takeInfo = fbxScene->getTakeInfo(fbxAnimationStack->name);
    if(!takeInfo && strncmp(fbxAnimationStack->name, "AnimStack::", 11) == 0) {
        takeInfo = fbxScene->getTakeInfo(fbxAnimationStack->name + 11);
    }

    //------------------------------------------------------
    // アニメーションの長さ
    //------------------------------------------------------
    f64 animationLength;   // ■アニメーションの長さ (単位:秒)
    if(takeInfo) {
        // Take情報から取得
        animationLength = takeInfo->local_time_to - takeInfo->local_time_from;
    }
    else if(fbxScene->getGlobalSettings()) {
        // グローバル設定から取得
        animationLength = fbxScene->getGlobalSettings()->TimeSpanStop;
    }
    else {
        ASSERT_MESSAGE(false, "未サポートのアニメーションです。");
        return false;
    }

    [[maybe_unused]] u32 frameCount = static_cast<u32>(animationLength * fps + 0.5f);   // ■総フレーム数
    animationLength_                = static_cast<f32>(animationLength);

    //------------------------------------------------------
    // キーフレームの抽出
    //------------------------------------------------------
    auto& allKeys = allKeys_;

    auto fbxToAnimationTime = [animationLength](s64 fbxTime) {
        const f64 t = std::clamp(ofbx::fbxTimeToSeconds(fbxTime) / animationLength, 0.0, 1.0);
        return static_cast<f32>(t);
    };

    allKeys.reserve(fbxBones.size());
    for(auto fbxBone : fbxBones) {
        std::vector<ImportKey> keys;
        gatherKeyframe(*fbxBone, animationLength, *fbxLayer, keys);

        allKeys.emplace_back(std::move(keys));
    }

    //------------------------------------------------------
    // Translationを出力
    //------------------------------------------------------
    auto buildTranslation = [&](s32 i) {
        auto& fbxBone = fbxBones[i];
        auto& keys    = allKeys[i];
        if(keys.size() == 0)
            return;

        AnimationLayerImpl::CurveT curve;
        constexpr s32              ROOT_ID = 1;   //!< アニメションのメイントランク

        curve.name_ = static_cast<u32>(std::hash<std::string>()(fbxBone->name));
        {
            for(auto& key : keys) {
                curve.times_.emplace_back(std::move(fbxToAnimationTime(key.time_)));   // ■出力
            }
            for(auto& key : keys) {
                float3 pos;
                //fbxRootBoneId_ != -1 && i == fbxRootBoneId_
                if(i == 1) {
                    // 移動しないように、アニメションが常に原点に動く
                    float3 root_pos = float3(0.0f, key.position_.f32[1], 0.0f);
                    pos             = fixOrientation(root_pos * config.meshScale_ * fbxSclale);
                }
                else {
                    pos = fixOrientation(key.position_ * config.meshScale_ * fbxSclale);
                }
                //pos = fixOrientation(key.position_ * config.meshScale_ * fbxSclale);
                curve.positions_.emplace_back(std::move(pos));   // ■出力
            }
        }
        curvesT_.emplace_back(std::move(curve));
    };
    // 並列実行ジョブとして登録
    for(s32 i = 0; i < fbxBones.size(); ++i) {
        //threadPool()->submit([&, i] { buildTranslation(i); });

        buildTranslation(i);
    }
    // ジョブの実行終了待機
    //threadPool()->wait();
    //------------------------------------------------------
    // Rotationを出力
    //------------------------------------------------------
    auto buildRotation = [&](s32 i) {
        auto& fbxBone = fbxBones[i];
        auto& keys    = allKeys[i];

        if(keys.size() == 0)
            return;

        AnimationLayerImpl::CurveR curve;
        curve.name_ = static_cast<u32>(std::hash<std::string>()(fbxBone->name));
        {
            for(auto& key : keys) {
                curve.times_.emplace_back(std::move(fbxToAnimationTime(key.time_)));   // ■出力
            }
            for(auto& key : keys) {
                quaternion r = fixOrientation(key.rotation_);

                curve.rotations_.emplace_back(std::move(r));   // ■出力
            }
        }
        curvesR_.emplace_back(std::move(curve));
    };
    // 並列実行ジョブとして登録
    for(s32 i = 0; i < fbxBones.size(); ++i) {
        //threadPool()->submit([&, i] { buildRotation(i); });

        buildRotation(i);
    }

    // ジョブの実行終了待機
    //threadPool()->wait();

    return true;
}

//===========================================================================
// Fbx
//===========================================================================

//---------------------------------------------------------------------------
//! 読み込み
//---------------------------------------------------------------------------
bool ImportFbxImpl::load(std::string_view path, f32 scale, u32 flags, s32 rootBoneId)
{
    config.meshScale_ = scale;
    fbxRootBoneId_    = rootBoneId;
    //---------------------------------------------------------------------
    // ファイルから読み込み
    //---------------------------------------------------------------------
    std::vector<ofbx::u8> source;
    {
        // ファイルから読み込み
        std::ifstream file(std::string(path), std::ios::in | std::ios::binary | std::ios::ate);   // ateを指定すると最初からファイルポインタが末尾に移動
        if(!file.is_open()) {
            return false;
        }
        auto size = file.tellg();
        source.resize(static_cast<size_t>(size));

        file.seekg(0, std::ios::beg);
        file.read(reinterpret_cast<char*>(source.data()), size);
        file.close();
    }
    //----------------------------------------------------------
    // FBXシーンを読み込み
    //----------------------------------------------------------
    ofbx::IScene* fbxScene = ofbx::load(source.data(), static_cast<u32>(source.size()), static_cast<u64>(ofbx::LoadFlags::TRIANGULATE));
    if(!fbxScene) {
        ASSERT_MESSAGE(false, ofbx::getError());
        return false;
    }

    // 全体スケール
    fbxScale_ = fbxScene->getGlobalSettings()->UnitScaleFactor * 0.01f;

    // 軸方向 (3dsMax, UEなどで座標系が異なる)
    const ofbx::GlobalSettings* settings = fbxScene->getGlobalSettings();
    switch(settings->UpAxis) {
        case ofbx::UpVector_AxisX: orientation_ = Orientation::X_UP; break;
        case ofbx::UpVector_AxisY: orientation_ = Orientation::Y_UP; break;
        case ofbx::UpVector_AxisZ: orientation_ = Orientation::Z_UP; break;
    }

    auto sourceDirectory = std::filesystem::path(path).parent_path();

    gatherGeometries(fbxScene);   // ジオメトリを収集
    gatherMeshes(fbxScene);       // メッシュを収集

    //------------------------------------------------------
    // ボーンの収集
    //------------------------------------------------------
    bool skinned = false;
    for(auto& mesh : meshes_) {
        skinned |= mesh.isSkinned_;
    }
    gatherBones(fbxScene, skinned);

    if(flags & ImportFbx::FLAG_IMPORT_GEOMETRY) {
        extractEmbedded(fbxScene, sourceDirectory.string());   // 埋め込みデーターをファイルに展開

        //------------------------------------------------------
        // マテリアルの収集
        //------------------------------------------------------
        gatherMaterials(sourceDirectory.string());

        // 重複したマテリアルを削除
        {
            std::sort(std::begin(materials_), std::end(materials_),
                      [](auto const& lhs, auto const& rhs) {
                          return lhs.fbxMaterial_ < rhs.fbxMaterial_;
                      });
            decltype(materials_)::iterator result = std::unique(std::begin(materials_), std::end(materials_),
                                                                [](auto const& lhs, auto const& rhs) {
                                                                    return lhs.fbxMaterial_ == rhs.fbxMaterial_;
                                                                });
            // 不要になった要素を削除
            materials_.erase(result, std::end(materials_));
        }

        //------------------------------------------------------
        // メッシュとマテリアルを関連付けしてインデックス番号取得
        //------------------------------------------------------
        for(auto& mesh : meshes_) {
            // Material番号
            auto it = std::find_if(std::begin(materials_), std::end(materials_), [&](auto& x) { return x.fbxMaterial_ == mesh.fbxMaterial_; });
            if(it == std::end(materials_)) {
                continue;
            }
            mesh.materialIndex_ = static_cast<s32>(std::distance(std::begin(materials_), it));
        }
    }

    //------------------------------------------------------
    // アニメーションの収集
    //------------------------------------------------------
    if(flags & ImportFbx::FLAG_IMPORT_ANIMATION) {
        printf_s("Start Import animation - %s\n", path.data());
        gatherAnimations(fbxScene);
        buildAnimations(fbxScene);
    }

    buildSkeletons();   // スケルトン階層情報の構築
    buildBones();       // ボーンの作成

    if(flags & ImportFbx::FLAG_IMPORT_GEOMETRY) {
        buildMeshes();    // 頂点データーの構築
        loadTextures();   // テクスチャの読み込み
    }

    //----------------------------------------------------------
    // FBXシーンの解放
    //----------------------------------------------------------
    fbxScene->destroy();

    return true;
}

//---------------------------------------------------------------------------
//! モデルにコンバート
//---------------------------------------------------------------------------
std::shared_ptr<ModelImpl> ImportFbxImpl::convertToModel() const
{
    std::shared_ptr<ModelImpl> p = std::make_shared<ModelImpl>();
    if(!p)
        return nullptr;

    p->geometryVertices_ = geometryVertices_;   // 衝突判定用頂点配列
    p->geometryIndices_  = geometryIndices_;    // 衝突判定三角形インデックス情報

    for(u32 i = 0; i < meshes_.size(); ++i) {
        p->meshes_.push_back(meshes_[i]);
    }

    for(u32 i = 0; i < materials_.size(); ++i) {
        p->materials_.push_back(materials_[i]);
    }

    p->animation_ = std::make_shared<AnimationImpl>();
    for(u32 i = 0; i < animationCount(); ++i) {
        std::string name = "layer" + std::to_string(i);
        p->animation_->appendLayer(name.c_str(), convertToAnimation(i));
    }

    for(u32 i = 0; i < bones_.size(); ++i) {
        p->bones_.push_back(bones_[i]);
    }

    p->bufferVb_    = bufferVb_;
    p->bufferIb_    = bufferIb_;
    p->boneHashMap_ = boneHashMap_;

    return p;
}

//---------------------------------------------------------------------------
//! アニメーションにコンバート
//! @param  [in]    index   アニメーション番号
//---------------------------------------------------------------------------
std::shared_ptr<AnimationLayerImpl> ImportFbxImpl::convertToAnimation(u32 index) const
{
    std::shared_ptr<AnimationLayerImpl> p = std::make_shared<AnimationLayerImpl>();
    if(!p)
        return nullptr;

    p->animationLength_ = animations_[index].animationLength_;
    p->curvesT_         = animations_[index].curvesT_;
    p->curvesR_         = animations_[index].curvesR_;

    return p;
}

//---------------------------------------------------------------------------
//! FBXファイルに埋め込まれているデーターを書き出し
//! @param  [in]    fbxScene        FBXシーンオブジェクト
//! @param  [in]    srcDirectory    書き出すフォルダパス
//---------------------------------------------------------------------------
void ImportFbxImpl::extractEmbedded(const ofbx::IScene* fbxScene, std::string_view srcDirectory)
{
    for(s32 i = 0; i < fbxScene->getEmbeddedDataCount(); ++i) {
        //-------------------------------------------------------
        // 埋め込みファイル名を取得
        //-------------------------------------------------------
        // 出力先をsrcDirectoryからの相対パスに加工
        ofbx::DataView embeddedFileName = fbxScene->getEmbeddedFilename(i);
        char           filePath[MAX_PATH];
        embeddedFileName.toString(filePath);

        // ディレクトリ + ファイルパス
        std::filesystem::path path = srcDirectory;
        path /= std::filesystem::path(filePath).filename();
        path = path.lexically_normal();   // 正規化

        //-------------------------------------------------------
        // ファイルを書き出し
        //-------------------------------------------------------
        const ofbx::DataView embedded = fbxScene->getEmbeddedData(i);
        std::ofstream        file(path.c_str(), std::ios::out | std::ios::binary | std::ios::trunc);   // truncを指定すると空のファイルとして生成
        if(!file.is_open()) {
            continue;
        }

        auto image     = embedded.begin + 4;                  // イメージ先頭
        auto imageSize = embedded.end - embedded.begin - 4;   // イメージサイズ

        // 書き込み
        file.write(reinterpret_cast<const char*>(image), imageSize);
        file.close();
    }
}

//---------------------------------------------------------------------------
// ジオメトリを収集
//! @param  [in]    fbxScene    FBXシーンオブジェクト
//---------------------------------------------------------------------------
void ImportFbxImpl::gatherGeometries(const ofbx::IScene* fbxScene)
{
    for(s32 i = 0; i < fbxScene->getGeometryCount(); ++i) {
        ImportGeometry g;
        g.fbxGeometry_ = fbxScene->getGeometry(i);

        geometries_.emplace_back(std::move(g));
    }
}

//---------------------------------------------------------------------------
// メッシュを収集
//! @param  [in]    fbxScene    FBXシーンオブジェクト
//---------------------------------------------------------------------------
void ImportFbxImpl::gatherMeshes(const ofbx::IScene* fbxScene)
{
    for(s32 i = 0; i < fbxScene->getMeshCount(); ++i) {
        const ofbx::Mesh* fbxMesh       = fbxScene->getMesh(i);          // FBXメッシュ
        const s32         materialCount = fbxMesh->getMaterialCount();   // マテリアル数

        for(s32 j = 0; j < materialCount; ++j) {
            ImportMesh mesh;

            //--------------------------------------------------
            // スキニングあり/なしのチェック
            //--------------------------------------------------
            mesh.isSkinned_ = false;
            if(fbxMesh->getGeometry() && fbxMesh->getGeometry()->getSkin()) {
                const ofbx::Skin* skin = fbxMesh->getGeometry()->getSkin();
                for(s32 n = 0; n < skin->getClusterCount(); ++n) {
                    const ofbx::Cluster* fbxCluster = skin->getCluster(n);

                    if(fbxCluster->getIndicesCount() > 0) {
                        mesh.isSkinned_ = true;   // スキニングあり
                        break;
                    }
                }
            }
            mesh.fbxMesh_     = fbxMesh;
            mesh.fbxMaterial_ = fbxMesh->getMaterial(j);
            mesh.submesh_     = materialCount > 1 ? j : -1;

            meshes_.emplace_back(std::move(mesh));
        }
    }
}

//---------------------------------------------------------------------------
//! テクスチャファイルの存在を検出
//! @param  [in]    sourceDirectory     検索のルートになるディレクトリ
//! @param  [in]    ext                 テクスチャの拡張子
//! @param  [in]    texture             ファイルパスを格納するテクスチャ
//---------------------------------------------------------------------------
bool findTexture(std::string_view sourceDirectory, std::string_view ext, ImportTexture* importTexture)
{
    //----------------------------------------------------------
    // テクスチャのファイルパスを生成
    // sourceDirectoryの直下を調べる
    //----------------------------------------------------------
    {
        std::filesystem::path path(sourceDirectory);
        path /= std::filesystem::path(importTexture->path_).stem();
        path = path.replace_extension(ext);

        // ファイルの存在チェック
        if(std::filesystem::exists(path)) {
            importTexture->path_    = std::move(path.string());
            importTexture->isExist_ = true;
            return true;
        }
    }

    // ファイルが存在しなかった場合filePathの指定通りの場所調べる
    {
        std::filesystem::path path(sourceDirectory);
        path /= importTexture->path_;
        path = path.replace_extension(ext);
        if(std::filesystem::exists(path)) {
            importTexture->path_    = std::move(path.string());
            importTexture->isExist_ = true;
            return true;
        }
    }

    // 発見できなかった
    return false;
}

//---------------------------------------------------------------------------
// マテリアルを収集
//! @param  [in]    sourceDirectory 親ディレクトリ
//---------------------------------------------------------------------------
void ImportFbxImpl::gatherMaterials(std::string_view sourceDirectory)
{
    for(auto& mesh : meshes_) {
        const ofbx::Material* fbxMaterial = mesh.fbxMaterial_;

        // マテリアルがなければスキップ
        if(!fbxMaterial)
            continue;

        ImportMaterial material;
        material.fbxMaterial_ = fbxMaterial;

        //------------------------------------------------------
        // テクスチャを収集
        //------------------------------------------------------
        auto gatherTexture = [&](ofbx::Texture::TextureType type) {
            // 種類指定でテクスチャオブジェクトを取得
            const ofbx::Texture* fbxTexture = material.fbxMaterial_->getTexture(type);
            if(!fbxTexture)
                return;

            std::shared_ptr<ImportTexture> importTexture = std::make_shared<ImportTexture>();
            importTexture->fbxTexture_                   = fbxTexture;
            ofbx::DataView fileName                      = fbxTexture->getRelativeFileName();

            if(fileName == "")
                fileName = fbxTexture->getFileName();

            //--------------------------------------------------
            // テクスチャのファイルパスを取得
            //--------------------------------------------------
            char filePath[MAX_PATH];

            fileName.toString(filePath);               // ファイル名を配列に取得
            importTexture->originalPath_ = filePath;   // FBXオリジナルのパスを保存
            std::filesystem::path path(filePath);
            path = path.lexically_normal();   // 正規化

            // ファイルの存在をチェック
            importTexture->path_    = path.string();
            importTexture->isExist_ = std::filesystem::exists(importTexture->path_);

            // ファイルが見つからなかった場合は検索
            std::string fileExt = path.extension().string();   // ファイル拡張子

            if(!importTexture->isExist_ && !findTexture(sourceDirectory, fileExt, importTexture.get())) {
                constexpr char* exts[] = { "dds", "png", "jpg", "jpeg", "tga", "bmp" };

                // それでも見つからなかったら拡張子を変更しながら検索
                for(auto* ext : exts) {
                    if(findTexture(sourceDirectory, ext, importTexture.get()))
                        break;
                }
            }

            material.textures_[type] = std::move(importTexture);
        };

        gatherTexture(ofbx::Texture::DIFFUSE);      // ディフューズ
        gatherTexture(ofbx::Texture::NORMAL);       // 法線
        gatherTexture(ofbx::Texture::SPECULAR);     // スペキュラー
        gatherTexture(ofbx::Texture::SHININESS);    // シャイニネス
        gatherTexture(ofbx::Texture::AMBIENT);      // アンビエント
        gatherTexture(ofbx::Texture::EMISSIVE);     // エミッシブ発光
        gatherTexture(ofbx::Texture::REFLECTION);   // 反射率

        // ディフューズカラー
        {
            auto fbxColor          = fbxMaterial->getDiffuseColor();
            material.diffuseColor_ = float3(fbxColor.r, fbxColor.g, fbxColor.b);
        }
        // スペキュラーカラー
        {
            auto fbxColor           = fbxMaterial->getSpecularColor();
            material.specularColor_ = float3(fbxColor.r, fbxColor.g, fbxColor.b);
        }
        // エミッシブカラー
        {
            auto fbxColor           = fbxMaterial->getEmissiveColor();
            material.emissiveColor_ = float3(fbxColor.r, fbxColor.g, fbxColor.b);
        }

        materials_.emplace_back(std::move(material));
    }
}

//---------------------------------------------------------------------------
//! ボーンのソート
//! @param  [in]    forceSkinned    強制スキニング
//---------------------------------------------------------------------------
void ImportFbxImpl::sortBones(bool forceSkinned)
{
    //----------------------------------------------------------
    // 親のボーンが手前に来るようにソート
    // これによって順方向に配列を処理することで親子関係が計算できるようになる
    //----------------------------------------------------------
    for(s32 i = 0; i < fbxBones_.size(); ++i) {
        for(s32 j = i + 1; j < fbxBones_.size(); ++j) {
            if(fbxBones_[i]->getParent() == fbxBones_[j]) {
                const ofbx::Object* bone = fbxBones_[j];

                fbxBones_[j] = fbxBones_.back();
                fbxBones_.pop_back();
                fbxBones_.insert(fbxBones_.begin() + i, bone);
                --i;
                break;
            }
        }
    }

    if(forceSkinned) {
        for(ImportMesh& mesh : meshes_) {
            // メッシュのボーン番号を求めて設定
            auto it = std::find(std::begin(fbxBones_), std::end(fbxBones_), mesh.fbxMesh_);
            assert(it != std::end(fbxBones_));
            mesh.boneIndex_ = static_cast<s32>(std::distance(std::begin(fbxBones_), it));

            mesh.isSkinned_ = true;
        }
    }
}

//---------------------------------------------------------------------------
//! ボーンの収集
//! @note ここでは有効なFBXボーンを収集する
//---------------------------------------------------------------------------
void ImportFbxImpl::gatherBones(const ofbx::IScene* fbxScene, bool forceSkinned)
{
    // ボーンノードの階層を辿り影響する親ボーンも含めて登録
    auto insertHierarchy = [&](const ofbx::Object* node) {
        while(node) {
            fbxBones_.push_back(node);
            node = node->getParent();
        }
    };

    //----------------------------------------------------------
    // メッシュからボーン検出
    //----------------------------------------------------------
    for(auto& mesh : meshes_) {
        auto* fbxGeometry = mesh.fbxMesh_->getGeometry();
        if(!fbxGeometry)
            continue;

        const ofbx::Skin* fbxSkin = fbxGeometry->getSkin();
        if(fbxSkin) {
            for(s32 i = 0; i < fbxSkin->getClusterCount(); ++i) {
                const ofbx::Cluster* cluster = fbxSkin->getCluster(i);
                insertHierarchy(cluster->getLink());
            }
        }

        //　メッシュをボーンにする
        if(forceSkinned) {
            insertHierarchy(mesh.fbxMesh_);
        }
    }

    //----------------------------------------------------------
    // アニメーションStackからボーンを検出
    //----------------------------------------------------------
    if constexpr(false) {
        for(s32 i = 0; i < fbxScene->getAnimationStackCount(); ++i) {
            const ofbx::AnimationStack* stack = fbxScene->getAnimationStack(i);
            for(s32 j = 0; stack->getLayer(j); ++j) {
                const ofbx::AnimationLayer* layer = stack->getLayer(j);
                for(s32 k = 0; layer->getCurveNode(k); ++k) {
                    const ofbx::AnimationCurveNode* node = layer->getCurveNode(k);

                    if(node->getBone()) {
                        insertHierarchy(node->getBone());
                    }
                }
            }
        }
    }
    //----------------------------------------------------------
    // ボーンの重複削除
    //----------------------------------------------------------
    {
        std::sort(std::begin(fbxBones_), std::end(fbxBones_));
        decltype(fbxBones_)::iterator result = std::unique(std::begin(fbxBones_), std::end(fbxBones_));

        // 不要になった要素を削除
        fbxBones_.erase(result, std::end(fbxBones_));
    }

    //----------------------------------------------------------
    // ボーンのソート
    //----------------------------------------------------------
    sortBones(forceSkinned);
}

//---------------------------------------------------------------------------
//! アニメーションの収集
//---------------------------------------------------------------------------
void ImportFbxImpl::gatherAnimations(const ofbx::IScene* fbxScene)
{
    //----------------------------------------------------------
    // アニメーションスタックからTake情報を取得
    //----------------------------------------------------------
    for(s32 i = 0; i < fbxScene->getAnimationStackCount(); ++i) {
        ImportAnimation animation;
        animation.fbxRootBoneId_     = fbxRootBoneId_;
        animation.fbxScene_          = fbxScene;                         // FBXシーン
        animation.fbxAnimationStack_ = fbxScene->getAnimationStack(i);   // FBXアニメーションスタック

        const ofbx::TakeInfo* fbxTakeInfo = fbxScene->getTakeInfo(animation.fbxAnimationStack_->name);
        if(fbxTakeInfo) {
            // 名前
            if(fbxTakeInfo->name.begin != fbxTakeInfo->name.end) {
                char name[1024];
                // 名前が見つかった場合
                fbxTakeInfo->name.toString(name);
                animation.name_ = name;
            }

            // 名前がなかった場合はファイル名から取得
            if(animation.name_.empty()) {
                if(fbxTakeInfo->filename.begin != fbxTakeInfo->filename.end) {
                    char fileName[MAX_PATH];
                    fbxTakeInfo->filename.toString(fileName);

                    std::filesystem::path filePath(fileName);
                    animation.name_ = std::move(filePath.stem().string());   // 拡張子を除去したファイル名
                }
            }
            // それでも空だった場合は
            if(animation.name_.empty()) {
                animation.name_ = "animation";   // 仮の名前を設定
            }
        }
        // 登録
        animations_.emplace_back(std::move(animation));
    }
}

//---------------------------------------------------------------------------
//! アニメーション情報の作成
//---------------------------------------------------------------------------
bool ImportFbxImpl::buildAnimations(const ofbx::IScene* fbxScene)
{
    bool result = true;
    for(auto& animation : animations_) {
        if(!animation.build(fbxScene, fbxBones_, fbxScale_)) {
            result = false;
        }
    }

    //----------------------------------------------------------
    // キー情報が無い無効なアニメーションを削除
    //----------------------------------------------------------
    {
        auto hasNoCurveKey = [](auto& animation) {
            auto allKeys = animation.allKeys_;

            // 全てのボーン用キー列を検索して全て空かチェック
            for(auto& keys : allKeys) {
                if(keys.size() > 0) {
                    return false;
                }
            }
            return true;
        };

        // C++20ではerase_ifに置換可能
        auto it = std::remove_if(std::begin(animations_), std::end(animations_), hasNoCurveKey);
        animations_.erase(it, std::end(animations_));
    }

    return result;
}

//---------------------------------------------------------------------------
// ベースポーズを取得
//---------------------------------------------------------------------------
ofbx::Matrix getBindPoseMatrix(const ImportMesh* mesh, const ofbx::Object* node)
{
    if(!mesh)
        return node->getGlobalTransform();

    if(!mesh->fbxMesh_) {
        // 単位行列 (double型)
        return { 1.0, 0.0, 0.0, 0.0,
                 0.0, 1.0, 0.0, 0.0,
                 0.0, 0.0, 1.0, 0.0,
                 0.0, 0.0, 0.0, 1.0 };
    }

    auto* fbxSkin = mesh->fbxMesh_->getGeometry()->getSkin();
    if(!fbxSkin)
        return node->getGlobalTransform();

    for(s32 c = 0; c < fbxSkin->getClusterCount(); ++c) {
        const ofbx::Cluster* cluster = fbxSkin->getCluster(c);
        if(cluster->getLink() == node) {
            return cluster->getTransformLinkMatrix();
        }
    }
    return node->getGlobalTransform();
}

//---------------------------------------------------------------------------
// スケルトン階層情報の構築
//---------------------------------------------------------------------------
void ImportFbxImpl::buildSkeletons()
{
    auto boneCount = fbxBones_.size();   // ■ボーンの個数

    bones_.reserve(boneCount);

    for(s32 i = 0; i < boneCount; ++i) {
        auto& fbxBone = fbxBones_[i];

        std::string name = fbxBone->name;   // ■名前

        ofbx::Object* parent      = fbxBone->getParent();
        s32           parentIndex = -1;   // ■親のインデックス
        if(parent) {
            auto it     = std::find(std::begin(fbxBones_), std::end(fbxBones_), parent);
            parentIndex = static_cast<s32>(std::distance(std::begin(fbxBones_), it));
        }

        // ボーン情報から関連付けされているボーン取得
        auto getAnyMeshFromBone = [&](const ofbx::Object* node, s32 boneIndex) -> ImportMesh* {
            for(auto& mesh : meshes_) {
                const ofbx::Mesh* fbxMesh = mesh.fbxMesh_;
                if(mesh.boneIndex_ == boneIndex) {
                    return &mesh;
                }

                auto* fbxSkin = fbxMesh->getGeometry()->getSkin();
                if(!fbxSkin)
                    continue;

                for(s32 c = 0; c < fbxSkin->getClusterCount(); ++c) {
                    if(fbxSkin->getCluster(c)->getLink() == node)
                        return &mesh;
                }
            }
            return nullptr;
        };
        const ImportMesh* mesh = getAnyMeshFromBone(fbxBone, i);

        //---------------------------------------------------
        // ベースのポーズ行列から姿勢を構築
        //---------------------------------------------------
        matrix transform = cast(getBindPoseMatrix(mesh, fbxBone));

        // スケールを正規化
        float3 scale(1.0f / length(float3(transform._11, transform._21, transform._31)),
                     1.0f / length(float3(transform._12, transform._22, transform._32)),
                     1.0f / length(float3(transform._13, transform._23, transform._33)));

        transform._11_12_13 *= scale;
        transform._21_22_23 *= scale;
        transform._31_32_33 *= scale;

        quaternion q = fixOrientation(math::makeRotation(transform));
        float3     t = fixOrientation(math::makeTranslation(transform));

        // ボーンを書き込み
        Bone bone;
        bone.name_        = name;          // 名前
        bone.index_       = i;             // ボーン番号
        bone.parentIndex_ = parentIndex;   // 親のボーン番号

        bone.transform_.position_ = t * config.meshScale_ * fbxScale_;
        bone.transform_.rotation_ = q;

        bones_.emplace_back(std::move(bone));
    }

    //------------------------------------------------------
    // ボーンのハッシュマップを生成
    // ボーン名からボーン情報を逆引きする場合の高速化
    //------------------------------------------------------
    for(auto& bone : bones_) {
        u32 hash = static_cast<u32>(std::hash<std::string>()(bone.name_));
        assert(boneHashMap_.find(hash) == std::end(boneHashMap_));
        boneHashMap_[hash] = bone.index_;
    }
}

//---------------------------------------------------------------------------
// ボーンの構築
//---------------------------------------------------------------------------
bool ImportFbxImpl::buildBones()
{
    //----------------------------------------------------------
    // ルートではない最初のボーン番号を計算
    //----------------------------------------------------------
    s32 firstNonRootBoneIndex = -1;
    for(s32 i = 0; i < bones_.size(); ++i) {
        Bone& bone = bones_[i];
        if(bone.parentIndex_ < 0) {
            // 親に接続されていないボーン(ルートボーン)が複数あった場合エラー
            if(firstNonRootBoneIndex != -1) {
                return false;
            }
            bone.parentIndex_ = -1;
        }
        else {
            // 親が自分よりも後ろにあったらエラー
            // sortBones()で親が前になるように並び変えている筈のデーターが不正な場合のチェック
            if(bone.parentIndex_ > i) {
                return false;
            }
            if(firstNonRootBoneIndex == -1)
                firstNonRootBoneIndex = i;
        }
    }

    //----------------------------------------------------------
    // ボーンを原点に移動させる行列を作成
    //----------------------------------------------------------
    for(auto& bone : bones_) {
        bone.transformInverse_ = bone.transform_.inverse();   // 位置の逆行列で原点に移動させることができる
    }

    //----------------------------------------------------------
    // 親からの相対変換行列を作成
    //----------------------------------------------------------
    for(s32 i = 0; i < bones_.size(); ++i) {
        s32 parent = bones_[i].parentIndex_;
        if(parent >= 0) {
            bones_[i].transformRelative_ = bones_[parent].transformInverse_ * bones_[i].transform_;
        }
        else {
            bones_[i].transformRelative_ = bones_[i].transform_;
        }
    }

    return true;
}

//---------------------------------------------------------------------------
// スキン情報を計算
//! @param  [out]   skins   出力するスキン情報
//! @param  [out]   mesh    メッシュ
//---------------------------------------------------------------------------
void ImportFbxImpl::writeSkinInfo(std::vector<ImportSkin>& skins, const ImportMesh& mesh)
{
    const ofbx::Mesh*     fbxMesh     = mesh.fbxMesh_;
    const ofbx::Geometry* fbxGeometry = fbxMesh->getGeometry();

    skins.resize(fbxGeometry->getVertexCount());
    memset(&skins[0], 0, skins.size() * sizeof(skins[0]));

    //----------------------------------------------------------
    // スキニング情報がない場合はメッシュのボーン番号100%で設定
    //----------------------------------------------------------
    auto* fbxSkin = fbxGeometry->getSkin();
    if(!fbxSkin) {
        assert(mesh.boneIndex_ >= 0);
        skins.resize(fbxGeometry->getIndexCount());
        for(auto& s : skins) {
            s.count_ = 1;

            s.jointWeights_[0].first = static_cast<u16>(mesh.boneIndex_);
            s.jointWeights_[1].first = static_cast<u16>(mesh.boneIndex_);
            s.jointWeights_[2].first = static_cast<u16>(mesh.boneIndex_);
            s.jointWeights_[3].first = static_cast<u16>(mesh.boneIndex_);

            s.jointWeights_[0].second = 1.0f;
            s.jointWeights_[1].second = 0.0f;
            s.jointWeights_[2].second = 0.0f;
            s.jointWeights_[3].second = 0.0f;
        }
        return;
    }

    //----------------------------------------------------------
    // スキニングありの場合
    //----------------------------------------------------------
    for(s32 i = 0; i < fbxSkin->getClusterCount(); ++i) {
        const ofbx::Cluster* fbxCluster = fbxSkin->getCluster(i);

        // ジオメトリ情報が無ければスキップ
        if(fbxCluster->getIndicesCount() == 0)
            continue;

        // クラスターから関節番号を逆引き
        u16 joint = static_cast<u16>(-1);
        {
            auto it = std::find(std::begin(fbxBones_), std::end(fbxBones_), fbxCluster->getLink());
            if(it != std::end(fbxBones_)) {
                joint = static_cast<u16>(std::distance(std::begin(fbxBones_), it));
            }
        }

        //------------------------------------------------------
        // スキニング頂点情報を作成
        //------------------------------------------------------
        const s32* indices = fbxCluster->getIndices();
        const f64* weights = fbxCluster->getWeights();
        for(s32 j = 0; j < fbxCluster->getIndicesCount(); ++j) {
            const s32 index  = indices[j];
            const f32 weight = static_cast<f32>(weights[j]);

            auto& s = skins[index];
            if(s.count_ < std::size(s.jointWeights_)) {
                // 影響する関節番号を4個まで登録
                s.jointWeights_[s.count_].first  = joint;
                s.jointWeights_[s.count_].second = weight;
                ++s.count_;
            }
            else {
                // 4-weight以降はウェイトで影響度が最も少ない要素を上書き
                auto it  = std::min_element(std::begin(s.jointWeights_), std::end(s.jointWeights_),
                                           [](const auto& a, const auto& b) {
                                               return a.second < b.second;
                                           });
                auto min = std::distance(std::begin(s.jointWeights_), it);

                if(s.jointWeights_[min].second < weight) {
                    s.jointWeights_[min].first  = joint;
                    s.jointWeights_[min].second = weight;
                }
            }
        }
    }

    //----------------------------------------------------------
    // ウェイト値の正規化(合計で100%になるように)
    //----------------------------------------------------------
    for(auto& s : skins) {
        // 合計値
        f32 sum = std::accumulate(std::begin(s.jointWeights_), std::end(s.jointWeights_), 0.0f,
                                  [](f32 acc, const auto& x) {
                                      return acc + x.second;
                                  });
        for(auto& w : s.jointWeights_) {
            w.second /= sum;   // 正規化
        }

        // ウェイト順にソート
        std::sort(std::begin(s.jointWeights_), std::end(s.jointWeights_), [](const auto& a, const auto& b) { return a.second > b.second; });

        assert(s.jointWeights_[0].second >= s.jointWeights_[1].second);
        assert(s.jointWeights_[1].second >= s.jointWeights_[2].second);
        assert(s.jointWeights_[2].second >= s.jointWeights_[3].second);
    }
}

//---------------------------------------------------------------------------
//! 頂点メッシュデーターの作成
//---------------------------------------------------------------------------
void ImportFbxImpl::buildMeshes()
{
    //----------------------------------------------------------
    // 頂点の不足要素生成と最適化
    // ※現時点では未実装
    //----------------------------------------------------------
    for(s32 geometryIndex = 0; geometryIndex < geometries_.size(); ++geometryIndex) {
        ImportGeometry&                    geometry    = geometries_[geometryIndex];
        const ofbx::Geometry*              fbxGeometry = geometry.fbxGeometry_;
        const s32                          vertexCount = fbxGeometry->getVertexCount();   // 頂点数
        [[maybe_unused]] const ofbx::Vec3* vertices    = fbxGeometry->getVertices();      // 頂点
        [[maybe_unused]] const ofbx::Vec3* normals     = fbxGeometry->getNormals();       // 法線
        [[maybe_unused]] const ofbx::Vec3* tangents    = fbxGeometry->getTangents();      // 接線
        [[maybe_unused]] const ofbx::Vec4* colors      = fbxGeometry->getColors();        // カラー
        [[maybe_unused]] const ofbx::Vec2* uvs         = fbxGeometry->getUVs();           // テクスチャUV

        // 頂点インデックス
        [[maybe_unused]] auto  indexCount = fbxGeometry->getIndexCount();    //!< インデックス数
        [[maybe_unused]] auto* fbxIndices = fbxGeometry->getFaceIndices();   //!< インデックス配列

        // 法線の生成

        // 接線の生成
    }

    //----------------------------------------------------------
    // 頂点データーの構築
    //----------------------------------------------------------
    auto buildVertex = [&](u32 meshIndex) {
        auto&                 mesh        = meshes_[meshIndex];
        const ofbx::Mesh*     fbxMesh     = mesh.fbxMesh_;
        const ofbx::Geometry* fbxGeometry = mesh.fbxMesh_->getGeometry();
        const ofbx::Material* fbxMaterial = mesh.fbxMaterial_;
        const ImportGeometry* geometry    = nullptr;

        const ImportMaterial& material = materials_[mesh.materialIndex_];

        // fbxGeometryからImportGeometryを逆引き
        {
            auto it = std::find_if(std::begin(geometries_), std::end(geometries_), [&](const ImportGeometry& x) { return x.fbxGeometry_ == fbxGeometry; });
            if(it != std::end(geometries_)) {
                geometry = static_cast<const ImportGeometry*>(&(*it));
            }
        }

        const ofbx::Vec3* vertices = fbxGeometry->getVertices();   // 頂点
        const ofbx::Vec3* normals  = fbxGeometry->getNormals();    // 法線
        const ofbx::Vec3* tangents = fbxGeometry->getTangents();   // 接線
        const ofbx::Vec4* colors   = fbxGeometry->getColors();     // カラー
        const ofbx::Vec2* uvs      = fbxGeometry->getUVs();        // テクスチャUV

        // 法線がない場合は自動計算したnormalを設定
        if(!normals)
            normals = geometry->computedNormals_.data();

        // 自動計算したtangentがある場合は置換
        if(!tangents && geometry->computedTangents_.size()) {
            tangents = geometry->computedTangents_.data();
        }

        //------------------------------------------------------
        // 行列を取得
        //------------------------------------------------------
        matrix geometicMatrix  = cast(fbxMesh->getGeometricMatrix());
        matrix transformMatrix = mul(geometicMatrix, cast(fbxMesh->getGlobalTransform()));

        //------------------------------------------------------
        // スキンウェイト情報を取得
        //------------------------------------------------------
        std::vector<ImportSkin> skins;
        if(mesh.isSkinned_)
            writeSkinInfo(skins, mesh);

        //------------------------------------------------------
        // FBXのマテリアル番号を取得
        //------------------------------------------------------
        s32 fbxMaterialIndex = -1;
        {
            for(s32 i = 0; i < fbxMesh->getMaterialCount(); ++i) {
                if(fbxMesh->getMaterial(i) != fbxMaterial)
                    continue;
                fbxMaterialIndex = i;

                break;
            }
        }

        //------------------------------------------------------
        // 頂点バッファ作成
        //------------------------------------------------------
        auto* geometryMaterials = fbxGeometry->getMaterials();   // マテリアル番号(三角形個数)

        for(s32 i = 0; i < fbxGeometry->getVertexCount(); ++i) {
            ModelVertex v{};

            // 該当マテリアルのみ抽出してそれ以外はスキップ
            if(geometryMaterials && geometryMaterials[i / 3] != fbxMaterialIndex)
                continue;

            // 頂点の書き込み
            {
                // 座標にワールド行列を事前処理で適用
                float3 position = cast(vertices[i]);

                position = mul(float4(position, 1.0f), transformMatrix).xyz;

                // 座標系とスケールを適用
                position *= config.meshScale_ * fbxScale_;
                position    = fixOrientation(position);
                v.position_ = DirectX::XMFLOAT3(position.x, position.y, position.z);
            }

            // 法線
            if(normals) {
                float3 n = cast(normals[i]);
                n        = mul(float4(n, 0.0f), transformMatrix).xyz;

                // 座標系補正
                n = fixOrientation(n);

                v.normal_ = DirectX::XMFLOAT3(n.x, n.y, n.z);
            }

            // テクスチャUV座標
            if(uvs) {
                // テクスチャUV座標はOpenGL系(左下が原点、上方向がV+)のためVを反転
                auto& uv = uvs[i];
                v.uv_    = DirectX::XMFLOAT2(static_cast<f32>(uv.x), 1.0f - static_cast<f32>(uv.y));
            }

            // カラー
            if(colors) {   // カラーがある場合はそのまま使用
                auto r   = static_cast<u8>(colors[i].x * 255.5);
                auto g   = static_cast<u8>(colors[i].y * 255.5);
                auto b   = static_cast<u8>(colors[i].z * 255.5);
                auto a   = static_cast<u8>(colors[i].w * 255.5);
                v.color_ = Color(r, g, b, a);
            }
            else if(material.textures_[Texture::DIFFUSE]) {   // カラーがない状態でテクスチャがある場合は白
                v.color_ = Color(255, 255, 255, 255);
            }
            else {   // フラットな場合はマテリアルカラー
                auto r   = static_cast<u8>(material.diffuseColor_.r * 255.5f);
                auto g   = static_cast<u8>(material.diffuseColor_.g * 255.5f);
                auto b   = static_cast<u8>(material.diffuseColor_.b * 255.5f);
                auto a   = static_cast<u8>(255);
                v.color_ = Color(r, g, b, a);
            }

            // 接線
            if(tangents) {
                float3 n = cast(tangents[i]);
                n        = mul(float4(n, 0.0f), transformMatrix).xyz;

                // 座標系補正
                n = fixOrientation(n);

                v.tangent_ = DirectX::XMFLOAT3(n.x, n.y, n.z);
            }
            else if(uvs){
                // ３点で面を構成なので, この3点の接線は一緒
                static float3 tangent = float3(0.0f, 0.0f, 0.0f);
                //static float3 bitangent = float3(0.0f, 0.0f, 0.0f);
                if(i % 3 / 3.0f == 0.0f) {
                    s32 index = i;
                    // Shortcuts for vertices
                    float3 vertex1 = cast(vertices[index]);
                    float3 vertex2 = cast(vertices[index + 1]);
                    float3 vertex3 = cast(vertices[index + 2]);

                    // Shortcuts for UVs
                    float2 uv1 = { uvs[index].x, uvs[index].y };
                    float2 uv2 = { uvs[index + 1].x, uvs[index + 1].y };
                    float2 uv3 = { uvs[index + 2].x, uvs[index + 2].y };

                    // Edges of the triangle : position delta
                    float3 deltaPos1 = vertex2 - vertex1;
                    float3 deltaPos2 = vertex3 - vertex1;

                    // UV delta
                    float2 deltaUV1 = uv2 - uv1;
                    float2 deltaUV2 = uv3 - uv1;

                    float den = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
                    tangent   = (deltaPos1 * deltaUV2.y - deltaPos2 * deltaUV1.y) * den;
                    //bitangent = (deltaPos2 * deltaUV1.x - deltaPos1 * deltaUV2.x) * den;
                    {
                        float3 n = tangent;
                        n        = mul(float4(n, 0.0f), transformMatrix).xyz;

                        // 座標系補正
                        n       = fixOrientation(n);
                        tangent = n;
                    }
                }
                // Set the same tangent for all three vertices of the triangle.
                v.tangent_ = DirectX::XMFLOAT3(tangent.x, tangent.y, tangent.z);
            }

            // スキニング インデックス＆ウェイト
            if(!skins.empty()) {
                v.skinIndex_.x = skins[i].jointWeights_[0].first;
                v.skinIndex_.y = skins[i].jointWeights_[1].first;
                v.skinIndex_.z = skins[i].jointWeights_[2].first;
                v.skinIndex_.w = skins[i].jointWeights_[3].first;

                v.skinWeight_.x = skins[i].jointWeights_[0].second;
                v.skinWeight_.y = skins[i].jointWeights_[1].second;
                v.skinWeight_.z = skins[i].jointWeights_[2].second;
                v.skinWeight_.w = skins[i].jointWeights_[3].second;
            }
            else {
                v.skinWeight_ = DirectX::XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);   // 単一スキニング 100%
            }

            // メッシュの頂点配列に登録
            // この後頂点バッファにこれをコピー
            mesh.vertices_.emplace_back(std::move(v));
        }

        //------------------------------------------------------
        // 縮退三角形 (一辺の長さが0になり面積が0になった三角形)の除去
        //------------------------------------------------------
        for(u32 i = 0; i < mesh.vertices_.size(); i += 3) {
            const auto& v0 = mesh.vertices_[i + 0].position_;
            const auto& v1 = mesh.vertices_[i + 1].position_;
            const auto& v2 = mesh.vertices_[i + 2].position_;

            float3 p0(v0.x, v0.y, v0.z);
            float3 p1(v1.x, v1.y, v1.z);
            float3 p2(v2.x, v2.y, v2.z);

            // 外積で面積を計算。完全に0の場合のみ除去。
            float3 c = cross(p1 - p0, p2 - p0);
            if(dot(c, c).x == 0.0f) {
                // 縮退していた場合は該当する3つの頂点を削除
                mesh.vertices_.erase(std::begin(mesh.vertices_) + i,
                                     std::begin(mesh.vertices_) + i + 3);
                // ループカウンタを同じ場所をもう一度処理するように戻す
                i -= 3;
            }
        }

        //------------------------------------------------------
        // メッシュを最適化
        //------------------------------------------------------

        // 頂点バッファ
        {
            size_t stride = sizeof(ModelVertex);
            // 最適化
            MeshOptimizer optimizer;
            optimizer.optimize(mesh.vertices_.data(), static_cast<u32>(mesh.vertices_.size()), stride, offsetof(ModelVertex, position_));

            // 最適化済み頂点を受け取る
            auto& optVertices = optimizer.optimizedVertices();
            auto& optIndices  = optimizer.optimizedIndices();

            u32 vertexCount = static_cast<u32>(optVertices.size() / stride);

            mesh.vertices_.clear();
            mesh.vertices_.reserve(vertexCount);   // 配列数を予約

            for(u32 i = 0; i < vertexCount; ++i) {
                auto* p = reinterpret_cast<const ModelVertex*>(optVertices.data() + stride * i);

                mesh.vertices_.push_back(*p);
            }
            mesh.indices_ = optIndices;
        }

        // 衝突判定用メッシュ
        {
            size_t stride = sizeof(float3);

            // 衝突判定用頂点を登録
            for(auto& i : mesh.indices_) {
                auto& v = mesh.vertices_[i].position_;

                float3 position(v.x, v.y, v.z);
                mesh.geometryVertices_.emplace_back(std::move(position));
            }

            // 最適化
            MeshOptimizer optimizer;
            optimizer.optimize(mesh.geometryVertices_.data(), static_cast<u32>(mesh.geometryVertices_.size()), stride, 0);

            // 最適化済み頂点を受け取る
            auto& optVertices = optimizer.optimizedVertices();
            auto& optIndices  = optimizer.optimizedIndices();

            u32 vertexCount = static_cast<u32>(optVertices.size() / stride);

            mesh.geometryVertices_.clear();
            mesh.geometryVertices_.reserve(vertexCount);   // 配列数を予約

            for(u32 i = 0; i < vertexCount; ++i) {
                auto* p = reinterpret_cast<const f32*>(optVertices.data() + stride * i);

                float3 position(p[0], p[1], p[2]);
                mesh.geometryVertices_.push_back(position);
            }
            mesh.geometryIndices_ = optIndices;
        }
    };

    // ワーカースレッドに全メッシュを登録
    for(auto i = 0; i < meshes_.size(); ++i) {
        threadPool()->submit([&, i] { buildVertex(i); });
    }

    // ワーカースレッド実行完了待ち
    threadPool()->wait();

    //----------------------------------------------------------
    // 頂点バッファ作成
    //----------------------------------------------------------
    {
        s32 startVertexLocation = 0;   // 頂点の開始位置

        u32 totalVertexCount = std::accumulate(std::begin(meshes_), std::end(meshes_), 0,
                                               [](u32 acc, const auto& mesh) {
                                                   return acc + static_cast<u32>(mesh.vertices_.size());
                                               });

        // 頂点数分メモリを確保
        std::vector<ModelVertex> vertices(totalVertexCount);   // ローカルの頂点ワーク

        for(auto& mesh : meshes_) {
            auto vertexCount          = static_cast<s32>(mesh.vertices_.size());   // 頂点数
            mesh.vertexCount_         = vertexCount;                               // 頂点数
            mesh.startVertexLocation_ = startVertexLocation;                       // 頂点の開始位置

            // イメージをコピー
            memcpy_s(&vertices[startVertexLocation],
                     sizeof(ModelVertex) * vertexCount,
                     mesh.vertices_.data(),
                     sizeof(ModelVertex) * vertexCount);

            startVertexLocation += vertexCount;   // 頂点の開始位置
        }

        bufferVb_ = dx11::createBuffer({ sizeof(ModelVertex) * vertices.size(), D3D11_BIND_VERTEX_BUFFER }, vertices.data());
    }

    //----------------------------------------------------------
    // インデックスバッファ作成
    //----------------------------------------------------------
    {
        s32 startIndexLocation = 0;   // インデックスの開始位置

        u32 totalIndexCount = std::accumulate(std::begin(meshes_), std::end(meshes_), 0,
                                              [](u32 acc, const auto& mesh) {
                                                  return acc + static_cast<u32>(mesh.indices_.size());
                                              });

        // インデックス数分メモリを確保
        std::vector<u32> indices(totalIndexCount);   // ローカルのインデックスワーク

        for(auto& mesh : meshes_) {
            auto indexCount          = static_cast<s32>(mesh.indices_.size());   // インデックス数
            mesh.indexCount_         = indexCount;                               // インデックス数
            mesh.startIndexLocation_ = startIndexLocation;                       // インデックスの開始位置

            // イメージをコピー
            for(s32 i = 0; i < indexCount; ++i) {
                indices[startIndexLocation + i] = mesh.indices_[i] + mesh.startVertexLocation_;   // インデックス番号の開始番号
            }
            startIndexLocation += indexCount;   // 頂点の開始開始位置
        }

        bufferIb_ = dx11::createBuffer({ sizeof(u32) * indices.size(), D3D11_BIND_INDEX_BUFFER }, indices.data());
    }

    // 衝突判定用頂点を追加しながらコピー(追記していく)
    {
        u32 indexOffset = 0;
        for(auto& mesh : meshes_) {
            // 頂点
            for(auto& x : mesh.geometryVertices_) {
                geometryVertices_.push_back(x);
            }

            // インデックス
            for(auto& x : mesh.geometryIndices_) {
                geometryIndices_.push_back(x + indexOffset);
            }
            // 追記するとインデックスが指す先もずれるため、登録数のぶんずらず
            indexOffset += static_cast<u32>(mesh.geometryVertices_.size());
        }
    }

    //----------------------------------------------------------
    // 頂点データーが無い無効なメッシュを削除
    //----------------------------------------------------------
    // C++20ではerase_ifに置換可能
    auto it = std::remove_if(meshes_.begin(), meshes_.end(), [](auto& x) { return x.vertexCount_ == 0; });
    meshes_.erase(it, meshes_.end());
}

//---------------------------------------------------------------------------
// テクスチャの読み込み
//---------------------------------------------------------------------------
bool ImportFbxImpl::loadTextures()
{
    bool result = true;

    for(auto& material : materials_) {
        for(auto& t : material.textures_) {
            if(!t)
                continue;

            if(!t->load()) {
                result = false;
            }
        }
    }

    return result;
}

//---------------------------------------------------------------------------
// FBXインポーターを作成
//---------------------------------------------------------------------------
std::unique_ptr<importer::ImportFbx> createImportFbx(std::string_view path, f32 scale, u32 flags, s32 rootBoneId)
{
    auto p = std::make_unique<importer::ImportFbxImpl>();
    if(p && !p->load(path, scale, flags, rootBoneId)) {
        p.reset();
    }
    return p;
}

}   // namespace importer
