﻿//---------------------------------------------------------------------------
//!	@file	importer_fbx.h
//!	@brief	FBXインポーター
//---------------------------------------------------------------------------
#pragma once

namespace importer {

//===========================================================================
//! FBXインポーター
//===========================================================================
class ImportFbx
{
public:
    enum FLAGS
    {
        FLAG_IMPORT_GEOMETRY  = (1ul << 0),   //!< ジオメトリ情報をインポート
        FLAG_IMPORT_ANIMATION = (1ul << 1),   //!< アニメーション情報をインポート
    };

    virtual ~ImportFbx() = default;

    //! モデルにコンバート
    virtual std::shared_ptr<ModelImpl> convertToModel() const = 0;

    //! アニメーションにコンバート
    //! @param  [in]    index   アニメーション番号
    virtual std::shared_ptr<AnimationLayerImpl> convertToAnimation(u32 index) const = 0;

    //! アニメーションの数を取得
    virtual size_t animationCount() const = 0;
};

// FBXインポーターを作成
//! @param  [in]    path    FBXファイルパス
//! @param  [in]    scale   全体スケール(default:1.0f)
//! @param  [in]    flags   オプション設定(ImportFbx::FLAGSの組み合わせ)
[[nodiscard]] std::unique_ptr<importer::ImportFbx> createImportFbx(std::string_view path,
                                                                   f32              scale      = 1.0f,
                                                                   u32              flags      = ImportFbx::FLAG_IMPORT_GEOMETRY | ImportFbx::FLAG_IMPORT_ANIMATION,
                                                                   s32              rootBoneId = -1);

}   // namespace importer
