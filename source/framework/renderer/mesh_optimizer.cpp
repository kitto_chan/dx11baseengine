﻿//---------------------------------------------------------------------------
//!	@file	mesh_optimizer.cpp
//!	@brief	メッシュ最適化
//---------------------------------------------------------------------------
#include "mesh_optimizer.h"
#include "impl/model_impl.h"

// meshoptimizer
#include <meshoptimizer/src/meshoptimizer.h>

//---------------------------------------------------------------------------
//! 最適化
//---------------------------------------------------------------------------
bool MeshOptimizer::optimize(const void* vertices, u32 vertexCount, size_t vertexStride, u32 positionOffset)
{
    //! 頂点がなければ処理しない
    if(vertexCount == 0)
        return false;

    // 三角形として3の倍数の個数でなければエラー
    if(vertexCount % 3 != 0)
        return false;

    //----------------------------------------------------------
    // [meshoptimizer] 共有頂点を検出しインデックスを振り直す
    //----------------------------------------------------------
    u32 indexCount = vertexCount;   // インデックスが無いため

    std::vector<u32> remap(vertexCount);   // 振り直したインデックス番号

    // 頂点の共有頂点をマージしてインデックス列を生成
    size_t totalVertices = meshopt_generateVertexRemap(remap.data(), nullptr, indexCount, vertices, vertexCount, vertexStride);

    // 振り直したインデックスを生成
    optIndices_.resize(indexCount);
    meshopt_remapIndexBuffer(optIndices_.data(), nullptr, indexCount, remap.data());

    // 頂点を前方に詰め直して生成
    optVertices_.resize(vertexStride * totalVertices);
    meshopt_remapVertexBuffer(optVertices_.data(), vertices, indexCount, vertexStride, remap.data());

    //----------------------------------------------------------
    // [meshoptimizer] 三角形のリダクション
    //----------------------------------------------------------
    {
        f32 targetError      = 0.01f;            // 誤差範囲
        u32 targetIndexCount = indexCount / 2;   // 減らした後の頂点数

        std::vector<u32> simplifiedIndex(indexCount);
        auto             resultSize = meshopt_simplify(simplifiedIndex.data(),
                                           optIndices_.data(),
                                           optIndices_.size(),
                                           reinterpret_cast<const float*>(optVertices_.data()),
                                           totalVertices,
                                           vertexStride,
                                           targetIndexCount, targetError, &targetError);

        optIndices_ = simplifiedIndex;
        indexCount  = static_cast<u32>(resultSize);
    }

    //----------------------------------------------------------
    // [meshoptimizer] 高度な最適化
    //----------------------------------------------------------

    // 頂点キャッシュ最適化。頂点キャッシュで隣接頂点を再利用できるように近い位置に並び替え
    // 頂点キャッシュの最適化はオーバードローの開始順序を提供するため、最初に行う。
    meshopt_optimizeVertexCache(optIndices_.data(), optIndices_.data(), optIndices_.size(), totalVertices);

    // オーバードロー最適化でインデックス並び替え（オーバードローと頂点キャッシュの効率性のバランス）
    {
        constexpr f32 threshold = 1.01f;   // 平均キャッシュミス率(ACMR)を1%まで悪化させて、オーバードローのためのリオーダリングの機会を増やす。
        meshopt_optimizeOverdraw(optIndices_.data(), optIndices_.data(), optIndices_.size(), reinterpret_cast<const f32*>(optVertices_.data() + positionOffset), totalVertices, vertexStride, threshold);
    }

    // 頂点フェッチ最適化。頂点取得の最適化は最終的なインデックスの順番に依存するため最後に行う
    meshopt_optimizeVertexFetch(optVertices_.data(), optIndices_.data(), optIndices_.size(), optVertices_.data(), totalVertices, vertexStride);

    //----------------------------------------------------------
    // [meshoptimizer] 最適化結果を分析
    //----------------------------------------------------------
    if constexpr(false) {
        constexpr size_t cacheSize = 16;   // 頂点キャッシュサイズ

        meshopt_VertexCacheStatistics vcs = meshopt_analyzeVertexCache(optIndices_.data(), optIndices_.size(), totalVertices, cacheSize, 0, 0);
        meshopt_VertexFetchStatistics vfs = meshopt_analyzeVertexFetch(optIndices_.data(), optIndices_.size(), totalVertices, vertexStride);
        meshopt_OverdrawStatistics    os  = meshopt_analyzeOverdraw(optIndices_.data(), optIndices_.size(), reinterpret_cast<const f32*>(optVertices_.data() + positionOffset), totalVertices, vertexStride);

        meshopt_VertexCacheStatistics vcsNVIDIA = meshopt_analyzeVertexCache(optIndices_.data(), optIndices_.size(), totalVertices, 32, 32, 32);    // NVIDIA GeForce  計測結果
        meshopt_VertexCacheStatistics vcsAMD    = meshopt_analyzeVertexCache(optIndices_.data(), optIndices_.size(), totalVertices, 14, 64, 128);   // AMD    Radeon   計測結果
        meshopt_VertexCacheStatistics vcsIntel  = meshopt_analyzeVertexCache(optIndices_.data(), optIndices_.size(), totalVertices, 128, 0, 0);     // Intel  Graphics 計測結果
    }
    return true;
}
