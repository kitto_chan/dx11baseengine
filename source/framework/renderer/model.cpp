﻿//---------------------------------------------------------------------------
//!	@file	model.cpp
//!	@brief	3Dモデル
//---------------------------------------------------------------------------
#include "model.h"
#include "impl/model_impl.h"
#include "impl/animation_impl.h"
#include "impl/animation_layer_impl.h"
#include "import_fbx.h"

#include <iostream>
#include <fstream>
#include <filesystem>

//===========================================================================
// ポーズ  Pose
//===========================================================================

//---------------------------------------------------------------------------
//! 絶対位置を計算
//---------------------------------------------------------------------------
void Pose::computeAbsolute(const ModelImpl& model)
{
    for(s32 i = 0; i < size(); ++i) {
        s32 parent = model.bone(i)->parentIndex_;

        if(parent >= 0) {
            positions_[i] = rotations_[parent] * positions_[i] + positions_[parent];
            rotations_[i] = rotations_[parent] * rotations_[i];
        }
        else {
            positions_[i] = positions_[i];
            rotations_[i] = rotations_[i];
        }
    }
}

//---------------------------------------------------------------------------
//! 相対位置を計算
//---------------------------------------------------------------------------
void Pose::computeRelative(const ModelImpl& model)
{
    for(s32 i = static_cast<s32>(size() - 1); i >= 0; --i) {
        s32 parent = model.bone(i)->parentIndex_;

        if(parent >= 0) {
            positions_[i] = conjugate(rotations_[parent]) * (positions_[i] - positions_[parent]);
            rotations_[i] = conjugate(rotations_[parent]) * rotations_[i];
        }
        else {
            positions_[i] = positions_[i];
            rotations_[i] = rotations_[i];
        }
    }
}

//---------------------------------------------------------------------------
//! ポーズ同士を補間
//---------------------------------------------------------------------------
void Pose::blend(Pose& rhs, f32 weight)
{
    assert(size() == rhs.size());

    // ウェイトが小さければ無視
    if(weight <= 0.001f)
        return;

    weight = std::clamp(weight, 0.0f, 1.0f);
    for(int i = 0; i < size(); ++i) {
        positions_[i] = lerp(positions_[i], rhs.positions_[i], weight);
        rotations_[i] = nlerp(rotations_[i], rhs.rotations_[i], weight);
    }
}

namespace {

//--------------------------------------------------------------
// 定数バッファ
//--------------------------------------------------------------

// カラー定数バッファ
struct ColorCB
{
    float4 meshColor_;   //< メッシュの色
};

// モデル用定数バッファ
struct ModelCB
{
    matrix matJoints_[ModelImpl::JOINT_COUNT_MAX];   // 関節行列
};
// アウトライン描画用
struct OutlineCB
{
    DirectX::XMFLOAT3 outlineColor;
    f32               outlineWidth;
};
// Phongマテリアル
struct PhongMaterialCB
{
    DirectX::XMFLOAT4 _ambient;
    DirectX::XMFLOAT4 _diffuse;
    DirectX::XMFLOAT3 _specular;
    f32               _specularShininess;
};
struct TextureMappingFlag
{
    int hasNormalMap = 0;
    int useless[3];
};

}   // namespace

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void ModelImpl::update()
{
    if(bones_.size() > 0) {
        //------------------------------------------------------
        // アニメーション行列の更新
        //------------------------------------------------------
        // デフォルト値として相対位置を各ボーンに設定
        auto& positions = pose_.positions_;
        auto& rotations = pose_.rotations_;
        for(s32 i = 0; i < bones_.size(); ++i) {
            positions[i] = bones_[i].transformRelative_.position_;
            rotations[i] = bones_[i].transformRelative_.rotation_;
        }

        // アニメーションからポーズ情報を取得
        if(animation_) {
            animation_->buildPose(pose_, this);
        }

        // 親子関係を適用して計算
        pose_.computeAbsolute(*this);

        // モデル描画用の行列を作成
        for(u32 i = 0; i < pose_.size(); ++i) {
            auto&          bone = bones_[i];
            LocalTransform tmp  = { pose_.positions_[i], (pose_.rotations_[i]) };
            matrices_[i]        = (tmp * bone.transformInverse_).makeMatrix();
        }
    }
}

//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void ModelImpl::render()
{
    //----------------------------------------------------------
    // ワールド行列の反映
    //----------------------------------------------------------
    dx11::setConstantBuffer("WorldCB", matWorld_);   // GPUへ変更を反映

    //----------------------------------------------------------
    // 行列を定数バッファに設定
    //----------------------------------------------------------
    ModelCB modelCb;
    s32     jointCount = static_cast<s32>(bones_.size());
    if(jointCount) {
        // スキニングアニメーションの場合
        for(s32 i = 0; i < jointCount; ++i) {
            modelCb.matJoints_[i] = matrices_[i];
        }
    }
    else {
        // 単体モデルの場合
        modelCb.matJoints_[0] = math::identity();
    }

    //----------------------------------------------------------
    // モデル描画
    //----------------------------------------------------------
    // 設定を退避
    auto inputLayoutLast = dx11::getInputLayout();

    dx11::setDepthStencilState(dx11::commonStates().DepthDefault());

    // ジオメトリ情報
    dx11::setInputLayout(inputLayout_);                         // 入力レイアウト
    dx11::setVertexBuffer(0, bufferVb_, sizeof(ModelVertex));   // 頂点バッファ
    dx11::setIndexBuffer(bufferIb_);                            // インデックスバッファ

    // シェーダー定数
    dx11::setConstantBuffer("ModelCB", modelCb);

    // 半透明ON
    dx11::setBlendState(dx11::commonStates().NonPremultiplied());

    for(auto& mesh : meshes_) {
        //--------------------------------------------------
        // テクスチャ取得
        //--------------------------------------------------
        static shr_ptr<dx11::Texture> whiteTexture = dx11::createTextureFromFile("textures/color/white2x2.jpg");

        TextureMappingFlag textureMappingFlag;

        dx11::Texture* texture       = nullptr;
        dx11::Texture* textureNormal = nullptr;
        dx11::Texture* textureSpec   = nullptr;
        if(mesh.materialIndex_ != -1) {
            auto& diffuseTexture = materials_[mesh.materialIndex_].textures_[Texture::DIFFUSE];
            texture              = (diffuseTexture) ? diffuseTexture->texture_.get() : nullptr;

            auto& matTexSpec = materials_[mesh.materialIndex_].textures_[Texture::SPECULAR];
            textureSpec      = (matTexSpec) ? matTexSpec->texture_.get() : whiteTexture.get();

            auto& matTextureNormal = materials_[mesh.materialIndex_].textures_[Texture::NORMAL];
            if(matTextureNormal) {
                textureMappingFlag.hasNormalMap = 1;
                textureNormal                   = matTextureNormal->texture_.get();
            }
            else {
                textureMappingFlag.hasNormalMap = 0;
                textureNormal                   = nullptr;
            }
        }

        //--------------------------------------------------
        // 描画
        //--------------------------------------------------
        if(_isRenderOutLine) {
            static constexpr OutlineCB outLineCB{
                { 0.0f, 1.0f, 0.0f },
                0.005f
            };

            dx11::setRasterizerState(dx11::commonStates().CullCounterClockwise());
            dx11::vs::setShader(shaderVs_Model_Outline_);
            dx11::ps::setShader(shaderPs_Flat_);
            dx11::setConstantBuffer("OutlineCB", outLineCB);
            dx11::drawIndexed(dx11::Primitive::TriangleList, mesh.indexCount_, mesh.startIndexLocation_, 0);
            dx11::setRasterizerState(dx11::commonStates().CullNone());
        }

        // シェーダー
        dx11::vs::setShader(shaderVs_Model_4Skin_);   // VS 頂点シェーダー
        if(_isPBRModel) {
            dx11::ps::setShader(shaderPs_PBRTexture_);   // PS ピクセルシェーダー
            auto& pbrTextures = materials_[mesh.materialIndex_].pbrTextures_;
            dx11::ps::setTexture(21, pbrTextures[Model::PBRTextureType::Normal]->texture_);
            dx11::ps::setTexture(22, pbrTextures[Model::PBRTextureType::Metalness]->texture_);
            dx11::ps::setTexture(23, pbrTextures[Model::PBRTextureType::Roughness]->texture_);
            dx11::ps::setTexture(0, texture);
        }
        else {
            static shr_ptr<dx11::Texture> noise = dx11::createTextureFromFile("textures/shaders/noise.jpg");

            PhongMaterialCB phongMaterialCB{
                math::CastToXMFLOAT4(_phongMaterial._ambient),
                math::CastToXMFLOAT4(_phongMaterial._diffuse),
                math::CastToXMFLOAT3(_phongMaterial._specular),
                _phongMaterial._specularShininess
            };

            dx11::setConstantBuffer("PhongMaterialCB", phongMaterialCB);
            dx11::setConstantBuffer("TextureMappingFlagCB", textureMappingFlag);
            dx11::ps::setShader(shaderPs_Texture_);   // PS ピクセルシェーダー
            // テクスチャ

            auto& pbrTextures = materials_[mesh.materialIndex_].pbrTextures_;
            if(!textureNormal && pbrTextures[Model::PBRTextureType::Normal]) {
                textureNormal = pbrTextures[Model::PBRTextureType::Normal]->texture_.get();
            }

            dx11::ps::setTexture(0, texture);
            dx11::ps::setTexture(1, textureNormal);
            dx11::ps::setTexture(2, textureSpec);
            dx11::ps::setTexture(3, noise);
        }
        // サンプラー
        dx11::ps::setSamplerState(0, dx11::commonStates().LinearWrap());

        dx11::setConstantBuffer("ColorCB", ColorCB{ float4(1.0f, 1.0f, 1.0f, 1.0f) });
        // 描画発行
        dx11::drawIndexed(dx11::Primitive::TriangleList, mesh.indexCount_, mesh.startIndexLocation_, 0);
    }
    // 元に戻す
    //dx11::setInputLayout(inputLayoutLast);
    dx11::setBlendState(dx11::commonStates().Opaque());   // 半透明OFF
}

//---------------------------------------------------------------------------
//! デバッグ描画
//---------------------------------------------------------------------------
void ModelImpl::renderDebug()
{
    if(bones_.size() > 0) {
        // シェーダー
        dx11::vs::setShader(shaderVs_3D_);     // VS 頂点シェーダー
        dx11::ps::setShader(shaderPs_Flat_);   // PS ピクセルシェーダー

        dx11::setDepthStencilState(dx11::commonStates().DepthNone());   // デプスなし

        // デプスOFF
        u32 flags = 0;   // デバッグ描画フラグ(debug::DRAW_FLAGの組み合わせ)

        // Debug
        //{
        //    static s32  id      = 0;
        //    const ImS32 s32_one = 1;
        //    ImGui::Begin("ModelMatrix");
        //    ImGui::InputScalar("input s32", ImGuiDataType_S32, &id, &s32_one, NULL, "%d");
        //    ImGui::DragInt("m", &id);
        //    ImGui::End();
        //    auto mat = mul(bones_[id].transform_.makeMatrix(), matrices_[id]);
        //    debug::drawMatrix(mul(mat, matWorld_), 0.1f, flags);
        //}

        for(u32 i = 0; i < bones_.size(); ++i) {
            //-------------------------------------------------
            // 関節行列の描画
            //-------------------------------------------------
            auto m = mul(bones_[i].transform_.makeMatrix(), matrices_[i]);

            debug::drawMatrix(mul(m, matWorld_), 0.1f, flags);

            //-------------------------------------------------
            // ボーンの描画
            //-------------------------------------------------
            auto parent = bones_[i].parentIndex_;   // 親のボーン番号
            if(parent >= 0) {
                auto p0 = mul(float4(bones_[parent].transform_.position_, 1.0f), matrices_[parent]).xyz;
                auto p1 = mul(float4(bones_[i].transform_.position_, 1.0f), matrices_[i]).xyz;

                // ワールド座標へ座標変換
                p0 = mul(float4(p0, 1.0f), matWorld_).xyz;
                p1 = mul(float4(p1, 1.0f), matWorld_).xyz;

                debug::drawBone(p0, p1, Color(0, 255, 255), flags);
            }
        }

        dx11::setDepthStencilState(dx11::commonStates().DepthDefault());   // 元に戻す
    }
    //static s32  idd      = 0;
    //const ImS32 s32_one = 1;
    //ImGui::Begin("ModelMatrix");
    //ImGui::InputScalar("input ss32", ImGuiDataType_S32, &idd, &s32_one, NULL, "%d");
    //ImGui::DragInt("m", &idd);
    //ImGui::End();
    //debug::drawSphere(geometryVertices_[idd], 0.02f, Color(1.0f, 0.0f, 0.0f, 1.0f));
    //debug::drawMatrix(mul(math::translate(geometryVertices_[idd]), matWorld_), 0.1f, 0);
}

//---------------------------------------------------------------------------
//! アニメーションを設定
//---------------------------------------------------------------------------
void ModelImpl::bindAnimation(std::shared_ptr<Animation> animation)
{
    animation_ = std::dynamic_pointer_cast<AnimationImpl>(animation);
}

//---------------------------------------------------------------------------
//! ワールド行列を設定
//---------------------------------------------------------------------------
void ModelImpl::setWorldMatrix(const matrix& matWorld)
{
    matWorld_ = matWorld;
}

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool ModelImpl::initialize()
{
    pose_ = std::move(Pose(bones_.size()));

    //----------------------------------------------------------
    // 入力レイアウトの作成
    //----------------------------------------------------------
    // clang-format off
    static constexpr D3D11_INPUT_ELEMENT_DESC layout[]{
        // セマンテック名(任意)                ストリームスロット番号(0-15)
        // ↓  セマンテック番号(0～7)                    ↓    構造体の先頭からのオフセットアドレス(先頭からnバイト目)
        // ↓        , ↓,         データ形式          , ↓,    ↓                       , 頂点読み込みの更新周期       , 更新間隔
        { "POSITION"    , 0, DXGI_FORMAT_R32G32B32_FLOAT   , 0, offsetof(ModelVertex, position_)  , D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "COLOR"       , 0, DXGI_FORMAT_R8G8B8A8_UNORM    , 0, offsetof(ModelVertex, color_)     , D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "TEXCOORD"    , 0, DXGI_FORMAT_R32G32_FLOAT      , 0, offsetof(ModelVertex, uv_)        , D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "NORMAL"      , 0, DXGI_FORMAT_R32G32B32_FLOAT   , 0, offsetof(ModelVertex, normal_)    , D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "TANGENT"     , 0, DXGI_FORMAT_R32G32B32_FLOAT   , 0, offsetof(ModelVertex, tangent_)   , D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "BLENDINDICES", 0, DXGI_FORMAT_R32G32B32A32_SINT , 0, offsetof(ModelVertex, skinIndex_) , D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "BLENDWEIGHT" , 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, offsetof(ModelVertex, skinWeight_), D3D11_INPUT_PER_VERTEX_DATA, 0 },
    };
    // clang-format on

    inputLayout_ = dx11::createInputLayout(layout, std::size(layout));

    //-------------------------------------------------------------
    // シェーダーをコンパイル
    //-------------------------------------------------------------
    shaderVs_3D_          = dx11::createShader("dx11/vs_3d.fx", "main", "vs_5_0");            // 頂点シェーダー
    shaderVs_Model_4Skin_ = dx11::createShader("dx11/vs_model_4skin.fx", "main", "vs_5_0");   // 頂点シェーダー
    shaderPs_Flat_        = dx11::createShader("dx11/ps_flat.fx", "main", "ps_5_0");          // ピクセルシェーダー
    //shaderPs_Texture_     = dx11::createShader("dx11/ps_model.fx", "main", "ps_5_0");            // ピクセルシェーダー
    shaderVs_Model_Outline_ = dx11::createShader("gameSource/ModelOutline_VS.fx", "main", "vs_5_0");   // 頂点シェーダー
    shaderPs_Texture_       = dx11::createShader("gameSource/PhongModel_PS.fx", "main", "ps_5_0");     // ピクセルシェーダー
    shaderPs_PBRTexture_    = dx11::createShader("gameSource/PBRModel_PS.fx", "main", "ps_5_0");       // ピクセルシェーダー
    if(!shaderVs_3D_ || !shaderVs_Model_4Skin_ || !shaderPs_Flat_ || !shaderPs_Texture_ || !shaderPs_PBRTexture_ || !shaderVs_Model_Outline_)
        return false;

    return true;
}
//---------------------------------------------------------------------------
// !外部（別の）DiffuseTexture設定する、fbx Fileのテクスチャ載ってない(かも)
//---------------------------------------------------------------------------
void ModelImpl::loadExternalDiffuseTexture(std::string_view texturePath)
{
    for(auto& mesh : meshes_) {
        shr_ptr<Texture> modelTexture = std::make_shared<Texture>();
        modelTexture->path_           = texturePath;

        if(modelTexture->load()) {
            materials_[mesh.materialIndex_].textures_[Texture::DIFFUSE] = std::move(modelTexture);
        }
    }
}
//---------------------------------------------------------------------------
// PBRテクスチャーを設定
//---------------------------------------------------------------------------
bool ModelImpl::SetPBRTexture(PBRTextureType type, std::string_view path)
{
    for(auto& mesh : meshes_) {
        shr_ptr<PBRTexture> modelTexture = std::make_shared<PBRTexture>();
        modelTexture->path_              = path;

        if(modelTexture->load()) {
            materials_[mesh.materialIndex_].pbrTextures_[type] = std::move(modelTexture);
            return true;
        }
    }
    return false;
}
//---------------------------------------------------------------------------
//! モデルなかに特定の頂点行列を取得
//---------------------------------------------------------------------------
matrix ModelImpl::GetGeometryVertexMatrix(s32 index)
{
    if(index < 0 || index >= geometryIndices_.size()) {
        ASSERT_MESSAGE(false, "Out of Index");
        return math::identity();
    }
    return mul(math::translate(geometryVertices_[index]), matWorld_);
}
//---------------------------------------------------------------------------
//! Phongのマテリアル設定
//---------------------------------------------------------------------------
void ModelImpl::SetPhongMaterial(const cb::PhongMaterial& phongMaterial)
{
    _phongMaterial = phongMaterial;
}

//---------------------------------------------------------------------------
//! モデルの読み込み
//---------------------------------------------------------------------------
std::shared_ptr<Model> createModel(const char* path, f32 scale)
{
    std::unique_ptr<importer::ImportFbx> importFbx = importer::createImportFbx(path, scale, importer::ImportFbx::FLAG_IMPORT_GEOMETRY);

    if(!importFbx) {
        return nullptr;
    }

    // クラスを初期化
    std::shared_ptr<ModelImpl> p = importFbx->convertToModel();

    if(p && !p->initialize()) {
        p.reset();
    }

    return p;
}
