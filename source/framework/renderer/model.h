﻿//---------------------------------------------------------------------------
//!	@file	model.h
//!	@brief	3Dモデル
//---------------------------------------------------------------------------
#pragma once

//===========================================================================
//! 3Dモデル
//===========================================================================
class Model
{
public:
    enum PBRTextureType
    {
        Normal,             //!< 法線
        AmbientOcclusion,   //!< AO MAP
        Roughness,          //!< ラフネス (Roughness / Glossiness / Shininess )
        Metalness,          //!< 金属/非金属の値
        EMISSIVE,           //!< エミッシブ発光
        //----
        COUNT,
    };

    virtual ~Model() = default;

    //! 更新
    virtual void update() = 0;

    //! 描画
    virtual void render() = 0;

    //! デバッグ描画
    virtual void renderDebug() = 0;

    //! アニメーションを設定
    virtual void bindAnimation(std::shared_ptr<Animation> animation) = 0;

    //! ワールド行列を設定
    virtual void setWorldMatrix(const matrix& matWorld) = 0;

    //! 外部（別の）DiffuseTexture設定する、fbx Fileのテクスチャ載ってない(かも)
    virtual void loadExternalDiffuseTexture(std::string_view texturePath) = 0;

    // PBRテクスチャーを設定
    virtual bool SetPBRTexture(PBRTextureType type, std::string_view path) = 0;

    //! 特定のボーンの行列を取得
    virtual matrix GetBoneMatrix(s32 index) const = 0;

    //! モデルなかに特定の頂点行列を取得
    virtual matrix GetGeometryVertexMatrix(s32 index) = 0;

    virtual const std::vector<float3>* GetGeometryVertices() = 0;
    virtual const std::vector<u32>* GetGeometryIndices() = 0;

    //! PBRモデルかどうかのフラッグ設定
    virtual void SetIsPBRModel(bool isPBRModel) = 0;

    //! Phongモデルのマテリアル設定
    virtual void SetPhongMaterial(const cb::PhongMaterial& phongMaterial) = 0;

    //! モデルのアウトラインを描画
    virtual void SetIsRenderOutline(bool isRenderOutline) = 0;

    //----------------------------------------------------------
    //! @name   衝突判定用三角形リストを取得
    //----------------------------------------------------------
    //@{

    //! 頂点数を取得
    virtual u32 vertexCount() const = 0;

    //! 頂点配列を取得
    virtual const float3* vertices() const = 0;

    //! インデックス数を取得
    virtual u32 indexCount() const = 0;

    //! インデックス配列を取得
    virtual const u32* indices() const = 0;

    //@}
};

//! モデルの読み込み
//! @param  [in]    path    fbxファイルパス
//! @param  [in]    scale   全体スケール値
[[nodiscard]] std::shared_ptr<Model> createModel(const char* path, f32 scale = 1.0f);
