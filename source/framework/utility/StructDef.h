﻿//---------------------------------------------------------------------------
//!	@file	StructDef.h
//!	@brief	通用構造体定義
//---------------------------------------------------------------------------
#pragma once
namespace cb {
//! カメラ用定数バッファ
struct CameraCB
{
    matrix matView = math::identity();   //!< ビュー行列
    matrix matProj = math::identity();   //!< 投影行列

    float3 eyePos = float3(0.0f, 0.0f, 0.0f);   //!< カメラ座標
};

struct MeshColorCB
{
    float4 color = math::ONE;
};
// ワールド行列用定数バッファ
struct WorldCB
{
    matrix matWorld = math::identity();   //!< ワールド行列
};

struct InvWorldCB
{
    matrix invMatWorld = math::identity();   //!< ワールド行列
};

struct BlurCB
{
    f32 power = 1.0f;
    s32 pad[3];
};

struct DissolveCB
{
    float4 edgeColor    = math::ONE;
    f32    edgeWidth    = 0.0f;
    f32    edgeSoftness = 0.0f;
    f32    dissolve     = 0.0f;   //!< ディゾルブ値
    s32    pad;                   //!< 16bitようpadding1入れてとく
};

struct PhongMaterial
{
    float4 _ambient           = float4(1.0f, 1.0f, 1.0f, 1.0f);
    float4 _diffuse           = float4(1.0f, 1.0f, 1.0f, 1.0f);
    float3 _specular          = float3(1.0f, 1.0f, 1.0f);
    f32    _specularShininess = 128.f;   // w = SpecPower
};

// 霧
struct FogCB
{
    int               isUseFog = false;
    DirectX::XMFLOAT3 fogColor = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f);

    DirectX::XMFLOAT2 fogTexel  = DirectX::XMFLOAT2(0.0f, 0.0f);
    f32               minHeight = 0.0f;
    f32               maxHeight = 3.0f;
};

struct ProcessBarCB
{
    f32 fillAmount; //!< 0 ~ 1
    s32 useless[3];
};
}   // namespace cb
