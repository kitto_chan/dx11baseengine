﻿//---------------------------------------------------------------------------
//!	@file	SystemHelper.h
//!	@brief	システム関してのヘルパークラス
//---------------------------------------------------------------------------
#pragma once

// システム関して
namespace sys {
float2 GetWindowSize()
{
    const auto* backBuffer = dx11::swapChain()->backBuffer();
    f32         width      = static_cast<f32>(backBuffer->desc().width_);
    f32         height     = static_cast<f32>(backBuffer->desc().height_);

    return float2(width, height);
}
}   // namespace sys
