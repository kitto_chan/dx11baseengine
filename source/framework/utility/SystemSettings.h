﻿//---------------------------------------------------------------------------
//!	@file	SystemSettings.h
//!	@brief	ゲーム設定
//---------------------------------------------------------------------------
#pragma once
namespace manager {
using namespace sys;
class SystemSettings : public Singleton<SystemSettings>
{
public:
    SystemSettings()  = default;   //!< コンストラクタ
    ~SystemSettings() = default;   //!< デストラクタ

    //---------------------------------------------------------------------------
    // 関数
    //---------------------------------------------------------------------------

    //---------
    //! Imguimoのアクション
    ImGuizmoAction GetImGuizmoAction() const;                      //!< Imguimoのアクションを取得
    void           SetImGuizmoAction(const ImGuizmoAction& set);   //!< Imguimoのアクションを設定

    //---------
    // システムのモード
    SystemMode GetSysMode() const;     //! SystemModeを取得
    void       SwapMode();             //! SystemModeを切り替えする EditorMode <-> GameMode
    bool       IsEditorMode() const;   //!<  EditorModeかどうか

    //----------
    // システムのステート
    SystemState GetSystemState() const;   //!< システムステート
    void        SetSystemState(SystemState systemState) { _sysState = systemState; }
    void        SwapSysState();         //!< システムステートを切り替える Play <->Pause
    bool        IsPlayState() const;    //!<  再生ステートかどうか
    bool        IsPauseState() const;   //!<  ポーズステートかどうか

    //----------
    // ブレットエンジン関して
    bool IsPhyDebug() const;   //!< 物理デバッグ表示すかどうか
    void SwapPhyDebug();       //!< 物理デバッグ描画フラッグ true <-> false swap

    //----------
    // Imgui
    bool IsUseImguiDemo() const;   //!< _isUseImguiDemoフラッグ
    void SwapImguiDemoFlag();      //!< Imguiデモ表示フラッグ true <-> false swap

    //----------
    // Loading
    bool IsLoading() const { return _isLoading; }          //!< _isLoadingの状態を取得
    void SwapLoadingFlag() { _isLoading = !_isLoading; }   //!< _isLoadingのフラッグをスワップする  true <-> false swap
    void SetLoading(bool flag) { _isLoading = flag; }      //!< ローディングフラッグを設定

    //----------
    // HintText
    bool IsShowHintOverlay() const { return _isShowHintOverlay; }              //!< _showHintOverlayの状態を取得
    void SwapShowHintOverlay() { _isShowHintOverlay = !_isShowHintOverlay; }   //!< _showHintOverlayのフラッグをスワップする  true <-> false swap
    void SetShowHintOverlay(bool flag) { _isShowHintOverlay = flag; }          //!< ヒントを表示かどうかのフラッグを設定

    //----------
    // SceneViewportSize
    void   SetSceneViewportSize(const float2& size) { _sceneViewportSize = size; };   //!< SceneViewportSizeを設定
    float2 GetSceneViewportSize() { return _sceneViewportSize; };                     //!< SceneViewportSizeを取得

    void SetIsGameUpdate(bool isGameUpdate) { _isGameUpdate = isGameUpdate; };   //!< ゲーム更新フラグを設定
    bool GetIsGameUpdate() { return _isGameUpdate; };                            //!< ゲーム更新フラグを取得

    void QuitGame() { _isQuitGame = true; };    //!< ゲーム終了を設定
    bool IsQuitGame() { return _isQuitGame; }   //!< ゲーム終了を取得

private:
    //---------------------------------------------------------------------------
    //! 内部変数
    //---------------------------------------------------------------------------
    ImGuizmoAction _imguizmoAction = ImGuizmoAction::Transform;   //!< Imguimoのアクション
    SystemMode     _sysMode        = SystemMode::EditorMode;      //!< システムのモード
    SystemState    _sysState       = SystemState::Play;           //!< システムのステート

    float2 _sceneViewportSize = { 1440.f, 1080.f };   //!< Imguiシーンのビューポートのサイズ

    bool _isPhyDebug     = false;   //<! ブレットエンジンデバッグ描画
    bool _isUseImguiDemo = false;   //<! ImGuiDemo表示かどうか

    bool _isLoading = false;   //!< ゲームがローディングしているかどうか

    bool _isShowHintOverlay = false;   //!< ヒントを表示かどうか

    bool _isGameUpdate = false;   //!< ゲーム更新

    bool _isQuitGame = false;   //!< ゲーム終了
};

}   // namespace manager
manager::SystemSettings* SystemSettingsMgr();
