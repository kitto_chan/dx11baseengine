﻿//---------------------------------------------------------------------------
//!	@file	color.h
//!	@brief	カラー
//---------------------------------------------------------------------------
#pragma once

//===========================================================================
// カラー構造体
//===========================================================================
struct Color
{
public:
    Color() = default;

    //! 初期化
    Color(u8 r, u8 g, u8 b, u8 a = 255)
    : r_(r)
    , g_(g)
    , b_(b)
    , a_(a)
    {
    }

    //! 初期化
    Color(u32 color)
    : color_(color)
    {
    }

	    //! 初期化
    Color(float4 color4)
    : r_(static_cast<u8>(color4.f32[0] * 255.0f))
    , g_(static_cast<u8>(color4.f32[1] * 255.0f))
    , b_(static_cast<u8>(color4.f32[2] * 255.0f))
    , a_(static_cast<u8>(color4.f32[3] * 255.0f))
    {
    }

    //! コピーコンストラクタ
    Color(const Color& rhs) { color_ = rhs.color_; }

    //! moveコンストラクタ
    Color(Color&& lhs) { color_ = std::move(lhs.color_); }

    //! 代入
    Color& operator=(const Color& rhs)
    {
        color_ = rhs.color_;
        return *this;
    }

    //! move代入
    Color& operator=(Color&& lhs)
    {
        color_ = std::move(lhs.color_);
        return *this;
    }

	static float4 GetColorFromRGBA(const float4& rgba)
    {
        float4 color;
        color.f32[0] = rgba.f32[0] / 255.0f;
        color.f32[1] = rgba.f32[1] / 255.0f;
        color.f32[2] = rgba.f32[2] / 255.0f;
        color.f32[3] = rgba.f32[3] / 255.0f;
        return color;
    };

public:
    union
    {
        struct
        {
            u8 r_;   //!< Red   赤成分
            u8 g_;   //!< Green 緑成分
            u8 b_;   //!< Blue  青成分
            u8 a_;   //!< Alpha 透明度
        };
        u32 color_;   //!< 一括アクセス
    };
};
