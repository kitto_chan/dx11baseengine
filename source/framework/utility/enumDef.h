﻿//---------------------------------------------------------------------------
//!	@file	enumDef.h
//!	@brief	型定義
//---------------------------------------------------------------------------
#pragma once

// システム関して
namespace sys {
// モード
enum class SystemMode
{
    EditorMode,   //!< クリエイトモード（開発用）
    GameMode      //!< ゲームモード    　(クライアント側)
};

// システムステート
enum class SystemState
{
    Play,   //!< 更新を行う
    Pause,  //!< 更新しない
	Close   //!< クロス
};

// ImGuizmoのアクション
enum class ImGuizmoAction
{
    Transform,   //!< 変換座標
    Rotate,      //!< 回転
    Scale        //!< スケール
};

}   // namespace sys

// エンティティに関して
namespace entity {
constexpr const char* LayerName[] = {
    "None",
    "Default",
    "Environment",
    "Floor",
    "Wall",
    "Player",
    "Enemy",
    "PlayerWeapon",
    "EnemyWeapon"
};
//! レイヤー
enum Layer
{
    None         = 0,
    Default      = 1 << 1,
    Environment  = 1 << 2,
    Floor        = 1 << 3,
    Wall         = 1 << 4,
    Player       = 1 << 5,
    Enemy        = 1 << 6,
    PlayerWeapon = 1 << 7,
    EnemyWeapon  = 1 << 8
};
//! タグ
enum class Tag
{
    Untagged,
    Player,
    Enemey
};
}   // namespace entity

namespace state {
//! キャラのメインステート
enum class ECharacterState
{
    Idle,     //!< 待機
    Move,     //!< 移動
    Jump,     //!< ジャンプ
    Attack,   //!< 攻撃
    Hit,      //!< 攻撃を受けた
    Roll,     //!< 回避
    Death     //!< 死亡
};
}   // namespace state

namespace physics {
//!　当たり判定のフィルター
static s32 GetCollisionGroup(entity::Layer layer)
{
    {
        using Layer = entity::Layer;
        switch(layer) {
            case Layer::None:
                return 0;
                break;
            case Layer::Default:
                return -1;
            case Layer::Environment:
                return Layer::Enemy | Layer::Player;
            case Layer::Floor:
                return Layer::Enemy | Layer::Player;
            case Layer::Wall:
                return Layer::Enemy | Layer::Player;
            case Layer::Player:
                return Layer::Environment | Layer::Floor | Layer::Wall | Layer::Enemy | Layer::EnemyWeapon;
            case Layer::Enemy:
                return Layer::Environment | Layer::Floor | Layer::Wall | Layer::Player | Layer::Environment | Layer::PlayerWeapon;
            case Layer::PlayerWeapon:
                return Layer::Enemy;
            case Layer::EnemyWeapon:
                return Layer::Player;
            default:
                ASSERT_MESSAGE(false, "Layer not setted");
                return 0;
                break;
        }
    }
}
}   // namespace physics
