﻿//---------------------------------------------------------------------------
//!	@file	Timer.cpp
//!	@brief	タイムクラス
//! Introduction to 3D Game Programming With Directx 11 
//! この本を参考にしました
//---------------------------------------------------------------------------
#include "Timer.h"

namespace timer {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
Timer::Timer()
: _secondsPerCount(0.0)
, _deltaTime(-1.0)
, _baseTime(0)
, _stopTime(0)
, _pausedTime(0)
, _prevTime(0)
, _currTime(0)
, _stopped(false)
{
    u64 countsPerSec;
    QueryPerformanceFrequency((LARGE_INTEGER*)&countsPerSec);
    _secondsPerCount = 1.0 / (double)countsPerSec;
}

//---------------------------------------------------------------------------
//! 総プレイ時間を取得
//! 総プレイ時間をカウントすろ Reset()したら, 0に戻す
//! もしstop()したら、カウントもストップする
//---------------------------------------------------------------------------
f32 Timer::TotalTime() const
{
    // If we are stopped, do not count the time that has passed since we stopped.
    // Moreover, if we previously already had a pause, the distance
    // m_StopTime - m_BaseTime includes paused time, which we do not want to count.
    // To correct this, we can subtract the paused time from m_StopTime:
    //
    //                     |<--paused time-->|
    // ----*---------------*-----------------*------------*------------*------> time
    //  m_BaseTime       m_StopTime        startTime     m_StopTime    m_CurrTime

    if(_stopped) {
        return (f32)(((_stopTime - _pausedTime) - _baseTime) * _secondsPerCount);
    }
    else {
        // The distance m_CurrTime - m_BaseTime includes paused time,
        // which we do not want to count.  To correct this, we can subtract
        // the paused time from m_CurrTime:
        //
        //  (m_CurrTime - m_PausedTime) - m_BaseTime
        //
        //                     |<--paused time-->|
        // ----*---------------*-----------------*------------*------> time
        //  m_BaseTime       m_StopTime        startTime     m_CurrTime
        return (f32)(((_currTime - _pausedTime) - _baseTime) * _secondsPerCount);
    }
}
//---------------------------------------------------------------------------
//! 直前のフレームと今のフレーム間で経過した時間（秒）
//---------------------------------------------------------------------------
f32 Timer::DeltaTime() const
{
    return (f32)_deltaTime;
}

//---------------------------------------------------------------------------
//! リセット
//---------------------------------------------------------------------------
void Timer::Reset()
{
    s64 currTime;
    QueryPerformanceCounter((LARGE_INTEGER*)&currTime);
    _baseTime   = currTime;
    _prevTime   = currTime;
    _stopTime   = 0;
    _pausedTime = 0;
    _stopped    = false;
}
//---------------------------------------------------------------------------
//! カウントスタート
//---------------------------------------------------------------------------
void Timer::Start()
{
    s64 startTime = 0;
    QueryPerformanceCounter((LARGE_INTEGER*)&startTime);

    // Accumulate the time elapsed between stop and start pairs.
    //
    //                     |<-------d------->|
    // ----*---------------*-----------------*------------> time
    //  m_BaseTime       m_StopTime        startTime

    if(_stopped) {
        _pausedTime += (startTime - _stopTime);

        _prevTime = startTime;
        _stopTime = 0;
        _stopped  = false;
    }
}
//---------------------------------------------------------------------------
//! カウントストップ
//---------------------------------------------------------------------------
void Timer::Stop()
{
    if(!_stopped) {
        s64 currTime = 0;
        QueryPerformanceCounter((LARGE_INTEGER*)&currTime);

        _stopTime = currTime;
        _stopped  = true;
    }
}
//---------------------------------------------------------------------------
//! 時間チカチカ(アップデート)
//---------------------------------------------------------------------------
void Timer::Tick()
{
    if(_stopped) {
        _deltaTime = 0.0;
        return;
    }

    __int64 currTime;
    QueryPerformanceCounter((LARGE_INTEGER*)&currTime);
    _currTime = currTime;

    // Time difference between this frame and the previous.
    _deltaTime = (_currTime - _prevTime) * _secondsPerCount;

    // Prepare for next frame.
    _prevTime = _currTime;

    if(_deltaTime < 0.0) {
        _deltaTime = 0.0;
    }
}
//---------------------------------------------------------------------------
//! 実体を取得
//---------------------------------------------------------------------------
timer::Timer* TimerIns()
{
    return Timer::Instance();
}
//---------------------------------------------------------------------------
//! デルタタイム
//---------------------------------------------------------------------------
f32 DeltaTime()
{
    return TimerIns()->DeltaTime();
}
//---------------------------------------------------------------------------
//! 総プレイ時間
//---------------------------------------------------------------------------
f32 TotalTime()
{
    return TimerIns()->TotalTime();
}
}   // namespace timer
