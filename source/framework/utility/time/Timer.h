﻿//---------------------------------------------------------------------------
//!	@file	Timer.cpp
//!	@brief	タイムクラス
//! Introduction to 3D Game Programming With Directx 11
//! この本を参考にしました
//---------------------------------------------------------------------------
#pragma once
#include "pattern/Singleton.h"
namespace timer {
class Timer : public Singleton<Timer>
{
public:
    Timer();   //! コンストラクタ

    // コピー禁止 / ムブー禁止
    Timer(const Timer&) = delete;
    Timer(Timer&&)      = delete;
    Timer& operator=(const Timer&) = delete;
    Timer& operator=(Timer&&) = delete;

    f32 TotalTime() const;   //!< 総プレイ時間
    f32 DeltaTime() const;   //!< 直前のフレームと今のフレーム間で経過した時間（秒）

    void Reset();   //!< リセット
    void Start();   //!< カウントスタート
    void Stop();    //!< ストップ
    void Tick();    //!< アップデート
private:
    f64 _secondsPerCount;   //!<毎秒のカウント
    f64 _deltaTime;         //!< デルタタイム

    s64 _baseTime;     //!< 開始時間
    s64 _pausedTime;   //!< 一時停止時間
    s64 _stopTime;     //!< 停止時間
    s64 _prevTime;     //!< 一時保管
    s64 _currTime;     //!< 現在時間

    bool _stopped;   //!< ストップフラッグ
};
timer::Timer* TimerIns();   //!< シングルトンの実体を取得

f32 DeltaTime();   //!< デルタタイム
f32 TotalTime();   //!< 総プレイ時間
}   // namespace timer
