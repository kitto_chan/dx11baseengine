﻿//---------------------------------------------------------------------------
//!	@file	Geometry.cpp
//!	@brief	ジオメトリの頂点データ
//---------------------------------------------------------------------------
#include "Geometry.h"
namespace geometry {
//---------------------------------------------------------------------------
//! 球体のメッシュデータを生成
//---------------------------------------------------------------------------
MeshData CreateSphere(f32 rad, u32 levels, u32 slices, const float4& col)
{
    MeshData   meshData;
    VertexData vertexData;

    u32 vertexNum = 2 + (levels - 1) * (slices + 1);
    u32 indexNum  = 6 * (levels - 1) * slices;

    meshData.vertexData.resize(vertexNum);
    meshData.indexData.resize(indexNum);

    u32 vertexCount = 0;
    u32 indexCount  = 0;

    f32 phi   = 0.0f;
    f32 theta = 0.0f;

    f32 per_phi   = DirectX::XM_PI / levels;
    f32 per_theta = DirectX::XM_2PI / slices;

    f32 x, y, z;

    // 球体, 上の中心点
    // この点から描画する
    vertexData = { DirectX::XMFLOAT3(0.0f, rad, 0.0f),
                   DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f),
                   DirectX::XMFLOAT3(1.0f, 0.0f, 0.0f),
                   math::CastToXMFLOAT4(col),
                   DirectX::XMFLOAT2(0.0f, 0.0f) };

    meshData.vertexData[vertexCount++] = vertexData;

    for(u32 i = 1; i < levels; ++i) {
        phi = per_phi * i;
        //! sclices値+1, 起点と終点の位置は一緒だけど
        //! テクスチャ座標は違う
        for(u32 j = 0; j <= slices; ++j) {
            theta = per_theta * j;

            x = rad * sinf(phi) * cosf(theta);
            y = rad * cosf(phi);
            z = rad * sinf(phi) * sinf(theta);

            DirectX::XMFLOAT3 pos = DirectX::XMFLOAT3(x, y, z);
            DirectX::XMFLOAT3 nor;
            DirectX::XMStoreFloat3(&nor, DirectX::XMVector3Normalize(DirectX::XMLoadFloat3(&pos)));
            DirectX::XMFLOAT3 tan = DirectX::XMFLOAT3(-sinf(theta), 0.0f, cosf(theta));
            DirectX::XMFLOAT2 tex = DirectX::XMFLOAT2(theta / math::PI *-1.0f, phi / math::PI);

            vertexData = { pos, nor, tan, math::CastToXMFLOAT4(col), tex };

            meshData.vertexData[vertexCount++] = vertexData;
        }
    }

    // 球体,下の中心点
    vertexData = { DirectX::XMFLOAT3(0.0f, -rad, 0.0f),
                   DirectX::XMFLOAT3(0.0f, -1.0f, 0.0f),
                   DirectX::XMFLOAT3(-1.0f, 0.0f, 0.0f),
                   math::CastToXMFLOAT4(col),
                   DirectX::XMFLOAT2(0.0f, 1.0f) };

    meshData.vertexData[vertexCount++] = vertexData;

    // インデックス
    if(levels > 1) {
        for(u32 j = 1; j <= slices; ++j) {
            meshData.indexData[indexCount++] = 0;
            meshData.indexData[indexCount++] = j % (slices + 1) + 1;
            meshData.indexData[indexCount++] = j;
        }
    }

    for(UINT i = 1; i < levels - 1; ++i) {
        for(UINT j = 1; j <= slices; ++j) {
            meshData.indexData[indexCount++] = (i - 1) * (slices + 1) + j;
            meshData.indexData[indexCount++] = (i - 1) * (slices + 1) + j % (slices + 1) + 1;
            meshData.indexData[indexCount++] = i * (slices + 1) + j % (slices + 1) + 1;

            meshData.indexData[indexCount++] = i * (slices + 1) + j % (slices + 1) + 1;
            meshData.indexData[indexCount++] = i * (slices + 1) + j;
            meshData.indexData[indexCount++] = (i - 1) * (slices + 1) + j;
        }
    }

    if(levels > 1) {
        for(UINT j = 1; j <= slices; ++j) {
            meshData.indexData[indexCount++] = (levels - 2) * (slices + 1) + j;
            meshData.indexData[indexCount++] = (levels - 2) * (slices + 1) + j % (slices + 1) + 1;
            meshData.indexData[indexCount++] = (levels - 1) * (slices + 1) + 1;
        }
    }

    return meshData;
}
//---------------------------------------------------------------------------
//! 3Dキューブのメッシュデータを生成
//---------------------------------------------------------------------------
MeshData CreateBox(const float3& size, const float4& col)
{
    MeshData meshData;

    // メモリ確保
    meshData.vertexData.resize(24);
    meshData.indexData.resize(24);

    float3 halfSize = size;

    // 右(+x)
    meshData.vertexData[0].pos = DirectX::XMFLOAT3(halfSize.x, -halfSize.y, -halfSize.z);
    meshData.vertexData[1].pos = DirectX::XMFLOAT3(halfSize.x, halfSize.y, -halfSize.z);
    meshData.vertexData[2].pos = DirectX::XMFLOAT3(halfSize.x, halfSize.y, halfSize.z);
    meshData.vertexData[3].pos = DirectX::XMFLOAT3(halfSize.x, -halfSize.y, halfSize.z);

    // 右(-x)
    meshData.vertexData[4].pos = DirectX::XMFLOAT3(-halfSize.x, -halfSize.y, halfSize.z);
    meshData.vertexData[5].pos = DirectX::XMFLOAT3(-halfSize.x, halfSize.y, halfSize.z);
    meshData.vertexData[6].pos = DirectX::XMFLOAT3(-halfSize.x, halfSize.y, -halfSize.z);
    meshData.vertexData[7].pos = DirectX::XMFLOAT3(-halfSize.x, -halfSize.y, -halfSize.z);

    // 上 (+y)
    meshData.vertexData[8].pos  = DirectX::XMFLOAT3(-halfSize.x, halfSize.y, -halfSize.z);
    meshData.vertexData[9].pos  = DirectX::XMFLOAT3(-halfSize.x, halfSize.y, halfSize.z);
    meshData.vertexData[10].pos = DirectX::XMFLOAT3(halfSize.x, halfSize.y, halfSize.z);
    meshData.vertexData[11].pos = DirectX::XMFLOAT3(halfSize.x, halfSize.y, -halfSize.z);

    // 下 (-y)
    meshData.vertexData[12].pos = DirectX::XMFLOAT3(halfSize.x, -halfSize.y, -halfSize.z);
    meshData.vertexData[13].pos = DirectX::XMFLOAT3(halfSize.x, -halfSize.y, halfSize.z);
    meshData.vertexData[14].pos = DirectX::XMFLOAT3(-halfSize.x, -halfSize.y, halfSize.z);
    meshData.vertexData[15].pos = DirectX::XMFLOAT3(-halfSize.x, -halfSize.y, -halfSize.z);

    // 前 (+Z)
    meshData.vertexData[16].pos = DirectX::XMFLOAT3(halfSize.x, -halfSize.y, halfSize.z);
    meshData.vertexData[17].pos = DirectX::XMFLOAT3(halfSize.x, halfSize.y, halfSize.z);
    meshData.vertexData[18].pos = DirectX::XMFLOAT3(-halfSize.x, halfSize.y, halfSize.z);
    meshData.vertexData[19].pos = DirectX::XMFLOAT3(-halfSize.x, -halfSize.y, halfSize.z);

    // 後(-Z)
    meshData.vertexData[20].pos = DirectX::XMFLOAT3(-halfSize.x, -halfSize.y, -halfSize.z);
    meshData.vertexData[21].pos = DirectX::XMFLOAT3(-halfSize.x, halfSize.y, -halfSize.z);
    meshData.vertexData[22].pos = DirectX::XMFLOAT3(halfSize.x, halfSize.y, -halfSize.z);
    meshData.vertexData[23].pos = DirectX::XMFLOAT3(halfSize.x, -halfSize.y, -halfSize.z);

    for(UINT i = 0; i < 4; ++i) {
        // 右 (+x)
        meshData.vertexData[i + 0].nor = DirectX::XMFLOAT3(1.0f, 0.0f, 0.0f);
        meshData.vertexData[i + 0].tan = DirectX::XMFLOAT3(0.0f, 0.0f, 1.0f);
        meshData.vertexData[i + 0].col = math::CastToXMFLOAT4(col);
        // 左 (-x)
        meshData.vertexData[i + 4].nor = DirectX::XMFLOAT3(-1.0f, 0.0f, 0.0f);
        meshData.vertexData[i + 4].tan = DirectX::XMFLOAT3(0.0f, 0.0f, -1.0f);
        meshData.vertexData[i + 4].col = math::CastToXMFLOAT4(col);
        // 上 (+y)
        meshData.vertexData[i + 8].nor = DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f);
        meshData.vertexData[i + 8].tan = DirectX::XMFLOAT3(1.0f, 0.0f, 0.0f);
        meshData.vertexData[i + 8].col = math::CastToXMFLOAT4(col);
        // 下 (-y)
        meshData.vertexData[i + 12].nor = DirectX::XMFLOAT3(0.0f, -1.0f, 0.0f);
        meshData.vertexData[i + 12].tan = DirectX::XMFLOAT3(-1.0f, 0.0f, 0.0f);
        meshData.vertexData[i + 12].col = math::CastToXMFLOAT4(col);
        // 前 (+Z面)
        meshData.vertexData[i + 16].nor = DirectX::XMFLOAT3(0.0f, 0.0f, 1.0f);
        meshData.vertexData[i + 16].tan = DirectX::XMFLOAT3(-1.0f, 0.0f, 0.0f);
        meshData.vertexData[i + 16].col = math::CastToXMFLOAT4(col);
        // 後ろ(-Z)
        meshData.vertexData[i + 20].nor = DirectX::XMFLOAT3(0.0f, 0.0f, -1.0f);
        meshData.vertexData[i + 20].tan = DirectX::XMFLOAT3(1.0f, 0.0f, 0.0f);
        meshData.vertexData[i + 20].col = math::CastToXMFLOAT4(col);
    }

    for(u32 i = 0; i < 6; ++i) {
        meshData.vertexData[i * 4 + 0].tex = DirectX::XMFLOAT2(1.0f, 1.0f);
        meshData.vertexData[i * 4 + 1].tex = DirectX::XMFLOAT2(1.0f, 0.0f);
        meshData.vertexData[i * 4 + 2].tex = DirectX::XMFLOAT2(0.0f, 0.0f);
        meshData.vertexData[i * 4 + 3].tex = DirectX::XMFLOAT2(0.0f, 1.0f);
    }

    meshData.indexData = {
        0, 1, 2, 2, 3, 0,         // 右(+X)
        4, 5, 6, 6, 7, 4,         // 左(-X)
        8, 9, 10, 10, 11, 8,      // 上(+Y)
        12, 13, 14, 14, 15, 12,   // 下(-Y)
        16, 17, 18, 18, 19, 16,   // 前(+Z)
        20, 21, 22, 22, 23, 20,   // 後(-Z)
    };
    return meshData;
}
//---------------------------------------------------------------------------
//! 2D表示の平面メッシュデータを生成
//! @param minX, minY 描画する四角形の左上の頂点座標
//! @param maxX, maxY 描画する四角形の右下の頂点座標
//---------------------------------------------------------------------------
MeshData CreatePlane2D(f32 minX, f32 minY, f32 maxX, f32 maxY, const float4& color)
{
    using namespace DirectX;
    MeshData meshData;

    // 空間確保
    meshData.vertexData.resize(4);
    meshData.indexData.resize(6);

    VertexData vertexData;

    // 左上
    vertexData = { XMFLOAT3(minX, minY, 0.0f),
                   XMFLOAT3(0.0f, 0.0f, -1.0f),
                   XMFLOAT3(1.0f, 0.0f, 0.0f),
                   math::CastToXMFLOAT4(color),
                   XMFLOAT2(0.0f, 1.0f) };

    meshData.vertexData[0] = vertexData;

    // 右上
    vertexData = { XMFLOAT3(maxX, minY, 0.0f),
                   XMFLOAT3(0.0f, 0.0f, -1.0f),
                   XMFLOAT3(1.0f, 0.0f, 0.0f),
                   math::CastToXMFLOAT4(color),
                   XMFLOAT2(1.0f, 1.0f) };

    meshData.vertexData[1] = vertexData;

    // 左下
    vertexData = { XMFLOAT3(minX, maxY, 0.0f),
                   XMFLOAT3(0.0f, 0.0f, -1.0f),
                   XMFLOAT3(1.0f, 0.0f, 0.0f),
                   math::CastToXMFLOAT4(color),
                   XMFLOAT2(0.0f, 0.0f) };

    meshData.vertexData[2] = vertexData;

    // 右下
    vertexData = { XMFLOAT3(maxX, maxY, 0.0f),
                   XMFLOAT3(0.0f, 0.0f, -1.0f),
                   XMFLOAT3(1.0f, 0.0f, 0.0f),
                   math::CastToXMFLOAT4(color),
                   XMFLOAT2(1.0f, 0.0f) };

    meshData.vertexData[3] = vertexData;

    meshData.indexData = { 0, 1, 2, 2, 3, 1 };
    return meshData;
}
}   // namespace geometry
