﻿//---------------------------------------------------------------------------
//!	@file	Geometry.h
//!	@brief	ジオメトリの頂点データ
//---------------------------------------------------------------------------
#pragma once
namespace geometry {
//! 頂点データリスト
struct VertexData
{
    DirectX::XMFLOAT3 pos;   //!< 座標(position)
    DirectX::XMFLOAT3 nor;   //!< ノーマル(normal)
    DirectX::XMFLOAT3 tan;   //!< 正接(tangent)
    DirectX::XMFLOAT4 col;   //!< 色 (color)
    DirectX::XMFLOAT2 tex;   //!< テクスチャ座標系 (tex coord)
};

//! メッシュのデータ
struct MeshData
{
    std::vector<VertexData> vertexData;   //!< 頂点データリスト
    std::vector<s32>        indexData;    //!< インデックス
};

//! 球体のメッシュデータを生成
MeshData CreateSphere(f32 rad, u32 levels = 20, u32 slices = 20, const float4& color = { 1.0f, 0.0f, 0.0f, 1.0f });

//! 3Dキューブのメッシュデータを生成
MeshData CreateBox(const float3& size, const float4& color = math::ONE);

//! 2D表示の平面メッシュデータを生成
MeshData CreatePlane2D(f32 minX, f32 minY, f32 maxX, f32 maxY, const float4& color = math::ONE);
}   // namespace geometry
