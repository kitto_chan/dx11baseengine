﻿//---------------------------------------------------------------------------
//!	@file	ImguiWidget.cpp
//!	@brief	自分の描画スタイルを統一する
//---------------------------------------------------------------------------
#include "imgui/imgui.h"
#include "imgui/imgui_internal.h"
#include "IconFont/IconsFontAwesome5.h"
#include "IconFont/IconsKenney.h"
#include "ImguiWidget.h"
#include <magic_enum/include/magic_enum.hpp>

namespace imgui {
namespace {
constexpr f32 LABEL_COL_SIZE = 100.0f;   //!< ラベルのColumnサイズ

constexpr f32 dragSpeed  = 0.1f;         // DragFloatの速さ
const ImVec2  buttonSize = { 22, 22 };   // ボタンサイズ
}   // namespace
void ImguiDragXY(const std::string& label, float2& xy)
{
    f32 x = xy.x;
    f32 y = xy.y;

    ImGui::PushID(label.c_str());

    // 左のラベル表示
    ImGui::Columns(2);
    ImGui::SetColumnWidth(0, LABEL_COL_SIZE);
    ImGui::Text(label.c_str());

    // 右のDragFloat設定
    ImGui::NextColumn();
    ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2{ 0.0f, 5.0f });
    ImGui::PushMultiItemsWidths(2, ImGui::CalcItemWidth());

    //----------
    // X設定
    // ボタンスタイル
    ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.8f, 0.15f, 0.15f, 1.0f });
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.9f, 0.2f, 0.2f, 1.0f });
    ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.8f, 0.15f, 0.15f, 1.0f });
    if(ImGui::Button("X", buttonSize)) x = 0.0f;   // Reset
    ImGui::PopStyleColor(3);

    // DragFloat
    ImGui::SameLine();
    ImGui::DragFloat("##X", &x, dragSpeed, 0.0f, 0.0f, "%.3f");
    ImGui::PopItemWidth();

    //----------
    // Y設定
    // ボタンスタイル
    ImGui::SameLine();
    ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.15f, 0.8f, 0.15f, 1.0f });
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.2f, 0.9f, 0.2f, 1.0f });
    ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.15f, 0.8f, 0.15f, 1.0f });
    if(ImGui::Button("Y", buttonSize)) y = 0.0f;   // Reset
    ImGui::PopStyleColor(3);

    // DragFloat
    ImGui::SameLine();
    ImGui::DragFloat("##Y", &y, dragSpeed, 0.0f, 0.0f, "%.3f");
    ImGui::PopItemWidth();

    ImGui::Columns(1);

    xy.x = x;
    xy.y = y;
    ImGui::PopStyleVar();
    ImGui::PopID();
}
//---------------------------------------------------------------------------
//! float3 を Imgui DragFloatの形で描画する
//---------------------------------------------------------------------------
void ImguiDragXYZ(const std::string& label, float3& xyz)
{
    f32 x = xyz.x;
    f32 y = xyz.y;
    f32 z = xyz.z;

    ImGui::PushID(label.c_str());

    // 左のラベル表示
    ImGui::Columns(2);
    ImGui::SetColumnWidth(0, LABEL_COL_SIZE);
    ImGui::AlignTextToFramePadding();
    ImGui::Text(label.c_str());

    // 右のDragFloat設定
    ImGui::NextColumn();
    ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2{ 0.0f, 5.0f });
    ImGui::PushMultiItemsWidths(3, ImGui::CalcItemWidth());

    //----------
    // X設定
    // ボタンスタイル
    ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.8f, 0.15f, 0.15f, 1.0f });
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.9f, 0.2f, 0.2f, 1.0f });
    ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.8f, 0.15f, 0.15f, 1.0f });
    if(ImGui::Button("X", buttonSize)) x = 0.0f;   // Reset
    ImGui::PopStyleColor(3);

    // DragFloat
    ImGui::SameLine();
    ImGui::DragFloat("##X", &x, dragSpeed, 0.0f, 0.0f, "%.3f");
    ImGui::PopItemWidth();

    //----------
    // Y設定
    // ボタンスタイル
    ImGui::SameLine();
    ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.15f, 0.8f, 0.15f, 1.0f });
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.2f, 0.9f, 0.2f, 1.0f });
    ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.15f, 0.8f, 0.15f, 1.0f });
    if(ImGui::Button("Y", buttonSize)) y = 0.0f;   // Reset
    ImGui::PopStyleColor(3);

    // DragFloat
    ImGui::SameLine();
    ImGui::DragFloat("##Y", &y, dragSpeed, 0.0f, 0.0f, "%.3f");
    ImGui::PopItemWidth();

    //----------
    // Z設定
    // ボタンスタイル
    ImGui::SameLine();
    ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.15f, 0.15f, 0.8f, 1.0f });
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.2f, 0.2f, 0.9f, 1.0f });
    ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.15f, 0.15f, 0.8f, 1.0f });
    if(ImGui::Button("Z", buttonSize)) z = 0.0f;
    ImGui::PopStyleColor(3);

    // DragFloat
    ImGui::SameLine();
    ImGui::DragFloat("##Z", &z, dragSpeed, 0.0f, 0.0f, "%.3f");
    ImGui::PopItemWidth();

    ImGui::Columns(1);

    xyz.x = x;
    xyz.y = y;
    xyz.z = z;
    ImGui::PopStyleVar();
    ImGui::PopID();
}
//---------------------------------------------------------------------------
//! チェックボックス
//---------------------------------------------------------------------------
void ImguiCheckBox(std::string_view label, bool& value)
{
    ImGui::Columns(2);
    ImGui::SetColumnWidth(0, LABEL_COL_SIZE);
    ImGui::Text(label.data());
    ImGui::NextColumn();
    std::string labelStr = label.data();
    labelStr             = "##" + labelStr;
    ImGui::Checkbox(labelStr.c_str(), &value);

    ImGui::Columns(1);
}
//---------------------------------------------------------------------------
//! DragFloat
//---------------------------------------------------------------------------
void ImguiDragFloat(std::string_view label, f32& value)
{
    ImGui::PushID(label.data());

    ImGui::Columns(2);
    ImGui::SetColumnWidth(0, LABEL_COL_SIZE);
    ImGui::Text(label.data());

    ImGui::NextColumn();

    // ボータン
    ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2{ 0.0f, 5.0f });
    ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.8f, 0.15f, 0.15f, 1.0f });
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.9f, 0.2f, 0.2f, 1.0f });
    ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.8f, 0.15f, 0.15f, 1.0f });
    if(ImGui::Button(ICON_KI_RELOAD, buttonSize)) value = 0.0f;   // Reset
    ImGui::PopStyleColor(3);

    // DragFloat
    ImGui::SameLine();
    ImGui::DragFloat("##label", &value, dragSpeed, 0.0f, 0.0f, "%.3f");

    ImGui::Columns(1);
    ImGui::PopStyleVar();
    ImGui::PopID();
}
//---------------------------------------------------------------------------
//! ImguiSliderFloat
//---------------------------------------------------------------------------
void ImguiSliderFloat(std::string_view label, f32& v, f32 v_min, f32 v_max, std::string_view format, ImGuiSliderFlags flags)
{
    ImGui::PushID(label.data());
    ImGui::Columns(2);
    ImGui::SetColumnWidth(0, LABEL_COL_SIZE);
    ImGui::Text(label.data());
    ImGui::NextColumn();

    std::string labelStr = label.data();
    labelStr             = "##" + labelStr;
    ImGui::SliderFloat(labelStr.c_str(), &v, v_min, v_max, format.data(), flags);

    ImGui::Columns(1);
    ImGui::PopID();
}
//---------------------------------------------------------------------------
//! 自分のImguiスタイルで描画(Begin)
//---------------------------------------------------------------------------
void ImguiColumn2FormatBegin(std::string_view label)
{
    ImGui::PushID(label.data());
    ImGui::Columns(2);
    ImGui::SetColumnWidth(0, LABEL_COL_SIZE);
    ImGui::AlignTextToFramePadding();
    ImGui::Text(label.data());
    ImGui::NextColumn();

    std::string labelStr = label.data();
}
//---------------------------------------------------------------------------
//! 自分のImguiスタイルで描画(End)
//---------------------------------------------------------------------------
void ImguiColumn2FormatEnd()
{
    ImGui::Columns(1);
    ImGui::PopID();
}

//---------------------------------------------------------------------------
//! メニューバー描画
//---------------------------------------------------------------------------
void RenderMenuBar()
{
    if(input::IsKeyPress(DirectX::Keyboard::F3)) {
        SystemSettingsMgr()->SwapPhyDebug();
    }
    if(input::IsKeyPress(DirectX::Keyboard::F4)) {
        SystemSettingsMgr()->SwapImguiDemoFlag();
    }
    ImGui::Begin("MainDockspace");
    if(ImGui::BeginMenuBar()) {
        if(ImGui::BeginMenu("Debug")) {
            if(ImGui::MenuItem("Render Phy Debug", "F3", SystemSettingsMgr()->IsPhyDebug())) {
                SystemSettingsMgr()->SwapPhyDebug();
            }

            if(ImGui::MenuItem("Use Imgui Demo", "F4", SystemSettingsMgr()->IsUseImguiDemo())) {
                SystemSettingsMgr()->SwapImguiDemoFlag();
            }
            ImGui::EndMenu();
        }
        ImGui::EndMenuBar();
    }
    ImGui::End();
}
//---------------------------------------------------------------------------
//! 小さい(?)マークでヒントを表示する
//---------------------------------------------------------------------------
void HelpMarker(std::string_view hint)
{
    ImGui::TextDisabled("(?)");
    if(ImGui::IsItemHovered()) {
        ImGui::BeginTooltip();
        ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
        ImGui::TextUnformatted(hint.data());
        ImGui::PopTextWrapPos();
        ImGui::EndTooltip();
    }
}
}   // namespace imgui
