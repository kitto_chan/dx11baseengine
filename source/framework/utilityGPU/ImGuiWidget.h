﻿//---------------------------------------------------------------------------
//!	@file	ImguiWidget.h
//!	@brief	自分の描画スタイル
//---------------------------------------------------------------------------
#pragma once
#include <magic_enum/include/magic_enum.hpp>
namespace imgui {
//! float2 を Imgui DragFloatの形で描画する
void ImguiDragXY(const std::string& label, float2& xy);

//! float3 を Imgui DragFloatの形で描画する
void ImguiDragXYZ(const std::string& label, float3& xyz);

//! ImGui::CheckBox
void ImguiCheckBox(std::string_view label, bool& value);

//! ImGui::DragFloat
void ImguiDragFloat(std::string_view label, f32& value);

//! ImGui::SliderFloat
void ImguiSliderFloat(std::string_view label,
                      f32& v, f32 v_min, f32 v_max,
                      std::string_view format = "%.3f",
                      ImGuiSliderFlags flags  = 0);

//! 自分のImguiスタイルで描画(Begin)
void ImguiColumn2FormatBegin(std::string_view label);
//! 自分のImguiスタイルで描画(End)
void ImguiColumn2FormatEnd();

//! メニューバー描画
void RenderMenuBar();

//! 小さい(?)マークでヒントを表示する
void HelpMarker(std::string_view hint);

//! Enumがimgui Combo生成
template<typename EnumType>
void ImGuiEnumCombo(std::string_view label, EnumType& currentSelect)
{
    constexpr auto enumNames         = magic_enum::enum_names<EnumType>();
    auto           selectedLayerName = magic_enum::enum_name(currentSelect);

    imgui::ImguiColumn2FormatBegin(label);
    ImGui::PushItemWidth(ImGui::GetColumnWidth() - 15.f);

    std::string labelStr = label.data();
    labelStr             = "##" + labelStr;

    if(ImGui::BeginCombo(labelStr.data(), selectedLayerName.data())) {
        for(int n = 0; n < enumNames.size(); n++) {
            const bool is_selected = (magic_enum::enum_integer(currentSelect) == n);
            if(ImGui::Selectable(enumNames[n].data(), is_selected))
                currentSelect = magic_enum::enum_value<EnumType>(n);

            // Set the initial focus when opening the combo (scrolling + keyboard navigation focus)
            if(is_selected)
                ImGui::SetItemDefaultFocus();
        }
        ImGui::EndCombo();
    }
    imgui::ImguiColumn2FormatEnd();
}
}   // namespace imgui
