﻿//---------------------------------------------------------------------------
//!	@file	RenderState.h
//!	@brief	レンダーステート定義のヘルパークラス
//! @note   TODO: ちょっとデザインが悪いと思う
//!				　リファクタリング（再構築)しようかな
//---------------------------------------------------------------------------
#include "RenderState.h"

namespace renderer {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
RenderStates::RenderStates()
{
    Init();   //! ステート設定だけ、コンストラクタでやっても大丈夫
}
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
RenderStates::~RenderStates()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
void RenderStates::Init()
{
    // CullMode
    // D3D11_CULL_NONE　トライアングル　前後も描画する
    // D3D11_CULL_FRONT 前向きだけ描画する
    // D3D11_CULL_BACK　後ろだけ描画する

    //============================================================================
    // ラスタライザー
    //============================================================================
    D3D11_RASTERIZER_DESC rasterizerDesc;
    ZeroMemory(&rasterizerDesc, sizeof(rasterizerDesc));

    // WIREFRAME
    rasterizerDesc.FillMode              = D3D11_FILL_WIREFRAME;
    rasterizerDesc.CullMode              = D3D11_CULL_NONE;
    rasterizerDesc.FrontCounterClockwise = false;
    rasterizerDesc.DepthClipEnable       = true;
    dx11::d3dDevice()->CreateRasterizerState(&rasterizerDesc, _pRSWireframe.GetAddressOf());

    // Fill solid
    rasterizerDesc.FillMode              = D3D11_FILL_SOLID;
    rasterizerDesc.CullMode              = D3D11_CULL_NONE;
    rasterizerDesc.FrontCounterClockwise = false;
    rasterizerDesc.DepthClipEnable       = true;
    dx11::d3dDevice()->CreateRasterizerState(&rasterizerDesc, _pRSNoCull.GetAddressOf());

    // CullClockWise
    rasterizerDesc.FillMode              = D3D11_FILL_SOLID;
    rasterizerDesc.CullMode              = D3D11_CULL_BACK;
    rasterizerDesc.FrontCounterClockwise = true;
    rasterizerDesc.DepthClipEnable       = true;
    dx11::d3dDevice()->CreateRasterizerState(&rasterizerDesc, _pRSCullClockWise.GetAddressOf());

    //============================================================================
    // Sampler
    //============================================================================
    D3D11_SAMPLER_DESC sampDesc;
    ZeroMemory(&sampDesc, sizeof(sampDesc));

	// Point
    sampDesc.Filter         = D3D11_FILTER_MIN_MAG_MIP_POINT;
    sampDesc.AddressU       = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.AddressV       = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.AddressW       = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
    sampDesc.MinLOD         = 0;
    sampDesc.MaxLOD         = D3D11_FLOAT32_MAX;
    dx11::d3dDevice()->CreateSamplerState(&sampDesc, _pSSPointWrap.GetAddressOf());

    //Linear
    sampDesc.Filter         = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
    sampDesc.AddressU       = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.AddressV       = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.AddressW       = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
    sampDesc.MinLOD         = 0;
    sampDesc.MaxLOD         = D3D11_FLOAT32_MAX;
    dx11::d3dDevice()->CreateSamplerState(&sampDesc, _pSSLinearWrap.GetAddressOf());

    // anisotropic
    sampDesc.Filter         = D3D11_FILTER_ANISOTROPIC;
    sampDesc.AddressU       = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.AddressV       = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.AddressW       = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
    sampDesc.MaxAnisotropy  = 4;
    sampDesc.MinLOD         = 0;
    sampDesc.MaxLOD         = D3D11_FLOAT32_MAX;
    dx11::d3dDevice()->CreateSamplerState(&sampDesc, _pSSAnistropicWrap.GetAddressOf());

    //============================================================================
    // Blend
    //============================================================================
    D3D11_BLEND_DESC blendDesc;
    ZeroMemory(&blendDesc, sizeof(blendDesc));

    auto& rtDesc = blendDesc.RenderTarget[0];
    // Alpha-TO-Coverage
    blendDesc.AlphaToCoverageEnable  = true;
    blendDesc.IndependentBlendEnable = false;
    rtDesc.BlendEnable               = false;
    rtDesc.RenderTargetWriteMask     = D3D11_COLOR_WRITE_ENABLE_ALL;
    dx11::d3dDevice()->CreateBlendState(&blendDesc, _pBSAlphaToCoverage.GetAddressOf());

    // 透明
    blendDesc.AlphaToCoverageEnable  = false;
    blendDesc.IndependentBlendEnable = false;
    rtDesc.BlendEnable               = true;
    rtDesc.SrcBlend                  = D3D11_BLEND_SRC_ALPHA;
    rtDesc.DestBlend                 = D3D11_BLEND_INV_SRC_ALPHA;
    rtDesc.BlendOp                   = D3D11_BLEND_OP_ADD;
    rtDesc.SrcBlendAlpha             = D3D11_BLEND_ONE;
    rtDesc.DestBlendAlpha            = D3D11_BLEND_ZERO;
    rtDesc.BlendOpAlpha              = D3D11_BLEND_OP_ADD;
    dx11::d3dDevice()->CreateBlendState(&blendDesc, _pBSTransparent.GetAddressOf());

    // 加算
    // Color = SrcColor + DestColor
    // Alpha = SrcAlpha
    rtDesc.SrcBlend       = D3D11_BLEND_ONE;
    rtDesc.DestBlend      = D3D11_BLEND_ONE;
    rtDesc.BlendOp        = D3D11_BLEND_OP_ADD;
    rtDesc.SrcBlendAlpha  = D3D11_BLEND_ONE;
    rtDesc.DestBlendAlpha = D3D11_BLEND_ZERO;
    rtDesc.BlendOpAlpha   = D3D11_BLEND_OP_ADD;
    dx11::d3dDevice()->CreateBlendState(&blendDesc, _pBSAdditive.GetAddressOf());

    // 色なし
    rtDesc.BlendEnable           = false;
    rtDesc.SrcBlend              = D3D11_BLEND_ZERO;
    rtDesc.DestBlend             = D3D11_BLEND_ONE;
    rtDesc.BlendOp               = D3D11_BLEND_OP_ADD;
    rtDesc.SrcBlendAlpha         = D3D11_BLEND_ZERO;
    rtDesc.DestBlendAlpha        = D3D11_BLEND_ONE;
    rtDesc.BlendOpAlpha          = D3D11_BLEND_OP_ADD;
    rtDesc.RenderTargetWriteMask = 0;
    dx11::d3dDevice()->CreateBlendState(&blendDesc, _pBSNoColorWrite.GetAddressOf());

    //============================================================================
    //　Depth Stencil
    //============================================================================
    D3D11_DEPTH_STENCIL_DESC dsDesc;
    // デステスト　LESS　EQUAL
    // スカイボックス用、デスバッファ1.0の時depth test fail
    dsDesc.DepthEnable    = true;
    dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
    dsDesc.DepthFunc      = D3D11_COMPARISON_LESS_EQUAL;

    dsDesc.StencilEnable = false;

    dx11::d3dDevice()->CreateDepthStencilState(&dsDesc, _pDSSLessEqual.GetAddressOf());
}
//! 実体を取得
renderer::RenderStates* RenderStatesIns()
{
    return RenderStates::Instance();
}

}   // namespace render
