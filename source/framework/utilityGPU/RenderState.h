﻿//---------------------------------------------------------------------------
//!	@file	RenderState.h
//!	@brief	レンダーステート定義のヘルパークラス
//! @note   TODO: ちょっとデザインが悪いと思う
//!				　リファクタリング（再構築)しようかな
//---------------------------------------------------------------------------
#pragma once
namespace renderer {

class RenderStates : public Singleton<RenderStates>
{
public:
    RenderStates();    //!< コンストラクタ
    ~RenderStates();   //!< デストラクタ

    void Init();   //!< 初期化

public:
    //============================================================================
    // ラスタライザー
    //============================================================================
    com_ptr<ID3D11RasterizerState> _pRSWireframe;       //!< ワイヤフレームで塗りつぶす。
    com_ptr<ID3D11RasterizerState> _pRSNoCull;          //!< 前後も描画するとベタ塗りで塗りつぶす。
    com_ptr<ID3D11RasterizerState> _pRSCullClockWise;   //!< 時計回りに繋がれた頂点の面を描画します

    //============================================================================
    // サンプラー
    //============================================================================
    com_ptr<ID3D11SamplerState> _pSSPointWrap;        //!< Point
    com_ptr<ID3D11SamplerState> _pSSLinearWrap;       //!< リニアラップ
    com_ptr<ID3D11SamplerState> _pSSAnistropicWrap;   //!< ANISOTROPIC (画像もっときれい見えるけどちょっど重い)

    //============================================================================
    // ブレンダ
    //============================================================================
    com_ptr<ID3D11BlendState> _pBSNoColorWrite;      //!< 色なし
    com_ptr<ID3D11BlendState> _pBSTransparent;       //!< 透明
    com_ptr<ID3D11BlendState> _pBSAlphaToCoverage;   //!< alpha-to-coverage
    com_ptr<ID3D11BlendState> _pBSAdditive;          //!< 加算

    //============================================================================
    // 深度ステンシルステート
    //============================================================================
    com_ptr<ID3D11DepthStencilState> _pDSSLessEqual;   //!< less equal <=
};

//! 実体を取得
renderer::RenderStates* RenderStatesIns();

}   // namespace render
