﻿//---------------------------------------------------------------------------
//!	@file	vertex.cpp
//!	@brief	全て頂点定義と入力レイアウトをまとめます
//!
//! セマンテック名(任意)
//!  ↓  セマンテック番号(0～7)
//!  ↓          ↓          データ形式   ストリームスロット番号(0-15)
//!  ↓          ↓            ↓                  ↓  構造体の先頭からのオフセットアドレス(先頭からnバイト目)
//!  ↓          ↓            ↓                  ↓   ↓                              頂点読み込みの更新周期   更新間隔
//!  ↓          ↓            ↓                  ↓   ↓                                       ↓                ↓
//{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPos, pos), D3D11_INPUT_PER_VERTEX_DATA, 0 }
//---------------------------------------------------------------------------

#include "Vertex.h"
namespace vertex {
//---------------------------------------------------------------------------
//! pos    XYZ座標
//---------------------------------------------------------------------------
const D3D11_INPUT_ELEMENT_DESC VertexPos::inputLayout[1] = {
    { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPos, _pos), D3D11_INPUT_PER_VERTEX_DATA, 0 },
};
//---------------------------------------------------------------------------
//! pos     XYZ座標
//! color   色
//---------------------------------------------------------------------------
const D3D11_INPUT_ELEMENT_DESC VertexPosColor::inputLayout[2] = {
    { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosColor, pos), D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, offsetof(VertexPosColor, color), D3D11_INPUT_PER_VERTEX_DATA, 0 }
};
//---------------------------------------------------------------------------
//! pos     XYZ座標
//! normal  ノーマル
//! color   色
//---------------------------------------------------------------------------
const D3D11_INPUT_ELEMENT_DESC VertexPosNormalColor::inputLayout[3] = {
    { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosNormalColor, pos), D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosNormalColor, normal), D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, offsetof(VertexPosNormalColor, color), D3D11_INPUT_PER_VERTEX_DATA, 0 }
};
//---------------------------------------------------------------------------
//! pos     XYZ座標
//! tex     テクスチャ
//---------------------------------------------------------------------------
const D3D11_INPUT_ELEMENT_DESC VertexPosUv::inputLayout[2] = {
    { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
};
//---------------------------------------------------------------------------
//! pos     XYZ座標
//! normal  ノーマル
//! tex     テクスチャ
//---------------------------------------------------------------------------
const D3D11_INPUT_ELEMENT_DESC VertexPosNormalUv::inputLayout[3] = {
    { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 }
};
//---------------------------------------------------------------------------
//! pos     XYZ座標
//! normal  ノーマル
//! tex     テクスチャ
//! normal  ボーン
//! weight  ウェイト、メッシュの「影響度」
//---------------------------------------------------------------------------
const D3D11_INPUT_ELEMENT_DESC VertexPosNorTexBonWei::inputLayout[5] = {
    { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosNorTexBonWei, pos), D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosNorTexBonWei, normal), D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, offsetof(VertexPosNorTexBonWei, tex), D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "BONE_ID", 0, DXGI_FORMAT_R32G32B32A32_UINT, 0, offsetof(VertexPosNorTexBonWei, boneId), D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "WEIGHT", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, offsetof(VertexPosNorTexBonWei, weight), D3D11_INPUT_PER_VERTEX_DATA, 0 }
};
//---------------------------------------------------------------------------
//! pos     XYZ座標
//! normal  ノーマル
//! tangent 正接
//! color   色
//---------------------------------------------------------------------------
const D3D11_INPUT_ELEMENT_DESC VertexPosNorTanUv::inputLayout[4] = {
    { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosNorTanUv, _pos), D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosNorTanUv, _nor), D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosNorTanUv, _tan), D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, offsetof(VertexPosNorTanUv, _uv), D3D11_INPUT_PER_VERTEX_DATA, 0 }
};
//---------------------------------------------------------------------------
//! pos     XYZ座標
//! col     カラー
//! normal  ノーマル
//! tangent 正接
//! color   色
//---------------------------------------------------------------------------
const D3D11_INPUT_ELEMENT_DESC VertexPosColNorTanUv::inputLayout[5] = {
    { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosColNorTanUv, _pos), D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, offsetof(VertexPosColNorTanUv, _col), D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosColNorTanUv, _nor), D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(VertexPosColNorTanUv, _tan), D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, offsetof(VertexPosColNorTanUv, _uv), D3D11_INPUT_PER_VERTEX_DATA, 0 }
};
}   // namespace vertex
