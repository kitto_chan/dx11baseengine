﻿//===========================================================================
//! Vertex.h
//! 頂点データーを定義する
//===========================================================================
#pragma once
#include "UtilityGPU/LightHelper.h"
namespace vertex {
//---------------------------------------------------------------------------
//! @param pos    XYZ座標
//---------------------------------------------------------------------------
struct VertexPos
{
    VertexPos() = default;

    VertexPos(const VertexPos&) = default;
    VertexPos& operator=(const VertexPos&) = default;

    VertexPos(VertexPos&&) = default;
    VertexPos& operator=(VertexPos&&) = default;

    VertexPos(const DirectX::XMFLOAT3 pos)
    : _pos(pos)
    {
    }

    DirectX::XMFLOAT3                     _pos;
    static const D3D11_INPUT_ELEMENT_DESC inputLayout[1];
};
//---------------------------------------------------------------------------
//! @param pos     XYZ座標
//! @param color   色
//---------------------------------------------------------------------------
struct VertexPosColor
{
    VertexPosColor() = default;

    VertexPosColor(const VertexPosColor&) = default;
    VertexPosColor& operator=(const VertexPosColor&) = default;

    VertexPosColor(VertexPosColor&&) = default;
    VertexPosColor& operator=(VertexPosColor&&) = default;

    constexpr VertexPosColor(const DirectX::XMFLOAT3& _pos, const DirectX::XMFLOAT4& _color)
    : pos(_pos)
    , color(_color)
    {
    }

    DirectX::XMFLOAT3                     pos;
    DirectX::XMFLOAT4                     color;
    static const D3D11_INPUT_ELEMENT_DESC inputLayout[2];
};
//---------------------------------------------------------------------------
//! @param pos     XYZ座標
//! @param normal  ノーマル
//! @param color   色
//---------------------------------------------------------------------------
struct VertexPosNormalColor
{
    DirectX::XMFLOAT3                     pos;
    DirectX::XMFLOAT3                     normal;
    DirectX::XMFLOAT3                     color;
    static const D3D11_INPUT_ELEMENT_DESC inputLayout[3];
};
//---------------------------------------------------------------------------
//! @param pos     XYZ座標
//! @param normal  ノーマル
//! @param tex     テクスチャ
//---------------------------------------------------------------------------
struct VertexPosNormalUv
{
    VertexPosNormalUv() = default;

    VertexPosNormalUv(const VertexPosNormalUv&) = default;
    VertexPosNormalUv& operator=(const VertexPosNormalUv&) = default;

    VertexPosNormalUv(VertexPosNormalUv&&) = default;
    VertexPosNormalUv& operator=(VertexPosNormalUv&&) = default;

    constexpr VertexPosNormalUv(const DirectX::XMFLOAT3& _pos, const DirectX::XMFLOAT3& _normal,
                                const DirectX::XMFLOAT2& _uv)
    : pos(_pos)
    , normal(_normal)
    , uv(_uv)
    {
    }

    DirectX::XMFLOAT3                     pos;
    DirectX::XMFLOAT3                     normal;
    DirectX::XMFLOAT2                     uv;
    static const D3D11_INPUT_ELEMENT_DESC inputLayout[3];
};

//---------------------------------------------------------------------------
//! @param pos     XYZ座標
//! @param normal  ノーマル
//! @param tex     テクスチャ
//! @param normal  ボーン
//! @param weight  ウェイト、メッシュの「影響度」
//---------------------------------------------------------------------------
struct VertexPosNorTexBonWei
{
    VertexPosNorTexBonWei() = default;

    VertexPosNorTexBonWei(const VertexPosNorTexBonWei&) = default;
    VertexPosNorTexBonWei& operator=(const VertexPosNorTexBonWei&) = default;

    VertexPosNorTexBonWei(VertexPosNorTexBonWei&&) = default;
    VertexPosNorTexBonWei& operator=(VertexPosNorTexBonWei&&) = default;

    constexpr VertexPosNorTexBonWei(const DirectX::XMFLOAT3& _pos,
                                    const DirectX::XMFLOAT3& _normal,
                                    const DirectX::XMFLOAT2& _tex,
                                    const DirectX::XMINT4&   _boneId,
                                    const DirectX::XMFLOAT4& _weight)
    : pos(_pos)
    , normal(_normal)
    , tex(_tex)
    , boneId(_boneId)
    , weight(_weight)
    {
    }

    DirectX::XMFLOAT3 pos;
    DirectX::XMFLOAT3 normal;
    DirectX::XMFLOAT2 tex;
    DirectX::XMINT4   boneId;
    DirectX::XMFLOAT4 weight;

    static const D3D11_INPUT_ELEMENT_DESC inputLayout[5];
};
//---------------------------------------------------------------------------
//! @param pos     XYZ座標
//! @param uv      uv座標
//---------------------------------------------------------------------------
struct VertexPosUv
{
    VertexPosUv() = default;

    VertexPosUv(const VertexPosUv&) = default;
    VertexPosUv& operator=(const VertexPosUv&) = default;

    VertexPosUv(VertexPosUv&&) = default;
    VertexPosUv& operator=(VertexPosUv&&) = default;

    constexpr VertexPosUv(const DirectX::XMFLOAT3& _pos, const DirectX::XMFLOAT2& _uv)
    : pos(_pos)
    , uv(_uv)
    {
    }

    DirectX::XMFLOAT3                     pos;
    DirectX::XMFLOAT2                     uv;
    static const D3D11_INPUT_ELEMENT_DESC inputLayout[2];
};
//---------------------------------------------------------------------------
//! @param pos     XYZ座標
//! @param nor     ノーマル
//! @param tan     正接
//! @param uv      テクスチャUv
//---------------------------------------------------------------------------
struct VertexPosNorTanUv
{
    VertexPosNorTanUv() = default;

    VertexPosNorTanUv(const VertexPosNorTanUv&) = default;
    VertexPosNorTanUv& operator=(const VertexPosNorTanUv&) = default;

    VertexPosNorTanUv(VertexPosNorTanUv&&) = default;
    VertexPosNorTanUv& operator=(VertexPosNorTanUv&&) = default;

    constexpr VertexPosNorTanUv(const DirectX::XMFLOAT3& pos, const DirectX::XMFLOAT3& nor,
                                const DirectX::XMFLOAT3& tan, const DirectX::XMFLOAT2& uv)
    : _pos(pos)
    , _nor(nor)
    , _tan(tan)
    , _uv(uv)
    {
    }

    DirectX::XMFLOAT3 _pos;
    DirectX::XMFLOAT3 _nor;
    DirectX::XMFLOAT3 _tan;
    DirectX::XMFLOAT2 _uv;

    static const D3D11_INPUT_ELEMENT_DESC inputLayout[4];
};
//---------------------------------------------------------------------------
//! @param pos     XYZ座標
//! @param col	   カラー
//! @param nor     ノーマル
//! @param tan     正接
//! @param uv      テクスチャUv
//---------------------------------------------------------------------------
struct VertexPosColNorTanUv
{
    VertexPosColNorTanUv() = default;

    VertexPosColNorTanUv(const VertexPosColNorTanUv&) = default;
    VertexPosColNorTanUv& operator=(const VertexPosColNorTanUv&) = default;

    VertexPosColNorTanUv(VertexPosColNorTanUv&&) = default;
    VertexPosColNorTanUv& operator=(VertexPosColNorTanUv&&) = default;

    constexpr VertexPosColNorTanUv(const DirectX::XMFLOAT3& pos, const DirectX::XMFLOAT4& col, const DirectX::XMFLOAT3& nor,
                                   const DirectX::XMFLOAT3& tan, const DirectX::XMFLOAT2& uv)
    : _pos(pos)
    , _col(col)
    , _nor(nor)
    , _tan(tan)
    , _uv(uv)
    {
    }

    DirectX::XMFLOAT3 _pos;
    DirectX::XMFLOAT4 _col;
    DirectX::XMFLOAT3 _nor;
    DirectX::XMFLOAT3 _tan;
    DirectX::XMFLOAT2 _uv;

    static const D3D11_INPUT_ELEMENT_DESC inputLayout[5];
};

}   // namespace vertex
