﻿//---------------------------------------------------------------------------
//!	@file	GameClient.cpp
//!	@brief	ゲームクライアント
//---------------------------------------------------------------------------
#include "GameClient.h"

bool GameClient::IsStateConnect()
{
    return _status == Status::Connecting ||
           _status == Status::Connected;
}

//---------------------------------------------------------------------------
//!	@file	サーバーからのパケットを処理する
//---------------------------------------------------------------------------
void GameClient::HandleRecv(const std::string& recvMsg)
{
    json recvJson = json::parse(recvMsg);
    std::cout << recvJson << std::endl;
    std::string key = recvJson["key"].get<std::string>();

    if(key == "Accept") {
        _clientState = ClientState::Connected;
        _connentId   = recvJson["id"].get<s32>();
        SendConnectInfo();
    }
    else {
        throw ErrorHandler("Unknow Message");
    }
}

void GameClient::SendConnectInfo()
{
    json sendBuffer;
    sendBuffer["key"]         = "Info";
    sendBuffer["id"]          = _connentId;
    sendBuffer["playerChara"] = 0;
    SetSendBuffer(sendBuffer.dump());
}
