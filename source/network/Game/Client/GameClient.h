﻿//---------------------------------------------------------------------------
//!	@file	GameClient.h
//!	@brief	ゲームクライアント
//---------------------------------------------------------------------------
#pragma once
#include "Core/Client/BaseClient.h"

class GameClient : public _network::BaseClient
{
    enum class ClientState
    {
        Disconnect,
		Connecting,
        Connected,
        Accept,
    };

public:
    bool IsStateConnect();

    void HandleRecv(const std::string& recvMsg) override;

    void SendConnectInfo();   //!< 初期設定を発送
private:
    s32 _connentId = 0;

    ClientState _clientState = ClientState::Disconnect;
};
