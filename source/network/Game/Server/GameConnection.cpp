﻿//---------------------------------------------------------------------------
//!	@file	GameConttion.cpp
//!	@brief	ゲームの接続処理
//---------------------------------------------------------------------------
#include "GameConnection.h"
#include "GameServer.h"

namespace _network {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
GameConnection::GameConnection()
{
}
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
GameConnection::~GameConnection()
{
}
//---------------------------------------------------------------------------
//! 通信したとき(一瞬)
//---------------------------------------------------------------------------
void GameConnection::OnConnected()
{
    BaseConnection::OnConnected();
}
//---------------------------------------------------------------------------
//! 届いたメッセージの処理
//---------------------------------------------------------------------------
void GameConnection::HandleRecv(const std::string& recvMsg)
{
    std::stringstream sstrFullRecv(recvMsg);
    std::string       strRecv;

    while(std::getline(sstrFullRecv, strRecv, '\n')) {
        std::stringstream sstrLineRecv(strRecv);
        std::string       cmd;
        sstrLineRecv >> cmd;
    }
}
}   // namespace _network
