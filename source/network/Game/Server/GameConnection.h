﻿//---------------------------------------------------------------------------
//!	@file	GameConttion.h
//!	@brief	ゲームの接続処理
//---------------------------------------------------------------------------
#pragma once
#include "Core/Server/BaseConnection.h"

namespace _network {

class GameConnection : public BaseConnection
{
public:
    GameConnection();
    ~GameConnection();

public:
    //---------------------------------------------------------------------------
    // 継承
    //---------------------------------------------------------------------------
    void OnConnected() override;                           //!<　通信したとき(一瞬)
    void HandleRecv(const std::string& recvMsg) override;   //!< 届いたメッセージの処理
};

}   // namespace _network
