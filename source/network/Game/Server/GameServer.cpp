﻿//---------------------------------------------------------------------------
//!	@file	GameServer.cpp
//!	@brief	ゲームサーバー
//---------------------------------------------------------------------------
#include "GameServer.h"
#include "GameConnection.h"

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
void GameServer::SendStartPkg()
{
}
void GameServer::SetGameStart()
{
    _isStarted = true;
}
bool GameServer::IsStarted()
{
    return _isStarted;
}
void GameServer::HandleConnected(_network::BaseConnection* connection, s32 size)
{
    json msg;
    msg["key"] = "Accept";
    msg["id"]  = size;
    connection->SetSendBuffer(msg.dump());
}
uni_ptr<_network::BaseConnection> GameServer::CreateConnection()
{
    return std::make_unique<_network::GameConnection>();
}
