﻿//---------------------------------------------------------------------------
//!	@file	GameServer.h
//!	@brief	ゲームサーバー
//---------------------------------------------------------------------------
#pragma once
#include "Core/Server/BaseServer.h"

class GameServer : public _network::BaseServer
{
public:
    void SendStartPkg();

    void SetGameStart();
    bool IsStarted();

    void HandleConnected(_network::BaseConnection* connection, s32 size);

private:
    bool _isStarted = false;

    uni_ptr<_network::BaseConnection> CreateConnection() override;
};
