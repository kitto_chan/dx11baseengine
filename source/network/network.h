﻿//---------------------------------------------------------------------------
//!	@file	network.h
//!	@brief	network共通ヘッダー
//---------------------------------------------------------------------------
#pragma once

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN   // 使用頻度の低いWindowsAPIを省略してヘッダー軽量化
#endif

#define NOMINMAX   // std::min std::maxとWindowsSDKのmin/maxマクロが衝突するためWindowsSDK側を無効化
#define STRICT     // Windowsオブジェクトの型を厳密に扱う

//--------------------------------------------------------------
//!	@defgroup	STL(Standard Template Library)
//--------------------------------------------------------------
//@{

#include <array>
#include <vector>
#include <string>
#include <string_view>
#include <memory>
#include <functional>
#include <algorithm>

#include <sstream>

#ifdef _WIN32
#include <Winsock2.h>   // must include before windows.h
#include <Windows.h>
#include <wrl.h>   // ComPtr用
#include <conio.h>
#pragma comment(lib, "Ws2_32.lib")
#else
#include <unistd.h>      // sleep()
#include <arpa/inet.h>   // htons
#include <sys/socket.h>
#endif

//@{
//---- ベクトル演算ライブラリ hlslpp
#pragma warning(push)
#pragma warning(disable : 26495)
#include <hlslpp/include/hlsl++.h>
using namespace hlslpp;
#pragma warning(pop)

//---- Json
#include <nlohmann/json.hpp>
using json = nlohmann::json;

//---- cereal
#include <cereal/cereal.hpp>   // シリアライザ
#include <cereal/archives/json.hpp>   // シリアライザ(Jsonフォンマット)
//@}
//--------------------------------------------------------------
//!	@defgroup	アプリケーション
//--------------------------------------------------------------
//@{
#include "../framework/utility/typedef.h"   // 型定義
#include "../framework/utility/raw_ptr.h"   // 生ポインタ

#include "Core/Utility/ErrorHandler.h"
//@}
